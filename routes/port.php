<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Port;
use App\Http\Controllers\ScanQrController;
use App\Http\Controllers\ScoreRuleController;
use App\Http\Controllers\JwtController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//网站web端接口

//后台管理员登录
Route::post('port/login/login', [Port\GroupLoginController::class, 'login']);
Route::get('port/login/getCaptcha', [Port\GroupLoginController::class, 'getCaptcha']); //获取图形验证码
#Route::get('slideVerify/getCaptcha', [App\Http\Controllers\SlideVerifyController::class, 'getCaptcha']); //获取滑动验证码参数


Route::get('port/auth/index', [Port\WechatAuthController::class, 'indexInfo'])->withoutMiddleware(['invite']); // 微信确认登陆后接受,获取信息   不能用 post  和 any，否则前端即使用get，也会预请求，造成 code 重复使用
Route::get('port/auth/appletIndex', [Port\WechatAuthController::class, 'appletIndexInfo'])->withoutMiddleware(['invite']); // 微信确认登陆后接受,获取信息   不能用 post  和 any，否则前端即使用get，也会预请求，造成 code 重复使用
Route::any('port/service/createParam', [Port\ServiceController::class, 'createParam'])->withoutMiddleware(['invite']); // 获取微信分享需要参数
#Route::get('/scoreRule/list', [ScoreRuleController::class, 'lists'])->withoutMiddleware('jwt'); //积分规则,因为微信也要使用，所以排除
#Route::get('/scoreRule/customScoreRule', [ScoreRuleController::class, 'customScoreRule'])->withoutMiddleware('jwt'); //获取自定义积分规则,因为微信也要使用，所以排除

#Route::post('/getToken', [JwtController::class, 'getAuthToken'])->withoutMiddleware(['jwt', 'invite']); //获取token,->withoutMiddleware([CheckJwtHeaderToken::class])排除这个中间件，因为在路由服务中间件中，自动运用了这个中间件

Route::get('port/userAccess/limitAccess', [Port\UserAccessDataController::class, 'limitAccess'])->withoutMiddleware(['invite']); // 获取活动访问人数（答题活动）

Route::get('port/index/checkInviteCode', [Port\IndexController::class, 'checkInviteCode']); // 第一次验证邀请码是否正确


//不需要token
Route::prefix('port')->group(function () {

    Route::get('/index/getAddressOrLonLat', [Port\IndexController::class, 'getAddressOrLonLat']); // 根据地址转经纬度、或根据经纬度转换地址

    Route::get('/userProtocol/getUserProtocol', [Port\UserProtocolController::class, 'getUserProtocol']); // 用户协议

    Route::get('/index/otherAccressAdd', [Port\IndexController::class, 'otherAccressAdd']); // 记录其他数据访问量

    /**
     * 图书馆相关（扫码借）
     */
    Route::get('/libInfo/getBookInfoList', [Port\LibInfoController::class, 'getBookInfoList']); // 获取检索信息列表
    Route::get('/libInfo/getLibBookDetail', [Port\LibInfoController::class, 'getLibBookDetail']); // 获取馆藏书籍详


    //新闻管理
    Route::get('/news/typeList', [Port\NewsController::class, 'typeList']); //新闻类型
    Route::get('/news/list', [Port\NewsController::class, 'lists']); //新闻列表

    //数字资源管理
    Route::get('/digital/typeList', [Port\DigitalController::class, 'typeList']); //数字资源类型
    Route::get('/digital/list', [Port\DigitalController::class, 'lists']); //列表
    Route::post('/digital/addBrowseNum', [Port\DigitalController::class, 'addBrowseNum']); //数字资源添加浏览量

    //好书推荐管理
    Route::get('/bookRecom/list', [Port\BookRecomController::class, 'lists']); //列表
    Route::get('/bookRecom/detail', [Port\BookRecomController::class, 'detail']); //详情

    /**
     * 场馆管理
     */
    Route::get('/branchInfo/list', [Port\BranchInfoController::class, 'lists']); // 场馆列表
    Route::get('/branchInfo/detail', [Port\BranchInfoController::class, 'detail']); // 场馆简介
    //   Route::post('/branch/addBrowseNum', [Port\BranchInfoController::class, 'addBrowseNum']); // 记录访问分馆的浏览量
    //   Route::post('/branch/branchAccountLogin', [Port\BranchInfoController::class, 'branchAccountLogin']); // 分馆用户登录账号登录



    /**
     * 活动管理
     */
    Route::get('/activity/getActivityCalendar', [Port\ActivityController::class, 'getActivityCalendar']); // 根据时间获取活动 展示活动日历
    Route::get('/activity/getActivityCalendarByTime', [Port\ActivityController::class, 'getActivityCalendarByTime']); // 根据时间获取活动 展示活动日历
    Route::get('/activity/getHotActivity', [Port\ActivityController::class, 'getHotActivity']); // 获取热门活动
    Route::get('/activity/getActivityRecom', [Port\ActivityController::class, 'getActivityRecom']); // 获取推荐，每个类型推荐
    Route::get('/activity/typeList', [Port\ActivityController::class, 'typeList']); // 活动类型列表
    Route::get('/activity/list', [Port\ActivityController::class, 'lists']); // 活动列表


    /**
     * 文旅地图
     */
    Route::get('/branchMap/list', [Port\BranchMapController::class, 'lists']); // 列表
    Route::get('/branchMap/detail', [Port\BranchMapController::class, 'detail']); // 详情
    /**
     * 根据商品类型  获取商品列表
     */
    Route::get('/goods/filterList', [Port\GoodsController::class, 'filterList']); // 类型(用于下拉框选择)


    /**
     * 预约
     */
    Route::get('/reservation/reservationNodeList', [Port\ReservationController::class, 'reservationNodeList']); // 预约种类列表
    Route::get('/reservation/reservationHallList', [Port\ReservationController::class, 'reservationHallList']); // 列表（每个种类都返回几个）
    Route::get('/reservation/list', [Port\ReservationController::class, 'lists']); //  预约列表
    //  Route::get('/reservation/detail', [Port\ReservationController::class, 'detail']); //  预约详情

    /**
     * 空间预约
     */
    Route::get('/studyRoomReservation/list', [Port\StudyRoomReservationController::class, 'lists']); //  空间预约列表

    /**
     * 志愿者服务
     */
    Route::get('/volunteer/volunteerPositionFilterList', [Port\VolunteerController::class, 'volunteerPositionFilterList']); // 服务岗位列表(用于下拉框选择)
    Route::get('/volunteer/getDegreeFilterList', [Port\VolunteerController::class, 'getDegreeFilterList']); // 教育程度(用于下拉框选择)
    Route::get('/volunteer/volunteerApplyParam', [Port\VolunteerController::class, 'volunteerApplyParam']); //  志愿者报名参数(用于下拉框选择)

    /**
     * 线上办证
     */
    Route::post('/onlineRegistration/sendVerify', [Port\OnlineRegistrationController::class, 'sendVerify']); // 发送验证码
    Route::get('/onlineRegistration/getOnlineParam', [Port\OnlineRegistrationController::class, 'getOnlineParam']); // 获取下拉选项参数
    Route::get('/onlineRegistration/getDynamicParam', [Port\OnlineRegistrationController::class, 'getDynamicParam']); // 获取动态参数

    Route::get('/onlineRegistration/getCardType', [Port\OnlineRegistrationController::class, 'getCardType']); //获取可办证的类型

    /**
     * 景点打卡
     */
    Route::get('/scenic/typeList', [Port\ScenicController::class, 'typeList']); // 书店筛选列表
    Route::get('/scenic/list', [Port\ScenicController::class, 'lists']); // 景点列表
    Route::get('/scenic/detail', [Port\ScenicController::class, 'detail']); // 景点详情

    /**
     * 图书到家（新书书籍）
     */
    Route::get('/newBookRecom/shopFiltrateList', [Port\NewBookRecomController::class, 'shopFiltrateList']); // 书店筛选列表
    Route::get('/newBookRecom/bookTypeFiltrateList', [Port\NewBookRecomController::class, 'bookTypeFiltrateList']); // 新书类型筛选列表

    /**
     * 图书到家（新书、馆藏书记录管理）
     */
    Route::get('/bookHomeBorrow/postageList', [Port\BookHomeBorrowController::class, 'postageList']); // 图书馆设置的运费详情(书店及图书馆)
    Route::get('/bookHomeBorrow/hotSearchList', [Port\BookHomeBorrowController::class, 'hotSearchList']); // 检索热词列表
    Route::get('/bookHomeBorrow/getCourierName', [Port\BookHomeBorrowController::class, 'getCourierName']); // 获取快递公司名称
    Route::get('/bookHomeBorrow/returnAddress', [Port\BookHomeBorrowController::class, 'returnAddress']); // 图书到家归还地址管理


    /**
     * 图书到家（馆藏书籍）
     */
    Route::get('/libBookRecom/bookTypeFiltrateList', [Port\LibBookRecomController::class, 'bookTypeFiltrateList']); // 馆藏书类型筛选列表
    Route::get('/libBookRecom/libBookList', [Port\LibBookRecomController::class, 'libBookList']); // 馆藏书检索


    /**
     * 服务指南
     */
    Route::get('/serviceDirectory/list', [Port\ServiceDirectoryController::class, 'lists']); //服务指南详情
    Route::get('/serviceDirectory/detail', [Port\ServiceDirectoryController::class, 'detail']); //服务指南详情


    /**
     * 室内导航管理
     */
    Route::get('/navigation/buildList', [Port\NavigationController::class, 'buildList']); //建筑列表
    Route::get('/navigation/areaFilterList', [Port\NavigationController::class, 'areaFilterList']); //区域简单列表
    Route::get('/navigation/areaList', [Port\NavigationController::class, 'areaList']); //区域列表
    Route::get('/navigation/pointList', [Port\NavigationController::class, 'pointList']); //查询指定区域点位列表
    Route::get('/navigation/pointSearchList', [Port\NavigationController::class, 'pointSearchList']); //点位搜索接口
    Route::get('/navigation/navigationLineList', [Port\NavigationController::class, 'navigationLineList']); //导航线路搜索

    /**
     * 线上大赛活动数据库
     */
    Route::get('/competiteActivityDatabase/list', [Port\CompetiteActivityDatabaseController::class, 'lists']); //列表
    Route::get('/competiteActivityDatabase/detail', [Port\CompetiteActivityDatabaseController::class, 'detail']); //详情
    Route::get('/competiteActivityDatabase/worksList', [Port\CompetiteActivityDatabaseController::class, 'worksList']); //数据库作品列表
    Route::get('/competiteActivityDatabase/ebookDetail', [Port\CompetiteActivityDatabaseController::class, 'ebookDetail']); //电子书详情+电子书作品列表


});

//token必选
Route::middleware(['check.web.token'])->prefix('port')->group(function () {


    /**
     * 个人中心
     */
    Route::get('/userInfo/index', [Port\UserInfoController::class, 'index']); // 个人中心首页


    //扫描二维码
    Route::post('/scanQr/scanAffirm', [ScanQrController::class, 'scanAffirm']); //扫描二维码

    //扫描二维码
    Route::post('/userInfo/wechatInfoChange', [Port\UserInfoController::class, 'wechatInfoChange']); //修改头像 和 昵称


    //新书荐购
    Route::post('/recommendBook/newBookRecom', [Port\RecommendBookController::class, 'newBookRecom']); //新书推荐
    Route::get('/recommendBook/myRecomList', [Port\RecommendBookController::class, 'myRecomList']); //我的荐购历史
    Route::post('/recommendBook/getBookInfo', [Port\RecommendBookController::class, 'getBookInfo']); //扫码关联其他书籍信息

    //用户留言管理
    Route::post('/feedback/add', [Port\FeedbackController::class, 'addFeedback']); //用户留言

    /**
     * 问卷调查
     */
    Route::get('/survey/list', [Port\SurveyController::class, 'lists']); // 列表
    Route::get('/survey/detail', [Port\SurveyController::class, 'detail']); // 详情
    Route::post('/survey/reply', [Port\SurveyController::class, 'reply']); // 回复

    /**
     * 活动管理
     */
    Route::get('/activity/myActList', [Port\ActivityController::class, 'myActList']); // 我的活动列表
    Route::post('/activity/apply', [Port\ActivityController::class, 'apply']); // 活动报名
    Route::post('/activity/cancel', [Port\ActivityController::class, 'cancel']); // 取消活动报名
    Route::get('/activity/getApplyQr', [Port\ActivityController::class, 'getApplyQr']); // 出示二维码


    /**
     * 根据商品类型  获取商品列表
     */
    Route::post('/goods/goodsCancelConversion', [Port\GoodsController::class, 'goodsCancelConversion']); // 商品取消兑换
    Route::post('/goods/goodsCancelOrder', [Port\GoodsController::class, 'goodsCancelOrder']); // 商品取消订单
    Route::get('/goods/orderList', [Port\GoodsController::class, 'orderList']); // 我的兑换列表
    Route::get('/goods/orderDetail', [Port\GoodsController::class, 'orderDetail']); // 我的兑换详情

    /**
     * 用户地址管理
     */
    Route::get('/userAddress/list', [Port\UserAddressController::class, 'lists']); // 列表
    Route::get('/userAddress/detail', [Port\UserAddressController::class, 'detail']); // 详情
    Route::post('/userAddress/add', [Port\UserAddressController::class, 'add']); // 新增
    Route::post('/userAddress/change', [Port\UserAddressController::class, 'change']); // 编辑
    Route::post('/userAddress/del', [Port\UserAddressController::class, 'del']); // 删除
    Route::post('/userAddress/switchoverDefault', [Port\UserAddressController::class, 'switchoverDefault']); // 修改默认状态


    /**
     * 系统消息
     */
    // Route::get('/systemInfo/list', [Port\SystemInfoController::class, 'lists']); // 列表
    // Route::get('/systemInfo/detail', [Port\SystemInfoController::class, 'detail']); // 详情

    /*用户获取消息列表*/
    Route::get('/system/systemList', [Port\SystemInfoController::class, 'systemList']); // 列表
    Route::get('/system/systemMessage', [Port\SystemInfoController::class, 'systemMessage']); //获取用户分类消息列表


    /**
     * 个人中心
     */
    Route::get('/userInfo/scoreList', [Port\UserInfoController::class, 'scoreList']); // 获取用户积分明细

    /**
     * 预约
     */
    Route::get('/reservation/seatList', [Port\ReservationController::class, 'seatList']); //  座位列表
    Route::post('/reservation/makeInfo', [Port\ReservationController::class, 'makeInfo']); // 文化配送 预约时间段
    Route::post('/reservation/cancelMake', [Port\ReservationController::class, 'cancelMake']); // 用户取消预约
    Route::get('/reservation/myMakeList', [Port\ReservationController::class, 'myMakeList']); //  我的预约列表
    Route::get('/reservation/getApplyQr', [Port\ReservationController::class, 'getApplyQr']); //  出示二维码

    /**
     * 空间预约
     */
    Route::post('/studyRoomReservation/makeInfo', [Port\StudyRoomReservationController::class, 'makeInfo']); // 文化配送 预约时间段
    Route::post('/studyRoomReservation/cancelMake', [Port\StudyRoomReservationController::class, 'cancelMake']); // 用户取消预约
    Route::get('/studyRoomReservation/myMakeList', [Port\StudyRoomReservationController::class, 'myMakeList']); //  我的预约列表
    Route::get('/studyRoomReservation/getApplyQr', [Port\StudyRoomReservationController::class, 'getApplyQr']); //  出示二维码

    /**
     * 年度账单
     */
    Route::get('/index/getYearBill', [Port\IndexController::class, 'getYearBill']); // 年度账单

    /**
     * 新闻
     */
    Route::post('/news/collectAndCancel', [Port\NewsController::class, 'collectAndCancel']); // 新闻收藏与取消收藏
    Route::get('/news/collectList', [Port\NewsController::class, 'collectList']); // 收藏新闻列表

    /**
     * 在线办证
     */
    Route::post('/onlineRegistration/registration', [Port\OnlineRegistrationController::class, 'registration']); // 办证
    Route::get('/onlineRegistration/historyList', [Port\OnlineRegistrationController::class, 'historyList']); // 办证历史记录
    Route::get('/onlineRegistration/detail', [Port\OnlineRegistrationController::class, 'detail']); // 办证详情信息


    /**
     * 景点打卡作品管理
     */
    Route::post('/scenicWorks/add', [Port\ScenicWorksController::class, 'add']); // 投稿
    Route::post('/scenicWorks/change', [Port\ScenicWorksController::class, 'change']); // 修改作品
    Route::post('/scenicWorks/worksVote', [Port\ScenicWorksController::class, 'worksVote']); // 打卡作品点赞与取消点赞
    Route::post('/scenicWorks/worksCancelAndDel', [Port\ScenicWorksController::class, 'worksCancelAndDel']); // 撤销、删除作品


    /**
     * 志愿者管理
     */
    Route::get('/volunteer/getVolunteerNotice', [Port\VolunteerController::class, 'getVolunteerNotice']); // 获取志愿者须知
    Route::get('/volunteer/getPullFilterList', [Port\VolunteerController::class, 'getPullFilterList']); // 参加志愿者所需下拉选项(用于下拉框选择)
    Route::get('/volunteer/volunteerApplyParam', [Port\VolunteerController::class, 'volunteerApplyParam']); // 志愿者报名参数(对应后台选择的参数)
    Route::get('/volunteer/volunteerDetail', [Port\VolunteerController::class, 'volunteerDetail']); // 志愿者详情
    Route::post('/volunteer/volunteerApply', [Port\VolunteerController::class, 'volunteerApply']); // 添加、修改或过期后重新提交用户志愿者信息  （修改后，自动提交到后台审核）
    Route::post('/volunteer/volunteerCancel', [Port\VolunteerController::class, 'volunteerCancel']); //  撤销志愿者


    /**
     * 图书到家（新书书籍）
     */
    Route::get('/newBookRecom/myNewBookCollectList', [Port\NewBookRecomController::class, 'myNewBookCollectList']); // 我的新书收藏列表
    Route::post('/newBookRecom/newBookSchoolbag', [Port\NewBookRecomController::class, 'newBookSchoolbag']); // 新书加入书袋与移出书袋
    Route::get('/newBookRecom/myNewBookSchoolbagList', [Port\NewBookRecomController::class, 'myNewBookSchoolbagList']); // 我的新书书袋列表
    Route::post('/newBookRecom/getOrderForecastPrice', [Port\NewBookRecomController::class, 'getOrderForecastPrice']); // 获取订单预估金额
    Route::post('/newBookRecom/getOrderInfo', [Port\NewBookRecomController::class, 'getOrderInfo']); // 获取预支付订单信息
    Route::get('/newBookRecom/myOrderList', [Port\NewBookRecomController::class, 'myOrderList']); // 我的 新书 订单列表
    Route::get('/newBookRecom/myOrderDetail', [Port\NewBookRecomController::class, 'myOrderDetail']); // 我的新书订单详情
    /**
     * 图书到家（新书、馆藏书记录管理）
     */
    Route::get('/bookHomeBorrow/borrowList', [Port\BookHomeBorrowController::class, 'borrowList']); // 采购借阅记录
    Route::get('/bookHomeBorrow/getNowBorrowList', [Port\BookHomeBorrowController::class, 'getNowBorrowList']); // 线上邮递书籍归还（获取当前借阅记录 ，包括新书或馆藏书）
    Route::post('/bookHomeBorrow/bookCollect', [Port\BookHomeBorrowController::class, 'bookCollect']); //  新书与馆藏书收藏与取消收藏
    Route::post('/bookHomeBorrow/recomReturn', [Port\BookHomeBorrowController::class, 'recomReturn']); //  书籍归还提交(包括新书和馆藏书)
    Route::post('/bookHomeBorrow/bookCancel', [Port\BookHomeBorrowController::class, 'bookCancel']); //  用户撤销线上书籍归还记录


    /**
     * 图书到家（馆藏书籍）
     */
    Route::get('/libBookRecom/myLibraryBookCollectList', [Port\LibBookRecomController::class, 'myLibraryBookCollectList']); // 我的馆藏书收藏列表
    Route::post('/libBookRecom/LibBookSchoolbag', [Port\LibBookRecomController::class, 'LibBookSchoolbag']); // 馆藏书加入书袋与移出书袋
    Route::get('/libBookRecom/myLibBookSchoolbagList', [Port\LibBookRecomController::class, 'myLibBookSchoolbagList']); // 我的馆藏书书袋列表


    /**
     * 新书推荐（独立功能）
     */
    Route::post('/newBookRecommend/newBookRecommendCollect', [Port\NewBookRecommendController::class, 'newBookRecommendCollect']); //  新书收藏与取消收藏
    Route::get('/newBookRecommend/myNewBookRecommendCollectList', [Port\NewBookRecommendController::class, 'myNewBookRecommendCollectList']); // 我的新书（推荐书）收藏列表


    /**
     * 阅读任务
     */
    Route::get('/readingTask/list', [Port\ReadingTaskController::class, 'lists']); // 阅读任务列表(我的阅读任务)
    Route::get('/readingTask/detail', [Port\ReadingTaskController::class, 'detail']); //阅读任务详情
    Route::get('/readingTask/worksList', [Port\ReadingTaskController::class, 'worksList']); //阅读任务数据库作品列表
    Route::get('/readingTask/ebookDetail', [Port\ReadingTaskController::class, 'ebookDetail']); //阅读任务 电子书详情+电子书作品列表
    Route::post('/readingTask/readingTaskReading', [Port\ReadingTaskController::class, 'readingTaskReading']); //用户阅读任务记录表
});

//token可选
Route::middleware(['check.may.web.token'])->prefix('port')->group(function () {

    //在线办证（行为分析时需要token，所以加在这里）
    Route::get('/onlineRegistration/getCertificateType', [Port\OnlineRegistrationController::class, 'getCertificateType']); //获取证件类型
    //馆藏检索（行为分析时需要token，所以加在这里）
    Route::get('/libInfo/hotLibSearchList', [Port\LibInfoController::class, 'hotLibSearchList']); // 馆藏检索热词列表


    //首页接口
    Route::get('/index/index', [Port\IndexController::class, 'index']); //首页

    /**
     * 活动管理
     */
    Route::get('/activity/detail', [Port\ActivityController::class, 'detail']); // 活动详情(我的活动详情)
    /**
     * 根据商品类型  获取商品列表
     */
    Route::get('/goods/list', [Port\GoodsController::class, 'lists']); // 列表
    Route::get('/goods/detail', [Port\GoodsController::class, 'detail']); // 详情


    /**
     * 预约
     */
    Route::get('/reservation/detail', [Port\ReservationController::class, 'detail']); //  预约详情

    /**
     * 空间预约
     */
    Route::get('/studyRoomReservation/detail', [Port\StudyRoomReservationController::class, 'detail']); //  预约详情

    /**
     * 新闻
     */
    Route::get('/news/detail', [Port\NewsController::class, 'detail']); //新闻详情

    /**
     * 景点打卡作品管理
     */
    Route::get('/scenicWorks/list', [Port\ScenicWorksController::class, 'lists']); // 作品列表
    Route::get('/scenicWorks/detail', [Port\ScenicWorksController::class, 'detail']); // 作品详情


    /**
     * 图书到家
     */
    Route::get('/bookHomeBorrow/index', [Port\BookHomeBorrowController::class, 'index']); // 图书到家主界面数据
    /**
     * 图书到家（新书书籍）
     */
    Route::get('/newBookRecom/newBookList', [Port\NewBookRecomController::class, 'newBookList']); // 新书列表
    Route::get('/newBookRecom/newBookDetail', [Port\NewBookRecomController::class, 'newBookDetail']); // 新书详情

    /**
     * 图书到家（馆藏书籍）
     */
    Route::get('/libBookRecom/getRecomList', [Port\LibBookRecomController::class, 'getRecomList']); //  馆藏书推荐列表（推荐书的接口）
    Route::get('/libBookRecom/getLibBookDetail', [Port\LibBookRecomController::class, 'getLibBookDetail']); // 馆藏书类型筛选列表

    /**
     * 新书推荐（独立功能）
     */
    Route::get('/newBookRecommend/typeList', [Port\NewBookRecommendController::class, 'typeList']); //  类型列表
    Route::get('/newBookRecommend/list', [Port\NewBookRecommendController::class, 'lists']); //  列表
    Route::get('/newBookRecommend/detail', [Port\NewBookRecommendController::class, 'detail']); // 详情


    /**
     * 码上借
     */
    Route::get('/codeOnBorrow/getCodeInfo', [Port\CodeOnBorrowController::class, 'getCodeInfo']); // 扫码借，扫描场所码后，获取场所信息，验证场所数据
    Route::get('/codeOnBorrow/getBookInfoByBarcode', [Port\CodeOnBorrowController::class, 'getBookInfoByBarcode']); //  扫码借书，还书，获取书籍信息
    Route::post('/codeOnBorrow/returnBook', [Port\CodeOnBorrowController::class, 'returnBook']); //  归还书籍

    Route::get('/codeOnBorrow/borrowList', [Port\CodeOnBorrowController::class, 'borrowList']); //  借阅操作列表
    Route::get('/codeOnBorrow/returnList', [Port\CodeOnBorrowController::class, 'returnList']); //  归还操作列表

    /**
     * 扫码导游 - 作品管理
     */
    Route::get('/codeGuideProduction/exhibitionList', [Port\CodeGuideProductionController::class, 'exhibitionList']); // 展览列表
    Route::get('/codeGuideProduction/exhibitionDetail', [Port\CodeGuideProductionController::class, 'exhibitionDetail']); // 展览详情
    Route::get('/codeGuideProduction/list', [Port\CodeGuideProductionController::class, 'lists']); // 作品列表
    Route::get('/codeGuideProduction/detail', [Port\CodeGuideProductionController::class, 'detail']); // 作品详情


     /**
     * 线上大赛
     */
    Route::get('/competiteActivity/productionList', [Port\CompetiteActivityController::class, 'productionList']); // 参赛作品列表

});


//必须绑定读者证
Route::middleware(['check.bind.reader.token'])->prefix('port')->group(function () {

    /**
     * 根据商品类型  获取商品列表
     */
    Route::post('/goods/goodsConversion', [Port\GoodsController::class, 'goodsConversion']); // 商品兑换 生成预支付订单


    Route::post('/libInfo/unBindReaderId', [Port\LibInfoController::class, 'unBindReaderId']); // 解除绑定读者证
    Route::post('/libInfo/changePwd', [Port\LibInfoController::class, 'changePwd']); // 修改读者证密码
    Route::post('/libInfo/cardDelay', [Port\LibInfoController::class, 'cardDelay']); // 读者证延期
    Route::post('/libInfo/lost', [Port\LibInfoController::class, 'lost']); // 读者证挂失操作
    Route::post('/libInfo/lostRecover', [Port\LibInfoController::class, 'lostRecover']); // 读者证恢复

    Route::get('/libInfo/getBorrowList', [Port\LibInfoController::class, 'getBorrowList']); // 获取借阅记录列表
    Route::post('/libInfo/renew', [Port\LibInfoController::class, 'renewBook']); // 在线续借

    /**
     * 积分签到
     */
    Route::get('/signScore/signScoreInfo', [Port\SignScoreController::class, 'signScoreInfo']); // 获取积分签到信息
    Route::post('/signScore/sign', [Port\SignScoreController::class, 'sign']); // 用户打卡签到

    /**
     * 欠费缴纳
     */
    Route::get('/oweProcess/oweList', [Port\OweProcessController::class, 'oweList']); // 欠费列表
    Route::post('/oweProcess/owePay', [Port\OweProcessController::class, 'owePay']); // 欠费支付 生成预支付订单

    /**
     * 图书到家（馆藏书籍）
     */
    Route::post('/libBookRecom/getOrderForecastPrice', [Port\LibBookRecomController::class, 'getOrderForecastPrice']); // 获取订单预估金额
    Route::post('/libBookRecom/getOrderInfo', [Port\LibBookRecomController::class, 'getOrderInfo']); // 获取预支付订单信息
    Route::get('/libBookRecom/myOrderList', [Port\LibBookRecomController::class, 'myOrderList']); // 我的 馆藏书 订单列表
    Route::get('/libBookRecom/myOrderDetail', [Port\LibBookRecomController::class, 'myOrderDetail']); // 我的馆藏书订单详情
    Route::post('/libBookRecom/bookRenew', [Port\LibBookRecomController::class, 'bookRenew']); // 馆藏书续借功能

    Route::post('/bookHomeBorrow/cancelBookHomeOrder', [Port\BookHomeBorrowController::class, 'cancelBookHomeOrder']); // 取消图书到家订单


    /**
     * 码上借
     */
    Route::post('/codeOnBorrow/borrowBook', [Port\CodeOnBorrowController::class, 'borrowBook']); //  借阅书籍

});


######################################################抽奖活动start#############################################################

Route::middleware(['limit.access.data'])->prefix('port')->group(function () {
    /**
     * 答题活动管理
     */
    Route::get('/answerActivity/typeList', [Port\AnswerActivityController::class, 'typeList']); //  活动类型列表
    Route::get('/answerActivity/list', [Port\AnswerActivityController::class, 'lists']); //  根据活动类型获取活动列表
    /**
     * 在线抽奖活动管理
     */
    Route::get('/turnActivity/list', [Port\TurnActivityController::class, 'lists']); //  根据活动类型获取活动列表
    /**
     *  活动区域管理
     */
    Route::get('/answerActivityArea/list', [Port\AnswerActivityAreaController::class, 'lists']); //  列表

    /**
     * 抽奖活动推荐记录
     */
    Route::get('/drawActivity/getRecomDrawActivity', [Port\DrawActivityController::class, 'getRecomDrawActivity']); // 抽奖活动推荐列表(3种活动)

    /**
     * 获取资源路径及颜色等信息
     */
    Route::get('/answerActivity/getResourceInfo', [Port\AnswerActivityController::class, 'getResourceInfo']); // 获取资源路径及颜色等信息

    /**
     * 获取资源路径及颜色等信息
     */
    Route::get('/turnActivity/getResourceInfo', [Port\TurnActivityController::class, 'getResourceInfo']); // 获取资源路径及颜色等信息

});

//token必选
Route::middleware(['check.web.token', 'limit.access.data'])->prefix('port')->group(function () {

    Route::post('/libInfo/bingReaderId', [Port\LibInfoController::class, 'bingReaderId']); // 绑定读者证  或 密码错误重新登录


    //分享活动答题机会
    Route::post('/userDrawShare/index', [Port\UserDrawShareController::class, 'index']); //分享活动答题机会

    Route::post('/answerActivity/scanActInfo', [Port\AnswerActivityController::class, 'scanActInfo']); //扫码后获取活动信息

    /**
     * 答题活动详情
     */
    Route::get('/answerActivity/detail', [Port\AnswerActivityController::class, 'detail']); //获取答题活动详情 （根据类型不同，调用不同的接口  act_type_id 为 1 调用此接口）
    Route::get('/answerActivity/getStairsFloor', [Port\AnswerActivityController::class, 'getStairsFloor']); //爬楼梯模式获取当前楼层


    Route::get('/userDrawAddress/getUserAddress', [Port\UserDrawAddressController::class, 'getUserAddress']); //  获取用户地址信息
    Route::post('/userDrawAddress/setUserAddress', [Port\UserDrawAddressController::class, 'setUserAddress']); //  设置用户地址信息

    Route::get('/userDrawInvite/checkInvitCode', [Port\UserDrawInviteController::class, 'checkInvitCode']); //  是否需要邀请码 且 是否已经输入了邀请码
    Route::post('/userDrawInvite/setInvitCode', [Port\UserDrawInviteController::class, 'setInvitCode']); //   输入邀请码

    Route::get('/userDrawUnit/checkRealInfo', [Port\UserDrawUnitController::class, 'checkRealInfo']); //  是否需要填写用户信息和单位信息 且 是否已经输入了用户信息
    Route::post('/userDrawUnit/setRealInfo', [Port\UserDrawUnitController::class, 'setRealInfo']); //   输入用户信息 或 所属图书馆

    /**
     *  答题活动单位管理
     */
    Route::get('/answerActivityUnit/list', [Port\AnswerActivityUnitController::class, 'lists']); //  列表
    Route::get('/answerActivityUnit/detail', [Port\AnswerActivityUnitController::class, 'detail']); //  详情
    /**
     * 答题活动答题管理
     */
    Route::get('/answerActivityAnswer/getAnswerProblem', [Port\AnswerActivityAnswerController::class, 'getAnswerProblem']); //开始答题（获取题目信息）
    Route::post('/answerActivityAnswer/replyProblem', [Port\AnswerActivityAnswerController::class, 'replyProblem']); //用户答题
    /**
     * 答题活动排名
     */
    Route::get('/answerActivityRanking/ranking', [Port\AnswerActivityRankingController::class, 'ranking']); //活动排名
    Route::get('/answerActivityRanking/myGrade', [Port\AnswerActivityRankingController::class, 'myGrade']); //我的成绩单

    /**
     * 答题活动礼物公示管理
     */
    Route::get('/answerActivityGiftPublic/list', [Port\AnswerActivityGiftPublicController::class, 'lists']); //列表

    /**
     * 线上大赛
     */
    // Route::get('/competiteActivity/myCompetiteLists', [Port\CompetiteActivityController::class, 'myCompetiteLists']); // 我的大赛列表
    // Route::get('/competiteActivity/getTypeList', [Port\CompetiteActivityController::class, 'getTypeList']); // 线上大赛类型
    // Route::get('/competiteActivity/myProductionList', [Port\CompetiteActivityController::class, 'myProductionList']); // 我的参赛作品列表
    // Route::get('/competiteActivity/getWorksUserInfo', [Port\CompetiteActivityController::class, 'getWorksUserInfo']); // 根据电话号码获取用户基本信息
    // Route::post('/competiteActivity/worksVote', [Port\CompetiteActivityController::class, 'worksVote']); // 作品投票
    // Route::post('/competiteActivity/worksCancelAndDel', [Port\CompetiteActivityController::class, 'worksCancelAndDel']); // 撤销、删除作品
    // Route::post('/competiteActivity/worksAdd', [Port\CompetiteActivityController::class, 'worksAdd']); // 上传作品
    // Route::post('/competiteActivity/worksChange', [Port\CompetiteActivityController::class, 'worksChange']); // 修改作品

    /**
     * 答题活动抽奖
     */
    Route::post('/answerActivityDraw/draw', [Port\AnswerActivityDrawController::class, 'draw']); // 抽奖
    Route::post('/answerActivityDraw/getDrawResult', [Port\AnswerActivityDrawController::class, 'getDrawResult']); // 获取抽奖结果

    Route::get('/answerActivityUserGift/list', [Port\AnswerActivityUserGiftController::class, 'lists']); // 获取用户礼物列表


    /**
     * 在线抽奖活动详情
     */
    Route::get('/turnActivity/detail', [Port\TurnActivityController::class, 'detail']); //获取在线抽奖活动详情 （根据类型不同，调用不同的接口  act_type_id 为 2 调用此接口）

    /**
     * 在线抽奖活动抽奖
     */
    Route::post('/turnActivityDraw/draw', [Port\TurnActivityDrawController::class, 'draw']); // 抽奖
    Route::post('/turnActivityDraw/getDrawResult', [Port\TurnActivityDrawController::class, 'getDrawResult']); // 获取抽奖结果

    Route::get('/turnActivityUserGift/list', [Port\TurnActivityUserGiftController::class, 'lists']); // 获取用户礼物列表
});



//token必选
Route::middleware(['check.web.group.token'])->prefix('port')->group(function () {
    /**
     * 线上大赛
     */
    Route::get('/competiteActivity/myCompetiteLists', [Port\CompetiteActivityController::class, 'myCompetiteLists']); // 我的大赛列表
    Route::get('/competiteActivity/myProductionList', [Port\CompetiteActivityController::class, 'myProductionList']); // 我的参赛作品列表
    Route::get('/competiteActivity/getWorksUserInfo', [Port\CompetiteActivityController::class, 'getWorksUserInfo']); // 根据电话号码获取用户基本信息
    Route::post('/competiteActivity/worksVote', [Port\CompetiteActivityController::class, 'worksVote']); // 作品投票
    Route::post('/competiteActivity/worksCancelAndDel', [Port\CompetiteActivityController::class, 'worksCancelAndDel']); // 撤销、删除作品
    Route::post('/competiteActivity/worksAdd', [Port\CompetiteActivityController::class, 'worksAdd']); // 上传作品
    Route::post('/competiteActivity/worksChange', [Port\CompetiteActivityController::class, 'worksChange']); // 修改作品
});

//token可选
Route::middleware(['check.may.web.group.token'])->prefix('port')->group(function () {
    /**
     * 线上大赛
     */
    Route::get('/competiteActivity/getTypeList', [Port\CompetiteActivityController::class, 'getTypeList']); // 线上大赛类型
    Route::get('/competiteActivity/lists', [Port\CompetiteActivityController::class, 'lists']); // 列表
    Route::get('/competiteActivity/detail', [Port\CompetiteActivityController::class, 'detail']); // 详情
    Route::get('/competiteActivity/productionDetail', [Port\CompetiteActivityController::class, 'productionDetail']); // 参赛作品详情+我的作品详情
});

######################################################抽奖活动end#############################################################


/******************************************************图片直播start*************************************************************** */
//token必选
Route::middleware(['check.web.token', 'limit.access.data'])->prefix('port')->group(function () {

    /**
     * 图片直播管理
     */
    Route::get('/pictureLive/detail', [Port\PictureLiveController::class, 'detail']); // 获取直播活动详情 （根据类型不同，调用不同的接口  act_type_id 为 4 调用此接口）

    /**
     * 图片直播作品管理
     */
    Route::get('/pictureLiveWorks/list', [Port\PictureLiveWorksController::class, 'lists']); // 获取图片列表
    Route::get('/pictureLiveWorks/detail', [Port\PictureLiveWorksController::class, 'detail']); // 获取图片详情
    Route::post('/pictureLiveWorks/addBrowseNumber', [Port\PictureLiveWorksController::class, 'addBrowseNumber']); // 添加图片浏览量

    Route::get('/pictureLiveWorks/surplusUploadsNumber', [Port\PictureLiveWorksController::class, 'surplusUploadsNumber']); // 获取剩余可上传图片张数
    Route::post('/pictureLiveWorks/add', [Port\PictureLiveWorksController::class, 'add']); // 图片直播，上传图片

    Route::post('/pictureLiveWorks/worksCancelAndDel', [Port\PictureLiveWorksController::class, 'worksCancelAndDel']); //  撤销、删除、重新上传作品
    Route::post('/pictureLiveWorks/worksVote', [Port\PictureLiveWorksController::class, 'worksVote']); //  作品投票


    /**
     * 直播图片个人中心
     */
    Route::get('/pictureLiveWorks/myBrowseLists', [Port\PictureLivePersonalCenterController::class, 'myBrowseLists']); //  我浏览过的活动列表
    Route::get('/pictureLiveWorks/myPictureLiveLists', [Port\PictureLivePersonalCenterController::class, 'myPictureLiveLists']); //  我的图片直播活动列表及图片
    Route::get('/pictureLiveWorks/myPictureLivePhotoLists', [Port\PictureLivePersonalCenterController::class, 'myPictureLivePhotoLists']); //  我的图片列表
    Route::get('/pictureLiveWorks/myPictureLiveDetail', [Port\PictureLivePersonalCenterController::class, 'myPictureLiveDetail']); //  我的图片详情


    Route::get('/pictureLiveWorks/myVotePictureLiveLists', [Port\PictureLivePersonalCenterController::class, 'myVotePictureLiveLists']); //  我的点赞过的直播活动列表及图片
    Route::get('/pictureLiveWorks/myVotePictureLivePhotoLists', [Port\PictureLivePersonalCenterController::class, 'myVotePictureLivePhotoLists']); //   我点赞过的图片列表

});
//token可选
Route::middleware(['check.may.web.token', 'limit.access.data'])->prefix('port')->group(function () {

    Route::get('/pictureLive/list', [Port\PictureLiveController::class, 'lists']); // 直播活动列表 （根据类型不同，调用不同的接口  act_type_id 为 4 调用此接口）
    Route::get('/pictureLive/liveLists', [Port\PictureLiveController::class, 'liveLists']); // 直播活动（图片+视频）列表 （根据类型不同，调用不同的接口  act_type_id 为 4 调用此接口）

});

/******************************************************图片直播end*************************************************************** */
/******************************************************视频直播end*************************************************************** */
//token可选
Route::middleware(['check.may.web.token'])->prefix('port')->group(function () {

    Route::get('/videoLive/list', [Port\VideoLiveController::class, 'lists']); // 视频直播活动列表 （根据类型不同，调用不同的接口  act_type_id 为 5 调用此接口）
    Route::get('/videoLive/detail', [Port\VideoLiveController::class, 'detail']); // 视频直播活动详情 （根据类型不同，调用不同的接口  act_type_id 为 5 调用此接口）
    Route::get('/videoLive/getVideoLiveStatus', [Port\VideoLiveController::class, 'getVideoLiveStatus']); // 获取直播状态

});
//token必选
Route::middleware(['check.web.token', 'limit.access.data'])->prefix('port')->group(function () {

    Route::post('/videoLive/voteAndCancel', [Port\VideoLiveController::class, 'voteAndCancel']); //  点赞与取消点赞


});

/******************************************************视频直播end*************************************************************** */
