<?php

use App\Http\Controllers\AdminMobile;
use App\Http\Controllers;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




// 后台接口

//后台管理员登录
Route::post('adminMobile/login/login', [AdminMobile\LoginController::class, 'login']);
Route::get('adminMobile/login/getCaptcha', [AdminMobile\LoginController::class, 'getCaptcha']); //获取图形验证码
Route::get('slideVerify/getCaptcha', [App\Http\Controllers\SlideVerifyController::class, 'getCaptcha']); //获取滑动验证码参数



Route::middleware(['check.admin.mobile.header.token', 'check.admin.mobile.permission'])->prefix('adminMobile')->group(function () {

    /**
     * 应用显示灰色
     */
    // Route::get('/appViewGray/list', [AdminMobile\AppViewGrayController::class, 'lists']); // 列表
    // Route::get('/appViewGray/detail', [AdminMobile\AppViewGrayController::class, 'detail']); // 详情
    // Route::post('/appViewGray/add', [AdminMobile\AppViewGrayController::class, 'add']); // 新增
    // Route::post('/appViewGray/change', [AdminMobile\AppViewGrayController::class, 'change']); // 编辑
    // Route::post('/appViewGray/del', [AdminMobile\AppViewGrayController::class, 'del']); // 删除


    // /**
    //  * 协议规则
    //  */
    // Route::get('/userProtocol/getProtocol', [AdminMobile\UserProtocolController::class, 'getProtocol']); //获取用户协议
    // Route::post('/userProtocol/setProtocol', [AdminMobile\UserProtocolController::class, 'setProtocol']); //配置用户协议


    /**
     * 管理员管理
     */
    Route::get('/manage/filterList', [AdminMobile\ManageController::class, 'filterList']);
    Route::get('/manage/list', [AdminMobile\ManageController::class, 'lists']);
    Route::get('/manage/detail', [AdminMobile\ManageController::class, 'detail']);
    Route::post('/manage/add', [AdminMobile\ManageController::class, 'add']);
    Route::post('/manage/change', [AdminMobile\ManageController::class, 'change']);
    Route::post('/manage/del', [AdminMobile\ManageController::class, 'del']);
    Route::post('/manage/changePwd', [AdminMobile\ManageController::class, 'manageChangePwd']); //修改密码
    Route::post('/manage/changeSelfPwd', [AdminMobile\ManageController::class, 'manageChangeSelfPwd']); //修改自己的密码


    /**
     * 新闻类型
     */
    // Route::get('/newsType/filterList', [AdminMobile\NewsTypeController::class, 'filterList']); // 筛选列表
    // Route::get('/newsType/list', [AdminMobile\NewsTypeController::class, 'lists']); // 列表
    // Route::get('/newsType/detail', [AdminMobile\NewsTypeController::class, 'detail']); // 详情
    // Route::post('/newsType/add', [AdminMobile\NewsTypeController::class, 'add']); // 新增
    // Route::post('/newsType/change', [AdminMobile\NewsTypeController::class, 'change']); // 编辑
    // Route::post('/newsType/del', [AdminMobile\NewsTypeController::class, 'del']); // 删除

    // /**
    //  * 新闻
    //  */
    // Route::get('/news/list', [AdminMobile\NewsController::class, 'lists']); // 列表
    // Route::get('/news/detail', [AdminMobile\NewsController::class, 'detail']); // 详情
    // Route::post('/news/add', [AdminMobile\NewsController::class, 'add']); // 新增
    // Route::post('/news/change', [AdminMobile\NewsController::class, 'change']); // 编辑
    // Route::post('/news/del', [AdminMobile\NewsController::class, 'del']); // 删除
    // Route::post('/news/topAndCancel', [AdminMobile\NewsController::class, 'topAndCancel']); // 置顶与取消置顶


    // /**
    //  * 服务指南
    //  */
    // Route::get('/serviceDirectory/list', [AdminMobile\ServiceDirectoryController::class, 'lists']); // 列表
    // Route::get('/serviceDirectory/detail', [AdminMobile\ServiceDirectoryController::class, 'detail']); // 详情
    // Route::post('/serviceDirectory/add', [AdminMobile\ServiceDirectoryController::class, 'add']); // 新增
    // Route::post('/serviceDirectory/change', [AdminMobile\ServiceDirectoryController::class, 'change']); // 编辑
    // Route::post('/serviceDirectory/del', [AdminMobile\ServiceDirectoryController::class, 'del']); // 删除

    // /**
    //  * 数字阅读类型
    //  */
    // Route::get('/digitalType/list', [AdminMobile\DigitalTypeController::class, 'lists']);
    // Route::get('/digitalType/detail', [AdminMobile\DigitalTypeController::class, 'detail']);
    // Route::post('/digitalType/add', [AdminMobile\DigitalTypeController::class, 'add']);
    // Route::post('/digitalType/change', [AdminMobile\DigitalTypeController::class, 'change']);
    // Route::post('/digitalType/del', [AdminMobile\DigitalTypeController::class, 'del']);
    // Route::get('/digitalType/filterList', [AdminMobile\DigitalTypeController::class, 'filterList']); //数字阅读类型列表（用于筛选）


    // /**
    //  * 数字阅读
    //  */
    // Route::get('/digital/list', [AdminMobile\DigitalController::class, 'lists']); // 列表
    // Route::get('/digital/detail', [AdminMobile\DigitalController::class, 'detail']); // 详情
    // Route::post('/digital/add', [AdminMobile\DigitalController::class, 'add']); // 新增
    // Route::post('/digital/change', [AdminMobile\DigitalController::class, 'change']); // 编辑
    // Route::post('/digital/del', [AdminMobile\DigitalController::class, 'del']); // 删除
    // Route::post('/digital/cancelAndRelease', [AdminMobile\DigitalController::class, 'cancelAndRelease']); //  撤销 和发布
    // Route::post('/digital/sortChange', [AdminMobile\DigitalController::class, 'sortChange']); // 数字阅读排序

    // /**
    //  * 新书推荐类型
    //  */
    // Route::get('/newBookRecommendType/filterList', [AdminMobile\NewBookRecommendTypeController::class, 'filterList']); // 筛选列表
    // Route::get('/newBookRecommendType/list', [AdminMobile\NewBookRecommendTypeController::class, 'lists']); // 列表
    // Route::get('/newBookRecommendType/detail', [AdminMobile\NewBookRecommendTypeController::class, 'detail']); // 详情
    // Route::post('/newBookRecommendType/add', [AdminMobile\NewBookRecommendTypeController::class, 'add']); // 新增
    // Route::post('/newBookRecommendType/change', [AdminMobile\NewBookRecommendTypeController::class, 'change']); // 编辑
    // Route::post('/newBookRecommendType/del', [AdminMobile\NewBookRecommendTypeController::class, 'del']); // 删除

    // /**
    //  * 新书推荐
    //  */
    // Route::get('/newBookRecommend/list', [AdminMobile\NewBookRecommendController::class, 'lists']); // 列表
    // Route::get('/newBookRecommend/detail', [AdminMobile\NewBookRecommendController::class, 'detail']); // 详情
    // Route::post('/newBookRecommend/add', [AdminMobile\NewBookRecommendController::class, 'add']); // 新增
    // Route::post('/newBookRecommend/change', [AdminMobile\NewBookRecommendController::class, 'change']); // 编辑
    // Route::post('/newBookRecommend/del', [AdminMobile\NewBookRecommendController::class, 'del']); // 删除
    // Route::post('/newBookRecommend/cancelAndRelease', [AdminMobile\NewBookRecommendController::class, 'cancelAndRelease']); // 推荐与取消推荐


    /**
     * 图书推荐
     */
    // Route::get('/bookRecom/list', [AdminMobile\BookRecomController::class, 'lists']); // 列表
    // Route::get('/bookRecom/detail', [AdminMobile\BookRecomController::class, 'detail']); // 详情
    // Route::post('/bookRecom/add', [AdminMobile\BookRecomController::class, 'add']); // 新增
    // Route::post('/bookRecom/change', [AdminMobile\BookRecomController::class, 'change']); // 编辑
    // Route::post('/bookRecom/del', [AdminMobile\BookRecomController::class, 'del']); // 删除
    // Route::post('/bookRecom/recomAndCancel', [AdminMobile\BookRecomController::class, 'recomAndCancel']); // 推荐与取消推荐


    /**
     * 活动
     */
    Route::get('/activity/getActivityApplyParam', [AdminMobile\ActivityController::class, 'getActivityApplyParam']); // 获取报名参数
    Route::get('/activity/list', [AdminMobile\ActivityController::class, 'lists']); // 列表
    Route::get('/activity/detail', [AdminMobile\ActivityController::class, 'detail']); // 详情
    Route::post('/activity/add', [AdminMobile\ActivityController::class, 'add']); // 新增
    Route::post('/activity/change', [AdminMobile\ActivityController::class, 'change']); // 编辑
    Route::post('/activity/del', [AdminMobile\ActivityController::class, 'del']); // 删除
    Route::post('/activity/cancel', [AdminMobile\ActivityController::class, 'cancel']); // 活动撤销
    Route::post('/activity/release', [AdminMobile\ActivityController::class, 'release']); // 活动发布
    Route::post('/activity/sign', [AdminMobile\ActivityController::class, 'sign']); // 签到
    Route::post('/activity/switchoverType', [AdminMobile\ActivityController::class, 'switchoverType']); // 切换类型
    Route::post('/activity/recomAndCancel', [AdminMobile\ActivityController::class, 'recomAndCancel']); // 活动推荐与取消推荐
    Route::post('/activity/push', [AdminMobile\ActivityController::class, 'push']); // 活动推送 发送模板消息给用户

    /**
     * 活动报名信息管理
     */
    Route::get('/activityApply/activityApplyList', [AdminMobile\ActivityApplyController::class, 'activityApplyList']); // 活动申请列表
    Route::get('/activityApply/activityApplyUser', [AdminMobile\ActivityApplyController::class, 'activityApplyUser']); // 活动人员列表
    Route::post('/activityApply/agreeAndRefused', [AdminMobile\ActivityApplyController::class, 'agreeAndRefused']); // 审核通过 和 拒绝
    Route::post('/activityApply/violateAndCancel', [AdminMobile\ActivityApplyController::class, 'violateAndCancel']); // 活动报名违规 与 取消违规操作

    /**
     * 活动类型
     */
    Route::get('/activityType/filterList', [AdminMobile\ActivityTypeController::class, 'filterList']); // 筛选列表
    Route::get('/activityType/list', [AdminMobile\ActivityTypeController::class, 'lists']); // 列表
    Route::get('/activityType/detail', [AdminMobile\ActivityTypeController::class, 'detail']); // 详情
    Route::post('/activityType/add', [AdminMobile\ActivityTypeController::class, 'add']); // 新增
    Route::post('/activityType/change', [AdminMobile\ActivityTypeController::class, 'change']); // 编辑
    Route::post('/activityType/del', [AdminMobile\ActivityTypeController::class, 'del']); // 删除


    /**
     * 商品类型管理
     */
    // Route::get('/goodsType/filterList', [AdminMobile\GoodsTypeController::class, 'filterList']); // 筛选列表
    // Route::get('/goodsType/list', [AdminMobile\GoodsTypeController::class, 'lists']); // 列表
    // Route::get('/goodsType/detail', [AdminMobile\GoodsTypeController::class, 'detail']); // 详情
    // Route::post('/goodsType/add', [AdminMobile\GoodsTypeController::class, 'add']); // 新增
    // Route::post('/goodsType/change', [AdminMobile\GoodsTypeController::class, 'change']); // 编辑
    // Route::post('/goodsType/del', [AdminMobile\GoodsTypeController::class, 'del']); // 删除

    // /**
    //  * 商品管理
    //  */
    // Route::get('/goods/list', [AdminMobile\GoodsController::class, 'lists']); // 列表
    // Route::get('/goods/detail', [AdminMobile\GoodsController::class, 'detail']); // 详情
    // Route::post('/goods/add', [AdminMobile\GoodsController::class, 'add']); // 新增
    // Route::post('/goods/change', [AdminMobile\GoodsController::class, 'change']); // 编辑
    // Route::post('/goods/del', [AdminMobile\GoodsController::class, 'del']); // 删除
    // Route::post('/goods/goodsOnAndOff', [AdminMobile\GoodsController::class, 'goodsOnAndOff']); // 上架与下架商品
    // Route::get('/goods/getGoodsPickAddress', [AdminMobile\GoodsController::class, 'getGoodsPickAddress']); // 获取商品自提地址
    // Route::post('/goods/setGoodsPickAddress', [AdminMobile\GoodsController::class, 'setGoodsPickAddress']); // 设置商品自提地址

    /**
     * 商品兑换管理
     */
    // Route::get('/goodsRecord/list', [AdminMobile\GoodsRecordController::class, 'lists']); // 用户商品兑换记录
    // Route::post('/goodsRecord/fetch', [AdminMobile\GoodsRecordController::class, 'fetchGoods']); // 领取
    // Route::post('/goodsRecord/send', [AdminMobile\GoodsRecordController::class, 'sendGoods']); // 发货
    // Route::post('/goodsRecord/cancel', [AdminMobile\GoodsRecordController::class, 'cancelConversionGoods']); // 取消兑换

    /**
     * 活动标签
     */
    Route::get('/activityTag/filterList', [AdminMobile\ActivityTagController::class, 'filterList']); // 筛选列表
    Route::get('/activityTag/list', [AdminMobile\ActivityTagController::class, 'lists']); // 列表
    Route::get('/activityTag/detail', [AdminMobile\ActivityTagController::class, 'detail']); // 详情
    Route::post('/activityTag/add', [AdminMobile\ActivityTagController::class, 'add']); // 新增
    Route::post('/activityTag/change', [AdminMobile\ActivityTagController::class, 'change']); // 编辑
    Route::post('/activityTag/del', [AdminMobile\ActivityTagController::class, 'del']); // 删除


    /**
     * 问卷调查
     */
    // Route::get('/survey/list', [AdminMobile\SurveyController::class, 'lists']); // 列表
    // Route::get('/survey/detail', [AdminMobile\SurveyController::class, 'detail']); // 详情
    // Route::post('/survey/add', [AdminMobile\SurveyController::class, 'add']); // 新增
    // Route::post('/survey/change', [AdminMobile\SurveyController::class, 'change']); // 编辑
    // Route::post('/survey/del', [AdminMobile\SurveyController::class, 'del']); // 删除
    // Route::post('/survey/cancelAndRelease', [AdminMobile\SurveyController::class, 'cancelAndRelease']); // 问卷调查 撤销  与 发布


    /**
     * 在线荐购
     */
    // Route::get('/recommendBook/list', [AdminMobile\RecommendBookController::class, 'lists']); // 列表
    // Route::post('/recommendBook/agreeAndRefused', [AdminMobile\RecommendBookController::class, 'agreeAndRefused']); // 审核


    /**
     * 权限
     */
    // Route::get('/permission/list', [AdminMobile\PermissionController::class, 'permissionList']); // 权限列表
    // Route::get('/permission/tree', [AdminMobile\PermissionController::class, 'permissionTree']); // 权限树形列表
    // Route::get('/permission/detail', [AdminMobile\PermissionController::class, 'permissionDetail']); // 权限详情
    // Route::post('/permission/add', [AdminMobile\PermissionController::class, 'permissionInsert']); // 权限新增
    // Route::post('/permission/change', [AdminMobile\PermissionController::class, 'permissionChange']); // 权限编辑
    // Route::post('/permission/del', [AdminMobile\PermissionController::class, 'permissionDel']); // 权限删除
    // Route::post('/permission/orderChange', [AdminMobile\PermissionController::class, 'permissionOrderChange']); // 更改权限排序

    /**
     * 角色
     */
    Route::get('/role/list', [AdminMobile\RoleController::class, 'roleList']); // 角色列表
    Route::get('/role/filterList', [AdminMobile\RoleController::class, 'roleFilterList']); // 角色列表 （筛选使用）
    Route::get('/role/detail', [AdminMobile\RoleController::class, 'roleDetail']); // 角色详情
    Route::post('/role/add', [AdminMobile\RoleController::class, 'roleAdd']); // 角色新增
    Route::post('/role/change', [AdminMobile\RoleController::class, 'roleChange']); // 角色编辑
    Route::post('/role/del', [AdminMobile\RoleController::class, 'roleDel']); // 角色删除


    /**
     * banner管理
     */
    Route::get('/banner/list', [AdminMobile\BannerController::class, 'lists']); // 列表
    Route::get('/banner/detail', [AdminMobile\BannerController::class, 'detail']); // 详情
    Route::post('/banner/add', [AdminMobile\BannerController::class, 'add']); // 新增
    Route::post('/banner/change', [AdminMobile\BannerController::class, 'change']); // 编辑
    Route::post('/banner/del', [AdminMobile\BannerController::class, 'del']); // 删除
    Route::post('/banner/cancelAndRelease', [AdminMobile\BannerController::class, 'cancelAndRelease']); // 切换显示方式
    Route::post('/banner/sortChange', [AdminMobile\BannerController::class, 'sortChange']); // banner 图排序


    /**
     * 景点打卡类型
     */


    /**
     * 主页信息
     */
    Route::get('/index/index', [AdminMobile\IndexController::class, 'index']); //首页数据统计
    Route::get('/index/accessStatistics', [AdminMobile\IndexController::class, 'accessStatistics']); //公众号访问统计

    /**
     * 预约
     */
    Route::get('/reservation/getReservationApplyParam', [AdminMobile\ReservationController::class, 'getReservationApplyParam']); // 获取预约参数
    Route::get('/reservation/filterTagList', [AdminMobile\ReservationController::class, 'filterTagList']); //文化配送标签列表（预定义）
    Route::get('/reservation/list', [AdminMobile\ReservationController::class, 'lists']); //列表
    Route::get('/reservation/detail', [AdminMobile\ReservationController::class, 'detail']); //详情
    Route::post('/reservation/add', [AdminMobile\ReservationController::class, 'add']); //新增
    Route::post('/reservation/change', [AdminMobile\ReservationController::class, 'change']); //修改
    Route::post('/reservation/del', [AdminMobile\ReservationController::class, 'del']); //删除
    Route::post('/reservation/playAndCancel', [AdminMobile\ReservationController::class, 'playAndCancel']); //推荐与取消推荐


    Route::get('/reservationOffline/getMakeQuantum', [AdminMobile\ReservationOfflineController::class, 'getMakeQuantum']); //手动预约获取预约时间段
    Route::post('/reservationOffline/makeInfo', [AdminMobile\ReservationOfflineController::class, 'makeInfo']); //手动预约时间段


    Route::get('/reservation/getReservationQr', [AdminMobile\ReservationController::class, 'getReservationQr']); //出示用户领取、归还钥匙二维码(动态二维码) （node 7 适用）  有效期3分钟


    //特殊排班时间
    Route::get('/reservationSpecialSchedule/list', [AdminMobile\ReservationSpecialScheduleController::class, 'lists']); //特殊时间列表
    Route::post('/reservationSpecialSchedule/add', [AdminMobile\ReservationSpecialScheduleController::class, 'add']); //特殊时间新增
    Route::post('/reservationSpecialSchedule/change', [AdminMobile\ReservationSpecialScheduleController::class, 'change']); //特殊时间编辑
    Route::get('/reservationSpecialSchedule/detail', [AdminMobile\ReservationSpecialScheduleController::class, 'detail']); //特殊时间详情
    Route::post('/reservationSpecialSchedule/del', [AdminMobile\ReservationSpecialScheduleController::class, 'del']); //特殊时间删除


    Route::get('/reservationSeat/scheduleList', [AdminMobile\ReservationSeatController::class, 'scheduleList']); //获取排版信息
    Route::get('/reservationSeat/list', [AdminMobile\ReservationSeatController::class, 'lists']); //座位预览
    Route::get('/reservationSeat/geSeatListBytStatus', [AdminMobile\ReservationSeatController::class, 'geSeatListBytStatus']); //根据状态获取格子列表 (只能用于 node 为 7 的情况)
    Route::post('/reservationSeat/add', [AdminMobile\ReservationSeatController::class, 'add']); //座位新增
    Route::post('/reservationSeat/playAndCancel', [AdminMobile\ReservationSeatController::class, 'playAndCancel']); //座位发布与撤销
    Route::post('/reservationSeat/del', [AdminMobile\ReservationSeatController::class, 'del']); //座位删除
    Route::post('/reservationSeat/downloadQr', [AdminMobile\ReservationSeatController::class, 'downloadQr']); //下载座位二维码

    Route::get('/reservationApply/list', [AdminMobile\ReservationApplyController::class, 'lists']); //预约申请列表
    Route::get('/reservationApply/reservationApplyHistory', [AdminMobile\ReservationApplyController::class, 'reservationApplyHistory']); //获取用户对于某个预约的预约记录
    Route::post('/reservationApply/agreeAndRefused', [AdminMobile\ReservationApplyController::class, 'agreeAndRefused']); //审核通过 和 拒绝
    Route::post('/reservationApply/violateAndCancel', [AdminMobile\ReservationApplyController::class, 'violateAndCancel']); //预约违规 与 取消违规操作

    Route::post('/reservationApply/applyCancel', [AdminMobile\ReservationApplyController::class, 'applyCancel']); //管理员取消预约
    Route::post('/reservationApply/applySignIn', [AdminMobile\ReservationApplyController::class, 'applySignIn']); //管理员打卡签到\签退
    Route::post('/reservationApply/applySignOut', [AdminMobile\ReservationApplyController::class, 'applySignOut']); // 管理员操作用户打卡签退(弃用，改为签到同一个接口)
    Route::post('/reservationApply/collectionAndReturn', [AdminMobile\ReservationApplyController::class, 'collectionAndReturn']); //管理员操作领取和归还钥匙

    /**
     * 整个项目 数据分析
     */
    // Route::get('/dataAnalysis/appletStatistics', [AdminMobile\DataAnalysisController::class, 'appletStatistics']); // 小程序访问统计
    // Route::get('/dataAnalysis/activityStatistics', [AdminMobile\DataAnalysisController::class, 'activityStatistics']); // 活动参与人数统计
    // Route::get('/dataAnalysis/recomAndSurveyStatistics', [AdminMobile\DataAnalysisController::class, 'recomAndSurveyStatistics']); // 荐购、问卷统计

    // Route::get('/dataAnalysis/accessStatistics', [AdminMobile\DataAnalysisController::class, 'accessStatistics']); // 小程序访问总统计图

    // Route::get('/answerActivityToatlDataAnalysis/answerDataStatistics', [AdminMobile\AnswerActivityToatlDataAnalysisController::class, 'answerDataStatistics']); // 答题总数据统计
    // Route::get('/answerActivityToatlDataAnalysis/answerTimeStatistics', [AdminMobile\AnswerActivityToatlDataAnalysisController::class, 'answerTimeStatistics']); // 答题次数统计
    // Route::get('/answerActivityToatlDataAnalysis/answerDataRanking', [AdminMobile\AnswerActivityToatlDataAnalysisController::class, 'answerDataRanking']); // 最新活动排行榜

    // Route::get('/turnActivityToatlDataAnalysis/turnDataStatistics', [AdminMobile\TurnActivityToatlDataAnalysisController::class, 'turnDataStatistics']); // 翻奖总数据统计
    // Route::get('/turnActivityToatlDataAnalysis/turnTimeStatistics', [AdminMobile\TurnActivityToatlDataAnalysisController::class, 'turnTimeStatistics']); // 翻奖次数总统计
    // Route::get('/turnActivityToatlDataAnalysis/turnDataRanking', [AdminMobile\TurnActivityToatlDataAnalysisController::class, 'turnDataRanking']); // 最新活动排行榜


    // Route::get('/contestToatlDataAnalysis/contestDataStatistics', [AdminMobile\ContestToatlDataAnalysisController::class, 'contestDataStatistics']); // 在线投票总数据统计
    // Route::get('/contestToatlDataAnalysis/contestDataRanking', [AdminMobile\ContestToatlDataAnalysisController::class, 'contestDataRanking']); // 最新活动排行榜

    // Route::get('/pictureLiveToatlDataAnalysis/pictureLiveDataStatistics', [AdminMobile\PictureLiveToatlDataAnalysisController::class, 'pictureLiveDataStatistics']); // 图片直播总数据统计
    // Route::get('/pictureLiveToatlDataAnalysis/pictureLiveDataRanking', [AdminMobile\PictureLiveToatlDataAnalysisController::class, 'pictureLiveDataRanking']); // 最新活动排行榜

    // Route::get('/dataAnalysis/authRegisterStatistics', [AdminMobile\DataAnalysisController::class, 'authRegisterStatistics']); // 用户授权注册总统计图
    // Route::get('/dataAnalysis/otherStatistics', [AdminMobile\DataAnalysisController::class, 'otherStatistics']); // 其他数据统计
    // Route::get('/dataAnalysis/activityTypeStatistics', [AdminMobile\DataAnalysisController::class, 'activityTypeStatistics']); // 活动类型分布统计图
    // Route::get('/dataAnalysis/activityApplyStatistics', [AdminMobile\DataAnalysisController::class, 'activityApplyStatistics']); // 活动参与报名人数统计
    // Route::get('/dataAnalysis/competiteActivityStatistics', [AdminMobile\DataAnalysisController::class, 'competiteActivityStatistics']); // 征集活动作品大类型统计
    // Route::get('/dataAnalysis/competiteActivityPersonStatistics', [AdminMobile\DataAnalysisController::class, 'competiteActivityPersonStatistics']); // 征集活动投递人次
    // Route::get('/dataAnalysis/databaseWorksStatistics', [AdminMobile\DataAnalysisController::class, 'databaseWorksStatistics']); // 数据库作品统计
    // Route::get('/dataAnalysis/userStatistics', [AdminMobile\DataAnalysisController::class, 'userStatistics']); // 用户统计
    // Route::get('/dataAnalysis/singleUserStatistics', [AdminMobile\DataAnalysisController::class, 'singleUserStatistics']); // 单个用户统计





    // /**
    //  * 服务意向
    //  */
    // Route::get('/volunteerServiceIntention/filterList', [AdminMobile\VolunteerServiceIntentionController::class, 'filterList']); //志愿者服务意向类
    // Route::get('/volunteerServiceIntention/list', [AdminMobile\VolunteerServiceIntentionController::class, 'lists']); //列表
    // Route::get('/volunteerServiceIntention/detail', [AdminMobile\VolunteerServiceIntentionController::class, 'detail']); //详情
    // Route::post('/volunteerServiceIntention/add', [AdminMobile\VolunteerServiceIntentionController::class, 'add']); //新增
    // Route::post('/volunteerServiceIntention/change', [AdminMobile\VolunteerServiceIntentionController::class, 'change']); //修改
    // Route::post('/volunteerServiceIntention/del', [AdminMobile\VolunteerServiceIntentionController::class, 'del']); //删除

    // /**
    //  * 服务时间
    //  */
    // Route::get('/volunteerServiceTime/filterList', [AdminMobile\VolunteerServiceTimeController::class, 'filterList']); //志愿者服务时间类
    // Route::get('/volunteerServiceTime/list', [AdminMobile\VolunteerServiceTimeController::class, 'lists']); //列表
    // Route::get('/volunteerServiceTime/detail', [AdminMobile\VolunteerServiceTimeController::class, 'detail']); //详情
    // Route::post('/volunteerServiceTime/add', [AdminMobile\VolunteerServiceTimeController::class, 'add']); //新增
    // Route::post('/volunteerServiceTime/change', [AdminMobile\VolunteerServiceTimeController::class, 'change']); //修改
    // Route::post('/volunteerServiceTime/del', [AdminMobile\VolunteerServiceTimeController::class, 'del']); //删除

    // /**
    //  * 志愿者服务
    //  */
    // Route::get('/volunteer/filterList', [AdminMobile\VolunteerController::class, 'filterList']); // 服务岗位列表(用于下拉框选择)
    // Route::get('/volunteer/getPullFilterList', [AdminMobile\VolunteerController::class, 'getPullFilterList']); // 参加志愿者所需下拉选项(用于下拉框选择)
    // Route::get('/volunteer/volunteerApplyParam', [AdminMobile\VolunteerController::class, 'volunteerApplyParam']); // 志愿者报名参数(用于下拉框选择)
    // Route::get('/volunteer/list', [AdminMobile\VolunteerController::class, 'lists']); // 服务岗位列表
    // Route::get('/volunteer/detail', [AdminMobile\VolunteerController::class, 'detail']); // 服务岗位详情
    // Route::post('/volunteer/add', [AdminMobile\VolunteerController::class, 'add']); // 服务岗位添加
    // Route::post('/volunteer/change', [AdminMobile\VolunteerController::class, 'change']); // 服务岗位修改
    // Route::post('/volunteer/del', [AdminMobile\VolunteerController::class, 'del']); // 服务岗位删除
    // Route::post('/volunteer/playAndCancel', [AdminMobile\VolunteerController::class, 'playAndCancel']); // 发布与取消发布
    // Route::get('/volunteer/getVolunteerNotice', [AdminMobile\VolunteerController::class, 'getVolunteerNotice']); //  获取志愿者须知
    // Route::post('/volunteer/setVolunteerNotice', [AdminMobile\VolunteerController::class, 'setVolunteerNotice']); //  设置志愿者须知

    // /**
    //  * 志愿者服务报名信息
    //  */
    // Route::get('/volunteerApply/list', [AdminMobile\VolunteerApplyController::class, 'lists']); //  志愿者服务报名列表
    // Route::get('/volunteerApply/detail', [AdminMobile\VolunteerApplyController::class, 'detail']); //  志愿者服务报名详情
    // Route::post('/volunteerApply/check', [AdminMobile\VolunteerApplyController::class, 'check']); //  志愿者服务报名获取审核
    // Route::get('/volunteerApply/serviceTimeList', [AdminMobile\VolunteerApplyController::class, 'serviceTimeList']); //  获取服务时长列表
    // Route::post('/volunteerApply/serviceTimeChange', [AdminMobile\VolunteerApplyController::class, 'serviceTimeChange']); //  调整用户服务时长


    // /**
    //  * 总分馆简介（分馆简介）
    //  */
    // Route::get('/branchInfo/filterList', [AdminMobile\BranchInfoController::class, 'filterList']); // 总分馆(用于下拉框选择)
    // Route::get('/branchInfo/list', [AdminMobile\BranchInfoController::class, 'lists']); // 列表
    // Route::get('/branchInfo/detail', [AdminMobile\BranchInfoController::class, 'detail']); // 详情
    // Route::post('/branchInfo/add', [AdminMobile\BranchInfoController::class, 'add']); // 新增
    // Route::post('/branchInfo/change', [AdminMobile\BranchInfoController::class, 'change']); // 编辑
    // Route::post('/branchInfo/del', [AdminMobile\BranchInfoController::class, 'del']); // 删除

    // /**
    //  * 分馆用户账号管理
    //  */
    // Route::get('/branchAccount/list', [AdminMobile\BranchAccountController::class, 'lists']); // 列表
    // //Route::get('/branchAccount/detail', [AdminMobile\BranchAccountController::class, 'detail']); // 详情
    // Route::post('/branchAccount/add', [AdminMobile\BranchAccountController::class, 'add']); // 新增
    // Route::post('/branchAccount/change', [AdminMobile\BranchAccountController::class, 'change']); // 编辑
    // Route::post('/branchAccount/del', [AdminMobile\BranchAccountController::class, 'del']); // 删除
    // Route::post('/branchAccount/changePwd', [AdminMobile\BranchAccountController::class, 'changePwd']); //  用户管理账号修改密码

    // /**
    //  * 欠费缴纳
    //  */
    // Route::get('/oweProcess/list', [AdminMobile\OweProcessController::class, 'lists']); // 列表
    // Route::get('/oweProcess/subscriptionRatioSet', [AdminMobile\OweProcessController::class, 'subscriptionRatioSet']); // 欠费缴纳积分抵扣设置


    // /**
    //  * 在线办证配置
    //  */
    // Route::get('/onlineRegistrationConfig/getOnlineRegistrationConfigParam', [AdminMobile\OnlineRegistrationConfigController::class, 'getOnlineRegistrationConfigParam']); // 获取在线办证参数（获取已配置的）
    // Route::get('/onlineRegistrationConfig/setOnlineRegistrationConfigParam', [AdminMobile\OnlineRegistrationConfigController::class, 'setOnlineRegistrationConfigParam']); // 配置在线办证参数

    // /**
    //  * 在线办证
    //  */
    // Route::get('/onlineRegistration/getDynamicParam', [AdminMobile\OnlineRegistrationController::class, 'getDynamicParam']); // 获取动态参数
    // Route::post('/onlineRegistration/sendVerify', [AdminMobile\OnlineRegistrationController::class, 'sendVerify']); // 发送验证码
    // Route::get('/onlineRegistration/getCertificateType', [AdminMobile\OnlineRegistrationController::class, 'getCertificateType']); // 获取证件类型
    // Route::get('/onlineRegistration/getCardType', [AdminMobile\OnlineRegistrationController::class, 'getCardType']); // 获取在线办证类型
    // Route::get('/onlineRegistration/getFrontCardType', [AdminMobile\OnlineRegistrationController::class, 'getFrontCardType']); // 获取在前台线办证类型
    // Route::post('/onlineRegistration/registration', [AdminMobile\OnlineRegistrationController::class, 'registration']); // 读者证-办证接口
    // Route::get('/onlineRegistration/sponsorRegistrationList', [AdminMobile\OnlineRegistrationController::class, 'sponsorRegistrationList']); //（发起方）办证统计列表 （前台在线办证列表）
    // Route::get('/onlineRegistration/sponsorRegistrationStatistics', [AdminMobile\OnlineRegistrationController::class, 'sponsorRegistrationStatistics']); //（发起方）办证统计数据  （前台在线办证列表）
    // Route::get('/onlineRegistration/confirmPostageList', [AdminMobile\OnlineRegistrationController::class, 'confirmPostageList']); //（确认方）办证统计列表
    // Route::get('/onlineRegistration/confirmPostageStatistics', [AdminMobile\OnlineRegistrationController::class, 'confirmPostageStatistics']); //（确认方） 邮费统计 金额及书籍本数 统计
    // Route::post('/onlineRegistration/sponsorSettleState', [AdminMobile\OnlineRegistrationController::class, 'sponsorSettleState']); //（发起方）标记办证状态为结算中
    // Route::post('/onlineRegistration/confirmSettleState', [AdminMobile\OnlineRegistrationController::class, 'confirmSettleState']); //（确认方） 标记邮费状态为已结算
    // Route::post('/onlineRegistration/toReceive', [AdminMobile\OnlineRegistrationController::class, 'toReceive']); // 领取读者证
    // Route::get('/onlineRegistration/getOnlineParam', [AdminMobile\OnlineRegistrationController::class, 'getOnlineParam']); // 获取办证参数
    // Route::post('/onlineRegistration/refund', [AdminMobile\OnlineRegistrationController::class, 'refund']); //  写入图书馆系统接口失败，办证失败，进行退款操作  （用于自动退款失败后手动退款）



    // /**
    //  * 服务提醒通知
    //  */
    // Route::get('/serviceNotice/list', [AdminMobile\ServiceNoticeController::class, 'lists']); // 列表
    // Route::get('/serviceNotice/detail', [AdminMobile\ServiceNoticeController::class, 'detail']); // 详情
    // Route::post('/serviceNotice/add', [AdminMobile\ServiceNoticeController::class, 'add']); // 新增
    // Route::post('/serviceNotice/change', [AdminMobile\ServiceNoticeController::class, 'change']); // 修改
    // Route::post('/serviceNotice/send', [AdminMobile\ServiceNoticeController::class, 'send']); // 发送
    // Route::post('/serviceNotice/del', [AdminMobile\ServiceNoticeController::class, 'del']); // 删除




    ######################################################图书到家start#############################################################

    /**
     * （图书到家）书店管理
     */
    // Route::get('/shop/filterList', [AdminMobile\ShopController::class, 'filterList']); //  书店筛选列表
    // Route::get('/shop/list', [AdminMobile\ShopController::class, 'lists']); // 列表
    // Route::get('/shop/detail', [AdminMobile\ShopController::class, 'detail']); // 详情
    // Route::post('/shop/add', [AdminMobile\ShopController::class, 'add']); // 新增
    // Route::post('/shop/change', [AdminMobile\ShopController::class, 'change']); // 编辑
    // Route::post('/shop/del', [AdminMobile\ShopController::class, 'del']); // 删除
    // /**
    //  * （图书到家）书店书籍类型管理
    //  */
    // Route::get('/newBookRecomType/filterList', [AdminMobile\NewBookRecomTypeController::class, 'filterList']); //  筛选列表
    // Route::get('/newBookRecomType/list', [AdminMobile\NewBookRecomTypeController::class, 'lists']); // 列表
    // Route::get('/newBookRecomType/detail', [AdminMobile\NewBookRecomTypeController::class, 'detail']); // 详情
    // Route::post('/newBookRecomType/add', [AdminMobile\NewBookRecomTypeController::class, 'add']); // 新增
    // Route::post('/newBookRecomType/change', [AdminMobile\NewBookRecomTypeController::class, 'change']); // 编辑
    // Route::post('/newBookRecomType/del', [AdminMobile\NewBookRecomTypeController::class, 'del']); // 删除

    // /**
    //  * （图书到家）书店书籍管理
    //  */
    // Route::get('/newBookRecom/list', [AdminMobile\NewBookRecomController::class, 'lists']); // 列表
    // Route::get('/newBookRecom/detail', [AdminMobile\NewBookRecomController::class, 'detail']); // 详情
    // Route::post('/newBookRecom/add', [AdminMobile\NewBookRecomController::class, 'add']); // 新增
    // Route::post('/newBookRecom/change', [AdminMobile\NewBookRecomController::class, 'change']); // 编辑
    // Route::post('/newBookRecom/del', [AdminMobile\NewBookRecomController::class, 'del']); // 删除
    // Route::post('/newBookRecom/cancelAndRelease', [AdminMobile\NewBookRecomController::class, 'cancelAndRelease']); //  推荐与取消推荐
    // Route::post('/newBookRecom/upAndDown', [AdminMobile\NewBookRecomController::class, 'upAndDown']); //  上架和下架


    // /**
    //  * （图书到家）图书馆书籍类型管理
    //  */
    // Route::get('/libBookRecomType/filterList', [AdminMobile\LibBookRecomTypeController::class, 'filterList']); //  筛选列表
    // Route::get('/libBookRecomType/list', [AdminMobile\LibBookRecomTypeController::class, 'lists']); // 列表
    // Route::get('/libBookRecomType/detail', [AdminMobile\LibBookRecomTypeController::class, 'detail']); // 详情
    // Route::post('/libBookRecomType/add', [AdminMobile\LibBookRecomTypeController::class, 'add']); // 新增
    // Route::post('/libBookRecomType/change', [AdminMobile\LibBookRecomTypeController::class, 'change']); // 编辑
    // Route::post('/libBookRecomType/del', [AdminMobile\LibBookRecomTypeController::class, 'del']); // 删除

    // /**
    //  * （图书到家）图书馆书籍管理
    //  */
    // Route::get('/libBookRecom/list', [AdminMobile\LibBookRecomController::class, 'lists']); // 列表
    // Route::get('/libBookRecom/detail', [AdminMobile\LibBookRecomController::class, 'detail']); // 详情
    // Route::post('/libBookRecom/add', [AdminMobile\LibBookRecomController::class, 'add']); // 新增
    // Route::post('/libBookRecom/change', [AdminMobile\LibBookRecomController::class, 'change']); // 编辑
    // Route::post('/libBookRecom/del', [AdminMobile\LibBookRecomController::class, 'del']); // 删除
    // Route::post('/libBookRecom/cancelAndRelease', [AdminMobile\LibBookRecomController::class, 'cancelAndRelease']); //  推荐与取消推荐

    // /**
    //  * 图书到家 馆藏书借阅
    //  */
    // Route::get('/bookHomeLibBook/orderList', [AdminMobile\BookHomeLibBookController::class, 'orderList']); // 申请列表、发货列表、所有订单列表 （馆藏书）
    // Route::get('/bookHomeLibBook/getOrderNumber', [AdminMobile\BookHomeLibBookController::class, 'getOrderNumber']); // 获取未处理订单 和未发货订单数量 (馆藏书)
    // Route::get('/bookHomeLibBook/orderDetail', [AdminMobile\BookHomeLibBookController::class, 'orderDetail']); // 获取馆藏书订单详情
    // Route::post('/bookHomeLibBook/printExpressSheetLib', [AdminMobile\BookHomeLibBookController::class, 'printExpressSheetLib']); // 打印电子面单 （馆藏书）
    // Route::get('/bookHomeLibBook/purchaseDetail', [AdminMobile\BookHomeLibBookController::class, 'purchaseDetail']); // 获取采购清单详情
    // Route::get('/bookHomeLibBook/getReaderBorrowList', [AdminMobile\BookHomeLibBookController::class, 'getReaderBorrowList']); //线下书籍归还列表（根据读者证号查询数据）
    // Route::get('/bookHomeLibBook/getBookBorrow', [AdminMobile\BookHomeLibBookController::class, 'getBookBorrow']); //线下书籍归还查询（根据条形码查询数据）
    // Route::post('/bookHomeLibBook/offlineReturn', [AdminMobile\BookHomeLibBookController::class, 'offlineReturn']); //线下书籍确认归还 (新书的馆藏书的归还)
    // Route::post('/bookHomeLibBook/offlineReturnDirect', [AdminMobile\BookHomeLibBookController::class, 'offlineReturnDirect']); //线下书籍直接确认归还（涉及只有条形码，直接归还数据）
    // Route::post('/bookHomeLibBook/onlineReturn', [AdminMobile\BookHomeLibBookController::class, 'onlineReturn']); //线上书籍归还
    // Route::get('/bookHomeLibBook/postalReturnList', [AdminMobile\BookHomeLibBookController::class, 'postalReturnList']); //邮递还书列表
    // Route::post('/bookHomeLibBook/disposeOrderApply', [AdminMobile\BookHomeLibBookController::class, 'disposeOrderApply']); //管理员同意 和拒绝用户的 借阅申请（馆藏书）
    // Route::post('/bookHomeLibBook/disposeOrderDeliver', [AdminMobile\BookHomeLibBookController::class, 'disposeOrderDeliver']); //确认发货或无法发货
    // Route::get('/bookHomeLibBook/bookReturnState', [AdminMobile\BookHomeLibBookController::class, 'bookReturnState']); //获取书籍归还状态，修改本地书籍归还账号（外部 定时任务执行，可根据需求执行）


    // /**
    //  * 新书采购清单 (图书馆端)
    //  */
    // Route::get('/bookHomeNewBookLib/purchaseShopList', [AdminMobile\BookHomeNewBookLibController::class, 'purchaseShopList']); // 所有(新书)采购清单 (图书馆端查看)
    // Route::get('/bookHomeNewBookLib/purchaseLibList', [AdminMobile\BookHomeNewBookLibController::class, 'purchaseLibList']); // 所有(馆藏书)采购清单 (图书馆端查看)(只查看线上的)
    // Route::get('/bookHomeNewBookLib/disposeSuccess', [AdminMobile\BookHomeNewBookLibController::class, 'disposeSuccess']); //  新书 数据 写入图书馆系统失败后，手动处理
    // Route::get('/bookHomeNewBookLib/getPurchaseNumber', [AdminMobile\BookHomeNewBookLibController::class, 'getPurchaseNumber']); //  计算采购数量及结算状态数量
    // Route::post('/bookHomeNewBookLib/purchaseStateModify', [AdminMobile\BookHomeNewBookLibController::class, 'purchaseStateModify']); //  修改采购清单状态为 结算中
    // Route::get('/bookHomeNewBookLib/purchaseDetail', [AdminMobile\BookHomeNewBookLibController::class, 'purchaseDetail']); //  获取采购清单详情
    // Route::get('/bookHomeNewBookLib/refundList', [AdminMobile\BookHomeNewBookLibController::class, 'refundList']); //  申请退款列表
    // Route::post('/bookHomeNewBookLib/refund', [AdminMobile\BookHomeNewBookLibController::class, 'refund']); //  审核拒绝或无法发货，进行退款操作


    // /**
    //  * 图书到家 新书采购清单（书店端）
    //  */
    // Route::get('/bookHomeNewBookShop/orderList', [AdminMobile\BookHomeNewBookShopController::class, 'orderList']); // 申请列表、发货列表、所有订单列表 （新书）
    // Route::get('/bookHomeNewBookShop/getOrderNumber', [AdminMobile\BookHomeNewBookShopController::class, 'getOrderNumber']); // 获取未处理订单 和未发货订单数量 (新书)
    // Route::get('/bookHomeNewBookShop/orderDetail', [AdminMobile\BookHomeNewBookShopController::class, 'orderDetail']); // 获取订单详情
    // Route::get('/bookHomeNewBookShop/disposeOrderApply', [AdminMobile\BookHomeNewBookShopController::class, 'disposeOrderApply']); // 管理员同意 和拒绝用户的 采购申请
    // Route::post('/bookHomeNewBookShop/disposeOrderDeliver', [AdminMobile\BookHomeNewBookShopController::class, 'disposeOrderDeliver']); // 确认发货或无法发货
    // Route::post('/bookHomeNewBookShop/againGetTrackingNumberShop', [AdminMobile\BookHomeNewBookShopController::class, 'againGetTrackingNumberShop']); // 针对邮递发货 获取运单号失败后，补发获取快递单号（新书）
    // Route::get('/bookHomeNewBookShop/printExpressSheetShop', [AdminMobile\BookHomeNewBookShopController::class, 'printExpressSheetShop']); // 打印电子面单 （新书）
    // Route::get('/bookHomeNewBookShop/purchaseList', [AdminMobile\BookHomeNewBookShopController::class, 'purchaseList']); // 书店采购清单(书店查看)
    // Route::get('/bookHomeNewBookShop/getPurchaseNumber', [AdminMobile\BookHomeNewBookShopController::class, 'getPurchaseNumber']); // 计算采购数量及结算状态数量
    // Route::post('/bookHomeNewBookShop/purchaseStateModify', [AdminMobile\BookHomeNewBookShopController::class, 'purchaseStateModify']); // 修改采购清单状态为 已结算
    // Route::get('/bookHomeNewBookShop/purchaseAllList', [AdminMobile\BookHomeNewBookShopController::class, 'purchaseAllList']); // 所有(新书)采购清单 (书店端查看)


    // /**
    //  * 图书到家线下荐购
    //  */
    // Route::get('/bookHomeOfflineRecom/getReaderInfo', [AdminMobile\BookHomeOfflineRecomController::class, 'getReaderInfo']); // 根据读者证号获取用户基本信息
    // Route::get('/bookHomeOfflineRecom/getBookInfo', [AdminMobile\BookHomeOfflineRecomController::class, 'getBookInfo']); // 根据数据ISBN号，获取书籍信息，并判断书籍是否符合借阅要求
    // Route::post('/bookHomeOfflineRecom/affirmBorrow', [AdminMobile\BookHomeOfflineRecomController::class, 'affirmBorrow']); // 确认线下借阅
    // Route::get('/bookHomeOfflineRecom/offlineListShop', [AdminMobile\BookHomeOfflineRecomController::class, 'offlineListShop']); // 线下取书记录 书店端 查看
    // Route::get('/bookHomeOfflineRecom/offlineListLib', [AdminMobile\BookHomeOfflineRecomController::class, 'offlineListLib']); // 线下取书记录(图书馆端) 查看

    // /**
    //  * 邮费统计  书店为确认方，图书馆为 发起方
    //  */
    // Route::get('/bookHomePostage/list', [AdminMobile\BookHomePostageController::class, 'lists']); // （发起方、确认方） 邮费统计列表
    // Route::get('/bookHomePostage/statistics', [AdminMobile\BookHomePostageController::class, 'statistics']); // (发起方、确认方)邮费统计 金额及书籍本数 统计
    // Route::post('/bookHomePostage/sponsorSettleState', [AdminMobile\BookHomePostageController::class, 'sponsorSettleState']); //  (发起方)标记邮费状态为结算中
    // Route::post('/bookHomePostage/affirmSettleState', [AdminMobile\BookHomePostageController::class, 'affirmSettleState']); // （确认方）标记邮费状态为已结算

    // /**
    //  * 图书到家系统设置
    //  */
    // Route::post('/bookHomePurchaseSet/purchaseSet', [AdminMobile\BookHomePurchaseSetController::class, 'purchaseSet']); // 图书经费+读者采购设置
    // Route::post('/bookHomePurchaseSet/postageSet', [AdminMobile\BookHomePurchaseSetController::class, 'postageSet']); // 邮费设置 或修改
    // Route::get('/bookHomePurchaseSet/purchaseSetRecord', [AdminMobile\BookHomePurchaseSetController::class, 'purchaseSetRecord']); //  设置记录
    // Route::get('/bookHomePurchaseSet/purchaseBudgetList', [AdminMobile\BookHomePurchaseSetController::class, 'purchaseBudgetList']); // 采购预算、邮费 增加的历史表
    // Route::post('/bookHomePurchaseSet/libDeliverAddress', [AdminMobile\BookHomePurchaseSetController::class, 'libDeliverAddress']); // 图书馆发货地址设置
    // Route::post('/bookHomePurchaseSet/shopDeliverAddress', [AdminMobile\BookHomePurchaseSetController::class, 'shopDeliverAddress']); // 书店发货地址设置

    // /**
    //  * 邮递、现场归还地址管理
    //  */
    // Route::get('/bookHomeReturnAddress/list', [AdminMobile\BookHomeReturnAddressController::class, 'lists']); // 列表
    // Route::get('/bookHomeReturnAddress/detail', [AdminMobile\BookHomeReturnAddressController::class, 'detail']); // 详情
    // Route::post('/bookHomeReturnAddress/add', [AdminMobile\BookHomeReturnAddressController::class, 'add']); // 新增
    // Route::post('/bookHomeReturnAddress/change', [AdminMobile\BookHomeReturnAddressController::class, 'change']); // 编辑
    // Route::post('/bookHomeReturnAddress/del', [AdminMobile\BookHomeReturnAddressController::class, 'del']); // 删除
    // Route::get('/bookHomeReturnAddress/getAddressExplain', [AdminMobile\BookHomeReturnAddressController::class, 'getAddressExplain']); //  获取图书到家归还地址说明
    // Route::post('/bookHomeReturnAddress/setAddressExplain', [AdminMobile\BookHomeReturnAddressController::class, 'setAddressExplain']); //  设置图书到家归还地址说明

    // /**
    //  * 扫码借配置
    //  */
    // Route::get('/codePlace/filterList', [AdminMobile\CodePlaceController::class, 'filterList']); // 筛选列表
    // Route::get('/codePlace/list', [AdminMobile\CodePlaceController::class, 'lists']); // 列表
    // Route::get('/codePlace/detail', [AdminMobile\CodePlaceController::class, 'detail']); // 详情
    // Route::post('/codePlace/add', [AdminMobile\CodePlaceController::class, 'add']); // 新增
    // Route::post('/codePlace/change', [AdminMobile\CodePlaceController::class, 'change']); // 编辑
    // Route::post('/codePlace/del', [AdminMobile\CodePlaceController::class, 'del']); // 删除
    // Route::post('/codePlace/playAndCancel', [AdminMobile\CodePlaceController::class, 'playAndCancel']); //  发布与取消发布

    // /**
    //  * 扫码借记录
    //  */
    // Route::get('/codeOnBorrowLog/borrowList', [AdminMobile\CodeOnBorrowLogController::class, 'borrowList']); // 借阅列表
    // Route::get('/codeOnBorrowLog/returnList', [AdminMobile\CodeOnBorrowLogController::class, 'returnList']); // 归还列表


    ######################################################抽奖活动end#############################################################

    /**
     * 导出功能
     */
    // Route::get('/newBookRecommendExport/index', [Controllers\Export\NewBookRecommendExport::class, 'index']); // 新书推荐列表导出
    // Route::get('/newBookRecommendExport/template', [Controllers\Export\NewBookRecommendExport::class, 'template']); // 新书推荐模板导出
    Route::get('/activityApplyExport/activityApplyList', [Controllers\Export\ActivityApplyExport::class, 'activityApplyList']); // 活动申请列表导出
    Route::get('/activityApplyExport/activityApplyUser', [Controllers\Export\ActivityApplyExport::class, 'activityApplyUser']); // 活动人员列表导出
    Route::get('/activityApplyExport/activityApplyListAll', [Controllers\Export\ActivityApplyExport::class, 'activityApplyListAll']); // 批量活动申请列表导出
    Route::get('/activityApplyExport/activityApplyUserAll', [Controllers\Export\ActivityApplyExport::class, 'activityApplyUserAll']); // 批量活动人员列表导出
    // Route::get('/recommendBookExport/index', [Controllers\Export\RecommendBookExport::class, 'index']); // 用户荐购列表（在线荐购）导出
    // Route::get('/surveyExport/index', [Controllers\Export\SurveyExport::class, 'index']); // 问卷调查答案导出


    // Route::get('/bookHomeExport/orderList', [Controllers\Export\BookHomeExport::class, 'orderList']); // 申请列表、发货列表、所有订单列表 导出(新书) 导出
    // Route::get('/bookHomeExport/orderLibraryList', [Controllers\Export\BookHomeExport::class, 'orderLibraryList']); // 申请列表、发货列表、所有订单列表 （馆藏书）导出
    // Route::get('/bookHomeExport/offlineListShop', [Controllers\Export\BookHomeExport::class, 'offlineListShop']); // 线下取书记录 (书店端) 导出
    // Route::get('/bookHomeExport/offlineListLib', [Controllers\Export\BookHomeExport::class, 'offlineListLib']); // 线下取书记录(图书馆端) 查看   导出
    // Route::get('/bookHomeExport/libPurchaseLibList', [Controllers\Export\BookHomeExport::class, 'libPurchaseLibList']);   // 所有(馆藏书)采购清单 (图书馆端查看)(只查看线上的)   导出
    // Route::get('/bookHomeExport/libPurchaseShopList', [Controllers\Export\BookHomeExport::class, 'libPurchaseShopList']); // 新书线上取书记录列表 (图书馆端查看)(只查看线上的)   导出
    // Route::get('/bookHomeExport/shopPurchaseAllList', [Controllers\Export\BookHomeExport::class, 'shopPurchaseAllList']); // 新书线上取书记录列表 (书店端查看)(只查看线上的)   导出
    // Route::get('/bookHomeExport/purchaseLibList', [Controllers\Export\BookHomeExport::class, 'purchaseLibList']);         // 新书结算清单列表（图书馆端查看)(只查看线上的)   导出
    // Route::get('/bookHomeExport/purchaseShopList', [Controllers\Export\BookHomeExport::class, 'purchaseShopList']);       // 新书结算清单列表（书店端查看)(只查看线上的)   导出
    // Route::get('/postalReturnExport/index', [Controllers\Export\PostalReturnExport::class, 'index']); //  邮递还书列表 导出
    // Route::get('/reservationApplyExport/index', [Controllers\Export\ReservationApplyExport::class, 'index']); //  预约申请、预约人数列表导出
    // Route::get('/studyRoomReservationApplyExport/index', [Controllers\Export\StudyRoomReservationApplyExport::class, 'index']); //  空间预约申请、预约人数列表导出
    // Route::get('/goodsExchangeExport/index', [Controllers\Export\GoodsExchangeExport::class, 'index']); //  商品兑换记录 导出
    // Route::get('/templateExport/newBookRecommend', [Controllers\Export\TemplateExport::class, 'newBookRecommend']); //  新书推荐模板导出
    // Route::get('/templateExport/shopNewBookRecommend', [Controllers\Export\TemplateExport::class, 'shopNewBookRecommend']); //  书店新书模板导出
    // Route::get('/templateExport/libBookRecommend', [Controllers\Export\TemplateExport::class, 'libBookRecommend']); //  馆藏书模板下载导出


    // Route::get('/volunteerExport/volunteerApplyList', [Controllers\Export\VolunteerExport::class, 'volunteerApplyList']); // 志愿者申请列表导出
    // Route::get('/volunteerExport/volunteerList', [Controllers\Export\VolunteerExport::class, 'volunteerList']); // 志愿者列表导出



    // Route::get('/codeBorrowLogExport/borrowList', [Controllers\Export\CodeBorrowLogExport::class, 'borrowList']); // 扫码借-借阅列表 导出
    // Route::get('/codeBorrowLogExport/returnList', [Controllers\Export\CodeBorrowLogExport::class, 'returnList']); // 扫码借-归还列表 导出


    // /**
    //  * 导入功能
    //  */
    // Route::post('/newBookRecommendImport/index', [Controllers\Import\NewBookRecommendImport::class, 'index']); // 新书推荐管理导入
    // Route::post('/libBookRecomImport/index', [Controllers\Import\LibBookRecomImport::class, 'index']); // 馆藏书批量导入
    // Route::post('/shopBookImport/index', [Controllers\Import\ShopBookImport::class, 'index']); // 书店新书推荐管理导入


    ######################################################抽奖活动start#############################################################

    /**
     * 答题活动基础单位管理
     */
    // Route::get('/answerActivityBaseUnit/getNatureName', [AdminMobile\AnswerActivityBaseUnitController::class, 'getNatureName']); //获取单位性质
    // Route::get('/answerActivityBaseUnit/filterList', [AdminMobile\AnswerActivityBaseUnitController::class, 'filterList']); //类型(用于下拉框选择)
    // Route::get('/answerActivityBaseUnit/list', [AdminMobile\AnswerActivityBaseUnitController::class, 'lists']); //  列表
    // Route::get('/answerActivityBaseUnit/detail', [AdminMobile\AnswerActivityBaseUnitController::class, 'detail']); // 详情
    // Route::post('/answerActivityBaseUnit/add', [AdminMobile\AnswerActivityBaseUnitController::class, 'add']); // 新增
    // Route::post('/answerActivityBaseUnit/change', [AdminMobile\AnswerActivityBaseUnitController::class, 'change']); // 编辑
    // Route::post('/answerActivityBaseUnit/del', [AdminMobile\AnswerActivityBaseUnitController::class, 'del']); // 删除
    // Route::post('/answerActivityBaseUnit/orderChange', [AdminMobile\AnswerActivityBaseUnitController::class, 'orderChange']); // 修改区域排序


    // /**
    //  * 答题活动
    //  */
    // Route::get('/answerActivity/getNodeName', [AdminMobile\AnswerActivityController::class, 'getNodeName']); // 获取活动类型
    // Route::get('/answerActivity/getPatternName', [AdminMobile\AnswerActivityController::class, 'getPatternName']); //  获取答题模式
    // Route::get('/answerActivity/getPrizeFormName', [AdminMobile\AnswerActivityController::class, 'getPrizeFormName']); // 获取获奖情况
    // Route::get('/answerActivity/list', [AdminMobile\AnswerActivityController::class, 'lists']); // 列表
    // Route::get('/answerActivity/detail', [AdminMobile\AnswerActivityController::class, 'detail']); // 详情
    // Route::post('/answerActivity/add', [AdminMobile\AnswerActivityController::class, 'add']); // 新增
    // Route::post('/answerActivity/change', [AdminMobile\AnswerActivityController::class, 'change']); // 编辑
    // Route::post('/answerActivity/del', [AdminMobile\AnswerActivityController::class, 'del']); // 删除
    // Route::post('/answerActivity/cancelAndRelease', [AdminMobile\AnswerActivityController::class, 'cancelAndRelease']); // 撤销 和发布
    // Route::post('/answerActivity/invitCodeChange', [AdminMobile\AnswerActivityController::class, 'invitCodeChange']); // 邀请码修改
    // Route::post('/answerActivity/setAnswerRule', [AdminMobile\AnswerActivityController::class, 'set_answer_rule']); // 设置答题总规则

    // Route::post('/answerActivity/resetData', [AdminMobile\AnswerActivityController::class, 'resetData']); // 重置数据
    // Route::post('/answerActivity/copyData', [AdminMobile\AnswerActivityController::class, 'copyData']); // 复制活动


    // /**
    //  * 答题活动区域管理
    //  */
    // Route::get('/answerActivityArea/filterList', [AdminMobile\AnswerActivityAreaController::class, 'filterList']); //类型(用于下拉框选择)
    // Route::get('/answerActivityArea/list', [AdminMobile\AnswerActivityAreaController::class, 'lists']); //  列表
    // Route::get('/answerActivityArea/detail', [AdminMobile\AnswerActivityAreaController::class, 'detail']); // 详情
    // Route::post('/answerActivityArea/add', [AdminMobile\AnswerActivityAreaController::class, 'add']); // 新增
    // Route::post('/answerActivityArea/change', [AdminMobile\AnswerActivityAreaController::class, 'change']); // 编辑
    // Route::post('/answerActivityArea/del', [AdminMobile\AnswerActivityAreaController::class, 'del']); // 删除
    // Route::post('/answerActivityArea/orderChange', [AdminMobile\AnswerActivityAreaController::class, 'orderChange']); // 修改区域排序


    // /**
    //  * 答题活动单位管理
    //  */
    // Route::get('/answerActivityUnit/getNatureName', [AdminMobile\AnswerActivityUnitController::class, 'getNatureName']); //获取单位性质
    // Route::get('/answerActivityUnit/filterList', [AdminMobile\AnswerActivityUnitController::class, 'filterList']); //类型(用于下拉框选择)
    // Route::get('/answerActivityUnit/list', [AdminMobile\AnswerActivityUnitController::class, 'lists']); //  列表
    // Route::get('/answerActivityUnit/detail', [AdminMobile\AnswerActivityUnitController::class, 'detail']); // 详情
    // Route::post('/answerActivityUnit/add', [AdminMobile\AnswerActivityUnitController::class, 'add']); // 新增
    // Route::post('/answerActivityUnit/change', [AdminMobile\AnswerActivityUnitController::class, 'change']); // 编辑
    // Route::post('/answerActivityUnit/del', [AdminMobile\AnswerActivityUnitController::class, 'del']); // 删除
    // Route::post('/answerActivityUnit/orderChange', [AdminMobile\AnswerActivityUnitController::class, 'orderChange']); // 修改区域排序
    // Route::post('/answerActivityUnit/cancelAndDefault', [AdminMobile\AnswerActivityUnitController::class, 'cancelAndDefault']); // 默认与取消默认
    // Route::get('/answerActivityUnit/downloadUnitQr', [AdminMobile\AnswerActivityUnitController::class, 'downloadUnitQr']); // 下载所有单位二维码
    // Route::post('/answerActivityUnit/selectBaseUnit', [AdminMobile\AnswerActivityUnitController::class, 'selectBaseUnit']); // 从基础数据库中选择单位到活动里面


    // /**
    //  * 答题活动限制地址
    //  */
    // Route::get('/answerActivityLimitAddres/list', [AdminMobile\AnswerActivityLimitAddressController::class, 'lists']); //  列表
    // Route::get('/answerActivityLimitAddres/detail', [AdminMobile\AnswerActivityLimitAddressController::class, 'detail']); // 详情
    // Route::post('/answerActivityLimitAddres/add', [AdminMobile\AnswerActivityLimitAddressController::class, 'add']); // 新增
    // Route::post('/answerActivityLimitAddres/change', [AdminMobile\AnswerActivityLimitAddressController::class, 'change']); // 编辑
    // Route::post('/answerActivityLimitAddres/del', [AdminMobile\AnswerActivityLimitAddressController::class, 'del']); // 删除

    // /**
    //  * 答题活动题库管理
    //  */
    // Route::get('/answerActivityProblem/list', [AdminMobile\AnswerActivityProblemController::class, 'lists']); //  列表
    // Route::get('/answerActivityProblem/detail', [AdminMobile\AnswerActivityProblemController::class, 'detail']); // 详情
    // Route::post('/answerActivityProblem/add', [AdminMobile\AnswerActivityProblemController::class, 'add']); // 新增
    // Route::post('/answerActivityProblem/change', [AdminMobile\AnswerActivityProblemController::class, 'change']); // 编辑
    // Route::post('/answerActivityProblem/del', [AdminMobile\AnswerActivityProblemController::class, 'del']); // 删除
    // Route::post('/answerActivityProblem/delMany', [AdminMobile\AnswerActivityProblemController::class, 'delMany']); // 删除多个问题
    // Route::post('/answerActivityProblem/disablingAndEnabling', [AdminMobile\AnswerActivityProblemController::class, 'disablingAndEnabling']); // 禁用与启用
    // Route::post('/answerActivityProblem/analysisShowAndHidden', [AdminMobile\AnswerActivityProblemController::class, 'analysisShowAndHidden']); // 显示与隐藏解析

    // /**
    //  * 答题活动礼物管理
    //  */
    // Route::get('/answerActivityGift/list', [AdminMobile\AnswerActivityGiftController::class, 'lists']); //  列表
    // Route::get('/answerActivityGift/detail', [AdminMobile\AnswerActivityGiftController::class, 'detail']); // 详情
    // Route::post('/answerActivityGift/add', [AdminMobile\AnswerActivityGiftController::class, 'add']); // 新增
    // Route::post('/answerActivityGift/change', [AdminMobile\AnswerActivityGiftController::class, 'change']); // 编辑
    // Route::post('/answerActivityGift/del', [AdminMobile\AnswerActivityGiftController::class, 'del']); // 删除

    // /**
    //  * 答题活动礼物公示管理
    //  */
    // Route::get('/answerActivityGiftPublic/list', [AdminMobile\AnswerActivityGiftPublicController::class, 'lists']); //  列表
    // Route::get('/answerActivityGiftPublic/detail', [AdminMobile\AnswerActivityGiftPublicController::class, 'detail']); // 详情
    // Route::post('/answerActivityGiftPublic/add', [AdminMobile\AnswerActivityGiftPublicController::class, 'add']); // 新增
    // Route::post('/answerActivityGiftPublic/change', [AdminMobile\AnswerActivityGiftPublicController::class, 'change']); // 编辑
    // Route::post('/answerActivityGiftPublic/del', [AdminMobile\AnswerActivityGiftPublicController::class, 'del']); // 删除

    // /**
    //  * 答题活动礼物排版管理
    //  */
    // Route::get('/answerActivityGiftTime/list', [AdminMobile\AnswerActivityGiftTimeController::class, 'lists']); //  列表
    // Route::get('/answerActivityGiftTime/detail', [AdminMobile\AnswerActivityGiftTimeController::class, 'detail']); // 详情
    // Route::post('/answerActivityGiftTime/add', [AdminMobile\AnswerActivityGiftTimeController::class, 'add']); // 新增
    // Route::post('/answerActivityGiftTime/change', [AdminMobile\AnswerActivityGiftTimeController::class, 'change']); // 编辑
    // Route::post('/answerActivityGiftTime/del', [AdminMobile\AnswerActivityGiftTimeController::class, 'del']); // 删除

    // Route::get('/answerActivityGiftTime/getSendTotalConfig', [AdminMobile\AnswerActivityGiftTimeController::class, 'getSendTotalConfig']); //  获取礼物发放总概率
    // Route::post('/answerActivityGiftTime/setSendTotalConfig', [AdminMobile\AnswerActivityGiftTimeController::class, 'setSendTotalConfig']); // 设置礼物发放总概率

    // /**
    //  * 答题活动获奖列表
    //  */
    // Route::get('/answerActivityUserGift/getUnitList', [AdminMobile\AnswerActivityUserGiftController::class, 'getUnitList']); // 答题活动，获取单位列表
    // Route::get('/answerActivityUserGift/list', [AdminMobile\AnswerActivityUserGiftController::class, 'lists']); // 答题活动，获奖列表
    // Route::post('/answerActivityUserGift/userGiftGrant', [AdminMobile\AnswerActivityUserGiftController::class, 'userGiftGrant']); // 奖品领取或发奖
    // Route::post('/answerActivityUserGift/userPacketTransfer', [AdminMobile\AnswerActivityUserGiftController::class, 'userPacketTransfer']); // 红包转账失败，进行重新转账


    // /**
    //  * 活动资源管理
    //  */
    // Route::get('/answerActivityResource/getColorResource', [AdminMobile\AnswerActivityResourceController::class, 'getColorResource']); // 获取系统资源配置 （颜色配置）
    // Route::post('/answerActivityResource/setColorResource', [AdminMobile\AnswerActivityResourceController::class, 'setColorResource']); // 设置系统资源配置 （颜色配置）
    // Route::get('/answerActivityResource/resourceDownload', [AdminMobile\AnswerActivityResourceController::class, 'resourceDownload']); // 下载ui资源文件
    // Route::post('/answerActivityResource/resourceUpload', [AdminMobile\AnswerActivityResourceController::class, 'resourceUpload']); // 上传ui资源文件
    // Route::get('/answerActivityResource/getResourceTemplateParam', [AdminMobile\AnswerActivityResourceController::class, 'getResourceTemplateParam']); // 获取资源图片模板列表
    // Route::get('/answerActivityResource/getDefaultResourceAddr', [AdminMobile\AnswerActivityResourceController::class, 'getDefaultResourceAddr']); // 获取默认活动资源路径

    // Route::get('/answerActivityResource/getWordResource', [AdminMobile\AnswerActivityResourceController::class, 'getWordResource']); // 获取文字资源
    // Route::post('/answerActivityResource/setWordResource', [AdminMobile\AnswerActivityResourceController::class, 'setWordResource']); // 文字资源配置


    // /**
    //  * 活动排名
    //  */
    // Route::get('/answerActivityRanking/ranking', [AdminMobile\AnswerActivityRankingController::class, 'ranking']); // 活动排名
    // Route::post('/answerActivityRanking/rankingTransfer', [AdminMobile\AnswerActivityRankingController::class, 'rankingTransfer']); // 活动排名转账


    // /**
    //  * 线上答题活动 数据分析
    //  */
    // Route::get('/answerActivityDataAnalysis/dataStatistics', [AdminMobile\AnswerActivityDataAnalysisController::class, 'dataStatistics']); // 数据统计
    // Route::get('/answerActivityDataAnalysis/answerStatistics', [AdminMobile\AnswerActivityDataAnalysisController::class, 'answerStatistics']); // 答题次数统计
    // Route::get('/answerActivityDataAnalysis/giftGrantStatistics', [AdminMobile\AnswerActivityDataAnalysisController::class, 'giftGrantStatistics']); // 礼物发放统计
    // Route::get('/answerActivityDataAnalysis/unitStatisticsList', [AdminMobile\AnswerActivityDataAnalysisController::class, 'unitStatisticsList']); // 单位数据统计列表
    // Route::get('/answerActivityDataAnalysis/systemGiftGrantStatistics', [AdminMobile\AnswerActivityDataAnalysisController::class, 'systemGiftGrantStatistics']); // 系统投放礼物数据

    // /**
    //  * 线上大赛类型
    //  */
    // Route::get('/contestActivityType/filterList', [AdminMobile\ContestActivityTypeController::class, 'filterList']); // 筛选列表
    // Route::get('/contestActivityType/list', [AdminMobile\ContestActivityTypeController::class, 'lists']); // 列表
    // Route::get('/contestActivityType/detail', [AdminMobile\ContestActivityTypeController::class, 'detail']); // 详情
    // Route::post('/contestActivityType/add', [AdminMobile\ContestActivityTypeController::class, 'add']); // 新增
    // Route::post('/contestActivityType/change', [AdminMobile\ContestActivityTypeController::class, 'change']); // 编辑
    // Route::post('/contestActivityType/del', [AdminMobile\ContestActivityTypeController::class, 'del']); // 删除


    /**
     * 线上大赛 原创申明   原创申请，只在2个地方配置；1.创建大赛 2.单位管理
     */
    // Route::get('/contestActivityDeclare/detail', [AdminMobile\ContestActivityDeclareController::class, 'detail']); // 详情
    // Route::post('/contestActivityDeclare/change', [AdminMobile\ContestActivityDeclareController::class, 'change']); //编辑

    /**
     * 线上大赛
     */
    // Route::get('/contestActivity/getContestActivityApplyParam', [AdminMobile\ContestActivityController::class, 'getContestActivityApplyParam']); // 获取报名参数
    // Route::get('/contestActivity/list', [AdminMobile\ContestActivityController::class, 'lists']); // 列表
    // Route::get('/contestActivity/detail', [AdminMobile\ContestActivityController::class, 'detail']); // 详情
    // Route::post('/contestActivity/add', [AdminMobile\ContestActivityController::class, 'add']); // 新增
    // Route::post('/contestActivity/change', [AdminMobile\ContestActivityController::class, 'change']); // 编辑
    // Route::post('/contestActivity/del', [AdminMobile\ContestActivityController::class, 'del']); // 删除
    // Route::post('/contestActivity/playAndCancel', [AdminMobile\ContestActivityController::class, 'playAndCancel']); // 发布与取消发布

    // /**
    //  * 线上大赛作品
    //  */
    // Route::get('/contestActivityWorks/getLibName', [AdminMobile\ContestActivityWorksController::class, 'getLibName']); // 获取图书馆列表（和总馆的可能不一样）
    // Route::get('/contestActivityWorks/list', [AdminMobile\ContestActivityWorksController::class, 'lists']); // 列表
    // Route::get('/contestActivityWorks/detail', [AdminMobile\ContestActivityWorksController::class, 'detail']); // 详情
    // Route::post('/contestActivityWorks/check', [AdminMobile\ContestActivityWorksController::class, 'worksCheck']); // 新增
    // Route::post('/contestActivityWorks/violateAndCancel', [AdminMobile\ContestActivityWorksController::class, 'violateAndCancel']); // 作品违规 与 取消违规操作

    // Route::get('/contestActivityWorks/downloadWorks', [AdminMobile\ContestActivityWorksController::class, 'downloadWorks']); //  单个下载作品
    // Route::get('/contestActivityWorks/downloadWorksAll', [AdminMobile\ContestActivityWorksController::class, 'downloadWorksAll']); //  批量下载作品


    // /**
    //  * 线上大赛单位管理
    //  */
    // Route::get('/contestActivityUnit/getNatureName', [AdminMobile\ContestActivityUnitController::class, 'getNatureName']); //获取单位性质
    // Route::get('/contestActivityUnit/filterList', [AdminMobile\ContestActivityUnitController::class, 'filterList']); //类型(用于下拉框选择)
    // Route::get('/contestActivityUnit/list', [AdminMobile\ContestActivityUnitController::class, 'lists']); //  列表
    // Route::get('/contestActivityUnit/detail', [AdminMobile\ContestActivityUnitController::class, 'detail']); // 详情
    // Route::post('/contestActivityUnit/add', [AdminMobile\ContestActivityUnitController::class, 'add']); // 新增
    // Route::post('/contestActivityUnit/change', [AdminMobile\ContestActivityUnitController::class, 'change']); // 编辑
    // Route::post('/contestActivityUnit/del', [AdminMobile\ContestActivityUnitController::class, 'del']); // 删除
    // Route::post('/contestActivityUnit/orderChange', [AdminMobile\ContestActivityUnitController::class, 'orderChange']); // 修改区域排序

    // /**
    //  * 在线抽奖活动类
    //  */
    // Route::get('/turnActivity/list', [AdminMobile\TurnActivityController::class, 'lists']); // 列表
    // Route::get('/turnActivity/detail', [AdminMobile\TurnActivityController::class, 'detail']); // 详情
    // Route::post('/turnActivity/add', [AdminMobile\TurnActivityController::class, 'add']); // 新增
    // Route::post('/turnActivity/change', [AdminMobile\TurnActivityController::class, 'change']); // 编辑
    // Route::post('/turnActivity/del', [AdminMobile\TurnActivityController::class, 'del']); // 删除
    // Route::post('/turnActivity/cancelAndRelease', [AdminMobile\TurnActivityController::class, 'cancelAndRelease']); // 撤销 和发布
    // Route::post('/turnActivity/invitCodeChange', [AdminMobile\TurnActivityController::class, 'invitCodeChange']); // 邀请码修改

    // Route::post('/turnActivity/copyData', [AdminMobile\TurnActivityController::class, 'copyData']); // 复制活动

    // /**
    //  * 在线抽奖活动礼物管理
    //  */
    // Route::get('/turnActivityGift/list', [AdminMobile\TurnActivityGiftController::class, 'lists']); //  列表
    // Route::get('/turnActivityGift/detail', [AdminMobile\TurnActivityGiftController::class, 'detail']); // 详情
    // Route::post('/turnActivityGift/add', [AdminMobile\TurnActivityGiftController::class, 'add']); // 新增
    // Route::post('/turnActivityGift/change', [AdminMobile\TurnActivityGiftController::class, 'change']); // 编辑
    // Route::post('/turnActivityGift/del', [AdminMobile\TurnActivityGiftController::class, 'del']); // 删除

    // /**
    //  * 在线抽奖活动活动资源管理
    //  */
    // Route::get('/turnActivityResource/getColorResource', [AdminMobile\TurnActivityResourceController::class, 'getColorResource']); // 获取系统资源配置 （颜色配置）
    // Route::post('/turnActivityResource/setColorResource', [AdminMobile\TurnActivityResourceController::class, 'setColorResource']); // 设置系统资源配置 （颜色配置）
    // Route::get('/turnActivityResource/resourceDownload', [AdminMobile\TurnActivityResourceController::class, 'resourceDownload']); // 下载ui资源文件
    // Route::post('/turnActivityResource/resourceUpload', [AdminMobile\TurnActivityResourceController::class, 'resourceUpload']); // 上传ui资源文件
    // Route::get('/turnActivityResource/getDefaultResourceAddr', [AdminMobile\TurnActivityResourceController::class, 'getDefaultResourceAddr']); // 获取默认活动资源路径


    // /**
    //  * 答题活动限制地址
    //  */
    // Route::get('/turnActivityLimitAddress/list', [AdminMobile\TurnActivityLimitAddressController::class, 'lists']); //  列表
    // Route::get('/turnActivityLimitAddress/detail', [AdminMobile\TurnActivityLimitAddressController::class, 'detail']); // 详情
    // Route::post('/turnActivityLimitAddress/add', [AdminMobile\TurnActivityLimitAddressController::class, 'add']); // 新增
    // Route::post('/turnActivityLimitAddress/change', [AdminMobile\TurnActivityLimitAddressController::class, 'change']); // 编辑
    // Route::post('/turnActivityLimitAddress/del', [AdminMobile\TurnActivityLimitAddressController::class, 'del']); // 删除

    // /**
    //  * 在线抽奖活动礼物排版管理
    //  */
    // Route::get('/turnActivityGiftTime/list', [AdminMobile\TurnActivityGiftTimeController::class, 'lists']); //  列表
    // Route::get('/turnActivityGiftTime/detail', [AdminMobile\TurnActivityGiftTimeController::class, 'detail']); // 详情
    // Route::post('/turnActivityGiftTime/add', [AdminMobile\TurnActivityGiftTimeController::class, 'add']); // 新增
    // Route::post('/turnActivityGiftTime/change', [AdminMobile\TurnActivityGiftTimeController::class, 'change']); // 编辑
    // Route::post('/turnActivityGiftTime/del', [AdminMobile\TurnActivityGiftTimeController::class, 'del']); // 删除

    // Route::get('/turnActivityGiftTime/getSendTotalConfig', [AdminMobile\TurnActivityGiftTimeController::class, 'getSendTotalConfig']); //  获取礼物发放总概率
    // Route::post('/turnActivityGiftTime/setSendTotalConfig', [AdminMobile\TurnActivityGiftTimeController::class, 'setSendTotalConfig']); // 设置礼物发放总概率

    // /**
    //  * 在线抽奖活动获奖列表
    //  */
    // Route::get('/turnActivityUserGift/list', [AdminMobile\TurnActivityUserGiftController::class, 'lists']); // 答题活动，获奖列表
    // Route::post('/turnActivityUserGift/userGiftGrant', [AdminMobile\TurnActivityUserGiftController::class, 'userGiftGrant']); // 奖品领取或发奖

    // /**
    //  * 线上答题活动 数据分析
    //  */
    // Route::get('/turnActivityDataAnalysis/dataStatistics', [AdminMobile\TurnActivityDataAnalysisController::class, 'dataStatistics']); // 数据统计
    // Route::get('/turnActivityDataAnalysis/drawStatistics', [AdminMobile\TurnActivityDataAnalysisController::class, 'drawStatistics']); // 抽奖次数统计
    // Route::get('/turnActivityDataAnalysis/giftGrantStatistics', [AdminMobile\TurnActivityDataAnalysisController::class, 'giftGrantStatistics']); // 礼物发放统计
    //  Route::get('/turnActivityDataAnalysis/systemGiftGrantStatistics', [AdminMobile\TurnActivityDataAnalysisController::class, 'systemGiftGrantStatistics']); // 系统投放礼物数据

    /**
     * 导出
     */
    // Route::get('/answerActivityBaseUnitExport/getUnitList', [Controllers\Export\AnswerActivityBaseUnitExport::class, 'getUnitList']); // 基础单位导出
    // Route::get('/answerActivityUnitExport/getUnitList', [Controllers\Export\AnswerActivityUnitExport::class, 'getUnitList']); // 活动单位导出
    // Route::get('/answerActivityUnitExport/template', [Controllers\Export\AnswerActivityUnitExport::class, 'template']); // 活动单位模板导出
    // Route::get('/answerActivityRankingExport/ranking', [Controllers\Export\AnswerActivityRankingExport::class, 'ranking']); // 活动排名导出

    // Route::get('/answerActivityProblemExport/index', [Controllers\Export\AnswerActivityProblemExport::class, 'index']); // 活动题库导出
    // Route::get('/answerActivityProblemExport/template', [Controllers\Export\AnswerActivityProblemExport::class, 'template']); // 活动题库模板导出

    // Route::get('/answerActivityUserGiftExport/index', [Controllers\Export\AnswerActivityUserGiftExport::class, 'index']); // 答题活动-用户活动获取礼物导出
    // Route::get('/turnActivityUserGiftExport/index', [Controllers\Export\TurnActivityUserGiftExport::class, 'index']); // 在线抽奖活动-用户活动获取礼物导出


    // /**
    //  * 导入
    //  */
    // Route::post('/answerActivityUnitImport/index', [Controllers\Import\AnswerActivityUnitImport::class, 'index']); // 活动单位导入
    // Route::post('/answerActivityProblemImport/index', [Controllers\Import\AnswerActivityProblemImport::class, 'index']); // 活动题库导入

    ######################################################抽奖活动end#############################################################

    ######################################################图片直播start#############################################################
    /**
     * 图片直播管理
     */
    // Route::get('/pictureLive/list', [AdminMobile\PictureLiveController::class, 'lists']); //  列表
    // Route::get('/pictureLive/detail', [AdminMobile\PictureLiveController::class, 'detail']); //  详情
    // Route::post('/pictureLive/add', [AdminMobile\PictureLiveController::class, 'add']); //  新增
    // Route::post('/pictureLive/change', [AdminMobile\PictureLiveController::class, 'change']); //  修改
    // Route::post('/pictureLive/del', [AdminMobile\PictureLiveController::class, 'del']); //  删除
    // Route::post('/pictureLive/cancelAndRelease', [AdminMobile\PictureLiveController::class, 'cancelAndRelease']); //  撤销 和发布
    // /**
    //  * 图片直播黑名单用户
    //  */
    // Route::get('/pictureLiveBlack/list', [AdminMobile\PictureLiveBlackController::class, 'lists']); //  黑名单用户列表
    // Route::post('/pictureLiveBlack/add', [AdminMobile\PictureLiveBlackController::class, 'add']); //  黑名单用户新增
    // Route::post('/pictureLiveBlack/del', [AdminMobile\PictureLiveBlackController::class, 'del']); //  黑名单用户删除


    /**
     * 图片直播指定用户
     */
    Route::get('/pictureLiveAppoint/list', [AdminMobile\PictureLiveAppointController::class, 'lists']); //  指定用户列表
    Route::post('/pictureLiveAppoint/add', [AdminMobile\PictureLiveAppointController::class, 'add']); //  指定用户新增
    Route::post('/pictureLiveAppoint/del', [AdminMobile\PictureLiveAppointController::class, 'del']); //  指定用户删除

    /**
     * 直播活动作品管理
     */
    // Route::get('/pictureLiveWorks/list', [AdminMobile\PictureLiveWorksController::class, 'lists']); //  列表
    // Route::get('/pictureLiveWorks/detail', [AdminMobile\PictureLiveWorksController::class, 'detail']); //  详情
    // Route::post('/pictureLiveWorks/worksCheck', [AdminMobile\PictureLiveWorksController::class, 'worksCheck']); //  审核作品


    // Route::post('/pictureLiveWorks/add', [AdminMobile\PictureLiveWorksController::class, 'add']); //  图片直播，上传图片（单张上传）
    // Route::post('/pictureLiveWorks/batchAdd', [AdminMobile\PictureLiveWorksController::class, 'batchAdd']); //  图片直播，上传图片(批量上传)
    // Route::post('/pictureLiveWorks/change', [AdminMobile\PictureLiveWorksController::class, 'change']); //  修改作品上传用户信息
    // Route::post('/pictureLiveWorks/del', [AdminMobile\PictureLiveWorksController::class, 'del']); //  删除作品

    // Route::get('/pictureLiveWorks/downloadWorksImg', [AdminMobile\PictureLiveWorksController::class, 'downloadWorksImg']); //  下载直播图片

    ######################################################视频直播start#############################################################
    /**
     * 视频直播管理
     */
    // Route::get('/videoLive/list', [AdminMobile\VideoLiveController::class, 'lists']); //  列表
    // Route::get('/videoLive/detail', [AdminMobile\VideoLiveController::class, 'detail']); //  详情
    // Route::post('/videoLive/add', [AdminMobile\VideoLiveController::class, 'add']); //  新增
    // Route::post('/videoLive/change', [AdminMobile\VideoLiveController::class, 'change']); //  修改
    // Route::post('/videoLive/del', [AdminMobile\VideoLiveController::class, 'del']); //  删除
    // Route::post('/videoLive/cancelAndRelease', [AdminMobile\VideoLiveController::class, 'cancelAndRelease']); //  撤销 和发布
    // Route::post('/videoLive/changeLiveStatus', [AdminMobile\VideoLiveController::class, 'changeLiveStatus']); //  修改直播状态
    // Route::post('/videoLive/setVideoCallbackAddress', [AdminMobile\VideoLiveController::class, 'setVideoCallbackAddress']); //  设置视频录制回调地址
    // Route::get('/videoLive/getVideoCallbackAddress', [AdminMobile\VideoLiveController::class, 'getVideoCallbackAddress']); //  获取视频录制回调地址
    // Route::post('/videoLive/setChannelTranscribeAddrByState', [AdminMobile\VideoLiveController::class, 'setChannelTranscribeAddrByState']); //  设置频道状态变化回调地址（程序调用）
    // Route::get('/videoLive/getChannelTranscribeAddrByState', [AdminMobile\VideoLiveController::class, 'getChannelTranscribeAddrByState']); //  获取频道状态变化回调地址（程序调用）



    /******************************************扫码导游***************************************************/
    /**
     * 展览配置类
     */
    // Route::get('/codeGuideExhibition/list', [AdminMobile\CodeGuideExhibitionController::class, 'lists']); // 列表
    // Route::get('/codeGuideExhibition/detail', [AdminMobile\CodeGuideExhibitionController::class, 'detail']); // 详情
    // Route::post('/codeGuideExhibition/add', [AdminMobile\CodeGuideExhibitionController::class, 'add']); // 新增
    // Route::post('/codeGuideExhibition/change', [AdminMobile\CodeGuideExhibitionController::class, 'change']); // 编辑
    // Route::post('/codeGuideExhibition/del', [AdminMobile\CodeGuideExhibitionController::class, 'del']); // 删除
    // Route::post('/codeGuideExhibition/cancelAndRelease', [AdminMobile\CodeGuideExhibitionController::class, 'cancelAndRelease']); //  撤销 和发布



    // /**
    //  * 展览作品配置类
    //  */
    // Route::get('/codeGuideProduction/list', [AdminMobile\CodeGuideProductionController::class, 'lists']); // 列表
    // Route::get('/codeGuideProduction/detail', [AdminMobile\CodeGuideProductionController::class, 'detail']); // 详情
    // Route::post('/codeGuideProduction/add', [AdminMobile\CodeGuideProductionController::class, 'add']); // 新增
    // Route::post('/codeGuideProduction/change', [AdminMobile\CodeGuideProductionController::class, 'change']); // 编辑
    // Route::post('/codeGuideProduction/del', [AdminMobile\CodeGuideProductionController::class, 'del']); // 删除

    // /**
    //  * 展览作品类型
    //  */
    // Route::get('/codeGuideProductionType/filterList', [AdminMobile\CodeGuideProductionTypeController::class, 'filterList']); // 筛选列表
    // Route::get('/codeGuideProductionType/list', [AdminMobile\CodeGuideProductionTypeController::class, 'lists']); // 列表
    // Route::get('/codeGuideProductionType/detail', [AdminMobile\CodeGuideProductionTypeController::class, 'detail']); // 详情
    // Route::post('/codeGuideProductionType/add', [AdminMobile\CodeGuideProductionTypeController::class, 'add']); // 新增
    // Route::post('/codeGuideProductionType/change', [AdminMobile\CodeGuideProductionTypeController::class, 'change']); // 编辑
    // Route::post('/codeGuideProductionType/del', [AdminMobile\CodeGuideProductionTypeController::class, 'del']); // 删除


    /******************************************空间预约***************************************************/
    /**
     * 预约
     */
    // Route::get('/studyRoomReservation/getReservationApplyParam', [AdminMobile\StudyRoomReservationController::class, 'getReservationApplyParam']); // 获取预约参数
    // Route::get('/studyRoomReservation/filterTagList', [AdminMobile\StudyRoomReservationController::class, 'filterTagList']); //文化配送标签列表（预定义）
    // Route::get('/studyRoomReservation/getFilterList', [AdminMobile\StudyRoomReservationController::class, 'getFilterList']); //获取筛选列表
    // Route::get('/studyRoomReservation/list', [AdminMobile\StudyRoomReservationController::class, 'lists']); //列表
    // Route::get('/studyRoomReservation/detail', [AdminMobile\StudyRoomReservationController::class, 'detail']); //详情
    // Route::post('/studyRoomReservation/add', [AdminMobile\StudyRoomReservationController::class, 'add']); //新增
    // Route::post('/studyRoomReservation/change', [AdminMobile\StudyRoomReservationController::class, 'change']); //修改
    // Route::post('/studyRoomReservation/del', [AdminMobile\StudyRoomReservationController::class, 'del']); //删除
    // Route::post('/studyRoomReservation/playAndCancel', [AdminMobile\StudyRoomReservationController::class, 'playAndCancel']); //推荐与取消推荐

    // //特殊排班时间
    // Route::get('/studyRoomReservationSpecialSchedule/list', [AdminMobile\StudyRoomReservationSpecialScheduleController::class, 'lists']); //特殊时间列表
    // Route::post('/studyRoomReservationSpecialSchedule/add', [AdminMobile\StudyRoomReservationSpecialScheduleController::class, 'add']); //特殊时间新增
    // Route::post('/studyRoomReservationSpecialSchedule/change', [AdminMobile\StudyRoomReservationSpecialScheduleController::class, 'change']); //特殊时间编辑
    // Route::get('/studyRoomReservationSpecialSchedule/detail', [AdminMobile\StudyRoomReservationSpecialScheduleController::class, 'detail']); //特殊时间详情
    // Route::post('/studyRoomReservationSpecialSchedule/del', [AdminMobile\StudyRoomReservationSpecialScheduleController::class, 'del']); //特殊时间删除

    // Route::get('/studyRoomReservationApply/list', [AdminMobile\StudyRoomReservationApplyController::class, 'lists']); //预约申请列表
    // Route::get('/studyRoomReservationApply/reservationApplyHistory', [AdminMobile\StudyRoomReservationApplyController::class, 'reservationApplyHistory']); //获取用户对于某个预约的预约记录
    // Route::post('/studyRoomReservationApply/agreeAndRefused', [AdminMobile\StudyRoomReservationApplyController::class, 'agreeAndRefused']); //审核通过 和 拒绝
    // Route::post('/studyRoomReservationApply/violateAndCancel', [AdminMobile\StudyRoomReservationApplyController::class, 'violateAndCancel']); //预约违规 与 取消违规操作

    // Route::post('/studyRoomReservationApply/applyCancel', [AdminMobile\StudyRoomReservationApplyController::class, 'applyCancel']); //管理员取消预约
    // Route::post('/studyRoomReservationApply/applySignIn', [AdminMobile\StudyRoomReservationApplyController::class, 'applySignIn']); //管理员打卡签到
    // Route::post('/studyRoomReservationApply/applySignOut', [AdminMobile\StudyRoomReservationApplyController::class, 'applySignOut']); //管理员打卡签退

    // /**
    //  * 白名单配置
    //  */
    // Route::get('studyRoomReservationWhite/list', [AdminMobile\StudyRoomReservationWhiteController::class, 'lists']); //列表
    // Route::post('studyRoomReservationWhite/add', [AdminMobile\StudyRoomReservationWhiteController::class, 'add']); //添加
    // Route::post('studyRoomReservationWhite/change',  [AdminMobile\StudyRoomReservationWhiteController::class, 'change']); //修改
    // Route::post('studyRoomReservationWhite/del',  [AdminMobile\StudyRoomReservationWhiteController::class, 'del']); //删除
    // Route::get('studyRoomReservationWhite/getApplyQr',  [AdminMobile\StudyRoomReservationWhiteController::class, 'getApplyQr']); //出示二维码

    // /**
    //  * 远程操作
    //  */
    // Route::get('studyRoomReservationRemoteOperation/list', [AdminMobile\StudyRoomReservationRemoteOperationController::class, 'lists']); //远程操作列表
    // Route::post('studyRoomReservationRemoteOperation/remoteOperation', [AdminMobile\StudyRoomReservationRemoteOperationController::class, 'remoteOperation']); //远程操作

    // /******************************************室内导航配置***************************************************/

    // /**
    //  * 室内导航区域配置
    //  */
    // Route::get('/navigationArea/filterList', [AdminMobile\NavigationAreaController::class, 'filterList']); //筛选列表
    // Route::get('/navigationArea/list', [AdminMobile\NavigationAreaController::class, 'lists']); //列表
    // Route::get('/navigationArea/detail', [AdminMobile\NavigationAreaController::class, 'detail']); //详情
    // Route::post('/navigationArea/add', [AdminMobile\NavigationAreaController::class, 'add']); //新增
    // Route::post('/navigationArea/change', [AdminMobile\NavigationAreaController::class, 'change']); //修改
    // Route::post('/navigationArea/del', [AdminMobile\NavigationAreaController::class, 'del']); //删除
    // Route::post('/navigationArea/playAndCancel', [AdminMobile\NavigationAreaController::class, 'playAndCancel']); //推荐与取消推荐
    // Route::post('/navigationArea/sortChange', [AdminMobile\NavigationAreaController::class, 'sortChange']); //修改区域排序

    // /**
    //  * 室内导航建筑配置
    //  */
    // Route::get('/navigationBuild/list', [AdminMobile\NavigationBuildController::class, 'lists']); //列表
    // Route::get('/navigationBuild/detail', [AdminMobile\NavigationBuildController::class, 'detail']); //详情
    // Route::post('/navigationBuild/add', [AdminMobile\NavigationBuildController::class, 'add']); //新增
    // Route::post('/navigationBuild/change', [AdminMobile\NavigationBuildController::class, 'change']); //修改
    // Route::post('/navigationBuild/del', [AdminMobile\NavigationBuildController::class, 'del']); //删除
    // Route::post('/navigationBuild/playAndCancel', [AdminMobile\NavigationBuildController::class, 'playAndCancel']); //推荐与取消推荐
    // /**
    //  *  室内导航点位配置
    //  */
    // Route::get('/navigationPoint/list', [AdminMobile\NavigationPointController::class, 'lists']); //列表
    // Route::get('/navigationPoint/detail', [AdminMobile\NavigationPointController::class, 'detail']); //详情
    // Route::post('/navigationPoint/add', [AdminMobile\NavigationPointController::class, 'add']); //新增
    // Route::post('/navigationPoint/change', [AdminMobile\NavigationPointController::class, 'change']); //修改
    // Route::post('/navigationPoint/del', [AdminMobile\NavigationPointController::class, 'del']); //删除
    // Route::post('/navigationPoint/playAndCancel', [AdminMobile\NavigationPointController::class, 'playAndCancel']); //推荐与取消推荐
    // Route::post('/navigationPoint/sortChange', [AdminMobile\NavigationAreaController::class, 'sortChange']); //修改区域排序

    // /**
    //  *  应用邀请码类
    //  */
    // Route::get('/appInviteCode/filterList', [AdminMobile\AppInviteCodeController::class, 'filterList']); //用于下拉框选择
    // Route::get('/appInviteCode/list', [AdminMobile\AppInviteCodeController::class, 'lists']); //列表
    // Route::get('/appInviteCode/detail', [AdminMobile\AppInviteCodeController::class, 'detail']); //详情
    // Route::post('/appInviteCode/add', [AdminMobile\AppInviteCodeController::class, 'add']); //新增
    // Route::post('/appInviteCode/change', [AdminMobile\AppInviteCodeController::class, 'change']); //修改
    // Route::post('/appInviteCode/del', [AdminMobile\AppInviteCodeController::class, 'del']); //删除
    // /**
    //  *  应用邀请码行为类
    //  */
    // Route::get('/appInviteCodeBehavior/filterList', [AdminMobile\AppInviteCodeBehaviorController::class, 'filterList']); //类型(用于下拉框选择)
    // Route::get('/appInviteCodeBehavior/list', [AdminMobile\AppInviteCodeBehaviorController::class, 'lists']); //列表
    // Route::get('/appInviteCodeBehavior/accessStatistics', [AdminMobile\AppInviteCodeBehaviorController::class, 'accessStatistics']); //应用访问总统计图






    // /**
    //  * 线上大赛作品类型
    //  */
    // Route::get('/competiteActivityWorksType/filterList', [AdminMobile\CompetiteActivityWorksTypeController::class, 'filterList']); // 筛选列表
    // Route::get('/competiteActivityWorksType/list', [AdminMobile\CompetiteActivityWorksTypeController::class, 'lists']); // 列表
    // Route::get('/competiteActivityWorksType/detail', [AdminMobile\CompetiteActivityWorksTypeController::class, 'detail']); // 详情
    // Route::post('/competiteActivityWorksType/add', [AdminMobile\CompetiteActivityWorksTypeController::class, 'add']); // 新增
    // Route::post('/competiteActivityWorksType/change', [AdminMobile\CompetiteActivityWorksTypeController::class, 'change']); // 编辑
    // Route::post('/competiteActivityWorksType/del', [AdminMobile\CompetiteActivityWorksTypeController::class, 'del']); // 删除

    // /**
    //  * 线上大赛
    //  */
    // Route::get('/competiteActivity/getCompetiteActivityApplyParam', [AdminMobile\CompetiteActivityController::class, 'getCompetiteActivityApplyParam']); // 获取报名参数
    // Route::get('/competiteActivity/filterList', [AdminMobile\CompetiteActivityController::class, 'filterList']); // 筛选列表
    // Route::get('/competiteActivity/list', [AdminMobile\CompetiteActivityController::class, 'lists']); // 列表
    // Route::get('/competiteActivity/detail', [AdminMobile\CompetiteActivityController::class, 'detail']); // 详情
    // Route::post('/competiteActivity/add', [AdminMobile\CompetiteActivityController::class, 'add']); // 新增
    // Route::post('/competiteActivity/change', [AdminMobile\CompetiteActivityController::class, 'change']); // 编辑
    // Route::post('/competiteActivity/del', [AdminMobile\CompetiteActivityController::class, 'del']); // 删除
    // Route::post('/competiteActivity/playAndCancel', [AdminMobile\CompetiteActivityController::class, 'playAndCancel']); // 发布与取消发布


    // /**
    //  * 征集活动类型
    //  */
    // Route::get('/competiteActivityType/filterList', [AdminMobile\CompetiteActivityTypeController::class, 'filterList']); // 筛选列表
    // Route::get('/competiteActivityType/list', [AdminMobile\CompetiteActivityTypeController::class, 'lists']); // 列表
    // Route::get('/competiteActivityType/detail', [AdminMobile\CompetiteActivityTypeController::class, 'detail']); // 详情
    // Route::post('/competiteActivityType/add', [AdminMobile\CompetiteActivityTypeController::class, 'add']); // 新增
    // Route::post('/competiteActivityType/change', [AdminMobile\CompetiteActivityTypeController::class, 'change']); // 编辑
    // Route::post('/competiteActivityType/del', [AdminMobile\CompetiteActivityTypeController::class, 'del']); // 删除

    // /**
    //  * 征集活动标签
    //  */
    // Route::get('/competiteActivityTag/filterList', [AdminMobile\CompetiteActivityTagController::class, 'filterList']); // 筛选列表
    // Route::get('/competiteActivityTag/list', [AdminMobile\CompetiteActivityTagController::class, 'lists']); // 列表
    // Route::get('/competiteActivityTag/detail', [AdminMobile\CompetiteActivityTagController::class, 'detail']); // 详情
    // Route::post('/competiteActivityTag/add', [AdminMobile\CompetiteActivityTagController::class, 'add']); // 新增
    // Route::post('/competiteActivityTag/change', [AdminMobile\CompetiteActivityTagController::class, 'change']); // 编辑
    // Route::post('/competiteActivityTag/del', [AdminMobile\CompetiteActivityTagController::class, 'del']); // 删除


    // /**
    //  * 线上大赛作品
    //  */
    // Route::get('/competiteActivityWorks/list', [AdminMobile\CompetiteActivityWorksController::class, 'lists']); // 列表
    // Route::get('/competiteActivityWorks/detail', [AdminMobile\CompetiteActivityWorksController::class, 'detail']); // 详情

    // Route::get('/competiteActivityWorks/getNextUnchecked', [AdminMobile\CompetiteActivityWorksController::class, 'getNextUnchecked']); // 管理员自动获取下一个未审核作品
    // Route::get('/competiteActivityWorks/getNextUnScoreed', [AdminMobile\CompetiteActivityWorksController::class, 'getNextUnScoreed']); // 管理员自动获取下一个未打分作品

    // Route::post('/competiteActivityWorks/check', [AdminMobile\CompetiteActivityWorksController::class, 'worksCheck']); // 审核
    // Route::post('/competiteActivityWorks/reviewScore', [AdminMobile\CompetiteActivityWorksController::class, 'worksReviewScore']); // 作品评审打分
    // Route::post('/competiteActivityWorks/violateAndCancel', [AdminMobile\CompetiteActivityWorksController::class, 'violateAndCancel']); // 作品违规 与 取消违规操作

    // Route::get('/competiteActivityWorks/downloadWorks', [AdminMobile\CompetiteActivityWorksController::class, 'downloadWorks']); //  单个下载作品
    // Route::get('/competiteActivityWorks/downloadWorksAll', [AdminMobile\CompetiteActivityWorksController::class, 'downloadWorksAll']); //  批量下载作品
    // Route::post('/competiteActivityWorks/showAndCancel', [AdminMobile\CompetiteActivityWorksController::class, 'showAndCancel']); //  显示与取消显示
    // Route::post('/competiteActivityWorks/worksDelete', [AdminMobile\CompetiteActivityWorksController::class, 'worksDelete']); //  删除作品
    // Route::post('/competiteActivityWorks/worksRefuseDirectly', [AdminMobile\CompetiteActivityWorksController::class, 'worksRefuseDirectly']); //  一键拒绝作品（最高管理员）

    // Route::get('/competiteActivityWorksExport/index', [Controllers\Export\CompetiteActivityWorksExport::class, 'index']); //  大赛作品导出

    // Route::get('/competiteActivityWorksScore/scoreList', [AdminMobile\CompetiteActivityWorksScoreController::class, 'scoreLists']); // 打分列表
    // Route::get('/competiteActivityWorksScore/scoreDetail', [AdminMobile\CompetiteActivityWorksScoreController::class, 'scoreDetail']); // 打分详情
    // Route::get('/competiteActivityWorksScore/getNextUnScoreWorks', [AdminMobile\CompetiteActivityWorksScoreController::class, 'getNextUnScoreWorks']); // 管理员自动获取下一个未打分作品


    // /**
    //  * 线上大赛单位管理
    //  */
    // Route::get('/competiteActivityGroup/filterList', [AdminMobile\CompetiteActivityGroupController::class, 'filterList']); //类型(用于下拉框选择)
    // Route::get('/competiteActivityGroup/list', [AdminMobile\CompetiteActivityGroupController::class, 'lists']); //  列表
    // Route::get('/competiteActivityGroup/detail', [AdminMobile\CompetiteActivityGroupController::class, 'detail']); // 详情
    // Route::post('/competiteActivityGroup/add', [AdminMobile\CompetiteActivityGroupController::class, 'add']); // 新增
    // Route::post('/competiteActivityGroup/change', [AdminMobile\CompetiteActivityGroupController::class, 'change']); // 编辑
    // Route::post('/competiteActivityGroup/del', [AdminMobile\CompetiteActivityGroupController::class, 'del']); // 删除
    // Route::post('/competiteActivityGroup/changeGroupPassword', [AdminMobile\CompetiteActivityGroupController::class, 'changeGroupPassword']); // 修改团队账号密码
    // Route::post('/competiteActivityGroup/orderChange', [AdminMobile\CompetiteActivityGroupController::class, 'orderChange']); // 修改区域排序



    // /**
    //  * 线上大赛作品数据库
    //  */
    // Route::get('/competiteActivityDatabase/filterList', [AdminMobile\CompetiteActivityDatabaseController::class, 'filterList']); //列表(用于下拉框选择)
    // Route::get('/competiteActivityDatabase/list', [AdminMobile\CompetiteActivityDatabaseController::class, 'lists']); //  列表
    // Route::get('/competiteActivityDatabase/detail', [AdminMobile\CompetiteActivityDatabaseController::class, 'detail']); // 详情
    // Route::post('/competiteActivityDatabase/add', [AdminMobile\CompetiteActivityDatabaseController::class, 'add']); // 新增
    // Route::post('/competiteActivityDatabase/change', [AdminMobile\CompetiteActivityDatabaseController::class, 'change']); // 编辑
    // Route::post('/competiteActivityDatabase/del', [AdminMobile\CompetiteActivityDatabaseController::class, 'del']); // 删除
    // Route::post('/competiteActivityDatabase/playAndCancel', [AdminMobile\CompetiteActivityDatabaseController::class, 'playAndCancel']); // 发布与取消发布
    // Route::post('/competiteActivityDatabase/sortChange', [AdminMobile\CompetiteActivityDatabaseController::class, 'sortChange']); // 排序


    // /**
    //  * 线上大赛作品数据库（作品管理）
    //  */
    // Route::get('/competiteActivityWorksDatabase/filterList', [AdminMobile\CompetiteActivityWorksDatabaseController::class, 'filterList']); //获取已收录的作品id，或电子书id
    // Route::get('/competiteActivityWorksDatabase/list', [AdminMobile\CompetiteActivityWorksDatabaseController::class, 'lists']); //  列表
    // Route::post('/competiteActivityWorksDatabase/add', [AdminMobile\CompetiteActivityWorksDatabaseController::class, 'add']); // 新增
    // Route::post('/competiteActivityWorksDatabase/del', [AdminMobile\CompetiteActivityWorksDatabaseController::class, 'del']); // 删除
    // Route::post('/competiteActivityWorksDatabase/sortChange', [AdminMobile\CompetiteActivityWorksDatabaseController::class, 'sortChange']); // 排序



    // /**
    //  * 线上大赛作品电子书
    //  */
    // Route::get('/competiteActivityEbook/list', [AdminMobile\CompetiteActivityEbookController::class, 'lists']); //  列表
    // Route::get('/competiteActivityEbook/detail', [AdminMobile\CompetiteActivityEbookController::class, 'detail']); // 详情
    // Route::post('/competiteActivityEbook/add', [AdminMobile\CompetiteActivityEbookController::class, 'add']); // 新增
    // Route::post('/competiteActivityEbook/change', [AdminMobile\CompetiteActivityEbookController::class, 'change']); // 编辑
    // Route::post('/competiteActivityEbook/del', [AdminMobile\CompetiteActivityEbookController::class, 'del']); // 删除
    // Route::post('/competiteActivityEbook/playAndCancel', [AdminMobile\CompetiteActivityEbookController::class, 'playAndCancel']); // 发布与取消发布
    // Route::post('/competiteActivityEbook/sortChange', [AdminMobile\CompetiteActivityEbookController::class, 'sortChange']); // 排序

    // /**
    //  * 线上大赛作品电子书（作品管理）
    //  */
    // Route::get('/competiteActivityWorksEbook/filterList', [AdminMobile\CompetiteActivityWorksEbookController::class, 'filterList']); //获取已收录的作品id
    // Route::get('/competiteActivityWorksEbook/list', [AdminMobile\CompetiteActivityWorksEbookController::class, 'lists']); //  列表
    // Route::post('/competiteActivityWorksEbook/add', [AdminMobile\CompetiteActivityWorksEbookController::class, 'add']); // 收录作品（大赛作品、上传图片、上传pdf）
    // Route::post('/competiteActivityWorksEbook/del', [AdminMobile\CompetiteActivityWorksEbookController::class, 'del']); // 删除
    // Route::post('/competiteActivityWorksEbook/sortChange', [AdminMobile\CompetiteActivityWorksEbookController::class, 'sortChange']); // 排序


    // /**
    //  * 线上大赛作品指定打分人
    //  */
    // Route::get('/competiteActivityWorksAppointScore/filterList', [AdminMobile\CompetiteActivityWorksAppointScoreController::class, 'filterList']); //根据打分人获取所有的作品id，或名字  简单列表
    // Route::get('/competiteActivityWorksAppointScore/list', [AdminMobile\CompetiteActivityWorksAppointScoreController::class, 'lists']); //  列表
    // Route::get('/competiteActivityWorksAppointScore/getScoreWorks', [AdminMobile\CompetiteActivityWorksAppointScoreController::class, 'getScoreWorks']); // 根据打分人获取所有的作品id，或名字 分页列表
    // Route::post('/competiteActivityWorksAppointScore/modify', [AdminMobile\CompetiteActivityWorksAppointScoreController::class, 'modify']); // 添加、修改打分作品
    // Route::post('/competiteActivityWorksAppointScore/del', [AdminMobile\CompetiteActivityWorksAppointScoreController::class, 'del']); // 删除打分作品


    // /**
    //  * 阅读任务
    //  */
    // Route::get('/readingTask/list', [AdminMobile\ReadingTaskController::class, 'lists']); //  列表
    // Route::get('/readingTask/detail', [AdminMobile\ReadingTaskController::class, 'detail']); //  详情
    // Route::post('/readingTask/add', [AdminMobile\ReadingTaskController::class, 'add']); // 新增
    // Route::post('/readingTask/change', [AdminMobile\ReadingTaskController::class, 'change']); // 修改
    // Route::post('/readingTask/del', [AdminMobile\ReadingTaskController::class, 'del']); // 删除
    // Route::post('/readingTask/cancelAndRelease', [AdminMobile\ReadingTaskController::class, 'cancelAndRelease']); // 撤销 和发布

    // Route::get('/readingTaskUser/list', [AdminMobile\ReadingTaskUserController::class, 'lists']); // 用户阅读任务列表


    // /**
    //  * 阅读任务指定用户
    //  */
    // Route::get('/readingTaskAppoint/list', [AdminMobile\readingTaskAppointController::class, 'lists']); //  指定用户列表
    // Route::post('/readingTaskAppoint/add', [AdminMobile\readingTaskAppointController::class, 'add']); //  指定用户新增
    // Route::post('/readingTaskAppoint/del', [AdminMobile\readingTaskAppointController::class, 'del']); //  指定用户删除





});
