<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Wechat;
use App\Http\Controllers\ScanQrController;
use App\Http\Controllers\ScoreRuleController;
use App\Http\Controllers\JwtController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//网站web端接口


Route::get('wechat/auth/index', [Wechat\WechatAuthController::class, 'indexInfo'])->withoutMiddleware(['invite', 'wechat_auth']); // 微信确认登陆后接受,获取信息  - 用户确认授权  不能用 post  和 any，否则前端即使用get，也会预请求，造成 code 重复使用
Route::get('wechat/auth/silentIndex', [Wechat\WechatAuthController::class, 'silentIndexInfo'])->withoutMiddleware(['invite', 'wechat_auth']); // 微信确认登陆后接受,获取信息 - 静默授权  不能用 post  和 any，否则前端即使用get，也会预请求，造成 code 重复使用
Route::get('wechat/auth/appletIndex', [Wechat\WechatAuthController::class, 'appletIndexInfo'])->withoutMiddleware(['invite', 'wechat_auth']); // 微信确认登陆后接受,获取信息 - 小程序授权   不能用 post  和 any，否则前端即使用get，也会预请求，造成 code 重复使用
Route::any('wechat/service/createParam', [Wechat\ServiceController::class, 'createParam'])->withoutMiddleware(['invite', 'wechat_auth']); // 获取微信分享需要参数
Route::get('/scoreRule/list', [ScoreRuleController::class, 'lists'])->withoutMiddleware(['jwt', 'wechat_auth']); //积分规则,因为微信也要使用，所以排除
#Route::get('/scoreRule/customScoreRule', [ScoreRuleController::class, 'customScoreRule'])->withoutMiddleware('jwt'); //获取自定义积分规则,因为微信也要使用，所以排除

Route::post('/getToken', [JwtController::class, 'getAuthToken'])->withoutMiddleware(['jwt', 'invite', 'wechat_auth']); //获取token,->withoutMiddleware([CheckJwtHeaderToken::class])排除这个中间件，因为在路由服务中间件中，自动运用了这个中间件

Route::get('wechat/userAccess/limitAccess', [Wechat\UserAccessDataController::class, 'limitAccess'])->withoutMiddleware(['invite', 'wechat_auth']); // 获取活动访问人数（答题活动）

Route::get('wechat/index/checkInviteCode', [Wechat\IndexController::class, 'checkInviteCode'])->withoutMiddleware(['wechat_auth']); // 第一次验证邀请码是否正确


//不需要token
Route::prefix('wechat')->group(function () {

    Route::get('/index/getAddressOrLonLat', [Wechat\IndexController::class, 'getAddressOrLonLat']); // 根据地址转经纬度、或根据经纬度转换地址

    Route::get('/userProtocol/getUserProtocol', [Wechat\UserProtocolController::class, 'getUserProtocol']); // 用户协议

    Route::get('/index/otherAccressAdd', [Wechat\IndexController::class, 'otherAccressAdd']); // 记录其他数据访问量

    /**
     * 图书馆相关（扫码借）
     */
    Route::get('/libInfo/getBookInfoList', [Wechat\LibInfoController::class, 'getBookInfoList']); // 获取检索信息列表
    Route::get('/libInfo/getLibBookDetail', [Wechat\LibInfoController::class, 'getLibBookDetail']); // 获取馆藏书籍详


    //新闻管理
    Route::get('/news/typeList', [Wechat\NewsController::class, 'typeList']); //新闻类型
    Route::get('/news/list', [Wechat\NewsController::class, 'lists']); //新闻列表

    //数字资源管理
    Route::get('/digital/typeList', [Wechat\DigitalController::class, 'typeList']); //数字资源类型
    Route::get('/digital/list', [Wechat\DigitalController::class, 'lists']); //列表
    Route::post('/digital/addBrowseNum', [Wechat\DigitalController::class, 'addBrowseNum']); //数字资源添加浏览量

    //好书推荐管理
    Route::get('/bookRecom/list', [Wechat\BookRecomController::class, 'lists']); //列表
    Route::get('/bookRecom/detail', [Wechat\BookRecomController::class, 'detail']); //详情

    /**
     * 场馆管理
     */
    Route::get('/branchInfo/list', [Wechat\BranchInfoController::class, 'lists']); // 场馆列表
    Route::get('/branchInfo/detail', [Wechat\BranchInfoController::class, 'detail']); // 场馆简介
    //   Route::post('/branch/addBrowseNum', [Wechat\BranchInfoController::class, 'addBrowseNum']); // 记录访问分馆的浏览量
    //   Route::post('/branch/branchAccountLogin', [Wechat\BranchInfoController::class, 'branchAccountLogin']); // 分馆用户登录账号登录



    /**
     * 活动管理
     */
    Route::get('/activity/getActivityCalendar', [Wechat\ActivityController::class, 'getActivityCalendar']); // 根据时间获取活动 展示活动日历
    Route::get('/activity/getActivityCalendarByTime', [Wechat\ActivityController::class, 'getActivityCalendarByTime']); // 根据时间获取活动 展示活动日历
    Route::get('/activity/getHotActivity', [Wechat\ActivityController::class, 'getHotActivity']); // 获取热门活动
    Route::get('/activity/getActivityRecom', [Wechat\ActivityController::class, 'getActivityRecom']); // 获取推荐，每个类型推荐
    Route::get('/activity/typeList', [Wechat\ActivityController::class, 'typeList']); // 活动类型列表
    Route::get('/activity/list', [Wechat\ActivityController::class, 'lists']); // 活动列表


    /**
     * 文旅地图
     */
    Route::get('/branchMap/list', [Wechat\BranchMapController::class, 'lists']); // 列表
    Route::get('/branchMap/detail', [Wechat\BranchMapController::class, 'detail']); // 详情
    /**
     * 根据商品类型  获取商品列表
     */
    Route::get('/goods/filterList', [Wechat\GoodsController::class, 'filterList']); // 类型(用于下拉框选择)

    /**
     * 预约
     */
    Route::get('/reservation/reservationNodeList', [Wechat\ReservationController::class, 'reservationNodeList']); // 预约种类列表
    Route::get('/reservation/reservationHallList', [Wechat\ReservationController::class, 'reservationHallList']); // 列表（每个种类都返回几个）
    Route::get('/reservation/list', [Wechat\ReservationController::class, 'lists']); //  预约列表
    //  Route::get('/reservation/detail', [Wechat\ReservationController::class, 'detail']); //  预约详情

    /**
     * 空间预约
     */
    Route::get('/studyRoomReservation/list', [Wechat\StudyRoomReservationController::class, 'lists']); //  空间预约列表

    /**
     * 志愿者服务
     */
    Route::get('/volunteer/volunteerPositionFilterList', [Wechat\VolunteerController::class, 'volunteerPositionFilterList']); // 服务岗位列表(用于下拉框选择)
    Route::get('/volunteer/getDegreeFilterList', [Wechat\VolunteerController::class, 'getDegreeFilterList']); // 教育程度(用于下拉框选择)
    Route::get('/volunteer/volunteerApplyParam', [Wechat\VolunteerController::class, 'volunteerApplyParam']); //  志愿者报名参数(用于下拉框选择)

    /**
     * 线上办证
     */
    Route::post('/onlineRegistration/sendVerify', [Wechat\OnlineRegistrationController::class, 'sendVerify']); // 发送验证码
    Route::get('/onlineRegistration/getOnlineParam', [Wechat\OnlineRegistrationController::class, 'getOnlineParam']); // 获取下拉选项参数
    Route::get('/onlineRegistration/getDynamicParam', [Wechat\OnlineRegistrationController::class, 'getDynamicParam']); // 获取动态参数

    Route::get('/onlineRegistration/getCardType', [Wechat\OnlineRegistrationController::class, 'getCardType']); //获取可办证的类型

    /**
     * 景点打卡
     */
    Route::get('/scenic/typeList', [Wechat\ScenicController::class, 'typeList']); // 书店筛选列表
    Route::get('/scenic/list', [Wechat\ScenicController::class, 'lists']); // 景点列表
    Route::get('/scenic/detail', [Wechat\ScenicController::class, 'detail']); // 景点详情

    /**
     * 图书到家（新书书籍）
     */
    Route::get('/newBookRecom/shopFiltrateList', [Wechat\NewBookRecomController::class, 'shopFiltrateList']); // 书店筛选列表
    Route::get('/newBookRecom/bookTypeFiltrateList', [Wechat\NewBookRecomController::class, 'bookTypeFiltrateList']); // 新书类型筛选列表

    /**
     * 图书到家（新书、馆藏书记录管理）
     */
    Route::get('/bookHomeBorrow/postageList', [Wechat\BookHomeBorrowController::class, 'postageList']); // 图书馆设置的运费详情(书店及图书馆)
    Route::get('/bookHomeBorrow/hotSearchList', [Wechat\BookHomeBorrowController::class, 'hotSearchList']); // 检索热词列表
    Route::get('/bookHomeBorrow/getCourierName', [Wechat\BookHomeBorrowController::class, 'getCourierName']); // 获取快递公司名称
    Route::get('/bookHomeBorrow/returnAddress', [Wechat\BookHomeBorrowController::class, 'returnAddress']); // 图书到家归还地址管理


    /**
     * 图书到家（馆藏书籍）
     */
    Route::get('/libBookRecom/bookTypeFiltrateList', [Wechat\LibBookRecomController::class, 'bookTypeFiltrateList']); // 馆藏书类型筛选列表
    Route::get('/libBookRecom/libBookList', [Wechat\LibBookRecomController::class, 'libBookList']); // 馆藏书检索


    /**
     * 服务指南
     */
    Route::get('/serviceDirectory/list', [Wechat\ServiceDirectoryController::class, 'lists']); //服务指南详情
    Route::get('/serviceDirectory/detail', [Wechat\ServiceDirectoryController::class, 'detail']); //服务指南详情


    /**
     * 室内导航管理
     */
    Route::get('/navigation/buildList', [Wechat\NavigationController::class, 'buildList']); //建筑列表
    Route::get('/navigation/areaFilterList', [Wechat\NavigationController::class, 'areaFilterList']); //区域简单列表
    Route::get('/navigation/areaList', [Wechat\NavigationController::class, 'areaList']); //区域列表
    Route::get('/navigation/pointList', [Wechat\NavigationController::class, 'pointList']); //查询指定区域点位列表
    Route::get('/navigation/pointSearchList', [Wechat\NavigationController::class, 'pointSearchList']); //点位搜索接口
    Route::get('/navigation/navigationLineList', [Wechat\NavigationController::class, 'navigationLineList']); //导航线路搜索



    /**
     * 征集活动数据库
     */
    Route::get('/competiteActivityDatabase/list', [Wechat\CompetiteActivityDatabaseController::class, 'lists']); //列表
    Route::get('/competiteActivityDatabase/detail', [Wechat\CompetiteActivityDatabaseController::class, 'detail']); //详情
    Route::get('/competiteActivityDatabase/worksList', [Wechat\CompetiteActivityDatabaseController::class, 'worksList']); //数据库作品列表
    Route::get('/competiteActivityDatabase/ebookDetail', [Wechat\CompetiteActivityDatabaseController::class, 'ebookDetail']); //电子书详情+电子书作品列表


});

//token必选
Route::middleware(['check.wechat.token'])->prefix('wechat')->group(function () {

    /**
     * 个人中心
     */
    Route::get('/userInfo/index', [Wechat\UserInfoController::class, 'index']); // 个人中心首页


    //扫描二维码
    Route::post('/scanQr/scanAffirm', [ScanQrController::class, 'scanAffirm']); //扫描二维码

    //扫描二维码
    Route::post('/userInfo/wechatInfoChange', [Wechat\UserInfoController::class, 'wechatInfoChange']); //修改头像 和 昵称


    //新书荐购
    Route::post('/recommendBook/newBookRecom', [Wechat\RecommendBookController::class, 'newBookRecom']); //新书推荐
    Route::get('/recommendBook/myRecomList', [Wechat\RecommendBookController::class, 'myRecomList']); //我的荐购历史
    Route::post('/recommendBook/getBookInfo', [Wechat\RecommendBookController::class, 'getBookInfo']); //扫码关联其他书籍信息

    //用户留言管理
    Route::post('/feedback/add', [Wechat\FeedbackController::class, 'addFeedback']); //用户留言

    /**
     * 问卷调查
     */
    Route::get('/survey/list', [Wechat\SurveyController::class, 'lists']); // 列表
    Route::get('/survey/detail', [Wechat\SurveyController::class, 'detail']); // 详情
    Route::post('/survey/reply', [Wechat\SurveyController::class, 'reply']); // 回复

    /**
     * 活动管理
     */
    Route::get('/activity/myActList', [Wechat\ActivityController::class, 'myActList']); // 我的活动列表
    Route::post('/activity/apply', [Wechat\ActivityController::class, 'apply']); // 活动报名
    Route::post('/activity/cancel', [Wechat\ActivityController::class, 'cancel']); // 取消活动报名
    Route::get('/activity/getApplyQr', [Wechat\ActivityController::class, 'getApplyQr']); // 出示二维码


    /**
     * 根据商品类型  获取商品列表
     */
    Route::post('/goods/goodsCancelConversion', [Wechat\GoodsController::class, 'goodsCancelConversion']); // 商品取消兑换
    Route::post('/goods/goodsCancelOrder', [Wechat\GoodsController::class, 'goodsCancelOrder']); // 商品取消订单
    Route::get('/goods/orderList', [Wechat\GoodsController::class, 'orderList']); // 我的兑换列表
    Route::get('/goods/orderDetail', [Wechat\GoodsController::class, 'orderDetail']); // 我的兑换详情

    /**
     * 用户地址管理
     */
    Route::get('/userAddress/list', [Wechat\UserAddressController::class, 'lists']); // 列表
    Route::get('/userAddress/detail', [Wechat\UserAddressController::class, 'detail']); // 详情
    Route::post('/userAddress/add', [Wechat\UserAddressController::class, 'add']); // 新增
    Route::post('/userAddress/change', [Wechat\UserAddressController::class, 'change']); // 编辑
    Route::post('/userAddress/del', [Wechat\UserAddressController::class, 'del']); // 删除
    Route::post('/userAddress/switchoverDefault', [Wechat\UserAddressController::class, 'switchoverDefault']); // 修改默认状态


    /**
     * 系统消息
     */
    // Route::get('/systemInfo/list', [Wechat\SystemInfoController::class, 'lists']); // 列表
    // Route::get('/systemInfo/detail', [Wechat\SystemInfoController::class, 'detail']); // 详情

    /*用户获取消息列表*/
    Route::get('/system/systemList', [Wechat\SystemInfoController::class, 'systemList']); // 列表
    Route::get('/system/systemMessage', [Wechat\SystemInfoController::class, 'systemMessage']); //获取用户分类消息列表


    /**
     * 个人中心
     */
    Route::get('/userInfo/scoreList', [Wechat\UserInfoController::class, 'scoreList']); // 获取用户积分明细

    /**
     * 预约
     */
    Route::get('/reservation/seatList', [Wechat\ReservationController::class, 'seatList']); //  座位列表
    Route::post('/reservation/makeInfo', [Wechat\ReservationController::class, 'makeInfo']); // 文化配送 预约时间段
    Route::post('/reservation/cancelMake', [Wechat\ReservationController::class, 'cancelMake']); // 用户取消预约
    Route::get('/reservation/myMakeList', [Wechat\ReservationController::class, 'myMakeList']); //  我的预约列表
    Route::get('/reservation/getApplyQr', [Wechat\ReservationController::class, 'getApplyQr']); //  出示二维码

    /**
     * 空间预约
     */
    Route::post('/studyRoomReservation/makeInfo', [Wechat\StudyRoomReservationController::class, 'makeInfo']); // 文化配送 预约时间段
    Route::post('/studyRoomReservation/cancelMake', [Wechat\StudyRoomReservationController::class, 'cancelMake']); // 用户取消预约
    Route::get('/studyRoomReservation/myMakeList', [Wechat\StudyRoomReservationController::class, 'myMakeList']); //  我的预约列表
    Route::get('/studyRoomReservation/getApplyQr', [Wechat\StudyRoomReservationController::class, 'getApplyQr']); //  出示二维码

    /**
     * 年度账单
     */
    Route::get('/index/getYearBill', [Wechat\IndexController::class, 'getYearBill']); // 年度账单

    /**
     * 新闻
     */
    Route::post('/news/collectAndCancel', [Wechat\NewsController::class, 'collectAndCancel']); // 新闻收藏与取消收藏
    Route::get('/news/collectList', [Wechat\NewsController::class, 'collectList']); // 收藏新闻列表

    /**
     * 在线办证
     */
    Route::post('/onlineRegistration/registration', [Wechat\OnlineRegistrationController::class, 'registration']); // 办证
    Route::get('/onlineRegistration/historyList', [Wechat\OnlineRegistrationController::class, 'historyList']); // 办证历史记录
    Route::get('/onlineRegistration/detail', [Wechat\OnlineRegistrationController::class, 'detail']); // 办证详情信息


    /**
     * 景点打卡作品管理
     */
    Route::post('/scenicWorks/add', [Wechat\ScenicWorksController::class, 'add']); // 投稿
    Route::post('/scenicWorks/change', [Wechat\ScenicWorksController::class, 'change']); // 修改作品
    Route::post('/scenicWorks/worksVote', [Wechat\ScenicWorksController::class, 'worksVote']); // 打卡作品点赞与取消点赞
    Route::post('/scenicWorks/worksCancelAndDel', [Wechat\ScenicWorksController::class, 'worksCancelAndDel']); // 撤销、删除作品


    /**
     * 志愿者管理
     */
    Route::get('/volunteer/getVolunteerNotice', [Wechat\VolunteerController::class, 'getVolunteerNotice']); // 获取志愿者须知
    Route::get('/volunteer/getPullFilterList', [Wechat\VolunteerController::class, 'getPullFilterList']); // 参加志愿者所需下拉选项(用于下拉框选择)
    Route::get('/volunteer/volunteerApplyParam', [Wechat\VolunteerController::class, 'volunteerApplyParam']); // 志愿者报名参数(对应后台选择的参数)
    Route::get('/volunteer/volunteerDetail', [Wechat\VolunteerController::class, 'volunteerDetail']); // 志愿者详情
    Route::post('/volunteer/volunteerApply', [Wechat\VolunteerController::class, 'volunteerApply']); // 添加、修改或过期后重新提交用户志愿者信息  （修改后，自动提交到后台审核）
    Route::post('/volunteer/volunteerCancel', [Wechat\VolunteerController::class, 'volunteerCancel']); //  撤销志愿者


    /**
     * 图书到家（新书书籍）
     */
    Route::get('/newBookRecom/myNewBookCollectList', [Wechat\NewBookRecomController::class, 'myNewBookCollectList']); // 我的新书收藏列表
    Route::post('/newBookRecom/newBookSchoolbag', [Wechat\NewBookRecomController::class, 'newBookSchoolbag']); // 新书加入书袋与移出书袋
    Route::get('/newBookRecom/myNewBookSchoolbagList', [Wechat\NewBookRecomController::class, 'myNewBookSchoolbagList']); // 我的新书书袋列表
    Route::post('/newBookRecom/getOrderForecastPrice', [Wechat\NewBookRecomController::class, 'getOrderForecastPrice']); // 获取订单预估金额
    Route::post('/newBookRecom/getOrderInfo', [Wechat\NewBookRecomController::class, 'getOrderInfo']); // 获取预支付订单信息
    Route::get('/newBookRecom/myOrderList', [Wechat\NewBookRecomController::class, 'myOrderList']); // 我的 新书 订单列表
    Route::get('/newBookRecom/myOrderDetail', [Wechat\NewBookRecomController::class, 'myOrderDetail']); // 我的新书订单详情
    /**
     * 图书到家（新书、馆藏书记录管理）
     */
    Route::get('/bookHomeBorrow/borrowList', [Wechat\BookHomeBorrowController::class, 'borrowList']); // 采购借阅记录
    Route::get('/bookHomeBorrow/getNowBorrowList', [Wechat\BookHomeBorrowController::class, 'getNowBorrowList']); // 线上邮递书籍归还（获取当前借阅记录 ，包括新书或馆藏书）
    Route::post('/bookHomeBorrow/bookCollect', [Wechat\BookHomeBorrowController::class, 'bookCollect']); //  新书与馆藏书收藏与取消收藏
    Route::post('/bookHomeBorrow/recomReturn', [Wechat\BookHomeBorrowController::class, 'recomReturn']); //  书籍归还提交(包括新书和馆藏书)
    Route::post('/bookHomeBorrow/bookCancel', [Wechat\BookHomeBorrowController::class, 'bookCancel']); //  用户撤销线上书籍归还记录


    /**
     * 图书到家（馆藏书籍）
     */
    Route::get('/libBookRecom/myLibraryBookCollectList', [Wechat\LibBookRecomController::class, 'myLibraryBookCollectList']); // 我的馆藏书收藏列表
    Route::post('/libBookRecom/LibBookSchoolbag', [Wechat\LibBookRecomController::class, 'LibBookSchoolbag']); // 馆藏书加入书袋与移出书袋
    Route::get('/libBookRecom/myLibBookSchoolbagList', [Wechat\LibBookRecomController::class, 'myLibBookSchoolbagList']); // 我的馆藏书书袋列表


    /**
     * 新书推荐（独立功能）
     */
    Route::post('/newBookRecommend/newBookRecommendCollect', [Wechat\NewBookRecommendController::class, 'newBookRecommendCollect']); //  新书收藏与取消收藏
    Route::get('/newBookRecommend/myNewBookRecommendCollectList', [Wechat\NewBookRecommendController::class, 'myNewBookRecommendCollectList']); // 我的新书（推荐书）收藏列表



      /**
   * 线上大赛作品指定打分人
   */
  Route::get('/competiteActivityWorksAppointScore/filterList', [Admin\CompetiteActivityWorksAppointScoreController::class, 'filterList']); //根据打分人获取所有的作品id，或名字  简单列表
  Route::get('/competiteActivityWorksAppointScore/list', [Admin\CompetiteActivityWorksAppointScoreController::class, 'lists']); //  列表
  Route::get('/competiteActivityWorksAppointScore/getScoreWorks', [Admin\CompetiteActivityWorksAppointScoreController::class, 'getScoreWorks']); // 根据打分人获取所有的作品id，或名字 分页列表
  Route::post('/competiteActivityWorksAppointScore/modify', [Admin\CompetiteActivityWorksAppointScoreController::class, 'modify']); // 添加、修改打分作品
  Route::post('/competiteActivityWorksAppointScore/del', [Admin\CompetiteActivityWorksAppointScoreController::class, 'del']); // 删除打分作品



    /**
     * 阅读任务
     */
    Route::get('/readingTask/list', [Wechat\ReadingTaskController::class, 'lists']); // 阅读任务列表(我的阅读任务)
    Route::get('/readingTask/detail', [Wechat\ReadingTaskController::class, 'detail']); //阅读任务详情
    Route::get('/readingTask/worksList', [Wechat\ReadingTaskController::class, 'worksList']); //阅读任务数据库作品列表
    Route::get('/readingTask/ebookDetail', [Wechat\ReadingTaskController::class, 'ebookDetail']); //阅读任务 电子书详情+电子书作品列表
    Route::post('/readingTask/readingTaskReading', [Wechat\ReadingTaskController::class, 'readingTaskReading']); //用户阅读任务记录表


});

//token可选
Route::middleware(['check.may.wechat.token'])->prefix('wechat')->group(function () {

    //在线办证（行为分析时需要token，所以加在这里）
    Route::get('/onlineRegistration/getCertificateType', [Wechat\OnlineRegistrationController::class, 'getCertificateType']); //获取证件类型
    //馆藏检索（行为分析时需要token，所以加在这里）
    Route::get('/libInfo/hotLibSearchList', [Wechat\LibInfoController::class, 'hotLibSearchList']); // 馆藏检索热词列表


    //首页接口
    Route::get('/index/index', [Wechat\IndexController::class, 'index']); //首页

    /**
     * 活动管理
     */
    Route::get('/activity/detail', [Wechat\ActivityController::class, 'detail']); // 活动详情(我的活动详情)
    /**
     * 根据商品类型  获取商品列表
     */
    Route::get('/goods/list', [Wechat\GoodsController::class, 'lists']); // 列表
    Route::get('/goods/detail', [Wechat\GoodsController::class, 'detail']); // 详情


    /**
     * 预约
     */
    Route::get('/reservation/detail', [Wechat\ReservationController::class, 'detail']); //  预约详情

    /**
     * 空间预约
     */
    Route::get('/studyRoomReservation/detail', [Wechat\StudyRoomReservationController::class, 'detail']); //  预约详情

    /**
     * 新闻
     */
    Route::get('/news/detail', [Wechat\NewsController::class, 'detail']); //新闻详情

    /**
     * 景点打卡作品管理
     */
    Route::get('/scenicWorks/list', [Wechat\ScenicWorksController::class, 'lists']); // 作品列表
    Route::get('/scenicWorks/detail', [Wechat\ScenicWorksController::class, 'detail']); // 作品详情


    /**
     * 图书到家
     */
    Route::get('/bookHomeBorrow/index', [Wechat\BookHomeBorrowController::class, 'index']); // 图书到家主界面数据
    /**
     * 图书到家（新书书籍）
     */
    Route::get('/newBookRecom/newBookList', [Wechat\NewBookRecomController::class, 'newBookList']); // 新书列表
    Route::get('/newBookRecom/newBookDetail', [Wechat\NewBookRecomController::class, 'newBookDetail']); // 新书详情

    /**
     * 图书到家（馆藏书籍）
     */
    Route::get('/libBookRecom/getRecomList', [Wechat\LibBookRecomController::class, 'getRecomList']); //  馆藏书推荐列表（推荐书的接口）
    Route::get('/libBookRecom/getLibBookDetail', [Wechat\LibBookRecomController::class, 'getLibBookDetail']); // 馆藏书类型筛选列表

    /**
     * 新书推荐（独立功能）
     */
    Route::get('/newBookRecommend/typeList', [Wechat\NewBookRecommendController::class, 'typeList']); //  类型列表
    Route::get('/newBookRecommend/list', [Wechat\NewBookRecommendController::class, 'lists']); //  列表
    Route::get('/newBookRecommend/detail', [Wechat\NewBookRecommendController::class, 'detail']); // 详情


    /**
     * 码上借
     */
    Route::get('/codeOnBorrow/getCodeInfo', [Wechat\CodeOnBorrowController::class, 'getCodeInfo']); // 扫码借，扫描场所码后，获取场所信息，验证场所数据
    Route::get('/codeOnBorrow/getBookInfoByBarcode', [Wechat\CodeOnBorrowController::class, 'getBookInfoByBarcode']); //  扫码借书，还书，获取书籍信息
    Route::post('/codeOnBorrow/returnBook', [Wechat\CodeOnBorrowController::class, 'returnBook']); //  归还书籍

    Route::get('/codeOnBorrow/borrowList', [Wechat\CodeOnBorrowController::class, 'borrowList']); //  借阅操作列表
    Route::get('/codeOnBorrow/returnList', [Wechat\CodeOnBorrowController::class, 'returnList']); //  归还操作列表

    /**
     * 扫码导游 - 作品管理
     */
    Route::get('/codeGuideProduction/exhibitionList', [Wechat\CodeGuideProductionController::class, 'exhibitionList']); // 展览列表
    Route::get('/codeGuideProduction/exhibitionDetail', [Wechat\CodeGuideProductionController::class, 'exhibitionDetail']); // 展览详情
    Route::get('/codeGuideProduction/list', [Wechat\CodeGuideProductionController::class, 'lists']); // 作品列表
    Route::get('/codeGuideProduction/detail', [Wechat\CodeGuideProductionController::class, 'detail']); // 作品详情


    /**
     * 线上大赛
     */
    Route::get('/competiteActivity/productionList', [Wechat\CompetiteActivityController::class, 'productionList']); // 参赛作品列表


});


//必须绑定读者证
Route::middleware(['check.bind.reader.token'])->prefix('wechat')->group(function () {

    /**
     * 根据商品类型  获取商品列表
     */
    Route::post('/goods/goodsConversion', [Wechat\GoodsController::class, 'goodsConversion']); // 商品兑换 生成预支付订单


    Route::post('/libInfo/unBindReaderId', [Wechat\LibInfoController::class, 'unBindReaderId']); // 解除绑定读者证
    Route::post('/libInfo/changePwd', [Wechat\LibInfoController::class, 'changePwd']); // 修改读者证密码
    Route::post('/libInfo/cardDelay', [Wechat\LibInfoController::class, 'cardDelay']); // 读者证延期
    Route::post('/libInfo/lost', [Wechat\LibInfoController::class, 'lost']); // 读者证挂失操作
    Route::post('/libInfo/lostRecover', [Wechat\LibInfoController::class, 'lostRecover']); // 读者证恢复

    Route::get('/libInfo/getBorrowList', [Wechat\LibInfoController::class, 'getBorrowList']); // 获取借阅记录列表
    Route::post('/libInfo/renew', [Wechat\LibInfoController::class, 'renewBook']); // 在线续借

    /**
     * 积分签到
     */
    Route::get('/signScore/signScoreInfo', [Wechat\SignScoreController::class, 'signScoreInfo']); // 获取积分签到信息
    Route::post('/signScore/sign', [Wechat\SignScoreController::class, 'sign']); // 用户打卡签到

    /**
     * 欠费缴纳
     */
    Route::get('/oweProcess/oweList', [Wechat\OweProcessController::class, 'oweList']); // 欠费列表
    Route::post('/oweProcess/owePay', [Wechat\OweProcessController::class, 'owePay']); // 欠费支付 生成预支付订单

    /**
     * 图书到家（馆藏书籍）
     */
    Route::post('/libBookRecom/getOrderForecastPrice', [Wechat\LibBookRecomController::class, 'getOrderForecastPrice']); // 获取订单预估金额
    Route::post('/libBookRecom/getOrderInfo', [Wechat\LibBookRecomController::class, 'getOrderInfo']); // 获取预支付订单信息
    Route::get('/libBookRecom/myOrderList', [Wechat\LibBookRecomController::class, 'myOrderList']); // 我的 馆藏书 订单列表
    Route::get('/libBookRecom/myOrderDetail', [Wechat\LibBookRecomController::class, 'myOrderDetail']); // 我的馆藏书订单详情
    Route::post('/libBookRecom/bookRenew', [Wechat\LibBookRecomController::class, 'bookRenew']); // 馆藏书续借功能

    Route::post('/bookHomeBorrow/cancelBookHomeOrder', [Wechat\BookHomeBorrowController::class, 'cancelBookHomeOrder']); // 取消图书到家订单


    /**
     * 码上借
     */
    Route::post('/codeOnBorrow/borrowBook', [Wechat\CodeOnBorrowController::class, 'borrowBook']); //  借阅书籍

});


######################################################抽奖活动start#############################################################

Route::middleware(['limit.access.data'])->prefix('wechat')->group(function () {
    /**
     * 答题活动管理
     */
    Route::get('/answerActivity/typeList', [Wechat\AnswerActivityController::class, 'typeList']); //  活动类型列表
    Route::get('/answerActivity/list', [Wechat\AnswerActivityController::class, 'lists']); //  根据活动类型获取活动列表
    /**
     * 在线抽奖活动管理
     */
    Route::get('/turnActivity/list', [Wechat\TurnActivityController::class, 'lists']); //  根据活动类型获取活动列表
    /**
     *  活动区域管理
     */
    Route::get('/answerActivityArea/list', [Wechat\AnswerActivityAreaController::class, 'lists']); //  列表

    /**
     * 抽奖活动推荐记录
     */
    Route::get('/drawActivity/getRecomDrawActivity', [Wechat\DrawActivityController::class, 'getRecomDrawActivity']); // 抽奖活动推荐列表(3种活动)

    /**
     * 获取资源路径及颜色等信息
     */
    Route::get('/answerActivity/getResourceInfo', [Wechat\AnswerActivityController::class, 'getResourceInfo']); // 获取资源路径及颜色等信息

    /**
     * 获取资源路径及颜色等信息
     */
    Route::get('/turnActivity/getResourceInfo', [Wechat\TurnActivityController::class, 'getResourceInfo']); // 获取资源路径及颜色等信息

});

//token必选
Route::middleware(['check.wechat.token', 'limit.access.data'])->prefix('wechat')->group(function () {

    Route::post('/libInfo/bingReaderId', [Wechat\LibInfoController::class, 'bingReaderId']); // 绑定读者证  或 密码错误重新登录


    //分享活动答题机会
    Route::post('/userDrawShare/index', [Wechat\UserDrawShareController::class, 'index']); //分享活动答题机会

    Route::post('/answerActivity/scanActInfo', [Wechat\AnswerActivityController::class, 'scanActInfo']); //扫码后获取活动信息

    /**
     * 答题活动详情
     */
    Route::get('/answerActivity/detail', [Wechat\AnswerActivityController::class, 'detail']); //获取答题活动详情 （根据类型不同，调用不同的接口  act_type_id 为 1 调用此接口）
    Route::get('/answerActivity/getStairsFloor', [Wechat\AnswerActivityController::class, 'getStairsFloor']); //爬楼梯模式获取当前楼层


    Route::get('/userDrawAddress/getUserAddress', [Wechat\UserDrawAddressController::class, 'getUserAddress']); //  获取用户地址信息
    Route::post('/userDrawAddress/setUserAddress', [Wechat\UserDrawAddressController::class, 'setUserAddress']); //  设置用户地址信息

    Route::get('/userDrawInvite/checkInvitCode', [Wechat\UserDrawInviteController::class, 'checkInvitCode']); //  是否需要邀请码 且 是否已经输入了邀请码
    Route::post('/userDrawInvite/setInvitCode', [Wechat\UserDrawInviteController::class, 'setInvitCode']); //   输入邀请码

    Route::get('/userDrawUnit/checkRealInfo', [Wechat\UserDrawUnitController::class, 'checkRealInfo']); //  是否需要填写用户信息和单位信息 且 是否已经输入了用户信息
    Route::post('/userDrawUnit/setRealInfo', [Wechat\UserDrawUnitController::class, 'setRealInfo']); //   输入用户信息 或 所属图书馆

    /**
     *  答题活动单位管理
     */
    Route::get('/answerActivityUnit/list', [Wechat\AnswerActivityUnitController::class, 'lists']); //  列表
    Route::get('/answerActivityUnit/detail', [Wechat\AnswerActivityUnitController::class, 'detail']); //  详情
    /**
     * 答题活动答题管理
     */
    Route::get('/answerActivityAnswer/getAnswerProblem', [Wechat\AnswerActivityAnswerController::class, 'getAnswerProblem']); //开始答题（获取题目信息）
    Route::post('/answerActivityAnswer/replyProblem', [Wechat\AnswerActivityAnswerController::class, 'replyProblem']); //用户答题
    /**
     * 答题活动排名
     */
    Route::get('/answerActivityRanking/ranking', [Wechat\AnswerActivityRankingController::class, 'ranking']); //活动排名
    Route::get('/answerActivityRanking/myGrade', [Wechat\AnswerActivityRankingController::class, 'myGrade']); //我的成绩单

    /**
     * 答题活动礼物公示管理
     */
    Route::get('/answerActivityGiftPublic/list', [Wechat\AnswerActivityGiftPublicController::class, 'lists']); //列表

    /**
     * 线上大赛
     */
    Route::get('/contestActivity/myContestLists', [Wechat\ContestActivityController::class, 'myContestLists']); // 我的大赛列表
    Route::get('/contestActivity/getTypeList', [Wechat\ContestActivityController::class, 'getTypeList']); // 线上大赛类型
    Route::get('/contestActivity/productionList', [Wechat\ContestActivityController::class, 'productionList']); // 参赛作品列表
    Route::get('/contestActivity/myProductionList', [Wechat\ContestActivityController::class, 'myProductionList']); // 我的参赛作品列表
    Route::get('/contestActivity/getWorksUserInfo', [Wechat\ContestActivityController::class, 'getWorksUserInfo']); // 根据电话号码获取用户基本信息
    Route::post('/contestActivity/worksVote', [Wechat\ContestActivityController::class, 'worksVote']); // 作品投票
    Route::post('/contestActivity/worksCancelAndDel', [Wechat\ContestActivityController::class, 'worksCancelAndDel']); // 撤销、删除作品
    Route::post('/contestActivity/worksAdd', [Wechat\ContestActivityController::class, 'worksAdd']); // 上传作品
    Route::post('/contestActivity/worksChange', [Wechat\ContestActivityController::class, 'worksChange']); // 修改作品


    /**
     * 征集活动
     */
    Route::get('/competiteActivity/myCompetiteLists', [Wechat\CompetiteActivityController::class, 'myCompetiteLists']); // 我的大赛列表
    Route::get('/competiteActivity/getTypeList', [Wechat\CompetiteActivityController::class, 'getTypeList']); // 线上大赛类型
    Route::get('/competiteActivity/myProductionList', [Wechat\CompetiteActivityController::class, 'myProductionList']); // 我的参赛作品列表
    Route::get('/competiteActivity/getWorksUserInfo', [Wechat\CompetiteActivityController::class, 'getWorksUserInfo']); // 根据电话号码获取用户基本信息
    Route::post('/competiteActivity/worksVote', [Wechat\CompetiteActivityController::class, 'worksVote']); // 作品投票
    Route::post('/competiteActivity/worksCancelAndDel', [Wechat\CompetiteActivityController::class, 'worksCancelAndDel']); // 撤销、删除作品
    Route::post('/competiteActivity/worksAdd', [Wechat\CompetiteActivityController::class, 'worksAdd']); // 上传作品
    Route::post('/competiteActivity/worksChange', [Wechat\CompetiteActivityController::class, 'worksChange']); // 修改作品


    /**
     * 答题活动抽奖
     */
    Route::post('/answerActivityDraw/draw', [Wechat\AnswerActivityDrawController::class, 'draw']); // 抽奖
    Route::post('/answerActivityDraw/getDrawResult', [Wechat\AnswerActivityDrawController::class, 'getDrawResult']); // 获取抽奖结果

    Route::get('/answerActivityUserGift/list', [Wechat\AnswerActivityUserGiftController::class, 'lists']); // 获取用户礼物列表


    /**
     * 在线抽奖活动详情
     */
    Route::get('/turnActivity/detail', [Wechat\TurnActivityController::class, 'detail']); //获取在线抽奖活动详情 （根据类型不同，调用不同的接口  act_type_id 为 2 调用此接口）

    /**
     * 在线抽奖活动抽奖
     */
    Route::post('/turnActivityDraw/draw', [Wechat\TurnActivityDrawController::class, 'draw']); // 抽奖
    Route::post('/turnActivityDraw/getDrawResult', [Wechat\TurnActivityDrawController::class, 'getDrawResult']); // 获取抽奖结果

    Route::get('/turnActivityUserGift/list', [Wechat\TurnActivityUserGiftController::class, 'lists']); // 获取用户礼物列表
});

//token可选
Route::middleware(['check.may.wechat.token'])->prefix('wechat')->group(function () {
    /**
     * 线上大赛
     */
    Route::get('/contestActivity/lists', [Wechat\ContestActivityController::class, 'lists']); // 列表
    Route::get('/contestActivity/detail', [Wechat\ContestActivityController::class, 'detail']); // 详情
    Route::get('/contestActivity/getLibName', [Wechat\ContestActivityController::class, 'getLibName']); // 获取图书馆列表（上传作品专用）
    Route::get('/contestActivity/productionDetail', [Wechat\ContestActivityController::class, 'productionDetail']); // 参赛作品详情+我的作品详情


    /**
     * 征集活动
     */
    Route::get('/competiteActivity/lists', [Wechat\CompetiteActivityController::class, 'lists']); // 列表
    Route::get('/competiteActivity/detail', [Wechat\CompetiteActivityController::class, 'detail']); // 详情
    Route::get('/competiteActivity/productionDetail', [Wechat\CompetiteActivityController::class, 'productionDetail']); // 参赛作品详情+我的作品详情


});

######################################################抽奖活动end#############################################################


/******************************************************图片直播start*************************************************************** */
//token必选
Route::middleware(['check.wechat.token'])->prefix('wechat')->group(function () {

    /**
     * 图片直播管理
     */

    Route::get('/pictureLive/detail', [Wechat\PictureLiveController::class, 'detail']); // 获取直播活动详情 （根据类型不同，调用不同的接口  act_type_id 为 4 调用此接口）

    /**
     * 图片直播作品管理
     */
    Route::get('/pictureLiveWorks/list', [Wechat\PictureLiveWorksController::class, 'lists']); // 获取图片列表
    Route::get('/pictureLiveWorks/detail', [Wechat\PictureLiveWorksController::class, 'detail']); // 获取图片详情
    Route::post('/pictureLiveWorks/addBrowseNumber', [Wechat\PictureLiveWorksController::class, 'addBrowseNumber']); // 添加图片浏览量

    Route::get('/pictureLiveWorks/surplusUploadsNumber', [Wechat\PictureLiveWorksController::class, 'surplusUploadsNumber']); // 获取剩余可上传图片张数
    Route::post('/pictureLiveWorks/add', [Wechat\PictureLiveWorksController::class, 'add']); // 图片直播，上传图片

    Route::post('/pictureLiveWorks/worksCancelAndDel', [Wechat\PictureLiveWorksController::class, 'worksCancelAndDel']); //  撤销、删除、重新上传作品
    Route::post('/pictureLiveWorks/worksVote', [Wechat\PictureLiveWorksController::class, 'worksVote']); //  作品投票


    /**
     * 直播图片个人中心
     */
    Route::get('/pictureLiveWorks/myBrowseLists', [Wechat\PictureLivePersonalCenterController::class, 'myBrowseLists']); //  我浏览过的活动列表
    Route::get('/pictureLiveWorks/myPictureLiveLists', [Wechat\PictureLivePersonalCenterController::class, 'myPictureLiveLists']); //  我的图片直播活动列表及图片
    Route::get('/pictureLiveWorks/myPictureLivePhotoLists', [Wechat\PictureLivePersonalCenterController::class, 'myPictureLivePhotoLists']); //  我的图片列表
    Route::get('/pictureLiveWorks/myPictureLiveDetail', [Wechat\PictureLivePersonalCenterController::class, 'myPictureLiveDetail']); //  我的图片详情


    Route::get('/pictureLiveWorks/myVotePictureLiveLists', [Wechat\PictureLivePersonalCenterController::class, 'myVotePictureLiveLists']); //  我的点赞过的直播活动列表及图片
    Route::get('/pictureLiveWorks/myVotePictureLivePhotoLists', [Wechat\PictureLivePersonalCenterController::class, 'myVotePictureLivePhotoLists']); //   我点赞过的图片列表

});
//token可选
Route::middleware(['check.may.wechat.token'])->prefix('wechat')->group(function () {

    Route::get('/pictureLive/list', [Wechat\PictureLiveController::class, 'lists']); // 直播活动列表 （根据类型不同，调用不同的接口  act_type_id 为 4 调用此接口）
    Route::get('/pictureLive/liveLists', [Wechat\PictureLiveController::class, 'liveLists']); // 直播活动（图片+视频）列表 （根据类型不同，调用不同的接口  act_type_id 为 4 调用此接口）

});

/******************************************************图片直播end*************************************************************** */
/******************************************************视频直播end*************************************************************** */
//token可选
Route::middleware(['check.may.wechat.token'])->prefix('wechat')->group(function () {

    Route::get('/videoLive/list', [Wechat\VideoLiveController::class, 'lists']); // 视频直播活动列表 （根据类型不同，调用不同的接口  act_type_id 为 5 调用此接口）
    Route::get('/videoLive/detail', [Wechat\VideoLiveController::class, 'detail']); // 视频直播活动详情 （根据类型不同，调用不同的接口  act_type_id 为 5 调用此接口）
    Route::get('/videoLive/getVideoLiveStatus', [Wechat\VideoLiveController::class, 'getVideoLiveStatus']); // 获取直播状态

});
//token必选
Route::middleware(['check.wechat.token'])->prefix('wechat')->group(function () {

    Route::post('/videoLive/voteAndCancel', [Wechat\VideoLiveController::class, 'voteAndCancel']); //  点赞与取消点赞


});

/******************************************************视频直播end*************************************************************** */
