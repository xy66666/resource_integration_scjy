<?php

use App\Http\Controllers\Admin;
use App\Http\Controllers\Live;
use App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\VideoPlayController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('videoPlay/play', [VideoPlayController::class, 'play']); // 播放视频资源接口


// 后台接口

//后台管理员登录
Route::post('admin/login/login', [Admin\LoginController::class, 'login']);
Route::get('admin/login/getCaptcha', [Admin\LoginController::class, 'getCaptcha']); //获取图形验证码
Route::get('slideVerify/getCaptcha', [App\Http\Controllers\SlideVerifyController::class, 'getCaptcha']); //获取滑动验证码参数

Route::get('text/bookTypesFilterList', [App\Http\Controllers\TextController::class, 'bookTypesFilterList']); //书籍类型筛选列表(用于下拉框选择)

Route::get('text/getCityAll', [App\Http\Controllers\TextController::class, 'getCityAll']); //获取城市列表

//文件上传
Route::post('/uploads/files', [App\Http\Controllers\UploadsFileController::class, 'commonUpload']); //公共文件上传
Route::post('/uploads/ocrIdCardImg', [App\Http\Controllers\IdCardUploadsOcrController::class, 'ocrIdCardImg']); //身份证图片上传，然后进行ocr识别

Route::post('/pictureLiveUploads/files', [App\Http\Controllers\PictureLiveUploadsFileController::class, 'commonUpload']); //图片直播公共文件上传


/***直播 */
Route::post('live/liveVideoCallback/getVideo', [Live\LiveVideoCallbackController::class, 'getVideo']); //  视频录制回调地址（处理数据）(唯一)  必须是post格式
Route::post('live/liveVideoCallback/channelStatus', [Live\LiveVideoCallbackController::class, 'channelStatus']); // 频道状态变化回调地址（处理数据）(唯一)  必须是post格式


//异步发送推送数据
Route::post('admin/serviceNotice/asyncSend', [Admin\ServiceNoticeController::class, 'asyncSend']); // 异步发送数据
Route::get('admin/userInfo/asyncSend', [Admin\UserInfoController::class, 'asyncSend']); // 异步发送数据


/***直播 */

Route::middleware(['check.admin.header.token', 'check.admin.permission'])->prefix('admin')->group(function () {

    /**
     * 应用显示灰色
     */
    Route::get('/appViewGray/list', [Admin\AppViewGrayController::class, 'lists']); // 列表
    Route::get('/appViewGray/detail', [Admin\AppViewGrayController::class, 'detail']); // 详情
    Route::post('/appViewGray/add', [Admin\AppViewGrayController::class, 'add']); // 新增
    Route::post('/appViewGray/change', [Admin\AppViewGrayController::class, 'change']); // 编辑
    Route::post('/appViewGray/del', [Admin\AppViewGrayController::class, 'del']); // 删除


    /**
     * 协议规则
     */
    Route::get('/userProtocol/getProtocol', [Admin\UserProtocolController::class, 'getProtocol']); //获取用户协议
    Route::post('/userProtocol/setProtocol', [Admin\UserProtocolController::class, 'setProtocol']); //配置用户协议


    /**
     * 管理员管理
     */
    Route::get('/manage/filterList', [Admin\ManageController::class, 'filterList']);
    Route::get('/manage/list', [Admin\ManageController::class, 'lists']);
    Route::get('/manage/detail', [Admin\ManageController::class, 'detail']);
    Route::post('/manage/add', [Admin\ManageController::class, 'add']);
    Route::post('/manage/change', [Admin\ManageController::class, 'change']);
    Route::post('/manage/del', [Admin\ManageController::class, 'del']);
    Route::post('/manage/changePwd', [Admin\ManageController::class, 'manageChangePwd']); //修改密码
    Route::post('/manage/changeSelfPwd', [Admin\ManageController::class, 'manageChangeSelfPwd']); //修改自己的密码


    /**
     * 新闻类型
     */
    Route::get('/newsType/filterList', [Admin\NewsTypeController::class, 'filterList']); // 筛选列表
    Route::get('/newsType/list', [Admin\NewsTypeController::class, 'lists']); // 列表
    Route::get('/newsType/detail', [Admin\NewsTypeController::class, 'detail']); // 详情
    Route::post('/newsType/add', [Admin\NewsTypeController::class, 'add']); // 新增
    Route::post('/newsType/change', [Admin\NewsTypeController::class, 'change']); // 编辑
    Route::post('/newsType/del', [Admin\NewsTypeController::class, 'del']); // 删除

    /**
     * 新闻
     */
    Route::get('/news/list', [Admin\NewsController::class, 'lists']); // 列表
    Route::get('/news/detail', [Admin\NewsController::class, 'detail']); // 详情
    Route::post('/news/add', [Admin\NewsController::class, 'add']); // 新增
    Route::post('/news/change', [Admin\NewsController::class, 'change']); // 编辑
    Route::post('/news/del', [Admin\NewsController::class, 'del']); // 删除
    Route::post('/news/topAndCancel', [Admin\NewsController::class, 'topAndCancel']); // 置顶与取消置顶


    /**
     * 服务指南
     */
    Route::get('/serviceDirectory/list', [Admin\ServiceDirectoryController::class, 'lists']); // 列表
    Route::get('/serviceDirectory/detail', [Admin\ServiceDirectoryController::class, 'detail']); // 详情
    Route::post('/serviceDirectory/add', [Admin\ServiceDirectoryController::class, 'add']); // 新增
    Route::post('/serviceDirectory/change', [Admin\ServiceDirectoryController::class, 'change']); // 编辑
    Route::post('/serviceDirectory/del', [Admin\ServiceDirectoryController::class, 'del']); // 删除

    /**
     * 数字阅读类型
     */
    Route::get('/digitalType/list', [Admin\DigitalTypeController::class, 'lists']);
    Route::get('/digitalType/detail', [Admin\DigitalTypeController::class, 'detail']);
    Route::post('/digitalType/add', [Admin\DigitalTypeController::class, 'add']);
    Route::post('/digitalType/change', [Admin\DigitalTypeController::class, 'change']);
    Route::post('/digitalType/del', [Admin\DigitalTypeController::class, 'del']);
    Route::get('/digitalType/filterList', [Admin\DigitalTypeController::class, 'filterList']); //数字阅读类型列表（用于筛选）


    /**
     * 数字阅读
     */
    Route::get('/digital/list', [Admin\DigitalController::class, 'lists']); // 列表
    Route::get('/digital/detail', [Admin\DigitalController::class, 'detail']); // 详情
    Route::post('/digital/add', [Admin\DigitalController::class, 'add']); // 新增
    Route::post('/digital/change', [Admin\DigitalController::class, 'change']); // 编辑
    Route::post('/digital/del', [Admin\DigitalController::class, 'del']); // 删除
    Route::post('/digital/cancelAndRelease', [Admin\DigitalController::class, 'cancelAndRelease']); //  撤销 和发布
    Route::post('/digital/sortChange', [Admin\DigitalController::class, 'sortChange']); // 数字阅读排序

    /**
     * 新书推荐类型
     */
    Route::get('/newBookRecommendType/filterList', [Admin\NewBookRecommendTypeController::class, 'filterList']); // 筛选列表
    Route::get('/newBookRecommendType/list', [Admin\NewBookRecommendTypeController::class, 'lists']); // 列表
    Route::get('/newBookRecommendType/detail', [Admin\NewBookRecommendTypeController::class, 'detail']); // 详情
    Route::post('/newBookRecommendType/add', [Admin\NewBookRecommendTypeController::class, 'add']); // 新增
    Route::post('/newBookRecommendType/change', [Admin\NewBookRecommendTypeController::class, 'change']); // 编辑
    Route::post('/newBookRecommendType/del', [Admin\NewBookRecommendTypeController::class, 'del']); // 删除

    /**
     * 新书推荐
     */
    Route::get('/newBookRecommend/list', [Admin\NewBookRecommendController::class, 'lists']); // 列表
    Route::get('/newBookRecommend/detail', [Admin\NewBookRecommendController::class, 'detail']); // 详情
    Route::post('/newBookRecommend/add', [Admin\NewBookRecommendController::class, 'add']); // 新增
    Route::post('/newBookRecommend/change', [Admin\NewBookRecommendController::class, 'change']); // 编辑
    Route::post('/newBookRecommend/del', [Admin\NewBookRecommendController::class, 'del']); // 删除
    Route::post('/newBookRecommend/cancelAndRelease', [Admin\NewBookRecommendController::class, 'cancelAndRelease']); // 推荐与取消推荐


    /**
     * 图书推荐
     */
    Route::get('/bookRecom/list', [Admin\BookRecomController::class, 'lists']); // 列表
    Route::get('/bookRecom/detail', [Admin\BookRecomController::class, 'detail']); // 详情
    Route::post('/bookRecom/add', [Admin\BookRecomController::class, 'add']); // 新增
    Route::post('/bookRecom/change', [Admin\BookRecomController::class, 'change']); // 编辑
    Route::post('/bookRecom/del', [Admin\BookRecomController::class, 'del']); // 删除
    Route::post('/bookRecom/recomAndCancel', [Admin\BookRecomController::class, 'recomAndCancel']); // 推荐与取消推荐


    /**
     * 活动
     */
    Route::get('/activity/getActivityApplyParam', [Admin\ActivityController::class, 'getActivityApplyParam']); // 获取报名参数
    Route::get('/activity/list', [Admin\ActivityController::class, 'lists']); // 列表
    Route::get('/activity/detail', [Admin\ActivityController::class, 'detail']); // 详情
    Route::post('/activity/add', [Admin\ActivityController::class, 'add']); // 新增
    Route::post('/activity/change', [Admin\ActivityController::class, 'change']); // 编辑
    Route::post('/activity/del', [Admin\ActivityController::class, 'del']); // 删除
    Route::post('/activity/cancel', [Admin\ActivityController::class, 'cancel']); // 活动撤销
    Route::post('/activity/release', [Admin\ActivityController::class, 'release']); // 活动发布
    Route::post('/activity/sign', [Admin\ActivityController::class, 'sign']); // 签到
    Route::post('/activity/switchoverType', [Admin\ActivityController::class, 'switchoverType']); // 切换类型
    Route::post('/activity/recomAndCancel', [Admin\ActivityController::class, 'recomAndCancel']); // 活动推荐与取消推荐
    Route::post('/activity/push', [Admin\ActivityController::class, 'push']); // 活动推送 发送模板消息给用户

    /**
     * 活动报名信息管理
     */
    Route::get('/activityApply/activityApplyList', [Admin\ActivityApplyController::class, 'activityApplyList']); // 活动申请列表
    Route::get('/activityApply/activityApplyUser', [Admin\ActivityApplyController::class, 'activityApplyUser']); // 活动人员列表
    Route::post('/activityApply/agreeAndRefused', [Admin\ActivityApplyController::class, 'agreeAndRefused']); // 审核通过 和 拒绝
    Route::post('/activityApply/violateAndCancel', [Admin\ActivityApplyController::class, 'violateAndCancel']); // 活动报名违规 与 取消违规操作

    /**
     * 活动类型
     */
    Route::get('/activityType/filterList', [Admin\ActivityTypeController::class, 'filterList']); // 筛选列表
    Route::get('/activityType/list', [Admin\ActivityTypeController::class, 'lists']); // 列表
    Route::get('/activityType/detail', [Admin\ActivityTypeController::class, 'detail']); // 详情
    Route::post('/activityType/add', [Admin\ActivityTypeController::class, 'add']); // 新增
    Route::post('/activityType/change', [Admin\ActivityTypeController::class, 'change']); // 编辑
    Route::post('/activityType/del', [Admin\ActivityTypeController::class, 'del']); // 删除


    /**
     * 商品类型管理
     */
    Route::get('/goodsType/filterList', [Admin\GoodsTypeController::class, 'filterList']); // 筛选列表
    Route::get('/goodsType/list', [Admin\GoodsTypeController::class, 'lists']); // 列表
    Route::get('/goodsType/detail', [Admin\GoodsTypeController::class, 'detail']); // 详情
    Route::post('/goodsType/add', [Admin\GoodsTypeController::class, 'add']); // 新增
    Route::post('/goodsType/change', [Admin\GoodsTypeController::class, 'change']); // 编辑
    Route::post('/goodsType/del', [Admin\GoodsTypeController::class, 'del']); // 删除

    /**
     * 商品管理
     */
    Route::get('/goods/list', [Admin\GoodsController::class, 'lists']); // 列表
    Route::get('/goods/detail', [Admin\GoodsController::class, 'detail']); // 详情
    Route::post('/goods/add', [Admin\GoodsController::class, 'add']); // 新增
    Route::post('/goods/change', [Admin\GoodsController::class, 'change']); // 编辑
    Route::post('/goods/del', [Admin\GoodsController::class, 'del']); // 删除
    Route::post('/goods/goodsOnAndOff', [Admin\GoodsController::class, 'goodsOnAndOff']); // 上架与下架商品
    Route::get('/goods/getGoodsPickAddress', [Admin\GoodsController::class, 'getGoodsPickAddress']); // 获取商品自提地址
    Route::post('/goods/setGoodsPickAddress', [Admin\GoodsController::class, 'setGoodsPickAddress']); // 设置商品自提地址

    /**
     * 商品兑换管理
     */
    Route::get('/goodsRecord/list', [Admin\GoodsRecordController::class, 'lists']); // 用户商品兑换记录
    Route::post('/goodsRecord/fetch', [Admin\GoodsRecordController::class, 'fetchGoods']); // 领取
    Route::post('/goodsRecord/send', [Admin\GoodsRecordController::class, 'sendGoods']); // 发货
    Route::post('/goodsRecord/cancel', [Admin\GoodsRecordController::class, 'cancelConversionGoods']); // 取消兑换

    /**
     * 活动标签
     */
    Route::get('/activityTag/filterList', [Admin\ActivityTagController::class, 'filterList']); // 筛选列表
    Route::get('/activityTag/list', [Admin\ActivityTagController::class, 'lists']); // 列表
    Route::get('/activityTag/detail', [Admin\ActivityTagController::class, 'detail']); // 详情
    Route::post('/activityTag/add', [Admin\ActivityTagController::class, 'add']); // 新增
    Route::post('/activityTag/change', [Admin\ActivityTagController::class, 'change']); // 编辑
    Route::post('/activityTag/del', [Admin\ActivityTagController::class, 'del']); // 删除


    /**
     * 问卷调查
     */
    Route::get('/survey/list', [Admin\SurveyController::class, 'lists']); // 列表
    Route::get('/survey/detail', [Admin\SurveyController::class, 'detail']); // 详情
    Route::post('/survey/add', [Admin\SurveyController::class, 'add']); // 新增
    Route::post('/survey/change', [Admin\SurveyController::class, 'change']); // 编辑
    Route::post('/survey/del', [Admin\SurveyController::class, 'del']); // 删除
    Route::post('/survey/cancelAndRelease', [Admin\SurveyController::class, 'cancelAndRelease']); // 问卷调查 撤销  与 发布


    /**
     * 在线荐购
     */
    Route::get('/recommendBook/list', [Admin\RecommendBookController::class, 'lists']); // 列表
    Route::post('/recommendBook/agreeAndRefused', [Admin\RecommendBookController::class, 'agreeAndRefused']); // 审核


    /**
     * 权限
     */
    Route::get('/permission/list', [Admin\PermissionController::class, 'permissionList']); // 权限列表
    Route::get('/permission/tree', [Admin\PermissionController::class, 'permissionTree']); // 权限树形列表
    Route::get('/permission/detail', [Admin\PermissionController::class, 'permissionDetail']); // 权限详情
    Route::post('/permission/add', [Admin\PermissionController::class, 'permissionInsert']); // 权限新增
    Route::post('/permission/change', [Admin\PermissionController::class, 'permissionChange']); // 权限编辑
    Route::post('/permission/del', [Admin\PermissionController::class, 'permissionDel']); // 权限删除
    Route::post('/permission/orderChange', [Admin\PermissionController::class, 'permissionOrderChange']); // 更改权限排序

    /**
     * 角色
     */
    Route::get('/role/list', [Admin\RoleController::class, 'roleList']); // 角色列表
    Route::get('/role/filterList', [Admin\RoleController::class, 'roleFilterList']); // 角色列表 （筛选使用）
    Route::get('/role/detail', [Admin\RoleController::class, 'roleDetail']); // 角色详情
    Route::post('/role/add', [Admin\RoleController::class, 'roleAdd']); // 角色新增
    Route::post('/role/change', [Admin\RoleController::class, 'roleChange']); // 角色编辑
    Route::post('/role/del', [Admin\RoleController::class, 'roleDel']); // 角色删除


    /**
     * banner管理
     */
    Route::get('/banner/list', [Admin\BannerController::class, 'lists']); // 列表
    Route::get('/banner/detail', [Admin\BannerController::class, 'detail']); // 详情
    Route::post('/banner/add', [Admin\BannerController::class, 'add']); // 新增
    Route::post('/banner/change', [Admin\BannerController::class, 'change']); // 编辑
    Route::post('/banner/del', [Admin\BannerController::class, 'del']); // 删除
    Route::post('/banner/cancelAndRelease', [Admin\BannerController::class, 'cancelAndRelease']); // 切换显示方式
    Route::post('/banner/sortChange', [Admin\BannerController::class, 'sortChange']); // banner 图排序


    /**
     * 景点打卡类型
     */
    Route::get('/scenicType/filterList', [Admin\ScenicTypeController::class, 'filterList']); // 筛选列表
    Route::get('/scenicType/list', [Admin\ScenicTypeController::class, 'lists']); // 列表
    Route::get('/scenicType/detail', [Admin\ScenicTypeController::class, 'detail']); // 详情
    Route::post('/scenicType/add', [Admin\ScenicTypeController::class, 'add']); // 新增
    Route::post('/scenicType/change', [Admin\ScenicTypeController::class, 'change']); // 编辑
    Route::post('/scenicType/del', [Admin\ScenicTypeController::class, 'del']); // 删除

    /**
     * 景点打卡
     */
    Route::get('/scenic/list', [Admin\ScenicController::class, 'lists']); // 列表
    Route::get('/scenic/detail', [Admin\ScenicController::class, 'detail']); // 详情
    Route::post('/scenic/add', [Admin\ScenicController::class, 'add']); // 新增
    Route::post('/scenic/change', [Admin\ScenicController::class, 'change']); // 编辑
    Route::post('/scenic/del', [Admin\ScenicController::class, 'del']); // 删除
    Route::post('/scenic/cancelAndRelease', [Admin\ScenicController::class, 'cancelAndRelease']); // 撤销 和发布


    /**
     * 景点打卡作品
     */
    Route::get('/scenicWorks/list', [Admin\ScenicWorksController::class, 'lists']); // 列表
    Route::get('/scenicWorks/detail', [Admin\ScenicWorksController::class, 'detail']); // 详情
    Route::post('/scenicWorks/worksCheck', [Admin\ScenicWorksController::class, 'worksCheck']); // 审核作品
    Route::post('/scenicWorks/violateAndCancel', [Admin\ScenicWorksController::class, 'violateAndCancel']); // 作品违规 与 取消违规操作

    /**
     * 积分设置
     */
    Route::get('/scoreInfo/list', [Admin\ScoreInfoController::class, 'lists']); // 积分规则列表
    Route::post('/scoreInfo/change', [Admin\ScoreInfoController::class, 'change']); // 修改积分规则
    Route::get('/scoreInfo/scoreLog', [Admin\ScoreInfoController::class, 'scoreLog']); // 积分变动记录
    Route::post('/scoreInfo/scoreChange', [Admin\ScoreInfoController::class, 'scoreChange']); // 手动调整用户积分


    Route::get('/scoreInfo/customScoreRule', [Admin\ScoreInfoController::class, 'customScoreRule']); // 获取自定义积分规则
    Route::post('/scoreInfo/customScoreRuleChange', [Admin\ScoreInfoController::class, 'customScoreRuleChange']); // 设置自定义积分规则

    /**
     * 违规设置
     */
    Route::get('/violateConfig/detail', [Admin\ViolateConfigController::class, 'detail']); // 详情
    Route::post('/violateConfig/change', [Admin\ViolateConfigController::class, 'change']); // 修改违规设置

    /**
     * 用户管理
     */
    Route::get('/userInfo/list', [Admin\UserInfoController::class, 'lists']); //用户列表
    Route::post('/userInfo/clearViolate', [Admin\UserInfoController::class, 'clearViolate']); //  清空违规次数
    Route::get('/userInfo/getViolateList', [Admin\UserInfoController::class, 'getViolateList']); //  获取用户违规列表

    /**
     * 用户留言管理
     */
    Route::get('/feedback/list', [Admin\FeedbackController::class, 'lists']); //用户留言列表
    Route::post('/feedback/reply', [Admin\FeedbackController::class, 'reply']); //  用户留言回复


    /**
     * 主页信息
     */
    Route::get('/index/index', [Admin\IndexController::class, 'index']); //首页数据统计
    Route::get('/index/accessStatistics', [Admin\IndexController::class, 'accessStatistics']); //公众号访问统计

    /**
     * 预约
     */
    Route::get('/reservation/getReservationApplyParam', [Admin\ReservationController::class, 'getReservationApplyParam']); // 获取预约参数
    Route::get('/reservation/filterTagList', [Admin\ReservationController::class, 'filterTagList']); //文化配送标签列表（预定义）
    Route::get('/reservation/list', [Admin\ReservationController::class, 'lists']); //列表
    Route::get('/reservation/detail', [Admin\ReservationController::class, 'detail']); //详情
    Route::post('/reservation/add', [Admin\ReservationController::class, 'add']); //新增
    Route::post('/reservation/change', [Admin\ReservationController::class, 'change']); //修改
    Route::post('/reservation/del', [Admin\ReservationController::class, 'del']); //删除
    Route::post('/reservation/playAndCancel', [Admin\ReservationController::class, 'playAndCancel']); //推荐与取消推荐


    Route::get('/reservationOffline/getMakeQuantum', [Admin\ReservationOfflineController::class, 'getMakeQuantum']); //手动预约获取预约时间段
    Route::post('/reservationOffline/makeInfo', [Admin\ReservationOfflineController::class, 'makeInfo']); //手动预约时间段


    Route::get('/reservation/getReservationQr', [Admin\ReservationController::class, 'getReservationQr']); //出示用户领取、归还钥匙二维码(动态二维码) （node 7 适用）  有效期3分钟


    //特殊排班时间
    Route::get('/reservationSpecialSchedule/list', [Admin\ReservationSpecialScheduleController::class, 'lists']); //特殊时间列表
    Route::post('/reservationSpecialSchedule/add', [Admin\ReservationSpecialScheduleController::class, 'add']); //特殊时间新增
    Route::post('/reservationSpecialSchedule/change', [Admin\ReservationSpecialScheduleController::class, 'change']); //特殊时间编辑
    Route::get('/reservationSpecialSchedule/detail', [Admin\ReservationSpecialScheduleController::class, 'detail']); //特殊时间详情
    Route::post('/reservationSpecialSchedule/del', [Admin\ReservationSpecialScheduleController::class, 'del']); //特殊时间删除


    Route::get('/reservationSeat/scheduleList', [Admin\ReservationSeatController::class, 'scheduleList']); //获取排版信息
    Route::get('/reservationSeat/list', [Admin\ReservationSeatController::class, 'lists']); //座位预览
    Route::get('/reservationSeat/geSeatListBytStatus', [Admin\ReservationSeatController::class, 'geSeatListBytStatus']); //根据状态获取格子列表 (只能用于 node 为 7 的情况)
    Route::post('/reservationSeat/add', [Admin\ReservationSeatController::class, 'add']); //座位新增
    Route::post('/reservationSeat/playAndCancel', [Admin\ReservationSeatController::class, 'playAndCancel']); //座位发布与撤销
    Route::post('/reservationSeat/del', [Admin\ReservationSeatController::class, 'del']); //座位删除
    Route::post('/reservationSeat/downloadQr', [Admin\ReservationSeatController::class, 'downloadQr']); //下载座位二维码

    Route::get('/reservationApply/list', [Admin\ReservationApplyController::class, 'lists']); //预约申请列表
    Route::get('/reservationApply/reservationApplyHistory', [Admin\ReservationApplyController::class, 'reservationApplyHistory']); //获取用户对于某个预约的预约记录
    Route::post('/reservationApply/agreeAndRefused', [Admin\ReservationApplyController::class, 'agreeAndRefused']); //审核通过 和 拒绝
    Route::post('/reservationApply/violateAndCancel', [Admin\ReservationApplyController::class, 'violateAndCancel']); //预约违规 与 取消违规操作

    Route::post('/reservationApply/applyCancel', [Admin\ReservationApplyController::class, 'applyCancel']); //管理员取消预约
    Route::post('/reservationApply/applySignIn', [Admin\ReservationApplyController::class, 'applySignIn']); //管理员打卡签到\签退
    Route::post('/reservationApply/applySignOut', [Admin\ReservationApplyController::class, 'applySignOut']); // 管理员操作用户打卡签退(弃用，改为签到同一个接口)
    Route::post('/reservationApply/collectionAndReturn', [Admin\ReservationApplyController::class, 'collectionAndReturn']); //管理员操作领取和归还钥匙

    /**
     * 整个项目 数据分析
     */
    Route::get('/dataAnalysis/appletStatistics', [Admin\DataAnalysisController::class, 'appletStatistics']); // 小程序访问统计
    Route::get('/dataAnalysis/activityStatistics', [Admin\DataAnalysisController::class, 'activityStatistics']); // 活动参与人数统计
    Route::get('/dataAnalysis/recomAndSurveyStatistics', [Admin\DataAnalysisController::class, 'recomAndSurveyStatistics']); // 荐购、问卷统计

    Route::get('/dataAnalysis/accessStatistics', [Admin\DataAnalysisController::class, 'accessStatistics']); // 小程序访问总统计图

    Route::get('/answerActivityToatlDataAnalysis/answerDataStatistics', [Admin\AnswerActivityToatlDataAnalysisController::class, 'answerDataStatistics']); // 答题总数据统计
    Route::get('/answerActivityToatlDataAnalysis/answerTimeStatistics', [Admin\AnswerActivityToatlDataAnalysisController::class, 'answerTimeStatistics']); // 答题次数统计
    Route::get('/answerActivityToatlDataAnalysis/answerDataRanking', [Admin\AnswerActivityToatlDataAnalysisController::class, 'answerDataRanking']); // 最新活动排行榜

    Route::get('/turnActivityToatlDataAnalysis/turnDataStatistics', [Admin\TurnActivityToatlDataAnalysisController::class, 'turnDataStatistics']); // 翻奖总数据统计
    Route::get('/turnActivityToatlDataAnalysis/turnTimeStatistics', [Admin\TurnActivityToatlDataAnalysisController::class, 'turnTimeStatistics']); // 翻奖次数总统计
    Route::get('/turnActivityToatlDataAnalysis/turnDataRanking', [Admin\TurnActivityToatlDataAnalysisController::class, 'turnDataRanking']); // 最新活动排行榜


    Route::get('/contestToatlDataAnalysis/contestDataStatistics', [Admin\ContestToatlDataAnalysisController::class, 'contestDataStatistics']); // 在线投票总数据统计
    Route::get('/contestToatlDataAnalysis/contestDataRanking', [Admin\ContestToatlDataAnalysisController::class, 'contestDataRanking']); // 最新活动排行榜

    Route::get('/pictureLiveToatlDataAnalysis/pictureLiveDataStatistics', [Admin\PictureLiveToatlDataAnalysisController::class, 'pictureLiveDataStatistics']); // 图片直播总数据统计
    Route::get('/pictureLiveToatlDataAnalysis/pictureLiveDataRanking', [Admin\PictureLiveToatlDataAnalysisController::class, 'pictureLiveDataRanking']); // 最新活动排行榜

    Route::get('/dataAnalysis/authRegisterStatistics', [Admin\DataAnalysisController::class, 'authRegisterStatistics']); // 用户授权注册总统计图
    Route::get('/dataAnalysis/otherStatistics', [Admin\DataAnalysisController::class, 'otherStatistics']); // 其他数据统计
    Route::get('/dataAnalysis/activityTypeStatistics', [Admin\DataAnalysisController::class, 'activityTypeStatistics']); // 活动类型分布统计图
    Route::get('/dataAnalysis/activityApplyStatistics', [Admin\DataAnalysisController::class, 'activityApplyStatistics']); // 活动参与报名人数统计
    Route::get('/dataAnalysis/competiteActivityStatistics', [Admin\DataAnalysisController::class, 'competiteActivityStatistics']); // 征集活动作品大类型统计
    Route::get('/dataAnalysis/competiteActivityPersonStatistics', [Admin\DataAnalysisController::class, 'competiteActivityPersonStatistics']); // 征集活动投递人次
    Route::get('/dataAnalysis/databaseWorksStatistics', [Admin\DataAnalysisController::class, 'databaseWorksStatistics']); // 数据库作品统计
    Route::get('/dataAnalysis/userStatistics', [Admin\DataAnalysisController::class, 'userStatistics']); // 用户统计
    Route::get('/dataAnalysis/singleUserStatistics', [Admin\DataAnalysisController::class, 'singleUserStatistics']); // 单个用户统计





    /**
     * 服务意向
     */
    Route::get('/volunteerServiceIntention/filterList', [Admin\VolunteerServiceIntentionController::class, 'filterList']); //志愿者服务意向类
    Route::get('/volunteerServiceIntention/list', [Admin\VolunteerServiceIntentionController::class, 'lists']); //列表
    Route::get('/volunteerServiceIntention/detail', [Admin\VolunteerServiceIntentionController::class, 'detail']); //详情
    Route::post('/volunteerServiceIntention/add', [Admin\VolunteerServiceIntentionController::class, 'add']); //新增
    Route::post('/volunteerServiceIntention/change', [Admin\VolunteerServiceIntentionController::class, 'change']); //修改
    Route::post('/volunteerServiceIntention/del', [Admin\VolunteerServiceIntentionController::class, 'del']); //删除

    /**
     * 服务时间
     */
    Route::get('/volunteerServiceTime/filterList', [Admin\VolunteerServiceTimeController::class, 'filterList']); //志愿者服务时间类
    Route::get('/volunteerServiceTime/list', [Admin\VolunteerServiceTimeController::class, 'lists']); //列表
    Route::get('/volunteerServiceTime/detail', [Admin\VolunteerServiceTimeController::class, 'detail']); //详情
    Route::post('/volunteerServiceTime/add', [Admin\VolunteerServiceTimeController::class, 'add']); //新增
    Route::post('/volunteerServiceTime/change', [Admin\VolunteerServiceTimeController::class, 'change']); //修改
    Route::post('/volunteerServiceTime/del', [Admin\VolunteerServiceTimeController::class, 'del']); //删除

    /**
     * 志愿者服务
     */
    Route::get('/volunteer/filterList', [Admin\VolunteerController::class, 'filterList']); // 服务岗位列表(用于下拉框选择)
    Route::get('/volunteer/getPullFilterList', [Admin\VolunteerController::class, 'getPullFilterList']); // 参加志愿者所需下拉选项(用于下拉框选择)
    Route::get('/volunteer/volunteerApplyParam', [Admin\VolunteerController::class, 'volunteerApplyParam']); // 志愿者报名参数(用于下拉框选择)
    Route::get('/volunteer/list', [Admin\VolunteerController::class, 'lists']); // 服务岗位列表
    Route::get('/volunteer/detail', [Admin\VolunteerController::class, 'detail']); // 服务岗位详情
    Route::post('/volunteer/add', [Admin\VolunteerController::class, 'add']); // 服务岗位添加
    Route::post('/volunteer/change', [Admin\VolunteerController::class, 'change']); // 服务岗位修改
    Route::post('/volunteer/del', [Admin\VolunteerController::class, 'del']); // 服务岗位删除
    Route::post('/volunteer/playAndCancel', [Admin\VolunteerController::class, 'playAndCancel']); // 发布与取消发布
    Route::get('/volunteer/getVolunteerNotice', [Admin\VolunteerController::class, 'getVolunteerNotice']); //  获取志愿者须知
    Route::post('/volunteer/setVolunteerNotice', [Admin\VolunteerController::class, 'setVolunteerNotice']); //  设置志愿者须知

    /**
     * 志愿者服务报名信息
     */
    Route::get('/volunteerApply/list', [Admin\VolunteerApplyController::class, 'lists']); //  志愿者服务报名列表
    Route::get('/volunteerApply/detail', [Admin\VolunteerApplyController::class, 'detail']); //  志愿者服务报名详情
    Route::post('/volunteerApply/check', [Admin\VolunteerApplyController::class, 'check']); //  志愿者服务报名获取审核
    Route::get('/volunteerApply/serviceTimeList', [Admin\VolunteerApplyController::class, 'serviceTimeList']); //  获取服务时长列表
    Route::post('/volunteerApply/serviceTimeChange', [Admin\VolunteerApplyController::class, 'serviceTimeChange']); //  调整用户服务时长


    /**
     * 总分馆简介（分馆简介）
     */
    Route::get('/branchInfo/filterList', [Admin\BranchInfoController::class, 'filterList']); // 总分馆(用于下拉框选择)
    Route::get('/branchInfo/list', [Admin\BranchInfoController::class, 'lists']); // 列表
    Route::get('/branchInfo/detail', [Admin\BranchInfoController::class, 'detail']); // 详情
    Route::post('/branchInfo/add', [Admin\BranchInfoController::class, 'add']); // 新增
    Route::post('/branchInfo/change', [Admin\BranchInfoController::class, 'change']); // 编辑
    Route::post('/branchInfo/del', [Admin\BranchInfoController::class, 'del']); // 删除

    /**
     * 分馆用户账号管理
     */
    Route::get('/branchAccount/list', [Admin\BranchAccountController::class, 'lists']); // 列表
    //Route::get('/branchAccount/detail', [Admin\BranchAccountController::class, 'detail']); // 详情
    Route::post('/branchAccount/add', [Admin\BranchAccountController::class, 'add']); // 新增
    Route::post('/branchAccount/change', [Admin\BranchAccountController::class, 'change']); // 编辑
    Route::post('/branchAccount/del', [Admin\BranchAccountController::class, 'del']); // 删除
    Route::post('/branchAccount/changePwd', [Admin\BranchAccountController::class, 'changePwd']); //  用户管理账号修改密码

    /**
     * 欠费缴纳
     */
    Route::get('/oweProcess/list', [Admin\OweProcessController::class, 'lists']); // 列表
    Route::get('/oweProcess/subscriptionRatioSet', [Admin\OweProcessController::class, 'subscriptionRatioSet']); // 欠费缴纳积分抵扣设置


    /**
     * 在线办证配置
     */
    Route::get('/onlineRegistrationConfig/getOnlineRegistrationConfigParam', [Admin\OnlineRegistrationConfigController::class, 'getOnlineRegistrationConfigParam']); // 获取在线办证参数（获取已配置的）
    Route::get('/onlineRegistrationConfig/setOnlineRegistrationConfigParam', [Admin\OnlineRegistrationConfigController::class, 'setOnlineRegistrationConfigParam']); // 配置在线办证参数

    /**
     * 在线办证
     */
    Route::get('/onlineRegistration/getDynamicParam', [Admin\OnlineRegistrationController::class, 'getDynamicParam']); // 获取动态参数
    Route::post('/onlineRegistration/sendVerify', [Admin\OnlineRegistrationController::class, 'sendVerify']); // 发送验证码
    Route::get('/onlineRegistration/getCertificateType', [Admin\OnlineRegistrationController::class, 'getCertificateType']); // 获取证件类型
    Route::get('/onlineRegistration/getCardType', [Admin\OnlineRegistrationController::class, 'getCardType']); // 获取在线办证类型
    Route::get('/onlineRegistration/getFrontCardType', [Admin\OnlineRegistrationController::class, 'getFrontCardType']); // 获取在前台线办证类型
    Route::post('/onlineRegistration/registration', [Admin\OnlineRegistrationController::class, 'registration']); // 读者证-办证接口
    Route::get('/onlineRegistration/sponsorRegistrationList', [Admin\OnlineRegistrationController::class, 'sponsorRegistrationList']); //（发起方）办证统计列表 （前台在线办证列表）
    Route::get('/onlineRegistration/sponsorRegistrationStatistics', [Admin\OnlineRegistrationController::class, 'sponsorRegistrationStatistics']); //（发起方）办证统计数据  （前台在线办证列表）
    Route::get('/onlineRegistration/confirmPostageList', [Admin\OnlineRegistrationController::class, 'confirmPostageList']); //（确认方）办证统计列表
    Route::get('/onlineRegistration/confirmPostageStatistics', [Admin\OnlineRegistrationController::class, 'confirmPostageStatistics']); //（确认方） 邮费统计 金额及书籍本数 统计
    Route::post('/onlineRegistration/sponsorSettleState', [Admin\OnlineRegistrationController::class, 'sponsorSettleState']); //（发起方）标记办证状态为结算中
    Route::post('/onlineRegistration/confirmSettleState', [Admin\OnlineRegistrationController::class, 'confirmSettleState']); //（确认方） 标记邮费状态为已结算
    Route::post('/onlineRegistration/toReceive', [Admin\OnlineRegistrationController::class, 'toReceive']); // 领取读者证
    Route::get('/onlineRegistration/getOnlineParam', [Admin\OnlineRegistrationController::class, 'getOnlineParam']); // 获取办证参数
    Route::post('/onlineRegistration/refund', [Admin\OnlineRegistrationController::class, 'refund']); //  写入图书馆系统接口失败，办证失败，进行退款操作  （用于自动退款失败后手动退款）



    /**
     * 服务提醒通知
     */
    Route::get('/serviceNotice/list', [Admin\ServiceNoticeController::class, 'lists']); // 列表
    Route::get('/serviceNotice/detail', [Admin\ServiceNoticeController::class, 'detail']); // 详情
    Route::post('/serviceNotice/add', [Admin\ServiceNoticeController::class, 'add']); // 新增
    Route::post('/serviceNotice/change', [Admin\ServiceNoticeController::class, 'change']); // 修改
    Route::post('/serviceNotice/send', [Admin\ServiceNoticeController::class, 'send']); // 发送
    Route::post('/serviceNotice/del', [Admin\ServiceNoticeController::class, 'del']); // 删除




    ######################################################图书到家start#############################################################

    /**
     * （图书到家）书店管理
     */
    Route::get('/shop/filterList', [Admin\ShopController::class, 'filterList']); //  书店筛选列表
    Route::get('/shop/list', [Admin\ShopController::class, 'lists']); // 列表
    Route::get('/shop/detail', [Admin\ShopController::class, 'detail']); // 详情
    Route::post('/shop/add', [Admin\ShopController::class, 'add']); // 新增
    Route::post('/shop/change', [Admin\ShopController::class, 'change']); // 编辑
    Route::post('/shop/del', [Admin\ShopController::class, 'del']); // 删除
    /**
     * （图书到家）书店书籍类型管理
     */
    Route::get('/newBookRecomType/filterList', [Admin\NewBookRecomTypeController::class, 'filterList']); //  筛选列表
    Route::get('/newBookRecomType/list', [Admin\NewBookRecomTypeController::class, 'lists']); // 列表
    Route::get('/newBookRecomType/detail', [Admin\NewBookRecomTypeController::class, 'detail']); // 详情
    Route::post('/newBookRecomType/add', [Admin\NewBookRecomTypeController::class, 'add']); // 新增
    Route::post('/newBookRecomType/change', [Admin\NewBookRecomTypeController::class, 'change']); // 编辑
    Route::post('/newBookRecomType/del', [Admin\NewBookRecomTypeController::class, 'del']); // 删除

    /**
     * （图书到家）书店书籍管理
     */
    Route::get('/newBookRecom/list', [Admin\NewBookRecomController::class, 'lists']); // 列表
    Route::get('/newBookRecom/detail', [Admin\NewBookRecomController::class, 'detail']); // 详情
    Route::post('/newBookRecom/add', [Admin\NewBookRecomController::class, 'add']); // 新增
    Route::post('/newBookRecom/change', [Admin\NewBookRecomController::class, 'change']); // 编辑
    Route::post('/newBookRecom/del', [Admin\NewBookRecomController::class, 'del']); // 删除
    Route::post('/newBookRecom/cancelAndRelease', [Admin\NewBookRecomController::class, 'cancelAndRelease']); //  推荐与取消推荐
    Route::post('/newBookRecom/upAndDown', [Admin\NewBookRecomController::class, 'upAndDown']); //  上架和下架


    /**
     * （图书到家）图书馆书籍类型管理
     */
    Route::get('/libBookRecomType/filterList', [Admin\LibBookRecomTypeController::class, 'filterList']); //  筛选列表
    Route::get('/libBookRecomType/list', [Admin\LibBookRecomTypeController::class, 'lists']); // 列表
    Route::get('/libBookRecomType/detail', [Admin\LibBookRecomTypeController::class, 'detail']); // 详情
    Route::post('/libBookRecomType/add', [Admin\LibBookRecomTypeController::class, 'add']); // 新增
    Route::post('/libBookRecomType/change', [Admin\LibBookRecomTypeController::class, 'change']); // 编辑
    Route::post('/libBookRecomType/del', [Admin\LibBookRecomTypeController::class, 'del']); // 删除

    /**
     * （图书到家）图书馆书籍管理
     */
    Route::get('/libBookRecom/list', [Admin\LibBookRecomController::class, 'lists']); // 列表
    Route::get('/libBookRecom/detail', [Admin\LibBookRecomController::class, 'detail']); // 详情
    Route::post('/libBookRecom/add', [Admin\LibBookRecomController::class, 'add']); // 新增
    Route::post('/libBookRecom/change', [Admin\LibBookRecomController::class, 'change']); // 编辑
    Route::post('/libBookRecom/del', [Admin\LibBookRecomController::class, 'del']); // 删除
    Route::post('/libBookRecom/cancelAndRelease', [Admin\LibBookRecomController::class, 'cancelAndRelease']); //  推荐与取消推荐

    /**
     * 图书到家 馆藏书借阅
     */
    Route::get('/bookHomeLibBook/orderList', [Admin\BookHomeLibBookController::class, 'orderList']); // 申请列表、发货列表、所有订单列表 （馆藏书）
    Route::get('/bookHomeLibBook/getOrderNumber', [Admin\BookHomeLibBookController::class, 'getOrderNumber']); // 获取未处理订单 和未发货订单数量 (馆藏书)
    Route::get('/bookHomeLibBook/orderDetail', [Admin\BookHomeLibBookController::class, 'orderDetail']); // 获取馆藏书订单详情
    Route::post('/bookHomeLibBook/printExpressSheetLib', [Admin\BookHomeLibBookController::class, 'printExpressSheetLib']); // 打印电子面单 （馆藏书）
    Route::get('/bookHomeLibBook/purchaseDetail', [Admin\BookHomeLibBookController::class, 'purchaseDetail']); // 获取采购清单详情
    Route::get('/bookHomeLibBook/getReaderBorrowList', [Admin\BookHomeLibBookController::class, 'getReaderBorrowList']); //线下书籍归还列表（根据读者证号查询数据）
    Route::get('/bookHomeLibBook/getBookBorrow', [Admin\BookHomeLibBookController::class, 'getBookBorrow']); //线下书籍归还查询（根据条形码查询数据）
    Route::post('/bookHomeLibBook/offlineReturn', [Admin\BookHomeLibBookController::class, 'offlineReturn']); //线下书籍确认归还 (新书的馆藏书的归还)
    Route::post('/bookHomeLibBook/offlineReturnDirect', [Admin\BookHomeLibBookController::class, 'offlineReturnDirect']); //线下书籍直接确认归还（涉及只有条形码，直接归还数据）
    Route::post('/bookHomeLibBook/onlineReturn', [Admin\BookHomeLibBookController::class, 'onlineReturn']); //线上书籍归还
    Route::get('/bookHomeLibBook/postalReturnList', [Admin\BookHomeLibBookController::class, 'postalReturnList']); //邮递还书列表
    Route::post('/bookHomeLibBook/disposeOrderApply', [Admin\BookHomeLibBookController::class, 'disposeOrderApply']); //管理员同意 和拒绝用户的 借阅申请（馆藏书）
    Route::post('/bookHomeLibBook/disposeOrderDeliver', [Admin\BookHomeLibBookController::class, 'disposeOrderDeliver']); //确认发货或无法发货
    Route::get('/bookHomeLibBook/bookReturnState', [Admin\BookHomeLibBookController::class, 'bookReturnState']); //获取书籍归还状态，修改本地书籍归还账号（外部 定时任务执行，可根据需求执行）


    /**
     * 新书采购清单 (图书馆端)
     */
    Route::get('/bookHomeNewBookLib/purchaseShopList', [Admin\BookHomeNewBookLibController::class, 'purchaseShopList']); // 所有(新书)采购清单 (图书馆端查看)
    Route::get('/bookHomeNewBookLib/purchaseLibList', [Admin\BookHomeNewBookLibController::class, 'purchaseLibList']); // 所有(馆藏书)采购清单 (图书馆端查看)(只查看线上的)
    Route::get('/bookHomeNewBookLib/disposeSuccess', [Admin\BookHomeNewBookLibController::class, 'disposeSuccess']); //  新书 数据 写入图书馆系统失败后，手动处理
    Route::get('/bookHomeNewBookLib/getPurchaseNumber', [Admin\BookHomeNewBookLibController::class, 'getPurchaseNumber']); //  计算采购数量及结算状态数量
    Route::post('/bookHomeNewBookLib/purchaseStateModify', [Admin\BookHomeNewBookLibController::class, 'purchaseStateModify']); //  修改采购清单状态为 结算中
    Route::get('/bookHomeNewBookLib/purchaseDetail', [Admin\BookHomeNewBookLibController::class, 'purchaseDetail']); //  获取采购清单详情
    Route::get('/bookHomeNewBookLib/refundList', [Admin\BookHomeNewBookLibController::class, 'refundList']); //  申请退款列表
    Route::post('/bookHomeNewBookLib/refund', [Admin\BookHomeNewBookLibController::class, 'refund']); //  审核拒绝或无法发货，进行退款操作


    /**
     * 图书到家 新书采购清单（书店端）
     */
    Route::get('/bookHomeNewBookShop/orderList', [Admin\BookHomeNewBookShopController::class, 'orderList']); // 申请列表、发货列表、所有订单列表 （新书）
    Route::get('/bookHomeNewBookShop/getOrderNumber', [Admin\BookHomeNewBookShopController::class, 'getOrderNumber']); // 获取未处理订单 和未发货订单数量 (新书)
    Route::get('/bookHomeNewBookShop/orderDetail', [Admin\BookHomeNewBookShopController::class, 'orderDetail']); // 获取订单详情
    Route::get('/bookHomeNewBookShop/disposeOrderApply', [Admin\BookHomeNewBookShopController::class, 'disposeOrderApply']); // 管理员同意 和拒绝用户的 采购申请
    Route::post('/bookHomeNewBookShop/disposeOrderDeliver', [Admin\BookHomeNewBookShopController::class, 'disposeOrderDeliver']); // 确认发货或无法发货
    Route::post('/bookHomeNewBookShop/againGetTrackingNumberShop', [Admin\BookHomeNewBookShopController::class, 'againGetTrackingNumberShop']); // 针对邮递发货 获取运单号失败后，补发获取快递单号（新书）
    Route::get('/bookHomeNewBookShop/printExpressSheetShop', [Admin\BookHomeNewBookShopController::class, 'printExpressSheetShop']); // 打印电子面单 （新书）
    Route::get('/bookHomeNewBookShop/purchaseList', [Admin\BookHomeNewBookShopController::class, 'purchaseList']); // 书店采购清单(书店查看)
    Route::get('/bookHomeNewBookShop/getPurchaseNumber', [Admin\BookHomeNewBookShopController::class, 'getPurchaseNumber']); // 计算采购数量及结算状态数量
    Route::post('/bookHomeNewBookShop/purchaseStateModify', [Admin\BookHomeNewBookShopController::class, 'purchaseStateModify']); // 修改采购清单状态为 已结算
    Route::get('/bookHomeNewBookShop/purchaseAllList', [Admin\BookHomeNewBookShopController::class, 'purchaseAllList']); // 所有(新书)采购清单 (书店端查看)


    /**
     * 图书到家线下荐购
     */
    Route::get('/bookHomeOfflineRecom/getReaderInfo', [Admin\BookHomeOfflineRecomController::class, 'getReaderInfo']); // 根据读者证号获取用户基本信息
    Route::get('/bookHomeOfflineRecom/getBookInfo', [Admin\BookHomeOfflineRecomController::class, 'getBookInfo']); // 根据数据ISBN号，获取书籍信息，并判断书籍是否符合借阅要求
    Route::post('/bookHomeOfflineRecom/affirmBorrow', [Admin\BookHomeOfflineRecomController::class, 'affirmBorrow']); // 确认线下借阅
    Route::get('/bookHomeOfflineRecom/offlineListShop', [Admin\BookHomeOfflineRecomController::class, 'offlineListShop']); // 线下取书记录 书店端 查看
    Route::get('/bookHomeOfflineRecom/offlineListLib', [Admin\BookHomeOfflineRecomController::class, 'offlineListLib']); // 线下取书记录(图书馆端) 查看

    /**
     * 邮费统计  书店为确认方，图书馆为 发起方
     */
    Route::get('/bookHomePostage/list', [Admin\BookHomePostageController::class, 'lists']); // （发起方、确认方） 邮费统计列表
    Route::get('/bookHomePostage/statistics', [Admin\BookHomePostageController::class, 'statistics']); // (发起方、确认方)邮费统计 金额及书籍本数 统计
    Route::post('/bookHomePostage/sponsorSettleState', [Admin\BookHomePostageController::class, 'sponsorSettleState']); //  (发起方)标记邮费状态为结算中
    Route::post('/bookHomePostage/affirmSettleState', [Admin\BookHomePostageController::class, 'affirmSettleState']); // （确认方）标记邮费状态为已结算

    /**
     * 图书到家系统设置
     */
    Route::post('/bookHomePurchaseSet/purchaseSet', [Admin\BookHomePurchaseSetController::class, 'purchaseSet']); // 图书经费+读者采购设置
    Route::post('/bookHomePurchaseSet/postageSet', [Admin\BookHomePurchaseSetController::class, 'postageSet']); // 邮费设置 或修改
    Route::get('/bookHomePurchaseSet/purchaseSetRecord', [Admin\BookHomePurchaseSetController::class, 'purchaseSetRecord']); //  设置记录
    Route::get('/bookHomePurchaseSet/purchaseBudgetList', [Admin\BookHomePurchaseSetController::class, 'purchaseBudgetList']); // 采购预算、邮费 增加的历史表
    Route::post('/bookHomePurchaseSet/libDeliverAddress', [Admin\BookHomePurchaseSetController::class, 'libDeliverAddress']); // 图书馆发货地址设置
    Route::post('/bookHomePurchaseSet/shopDeliverAddress', [Admin\BookHomePurchaseSetController::class, 'shopDeliverAddress']); // 书店发货地址设置

    /**
     * 邮递、现场归还地址管理
     */
    Route::get('/bookHomeReturnAddress/list', [Admin\BookHomeReturnAddressController::class, 'lists']); // 列表
    Route::get('/bookHomeReturnAddress/detail', [Admin\BookHomeReturnAddressController::class, 'detail']); // 详情
    Route::post('/bookHomeReturnAddress/add', [Admin\BookHomeReturnAddressController::class, 'add']); // 新增
    Route::post('/bookHomeReturnAddress/change', [Admin\BookHomeReturnAddressController::class, 'change']); // 编辑
    Route::post('/bookHomeReturnAddress/del', [Admin\BookHomeReturnAddressController::class, 'del']); // 删除
    Route::get('/bookHomeReturnAddress/getAddressExplain', [Admin\BookHomeReturnAddressController::class, 'getAddressExplain']); //  获取图书到家归还地址说明
    Route::post('/bookHomeReturnAddress/setAddressExplain', [Admin\BookHomeReturnAddressController::class, 'setAddressExplain']); //  设置图书到家归还地址说明

    /**
     * 扫码借配置
     */
    Route::get('/codePlace/filterList', [Admin\CodePlaceController::class, 'filterList']); // 筛选列表
    Route::get('/codePlace/list', [Admin\CodePlaceController::class, 'lists']); // 列表
    Route::get('/codePlace/detail', [Admin\CodePlaceController::class, 'detail']); // 详情
    Route::post('/codePlace/add', [Admin\CodePlaceController::class, 'add']); // 新增
    Route::post('/codePlace/change', [Admin\CodePlaceController::class, 'change']); // 编辑
    Route::post('/codePlace/del', [Admin\CodePlaceController::class, 'del']); // 删除
    Route::post('/codePlace/playAndCancel', [Admin\CodePlaceController::class, 'playAndCancel']); //  发布与取消发布

    /**
     * 扫码借记录
     */
    Route::get('/codeOnBorrowLog/borrowList', [Admin\CodeOnBorrowLogController::class, 'borrowList']); // 借阅列表
    Route::get('/codeOnBorrowLog/returnList', [Admin\CodeOnBorrowLogController::class, 'returnList']); // 归还列表


    ######################################################抽奖活动end#############################################################

    /**
     * 导出功能
     */
    Route::get('/newBookRecommendExport/index', [Controllers\Export\NewBookRecommendExport::class, 'index']); // 新书推荐列表导出
    Route::get('/newBookRecommendExport/template', [Controllers\Export\NewBookRecommendExport::class, 'template']); // 新书推荐模板导出
    Route::get('/activityApplyExport/activityApplyList', [Controllers\Export\ActivityApplyExport::class, 'activityApplyList']); // 活动申请列表导出
    Route::get('/activityApplyExport/activityApplyUser', [Controllers\Export\ActivityApplyExport::class, 'activityApplyUser']); // 活动人员列表导出
    Route::get('/activityApplyExport/activityApplyListAll', [Controllers\Export\ActivityApplyExport::class, 'activityApplyListAll']); // 批量活动申请列表导出
    Route::get('/activityApplyExport/activityApplyUserAll', [Controllers\Export\ActivityApplyExport::class, 'activityApplyUserAll']); // 批量活动人员列表导出
    Route::get('/recommendBookExport/index', [Controllers\Export\RecommendBookExport::class, 'index']); // 用户荐购列表（在线荐购）导出
    Route::get('/surveyExport/index', [Controllers\Export\SurveyExport::class, 'index']); // 问卷调查答案导出


    Route::get('/bookHomeExport/orderList', [Controllers\Export\BookHomeExport::class, 'orderList']); // 申请列表、发货列表、所有订单列表 导出(新书) 导出
    Route::get('/bookHomeExport/orderLibraryList', [Controllers\Export\BookHomeExport::class, 'orderLibraryList']); // 申请列表、发货列表、所有订单列表 （馆藏书）导出
    Route::get('/bookHomeExport/offlineListShop', [Controllers\Export\BookHomeExport::class, 'offlineListShop']); // 线下取书记录 (书店端) 导出
    Route::get('/bookHomeExport/offlineListLib', [Controllers\Export\BookHomeExport::class, 'offlineListLib']); // 线下取书记录(图书馆端) 查看   导出
    Route::get('/bookHomeExport/libPurchaseLibList', [Controllers\Export\BookHomeExport::class, 'libPurchaseLibList']);   // 所有(馆藏书)采购清单 (图书馆端查看)(只查看线上的)   导出
    Route::get('/bookHomeExport/libPurchaseShopList', [Controllers\Export\BookHomeExport::class, 'libPurchaseShopList']); // 新书线上取书记录列表 (图书馆端查看)(只查看线上的)   导出
    Route::get('/bookHomeExport/shopPurchaseAllList', [Controllers\Export\BookHomeExport::class, 'shopPurchaseAllList']); // 新书线上取书记录列表 (书店端查看)(只查看线上的)   导出
    Route::get('/bookHomeExport/purchaseLibList', [Controllers\Export\BookHomeExport::class, 'purchaseLibList']);         // 新书结算清单列表（图书馆端查看)(只查看线上的)   导出
    Route::get('/bookHomeExport/purchaseShopList', [Controllers\Export\BookHomeExport::class, 'purchaseShopList']);       // 新书结算清单列表（书店端查看)(只查看线上的)   导出
    Route::get('/postalReturnExport/index', [Controllers\Export\PostalReturnExport::class, 'index']); //  邮递还书列表 导出
    Route::get('/reservationApplyExport/index', [Controllers\Export\ReservationApplyExport::class, 'index']); //  预约申请、预约人数列表导出
    Route::get('/studyRoomReservationApplyExport/index', [Controllers\Export\StudyRoomReservationApplyExport::class, 'index']); //  空间预约申请、预约人数列表导出
    Route::get('/goodsExchangeExport/index', [Controllers\Export\GoodsExchangeExport::class, 'index']); //  商品兑换记录 导出
    Route::get('/templateExport/newBookRecommend', [Controllers\Export\TemplateExport::class, 'newBookRecommend']); //  新书推荐模板导出
    Route::get('/templateExport/shopNewBookRecommend', [Controllers\Export\TemplateExport::class, 'shopNewBookRecommend']); //  书店新书模板导出
    Route::get('/templateExport/libBookRecommend', [Controllers\Export\TemplateExport::class, 'libBookRecommend']); //  馆藏书模板下载导出


    Route::get('/volunteerExport/volunteerApplyList', [Controllers\Export\VolunteerExport::class, 'volunteerApplyList']); // 志愿者申请列表导出
    Route::get('/volunteerExport/volunteerList', [Controllers\Export\VolunteerExport::class, 'volunteerList']); // 志愿者列表导出



    Route::get('/codeBorrowLogExport/borrowList', [Controllers\Export\CodeBorrowLogExport::class, 'borrowList']); // 扫码借-借阅列表 导出
    Route::get('/codeBorrowLogExport/returnList', [Controllers\Export\CodeBorrowLogExport::class, 'returnList']); // 扫码借-归还列表 导出


    /**
     * 导入功能
     */
    Route::post('/newBookRecommendImport/index', [Controllers\Import\NewBookRecommendImport::class, 'index']); // 新书推荐管理导入
    Route::post('/libBookRecomImport/index', [Controllers\Import\LibBookRecomImport::class, 'index']); // 馆藏书批量导入
    Route::post('/shopBookImport/index', [Controllers\Import\ShopBookImport::class, 'index']); // 书店新书推荐管理导入


    ######################################################抽奖活动start#############################################################

    /**
     * 答题活动基础单位管理
     */
    Route::get('/answerActivityBaseUnit/getNatureName', [Admin\AnswerActivityBaseUnitController::class, 'getNatureName']); //获取单位性质
    Route::get('/answerActivityBaseUnit/filterList', [Admin\AnswerActivityBaseUnitController::class, 'filterList']); //类型(用于下拉框选择)
    Route::get('/answerActivityBaseUnit/list', [Admin\AnswerActivityBaseUnitController::class, 'lists']); //  列表
    Route::get('/answerActivityBaseUnit/detail', [Admin\AnswerActivityBaseUnitController::class, 'detail']); // 详情
    Route::post('/answerActivityBaseUnit/add', [Admin\AnswerActivityBaseUnitController::class, 'add']); // 新增
    Route::post('/answerActivityBaseUnit/change', [Admin\AnswerActivityBaseUnitController::class, 'change']); // 编辑
    Route::post('/answerActivityBaseUnit/del', [Admin\AnswerActivityBaseUnitController::class, 'del']); // 删除
    Route::post('/answerActivityBaseUnit/orderChange', [Admin\AnswerActivityBaseUnitController::class, 'orderChange']); // 修改区域排序


    /**
     * 答题活动
     */
    Route::get('/answerActivity/getNodeName', [Admin\AnswerActivityController::class, 'getNodeName']); // 获取活动类型
    Route::get('/answerActivity/getPatternName', [Admin\AnswerActivityController::class, 'getPatternName']); //  获取答题模式
    Route::get('/answerActivity/getPrizeFormName', [Admin\AnswerActivityController::class, 'getPrizeFormName']); // 获取获奖情况
    Route::get('/answerActivity/list', [Admin\AnswerActivityController::class, 'lists']); // 列表
    Route::get('/answerActivity/detail', [Admin\AnswerActivityController::class, 'detail']); // 详情
    Route::post('/answerActivity/add', [Admin\AnswerActivityController::class, 'add']); // 新增
    Route::post('/answerActivity/change', [Admin\AnswerActivityController::class, 'change']); // 编辑
    Route::post('/answerActivity/del', [Admin\AnswerActivityController::class, 'del']); // 删除
    Route::post('/answerActivity/cancelAndRelease', [Admin\AnswerActivityController::class, 'cancelAndRelease']); // 撤销 和发布
    Route::post('/answerActivity/invitCodeChange', [Admin\AnswerActivityController::class, 'invitCodeChange']); // 邀请码修改
    Route::post('/answerActivity/setAnswerRule', [Admin\AnswerActivityController::class, 'set_answer_rule']); // 设置答题总规则

    Route::post('/answerActivity/resetData', [Admin\AnswerActivityController::class, 'resetData']); // 重置数据
    Route::post('/answerActivity/copyData', [Admin\AnswerActivityController::class, 'copyData']); // 复制活动


    /**
     * 答题活动区域管理
     */
    Route::get('/answerActivityArea/filterList', [Admin\AnswerActivityAreaController::class, 'filterList']); //类型(用于下拉框选择)
    Route::get('/answerActivityArea/list', [Admin\AnswerActivityAreaController::class, 'lists']); //  列表
    Route::get('/answerActivityArea/detail', [Admin\AnswerActivityAreaController::class, 'detail']); // 详情
    Route::post('/answerActivityArea/add', [Admin\AnswerActivityAreaController::class, 'add']); // 新增
    Route::post('/answerActivityArea/change', [Admin\AnswerActivityAreaController::class, 'change']); // 编辑
    Route::post('/answerActivityArea/del', [Admin\AnswerActivityAreaController::class, 'del']); // 删除
    Route::post('/answerActivityArea/orderChange', [Admin\AnswerActivityAreaController::class, 'orderChange']); // 修改区域排序


    /**
     * 答题活动单位管理
     */
    Route::get('/answerActivityUnit/getNatureName', [Admin\AnswerActivityUnitController::class, 'getNatureName']); //获取单位性质
    Route::get('/answerActivityUnit/filterList', [Admin\AnswerActivityUnitController::class, 'filterList']); //类型(用于下拉框选择)
    Route::get('/answerActivityUnit/list', [Admin\AnswerActivityUnitController::class, 'lists']); //  列表
    Route::get('/answerActivityUnit/detail', [Admin\AnswerActivityUnitController::class, 'detail']); // 详情
    Route::post('/answerActivityUnit/add', [Admin\AnswerActivityUnitController::class, 'add']); // 新增
    Route::post('/answerActivityUnit/change', [Admin\AnswerActivityUnitController::class, 'change']); // 编辑
    Route::post('/answerActivityUnit/del', [Admin\AnswerActivityUnitController::class, 'del']); // 删除
    Route::post('/answerActivityUnit/orderChange', [Admin\AnswerActivityUnitController::class, 'orderChange']); // 修改区域排序
    Route::post('/answerActivityUnit/cancelAndDefault', [Admin\AnswerActivityUnitController::class, 'cancelAndDefault']); // 默认与取消默认
    Route::get('/answerActivityUnit/downloadUnitQr', [Admin\AnswerActivityUnitController::class, 'downloadUnitQr']); // 下载所有单位二维码
    Route::post('/answerActivityUnit/selectBaseUnit', [Admin\AnswerActivityUnitController::class, 'selectBaseUnit']); // 从基础数据库中选择单位到活动里面


    /**
     * 答题活动限制地址
     */
    Route::get('/answerActivityLimitAddres/list', [Admin\AnswerActivityLimitAddressController::class, 'lists']); //  列表
    Route::get('/answerActivityLimitAddres/detail', [Admin\AnswerActivityLimitAddressController::class, 'detail']); // 详情
    Route::post('/answerActivityLimitAddres/add', [Admin\AnswerActivityLimitAddressController::class, 'add']); // 新增
    Route::post('/answerActivityLimitAddres/change', [Admin\AnswerActivityLimitAddressController::class, 'change']); // 编辑
    Route::post('/answerActivityLimitAddres/del', [Admin\AnswerActivityLimitAddressController::class, 'del']); // 删除

    /**
     * 答题活动题库管理
     */
    Route::get('/answerActivityProblem/list', [Admin\AnswerActivityProblemController::class, 'lists']); //  列表
    Route::get('/answerActivityProblem/detail', [Admin\AnswerActivityProblemController::class, 'detail']); // 详情
    Route::post('/answerActivityProblem/add', [Admin\AnswerActivityProblemController::class, 'add']); // 新增
    Route::post('/answerActivityProblem/change', [Admin\AnswerActivityProblemController::class, 'change']); // 编辑
    Route::post('/answerActivityProblem/del', [Admin\AnswerActivityProblemController::class, 'del']); // 删除
    Route::post('/answerActivityProblem/delMany', [Admin\AnswerActivityProblemController::class, 'delMany']); // 删除多个问题
    Route::post('/answerActivityProblem/disablingAndEnabling', [Admin\AnswerActivityProblemController::class, 'disablingAndEnabling']); // 禁用与启用
    Route::post('/answerActivityProblem/analysisShowAndHidden', [Admin\AnswerActivityProblemController::class, 'analysisShowAndHidden']); // 显示与隐藏解析

    /**
     * 答题活动礼物管理
     */
    Route::get('/answerActivityGift/list', [Admin\AnswerActivityGiftController::class, 'lists']); //  列表
    Route::get('/answerActivityGift/detail', [Admin\AnswerActivityGiftController::class, 'detail']); // 详情
    Route::post('/answerActivityGift/add', [Admin\AnswerActivityGiftController::class, 'add']); // 新增
    Route::post('/answerActivityGift/change', [Admin\AnswerActivityGiftController::class, 'change']); // 编辑
    Route::post('/answerActivityGift/del', [Admin\AnswerActivityGiftController::class, 'del']); // 删除

    /**
     * 答题活动礼物公示管理
     */
    Route::get('/answerActivityGiftPublic/list', [Admin\AnswerActivityGiftPublicController::class, 'lists']); //  列表
    Route::get('/answerActivityGiftPublic/detail', [Admin\AnswerActivityGiftPublicController::class, 'detail']); // 详情
    Route::post('/answerActivityGiftPublic/add', [Admin\AnswerActivityGiftPublicController::class, 'add']); // 新增
    Route::post('/answerActivityGiftPublic/change', [Admin\AnswerActivityGiftPublicController::class, 'change']); // 编辑
    Route::post('/answerActivityGiftPublic/del', [Admin\AnswerActivityGiftPublicController::class, 'del']); // 删除

    /**
     * 答题活动礼物排版管理
     */
    Route::get('/answerActivityGiftTime/list', [Admin\AnswerActivityGiftTimeController::class, 'lists']); //  列表
    Route::get('/answerActivityGiftTime/detail', [Admin\AnswerActivityGiftTimeController::class, 'detail']); // 详情
    Route::post('/answerActivityGiftTime/add', [Admin\AnswerActivityGiftTimeController::class, 'add']); // 新增
    Route::post('/answerActivityGiftTime/change', [Admin\AnswerActivityGiftTimeController::class, 'change']); // 编辑
    Route::post('/answerActivityGiftTime/del', [Admin\AnswerActivityGiftTimeController::class, 'del']); // 删除

    Route::get('/answerActivityGiftTime/getSendTotalConfig', [Admin\AnswerActivityGiftTimeController::class, 'getSendTotalConfig']); //  获取礼物发放总概率
    Route::post('/answerActivityGiftTime/setSendTotalConfig', [Admin\AnswerActivityGiftTimeController::class, 'setSendTotalConfig']); // 设置礼物发放总概率

    /**
     * 答题活动获奖列表
     */
    Route::get('/answerActivityUserGift/getUnitList', [Admin\AnswerActivityUserGiftController::class, 'getUnitList']); // 答题活动，获取单位列表
    Route::get('/answerActivityUserGift/list', [Admin\AnswerActivityUserGiftController::class, 'lists']); // 答题活动，获奖列表
    Route::post('/answerActivityUserGift/userGiftGrant', [Admin\AnswerActivityUserGiftController::class, 'userGiftGrant']); // 奖品领取或发奖
    Route::post('/answerActivityUserGift/userPacketTransfer', [Admin\AnswerActivityUserGiftController::class, 'userPacketTransfer']); // 红包转账失败，进行重新转账


    /**
     * 活动资源管理
     */
    Route::get('/answerActivityResource/getColorResource', [Admin\AnswerActivityResourceController::class, 'getColorResource']); // 获取系统资源配置 （颜色配置）
    Route::post('/answerActivityResource/setColorResource', [Admin\AnswerActivityResourceController::class, 'setColorResource']); // 设置系统资源配置 （颜色配置）
    Route::get('/answerActivityResource/resourceDownload', [Admin\AnswerActivityResourceController::class, 'resourceDownload']); // 下载ui资源文件
    Route::post('/answerActivityResource/resourceUpload', [Admin\AnswerActivityResourceController::class, 'resourceUpload']); // 上传ui资源文件
    Route::get('/answerActivityResource/getResourceTemplateParam', [Admin\AnswerActivityResourceController::class, 'getResourceTemplateParam']); // 获取资源图片模板列表
    Route::get('/answerActivityResource/getDefaultResourceAddr', [Admin\AnswerActivityResourceController::class, 'getDefaultResourceAddr']); // 获取默认活动资源路径

    Route::get('/answerActivityResource/getWordResource', [Admin\AnswerActivityResourceController::class, 'getWordResource']); // 获取文字资源
    Route::post('/answerActivityResource/setWordResource', [Admin\AnswerActivityResourceController::class, 'setWordResource']); // 文字资源配置


    /**
     * 活动排名
     */
    Route::get('/answerActivityRanking/ranking', [Admin\AnswerActivityRankingController::class, 'ranking']); // 活动排名
    Route::post('/answerActivityRanking/rankingTransfer', [Admin\AnswerActivityRankingController::class, 'rankingTransfer']); // 活动排名转账


    /**
     * 线上答题活动 数据分析
     */
    Route::get('/answerActivityDataAnalysis/dataStatistics', [Admin\AnswerActivityDataAnalysisController::class, 'dataStatistics']); // 数据统计
    Route::get('/answerActivityDataAnalysis/answerStatistics', [Admin\AnswerActivityDataAnalysisController::class, 'answerStatistics']); // 答题次数统计
    Route::get('/answerActivityDataAnalysis/giftGrantStatistics', [Admin\AnswerActivityDataAnalysisController::class, 'giftGrantStatistics']); // 礼物发放统计
    Route::get('/answerActivityDataAnalysis/unitStatisticsList', [Admin\AnswerActivityDataAnalysisController::class, 'unitStatisticsList']); // 单位数据统计列表
    Route::get('/answerActivityDataAnalysis/systemGiftGrantStatistics', [Admin\AnswerActivityDataAnalysisController::class, 'systemGiftGrantStatistics']); // 系统投放礼物数据

    /**
     * 线上大赛类型
     */
    Route::get('/contestActivityType/filterList', [Admin\ContestActivityTypeController::class, 'filterList']); // 筛选列表
    Route::get('/contestActivityType/list', [Admin\ContestActivityTypeController::class, 'lists']); // 列表
    Route::get('/contestActivityType/detail', [Admin\ContestActivityTypeController::class, 'detail']); // 详情
    Route::post('/contestActivityType/add', [Admin\ContestActivityTypeController::class, 'add']); // 新增
    Route::post('/contestActivityType/change', [Admin\ContestActivityTypeController::class, 'change']); // 编辑
    Route::post('/contestActivityType/del', [Admin\ContestActivityTypeController::class, 'del']); // 删除


    /**
     * 线上大赛 原创申明   原创申请，只在2个地方配置；1.创建大赛 2.单位管理
     */
    // Route::get('/contestActivityDeclare/detail', [Admin\ContestActivityDeclareController::class, 'detail']); // 详情
    // Route::post('/contestActivityDeclare/change', [Admin\ContestActivityDeclareController::class, 'change']); //编辑

    /**
     * 线上大赛
     */
    Route::get('/contestActivity/getContestActivityApplyParam', [Admin\ContestActivityController::class, 'getContestActivityApplyParam']); // 获取报名参数
    Route::get('/contestActivity/list', [Admin\ContestActivityController::class, 'lists']); // 列表
    Route::get('/contestActivity/detail', [Admin\ContestActivityController::class, 'detail']); // 详情
    Route::post('/contestActivity/add', [Admin\ContestActivityController::class, 'add']); // 新增
    Route::post('/contestActivity/change', [Admin\ContestActivityController::class, 'change']); // 编辑
    Route::post('/contestActivity/del', [Admin\ContestActivityController::class, 'del']); // 删除
    Route::post('/contestActivity/playAndCancel', [Admin\ContestActivityController::class, 'playAndCancel']); // 发布与取消发布

    /**
     * 线上大赛作品
     */
    Route::get('/contestActivityWorks/getLibName', [Admin\ContestActivityWorksController::class, 'getLibName']); // 获取图书馆列表（和总馆的可能不一样）
    Route::get('/contestActivityWorks/list', [Admin\ContestActivityWorksController::class, 'lists']); // 列表
    Route::get('/contestActivityWorks/detail', [Admin\ContestActivityWorksController::class, 'detail']); // 详情
    Route::post('/contestActivityWorks/check', [Admin\ContestActivityWorksController::class, 'worksCheck']); // 新增
    Route::post('/contestActivityWorks/violateAndCancel', [Admin\ContestActivityWorksController::class, 'violateAndCancel']); // 作品违规 与 取消违规操作

    Route::get('/contestActivityWorks/downloadWorks', [Admin\ContestActivityWorksController::class, 'downloadWorks']); //  单个下载作品
    Route::get('/contestActivityWorks/downloadWorksAll', [Admin\ContestActivityWorksController::class, 'downloadWorksAll']); //  批量下载作品


    /**
     * 线上大赛单位管理
     */
    Route::get('/contestActivityUnit/getNatureName', [Admin\ContestActivityUnitController::class, 'getNatureName']); //获取单位性质
    Route::get('/contestActivityUnit/filterList', [Admin\ContestActivityUnitController::class, 'filterList']); //类型(用于下拉框选择)
    Route::get('/contestActivityUnit/list', [Admin\ContestActivityUnitController::class, 'lists']); //  列表
    Route::get('/contestActivityUnit/detail', [Admin\ContestActivityUnitController::class, 'detail']); // 详情
    Route::post('/contestActivityUnit/add', [Admin\ContestActivityUnitController::class, 'add']); // 新增
    Route::post('/contestActivityUnit/change', [Admin\ContestActivityUnitController::class, 'change']); // 编辑
    Route::post('/contestActivityUnit/del', [Admin\ContestActivityUnitController::class, 'del']); // 删除
    Route::post('/contestActivityUnit/orderChange', [Admin\ContestActivityUnitController::class, 'orderChange']); // 修改区域排序

    /**
     * 在线抽奖活动类
     */
    Route::get('/turnActivity/list', [Admin\TurnActivityController::class, 'lists']); // 列表
    Route::get('/turnActivity/detail', [Admin\TurnActivityController::class, 'detail']); // 详情
    Route::post('/turnActivity/add', [Admin\TurnActivityController::class, 'add']); // 新增
    Route::post('/turnActivity/change', [Admin\TurnActivityController::class, 'change']); // 编辑
    Route::post('/turnActivity/del', [Admin\TurnActivityController::class, 'del']); // 删除
    Route::post('/turnActivity/cancelAndRelease', [Admin\TurnActivityController::class, 'cancelAndRelease']); // 撤销 和发布
    Route::post('/turnActivity/invitCodeChange', [Admin\TurnActivityController::class, 'invitCodeChange']); // 邀请码修改

    Route::post('/turnActivity/copyData', [Admin\TurnActivityController::class, 'copyData']); // 复制活动

    /**
     * 在线抽奖活动礼物管理
     */
    Route::get('/turnActivityGift/list', [Admin\TurnActivityGiftController::class, 'lists']); //  列表
    Route::get('/turnActivityGift/detail', [Admin\TurnActivityGiftController::class, 'detail']); // 详情
    Route::post('/turnActivityGift/add', [Admin\TurnActivityGiftController::class, 'add']); // 新增
    Route::post('/turnActivityGift/change', [Admin\TurnActivityGiftController::class, 'change']); // 编辑
    Route::post('/turnActivityGift/del', [Admin\TurnActivityGiftController::class, 'del']); // 删除

    /**
     * 在线抽奖活动活动资源管理
     */
    Route::get('/turnActivityResource/getColorResource', [admin\TurnActivityResourceController::class, 'getColorResource']); // 获取系统资源配置 （颜色配置）
    Route::post('/turnActivityResource/setColorResource', [admin\TurnActivityResourceController::class, 'setColorResource']); // 设置系统资源配置 （颜色配置）
    Route::get('/turnActivityResource/resourceDownload', [admin\TurnActivityResourceController::class, 'resourceDownload']); // 下载ui资源文件
    Route::post('/turnActivityResource/resourceUpload', [admin\TurnActivityResourceController::class, 'resourceUpload']); // 上传ui资源文件
    Route::get('/turnActivityResource/getDefaultResourceAddr', [Admin\TurnActivityResourceController::class, 'getDefaultResourceAddr']); // 获取默认活动资源路径


    /**
     * 答题活动限制地址
     */
    Route::get('/turnActivityLimitAddress/list', [Admin\TurnActivityLimitAddressController::class, 'lists']); //  列表
    Route::get('/turnActivityLimitAddress/detail', [Admin\TurnActivityLimitAddressController::class, 'detail']); // 详情
    Route::post('/turnActivityLimitAddress/add', [Admin\TurnActivityLimitAddressController::class, 'add']); // 新增
    Route::post('/turnActivityLimitAddress/change', [Admin\TurnActivityLimitAddressController::class, 'change']); // 编辑
    Route::post('/turnActivityLimitAddress/del', [Admin\TurnActivityLimitAddressController::class, 'del']); // 删除

    /**
     * 在线抽奖活动礼物排版管理
     */
    Route::get('/turnActivityGiftTime/list', [Admin\TurnActivityGiftTimeController::class, 'lists']); //  列表
    Route::get('/turnActivityGiftTime/detail', [Admin\TurnActivityGiftTimeController::class, 'detail']); // 详情
    Route::post('/turnActivityGiftTime/add', [Admin\TurnActivityGiftTimeController::class, 'add']); // 新增
    Route::post('/turnActivityGiftTime/change', [Admin\TurnActivityGiftTimeController::class, 'change']); // 编辑
    Route::post('/turnActivityGiftTime/del', [Admin\TurnActivityGiftTimeController::class, 'del']); // 删除

    Route::get('/turnActivityGiftTime/getSendTotalConfig', [Admin\TurnActivityGiftTimeController::class, 'getSendTotalConfig']); //  获取礼物发放总概率
    Route::post('/turnActivityGiftTime/setSendTotalConfig', [Admin\TurnActivityGiftTimeController::class, 'setSendTotalConfig']); // 设置礼物发放总概率

    /**
     * 在线抽奖活动获奖列表
     */
    Route::get('/turnActivityUserGift/list', [Admin\TurnActivityUserGiftController::class, 'lists']); // 答题活动，获奖列表
    Route::post('/turnActivityUserGift/userGiftGrant', [Admin\TurnActivityUserGiftController::class, 'userGiftGrant']); // 奖品领取或发奖

    /**
     * 线上答题活动 数据分析
     */
    Route::get('/turnActivityDataAnalysis/dataStatistics', [Admin\TurnActivityDataAnalysisController::class, 'dataStatistics']); // 数据统计
    Route::get('/turnActivityDataAnalysis/drawStatistics', [Admin\TurnActivityDataAnalysisController::class, 'drawStatistics']); // 抽奖次数统计
    Route::get('/turnActivityDataAnalysis/giftGrantStatistics', [Admin\TurnActivityDataAnalysisController::class, 'giftGrantStatistics']); // 礼物发放统计
    //  Route::get('/turnActivityDataAnalysis/systemGiftGrantStatistics', [Admin\TurnActivityDataAnalysisController::class, 'systemGiftGrantStatistics']); // 系统投放礼物数据

    /**
     * 导出
     */
    Route::get('/answerActivityBaseUnitExport/getUnitList', [Controllers\Export\AnswerActivityBaseUnitExport::class, 'getUnitList']); // 基础单位导出
    Route::get('/answerActivityUnitExport/getUnitList', [Controllers\Export\AnswerActivityUnitExport::class, 'getUnitList']); // 活动单位导出
    Route::get('/answerActivityUnitExport/template', [Controllers\Export\AnswerActivityUnitExport::class, 'template']); // 活动单位模板导出
    Route::get('/answerActivityRankingExport/ranking', [Controllers\Export\AnswerActivityRankingExport::class, 'ranking']); // 活动排名导出

    Route::get('/answerActivityProblemExport/index', [Controllers\Export\AnswerActivityProblemExport::class, 'index']); // 活动题库导出
    Route::get('/answerActivityProblemExport/template', [Controllers\Export\AnswerActivityProblemExport::class, 'template']); // 活动题库模板导出

    Route::get('/answerActivityUserGiftExport/index', [Controllers\Export\AnswerActivityUserGiftExport::class, 'index']); // 答题活动-用户活动获取礼物导出
    Route::get('/turnActivityUserGiftExport/index', [Controllers\Export\TurnActivityUserGiftExport::class, 'index']); // 在线抽奖活动-用户活动获取礼物导出


    /**
     * 导入
     */
    Route::post('/answerActivityUnitImport/index', [Controllers\Import\AnswerActivityUnitImport::class, 'index']); // 活动单位导入
    Route::post('/answerActivityProblemImport/index', [Controllers\Import\AnswerActivityProblemImport::class, 'index']); // 活动题库导入

    ######################################################抽奖活动end#############################################################

    ######################################################图片直播start#############################################################
    /**
     * 图片直播管理
     */
    Route::get('/pictureLive/list', [Admin\PictureLiveController::class, 'lists']); //  列表
    Route::get('/pictureLive/detail', [Admin\PictureLiveController::class, 'detail']); //  详情
    Route::post('/pictureLive/add', [Admin\PictureLiveController::class, 'add']); //  新增
    Route::post('/pictureLive/change', [Admin\PictureLiveController::class, 'change']); //  修改
    Route::post('/pictureLive/del', [Admin\PictureLiveController::class, 'del']); //  删除
    Route::post('/pictureLive/cancelAndRelease', [Admin\PictureLiveController::class, 'cancelAndRelease']); //  撤销 和发布
    /**
     * 图片直播黑名单用户
     */
    Route::get('/pictureLiveBlack/list', [Admin\PictureLiveBlackController::class, 'lists']); //  黑名单用户列表
    Route::post('/pictureLiveBlack/add', [Admin\PictureLiveBlackController::class, 'add']); //  黑名单用户新增
    Route::post('/pictureLiveBlack/del', [Admin\PictureLiveBlackController::class, 'del']); //  黑名单用户删除


    /**
     * 图片直播指定用户
     */
    Route::get('/pictureLiveAppoint/list', [Admin\PictureLiveAppointController::class, 'lists']); //  指定用户列表
    Route::post('/pictureLiveAppoint/add', [Admin\PictureLiveAppointController::class, 'add']); //  指定用户新增
    Route::post('/pictureLiveAppoint/del', [Admin\PictureLiveAppointController::class, 'del']); //  指定用户删除

    /**
     * 直播活动作品管理
     */
    Route::get('/pictureLiveWorks/list', [Admin\PictureLiveWorksController::class, 'lists']); //  列表
    Route::get('/pictureLiveWorks/detail', [Admin\PictureLiveWorksController::class, 'detail']); //  详情
    Route::post('/pictureLiveWorks/worksCheck', [Admin\PictureLiveWorksController::class, 'worksCheck']); //  审核作品


    Route::post('/pictureLiveWorks/add', [Admin\PictureLiveWorksController::class, 'add']); //  图片直播，上传图片（单张上传）
    Route::post('/pictureLiveWorks/batchAdd', [Admin\PictureLiveWorksController::class, 'batchAdd']); //  图片直播，上传图片(批量上传)
    Route::post('/pictureLiveWorks/change', [Admin\PictureLiveWorksController::class, 'change']); //  修改作品上传用户信息
    Route::post('/pictureLiveWorks/del', [Admin\PictureLiveWorksController::class, 'del']); //  删除作品

    Route::get('/pictureLiveWorks/downloadWorksImg', [Admin\PictureLiveWorksController::class, 'downloadWorksImg']); //  下载直播图片

    ######################################################视频直播start#############################################################
    /**
     * 视频直播管理
     */
    Route::get('/videoLive/list', [Admin\VideoLiveController::class, 'lists']); //  列表
    Route::get('/videoLive/detail', [Admin\VideoLiveController::class, 'detail']); //  详情
    Route::post('/videoLive/add', [Admin\VideoLiveController::class, 'add']); //  新增
    Route::post('/videoLive/change', [Admin\VideoLiveController::class, 'change']); //  修改
    Route::post('/videoLive/del', [Admin\VideoLiveController::class, 'del']); //  删除
    Route::post('/videoLive/cancelAndRelease', [Admin\VideoLiveController::class, 'cancelAndRelease']); //  撤销 和发布
    Route::post('/videoLive/changeLiveStatus', [Admin\VideoLiveController::class, 'changeLiveStatus']); //  修改直播状态
    Route::post('/videoLive/setVideoCallbackAddress', [Admin\VideoLiveController::class, 'setVideoCallbackAddress']); //  设置视频录制回调地址
    Route::get('/videoLive/getVideoCallbackAddress', [Admin\VideoLiveController::class, 'getVideoCallbackAddress']); //  获取视频录制回调地址
    Route::post('/videoLive/setChannelTranscribeAddrByState', [Admin\VideoLiveController::class, 'setChannelTranscribeAddrByState']); //  设置频道状态变化回调地址（程序调用）
    Route::get('/videoLive/getChannelTranscribeAddrByState', [Admin\VideoLiveController::class, 'getChannelTranscribeAddrByState']); //  获取频道状态变化回调地址（程序调用）



    /******************************************扫码导游***************************************************/
    /**
     * 展览配置类
     */
    Route::get('/codeGuideExhibition/list', [Admin\CodeGuideExhibitionController::class, 'lists']); // 列表
    Route::get('/codeGuideExhibition/detail', [Admin\CodeGuideExhibitionController::class, 'detail']); // 详情
    Route::post('/codeGuideExhibition/add', [Admin\CodeGuideExhibitionController::class, 'add']); // 新增
    Route::post('/codeGuideExhibition/change', [Admin\CodeGuideExhibitionController::class, 'change']); // 编辑
    Route::post('/codeGuideExhibition/del', [Admin\CodeGuideExhibitionController::class, 'del']); // 删除
    Route::post('/codeGuideExhibition/cancelAndRelease', [Admin\CodeGuideExhibitionController::class, 'cancelAndRelease']); //  撤销 和发布



    /**
     * 展览作品配置类
     */
    Route::get('/codeGuideProduction/list', [Admin\CodeGuideProductionController::class, 'lists']); // 列表
    Route::get('/codeGuideProduction/detail', [Admin\CodeGuideProductionController::class, 'detail']); // 详情
    Route::post('/codeGuideProduction/add', [Admin\CodeGuideProductionController::class, 'add']); // 新增
    Route::post('/codeGuideProduction/change', [Admin\CodeGuideProductionController::class, 'change']); // 编辑
    Route::post('/codeGuideProduction/del', [Admin\CodeGuideProductionController::class, 'del']); // 删除

    /**
     * 展览作品类型
     */
    Route::get('/codeGuideProductionType/filterList', [Admin\CodeGuideProductionTypeController::class, 'filterList']); // 筛选列表
    Route::get('/codeGuideProductionType/list', [Admin\CodeGuideProductionTypeController::class, 'lists']); // 列表
    Route::get('/codeGuideProductionType/detail', [Admin\CodeGuideProductionTypeController::class, 'detail']); // 详情
    Route::post('/codeGuideProductionType/add', [Admin\CodeGuideProductionTypeController::class, 'add']); // 新增
    Route::post('/codeGuideProductionType/change', [Admin\CodeGuideProductionTypeController::class, 'change']); // 编辑
    Route::post('/codeGuideProductionType/del', [Admin\CodeGuideProductionTypeController::class, 'del']); // 删除


    /******************************************空间预约***************************************************/
    /**
     * 预约
     */
    Route::get('/studyRoomReservation/getReservationApplyParam', [Admin\StudyRoomReservationController::class, 'getReservationApplyParam']); // 获取预约参数
    Route::get('/studyRoomReservation/filterTagList', [Admin\StudyRoomReservationController::class, 'filterTagList']); //文化配送标签列表（预定义）
    Route::get('/studyRoomReservation/getFilterList', [Admin\StudyRoomReservationController::class, 'getFilterList']); //获取筛选列表
    Route::get('/studyRoomReservation/list', [Admin\StudyRoomReservationController::class, 'lists']); //列表
    Route::get('/studyRoomReservation/detail', [Admin\StudyRoomReservationController::class, 'detail']); //详情
    Route::post('/studyRoomReservation/add', [Admin\StudyRoomReservationController::class, 'add']); //新增
    Route::post('/studyRoomReservation/change', [Admin\StudyRoomReservationController::class, 'change']); //修改
    Route::post('/studyRoomReservation/del', [Admin\StudyRoomReservationController::class, 'del']); //删除
    Route::post('/studyRoomReservation/playAndCancel', [Admin\StudyRoomReservationController::class, 'playAndCancel']); //推荐与取消推荐

    //特殊排班时间
    Route::get('/studyRoomReservationSpecialSchedule/list', [Admin\StudyRoomReservationSpecialScheduleController::class, 'lists']); //特殊时间列表
    Route::post('/studyRoomReservationSpecialSchedule/add', [Admin\StudyRoomReservationSpecialScheduleController::class, 'add']); //特殊时间新增
    Route::post('/studyRoomReservationSpecialSchedule/change', [Admin\StudyRoomReservationSpecialScheduleController::class, 'change']); //特殊时间编辑
    Route::get('/studyRoomReservationSpecialSchedule/detail', [Admin\StudyRoomReservationSpecialScheduleController::class, 'detail']); //特殊时间详情
    Route::post('/studyRoomReservationSpecialSchedule/del', [Admin\StudyRoomReservationSpecialScheduleController::class, 'del']); //特殊时间删除

    Route::get('/studyRoomReservationApply/list', [Admin\StudyRoomReservationApplyController::class, 'lists']); //预约申请列表
    Route::get('/studyRoomReservationApply/reservationApplyHistory', [Admin\StudyRoomReservationApplyController::class, 'reservationApplyHistory']); //获取用户对于某个预约的预约记录
    Route::post('/studyRoomReservationApply/agreeAndRefused', [Admin\StudyRoomReservationApplyController::class, 'agreeAndRefused']); //审核通过 和 拒绝
    Route::post('/studyRoomReservationApply/violateAndCancel', [Admin\StudyRoomReservationApplyController::class, 'violateAndCancel']); //预约违规 与 取消违规操作

    Route::post('/studyRoomReservationApply/applyCancel', [Admin\StudyRoomReservationApplyController::class, 'applyCancel']); //管理员取消预约
    Route::post('/studyRoomReservationApply/applySignIn', [Admin\StudyRoomReservationApplyController::class, 'applySignIn']); //管理员打卡签到
    Route::post('/studyRoomReservationApply/applySignOut', [Admin\StudyRoomReservationApplyController::class, 'applySignOut']); //管理员打卡签退

    /**
     * 白名单配置
     */
    Route::get('studyRoomReservationWhite/list', [Admin\StudyRoomReservationWhiteController::class, 'lists']); //列表
    Route::post('studyRoomReservationWhite/add', [Admin\StudyRoomReservationWhiteController::class, 'add']); //添加
    Route::post('studyRoomReservationWhite/change',  [Admin\StudyRoomReservationWhiteController::class, 'change']); //修改
    Route::post('studyRoomReservationWhite/del',  [Admin\StudyRoomReservationWhiteController::class, 'del']); //删除
    Route::get('studyRoomReservationWhite/getApplyQr',  [Admin\StudyRoomReservationWhiteController::class, 'getApplyQr']); //出示二维码

    /**
     * 远程操作
     */
    Route::get('studyRoomReservationRemoteOperation/list', [Admin\StudyRoomReservationRemoteOperationController::class, 'lists']); //远程操作列表
    Route::post('studyRoomReservationRemoteOperation/remoteOperation', [Admin\StudyRoomReservationRemoteOperationController::class, 'remoteOperation']); //远程操作

    /******************************************室内导航配置***************************************************/

    /**
     * 室内导航区域配置
     */
    Route::get('/navigationArea/filterList', [Admin\NavigationAreaController::class, 'filterList']); //筛选列表
    Route::get('/navigationArea/list', [Admin\NavigationAreaController::class, 'lists']); //列表
    Route::get('/navigationArea/detail', [Admin\NavigationAreaController::class, 'detail']); //详情
    Route::post('/navigationArea/add', [Admin\NavigationAreaController::class, 'add']); //新增
    Route::post('/navigationArea/change', [Admin\NavigationAreaController::class, 'change']); //修改
    Route::post('/navigationArea/del', [Admin\NavigationAreaController::class, 'del']); //删除
    Route::post('/navigationArea/playAndCancel', [Admin\NavigationAreaController::class, 'playAndCancel']); //推荐与取消推荐
    Route::post('/navigationArea/sortChange', [Admin\NavigationAreaController::class, 'sortChange']); //修改区域排序

    /**
     * 室内导航建筑配置
     */
    Route::get('/navigationBuild/list', [Admin\NavigationBuildController::class, 'lists']); //列表
    Route::get('/navigationBuild/detail', [Admin\NavigationBuildController::class, 'detail']); //详情
    Route::post('/navigationBuild/add', [Admin\NavigationBuildController::class, 'add']); //新增
    Route::post('/navigationBuild/change', [Admin\NavigationBuildController::class, 'change']); //修改
    Route::post('/navigationBuild/del', [Admin\NavigationBuildController::class, 'del']); //删除
    Route::post('/navigationBuild/playAndCancel', [Admin\NavigationBuildController::class, 'playAndCancel']); //推荐与取消推荐
    /**
     *  室内导航点位配置
     */
    Route::get('/navigationPoint/list', [Admin\NavigationPointController::class, 'lists']); //列表
    Route::get('/navigationPoint/detail', [Admin\NavigationPointController::class, 'detail']); //详情
    Route::post('/navigationPoint/add', [Admin\NavigationPointController::class, 'add']); //新增
    Route::post('/navigationPoint/change', [Admin\NavigationPointController::class, 'change']); //修改
    Route::post('/navigationPoint/del', [Admin\NavigationPointController::class, 'del']); //删除
    Route::post('/navigationPoint/playAndCancel', [Admin\NavigationPointController::class, 'playAndCancel']); //推荐与取消推荐
    Route::post('/navigationPoint/sortChange', [Admin\NavigationAreaController::class, 'sortChange']); //修改区域排序

    /**
     *  应用邀请码类
     */
    Route::get('/appInviteCode/filterList', [Admin\AppInviteCodeController::class, 'filterList']); //用于下拉框选择
    Route::get('/appInviteCode/list', [Admin\AppInviteCodeController::class, 'lists']); //列表
    Route::get('/appInviteCode/detail', [Admin\AppInviteCodeController::class, 'detail']); //详情
    Route::post('/appInviteCode/add', [Admin\AppInviteCodeController::class, 'add']); //新增
    Route::post('/appInviteCode/change', [Admin\AppInviteCodeController::class, 'change']); //修改
    Route::post('/appInviteCode/del', [Admin\AppInviteCodeController::class, 'del']); //删除
    /**
     *  应用邀请码行为类
     */
    Route::get('/appInviteCodeBehavior/filterList', [Admin\AppInviteCodeBehaviorController::class, 'filterList']); //类型(用于下拉框选择)
    Route::get('/appInviteCodeBehavior/list', [Admin\AppInviteCodeBehaviorController::class, 'lists']); //列表
    Route::get('/appInviteCodeBehavior/accessStatistics', [Admin\AppInviteCodeBehaviorController::class, 'accessStatistics']); //应用访问总统计图






    /**
     * 线上大赛作品类型
     */
    Route::get('/competiteActivityWorksType/filterList', [Admin\CompetiteActivityWorksTypeController::class, 'filterList']); // 筛选列表
    Route::get('/competiteActivityWorksType/list', [Admin\CompetiteActivityWorksTypeController::class, 'lists']); // 列表
    Route::get('/competiteActivityWorksType/detail', [Admin\CompetiteActivityWorksTypeController::class, 'detail']); // 详情
    Route::post('/competiteActivityWorksType/add', [Admin\CompetiteActivityWorksTypeController::class, 'add']); // 新增
    Route::post('/competiteActivityWorksType/change', [Admin\CompetiteActivityWorksTypeController::class, 'change']); // 编辑
    Route::post('/competiteActivityWorksType/del', [Admin\CompetiteActivityWorksTypeController::class, 'del']); // 删除

    /**
     * 线上大赛
     */
    Route::get('/competiteActivity/getCompetiteActivityApplyParam', [Admin\CompetiteActivityController::class, 'getCompetiteActivityApplyParam']); // 获取报名参数
    Route::get('/competiteActivity/filterList', [Admin\CompetiteActivityController::class, 'filterList']); // 筛选列表
    Route::get('/competiteActivity/list', [Admin\CompetiteActivityController::class, 'lists']); // 列表
    Route::get('/competiteActivity/detail', [Admin\CompetiteActivityController::class, 'detail']); // 详情
    Route::post('/competiteActivity/add', [Admin\CompetiteActivityController::class, 'add']); // 新增
    Route::post('/competiteActivity/change', [Admin\CompetiteActivityController::class, 'change']); // 编辑
    Route::post('/competiteActivity/del', [Admin\CompetiteActivityController::class, 'del']); // 删除
    Route::post('/competiteActivity/playAndCancel', [Admin\CompetiteActivityController::class, 'playAndCancel']); // 发布与取消发布


    /**
     * 征集活动类型
     */
    Route::get('/competiteActivityType/filterList', [Admin\CompetiteActivityTypeController::class, 'filterList']); // 筛选列表
    Route::get('/competiteActivityType/list', [Admin\CompetiteActivityTypeController::class, 'lists']); // 列表
    Route::get('/competiteActivityType/detail', [Admin\CompetiteActivityTypeController::class, 'detail']); // 详情
    Route::post('/competiteActivityType/add', [Admin\CompetiteActivityTypeController::class, 'add']); // 新增
    Route::post('/competiteActivityType/change', [Admin\CompetiteActivityTypeController::class, 'change']); // 编辑
    Route::post('/competiteActivityType/del', [Admin\CompetiteActivityTypeController::class, 'del']); // 删除

    /**
     * 征集活动标签
     */
    Route::get('/competiteActivityTag/filterList', [Admin\CompetiteActivityTagController::class, 'filterList']); // 筛选列表
    Route::get('/competiteActivityTag/list', [Admin\CompetiteActivityTagController::class, 'lists']); // 列表
    Route::get('/competiteActivityTag/detail', [Admin\CompetiteActivityTagController::class, 'detail']); // 详情
    Route::post('/competiteActivityTag/add', [Admin\CompetiteActivityTagController::class, 'add']); // 新增
    Route::post('/competiteActivityTag/change', [Admin\CompetiteActivityTagController::class, 'change']); // 编辑
    Route::post('/competiteActivityTag/del', [Admin\CompetiteActivityTagController::class, 'del']); // 删除


    /**
     * 线上大赛作品
     */
    Route::get('/competiteActivityWorks/list', [Admin\CompetiteActivityWorksController::class, 'lists']); // 列表
    Route::get('/competiteActivityWorks/detail', [Admin\CompetiteActivityWorksController::class, 'detail']); // 详情

    Route::get('/competiteActivityWorks/getNextUnchecked', [Admin\CompetiteActivityWorksController::class, 'getNextUnchecked']); // 管理员自动获取下一个未审核作品
    Route::get('/competiteActivityWorks/getNextUnScoreed', [Admin\CompetiteActivityWorksController::class, 'getNextUnScoreed']); // 管理员自动获取下一个未打分作品

    Route::post('/competiteActivityWorks/check', [Admin\CompetiteActivityWorksController::class, 'worksCheck']); // 审核
    Route::post('/competiteActivityWorks/reviewScore', [Admin\CompetiteActivityWorksController::class, 'worksReviewScore']); // 作品评审打分
    Route::post('/competiteActivityWorks/violateAndCancel', [Admin\CompetiteActivityWorksController::class, 'violateAndCancel']); // 作品违规 与 取消违规操作

    Route::get('/competiteActivityWorks/downloadWorks', [Admin\CompetiteActivityWorksController::class, 'downloadWorks']); //  单个下载作品
    Route::get('/competiteActivityWorks/downloadWorksAll', [Admin\CompetiteActivityWorksController::class, 'downloadWorksAll']); //  批量下载作品
    Route::post('/competiteActivityWorks/showAndCancel', [Admin\CompetiteActivityWorksController::class, 'showAndCancel']); //  显示与取消显示
    Route::post('/competiteActivityWorks/worksDelete', [Admin\CompetiteActivityWorksController::class, 'worksDelete']); //  删除作品
    Route::post('/competiteActivityWorks/worksRefuseDirectly', [Admin\CompetiteActivityWorksController::class, 'worksRefuseDirectly']); //  一键拒绝作品（最高管理员）

    Route::get('/competiteActivityWorksExport/index', [Controllers\Export\CompetiteActivityWorksExport::class, 'index']); //  大赛作品导出

    Route::get('/competiteActivityWorksScore/scoreList', [Admin\CompetiteActivityWorksScoreController::class, 'scoreLists']); // 打分列表
    Route::get('/competiteActivityWorksScore/scoreDetail', [Admin\CompetiteActivityWorksScoreController::class, 'scoreDetail']); // 打分详情
    Route::get('/competiteActivityWorksScore/getNextUnScoreWorks', [Admin\CompetiteActivityWorksScoreController::class, 'getNextUnScoreWorks']); // 管理员自动获取下一个未打分作品


    /**
     * 线上大赛单位管理
     */
    Route::get('/competiteActivityGroup/filterList', [Admin\CompetiteActivityGroupController::class, 'filterList']); //类型(用于下拉框选择)
    Route::get('/competiteActivityGroup/list', [Admin\CompetiteActivityGroupController::class, 'lists']); //  列表
    Route::get('/competiteActivityGroup/detail', [Admin\CompetiteActivityGroupController::class, 'detail']); // 详情
    Route::post('/competiteActivityGroup/add', [Admin\CompetiteActivityGroupController::class, 'add']); // 新增
    Route::post('/competiteActivityGroup/change', [Admin\CompetiteActivityGroupController::class, 'change']); // 编辑
    Route::post('/competiteActivityGroup/del', [Admin\CompetiteActivityGroupController::class, 'del']); // 删除
    Route::post('/competiteActivityGroup/changeGroupPassword', [Admin\CompetiteActivityGroupController::class, 'changeGroupPassword']); // 修改团队账号密码
    Route::post('/competiteActivityGroup/orderChange', [Admin\CompetiteActivityGroupController::class, 'orderChange']); // 修改区域排序



    /**
     * 线上大赛作品数据库
     */
    Route::get('/competiteActivityDatabase/filterList', [Admin\CompetiteActivityDatabaseController::class, 'filterList']); //列表(用于下拉框选择)
    Route::get('/competiteActivityDatabase/list', [Admin\CompetiteActivityDatabaseController::class, 'lists']); //  列表
    Route::get('/competiteActivityDatabase/detail', [Admin\CompetiteActivityDatabaseController::class, 'detail']); // 详情
    Route::post('/competiteActivityDatabase/add', [Admin\CompetiteActivityDatabaseController::class, 'add']); // 新增
    Route::post('/competiteActivityDatabase/change', [Admin\CompetiteActivityDatabaseController::class, 'change']); // 编辑
    Route::post('/competiteActivityDatabase/del', [Admin\CompetiteActivityDatabaseController::class, 'del']); // 删除
    Route::post('/competiteActivityDatabase/playAndCancel', [Admin\CompetiteActivityDatabaseController::class, 'playAndCancel']); // 发布与取消发布
    Route::post('/competiteActivityDatabase/sortChange', [Admin\CompetiteActivityDatabaseController::class, 'sortChange']); // 排序


    /**
     * 线上大赛作品数据库（作品管理）
     */
    Route::get('/competiteActivityWorksDatabase/filterList', [Admin\CompetiteActivityWorksDatabaseController::class, 'filterList']); //获取已收录的作品id，或电子书id
    Route::get('/competiteActivityWorksDatabase/list', [Admin\CompetiteActivityWorksDatabaseController::class, 'lists']); //  列表
    Route::post('/competiteActivityWorksDatabase/add', [Admin\CompetiteActivityWorksDatabaseController::class, 'add']); // 新增
    Route::post('/competiteActivityWorksDatabase/del', [Admin\CompetiteActivityWorksDatabaseController::class, 'del']); // 删除
    Route::post('/competiteActivityWorksDatabase/sortChange', [Admin\CompetiteActivityWorksDatabaseController::class, 'sortChange']); // 排序



    /**
     * 线上大赛作品电子书
     */
    Route::get('/competiteActivityEbook/list', [Admin\CompetiteActivityEbookController::class, 'lists']); //  列表
    Route::get('/competiteActivityEbook/detail', [Admin\CompetiteActivityEbookController::class, 'detail']); // 详情
    Route::post('/competiteActivityEbook/add', [Admin\CompetiteActivityEbookController::class, 'add']); // 新增
    Route::post('/competiteActivityEbook/change', [Admin\CompetiteActivityEbookController::class, 'change']); // 编辑
    Route::post('/competiteActivityEbook/del', [Admin\CompetiteActivityEbookController::class, 'del']); // 删除
    Route::post('/competiteActivityEbook/playAndCancel', [Admin\CompetiteActivityEbookController::class, 'playAndCancel']); // 发布与取消发布
    Route::post('/competiteActivityEbook/sortChange', [Admin\CompetiteActivityEbookController::class, 'sortChange']); // 排序

    /**
     * 线上大赛作品电子书（作品管理）
     */
    Route::get('/competiteActivityWorksEbook/filterList', [Admin\CompetiteActivityWorksEbookController::class, 'filterList']); //获取已收录的作品id
    Route::get('/competiteActivityWorksEbook/list', [Admin\CompetiteActivityWorksEbookController::class, 'lists']); //  列表
    Route::post('/competiteActivityWorksEbook/add', [Admin\CompetiteActivityWorksEbookController::class, 'add']); // 收录作品（大赛作品、上传图片、上传pdf）
    Route::post('/competiteActivityWorksEbook/del', [Admin\CompetiteActivityWorksEbookController::class, 'del']); // 删除
    Route::post('/competiteActivityWorksEbook/sortChange', [Admin\CompetiteActivityWorksEbookController::class, 'sortChange']); // 排序


    /**
     * 线上大赛作品指定打分人
     */
    Route::get('/competiteActivityWorksAppointScore/filterList', [Admin\CompetiteActivityWorksAppointScoreController::class, 'filterList']); //根据打分人获取所有的作品id，或名字  简单列表
    Route::get('/competiteActivityWorksAppointScore/list', [Admin\CompetiteActivityWorksAppointScoreController::class, 'lists']); //  列表
    Route::get('/competiteActivityWorksAppointScore/getScoreWorks', [Admin\CompetiteActivityWorksAppointScoreController::class, 'getScoreWorks']); // 根据打分人获取所有的作品id，或名字 分页列表
    Route::post('/competiteActivityWorksAppointScore/modify', [Admin\CompetiteActivityWorksAppointScoreController::class, 'modify']); // 添加、修改打分作品
    Route::post('/competiteActivityWorksAppointScore/del', [Admin\CompetiteActivityWorksAppointScoreController::class, 'del']); // 删除打分作品


    /**
     * 阅读任务
     */
    Route::get('/readingTask/list', [Admin\ReadingTaskController::class, 'lists']); //  列表
    Route::get('/readingTask/detail', [Admin\ReadingTaskController::class, 'detail']); //  详情
    Route::post('/readingTask/add', [Admin\ReadingTaskController::class, 'add']); // 新增
    Route::post('/readingTask/change', [Admin\ReadingTaskController::class, 'change']); // 修改
    Route::post('/readingTask/del', [Admin\ReadingTaskController::class, 'del']); // 删除
    Route::post('/readingTask/cancelAndRelease', [Admin\ReadingTaskController::class, 'cancelAndRelease']); // 撤销 和发布

    Route::get('/readingTaskUser/list', [Admin\ReadingTaskUserController::class, 'lists']); // 用户阅读任务列表


    /**
     * 阅读任务指定用户
     */
    Route::get('/readingTaskAppoint/list', [Admin\readingTaskAppointController::class, 'lists']); //  指定用户列表
    Route::post('/readingTaskAppoint/add', [Admin\readingTaskAppointController::class, 'add']); //  指定用户新增
    Route::post('/readingTaskAppoint/del', [Admin\readingTaskAppointController::class, 'del']); //  指定用户删除





});
