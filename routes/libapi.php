<?php

use App\Http\Controllers\LibApi;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// 图书馆接口

//读者证号登录
Route::get('libapi/gshqLib/login', [LibApi\GshqLibController::class, 'login']);




