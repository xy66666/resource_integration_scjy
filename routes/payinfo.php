<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PayInfo;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




//token必选
Route::middleware(['check.wechat.token'])->prefix('payinfo')->group(function () {
    /**
     * 在线办证支付
     */
    Route::post('/onlineRegistrationPayInfo/getPayInfo', [PayInfo\OnlineRegistrationPayInfoController::class, 'getPayInfo']); // 统一下单，获取支付所需的参数 
    Route::post('/onlineRegistrationPayInfo/paymentSuccess', [PayInfo\OnlineRegistrationPayInfoController::class, 'paymentSuccess']); // 支付成功,验证订单状态，验证成功后修改订单状态

    /**
     * 欠费支付
     */
    Route::post('/owePayInfo/getPayInfo', [PayInfo\OwePayInfoController::class, 'getPayInfo']); // 统一下单，获取支付所需的参数
    Route::post('/owePayInfo/paymentSuccess', [PayInfo\OwePayInfoController::class, 'paymentSuccess']); // 支付成功,验证订单状态，验证成功后修改订单状态

    /**
     * 商品购买支付
     */
    Route::post('/goodsPayInfo/getPayInfo', [PayInfo\GoodsPayInfoController::class, 'getPayInfo']); // 统一下单，获取支付所需的参数 
    Route::post('/goodsPayInfo/paymentSuccess', [PayInfo\GoodsPayInfoController::class, 'paymentSuccess']); // 支付成功,验证订单状态，验证成功后修改订单状态

    /**
     * 图书到家支付
     */
    Route::post('/bookHomePayInfo/getPayInfo', [PayInfo\BookHomePayInfoController::class, 'getPayInfo']); // 统一下单，获取支付所需的参数 
    Route::post('/bookHomePayInfo/paymentSuccess', [PayInfo\BookHomePayInfoController::class, 'paymentSuccess']); // 支付成功,验证订单状态，验证成功后修改订单状态


});









