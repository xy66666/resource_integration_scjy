<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Wall;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * 座位预约，大数据展示墙
 */
Route::get('wall/seatWall/index', [Wall\SeatWallController::class, 'index']); // 座位大数据首页
Route::get('wall/seatWall/detail', [Wall\SeatWallController::class, 'detail']); // 座位大数据详情
