<?php

use App\Http\Controllers\AccessSystem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//空间预约系统
Route::prefix('access_system')->group(function () {

    Route::post('/api/CheckCode', [AccessSystem\IndexController::class, 'checkCode']); //空间预约系统获取数据测试
    Route::post('/api/QueryCmd', [AccessSystem\IndexController::class, 'queryCmd']); //空间预约系统心跳
    Route::post('/api/QueryCmdPostData', [AccessSystem\IndexController::class, 'queryCmdPostData']); //空间预约系统心跳响应

    Route::post('/index/createQr', [AccessSystem\IndexController::class, 'createQr']); //生成二维码


});
