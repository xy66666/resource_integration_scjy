<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MigratedProcessingDataController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//处理用户数据
Route::get('migratedProcessingData/userInfo', [MigratedProcessingDataController::class, 'userInfo']); //同步用户数据
Route::get('migratedProcessingData/activity', [MigratedProcessingDataController::class, 'activity']); //同步活动数据与报名数据
Route::get('migratedProcessingData/digitalReadData', [MigratedProcessingDataController::class, 'digitalReadData']); //处理数据资源数据
Route::get('migratedProcessingData/answerActivityData', [MigratedProcessingDataController::class, 'answerActivityData']); //处理答题活动数据
Route::get('migratedProcessingData/pictureLiveData', [MigratedProcessingDataController::class, 'pictureLiveData']); // 处理图片直播数据
Route::get('migratedProcessingData/bannerData', [MigratedProcessingDataController::class, 'bannerData']); //处理banner数据

