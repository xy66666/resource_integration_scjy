<?php

namespace App\Jobs;


/** 
 * 活动抽奖
 */
class CommonDrawJob
{

    /**
     * 总概率方式抽奖 或者  独立概率方式抽奖
     * $proArr 奖品数组
     * $type //概率方式   1 单个奖品独立概率方式    2 所以奖品总概率形式
     */
    public function probabilityDraw($proArr, $type = 1)
    {
        $result = [];

        //概率数组的总概率精度
        $proSum = 10000;

        //概率数组循环
        foreach ($proArr as $key => $val) {
            $percent = $val['percent'] * 100;

            $randNum = mt_rand(1, $proSum);
            if ($randNum <= $percent) {
                $result = $val;
                break;
            }

            if ($type == 2) {
                $proSum -= $percent;
            }
        }
        unset($proArr);

        return $result;
    }
}
