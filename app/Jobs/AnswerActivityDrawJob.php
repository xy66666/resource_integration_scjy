<?php

namespace App\Jobs;

use App\Http\Controllers\RedisServiceController;
use App\Http\Controllers\Wechat\AnswerActivityDrawController;
use App\Models\AnswerActivity;
use App\Models\AnswerActivityGift;
use App\Models\AnswerActivityGiftConfig;
use App\Models\AnswerActivityUserGift;
use App\Models\AnswerActivityUserPrize;
use App\Models\AnswerActivityUserUnit;
use App\Models\AnswerActivityUserUsePrizeRecord;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

/**
 * 答题活动抽奖队列   Supervisor 里添加进程守护执行
 */
class AnswerActivityDrawJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    public $answer_red_transfer_key = 're_answer_red_transfer';
    public static $percent_way = 1; //概率方式   1 单个奖品独立概率方式    2 所以奖品总概率形式
    public static $gift_max_number_day = 100; //礼物每日发送奖品最大数量
    public static $red_max_number_day = 500; //红包每日发送奖品最大数量
    public static $user_gift_number_day = 1; //用户当日可获得多少实物礼品
    public static $user_red_number_day = 5; //用户当日可获得多少红包礼品

    public $key;
    public $value;

    /**
     * 任务最大尝试次数。
     *
     * @var int
     */
    public $tries = 3;   //这里写了，程序会自动尝试执行 几次
    /**
     * 任务运行的超时时间。
     *
     * @var int
     */
    public $timeout = 180;


    /**
     * Create a new job instance.
     *
     * @return void
     * //执行监听程序（php artisan queue:listen），会自动把之前写入队列的参数拿出，放到对应位置，这里只是个写入对应的程序参数对应，但是他们不是同步执行的
     * //执行监听程序 如果设置同步 执行，则不需要执行，系统会自动执行，但是这里的返回值，不管是什么，执行接口都不会收到反馈
     * 
     * 
     * php artisan queue:work
     * 或
     * php artisan queue:work  --queue=queue1   单个执行answer_red_transfer 需要在 dispatch($data1)->onQueue('queue1')  配置 
     * 
     * Laravel 自带了一个队列进程用来处理被推送到队列的新任务。你可以使用 queue:work 命令运行这个队列进程。请注意，队列进程开始运行后，会持续监听队列，直至你手动停止或关闭终端：

     * php artisan queue:listen
     * 此外，你还可以运行 queue:listen 命令。使用该命令时，代码修改后不需要手动重启队列进程，不过，该命令性能不及 queue:work：
     * 
     * php artisan queue:restart
     * 前文已经提到队列进程是长生命周期的进程，在重启以前，所有源码的修改并不会对其产生影响。所以，最简单的方法是在每次发布新版本后重新启动队列进程。你可以通过 Aritisan 命令 queue:restart 来优雅地重启队列进程:
     * 
     *
     * 目前有 4个进程   php artisan queue:work --queue=answer_activity_draw   答题活动抽奖
     *                 php artisan queue:work --queue=trun_activity_draw   在线抽奖活动抽奖
     *                 php artisan queue:work --queue=answer_red_transfer   答题活动红包转账
     *                 php artisan queue:work --queue=turn_red_transfer   在线抽奖活动红包转账
     *
     */

    public function __construct($key = null, $value = null)
    {
        $this->key = $key;
        $this->value = $value;
    }

    /**
     * Execute the job.
     *
     * @return void
     * 
     //执行 $tries 次失败后，会自动 存到数据表 表前缀_failed_jobs  中

      $value =  $data = [
            'act_id' => $this->request->act_id,
            'user_guid' => $this->request->token,
            'order_key' => $this->answer_activity_draw_key,
            'order_id' => $order_id,
            'create_time' => date("Y-m-d H:i:s"),
            'date' => date("Y-m-d"),
            'hour' => date("H")
        ];

     */
    public function handle()
    {
        $answerActivityDrawController = new AnswerActivityDrawController();
        if (empty($this->value['order_key']) || $this->value['order_key'] != $answerActivityDrawController->answer_activity_draw_key) {
            return false; //不是答题抽奖类作品，不处理
        }

        $answerActivityUserPrizeModel = new AnswerActivityUserPrize();
        $answerActivityUserGiftModel = new AnswerActivityUserGift();
        $activityModel = new AnswerActivity();

        $redisObj = RedisServiceController::getInstance();

        $act_id = $this->value['act_id'];
        $user_guid = $this->value['user_guid'];
        $result = false; //中奖数据
        DB::beginTransaction();
        try {
            //进行抽奖操作
            $act_info = $activityModel->detail($act_id);
            if (empty($act_info)) {
                throw new Exception('参数错误');
            }
            if ($act_info['prize_form'] != 2) {
                throw new Exception('此活动不是抽奖类活动，不允许抽奖');
            }

            //判断是否有抽奖次数
            $isHavePrizeNumber = $answerActivityUserPrizeModel->isHavePrizeNumber($act_id, $user_guid);
            if (!$isHavePrizeNumber) {
                throw new Exception('无抽奖次数');
            }
            //抽奖计算概率
            //获取总概率
            $total_gift_config = AnswerActivityGiftConfig::where('act_id', $act_id)->first();
            if (!empty($total_gift_config)) {
                if ($total_gift_config['percent_way']) self::$percent_way = $total_gift_config['percent_way']; //概率方式   1 单个奖品独立概率方式    2 所以奖品总概率形式
                if ($total_gift_config['gift_max_number_day']) self::$gift_max_number_day = $total_gift_config['gift_max_number_day']; //礼物每日发送奖品最大数量
                if ($total_gift_config['red_max_number_day']) self::$red_max_number_day = $total_gift_config['red_max_number_day']; //红包每日发送奖品最大数量
                if ($total_gift_config['user_gift_number_day']) self::$user_gift_number_day = $total_gift_config['user_gift_number_day']; //用户当日可获得多少实物礼品
                if ($total_gift_config['user_red_number_day']) self::$user_red_number_day = $total_gift_config['user_red_number_day']; //用户当日可获得多少红包礼品
            }

            $red_win = false; //获取红包机会
            $gift_win = false; //获取礼物机会

            //获取用户今日是否可以获取红包
            $answer_activity_gift_type = config('other.answer_activity_gift_type');
            if (in_array(1, $answer_activity_gift_type)) {
                $day_red_info = $answerActivityUserGiftModel->isDayWinChance($act_id, $user_guid, 1);
            } else {
                $day_red_info = false;
            }

            if (in_array(2, $answer_activity_gift_type)) {
                $day_gift_info = $answerActivityUserGiftModel->isDayWinChance($act_id, $user_guid, 2);
            } else {
                $day_gift_info = false;
            }

            if ($day_red_info) {
                //获取时间段是否可以中红包
                $time_quantum_red_info = $answerActivityUserGiftModel->isTimeQuantumWinChance($act_id, $user_guid, 1);
                $red_win = $time_quantum_red_info ? true : false;
            }

            if ($day_gift_info) {
                //获取时间段是否可以中礼物
                $time_quantum_gift_info = $answerActivityUserGiftModel->isTimeQuantumWinChance($act_id, $user_guid, 2);
                $gift_win = $time_quantum_gift_info ? true : false;
            }

            //至少中一个的逻辑
            if ($red_win || $gift_win) {
                if ($red_win && $gift_win) {
                    $type = 0; //所有都可以中
                } elseif ($gift_win) {
                    $type = 2; //只能中礼物
                } else {
                    $type = 1; //只能中红包
                }
                //获取所有可以中奖的礼物
                $answerActivityGiftModel = new AnswerActivityGift();
                //查看用户所属单位
                $user_unit_ids = AnswerActivityUserUnit::where('user_guid', $user_guid)->pluck('unit_id');
                $gift_data_all = $answerActivityGiftModel->getSurplusGift($act_id, $type, $user_unit_ids); //暂时不按馆获取数据

                //没有礼物直接未中奖
                if (!empty($gift_data_all)) {
                    $commonDrawJob = new CommonDrawJob();
                    $result = $commonDrawJob->probabilityDraw($gift_data_all, self::$percent_way);
                }
            }

            //修改奖品表数量（中奖执行）
            if ($result) {
                $answerActivityGiftModel->where('id', $result['id'])->increment('use_number');
            }
            //减少用户抽奖次数（中奖、未中奖都要执行）
            $answerActivityUserPrizeModel->where('act_id', $act_id)->where('user_guid', $user_guid)->increment('use_number');

            //增加抽奖表记录
            $use_prize_record_id = AnswerActivityUserUsePrizeRecord::insertGetId([
                'order_id' => $this->value['order_id'],
                'act_id' => $this->value['act_id'],
                'user_guid' => $this->value['user_guid'],
                'date' => date('Y-m-d'),
                'status' => !empty($result) ? 1 : 2, //是否中奖  1 中奖  2 未中奖
                'create_time' => $this->value['create_time'],
                'change_time' => date('Y-m-d H:i:s'),
            ]);

            //中奖逻辑
            if (!empty($result)) {
                //增加用户奖品
                $answer_activity_user_gift_id = $answerActivityUserGiftModel->insertGetId([
                    'act_id' => $act_id,
                    'user_guid' => $user_guid,
                    'gift_id' => $result['id'],
                    'price' => $result['type'] == 1 ? $result['price'] : '',
                    'order_id' => $this->value['order_id'],
                    'use_prize_record_id' => $use_prize_record_id,
                    'state' => $result['type'] == 1 ? 1 : ($result['way'] == 1 ? 6 : 3),
                    'date' => date('Y-m-d'),
                    'hour' => date('H'),
                    'type' => $result['type'],
                    'create_time' => date('Y-m-d H:i:s')
                ]);

                //如果是红包，还要进行转账操作（写入队列，进行转账）
                if ($result['type'] == 1) {
                    //写入红包队列
                    $data['user_gift_id'] = $answer_activity_user_gift_id;
                    $data['act_name'] = $act_info['title'];
                    //执行完后就不用管了，也不用管另外一个控制器怎么拿的到这些参数，各写各的逻辑即可  onQueue('answer_red_transfer')  后面的 answer_red_transfer 执行时直接采用   php artisan queue:work  --queue=answer_red_transfer  单个执行 即可
                    $queue_id = dispatch(new AnswerActivityTransferJob($this->answer_red_transfer_key, $data))->onQueue($this->answer_red_transfer_key);
                    //  $queue_id = $this->dispatch(new AnswerActivityTransferJob('answer_red_transfer', $data))->onQueue('answer_red_transfer'); 
                    if (!$queue_id) {
                        throw new Exception('未中奖');
                    }
                }
            }

            //删除用户正在抽奖中，可以再次抽奖
            $redisObj->delRedisValue($this->value['order_key'] . $this->value['user_guid']);
            //    $redisObj->delRedisValue($this->value['order_key'] . $this->value['order_id']);

            DB::commit();
            return '抽奖完成';
        } catch (Exception $e) {
            //删除用户正在抽奖中，可以再次抽奖
            $redisObj->delRedisValue($this->value['order_key'] . $this->value['user_guid']);
            //   $redisObj->delRedisValue($this->value['order_key'] . $this->value['order_id']);

            Log::channel('handcmdlog')->info('答题抽奖失败：' . $e->getMessage() . $e->getFile() . $e->getLine());

            DB::rollBack();
            return '抽奖失败：' . $e->getMessage();
        }
    }



    //失败多次，就是没中奖
    public function failed()
    {
        dump('failed');
    }
}
