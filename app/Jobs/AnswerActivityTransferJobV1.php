<?php

namespace App\Jobs;

use App\Http\Controllers\SmallPay;
use App\Models\AnswerActivityPacketTransfer;
use App\Models\AnswerActivityUserGift;
use App\Models\UserWechatInfo;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

/**
 * 答题活动红包转账队列
 */
class AnswerActivityTransferJobV1 implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    public $key;
    public $value;

    /**
     * 任务最大尝试次数。
     *
     * @var int
     */
    public $tries = 3;   //这里写了，程序会自动尝试执行
    /**
     * 任务运行的超时时间。
     *
     * @var int
     */
    public $timeout = 180;


    /**
     * Create a new job instance.
     *
     * @return void
     * //执行监听程序（php artisan queue:listen），会自动把之前写入队列的参数拿出，放到对应位置，这里只是个写入对应的程序参数对应，但是他们不是同步执行的
     */
    public function __construct($key = null, $value = null)
    {
        $this->key = $key;
        $this->value = $value;
    }

    /**
     * Execute the job.
    
     *   $value =  $data=[
     *          'act_name'=> $act_name,    //活动名称
     *          'user_gift_id'=> $answer_activity_user_gift_id,   //用户礼物id
     *  ];
     */
    public function handle()
    {
        $answerActivityUserGiftModel = new AnswerActivityUserGift();
        $answerActivityUserGiftInfo = $answerActivityUserGiftModel->where('id', $this->value['user_gift_id'])->first();
        if (empty($answerActivityUserGiftInfo) || $answerActivityUserGiftInfo->type != 1 || $answerActivityUserGiftInfo->state != 1 || empty($answerActivityUserGiftInfo->price)) {
            return false; //不是红包或已发放，不处理
        }
        //正常转账流程
        $user_guid = $answerActivityUserGiftInfo->user_guid;
        $order_id = $answerActivityUserGiftInfo->order_id;
        $packet_msg = $this->value['act_name'];
        $price = $answerActivityUserGiftInfo->price * 100;

      //  $price = 0.3 * 100; //TODO测试使用

        $userWechatModel = new UserWechatInfo();
        $wechat_user_info = $userWechatModel->where('user_guid', $user_guid)->where('node', 'YXYZ')->first();

        if (empty($wechat_user_info)) {
            Log::channel('handcmdlog')->info("用户获取失败,不能转账：" . $user_guid);
            return false;
        }

        $smallPayModel = new SmallPay();
        $res = $smallPayModel->transfer($order_id, $price, $wechat_user_info['open_id'], $packet_msg);

        //写入转账日志
        if (!empty($res['data'])) {
            $packet_transfer_model = new AnswerActivityPacketTransfer();
            $packet_transfer_model->insert($res['data']);
        }

        if ($res['code'] == 200) {
            $answerActivityUserGiftModel->where('order_id', $order_id)->update(['pay_time' => $res['pay_time'], 'state' => 2]);
            return true;
        }
        return false;
    }

     //转账失败
     public function failed()
     {
        Log::channel('handcmdlog')->info('答题红包转账失败');

         dump('failed'); //程序报错才会走这里，try cache 拦截后，不会走这里，程序会以执行成功处理
     }

}
