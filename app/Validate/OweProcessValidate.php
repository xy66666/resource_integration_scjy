<?php

namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * 欠费验证类
 * Class ActivityValidate
 * @package app\port\validate
 */
class OweProcessValidate extends BaseValidate
{
    protected $rule = [
        "fin_account_id|记录id" => "required",
        "way" => "required|integer|in:1,2",
    ];


    //自定义验证信息
    protected $message = [
        'fin_account_id.required' => '记录ID不能为空',
        'way.required' => '缴纳方式不能为空',
        'way.integer' => '缴纳方式格式不正确',
        'way.in' => '缴纳方式格式不正确',
    ];


    protected $scene = [
        // 欠费缴纳
        "owe_pay" => ["fin_account_id", 'way'],



    ];
}
