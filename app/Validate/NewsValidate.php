<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class NewsValidate extends  BaseValidate
{
     //验证规则
  protected $rule = [
    'id' => 'bail|required|integer',
      'img' => 'bail|required',
    'type_id' => 'bail|required|integer',
    'title' => 'bail|required',
    'content' => 'bail|required',

  ];
  //自定义验证信息
  protected $message = [
    'id.required' => 'ID不能为空',
    'id.integer' => 'ID格式不正确',
      'img.required' => '图片不能为空',
    'type_id.required' => '类型ID不能为空',
    'type_id.integer' => '类型ID格式不正确',
    'title.required' => '标题不能为空',
    'content.required' => '内容不能为空',
  ];


  //自定义场景
  protected $scene = [
    'detail' => ['id'], //详情
    'add' => ['title','type_id','content'], //添加
    'change' => ['id','title','type_id','content'], //修改
    'del' => ['id'], //删除
    'top_and_cancel' => ['id'], //置顶与取消置顶


  ];

}