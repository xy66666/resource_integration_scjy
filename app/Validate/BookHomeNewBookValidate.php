<?php

namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * （图书到家）新书到家操作（书店，图书馆操作）
 * Class NewBookValidate
 * @package app\api\validate
 */
class BookHomeNewBookValidate extends BaseValidate
{
    protected  $rule =[
        'book_id'=> 'required|integer',//新书id
        'is_pay'=> 'required|integer|in:2,5,6,7,8,9,5789',//订单类型
        'state'=> 'required|integer|in:1,2',//订单类型
        'status'=> 'required|integer|in:1,2',//订单类型
        'order_id'=> 'required|integer',//订单id
        'order_ids'=> 'required',//订单id
        'purchase_id'=> 'required|integer',//采购书籍id
        'purchase_ids'=> 'required',//采购清单id
        'address_id'=> 'required|integer',//收货地址id
        'book_ids'=> 'required',//新书id
        'ids'=> 'required',//新书id
        'barcodes'=> 'required',//条形码
        'courier_id'=> 'required|integer',//快递id
        'tracking_number'=> 'required|min:8|max:18',//采购id
        'refund_remark'=> 'required',//退款理由
        'account'=> 'required',//读者证号
        'barcode'=> 'required',//条形码
        'postal_id'=> 'required|integer',//快递id
    ];

        
    # 错误提示语
    protected $message = [
        "book_id.required" => "书籍ID不能为空",
        "book_id.integer" => "书籍ID规则不正确",
        "purchase_id.required" => "采购清单ID不能为空",
        "purchase_id.integer" => "采购清单ID规则不正确",


        "order_ids.required" => "订单号不能为空",
        "purchase_ids.required" => "采购清单id不能为空",
        "is_pay.required" => "类型不能为空",
        "is_pay.integer" => "类型规则不正确",
        "is_pay.in" => "类型规则不正确",

        "state.required" => "状态不能为空",
        "state.integer" => "状态规则不正确",
        "state.in" => "状态规则不正确",
    

        "status.required" => "状态不能为空",
        "status.integer" => "状态规则不正确",
        "status.in" => "状态规则不正确",

        "order_id.required" => "订单ID不能为空",
        "order_id.integer" => "订单ID规则不正确",
        "address_id.required" => "地址ID不能为空",
        "address_id.integer" => "地址ID规则不正确",
        "book_ids.required" => "书籍ID不能为空",
        "ids.required" => "书籍ID不能为空",
        "barcodes.required" => "条形码不能为空",
        "courier_id.required" => "快递公司名称ID不能为空",
        "courier_id.integer" => "快递公司名称ID规则不正确",

        "tracking_number.required" => "快递单号必须存在",

        "tracking_number.min" => "快递单号不能少于8位",
        "tracking_number.max" => "快递单号不能大于18位",
        'refund_remark.required' => '退款理由不能为空',//退款理由

        "account.required" => "读者证号必须存在",
        "barcode.required" => "条形码必须存在",
        "postal_id.required" => "快递ID不能为空",
        "postal_id.integer" => "快递ID规则不正确",
    ];

    protected  $scene =[
        'order_list'   => ['is_pay'],//申请列表、发货列表、所有订单列表
        
        'order_detail'   => ['order_id'],//订单详情

        'dispose_order_apply'   => ['order_ids','state'],//管理员同意 和拒绝用户的 采购申请
        'dispose_order_deliver'   => ['order_id','state'],//确认发货或无法发货
        'again_get_tracking_number_shop'   => ['order_id'],//针对邮递发货 获取运单号失败后，补发获取快递单号（新书）
        'print_express_sheet_shop'   => ['order_id'],//打印电子面单 （新书）
        'purchase_state_modify'   => ['purchase_ids'],//打印电子面单 （新书）

        'dispose_success'   => ['purchase_ids'],// 新书 数据 写入图书馆系统失败后，手动处理
        'purchase_detail'   => ['purchase_id'],// 获取采购清单详情

        'refund'   => ['order_id' , 'refund_remark'],// 退款


        'print_express_sheet_lib'   => ['order_id'],// 打印电子面单 （馆藏书）
        'print_express_sheet_shop'   => ['order_id'],// 打印电子面单 （新书）


        'reader_borrow_list'   => ['account'],// 线下书籍归还（根据读者证号查询数据）
        'book_borrow'   => ['barcode'],// 线下书籍归还（根据条形码查询数据）
        'offline_return'   => ['barcode','account'],// 线下书籍归还
        'offline_return_direct'   => ['barcode'],// 线下书籍直接归还
        'online_return'   => ['postal_id' , 'status'],// 线上书籍归还


        'sponsor_settle_state'   => ['order_ids'],//发起方标记邮费状态为结算中
        'affirm_settle_state'   => ['order_ids'],//确认发标记邮费状态为已结算

    //     'new_book_detail'   => ['book_id'],//新书详情
    //   //  'hot_search_list'   => ['is_pay'],//检索热词列表
    //   //  'postage_list'   => ['is_pay'],//图书馆设置的运费详情(书店及图书馆)
    //   //  'new_book_collect'   => ['book_id', 'is_pay'],//新书收藏与取消收藏
    //     'new_book_schoolbag'   => ['book_id'],//新书加入书袋与移出书袋
    //     'get_order_info'   => ['book_ids' , 'address_id'],//获取预支付订单信息
    //     'my_order_detail'   => ['order_id'],//我的订单详情
    //     //    'recom_return'   => ['courier_id','barcodes','tracking_number'],//书籍归还
    //     //'   => ['return_id'],//用户撤销线上书籍归还记录

    ];

}