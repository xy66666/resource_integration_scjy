<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class LibInfoValidate extends  BaseValidate
{
     //验证规则
  protected $rule = [
    'id' => 'bail|required|integer',
    'metaid' => 'bail|required',
    'metatable' => 'bail|required',
    'lib_id' => 'bail|required|integer',
    'type' => 'bail|required|integer:in:1,2',
    'account' => 'bail|required',
    'password' => 'bail|required',
    'keywords' => 'bail|required',
    'content' => 'bail|required',
    'old_password' => 'bail|required',
    'new_password' => 'bail|required',
  ];
  //自定义验证信息
  protected $message = [
    'id.required' => 'ID不能为空',
    'id.integer' => 'ID格式不正确',
    'metaid.required' => 'metaid不能为空',
    'metatable.required' => 'metatable不能为空',

    'lib_id.required' => '图书馆ID不能为空',
    'lib_id.integer' => '图书馆ID格式不正确',
    'account.required' => '读者证不能为空',
    'password.required' => '密码不能为空',
    'keywords.required' => '检索内容不能为空',
    'content.required' => '内容不能为空',
    'type.required' => '类型不能为空',
    'type.integer' => '类型格式不正确',
    'type.in' => '类型格式不正确',
    'old_password.required' => '旧密码不能为空',
    'new_password.required' => '新密码不能为空',
  ];


  //自定义场景
  protected $scene = [
    'bing_reader' => ['account','password'], //绑定读者证 或密码错误重新登录
    'get_borrow_list' => ['type'], //获取借阅记录列表


    'search_list' => ['keywords'], //获取检索信息
    'lib_book_detail' => ['metaid' ,'metatable'], //获取馆藏书籍详情
    
    'renew' => ['content'], //续借

    'change_password' => ['old_password','new_password'], //修改读者证密码
    

  ];

}