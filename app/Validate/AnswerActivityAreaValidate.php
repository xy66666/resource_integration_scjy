<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class AnswerActivityAreaValidate extends  BaseValidate
{
    protected  $rule = [
        'id' => 'bail|required|integer',
        'act_id' => 'bail|required|integer',
        'name' => 'bail|required|max:3',
        'img' => 'bail|required',
        'orders' => 'bail|required',
    ];

    # 错误提示语
    protected $message = [
        'id.required' => 'ID不能为空',
        'id.integer' => 'ID格式不正确',
        'act_id.required' => '活动ID不能为空',
        'act_id.integer' => '活动ID格式不正确',
        'name.required' => '名称不能为空',
        'name.max' => '名称最多3个字符',
        'img.required' => '图片不能为空',
        'orders.required' => '排序内容不能为空',
    ];


    protected  $scene = [
        'filter_list' => ['act_id'], //获取筛选列表
        'list' => ['act_id'], //获取列表
        'add' => ['act_id', 'name', 'img'], //新增验证
        'change' => ['id', 'name', 'img'], //更新验证
        'detail' => ['id'], //详情
        'del' => ['id'], //删除验证
        'order' => ['orders'], //排序验证


        'wx_list' => ['act_id'], //前台获取活动区域列表
    ];
}
