<?php
namespace App\Validate;

use App\Validate\BaseValidate;

class CompetiteActivityTagValidate extends  BaseValidate
{
    protected  $rule =[
        'id'=>'bail|required|integer',
        'tag_name' => 'bail|required',
    ];

    # 错误提示语
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        'tag_name.required'=>'名称不能为空',
    ];


    protected  $scene = [
        'add' => ['tag_name'], //新增验证
        'change' => ['id','tag_name'], //更新验证
        'detail' => ['id'], //详情
        'del' => ['id'] //删除验证
    ];

}