<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class UserDrawAddressValidate extends  BaseValidate
{
    protected  $rule =[
        'id'=>'bail|required|integer',
        'act_id'=>'bail|required|integer',
        'unit_id'=>'bail|required|integer',
        'username' => 'required|min:2',
        'province' => 'required',
        'city' => 'required',
        'district' => 'required',
        'address' => 'required',
        'type' => 'required|in:1,2',
        'is_default' => 'required|in:1,2',
        'tel' => 'required|check_tel_and_phone:',
        'invite_code' => 'required|digits:6',
      //  'act_type_id'=>'bail|required|integer|in:1,2,3',
    ];


    # 错误提示语
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        "act_id.required" => "活动ID不能为空",
        "act_id.integer" => "活动ID参数错误",
        "unit_id.required" => "单位ID不能为空",
        "unit_id.integer" => "单位ID参数错误",
        "username.required" => "用户名不能为空",
        "username.min" => "用户名最少2个字符",
        "province.required" => "省级地址不能为空",
        "city.required" => "市级地址不能为空",
        "district.required" => "区级地址不能为空",
        "address.required" => "详细地址不能为空",
        "is_default.required" => "是否默认地址不能为空",
        "is_default.in" => "是否默认地址规则不正确",
        "type.required" => "活动类型不能为空",
        "type.in" => "活动类型规则不正确",
    
        "tel.required" => "活动联系方式不能为空",
        "tel.check_tel_and_phone" => "活动联系方式格式不正确",

        "invite_code.required" => "邀请码不能为空",
        "invite_code.digits" => "邀请码格式不正确",

        // "act_type_id.required" => "活动类型ID不能为空",
        // "act_type_id.integer" => "活动类型ID参数错误",
        // "act_type_id.in" => "活动类型ID规则不正确",
    ];

    protected  $scene = [
        'get_user_address' => ['type','act_id'], //获取验证
        'set_user_address' => ['type','act_id','username','tel','province','city','district','address'], //更新验证


    ];


}