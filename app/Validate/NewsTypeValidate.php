<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class NewsTypeValidate extends  BaseValidate
{
     //验证规则
  protected $rule = [
    'id' => 'bail|required|integer',
    'type_name' => 'bail|required',
    'type' => 'bail|required|in:1,2',

  ];
  //自定义验证信息
  protected $message = [
    'id.required' => 'ID不能为空',
    'id.integer' => 'ID格式不正确',
    'type_name.required' => '名称不能为空',
    'type.required' => '展示类型不能为空',
    'type.in' => '展示类型规则不正确',

  ];


  //自定义场景
  protected $scene = [
    'detail' => ['id'], //详情
    'add' => ['type_name','type'], //添加
    'change' => ['id','type_name','type'], //修改
    'del' => ['id'], //删除


  ];

}