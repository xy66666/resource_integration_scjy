<?php
namespace App\Validate;

use App\Validate\BaseValidate;
/**
 * Banner管理
 */
class BannerValidate extends BaseValidate {
    //验证规则
    protected $rule =[
        'id'=>'bail|required|integer',
        'name'=>'bail|required',
        'img'=>'bail|required',
        'type'=>'bail|required|in:1,2',
        'is_play'=>'bail|required|in:1,2',
        'content'=>'bail|required',

    ];
    //自定义验证信息
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        'name.required'=>'名称不能为空',
        'img.required'=>'封面图片不能为空',
        'type.required'=>'类型不能为空',
        'type.in'=>'类型规则不正确',
        'is_play.required'=>'是否显示不能为空',
        'is_play.in'=>'是否显示规则不正确',
        'content.required'=>'内容不能为空',

    ];

    //自定义场景
    protected $scene = [
        'add' => ['name','img','type','is_play'],//添加
        'change' => ['id','name','img','type','is_play'],//修改
        'detail' => ['id'],//详情
        'del' => ['id'],//删除
        'cancel_and_release' => ['id','is_play'],//切换显示方式
        'sort_change' => ['content'],//banner 图排序
    ];




}