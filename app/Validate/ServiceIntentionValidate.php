<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class ServiceIntentionValidate extends  BaseValidate
{
     //验证规则
  protected $rule = [
    'id' => 'bail|required|integer',
    'name' => 'bail|required',

  ];
  //自定义验证信息
  protected $message = [
    'id.required' => 'ID不能为空',
    'id.integer' => 'ID格式不正确',
    'name.required' => '名称不能为空',


  ];


  //自定义场景
  protected $scene = [
    'detail' => ['id'], //详情
    'add' => ['name'], //添加
    'change' => ['id','name'], //修改
    'del' => ['id'], //删除


  ];

}