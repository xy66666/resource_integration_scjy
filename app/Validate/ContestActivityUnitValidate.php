<?php
namespace App\Validate;

use App\Validate\BaseValidate;

class ContestActivityUnitValidate extends  BaseValidate
{
    protected  $rule =[
        'id'=>'bail|required|integer',
        'con_id'=>'bail|required|integer',
        'nature'=>'bail|required|integer',
        'name' => 'bail|required|max:20',
        'img' => 'bail|required',
        'orders' => 'bail|required',
    ];

    # 错误提示语
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        'con_id.required'=>'大赛ID不能为空',
        'con_id.integer'=>'大赛ID格式不正确',
        'nature.required'=>'单位性质不能为空',
        'nature.integer'=>'单位性质格式不正确',
        'name.required'=>'名称不能为空',
        'name.max'=>'名称最多20个字符',
        'img.required'=>'图片不能为空',
        'orders.required'=>'排序内容不能为空',
    ];

    protected  $scene = [
        'filter_list' => ['con_id'], //获取筛选列表
        'list' => ['con_id'], //获取列表
        'add' => ['con_id','nature','name'], //新增验证
        'change' => ['id','nature','name'], //更新验证
        'detail' => ['id'], //详情
        'del' => ['id'], //删除验证
        'order' => ['orders'], //排序验证

        'wx_list' => ['con_id'], //前台获取大赛单位列表
    ];



}