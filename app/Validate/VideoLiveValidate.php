<?php

namespace App\Validate;

use App\Validate\BaseValidate;


/**
 * 视频直播验证类
 */
class VideoLiveValidate extends  BaseValidate
{
    protected  $rule = [
        'id' => 'bail|required',
        'title' => 'required|max:100',
        'main_img' => 'required',
        'img' => 'required',
        'act_id' => 'required|integer',
        'type_id' => 'required|integer',
        'status' => 'required|integer|in:2,3',

        'is_check' => 'required|integer|in:1,2',
        'is_appoint' => 'required|integer|in:1,2',
        'is_play' => 'required|integer|in:1,2',
        'vote_way' => 'required|integer|in:1,2',
        'uploads_way' => 'required|integer|in:1,2,3',
        'number' => 'required|integer',

        'start_time' => 'bail|required|date',
        'end_time' => 'bail|required|date|after:start_time',
        'vote_start_time' => 'bail|required|date|after_or_equal:start_time',
        'vote_end_time' => 'bail|required|date|after:vote_start_time',

        'callback_address' => 'bail|required|url'

    ];



    # 错误提示语
    protected $message = [
        'id.required' => 'ID不能为空',
        //  'id.integer' => 'ID格式不正确',
        "title.required" => "活动名称不能为空",
        "title.max" => "活动名称最多100个字符",
        "main_img.required" => "进入主界面封面图片不能为空",
        "img.required" => "活动封面不能为空",
        "type_id.required" => "活动类型ID不能为空",
        "type_id.integer" => "活动类型ID参数错误",
        "act_id.required" => "活动ID不能为空",
        "act_id.integer" => "活动ID参数错误",

        "is_check.required" => "是否需要审核规则不能为空",
        "is_check.integer" => "是否需要审核规则参数错误",
        "is_check.in" => "是否需要审核规则不正确",

        "status.required" => "直播状态不能为空",
        "status.integer" => "直播状态参数错误",
        "status.in" => "直播状态不正确",

        "is_appoint.required" => "是否指定用户上传规则不能为空",
        "is_appoint.integer" => "是否指定用户上传规则参数错误",
        "is_appoint.in" => "是否指定用户上传规则不正确",

        "is_play.required" => "状态不能为空",
        "is_play.integer" => "状态参数错误",
        "is_play.in" => "状态参数错误",
        "vote_way.required" => "投票方式不能为空",
        "vote_way.integer" => "投票方式参数错误",
        "vote_way.in" => "投票方式参数错误",
        "uploads_way.required" => "上传方式不能为空",
        "uploads_way.integer" => "上传方式参数错误",
        "uploads_way.in" => "上传方式参数错误",
        "number.required" => "每日答题次数不能为空",
        "number.integer" => "每日答题次数参数错误",


        'start_time.required' => '直播开始时间不能为空',
        'start_time.date' => '直播开始时间格式不正确',
        'end_time.required' => '直播结束时间不能为空',
        'end_time.date' => '直播结束时间格式不正确',
        'end_time.after' => '直播结束时间必须大于开始时间',

        'vote_start_time.required' => '投票开始时间不能为空',
        'vote_start_time.date' => '投票开始时间格式不正确',
        'vote_start_time.after_or_equal' => '投票开始时间必须大于等于直播开始时间',
        'vote_end_time.required' => '投票结束时间不能为空',
        'vote_end_time.date' => '投票结束时间格式不正确',
        'vote_end_time.after' => '投票结束时间必须大于投票开始时间',


        'callback_address.required' => '回调地址不能为空',
        'callback_address.url' => '回调地址规则不正确',
    ];

    protected  $scene = [
        'add' => ['title', 'img', 'is_play', 'start_time', 'end_time'], //新增验证
        'change' => ['id', 'title', 'img', 'is_play', 'start_time', 'end_time'], //更新验证
        'del' => ['id'], //活动删除
        'detail' => ['id'], //活动详情
        'cancel_and_release' => ['id', 'is_play'], //撤销 和发布

        'change_live_status' => ['id', 'status'], //修改直播状态
        'set_video_callback_address' => ['callback_address'], //设置视频录制回调地址
        'set_channel_transcribe_addr_by_state' => ['callback_address'], //设置频道状态变化回调地址



        'wx_detail' => ['id'], //前台获取活动详情
        'wx_vote_and_cancel' => ['id'], //点赞和取消点赞


        'wx_get_video_live_status' => ['id'], //获取直播状态
        'wx_vote_and_cancel' => ['id'], //点赞与取消点赞
        
        // 'wx_my_vote_picture_live_photo_list' => ['act_id'], //个人中心，我的点赞过的图片列表
        // 'wx_my_picture_live_photo_list' => ['act_id'], //个人中心，我的图片列表
        // 'wx_my_picture_live_detail' => ['id'], //个人中心，我的图片详情
    ];
}
