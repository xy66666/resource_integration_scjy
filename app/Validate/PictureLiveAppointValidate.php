<?php

namespace App\Validate;

use App\Validate\BaseValidate;


/**
 * 图片直播指定用户验证类
 */
class PictureLiveAppointValidate extends  BaseValidate
{
    protected  $rule = [
        'id' => 'bail|required|integer',
        'act_id' => 'required|integer',
        'user_id' => 'required|integer',
    ];



    # 错误提示语
    protected $message = [
        'id.required' => 'ID不能为空',
        'id.integer' => 'ID格式不正确',
        "user_id.required" => "用户ID不能为空",
        "user_id.integer" => "用户ID参数错误",
        "act_id.required" => "活动ID不能为空",
        "act_id.integer" => "活动ID参数错误",
    ];

    protected  $scene = [
        'list' => ['act_id'], //活动id
        'add' => ['act_id','user_id'], //新增验证
        'del' => ['id'], //删除



    ];
}
