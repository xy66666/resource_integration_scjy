<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class ServiceDirectoryValidate extends  BaseValidate
{
  //验证规则
  protected $rule = [
    'id' => 'bail|required|integer',
    'type' => 'bail|required|integer',
    'img' => 'bail|required',
    'title' => 'bail|required',
    'content' => 'bail|required',

  ];
  //自定义验证信息
  protected $message = [
    'id.required' => 'ID不能为空',
    'id.integer' => 'ID格式不正确',
    'type.integer' => '类型不能为空',
    'type.integer' => '类型格式不正确',
    'img.required' => '图片不能为空',
    'title.required' => '标题不能为空',
    'content.required' => '内容不能为空',
  ];


  //自定义场景
  protected $scene = [
    'detail' => ['id'], //详情
    'add' => ['title', 'content'], //添加
    'change' => ['id', 'title', 'content'], //修改
    'del' => ['id'], //删除


  ];
}
