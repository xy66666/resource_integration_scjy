<?php

namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * 屏幕反馈问题设置验证器
 */
class ProblemSetValidate extends BaseValidate
{
  //验证规则
  protected $rule = [
    'id' => 'bail|required|integer',
    'title' => 'bail|required|max:100'
  ];
  //自定义验证信息
  protected $message = [
    'id.required' => 'ID不能为空',
    'id.integer' => 'ID格式不正确',
    'title.required' => '标题不能为空',
    'title.max' => '标题最多100个字符',
  ];

  //自定义场景
  protected $scene = [
    'detail' => ['id'], //详情
    'add' => ['title'], //添加
    'change' => ['id','title'], //修改
    'del' => ['id'], //删除


  ];
}
