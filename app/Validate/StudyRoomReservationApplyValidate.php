<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class StudyRoomReservationApplyValidate extends  BaseValidate
{
    protected  $rule =[
        'id'=>'bail|required|integer',
        'ids'=>'bail|required',
        'reason' => 'required',
        'reservation_id' => 'required|integer',
        'status' => 'required|in:1,4',
       
    ];


    # 错误提示语
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        'ids.required'=>'ID不能为空',
        "reason.required" => "理由不能为空",
        "reservation_id.required" => "活动不能为空",
        "reservation_id.integer" => "活动参数错误",
        'status.required'=>'状态不能为空',
        'status.integer'=>'状态格式不正确',
    ];

    protected  $scene = [
        'check' => ['id' , 'status'], //审核通过和拒绝
        'violate_and_cancel' => ['id'], //活动违规 与 取消违规操作

        'apply_cancel' => ['ids'], //取消预约
        'apply_sign_in' => ['id'], //预约签到
        'apply_sign_out' => ['id'], //预约签退
    

    ];


}