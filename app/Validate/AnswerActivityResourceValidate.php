<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class AnswerActivityResourceValidate extends  BaseValidate
{
    protected  $rule =[
        'act_id' => 'required|integer',
        'theme_color' => 'required',
        'progress_color' => 'required',
        'invite_color' => 'required',
        'theme_text_color' => 'required',
        'warning_color' => 'required',
        'error_color' => 'required',
    ];



    # 错误提示语
    protected $message = [
      
        "act_id.required" => "活动ID不能为空",
        "act_id.integer" => "活动ID参数错误",
        'theme_color.required' => '主题色不能为空',
        'progress_color.required' => '进度条颜色色不能为空',
        'invite_color.required' => '邀请码颜色不能为空',
        'theme_text_color.required' => '主题文字色不能为空',
        'warning_color.required' => '警告颜色不能为空',
        'error_color.required' => '错误颜色不能为空',


    ];



    protected  $scene = [
        'get_color_resource' => ['act_id'], //获取活动资源（颜色）
        'set_color_resource' => ['act_id','theme_color'/* ,'progress_color' */,'invite_color','theme_text_color','warning_color','error_color'], //设置活动资源（颜色）
        'resource_download' => ['act_id'], //下载ui资源文件 
        'resource_upload' => ['act_id'], //上传ui资源文件 
        'default_resource_addr' => ['act_id'], //获取默认活动资源路径
       
       
        'get_word_resource'=>['act_id'], //获取文字资源路径
        'set_word_resource'=>['act_id'], //配置文字资源路径
    ];




}