<?php

namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * 屏幕反馈问题验证器
 */
class ProblemFeedbackValidate extends BaseValidate
{
  //验证规则
  protected $rule = [
    'id' => 'bail|required|integer',
    'problem_id' => 'bail|required|integer',
    'status' => 'bail|required|in:1,2,3',
    'case_guid' => 'bail|required|size:32',
    'user_guid' => 'bail|required|size:32',
    'content' => 'bail|required|max:200'
  ];
  //自定义验证信息
  protected $message = [
    'id.required' => 'ID不能为空',
    'id.integer' => 'ID格式不正确',
    'problem_id.required' => 'ID不能为空',
    'problem_id.integer' => 'ID格式不正确',
    'status.required' => '状态不能为空',
    'status.in' => '状态错误',
    'case_guid.size'    => '用户ID不合法',
    'case_guid.required' => '用户ID不能为空',
    'user_guid.size'    => '用户ID不合法',
    'user_guid.required' => '用户ID不能为空',
    'content.required' => '提交内容不能为空',
    'content.max' => '提交内容最多200个字',
  ];

  //自定义场景
  protected $scene = [
    'detail' => ['id'], //详情
    'solve' => ['id' ,'content'], //处理
    'status_change' => ['id','status'], //修改状态
    'submit_problem_feedback' => ['problem_id','case_guid','user_guid'], //修改状态


  ];
}
