<?php

namespace App\Validate;

use App\Validate\BaseValidate;


/**
 * 图片直播验证类
 */
class PictureLiveValidate extends  BaseValidate
{
    protected  $rule = [
        'id' => 'bail|required',
        'title' => 'required',
        'main_img' => 'required',
        'img' => 'required',
        'act_id' => 'required|integer',
        'type_id' => 'required|integer',

        'is_check' => 'required|integer|in:1,2',
        // 'is_appoint' => 'required|integer|in:1,2',
        'is_appoint' => 'required|integer|in:1', //只能指定
        'is_play' => 'required|integer|in:1,2',
        'vote_way' => 'required|integer|in:1,2,3',
        'uploads_way' => 'required|integer|in:1,2,3',
        'number' => 'required|integer',

        'con_start_time' => 'bail|required|date',
        'con_end_time' => 'bail|required|date|after:con_start_time',
        'vote_start_time' => 'bail|date|after_or_equal:con_start_time',
        'vote_end_time' => 'bail|date|after:vote_start_time',

    ];



    # 错误提示语
    protected $message = [
        'id.required' => 'ID不能为空',
        //  'id.integer' => 'ID格式不正确',
        "title.required" => "活动名称不能为空",
        "main_img.required" => "进入主界面封面图片不能为空",
        "img.required" => "活动封面不能为空",
        "type_id.required" => "活动类型ID不能为空",
        "type_id.integer" => "活动类型ID参数错误",
        "act_id.required" => "活动ID不能为空",
        "act_id.integer" => "活动ID参数错误",

        "is_check.required" => "是否需要审核规则不能为空",
        "is_check.integer" => "是否需要审核规则参数错误",
        "is_check.in" => "是否需要审核规则不正确",

        "is_appoint.required" => "是否指定用户上传规则不能为空",
        "is_appoint.integer" => "是否指定用户上传规则参数错误",
        "is_appoint.in" => "是否指定用户上传规则不正确",

        "is_play.required" => "状态不能为空",
        "is_play.integer" => "状态参数错误",
        "is_play.in" => "状态参数错误",
        "vote_way.required" => "点赞方式不能为空",
        "vote_way.integer" => "点赞方式参数错误",
        "vote_way.in" => "点赞方式参数错误",
        "uploads_way.required" => "上传方式不能为空",
        "uploads_way.integer" => "上传方式参数错误",
        "uploads_way.in" => "上传方式参数错误",
        "number.required" => "点赞次数不能为空",
        "number.integer" => "点赞次数参数错误",


        //    'con_start_time.required' => '直播开始时间不能为空',
        'con_start_time.date' => '直播开始时间格式不正确',
        //    'con_end_time.required' => '直播结束时间不能为空',
        'con_end_time.date' => '直播结束时间格式不正确',
        'con_end_time.after' => '直播结束时间必须大于开始时间',

        'vote_start_time.required' => '点赞开始时间不能为空',
        'vote_start_time.date' => '点赞开始时间格式不正确',
        'vote_start_time.after_or_equal' => '点赞开始时间必须大于等于直播开始时间',
        'vote_end_time.required' => '点赞结束时间不能为空',
        'vote_end_time.date' => '点赞结束时间格式不正确',
        'vote_end_time.after' => '点赞结束时间必须大于点赞开始时间',
    ];

    protected  $scene = [
        'add' => ['title', 'main_img', 'img', 'is_check', 'is_play', 'vote_way', 'uploads_way'/* , 'number' */, 'con_start_time', 'con_end_time'/* , 'vote_start_time', 'vote_end_time' */, 'is_appoint'], //新增验证
        'change' => ['id', 'title', 'is_check', 'main_img', 'img',  'is_play', 'vote_way', 'uploads_way'/* , 'number' */, 'con_start_time', 'con_end_time'/* , 'vote_start_time', 'vote_end_time' */, 'is_appoint'], //更新验证
        'del' => ['id'], //活动删除
        'detail' => ['id'], //活动详情
        'cancel_and_release' => ['id', 'is_play'], //撤销 和发布



        'wx_scan_act_info' => ['qr_id', 'lon', 'lat'], // 扫码后获取活动信息

        'wx_list' => ['act_type_id'], //前台获取活动列表
        'wx_stairs_floor' => ['id'], //获取楼梯活动当前楼层

        'wx_detail' => ['id'], //前台获取活动详情
        'wx_resource_info' => ['id'], //获取资源路径及颜色等信息


        'wx_my_vote_picture_live_photo_list' => ['act_id'], //个人中心，我的点赞过的图片列表
        'wx_my_picture_live_photo_list' => ['act_id'], //个人中心，我的图片列表
        'wx_my_picture_live_detail' => ['id'], //个人中心，我的图片详情
    ];
}
