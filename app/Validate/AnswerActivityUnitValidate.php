<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class AnswerActivityUnitValidate extends  BaseValidate
{
    protected  $rule = [
        'id' => 'bail|required|integer',
        'act_id' => 'bail|required|integer',
        'nature' => 'bail|required|integer',
        'name' => 'bail|required|max:20',
        'img' => 'bail|required',
        'unit_ids' => 'bail|required',
        'orders' => 'bail|required',
        'is_default' => 'bail|required|in:1,2',
    ];

    # 错误提示语
    protected $message = [
        'id.required' => 'ID不能为空',
        'id.integer' => 'ID格式不正确',
        'act_id.required' => '活动ID不能为空',
        'act_id.integer' => '活动ID格式不正确',
        'nature.required' => '单位性质不能为空',
        'nature.integer' => '单位性质格式不正确',
        'name.required' => '名称不能为空',
        'name.size' => '名称最多20个字符',
        'img.required' => '图片不能为空',
        'unit_ids.required' => '单位ID不能为空',
        'orders.required' => '排序内容不能为空',
        'is_default.required' => '状态不能为空',
        'is_default.in' => '状态格式不正确',
    ];

    protected  $scene = [
        'filter_list' => ['act_id'], //获取筛选列表
        'list' => ['act_id'], //获取列表
        'add' => ['act_id', 'nature', 'name'], //新增验证
        'change' => ['id', 'nature', 'name'], //更新验证
        'detail' => ['id'], //详情
        'del' => ['id'], //删除验证
        'order' => ['orders'], //排序验证
        'cancel_and_default' => ['id', 'is_default'], //默认与取消默认
        'select_base_unit' => ['act_id', 'unit_ids'], //从基础数据库中选择单位到活动里面

        'wx_list' => ['act_id'], //前台获取活动单位列表
    ];
}
