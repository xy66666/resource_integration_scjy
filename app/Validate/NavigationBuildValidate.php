<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class NavigationBuildValidate extends  BaseValidate
{
    protected  $rule = [
        'id' => 'bail|required|integer',
        'ids' => 'bail|required',
        'name' => 'required',
        'is_play' => 'required|in:1,2',
        'province' => 'required',
        'city' => 'required',
        'district' => 'required',
        'address' => 'required',
    ];


    # 错误提示语
    protected $message = [
        'id.required' => 'ID不能为空',
        'id.integer' => 'ID格式不能为空',
        'ids.required' => 'ID不能为空',
        "name.required" => "名称不能为空",
        "is_play.required" => "是否使用不能为空",
        "is_play.in" => "是否使用规则不正确",

        "province.required" => "省级地址不能为空",
        "city.required" => "市级地址不能为空",
        "district.required" => "区级地址不能为空",
        "address.required" => "详细地址不能为空",
    ];

    protected  $scene = [
        'add' => ['name', 'province', 'city', 'district', 'address'], //新增验证
        'change' => ['id', 'name', 'province', 'city', 'district', 'address'], //更新验证
        'del' => ['id'], //删除
        'play_and_cancel' => ['ids', 'is_play'], //是否禁用
        'detail' => ['id'], //详情
    ];
}
