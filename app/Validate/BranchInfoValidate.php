<?php
namespace App\Validate;

use App\Validate\BaseValidate;
/**
 * 本馆简介
 */
class BranchInfoValidate extends BaseValidate {
    //验证规则
    protected $rule =[
        'id'=>'bail|required|integer',
        'branch_name'=>'bail|required',
        'img'=>'bail|required',
        'dispark_time'=>'bail|required',
        'transport_line'=>'bail|required',
        'province'=>'bail|required',
        'city'=>'bail|required',
        'district'=>'bail|required',
        'address'=>'bail|required',
        'tel'=>'bail|required|check_tel_and_phone:',
        'contacts'=>'bail|required',
        'intro'=>'bail|required',
        'email'=>'bail|required|email',

        'account'=>'bail|required|alpha_num|min:2|self_alpha_num',//验证字段必须是字母或数字。
        'password'=>'bail|required|min:6|max:20',

    ];
    //自定义验证信息
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        'branch_name.required'=>'场馆名称不能为空',
        'transport_line.required'=>'交通线路不能为空',
        'dispark_time.required'=>'开放时间不能为空',
        'img.required'=>'封面图片不能为空',
        'province.required'=>'省不能为空',
        'city.required'=>'市不能为空',
        'district.required'=>'区不能为空',
        'address.required'=>'详细地址不能为空',
        'tel.required'=>'联系电话不能为空',
        'tel.check_tel_and_phone'=>'联系电话规则不正确',
        'contacts.required'=>'联系人不能为空',
        'intro.required'=>'简介不能为空',
        'email.required'=>'邮箱不能为空',
        'email.email'=>'邮箱规则不正确',


        'account.required'=>'账号不能为空',
        'account.min'    =>'账号位数不合法',
        'account.alpha_num'    =>'账号规则不合法',
        'account.self_alpha_num'    =>'账号规则不合法',
        'password.required'=>'密码不能为空',
        'password.min'    =>'密码位数不正确',
        'password.max'    =>'密码位数不正确',

    ];

    //自定义场景
    protected $scene = [
    //    'add' => ['transport_line','dispark_time','img','province','city','district','address','tel','contacts','email','intro'],//添加
    //    'change' => ['id','transport_line','dispark_time','img','province','city','district','address','tel','contacts','email','intro'],//修改
        'add' => ['branch_name'],//添加
        'change' => ['id','branch_name'],//修改
        'detail' => ['id'],//详情
        'del' => ['id'],//删除


        'branch_account_login' => ['id','account','password'],//分馆账号登录
        'add_browse' => ['id'],//添加浏览量
    ];




}