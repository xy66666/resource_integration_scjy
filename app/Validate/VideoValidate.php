<?php
namespace App\Validate;

use App\Validate\BaseValidate;
/**
 * 作品 - 视频类 验证器
 */
class VideoValidate extends BaseValidate {
    //验证规则
    protected $rule =[
        'id'=>'bail|required|integer',
        'author_id'=>'bail|required|integer',
        'type_id'=>'bail|required|integer',
        'pro_name'=>'bail|required',
        'video_addr'=>'bail|required',
    //   /  'img'=>'bail|required|min:10',
    ];
    //自定义验证信息
    protected $message = [
        'id.required'=>'视频ID不能为空',
        'id.integer'=>'视频ID格式不正确',
        'author_id.required'=>'作者ID不能为空',
        'author_id.integer'=>'作者ID格式不正确',
        'type_id.required'=>'类型ID不能为空',
        'type_id.integer'=>'类型ID格式不正确',
        'pro_name.required'=>'视频名称不能为空',
        'video_addr.required'=>'视频不能为空',
        // 'img.required'=>'图片不能为空',
        // 'img.min'=>'图片规则不正确',
    ];

    //自定义场景
    protected $scene = [
        'video_add' => ['video_name','author_id','type_id','video_addr'],//视频添加
        'video_change' => ['id','video_name','author_id','type_id','video_addr'],//视频修改
        'video_del' => ['id'],//视频删除
        'video_info' => ['id'],//视频详情
    ];


}