<?php
namespace App\Validate;

use App\Validate\BaseValidate;

class NewBookRecommendValidate extends  BaseValidate
{
    protected  $rule =[
        'id' => 'required|integer',
        'book_id' => 'required|integer',
        'book_name' => 'required',
        'author' => 'required',
        'price' => 'required',
        'type_id' => 'required|integer',
        'isbn' => 'required',
        'is_recom' => 'required|integer|in:1,2',
    ];

    protected $message = [
        'id.required' => '书籍id不能为空',
        'id.integer' => '书籍id格式不正确',
        'book_id.required' => '书籍id不能为空',
        'book_id.integer' => '书籍id格式不正确',
        'type_id.required' => '书籍类型id不能为空',
        'type_id.integer' => '书籍类型id格式不正确',

        'book_name.required'=>'书名不能为空',
        'isbn.required'=>'ISBN号不能为空',
        'author.required'=>'作者不能为空',
        'price.required'=>'价格不能为空',
        'is_recom.required' => '是否推荐类型不能为空',
        'is_recom.integer' => '是否推荐类型格式不正确',
        'is_recom.integer' => '是否推荐类型格式不正确',
    ];

    protected  $scene = [
        'detail' => ['id'], //详情验证
        'add' => ['book_name','isbn','author','price','type_id'], //新增验证
        'change' => ['id','book_name','isbn','author','price','type_id'], //更新验证
        'del' => ['id'], //删除
        'cancel_and_release' => ['id','is_recom'], //上架与下架


        'wx_detail' => ['book_id'], //详情验证
        'new_book_recommend_collect' => ['book_id'], //新书收藏与取消收藏
    ];

}