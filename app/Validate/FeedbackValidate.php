<?php
namespace App\Validate;

use App\Validate\BaseValidate;
/**
 * 意见反馈 - 留言
 */
class FeedbackValidate extends BaseValidate {
    //验证规则
    protected $rule =[
        'id'=>'bail|required|integer',
        'username'=>'bail|required',
        'tel' => 'required|check_tel_and_phone:',
        'feedback'=>'bail|required',
        'content'=>'bail|required',

    ];
    //自定义验证信息
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        'username.required'=>'用户名不能为空',
        'tel.required'=>'电话号码不能为空',
        'tel.check_tel_and_phone'=>'电话号码不符合规范',
        'feedback.required'=>'反馈内容不能为空',
        'content.required'=>'回复内容不能为空',

    ];

    //自定义场景
    protected $scene = [
        'feedback' => ['username','tel','feedback'],//前台添加反馈信息
        'reply' => ['id','content'],//回复用户留言（只能回复一次）

    ];




}