<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class GoodsValidate extends  BaseValidate
{
    protected  $rule = [
        'id' => 'bail|required|integer',
        'name' => 'required',
        'type_id' => 'required|integer',
        'address_id' => 'required|integer',
        'order_id' => 'required|integer',
        'img' => 'required',
        'intro' => 'required',
        'score' => 'required|integer',
        'times' => 'required|integer',
        'number' => 'required|integer',
        'tel' => 'required|check_tel_and_phone',
        'contacts' => 'required',
        'send_way' => 'required',
        'start_time' => 'required',
        'end_time' => 'required',
        'status' => 'required',
        'is_play' => 'bail|required|in:1,2',
        'is_all' => 'bail|required|in:1,2',

        'province' => 'bail|required',
        'city' => 'bail|required',
        'district' => 'bail|required',
        'address' => 'bail|required',
        'order_number' => 'bail|required|min:8',
        'refund_remark' => 'bail|required',
    ];

    # 错误提示语
    protected $message = [
        "id.required" => "ID不能为空",
        "id.integer" => "ID规则不正确",
        "name.required" => "商品名称不能为空",
        "type_id.required" => "请选择商品类型",
        "type_id.integer" => "商品类型格式错误",
        "address_id.required" => "请选择地址",
        "address_id.integer" => "地址不能为空",
        "order_id.required" => "订单ID不能为空",
        "order_id.integer" => "订单ID格式错误",
        "img.required" => "商品封面不能为空",
        "score.required" => "商品兑换所需积分不能为空",
        "score.integer" => "商品兑换所需积分格式错误",
        "number.required" => "商品库存数量不能为空",
        "number.integer" => "商品库存数量格式错误",
        "times.required" => "可兑换次数不能为空",
        "times.integer" => "可兑换次数格式错误",
        "tel.required" => "联系电话不能为空",
        'tel.check_tel_and_phone' => '联系电话规则不正确',
        "contacts.required" => "联系人不能为空",
        "send_way.required" => "请选择商品配送方式",
        "start_time.required" => "请选择商品兑换开始时间",
        "end_time.required" => "请选择商品兑换结束时间",
        "intro.required" => "商品简介不能为空",
        'is_play.required' => '类型不能为空',
        'is_play.in' => '类型规则不正确',
        'is_all.required' => '类型不能为空',
        'is_all.in' => '类型规则不正确',

        'refund_remark.required' => '理由不能为空',

        'province.required' => '省不能为空',
        'city.required' => '市不能为空',
        'district.required' => '区不能为空',
        'address.required' => '详细地址不能为空',
        'order_number.required' => '快递单号不能为空',
        'order_number.min' => '快递单号不符合规范',
    ];

    protected  $scene = [
        'detail' => ['id'],
        'add' => ['name', 'type_id', 'img', 'intro', 'score', 'number', 'times'/* , 'tel', 'contacts' */, 'send_way', 'start_time', 'end_time'], //新增验证
        'change' => ['id', 'name', 'type_id', 'img', 'intro', 'score', 'number', 'times'/* , 'tel', 'contacts' */, 'send_way', 'start_time', 'end_time'], //更新验证
        'del' => ['id'],

        'goodsOnAndOff' => ['is_play', 'is_all'], //上架个下架商品
        'setGoodsPickAddress' => ['province', 'city', 'district', 'address', 'tel', 'contacts'], //设置商品自提地址

        'web_detail' => ['id'], //前台详情
        'web_goods_conversion' => ['id', 'send_way'], //商品兑换 生成预支付订单
        'web_goods_cancel_conversion' => ['order_id', 'refund_remark'], //商品取消兑换
        'web_goods_cancel_order' => ['order_id', 'refund_remark'], //商品取消订单
        'web_order_detail' => ['order_id'], //我的兑换详情

        'fetch' => ['id'], //领取商品
        'send' => ['id','tracking_number'], //商品发货
        'cancel_conversion' => ['id', 'refund_remark'], //商品取消兑换
    ];
}
