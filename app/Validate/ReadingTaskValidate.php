<?php

namespace App\Validate;

use App\Validate\BaseValidate;


/**
 * 阅读任务验证类
 */
class ReadingTaskValidate extends  BaseValidate
{
    protected  $rule = [
        'id' => 'bail|required',
        'title' => 'required',
        'main_img' => 'required',
        'img' => 'required',
        'act_id' => 'required|integer',
        'type_id' => 'required|integer',
        'database_id' => 'required',
        'task_id' => 'required',
        'works_id' => 'required',

        'type' => 'required|integer|in:1,2',
        'is_check' => 'required|integer|in:1,2',
        'is_appoint' => 'required|integer|in:1,2',
        'is_play' => 'required|integer|in:1,2',
        'vote_way' => 'required|integer|in:1,2',
        'uploads_way' => 'required|integer|in:1,2,3',
        'number' => 'required|integer',

        'start_time' => 'bail|required|date',
        'end_time' => 'bail|required|date|after:start_time',

    ];



    # 错误提示语
    protected $message = [
        'id.required' => 'ID不能为空',
        //  'id.integer' => 'ID格式不正确',
        "title.required" => "阅读任务名称不能为空",
        "main_img.required" => "进入主界面封面图片不能为空",
        "img.required" => "阅读任务封面不能为空",
        "type_id.required" => "阅读任务类型ID不能为空",
        "type_id.required" => "阅读任务类型ID不能为空",
        "database_id.required" => "阅读任务数据库ID不能为空",
        "task_id.required" => "阅读任务ID不能为空",
        "works_id.required" => "阅读任务作品ID不能为空",
        "act_id.required" => "阅读任务ID不能为空",
        "act_id.integer" => "阅读任务ID参数错误",

        "is_check.required" => "是否需要审核规则不能为空",
        "is_check.integer" => "是否需要审核规则参数错误",
        "is_check.in" => "是否需要审核规则不正确",
        "type.required" => "类型规则不能为空",
        "type.integer" => "类型规则参数错误",
        "type.in" => "类型规则不正确",

        "is_appoint.required" => "是否指定用户上传规则不能为空",
        "is_appoint.integer" => "是否指定用户上传规则参数错误",
        "is_appoint.in" => "是否指定用户上传规则不正确",

        "is_play.required" => "状态不能为空",
        "is_play.integer" => "状态参数错误",
        "is_play.in" => "状态参数错误",
        "vote_way.required" => "投票方式不能为空",
        "vote_way.integer" => "投票方式参数错误",
        "vote_way.in" => "投票方式参数错误",
        "uploads_way.required" => "上传方式不能为空",
        "uploads_way.integer" => "上传方式参数错误",
        "uploads_way.in" => "上传方式参数错误",
        "number.required" => "每日答题次数不能为空",
        "number.integer" => "每日答题次数参数错误",


        'start_time.required' => '阅读任务开始时间不能为空',
        'start_time.date' => '阅读任务开始时间格式不正确',
        'end_time.required' => '阅读任务结束时间不能为空',
        'end_time.date' => '阅读任务结束时间格式不正确',
        'end_time.after' => '阅读任务结束时间必须大于开始时间',

        'vote_start_time.required' => '投票开始时间不能为空',
        'vote_start_time.date' => '投票开始时间格式不正确',
        'vote_start_time.after_or_equal' => '投票开始时间必须大于等于直播开始时间',
        'vote_end_time.required' => '投票结束时间不能为空',
        'vote_end_time.date' => '投票结束时间格式不正确',
        'vote_end_time.after' => '投票结束时间必须大于投票开始时间',
    ];

    protected  $scene = [
        'add' => ['title', 'img', 'is_play',  'start_time', 'end_time', 'is_appoint', 'database_id'], //新增验证
        'change' => ['id', 'title', 'img', 'start_time', 'end_time', 'is_appoint', 'database_id'], //更新验证
        'del' => ['id'], //阅读任务删除
        'detail' => ['id'], //阅读任务详情
        'cancel_and_release' => ['id', 'is_play'], //撤销 和发布

        'reading_task_user_execute_list' => ['task_id'], //用户阅读情况任务


        // 'wx_scan_act_info' => ['qr_id', 'lon', 'lat'], // 扫码后获取阅读任务信息

        // 'wx_list' => ['act_type_id'], //前台获取阅读任务列表

        'wx_works_database_list' => ['task_id','database_id'], //前台获取阅读任务详情
        'wx_detail' => ['id'], //前台获取阅读任务详情
        'wx_reading_task_reading' => ['database_id', 'task_id', 'type'], //用户阅读 阅读任务

        



    ];
}
