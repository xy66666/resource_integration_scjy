<?php

namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * 用户验证器
 */
class UserInfoValidate extends BaseValidate
{
    //验证规则
    protected $rule = [
        'user_guid' => 'bail|required|size:32',
        'account_id' => 'bail|required|integer',
        'user_id' => 'bail|required|integer',
        'account' => 'required',
        'password' => 'required',
        'type' => 'required|in:1,2',
        //  'node' => 'required|in:all,activity,reservation,contest,scenic,study_room_reservation',
        'node' => 'required',
    ];
    //自定义验证信息
    protected $message = [
        'account_id.required' => 'ID不能为空',
        'account_id.integer' => 'ID格式不正确',
        'user_id.required' => 'ID不能为空',
        'user_id.integer' => 'ID格式不正确',
        'user_guid.required' => 'ID不能为空',
        'user_guid.size'    => 'ID不合法',
        'account.required'  => '读者证号不能为空',
        'password.required' => '密码不能为空',
        "type.required" => "类型不能为空",
        "type.in" => "类型规则不正确",
        "node.required" => "类型不能为空",
        "node.in" => "类型规则不正确",

    ];

    //自定义场景
    protected $scene = [
        'web_login' => ['account', 'password'], //读者证号登录
        'wechat_login' => ['account', 'password', 'type'], //读者证号登录
        'clear_violate' => ['node'], //清空违规次数
        'violate_list' => ['user_id', 'node'], //获取违规列表

    ];
}
