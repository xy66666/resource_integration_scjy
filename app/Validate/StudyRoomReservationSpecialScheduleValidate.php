<?php

namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * 预约验证
 */
class StudyRoomReservationSpecialScheduleValidate extends  BaseValidate
{
    protected  $rule = [
        'id' => 'bail|required|integer',
        'reservation_id' => 'bail|required|integer',
        'name' => 'required',
        'date' => 'bail|required',
        'time' => 'bail|required',
    ];


    # 错误提示语
    protected $message = [
        "id.required" => "ID不能为空",
        "id.integer" => "ID规则不正确",
        "name.required" => "特殊预约名称不能为空",
        "reservation_id.required" => "ID不能为空",
        "reservation_id.integer" => "ID规则不正确",
        "date.required" => "日期不能为空",
        //   "date.date" => "日期格式不正确",
        "time.required" => "时间段不能为空",

    ];

    protected  $scene = [
        //特殊日期新增验证
        'list' => ['reservation_id'],
        //特殊日期新增验证
        'add' => ['date', 'name', 'reservation_id', 'time'],
        //特殊日期编辑验证
        'change' => ['id', 'name', 'reservation_id', 'time'],
        //特殊日期详细验证
        'detail' => ['id'],
        //特殊日期详细验证
        'del' => ['id'],

    ];
}
