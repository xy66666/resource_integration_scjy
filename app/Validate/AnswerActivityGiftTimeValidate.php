<?php
namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * 礼物排版表
 */
class AnswerActivityGiftTimeValidate extends  BaseValidate
{
    protected  $rule =[
        'id'=>'bail|required|integer',
        'act_id'=>'bail|required|integer',
        'number' => 'bail|required|integer|min:1|max:10000',
        'user_gift_number' => 'bail|required|integer|min:1|max:100',
        'type' => 'bail|required|in:1,2',
        'percent_way' => 'bail|required|in:1,2',
        'start_time' => 'required|date',
        'end_time' => 'required|date|after:start_time',
    ];

    # 错误提示语
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        'act_id.required'=>'活动ID不能为空',
        'act_id.integer'=>'活动ID格式不正确',
        'number.required'=>'发放数量不能为空',
        'number.integer'=>'发放数量格式不正确',
        'number.min'=>'发放数量最小不能少于1个',
        'number.max'=>'发放数量最大不能超过10000个',
        'user_gift_number.required'=>'用户当日可获得多少礼品不能为空',
        'user_gift_number.integer'=>'用户当日可获得多少礼品格式不正确',
        'user_gift_number.min'=>'用户当日可获得多少礼品不能少于1个',
        'user_gift_number.max'=>'用户当日可获得多少礼品不能超过100个',
        'type.required'=>'奖品类型不能为空',
        'type.in'=>'奖品类型格式不正确',
        'percent_way.required'=>'概率方式不能为空',
        'percent_way.in'=>'概率方式格式不正确',

        "start_time.required" => "活动开始时间不能为空",
        "start_time.date" => "活动开始时间参数错误",
        "end_time.required" => "活动结束时间不能为空",
        "end_time.date" => "活动结束时间参数错误",
        "end_time.after" => "活动结束时间必须在活动开始时间之后",
    ];

    protected  $scene = [
        'list' => ['act_id'], //获取列表

        'add' => ['act_id','start_time','end_time','type','number','user_gift_number'], //新增验证
        'change' => ['id','start_time','end_time','type','number','user_gift_number'], //更新验证
        'detail' => ['id'], //详情
        'del' => ['id'], //删除验证

        'get_send_total_config' => ['act_id'], //获取礼物发放总概率 
        'set_send_total_config' => ['act_id','percent_way'], //设置礼物发放总概率 

        

        'wx_list' => ['act_id'], //前台获取活动单位列表
    ];



}