<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class NavigationValidate extends BaseValidate
{
    protected $rule = [
        'build_id' => 'bail|required|integer',
        'area_id' => 'bail|required|integer',
        'start_area_id' => 'required|integer',
        'end_area_id' => 'required|integer',
        'end_point_id' => 'required|integer',
        'keywords_type' => 'required|in:1,2',
        'keywords' => 'bail|required',
    ];


    # 错误提示语
    protected $message = [
        'build_id.required' => 'ID不能为空',
        'build_id.integer' => 'ID格式不能为空',
        'area_id.required' => '区域ID不能为空',
        'area_id.integer' => '区域ID格式不能为空',

        'start_area_id.required' => "请选择起点区域",
        'end_area_id.required' => "请选择目的地区域",
        'end_point_id.required' => "请选择目的地点位",

        "keywords_type.required" => "检索类型不能为空",
        "keywords_type.in" => "检索类型规则不正确",
        'keywords.required' => '内容不能为空',
    ];

    protected $scene = [
        'area_list' => ['build_id'], //
        'point_list' => ['area_id'], //

        'search' => ['build_id', 'keywords_type', 'keywords'], //搜索
        'line_list' => ['start_area_id', 'end_point_id', 'end_area_id'],
    ];
}
