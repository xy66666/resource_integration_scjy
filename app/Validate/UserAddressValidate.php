<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class UserAddressValidate extends  BaseValidate
{
    protected  $rule =[
        'id'=>'bail|required|integer',
        'username' => 'required|min:2',
        'province' => 'required',
        'city' => 'required',
        'district' => 'required',
        'address' => 'required',
        'is_default' => 'required|in:1,2',
        'tel' => 'required|check_tel_and_phone:'
    ];


    # 错误提示语
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        "username.required" => "用户名不能为空",
        "username.min" => "用户名最少2个字",
        "province.required" => "省级地址不能为空",
        "city.required" => "市级地址不能为空",
        "district.required" => "区级地址不能为空",
        "address.required" => "详细地址不能为空",
        "is_default.required" => "是否默认地址不能为空",
        "is_default.in" => "是否默认地址规则不正确",
    
        "tel.required" => "活动联系方式不能为空",
        "tel.check_tel_and_phone" => "活动联系方式格式不正确",
    ];

    protected  $scene = [
        'web_add' => ['username','tel','province','city','district','address','is_default'], //新增验证
        'web_change' => ['id','username','tel','province','city','district','address','is_default'], //更新验证
        'web_del' => ['id'], //删除
        'web_detail' => ['id'], //详情

        'web_switchover_default' => ['id'], //切换默认状态
    ];


}