<?php
namespace App\Validate;

use App\Validate\BaseValidate;


class CodeGuideProductionTypeValidate extends  BaseValidate
{
    protected  $rule =[
        'id'=>'bail|required|integer',
        'type_name' => 'bail|required',
    ];

    # 错误提示语
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        'type_name.required'=>'类型不能为空',
    ];


    protected  $scene = [
        'add' => ['type_name'], //新增验证
        'change' => ['id','type_name'], //更新验证
        'detail' => ['id'], //详情
        'del' => ['id'] //删除验证
    ];



}