<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class TurnActivityValidate extends  BaseValidate
{
    protected  $rule = [
        'id' => 'bail|required|integer',
        'password' => 'bail|required|min:6',
        'act_type_id' => 'bail|required|integer|in:1,2,3',
        'title' => 'required|max:20',
        'act_id' => 'required|integer',
        'type_id' => 'required|integer',
        'tag_id' => 'required', //可选多个

        'theme_color' => 'required',
        'progress_color' => 'required',
        'invite_color' => 'required',

        'is_play' => 'required|integer|in:1,2',
        'node' => 'required|integer|in:1,2,3',
        'vote_way' => 'required|integer|in:1,2',

        'number' => 'required|integer',
        'share_number' => 'required|integer',
        'round_number' => 'required|integer|min:1|max:8',
        'invite_code' => 'required|digits:6',

        'rule_type' => 'required|integer|in:1,2',
        'rule' => 'required',
        'start_time' => 'required|date',
        'end_time' => 'required|date|after:start_time',
        'turn_start_time' => 'required|date',
        'turn_end_time' => 'required|date|after:turn_start_time',


        'lon' => 'bail|required',
        'lat' => 'bail|required',
    ];





    # 错误提示语
    protected $message = [
        'id.required' => 'ID不能为空',
        'id.integer' => 'ID格式不正确',
        "title.required" => "活动名称不能为空",
        "title.max" => "活动名称不能超过20个字",
        "type_id.required" => "活动类型ID不能为空",
        "type_id.integer" => "活动类型ID参数错误",
        "act_id.required" => "活动ID不能为空",
        "act_id.integer" => "活动ID参数错误",
        "tag_id.required" => "活动标签ID不能为空",

        "act_type_id.required" => "活动类型ID不能为空",
        "act_type_id.integer" => "活动类型ID参数错误",
        "act_type_id.in" => "活动类型ID规则不正确",

        "password.required" => "管理员密码不能为空",
        "password.min" => "管理员密码位数不正确",


        "is_play.required" => "状态不能为空",
        "is_play.integer" => "状态参数错误",
        "is_play.in" => "状态参数错误",

        "invite_code.required" => "邀请码不能为空",
        "invite_code.digits" => "邀请码格式不正确",

        "node.required" => "执行时间类型不能为空",
        "node.integer" => "执行时间类型参数错误",
        "node.in" => "执行时间类型参数错误",
        "vote_way.required" => "活动执行方式不能为空",
        "vote_way.integer" => "活动执行方式参数错误",
        "vote_way.in" => "活动执行方式参数错误",

        "number.required" => "每日答题次数不能为空",
        "number.integer" => "每日答题次数参数错误",
        "share_number.required" => "每日分享获取答题次数不能为空",
        "share_number.integer" => "每日分享获取答题次数参数错误",
        "round_number.required" => "每轮次数不能为空",
        "round_number.integer" => "每轮次数参数错误",
        "round_number.min" => "每轮次数不能少于1次",
        "round_number.max" => "每轮次数不能超过8次",

        "rule_type.required" => "规则方式不能为空",
        "rule_type.integer" => "规则方式参数错误",
        "rule_type.in" => "规则方式参数错误",
        "rule.required" => "规则不能为空",

        "start_time.required" => "活动开始时间不能为空",
        "start_time.date" => "活动开始时间参数错误",
        "end_time.required" => "活动结束时间不能为空",
        "end_time.date" => "活动结束时间参数错误",
        "end_time.after" => "活动结束时间必须在活动开始时间之后",

        "turn_start_time.required" => "抽奖开始时间时间不能为空",
        "turn_start_time.date" => "抽奖开始时间时间参数错误",


        "turn_end_time.required" => "抽奖结束时间不能为空",
        "turn_end_time.date" => "抽奖结束时间参数错误",
        "turn_end_time.after" => "抽奖结束时间必须在抽奖开始时间之后",


        'theme_color.required' => '主题色不能为空',
        'progress_color.required' => '进度条颜色色不能为空',
        'invite_color.required' => '邀请码颜色不能为空',


        'lon.required' => '经纬度不能为空',
        'lat.required' => '经纬度不能为空',
    ];





    protected  $scene = [
        'add' => ['title', 'node', 'execute_day', 'number',/* 'share_number', */ 'round_number', 'rule_type', 'rule', 'start_time', 'end_time', 'turn_start_time', 'turn_end_time'], //新增验证
        'change' => ['id', 'title', 'node', 'execute_day', 'number',/* 'share_number', */ 'round_number', 'rule_type', 'rule', 'start_time', 'end_time', 'turn_start_time', 'turn_end_time'], //更新验证
        'del' => ['id'], //活动删除
        'detail' => ['id'], //活动详情

        'cancel_and_release' => ['id', 'is_play'], //撤销 和发布
        'invite_code_change' => ['id', 'invite_code'], //邀请码修改

        'copy_data' => ['act_id', 'password'], //复制活动


        'get_color_resource' => ['id'], //获取活动资源（颜色）
        'set_color_resource' => ['id', 'theme_color', 'progress_color', 'invite_color'], //设置活动资源（颜色）




        'wx_list' => ['act_type_id'], //前台获取活动列表
        'wx_detail' => ['id', 'lon', 'lat'], //前台获取活动详情
        'wx_resource_info' => ['id'], //获取资源路径及颜色等信息
    ];
}
