<?php
namespace App\Validate;

use App\Validate\BaseValidate;

class VolunteerServiceIntentionValidate extends BaseValidate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id'=>'required|integer',
        'name' => 'required',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        'name.required'=>'服务意向不能为空',
    ];

    protected  $scene = [
        'add' => ['name'], //新增验证
        'change' => ['id','name'], //更新验证
        'detail' => ['id'], //详情
        'del' => ['id'] //删除验证
    ];
}
