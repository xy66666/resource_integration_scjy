<?php

namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * 图书到家线下荐购
 * Class BookHomeOfflineRecomValidate
 * @package app\api\validate
 */
class BookHomeOfflineRecomValidate extends BaseValidate
{
    protected  $rule =[
        'account|读者证号' => 'require',
        'password|读者证密码' => 'require',
        'shop_id|书店id' => 'require|number',
        'account_id|读者证号id' => 'require|number',
        'book_id|书籍id' => 'require|number',
        'isbn|isbn号' => 'require|check_isbn',
        'barcode|条形码' => 'require',
    ];


      //自定义验证信息
    protected $message = [
        'account_id.required' => '读者证号ID不能为空',
        'account_id.integer' => '读者证号ID格式不正确',
        'shop_id.required' => 'ID不能为空',
        'shop_id.integer' => 'ID格式不正确',
        'book_id.required' => 'ID不能为空',
        'book_id.integer' => 'ID格式不正确',
        'account.required' => '读者证号不能为空',
        'password.required' => '密码不能为空',

        'isbn.required' => 'ISBN号不能为空',
        'isbn.check_isbn' => 'ISBN号规则不正确',
        'barcode.required' => '条形码不能为空',
    ];

    protected  $scene = [
        'get_reader_info' => ['account' , 'password'], //获取读者证号信息
        'get_book_info' => ['shop_id' , 'isbn' , 'account_id'], //获取书籍信息
        'affirm_borrow' => ['shop_id', 'account_id' , 'barcode'], //确认借阅


    ];


}