<?php
namespace App\Validate;

use App\Validate\BaseValidate;
/**
 * 验证器
 */
class SurveyValidate extends BaseValidate 
{

     //验证规则
     protected $rule =[
        'id'=>'bail|required|integer',
        'sur_id'=>'bail|required|integer',
        'survey_name'=>'bail|required',
        'start_time' => 'required|date',
        'end_time' => 'required|date|after:start_time',
        'problem'=>'bail|required',
        'is_play'=>'bail|required|in:1,2',
        'type'=>'bail|required|in:1,2,3',
        'answers'=>'bail|required|json'
    ];
    //自定义验证信息
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        'sur_id.required'=>'问卷ID不能为空',
        'sur_id.integer'=>'问卷ID格式不正确',
        'survey_name.required'=>'问卷名不能为空',
        "start_time.required" => "问卷开始时间不能为空",
        "start_time.date" => "问卷开始时间参数错误",
        "end_time.required" => "问卷结束时间不能为空",
        "end_time.date" => "问卷结束时间参数错误",
        "end_time.after" => "问卷结束时间不能小于开始时间",
        "problem.required" => "问题不能为空",
        "is_play.required" => "状态不能为空",
        "is_play.in" => "状态有误",
        "is_play.required" => "题目类型不能为空",
        "is_play.in" => "题目类型有误",
        "answers.required" => "答案不能为空",
        "answers.json" => "答案格式不正确",
    ];


    protected $scene = [
        // 问卷调查详情
        "detail" => ["id"],
        // 问卷调查 前台详情
        "web_detail" => ["id"],

        // 问卷调查添加
        "add" => ["survey_name", "start_time",'end_time','problem'],

        // 问卷调查修改
        "change" => ["survey_name", "start_time",'end_time','problem','sur_id'],

        // 问卷调查删除
        "del" => ["sur_id"],

        // 问卷调查 撤销  与 发布
        "cancelAndRelease" => ["sur_id" , 'is_play'],

        // 问卷调查 前台用户回复问题
        "apply" => ["id" , 'answers'],
    ];


}