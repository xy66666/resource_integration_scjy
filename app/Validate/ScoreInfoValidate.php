<?php

namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * 积分规则设置
 */
class ScoreInfoValidate extends  BaseValidate
{
    protected  $rule =[
        'id'=>'bail|required|integer',
        'account_id'=>'bail|required|integer',
        'score' => 'required|integer',
        'number' => 'required|integer',
        'reason' => 'required',
    ];


    # 错误提示语
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        'account_id.required'=>'ID不能为空',
        'account_id.integer'=>'ID格式不正确',
        "score.required" => "积分不能为空",
        "score.integer" => "积分类型错误",
        "number.required" => "次数不能为空",
        "number.integer" => "次数类型错误",
        "reason.required" => "调整原因不能为空",
       
    ];

    protected  $scene = [
        'change' => ['id','number'], //设置规则
        'score_change' => ['account_id','score','reason'], //调整用户积分
        'score_log' => ['account_id'], //积分日志
     
    ];


}