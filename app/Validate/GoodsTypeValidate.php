<?php
namespace App\Validate;

use App\Validate\BaseValidate;

class GoodsTypeValidate extends  BaseValidate
{
    protected  $rule =[
        'type_name' => 'required|max:10',
        'id' => 'required|integer',
    ];

    # 错误提示语
    protected $message = [
        "id.required" => "类型id不能为空",
        "id.integer" => "类型id规则不正确",
        "type_name.required" => "类型名称不能为空",
        "type_name.max" => "类型名称最长为10个字符",
    ];

    protected  $scene =[
        'detail' => ['id'],
        'add' => ['type_name'], //新增验证
        'change' => ['id','type_name'], //更新验证
        'del' => ['id']
    ];

}