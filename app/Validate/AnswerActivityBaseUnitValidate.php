<?php
namespace App\Validate;

use App\Validate\BaseValidate;

class AnswerActivityBaseUnitValidate extends  BaseValidate
{
    protected  $rule =[
        'id'=>'bail|required|integer',
        'nature'=>'bail|required|integer',
        'name' => 'bail|required|max:20',
        'img' => 'bail|required',
        'orders' => 'bail|required',
        'is_default' => 'bail|required|in:1,2',
    ];

    # 错误提示语
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        'nature.required'=>'单位性质不能为空',
        'nature.integer'=>'单位性质格式不正确',
        'name.required'=>'名称不能为空',
        'name.size'=>'名称最多20个字符',
        'img.required'=>'图片不能为空',
        'orders.required'=>'排序内容不能为空',
        'is_default.required'=>'状态不能为空',
        'is_default.in'=>'状态格式不正确',
    ];

    protected  $scene = [
        'add' => ['nature','name'], //新增验证
        'change' => ['id','nature','name'], //更新验证
        'detail' => ['id'], //详情
        'del' => ['id'], //删除验证
        'order' => ['orders'], //排序验证
        'cancel_and_default' => ['id','is_default'], //默认与取消默认

    ];



}