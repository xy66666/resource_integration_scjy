<?php

namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * 线上大赛活动原创申明
 */
class CompetiteActivityDeclareValidate extends  BaseValidate
{
     //验证规则
  protected $rule = [
    'id' => 'bail|required|integer',


  ];


  //自定义验证信息
  protected $message = [
    'id.required' => 'ID不能为空',
    'id.integer' => 'ID格式不正确',
  

  ];

  //自定义场景
  protected $scene = [
    'detail' => ['id'], //详情
    'change' => ['id'], //详情


  ];

}



