<?php
namespace App\Validate;

use App\Validate\BaseValidate;

class AnswerActivityRankingValidate extends  BaseValidate
{
    protected  $rule =[
        'id'=>'bail|required|integer',
        'act_id'=>'bail|required|integer',
    ];

    # 错误提示语
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        'act_id.required'=>'活动ID不能为空',
        'act_id.integer'=>'活动ID格式不正确',
    ];


    protected  $scene = [
        'ranking' => ['act_id'], //排名

        'grade' => ['act_id'], //成绩

        'ranking_transfer' => ['content', 'login_password'], //排名转账


    ];



}