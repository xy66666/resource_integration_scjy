<?php
namespace App\Validate;

use App\Validate\BaseValidate;
/**
 * 码上借
 */
class CodeOnBorrowValidate extends BaseValidate {
    //验证规则
    protected $rule =[
        'id'=>'bail|required|integer',
        'barcode'=>'bail|required',
        'lon'=>'bail|required',
        'lat'=>'bail|required',
        'qr_code'=>'bail|required|size:10',

    ];
    //自定义验证信息
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',

        'barcode.required'=>'条形码不能为空',
        'lon.required'=>'经度不能为空',
        'lat.required'=>'纬度不能为空',
        'qr_code.required'=>'请扫码场所二维码',
        'qr_code.size'=>'请扫码正确场所二维码',


    ];

    //自定义场景
    protected $scene = [
        'get_code_info' => ['lon','lat','qr_code'],//扫码借，扫描场所码后，获取场所信息，验证场所数据
        'get_book_info_by_barcode' => ['barcode','lon','lat','qr_code'],//扫码借书，还书，获取书籍信息
        'borrow_book' => ['barcode','lon','lat','qr_code'],//借阅书籍
        'return_book' => ['barcode','lon','lat','qr_code'],//归还书籍
    ];




}