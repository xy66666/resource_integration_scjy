<?php

namespace App\Validate;

use App\Validate\BaseValidate;


/**
 * 景点打卡
 */
class ScenicValidate extends  BaseValidate
{
     //验证规则
  protected $rule = [
    'id' => 'bail|required|integer',
    'title' => 'bail|required',
    'province' => 'bail|required',
    'city' => 'bail|required',
    'district' => 'bail|required',
    'address' => 'bail|required',
    'is_reader' => 'bail|required|integer|in:1,2',
    'is_play' => 'bail|required|integer|in:1,2',
    'start_time' => 'required|date',
    'end_time' => 'required|date|after:start_time',
    'type_id' => 'required|integer',
  ];
  //自定义验证信息
  protected $message = [
    'id.required' => 'ID不能为空',
    'id.integer' => 'ID格式不正确',
    'title.required' => '名称不能为空',
    'province.required' => '省不能为空',
    'city.required' => '市不能为空',
    'district.required' => '区不能为空',
    'address.required' => '详细不能为空',
    'is_reader.required' => '是否需要绑定读者证不能为空',
    "is_reader.integer" => "是否需要绑定读者证规则不正确",
    'is_reader.in' => '是否需要绑定读者证规则不正确',

    'is_play.required' => '是否发布不能为空',
    "is_play.integer" => "是否发布规则不正确",
    'is_play.in' => '是否发布规则不正确',

    "start_time.required" => "活动开始时间不能为空",
    "start_time.date" => "活动开始时间参数错误",
    "end_time.required" => "活动结束时间不能为空",
    "end_time.date" => "活动结束时间参数错误",
    "end_time.after" => "活动结束时间必须在活动开始时间之后",

    "type_id.required" => "活动类型ID不能为空",
    "type_id.integer" => "活动类型ID参数错误",

  ];




  //自定义场景
  protected $scene = [
    'detail' => ['id'], //详情
    'add' => ['title','type_id','province','city','district','address','is_reader','start_time','end_time'], //添加
    'change' => ['id','title','type_id','province','city','district','address','is_reader','start_time','end_time'], //添加
    'del' => ['id'], //删除
    'cancel_and_release' => ['id','is_play'], //删除


  ];

}