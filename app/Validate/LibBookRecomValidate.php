<?php
namespace App\Validate;

use App\Validate\BaseValidate;

class LibBookRecomValidate extends  BaseValidate
{
    protected  $rule =[
        'id' => 'required|integer',
        'ids' => 'required',
        'book_id' => 'required|integer',
        'book_name' => 'required',
        'author' => 'required',
        'price' => 'required',
        'type_id' => 'required|integer',
        'isbn' => 'required',
        'barcode' => 'required',
        'is_recom' => 'required|integer|in:1,2',
    ];

    protected $message = [
        'id.required' => '书籍id不能为空',
        'id.integer' => '书籍id格式不正确',
        'ids.required' => '书籍id不能为空',
        'book_id.required' => '书籍id不能为空',
        'book_id.integer' => '书籍id格式不正确',
        'type_id.required' => '书籍类型id不能为空',
        'type_id.integer' => '书籍类型id格式不正确',
        'book_name.required'=>'书名不能为空',
        'isbn.required'=>'ISBN号不能为空',
        'barcode.required'=>'条形码不能为空',
        'author.required'=>'作者不能为空',
        'price.required'=>'价格不能为空',
        'is_recom.required' => '是否推荐类型不能为空',
        'is_recom.integer' => '是否推荐类型格式不正确',
        'is_recom.integer' => '是否推荐类型格式不正确',
    ];

    protected  $scene = [
        'detail' => ['id'], //详情验证
        'add' => ['book_name','isbn','barcode','author','price','type_id'], //新增验证
        'change' => ['id','book_name','isbn','barcode','author','price','type_id'], //更新验证
        'del' => ['id'], //删除

        'cancel_and_release'   => ['ids' , 'is_recom'],//推荐与取消推荐


        'lib_book_list'   => ['keywords_type','keywords'],//检索馆藏书籍
        'lib_book_detail'   => ['metaid','metatable'],//获取馆藏书籍详情
        'lib_book_schoolbag'   => ['book_id' , 'barcode_id'],//馆藏书加入书袋与移出书袋
        'get_order_forecast_price'   => ['barcode_ids'],//获取订单预估金额
        'get_order_info'   => ['barcode_ids' , 'address_id'],//获取预支付订单信息
        'my_order_detail'   => ['order_id'],//获取订单详情
        'book_renew'   => ['content'],// 续借
    ];

}