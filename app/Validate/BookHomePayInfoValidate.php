<?php

namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * 支付模块
 * Class PayInfoValidate
 * @package app\port\validate
 */
class BookHomePayInfoValidate extends BaseValidate
{
    protected  $rule = [
        'order_id' => 'bail|required|integer', //订单id
    ];

    //自定义验证信息
    protected $message = [
        'order_id.required' => '订单ID不能为空',
        'order_id.integer' => '订单ID格式不正确',
    ];

    protected  $scene = [
        'payment_success'   => ['order_id'], //订单id
        'get_pay_param'   => ['order_id'], //订单id
    ];
}
