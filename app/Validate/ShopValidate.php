<?php

namespace App\Validate;

use App\Validate\BaseValidate;


/**
 * 书店类型
 */
class ShopValidate extends  BaseValidate
{
     //验证规则
  protected $rule = [
    'id' => 'bail|required|integer',
    'name' => 'bail|required',
    'province' => 'bail|required',
    'city' => 'bail|required',
    'district' => 'bail|required',
    'address' => 'bail|required',
    'way' => 'bail|required|in:1,2,3',

  ];
  //自定义验证信息
  protected $message = [
    'id.required' => 'ID不能为空',
    'id.integer' => 'ID格式不正确',
    'name.required' => '名称不能为空',
    'province.required' => '省不能为空',
    'city.required' => '市不能为空',
    'district.required' => '区不能为空',
    'address.required' => '详细地址不能为空',
    'way.required' => '服务类型不能为空',
    'way.in' => '服务类型不能为空',

  ];


  //自定义场景
  protected $scene = [
    'detail' => ['id'], //详情
    'add' => ['name','province','city','district','address','way'], //添加
    'change' => ['id','name','province','city','district','address','way'], //修改
    'del' => ['id'], //删除


  ];

}