<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class NavigationAreaValidate extends BaseValidate
{
    protected $rule = [
        'id' => 'bail|required|integer',
        'ids' => 'bail|required',
        'name' => 'required',
        'is_start' => 'required',
        'build_id' => 'required|integer',
        'is_play' => 'required|in:1,2',
        'content' => 'bail|required',
        'rotation_angle' => 'bail|required|max:360|min:0',
    ];


    # 错误提示语
    protected $message = [
        'id.required' => 'ID不能为空',
        'id.integer' => 'ID格式不能为空',
        'ids.required' => 'ID不能为空',
        "name.required" => "名称不能为空",
        "build_id.required" => "建筑ID不能为空",
        "is_start.required" => "是否起点区域不能为空",
        "is_play.required" => "是否使用不能为空",
        "is_play.in" => "是否使用规则不正确",
        'content.required' => '内容不能为空',
        'rotation_angle.required' => '指北针偏移角度不能为空',
        'rotation_angle.max' => '指北针偏移角度最大360°',
        'rotation_angle.min' => '指北针偏移角度最小0°',
    ];

    protected $scene = [
        'add' => ['name', 'build_id', 'is_start','rotation_angle'], //新增验证
        'change' => ['id', 'name', 'build_id', 'is_start','rotation_angle'], //更新验证
        'del' => ['id'], //删除
        'detail' => ['id'], //详情
        'list' => ['build_id'], //区域列表
        'play_and_cancel' => ['ids', 'is_play'], //是否禁用
        'sort_change' => ['content'], //banner 图排序
    ];
}
