<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class AnswerActivityAnswerValidate extends  BaseValidate
{
    protected  $rule = [
        'id' => 'bail|required|integer',
        'act_id' => 'bail|required|integer',
        'answer' => 'bail|required',

        'timestamp' => 'bail|required|size:13',
        'sign' => 'bail|required|size:32',
        'token' => 'bail|required|size:32',

        'lon' => 'bail|required',
        'lat' => 'bail|required',
    ];

    # 错误提示语
    protected $message = [
        'id.required' => 'ID不能为空',
        'id.integer' => 'ID格式不正确',
        'act_id.required' => '活动ID不能为空',
        'act_id.integer' => '活动ID格式不正确',
        'answer.required' => '答案不能为空',

        'timestamp.required' => '时间不能为空',
        'timestamp.size' => '时间格式不正确',
        'sign.required' => '签名不能为空',
        'sign.size' => '签名格式不正确',
        'token.required' => '用户ID不能为空',
        'token.size' => '用户ID格式不正确',

        'lon.required' => '经纬度不能为空',
        'lat.required' => '经纬度不能为空',

    ];


    protected  $scene = [
        'get_answer_problem' => ['act_id', 'timestamp', 'sign', 'token', 'lon', 'lat'], //开始答题，获取题目

        'reply_problem' => ['act_id', 'answer', 'timestamp', 'sign', 'token'], //用户答题 


    ];
}
