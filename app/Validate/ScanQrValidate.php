<?php

namespace App\Validate;

use App\Validate\BaseValidate;


/**
 * 扫码签到
 */
class ScanQrValidate extends  BaseValidate
{
  //验证规则
  protected $rule = [
    'qr_code' => 'bail|required|min:10',


  ];
  //自定义验证信息
  protected $message = [
    'qr_code.required' => '扫码信息不能为空',
    'qr_code.min' => '请扫码正确二维码', //新版二维码超过10位了


  ];


  //自定义场景
  protected $scene = [
    'scan_qr' => ['qr_code'], //扫码
  ];
}
