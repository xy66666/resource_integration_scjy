<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class VolunteerServiceTimeValidate extends BaseValidate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id' => 'bail|required|integer',
        'times' => 'required',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.required' => 'ID不能为空',
        'id.integer' => 'ID格式不正确',
        'times.required' => '服务时间不能为空',
    ];

    protected  $scene = [
        'add' => ['times'], //新增验证
        'change' => ['id', 'times'], //更新验证
        'detail' => ['id'], //详情
        'del' => ['id'] //删除验证
    ];
}
