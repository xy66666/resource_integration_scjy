<?php

namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * 邮递地址设置
 * Class SystemSetValidate
 * @package app\api\validate
 */
class BookHomeReturnAddressValidate extends  BaseValidate
{
    protected $rule = [
        "id" => "required|integer",
        "way" => "required|integer|in:1,2",
        "type" => "required|integer|in:1,2",

        "content" => "required",

        "username" => "required",
        "tel" => "required|check_tel_and_phone",
        "province" => "required",
        "city" => "required",
        "district" => "required",
        "address" => "required",
    ];

    //自定义验证信息
    protected $message = [
        'id.required' => 'ID不能为空',
        'id.integer' => 'ID不能为空',

        'way.required' => '方式不能为空',
        'way.integer' => '方式格式不正确',
        'way.in' => '方式格式不正确',
        'type.required' => '类型不能为空',
        'type.integer' => '类型格式不正确',
        'type.in' => '类型格式不正确',

        'content.required' => '内容不能为空',

        'username.required' => '用户名不能为空',
        'tel.required' => '电话号码不能为空',
        'province.required' => '省不能为空',
        'city.required' => '市不能为空',
        'district.required' => '区不能为空',
        'address.required' => '地址不能为空',

    ];

    protected $scene = [
        // 详情
        "detail" => ["id"],
        // 添加
        "add" => ['way', 'type', "username", 'tel', 'province', 'city', 'district', 'address'],
        // 修改
        "change" => ['id', 'way', 'type', "username", 'tel', 'province', 'city', 'district', 'address'],
        // 删除
        "del" => ["id"],
        //获取图书到家归还地址说明
        'get_address_explain' => ['way'],
        //设置图书到家归还地址说明
        'set_address_explain' => ['way', 'content']
    ];
}
