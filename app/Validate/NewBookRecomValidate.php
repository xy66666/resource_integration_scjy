<?php
namespace App\Validate;

use App\Validate\BaseValidate;

class NewBookRecomValidate extends  BaseValidate
{
    protected  $rule =[
        'id' => 'required|integer',
        'shop_id' => 'required|integer',
        'ids' => 'required',
        'book_id' => 'required|integer',
        'book_name' => 'required',
        'author' => 'required',
        'price' => 'required',
        'type_id' => 'required|integer',
        'isbn' => 'required',
        'is_recom' => 'required|integer|in:1,2',
        'is_plane'=> 'required|integer|in:1,2',//类型
    ];

    protected $message = [
        'id.required' => '书籍id不能为空',
        'shop_id.integer' => '书店id格式不正确',
        'ids.required' => '书籍id不能为空',
        'book_id.required' => '书籍id不能为空',
        'book_id.integer' => '书籍id格式不正确',
        'type_id.required' => '书籍类型id不能为空',
        'type_id.integer' => '书籍类型id格式不正确',

        "is_plane.required" => "状态不能为空",
        "is_plane.integer" => "状态规则不正确",
        "is_plane.in" => "状态规则不正确",

        'book_name.required'=>'书名不能为空',
        'isbn.required'=>'ISBN号不能为空',
        'author.required'=>'作者不能为空',
        'price.required'=>'价格不能为空',
        'is_recom.required' => '是否推荐类型不能为空',
        'is_recom.integer' => '是否推荐类型格式不正确',
        'is_recom.integer' => '是否推荐类型格式不正确',
    ];

    protected  $scene = [
        'detail' => ['id'], //详情验证
        'add' => ['shop_id','book_name','isbn','author','price','type_id'], //新增验证
        'change' => ['id','shop_id','book_name','isbn','author','price','type_id'], //更新验证
        'del' => ['id'], //删除

        'cancel_and_release'   => ['ids' , 'is_recom'],//推荐与取消推荐
        'up_and_down'   => ['ids' , 'is_plane'],//上架和下架
    ];

}