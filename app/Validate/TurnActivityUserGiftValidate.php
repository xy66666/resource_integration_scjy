<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class TurnActivityUserGiftValidate extends  BaseValidate
{
    protected  $rule = [
        'id' => 'bail|required|integer',
        'act_id' => 'bail|required|integer',
        'ids' => 'bail|required',
        'state' => 'bail|required|in:4,7',
    ];


    # 错误提示语
    protected $message = [
        'id.required' => 'ID不能为空',
        'id.integer' => 'ID格式不正确',
        "act_id.required" => "活动ID不能为空",
        "act_id.integer" => "活动ID参数错误",


        'ids.required' => 'ID格式不正确',
        "state.required" => "状态不能为空",
        "state.in" => "状态参数错误",

    ];

    protected  $scene = [
        'list' => ['act_id'], // 获取用户礼物列表

        'user_gift_grant' => ['ids', 'state'], // 奖品领取或发奖

        'user_packet_transfer' => ['id'], // 红包转账失败，进行重新转账
    ];
}
