<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class CodeGuideExhibitionValidate extends  BaseValidate
{
    protected  $rule = [
        'id' => 'bail|required|integer',
        'name' => 'required',
        'is_play' => 'required|integer|in:1,2',
        'start_time' => 'required|date',
        'end_time' => 'required|date|after:start_time',
    ];


    # 错误提示语
    protected $message = [
        'id.required' => 'ID不能为空',
        'id.integer' => 'ID格式不正确',
        "name.required" => "展览名称不能为空",
        "is_play.required" => "是否发布不能为空",
        "is_play.integer" => "是否发布参数错误",
        "is_play.in" => "是否发布参数错误",
        "start_time.required" => "展览开始时间不能为空",
        "start_time.date" => "展览开始时间参数错误",
        "end_time.required" => "展览结束时间不能为空",
        "end_time.date" => "展览结束时间参数错误",
        "end_time.after" => "展览结束时间不能小于开始时间",
    ];

    protected  $scene = [
        'add' => ['name', 'start_time', 'end_time'], //新增验证
        'change' => ['id', 'name', 'start_time', 'end_time'], //更新验证


        'cancel_and_release' => ['id' , 'is_play'], //推荐为热门展览 或 取消热门推荐

        'del' => ['id'], //展览删除
        'detail' => ['id'], //展览详情

    ];
}
