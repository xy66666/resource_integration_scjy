<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class NavigationPointValidate extends  BaseValidate
{
    protected  $rule = [
        'id' => 'bail|required|integer',
        'ids' => 'bail|required',
        'name' => 'required',
        'area_id' => 'required',
        'type' => 'required|in:1,2,3,4',
        'point_id' => 'required',
        'is_play' => 'required|in:1,2',
        'content' => 'bail|required',
    ];


    # 错误提示语
    protected $message = [
        'id.required' => 'ID不能为空',
        'id.integer' => 'ID格式不能为空',
        'ids.required' => 'ID不能为空',
        "name.required" => "名称不能为空",
        "type.required" => "点位类型不能为空",
        "type.in" => "点位类型规则不正确",
        "area_id.required" => "请选择区域ID",
        "point_id.required" => "点位ID",
        "is_play.required" => "是否使用不能为空",
        "is_play.in" => "是否使用规则不正确",
        'content.required' => '内容不能为空',
    ];
    protected  $scene = [
        'add' => ['name', 'area_id', 'type'], //新增验证
        'change' => ['id', 'name', 'area_id', 'type'], //更新验证
        'del' => ['id'], //删除
        'detail' => ['id'], //详情

        'play_and_cancel' => ['ids', 'is_play'], //是否禁用
        'sort_change' => ['content'], //banner 图排序
    ];
}
