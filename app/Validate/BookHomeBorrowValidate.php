<?php

namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * 新书荐购表
 * Class NewBookValidate
 * @package app\api\validate
 */
class BookHomeBorrowValidate extends BaseValidate
{
    protected  $rule =[
        'book_id'=> 'required|integer',//新书id
        'type'=> 'required|integer|in:1,2,3,4,5',//类型
        'order_id'=> 'required|integer',//订单id
        'address_id'=> 'required|integer',//收货地址id
        'book_ids'=> 'required',//新书id
        'barcodes'=> 'required',//条形码
        'courier_id'=> 'required|integer',//快递id
        'tracking_number'=> 'required|min:8|max:18',//采购id
        'cancel_remark'=> 'required',//取消备注
    ];

        
    # 错误提示语
    protected $message = [
        "book_id.required" => "书籍ID不能为空",
        "book_id.integer" => "书籍ID规则不正确",

        "type.required" => "类型不能为空",
        "type.integer" => "类型规则不正确",
        "type.in" => "类型规则不正确",
        "order_id.required" => "订单ID不能为空",
        "order_id.integer" => "订单ID规则不正确",
        "address_id.required" => "地址ID不能为空",
        "address_id.integer" => "地址ID规则不正确",
        "book_ids.required" => "书籍ID不能为空",
        "barcodes.required" => "条形码不能为空",
        "courier_id.required" => "快递公司名称ID不能为空",
        "courier_id.integer" => "快递公司名称ID规则不正确",

        "tracking_number.required" => "快递单号必须存在",
        "tracking_number.min" => "快递单号不能少于8位",
        "tracking_number.max" => "快递单号不能大于18位",

        "tracking_number.required" => "取消订单备注不能为空",
    ];

    protected  $scene =[
    //    'new_book_detail'   => ['book_id'],//新书详情
        'hot_search_list'   => ['type'],//检索热词列表
        'postage_list'   => ['type'],//图书馆设置的运费详情(书店及图书馆)
         'new_book_collect'   => ['book_id', 'type'],//新书收藏与取消收藏
        // 'new_book_schoolbag'   => ['book_id'],//新书加入书袋与移出书袋
        // 'get_order_info'   => ['book_ids' , 'address_id'],//获取预支付订单信息
        // 'my_order_detail'   => ['order_id'],//我的订单详情
        'recom_return'   => ['courier_id','barcodes','tracking_number'],//书籍归还
        'cancel_return'   => ['return_id'],//用户撤销线上书籍归还记录

        'new_borrow_list'   => ['type'],//新书详情
        
        'cancel_book_home_order'   => ['order_id','cancel_remark'],//取消图书到家订单
    ];



}