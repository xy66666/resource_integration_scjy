<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class CompetiteActivityGroupValidate extends  BaseValidate
{
    protected  $rule = [
        'id' => 'bail|required|integer',
        'con_id' => 'bail|required|integer',
        'account' => 'bail|required|alpha_num|min:2|self_alpha_num',
        'name' => 'bail|required|max:20',
        'password' => 'bail|required|min:6|max:20',
        'confirm_password' => 'bail|required|min:6|max:20|same:password',
        'img' => 'bail|required',
        'orders' => 'bail|required',
    ];

    # 错误提示语
    protected $message = [
        'id.required' => 'ID不能为空',
        'id.integer' => 'ID格式不正确',
        'con_id.required' => '大赛ID不能为空',
        'con_id.integer' => '大赛ID格式不正确',
        'nature.required' => '单位性质不能为空',
        'nature.integer' => '单位性质格式不正确',
        'name.required' => '名称不能为空',
        'name.max' => '名称最多20个字符',
        'img.required' => '图片不能为空',
        'account.required' => '账号不能为空',
        'account.min'    => '账号位数不合法',
        'account.alpha_num'    => '账号规则不合法',
        'account.self_alpha_num'    => '账号规则不合法',
        'password.required' => '密码不能为空',
        'password.min'    => '密码位数不正确',
        'password.max'    => '密码位数不正确',
        'confirm_password.required' => '确认密码不能为空',
        'confirm_password.min'    => '确认密码位数不正确',
        'confirm_password.max'    => '确认密码位数不正确',
        'confirm_password.same'    => '2次密码输入不一致',
    ];

    protected  $scene = [
        'filter_list' => ['con_id'], //获取筛选列表
        'list' => ['con_id'], //获取列表
        'add' => ['con_id', 'name', 'account', 'password', 'confirm_password'], //新增验证
        'change' => ['id', 'name'], //更新验证
        'detail' => ['id'], //详情
        'del' => ['id'], //删除验证
        'change_group_password' => ['password','change_group_password'], //修改团队账号密码
        'order' => ['orders'], //排序验证

        'wx_list' => ['con_id'], //前台获取大赛单位列表
        
        'web_login' => ['con_id','account', 'password','captcha','key_str'],//团队账号登录
    ];
}
