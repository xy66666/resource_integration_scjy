<?php

namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * 线上大赛活动
 */
class ContestActivityValidate extends  BaseValidate
{
  //验证规则
  protected $rule = [
    'id' => 'bail|required|integer',
    'ids' => 'bail|required',
    'node' => 'bail|required|integer|in:1,2',
    'con_id' => 'bail|required|integer',
    'unit_id' => 'bail|required|integer',
    'works_id' => 'bail|required|integer',
    'type_id' => 'bail|required|integer',
    'title' => 'bail|required',
    'host_handle' => 'bail|required',
    'con_start_time' => 'bail|required|date',
    'con_end_time' => 'bail|required|date|after:con_start_time',
    'vote_start_time' => 'bail|date|after_or_equal:con_start_time',
    'vote_end_time' => 'bail|date|after:vote_start_time',
    'tel' => 'bail|required|check_tel_and_phone:',
    'vote_way' => 'bail|required|in:1,2,3',
    'is_reader' => 'bail|required|in:1,2',
    'is_play' => 'bail|required|in:1,2',
    'number' => 'bail|required',

    // 'content' => 'bail|required',

    'cover' => 'bail|required',
    'width' => 'bail|required',
    'height' => 'bail|required',

    'originals' => 'bail|required',

    'status' => 'bail|required|in:1,2,4,5',


    'username' => 'required',
    'unit' => 'required',
    'is_original' => 'required|in:1,2',
    'adviser' => 'required',
    'adviser_tel' => 'required:check_tel_and_phone:',
    'id_card' => 'required|check_id_card:',

  ];


  //自定义验证信息
  protected $message = [
    'id.required' => 'ID不能为空',
    'id.integer' => 'ID格式不正确',
    'ids.required' => 'ID不能为空',
    'unit_id.required' => '单位ID不能为空',
    'unit_id.integer' => '单位ID格式不正确',
    'node.required' => '大赛举办方式不能为空',
    'node.integer' => '大赛举办方式格式不正确',
    'node.in' => '大赛举办方式格式不正确',
    'con_id.required' => '大赛ID不能为空',
    'con_id.integer' => '大赛ID格式不正确',
    'type_id.required' => '类型ID不能为空',
    'type_id.integer' => '类型ID格式不正确',
    'works_id.required' => '作品ID不能为空',
    'works_id.integer' => '作品ID格式不正确',
    'title.required' => '标题不能为空',
    'host_handle.required' => '主办单位不能为空',
    'con_start_time.required' => '投稿开始时间不能为空',
    'con_start_time.date' => '投稿开始时间格式不正确',
    'con_end_time.required' => '投稿结束时间不能为空',
    'con_end_time.date' => '投稿结束时间格式不正确',
    'con_end_time.after' => '投稿结束时间必须大于开始时间',

    //   'vote_start_time.required' => '投票开始时间不能为空',
    'vote_start_time.date' => '投票开始时间格式不正确',
    'vote_start_time.after_or_equal' => '投票开始时间必须大于等于投稿开始时间',
    //  'vote_end_time.required' => '投票结束时间不能为空',
    'vote_end_time.date' => '投票结束时间格式不正确',
    'vote_end_time.after' => '投票结束时间必须大于投票开始时间',


    'tel.required' => '联系电话不能为空',
    "tel.check_tel_and_phone" => "联系电话格式不正确",
    'vote_way.required' => '投票方式不能为空',
    'vote_way.in' => '投票方式格式不正确',
    'is_reader.required' => '是否需要绑定读者证不能为空',
    'is_reader.in' => '是否需要绑定读者证格式不正确',
    'is_play.required' => '状态不能为空',
    'is_play.in' => '状态格式不正确',
    'number.required' => '投票数量不能为空',

    // 'content.required' => '原创内容不能为空',

    'originals.required' => '原创申明不能为空',

    'username.required'     => '读者姓名不能为空',
    'unit.required'     => '单位/学校不能为空',
    'id_card.required'      => '身份证号码不能为空',
    'id_card.check_id_card' => '身份证号码格式不正确',

    'status.required'      => '状态不能为空',
    'status.in' => '状态格式不正确',

    'cover.required'     => '作品封面不能为空',
    'width.required'     => '宽度不能为空',
    'height.required'     => '高度不能为空',


    'unit.required' => '单位学校不能为空',
    'is_original.required' => '是否原创不能为空',
    'is_original.in' => '是否原创格式不正确',
    'adviser.required' => '指导教师不能为空',
    'adviser_tel.required' => '指导教师电话不能为空',
    'adviser_tel.check_tel_and_phone' => '指导教师电话格式不正确',

  ];

  //自定义场景
  protected $scene = [
    'detail' => ['id'], //详情
    'add' => ['node', 'title', 'host_handle', 'con_start_time', 'con_end_time'/* , 'vote_start_time', 'vote_end_time' */, 'tel', 'vote_way', 'is_reader'/* , 'number' */, 'originals'], //添加
    'change' => ['node', 'id', 'title', 'host_handle', 'con_start_time', 'con_end_time'/* , 'vote_start_time', 'vote_end_time' */, 'tel', 'vote_way', 'is_reader'/* , 'number' */, 'originals'], //修改
    'del' => ['id'], //删除

    'play_and_cancel' => ['ids', 'is_play'], //发布与取消发布

    'setOriginalDeclare' => ['lib_id', 'content'], //修改原创申明

    'production_list' => ['con_id'], //作品列表
    'production_list_download_img' => ['id'], //单个下载作品
    'production_list_download_img_all' => ['con_id'], //批量下载作品
    'production_detail' => ['id'], //作品详情


    'works_check' => ['id', 'status'], //审核作品
    'violate_and_cancel' => ['id'], //违规 与 取消违规操作

    'wx_filter_unit_list' => ['con_id'], //微信端获取单位列表
    'wx_detail' => ['id'], //微信端详情
    'wx_production_list' => ['con_id'], //作品列表
    'wx_production_detail' => ['id'], //作品详情

    'wx_type_list' => ['con_id'], //大赛类型列表

    'wx_works_user_info' => ['tel'], //电话号码获取用户信息
    'wx_works_vote' => ['works_id'], //作品投票
    'wx_works_cancel_del' => ['works_id', 'status'], //作品投票

    'wx_works_add' => ['con_id', 'title', 'type_id', 'cover', 'width', 'height',/*'unit', 'is_original', 'adviser','adviser_tel'*/], //作品上传
    'wx_works_change' => ['works_id', 'con_id', 'title', 'type_id', 'cover', 'width', 'height',/*'unit', 'is_original', 'adviser','adviser_tel'*/], //作品上传

  ];
}
