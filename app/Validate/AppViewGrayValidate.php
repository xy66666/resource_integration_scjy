<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class AppViewGrayValidate extends  BaseValidate
{
    protected  $rule = [
        'id' => 'bail|required|integer',
        'name' => 'required',
        'start_time' => 'required|date',
        'end_time' => 'required|date|after:start_time',
    ];


    # 错误提示语
    protected $message = [
        'id.required' => 'ID不能为空',
        'id.integer' => 'ID格式不正确',
        "name.required" => "名称不能为空",
        "start_time.required" => "开始时间不能为空",
        "start_time.date" => "开始时间参数错误",
        "end_time.required" => "结束时间不能为空",
        "end_time.date" => "结束时间参数错误",
        "end_time.after" => "结束时间不能小于开始时间",
    ];

    protected  $scene = [
        'add' => ['name', 'start_time', 'end_time'], //新增验证
        'change' => ['id', 'name', 'start_time', 'end_time'], //更新验证
        'del' => ['id'], //删除
        'detail' => ['id'], //详情
    ];
}
