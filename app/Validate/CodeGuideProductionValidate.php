<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class CodeGuideProductionValidate extends  BaseValidate
{
    protected  $rule = [
        'id' => 'bail|required|integer',
        'name' => 'required',
        'img' => 'required',
        'exhibition_id' => 'required|integer',
        'type_id' => 'required|integer',
        'exhibition_id' => 'required|integer',
        'qr_code' => 'required|size:10',
    ];


    # 错误提示语
    protected $message = [
        'id.required' => 'ID不能为空',
        'id.integer' => 'ID格式不正确',
        "name.required" => "作品名称不能为空",
        "img.required" => "作品不能为空",
        "type_id.required" => "类型ID不能为空",
        "type_id.integer" => "类型ID参数错误",
        "exhibition_id.required" => "展览ID不能为空",
        "exhibition_id.integer" => "展览ID参数错误",
        "qr_code.required" => "扫码信息不能为空",
        "qr_code.size" => "扫码信息参数错误",
    ];

    protected  $scene = [
        'add' => ['name', 'type_id', 'exhibition_id'], //新增验证
        'change' => ['id', 'name', 'type_id', 'exhibition_id'], //更新验证



        'wx_exhibition_detail' => ['exhibition_id'], //展览详情
        'wx_lists' => ['qr_code'], //作品列表

        'del' => ['id'], //作品删除
        'detail' => ['id'], //作品详情

    ];
}
