<?php

namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * 线上大赛活动指定打分人
 */
class CompetiteActivityWorksAppointScoreValidate extends  BaseValidate
{
  //验证规则
  protected $rule = [
    'con_id' => 'bail|required|integer',
    'score_manage_id' => 'bail|required|integer',
    'works_ids' => 'bail|required',


  ];


  //自定义验证信息
  protected $message = [
    'con_id.required' => 'ID不能为空',
    'con_id.integer' => 'ID格式不正确',
    'score_manage_id.required' => '打分人ID不能为空',
    'score_manage_id.integer' => '打分人ID格式不正确',
    'works_ids.required' => '作品ID不能为空',


  ];

  //自定义场景
  protected $scene = [
    'works_appoint_score' => ['con_id'], //列表
    'works_score_works' => ['con_id', 'score_manage_id'], //根据打分人获取所有的作品id，或名字  简单列表
    'works_score_works_simple' => ['con_id', 'score_manage_id'], //根据打分人获取所有的作品id，或名字 分页列表
    'works_score_works_modify' => ['con_id', 'score_manage_id', 'works_ids'], //添加、修改打分作品
    'works_score_works_del' => ['con_id', 'score_manage_id'], //删除打分作品

  ];
}
