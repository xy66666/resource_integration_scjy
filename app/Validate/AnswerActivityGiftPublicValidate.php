<?php
namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * 礼物公示表
 */
class AnswerActivityGiftPublicValidate extends  BaseValidate
{
    protected  $rule =[
        'id'=>'bail|required|integer',
        'act_id'=>'bail|required|integer',
        'name' => 'bail|required|max:30',
        'percent' => 'bail|required|integer|max:100',
        'total_number' => 'bail|required|integer',
        'type' => 'bail|required|in:1,2',
        'way' => 'bail|required|in:1,2',
    ];

    # 错误提示语
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        'act_id.required'=>'活动ID不能为空',
        'act_id.integer'=>'活动ID格式不正确',
        'percent.required'=>'概率不能为空',
        'percent.integer'=>'概率格式不正确',
        'percent.max'=>'概率最大不能超过100',
        'name.required'=>'名称不能为空',
        'name.size'=>'名称最多20个字符',
        'total_number.required'=>'数量不能为空',
        'total_number.integer'=>'数量格式不正确',
        'type.required'=>'礼物类型不能为空',
        'type.in'=>'礼物类型格式不正确',
        'way.required'=>'礼物投放方式不能为空',
        'way.in'=>'礼物投放方式格式不正确',
    ];

    protected  $scene = [
        'list' => ['act_id'], //获取列表

        'add' => ['act_id','name','type','way','number'], //新增验证
        'change' => ['id','name','type','way','number'], //更新验证
        'detail' => ['id'], //详情
        'del' => ['id'], //删除验证

        

        'wx_list' => ['act_id'], //前台获取活动单位列表
    ];



}