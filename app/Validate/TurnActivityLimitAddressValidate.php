<?php
namespace App\Validate;

use App\Validate\BaseValidate;


/**
 * 在线抽奖活动限制地址
 */
class TurnActivityLimitAddressValidate extends  BaseValidate
{
    protected  $rule =[
        'id'=>'bail|required|integer',
        'act_id'=>'bail|required|integer',
        'province'=>'bail|required',


    ];

    # 错误提示语
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        'act_id.required'=>'活动ID不能为空',
        'act_id.integer'=>'活动ID格式不正确',
        'province.required'=>'地址不能为空',
      
    ];


    protected  $scene = [
        'filter_list' => ['act_id'], //获取筛选列表
        'list' => ['act_id'], //获取列表
        'add' => ['act_id', 'province'], //新增验证
        'change' => ['id', 'province'], //更新验证
        'detail' => ['id'], //详情
        'del' => ['id'], //删除验证



    ];



}