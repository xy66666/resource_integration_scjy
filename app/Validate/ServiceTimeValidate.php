<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class ServiceTimeValidate extends  BaseValidate
{
     //验证规则
  protected $rule = [
    'id' => 'bail|required|integer',
    'times' => 'bail|required',

  ];
  //自定义验证信息
  protected $message = [
    'id.required' => 'ID不能为空',
    'id.integer' => 'ID格式不正确',
    'times.required' => '时间不能为空',


  ];


  //自定义场景
  protected $scene = [
    'detail' => ['id'], //详情
    'add' => ['times'], //添加
    'change' => ['id','times'], //修改
    'del' => ['id'], //删除


  ];

}