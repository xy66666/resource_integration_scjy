<?php

namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * 采购设置
 * Class SystemSetValidate
 * @package app\api\validate
 */
class BookHomePurchaseSetValidate extends  BaseValidate
{
    protected $rule = [
        "type" => "required|integer|in:1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19",
        "number" => "required",
        "content" => "required",
        "price" => "required",
        "shop_id" => "required|integer",
        "username" => "required",
        "tel" => "required|check_tel_and_phone",
        "province" => "required",
        "city" => "required",
        "district" => "required",
        "address" => "required",
    ];

        //自定义验证信息
    protected $message = [
        'type.required' => '类型不能为空',
        'type.integer' => '类型格式不正确',
        'type.in' => '类型格式不正确',
        
        'number.required' => '数量不能为空',
        'content.required' => '内容不能为空',
        'price.required' => '价格不能为空',
        'shop_id.required' => '书店ID不能为空',
        'shop_id.integer' => '书店ID不能为空',
        'username.required' => '用户名不能为空',
        'tel.required' => '电话号码不能为空',
        'tel.check_tel_and_phone' => '电话号码规则不正确',
        'province.required' => '省不能为空',
        'city.required' => '市不能为空',
        'district.required' => '区不能为空',
        'address.required' => '地址不能为空',

    ];

    protected $scene = [
        // 采购设置
        "purchase_set" => ["type"],
        // 邮费设置
        "postage_set" => ["content"],
        // 图书馆邮费地址设置
        "lib_deliver_address" => ["username",'tel','province','city','district','address'],
        // 书店邮费设置
        "shop_deliver_address" => ['shop_id',"username",'tel','province','city','district','address'],
    ];

}