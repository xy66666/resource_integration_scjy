<?php

namespace App\Validate;

use App\Validate\BaseValidate;


/**
 * 景点打卡作品
 */
class ScenicWorksValidate extends  BaseValidate
{
  //验证规则
  protected $rule = [
    'id' => 'bail|required|integer',
    'scenic_id' => 'bail|required|integer',
    'status' => 'bail|required|in:1,2,4,5',

    'title' => 'bail|required',
    'content' => 'bail|required',
    'lon' => 'bail|required',
    'lat' => 'bail|required',
  ];
  //自定义验证信息
  protected $message = [
    'id.required' => 'ID不能为空',
    'id.integer' => 'ID格式不正确',
    'scenic_id.required' => '景点ID不能为空',
    'scenic_id.integer' => '景点ID格式不正确',
    'status.required' => '状态不能为空',
    'status.in' => '状态规则不正确',
    'title.required' => '标题不能为空',
    'content.required' => '内容不能为空',

    'lon.required' => '位置信息不能不能为空',
    'lat.required' => '内容不能为空',

  ];


  //自定义场景
  protected $scene = [
    'detail' => ['id'], //详情
    'works_check' => ['id', 'status'], //状态
    'violate_and_cancel' => ['id'], //违规 与 取消违规操作

    'wx_add' => ['scenic_id', 'title', 'content','lon','lat'], //前端景点打卡
    'wx_change' => ['id', 'scenic_id', 'title', 'content','lon','lat'], //前端景点修改
    'wx_works_vote' => ['id'], //前端作品点赞与取消点赞

    'wx_works_cancel_del' => ['id', 'status'], //前端撤销、删除作品


  ];
}
