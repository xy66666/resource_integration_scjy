<?php

namespace App\Validate;

use App\Validate\BaseValidate;


/**
 * 阅读任务指定用户验证类
 */
class ReadingTaskAppointValidate extends  BaseValidate
{
    protected  $rule = [
        'id' => 'bail|required|integer',
        'task_id' => 'required|integer',
        'user_id' => 'required|integer',
    ];



    # 错误提示语
    protected $message = [
        'id.required' => 'ID不能为空',
        'id.integer' => 'ID格式不正确',
        "user_id.required" => "用户ID不能为空",
        "user_id.integer" => "用户ID参数错误",
        "task_id.required" => "阅读任务ID不能为空",
        "task_id.integer" => "阅读任务ID参数错误",
    ];

    protected  $scene = [
        'list' => ['task_id'], //阅读任务id
        'add' => ['task_id','user_id'], //新增验证
        'del' => ['id'], //删除



    ];
}
