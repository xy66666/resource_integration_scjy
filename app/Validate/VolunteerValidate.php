<?php

namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * 志愿者服务验证
 */
class VolunteerValidate extends  BaseValidate
{
     //验证规则
  protected $rule = [
    'id' => 'bail|required|integer',
    'ids' => 'bail|required',
    'name' => 'bail|required',
   // 'position_id' => 'bail|required|integer',
    'is_play' => 'bail|required|in:1,2',
    'status' => 'bail|required|in:1,3',
    'username' => 'bail|required|min:2',
    'tel' => 'bail|required|check_tel',
    'sex' => 'bail|required|in:1,2',
    'birth' => 'bail|required|date',
    'address' => 'bail|required',
    'school' => 'bail|required',
    'education' => 'bail|required',
    'specialty' => 'bail|required',
    'intention_id' => 'bail|required',
    'times_id' => 'bail|required',
    'expire_time' => 'required|date',
    'start_time' => 'required|date',
    'end_time' => 'required|date|after:start_time',
  ];
  //自定义验证信息
  protected $message = [
    'id.required' => 'ID不能为空',
    'id.integer' => 'ID格式不正确',
    'ids.required' => 'ID不能为空',
    'name.required' => '名称不能为空',
    'name.integer' => '名称格式不正确',
 //   'position_id.required' => '岗位ID不能为空',
 //   'position_id.integer' => '岗位ID格式不正确',
    'is_play.required' => '状态不能为空',
    'is_play.in' => '状态不正确',
    'status.required' => '状态不能为空',
    'status.in' => '状态不正确',
    'username.required' => '用户姓名不能为空',
    'username.min' => '用户姓名规则不正确',
    'tel.required' => '电话号码不能为空',
    'tel.check_tel' => '电话号码规则不正确',
    'sex.required' => '性别不能为空',
    'sex.in' => '性别规则不正确',
    'birth.required' => '出生日期不能为空',
    'birth.date' => '出生日期规则不正确',
    'address.required' => '地址不能为空',
    'school.required' => '毕业学校不能为空',
    'education.required' => '学历不能为空',
    'specialty.required' => '个人特长不能为空',
    'intention_id.required' => '服务意向不能为空',
    'times_id.required' => '服务时间不能为空',

    "expire_time.required" => "过期时间不能为空",
    "expire_time.date" => "过期时间格式错误",
    "start_time.required" => "开始时间不能为空",
    "start_time.date" => "开始时间格式错误",
    "end_time.required" => "结束时间不能为空",
    "end_time.date" => "结束时间格式错误",
    "end_time.after" => "结束时间必须在开始时间之后",

  ];


  //自定义场景
  protected $scene = [
    'detail' => ['id'], //详情
    'check' => ['id','status'], //审核
    'add' => ['name','expire_time'], //服务岗位添加
    'change' => ['id','name','expire_time'], //服务岗位修改
    'del' => ['id'], //服务岗位删除
    'play_and_cancel' => ['ids' ,'is_play'], //发布与取消发布

   // 'web_change' => ['username','tel','sex','birth','address','school','education','specialty','intention_id','times_id'], //添加

    'volunteer_apply_detail' => ['id'], //详情
    
    "service_time_list" => ["user_id"], //服务时长列表
    "service_time_change" => ["user_id"], //服务时长修改

    'wx_volunteer_apply' => ['intention_id','times_id'], //志愿者报名

  ];

}