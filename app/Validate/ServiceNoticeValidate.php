<?php

namespace App\Validate;

use App\Validate\BaseValidate;


class ServiceNoticeValidate extends  BaseValidate
{
    //验证规则
    protected $rule = [
        'id' => 'bail|required|integer',
        'node' => 'bail|required|in:1,2,3',
        'content' => 'bail|required|max:100',
        'type_name' => 'bail|required|max:50',
        'remark' => 'bail|max:200', //内置验证时，如果未写 required，则表示，不验证此参数必要性，但是数据中不能有这个值，有值即使为空，也是要验证的，所以不验证此参数时，传入的值，需要排除掉此参数
        'start_time' => 'bail|required|date',
        'end_time' => 'bail|date',
        'link' => 'bail|url',

    ];
    //自定义验证信息
    protected $message = [
        'id.required' => 'ID不能为空',
        'id.integer' => 'ID格式不正确',
        'node.required' => '类型不能为空',
        'node.in' => '类型格式不正确',
        'content.required' => '内容不能为空',
        'content.max' => '内容最多100个字符',
        'type_name.required' => '服务类型不能为空',
        'type_name.max' => '服务类型最多50个字符',
        'remark.max' => '备注最多50个字符',
        'start_time.required' => '开始时间不能为空',
        'start_time.date' => '开始时间格式不正确',
        'end_time.date' => '结束时间格式不正确',
        'link.url' => '链接格式不正确',

    ];


    protected  $scene = [
        'add' => ['node', 'content', 'type_name', 'start_time', 'end_time', 'link'], //新增验证
        'change' => ['id', 'node', 'content', 'type_name', 'start_time', 'end_time', 'link'], //新增验证
        'detail' => ['id'], //详情
        'send' => ['id'], //发送验证
        'del' => ['id'], //删除验证
    ];
}
