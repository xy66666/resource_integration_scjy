<?php

namespace App\Validate;

use App\Validate\BaseValidate;
/**
 * 用户验证器
 */
class WechatAuthValidate extends BaseValidate {
    //验证规则
    protected $rule =[
        'user_guid'=>'bail|required|size:32',
        'code'=>'bail|required',
        'nickname' => 'required',
        'head_img' => 'required',

    ];
    //自定义验证信息
    protected $message = [
        'user_guid.required'=>'ID不能为空',
        'user_guid.size'    =>'ID不合法',

        'code.required'  => '微信授权code不能为空',
        'nickname.required' => '昵称不能为空',
        "head_img.required" => "头像不能为空",
  
    ];

    //自定义场景
    protected $scene = [
          'wx_auth' => ['code'],//微信授权
          'wx_applet_auth' => ['code'],//微信小程序授权


      ];




}