<?php
namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * 在线抽奖礼物发放表
 */
class TurnActivityGiftValidate extends  BaseValidate
{
    protected  $rule =[
        'id'=>'bail|required|integer',
        'act_id'=>'bail|required|integer',
        'name' => 'bail|required|max:30',
        'percent' => 'bail|required|numeric|max:100|min:0.01',
        'total_number' => 'bail|required|integer',
        'type' => 'bail|required|in:1,2',
    ];

    # 错误提示语
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        'act_id.required'=>'活动ID不能为空',
        'act_id.integer'=>'活动ID格式不正确',
        'percent.required'=>'概率不能为空',
        'percent.numeric'=>'概率格式不正确',
        'percent.max'=>'概率最大不能超过100',
        'percent.min'=>'概率最小不能小于0.01',
        'name.required'=>'名称不能为空',
        'name.size'=>'名称最多20个字符',
        'total_number.required'=>'数量不能为空',
        'total_number.integer'=>'数量格式不正确',
        'type.required'=>'礼物类型不能为空',
        'type.in'=>'礼物类型格式不正确',
    ];

    protected  $scene = [
        'list' => ['act_id'], //获取列表

        'add' => ['act_id','percent','name','total_number','type'], //新增验证
        'change' => ['id','percent','name','total_number','type'], //更新验证
        'detail' => ['id'], //详情
        'del' => ['id'], //删除验证
        'order' => ['orders'], //排序验证

        'wx_list' => ['act_id'], //前台获取活动单位列表
    ];



}