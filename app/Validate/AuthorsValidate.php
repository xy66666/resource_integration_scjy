<?php
namespace App\Validate;

use App\Validate\BaseValidate;
/**
 * 作者验证器
 */
class AuthorsValidate extends BaseValidate {
    //验证规则
    protected $rule =[
        'id'=>'bail|required|integer',
        'username'=>'bail|required',
        'sex'=>'bail|required|integer|in:1,2',
    ];
    //自定义验证信息
    protected $message = [
        'id.required'=>'作者ID不能为空',
        'id.integer'=>'作者ID格式不正确',
        'username.required'=>'作者名称不能为空',
        'sex.required'=>'性别不能为空',
        'sex.integer'=>'性别不符合规范',
        'sex.in'=>'性别不符合规范',
    ];

    //自定义场景
    protected $scene = [
        'author_add' => ['username','sex'],//作者添加
        'author_change' => ['id','username','sex'],//作者修改
        'author_del' => ['id'],//作者删除
        'author_info' => ['id'],//作者删除
    ];




}