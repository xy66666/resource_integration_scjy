<?php
namespace App\Validate;

use App\Validate\BaseValidate;
/**
 * 文旅地图
 */
class BranchMapValidate extends BaseValidate {
    //验证规则
    protected $rule =[
        'id'=>'bail|required|integer',
        'name'=>'bail|required',
        'img'=>'bail|required',
        'dispark_time'=>'bail|required',
        'province'=>'bail|required',
        'city'=>'bail|required',
        'district'=>'bail|required',
        'address'=>'bail|required',
        'tel'=>'bail|required|check_tel_and_phone:',
       // 'contacts'=>'bail|required',
     //   'intro'=>'bail|required',
        'type'=>'bail|required|in:1,2',

    ];
    //自定义验证信息
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        'name.required'=>'名称不能为空',
        'dispark_time.required'=>'开放时间不能为空',
        'img.required'=>'封面图片不能为空',
        'province.required'=>'省不能为空',
        'city.required'=>'市不能为空',
        'district.required'=>'区不能为空',
        'address.required'=>'详细地址不能为空',
        'tel.required'=>'联系电话不能为空',
        'tel.check_tel_and_phone'=>'联系电话规则不正确',
        //'contacts.required'=>'联系人不能为空',
       // 'intro.required'=>'简介不能为空',
        'type.required'=>'类型不能为空',
        'type.in'=>'类型规则不正确',

    ];

    //自定义场景
    protected $scene = [
        'add' => ['name','dispark_time','img','province','city','district','address','tel','type'],//添加
        'change' => ['id','name','dispark_time','img','province','city','district','address','tel','type'],//修改
        'detail' => ['id'],//详情
        'del' => ['id'],//删除
    ];




}