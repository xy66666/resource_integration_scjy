<?php
namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * 获取题目
 */
class AnswerActivityProblemValidate extends  BaseValidate
{
    protected  $rule =[
        'id'=>'bail|required|integer',
        'act_id'=>'bail|required|integer',
        'type'=>'bail|required|in:1,2',
        'state'=>'bail|required|in:1,2',
        'is_show_analysis'=>'bail|required|in:1,2',
        'title' => 'bail|required|max:150',
        'answer' => 'bail|required',
        'ids' => 'bail|required',
    ];

    # 错误提示语
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        'act_id.required'=>'活动ID不能为空',
        'act_id.integer'=>'活动ID格式不正确',
        'title.required'=>'名称不能为空',
        'title.size'=>'名称最多150个字符',
        'type.required'=>'类型规则不能为空',
        'type.size'=>'类型规则不正确',
        'state.required'=>'状态规则不能为空',
        'state.size'=>'状态规则不正确',
        'is_show_analysis.required'=>'是否显示解析规则不能为空',
        'is_show_analysis.size'=>'是否显示解析规则不正确',
        'answer.required'=>'答案内容不能为空',
        'ids.required'=>'ID不能为空',
    ];


    protected  $scene = [
        'list' => ['act_id'], //获取列表
        'add' => ['act_id','title','type','answer'], //新增验证
        'change' => ['id','name','type','answer'], //更新验证
        'detail' => ['id'], //详情
        'del' => ['id'], //删除验证
        'del_many' => ['act_id','ids'], //删除多个
        'disabling_and_enabling' => ['act_id','state'],//禁用与启用
        'analysis_show_and_hidden' => ['act_id','is_show_analysis'] //是否显示解析
    ];



}