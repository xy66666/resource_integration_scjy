<?php
namespace App\Validate;

use App\Validate\BaseValidate;
/**
 * 码上借
 */
class CodePlaceValidate extends BaseValidate {
    //验证规则
    protected $rule =[
        'id'=>'bail|required|integer',
        'ids'=>'bail|required',
        'name'=>'bail|required',
        'way'=>'bail|required|in:1,2',
        'lon'=>'bail|required',
        'lat'=>'bail|required',

    ];
    //自定义验证信息
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        'ids.required'=>'ID不能为空',

        'name.required'=>'名称不能为空',
        'lon.required'=>'经度不能为空',
        'lat.required'=>'纬度不能为空',

        'way.required'=>'定位方式不能为空',
        'way.in'=>'定位方式规则不正确',


    ];

    //自定义场景
    protected $scene = [
        'add' => ['name', 'way'], //添加
        'change' => ['id', 'name', 'way'], //修改
        'detail' => ['id'], //详情
        'del' => ['id'], //删除
        'play_and_cancel' => ['ids', 'is_play'], //撤销 和发布
    ];




}