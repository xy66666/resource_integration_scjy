<?php
namespace App\Validate;

use App\Validate\BaseValidate;
/**
 * 分馆管理员信息验证器
 */
class BranchAccountValidate extends BaseValidate {


    //验证规则
    protected $rule =[
        'id'=>'bail|required|integer',
        'branch_id'=>'bail|required|integer',
        'account'=>'bail|required|alpha_num|min:2|self_alpha_num',//验证字段必须是字母或数字。
        'username'=>'bail|required|min:2',
        'password'=>'bail|required|min:6|max:20',
        'confirm_password'=>'bail|required|min:6|max:20|same:password',
    ];
    //自定义验证信息
    protected $message = [
        'id.required' => '编号不能为空',
        'id.integer' => '编号格式不正确',
        'branch_id.required' => '分馆ID不能为空',
        'branch_id.integer' => '分馆ID格式不正确',
        'account.required'=>'账号不能为空',
        'account.min'    =>'账号位数不合法',
        'account.alpha_num'    =>'账号规则不合法',
        'account.self_alpha_num'    =>'账号规则不合法',
        'username.required'    =>'姓名不能为空',
        'username.min'    =>'姓名位数不合法',
        'password.required'=>'密码不能为空',
        'password.min'    =>'密码位数不正确',
        'password.max'    =>'密码位数不正确',
        'confirm_password.required'=>'确认密码不能为空',
        'confirm_password.min'    =>'确认密码位数不正确',
        'confirm_password.max'    =>'确认密码位数不正确',
        'confirm_password.same'    =>'2次密码输入不一致',
    ];

    //自定义场景
    protected $scene = [
        'lists' => ['branch_id'],//详情
        'detail' => ['id'],//详情
        'add' => ['branch_id','account', 'username','password','confirm_password'],//添加
        'change' => ['id','username'],//修改
        'del' => ['id'],//删除
        'change_pwd' => ['id','password','confirm_password'],//修改其他管理员密码
    ];




}