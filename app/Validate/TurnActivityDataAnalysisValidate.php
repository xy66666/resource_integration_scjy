<?php
namespace App\Validate;

use App\Validate\BaseValidate;


/**
 * 在线抽奖活动数据统计
 */
class TurnActivityDataAnalysisValidate extends  BaseValidate
{
    protected  $rule =[
        'id'=>'bail|required|integer',
        'act_id'=>'bail|required|integer',
    ];

    # 错误提示语
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        'act_id.required'=>'活动ID不能为空',
        'act_id.integer'=>'活动ID格式不正确',
    ];


    protected  $scene = [
    
        'statistics' => ['act_id'], //在线抽奖活动数据统计


    ];



}