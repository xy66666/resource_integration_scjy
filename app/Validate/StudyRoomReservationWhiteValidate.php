<?php

namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * 预约验证
 */
class StudyRoomReservationWhiteValidate extends  BaseValidate
{
    protected  $rule = [
        'id' => 'bail|required|integer',
        'username' => 'bail|required',
        'account' => 'bail|required',
        'tel' => 'required|check_tel_and_phone:',
    ];


    # 错误提示语
    protected $message = [
        "id.required" => "ID不能为空",
        "id.integer" => "ID规则不正确",
        "username.required" => "名字不能为空",
        "account.required" => "账号不能为空",
        "tel.required" => "联系方式不能为空",
        "tel.check_tel_and_phone" => "联系方式格式不正确",
    ];

    protected  $scene = [
        'add' => ['username', 'account'], //新增验证
        'change' => ['username', 'account'], //更新验证
        'detail' => ['id'],
        'del' => ['id'],
        'get_white_qr' => ['id'],



    ];



}
