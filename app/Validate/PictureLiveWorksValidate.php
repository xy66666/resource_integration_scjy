<?php

namespace App\Validate;

use App\Validate\BaseValidate;


/**
 * 图片直播图片上传验证类
 */
class PictureLiveWorksValidate extends  BaseValidate
{
    protected  $rule = [
        'id' => 'bail|required|integer',
        'title' => 'required',
        'act_id' => 'required|integer',
        'user_id' => 'required|integer',
        'works_id' => 'required|integer',
        'type_id' => 'required|integer',
        'picture_live' => 'required|file',
        'status' => 'bail|required|in:1,2,3,4,5,6',
        'content' => 'bail|required',

    ];



    # 错误提示语
    protected $message = [
        'id.required' => 'ID不能为空',
        'id.integer' => 'ID格式不正确',
        "title.required" => "活动名称不能为空",
        "type_id.required" => "活动类型ID不能为空",
        "type_id.integer" => "活动类型ID参数错误",
        "act_id.required" => "活动ID不能为空",
        "act_id.integer" => "活动ID参数错误",
        "user_id.required" => "用户ID不能为空",
        "user_id.integer" => "用户ID参数错误",
        "works_id.required" => "作品ID不能为空",
        "works_id.integer" => "作品ID参数错误",
        "picture_live.required" => "文件不能为空",
        "picture_live.file" => "文件类型错误",
        'status.required'      => '状态不能为空',
        'status.in' => '状态格式不正确',
        'content.required'      => '内容不能为空',
    ];

    protected  $scene = [
        'production_list' => ['act_id'], //列表验证
        'production_list_download_img' => ['act_id'], //列表验证导出图片
        'production_detail' => ['id'], //详情验证

        'works_check' => ['id', 'status'], //详情验证
        'works_add' => ['act_id', 'picture_live'], //详情验证
        'works_batch_add' => ['act_id', 'content'], //详情验证
        'works_change' => ['user_id', 'works_id'], //详情验证
        'works_del' => ['id'], //详情验证







        'wx_surplus_uploads_number' => ['id'], //获取剩余可上传图片张数




        'wx_list' => ['act_id'], //前台获取图片列表
        'wx_detail' => ['id'], //前台获取图片详情
        'wx_add' => ['act_id', 'picture_live'], //图片直播，上传图片

        'wx_works_vote' => ['works_id'], //作品投票
        'wx_works_cancel_del' => ['works_id', 'status'], //作品投票

        'wx_add_browse_number' => ['works_id'], //添加浏览量
    ];
}
