<?php
namespace App\Validate;

use App\Validate\BaseValidate;
/**
 * 管理员信息验证器
 */
class ManageInfoValidate extends BaseValidate {


    //验证规则
    protected $rule =[
        'id'=>'bail|required|integer',
        'account'=>'bail|required|alpha_num|min:2|self_alpha_num',//验证字段必须是字母或数字。
        'username'=>'bail|required|min:2',
        'password'=>'bail|required|min:6|max:20',
        'confirm_password'=>'bail|required|min:6|max:20|same:password',
        'key_str'=>'bail|required',
       // 'captcha'=>"bail|required|size:5",
       'captcha'=>"bail|required",
    ];
    //自定义验证信息
    protected $message = [
        'id.required' => '管理员ID不能为空',
        'id.integer' => '管理员ID格式不正确',
        'account.required'=>'账号不能为空',
        'account.min'    =>'账号位数不合法',
        'account.alpha_num'    =>'账号规则不合法',
        'account.self_alpha_num'    =>'账号规则不合法',
        'username.required'    =>'姓名不能为空',
        'username.min'    =>'姓名规则不合法',
        'password.required'=>'密码不能为空',
        'password.min'    =>'密码位数不正确',
        'password.max'    =>'密码位数不正确',
        'confirm_password.required'=>'确认密码不能为空',
        'confirm_password.min'    =>'确认密码位数不正确',
        'confirm_password.max'    =>'确认密码位数不正确',
        'confirm_password.same'    =>'2次密码输入不一致',
        'key_str.required'    =>'验证码key不能为空',
        'captcha.required'    =>'验证码不能为空',
       // 'captcha.size'    =>'验证码位数不正确',
    ];

    //自定义场景
    protected $scene = [
        'login' => ['account', 'password','captcha','key_str'],//后台管理员登录
        'add' => ['account', 'username','password','confirm_password'],//后台管理员添加
        'detail' => ['id'],//修改
        'change' => ['id','username'],//后台管理员修改
        'del' => ['id'],//后台管理员删除
        'manage_change_pwd' => ['id','password','confirm_password'],//后台管理员修改其他管理员密码
        'manage_change_self_pwd' => ['password','confirm_password'],//后台管理员删修改自己的密码
    ];




}