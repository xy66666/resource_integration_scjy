<?php

namespace App\Validate;

use App\Validate\BaseValidate;

/**
 * 预约验证
 */
class ReservationValidate extends  BaseValidate
{
    protected  $rule = [
        'id' => 'bail|required|integer',
        'ids' => 'bail|required',
        'reservation_id' => 'bail|required|integer',
        'seat_id' => 'bail|required|integer',
        'seat_ids' => 'bail|required',
        'schedule_id' => 'bail|required|integer',
        'make_id' => 'bail|required|integer',
        'make_time' => 'bail|required|date',
        'name' => 'required',
        'node' => 'required|integer|in:1,2,3,4,5,6',
        'status' => 'required|integer|in:1,4',
        'type_tag' => 'required',
        'number' => 'required|integer',
        'img' => 'required',
        'intro' => 'required',

        'is_play' => 'bail|required|in:1,2',
        'is_real' => 'required|integer|in:1,2',
        'is_sign' => 'bail|required|in:1,2',
        'is_reader' => 'bail|required|in:1,2',
        'is_unit' => 'bail|required|in:1,2',
        'is_id_card' => 'bail|required|in:1,2',
        'is_tel' => 'bail|required|in:1,2',
        'is_approval' => 'bail|required|in:1,2',
        'is_cancel' => 'bail|required|in:1,2',

        'make_status' => 'bail|required|in:1,2,3',

        'province' => 'bail|required',
        'city' => 'bail|required',
        'district' => 'bail|required',
        'address' => 'bail|required',
        'contacts' => 'required',
        'tel' => 'required|check_tel_and_phone:',

        'reader_id' => 'required',
    ];


    # 错误提示语
    protected $message = [
        "ids.required" => "ID不能为空",
        "id.required" => "ID不能为空",
        "id.integer" => "ID规则不正确",
        "reservation_id.required" => "ID不能为空",
        "reservation_id.integer" => "ID规则不正确",
        "seat_ids.required" => "ID不能为空",
        "seat_id.required" => "ID不能为空",
        "seat_id.integer" => "ID规则不正确",
        "schedule_id.required" => "排版ID不能为空",
        "schedule_id.integer" => "排版ID规则不正确",
        "make_id.required" => "预约ID不能为空",
        "make_id.integer" => "预约ID规则不正确",
        "name.required" => "名称不能为空",
        "type_tag.required" => "标签不能为空",
        "img.required" => "封面不能为空",
        "number.required" => "可预约数量不能为空",
        "number.integer" => "可预约数量格式错误",

        "intro.required" => "商品简介不能为空",

        'is_play.required' => '状态不能为空',
        'is_play.in' => '状态规则不正确',

        'status.required' => '状态不能为空',
        'status.in' => '状态规则不正确',

        "is_real.required" => "是否需要真实信息不能为空",
        "is_real.integer" => "是否需要真实信息参数错误",
        "is_real.in" => "是否需要真实信息参数错误",

        'is_sign.required' => '是否需要签到不能为空',
        'is_sign.in' => '是否需要签到规则不正确',
        'is_reader.required' => '是否需要绑定读者证不能为空',
        'is_reader.in' => '是否需要绑定读者证规则不正确',
        'is_unit.required' => '是否需要填写单位信息不能为空',
        'is_unit.in' => '是否需要填写单位信息规则不正确',
        'is_id_card.required' => '是否需要填写身份证号码不能为空',
        'is_id_card.in' => '是否需要填写身份证号码规则不正确',
        'is_tel.required' => '是否需要填写电话号码不能为空',
        'is_tel.in' => '是否需要填写电话号码规则不正确',
        'is_approval.required' => '预约是否需要审核不能为空',
        'is_approval.in' => '预约是否需要审核规则不正确',
        'is_cancel.required' => '预约是否可以取消不能为空',
        'is_cancel.in' => '预约是否可以取消规则不正确',
        'make_status.required' => '预约状态不能为空',
        'make_status.in' => '预约状态规则不正确',

        'make_time.required' => '预约时间不能为空',
        'make_time.date' => '预约时间格式不正确',

        'province.required' => '省不能为空',
        'city.required' => '市不能为空',
        'district.required' => '区不能为空',
        'address.required' => '详细地址不能为空',
        "contacts.required" => "联系人不能为空",
        "tel.required" => "联系方式不能为空",
        "tel.check_tel_and_phone" => "联系方式格式不正确",

        "reader_id.required" => "读者证号不能为空",
    ];

    protected  $scene = [
        'add' => ['name', 'type_tag', 'img', 'intro', 'number', 'contacts', 'tel', 'is_sign', 'is_reader', 'is_real', 'is_approval', 'is_cancel', 'province', 'city', 'district', 'address'], //新增验证
        'change' => ['id', 'name', 'type_tag', 'img', 'intro', 'number', 'contacts', 'tel', 'is_sign', 'is_reader', 'is_real', 'is_approval', 'is_cancel', 'province', 'city', 'district', 'address'], //更新验证
        'detail' => ['id'],
        'del' => ['id'],

        'seat_list' => ['reservation_id', 'schedule_id', 'make_time'], //座位列表
        'seat_add' => ['reservation_id', 'number'], //座位新增
        'download_qr' => ['reservation_id'], //下载座位二维码

        'get_make_quantum' => ['reservation_id'], //手动预约获取预约时间段
        'make' => ['reservation_id', 'schedule_id', 'make_time', 'reader_id'], //admin端预约
        'cancel_make' => ['make_id'], //取消预约


        'seat_play_and_cancel' => ['ids', 'is_play'], //座位发布与撤销
        'seat_del' => ['seat_ids'], //座位删除
        'play_and_cancel' => ['ids', 'is_play'], //发布与取消

        'wx_detail' => ['id'], //wx端详情
        'wx_make' => ['id', 'schedule_id', 'make_time'], //wx端预约


        'wx_cancel_make' => ['make_id'], //取消预约

        'wx_seat_list' => ['reservation_id', 'schedule_id', 'make_time'], //座位列表
        'get_seat_list_by_status' => ['reservation_id', 'make_status'], //获取空格子列表 (只能用于 node 为 7 的情况)

    ];
}
