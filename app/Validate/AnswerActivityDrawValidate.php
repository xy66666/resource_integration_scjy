<?php
namespace App\Validate;

use App\Validate\BaseValidate;


/**
 * 答题活动抽奖
 */
class AnswerActivityDrawValidate extends  BaseValidate
{
    protected  $rule =[
        'id'=>'bail|required|integer',
        'act_id'=>'bail|required|integer',
        'order_id'=>'bail|required|size:18',


        
        'timestamp'=>'bail|required|size:13',
        'sign'=>'bail|required|size:32',
        'token'=>'bail|required|size:32',
    ];

    # 错误提示语
    protected $message = [
        'id.required'=>'ID不能为空',
        'id.integer'=>'ID格式不正确',
        'act_id.required'=>'活动ID不能为空',
        'act_id.integer'=>'活动ID格式不正确',
        'order_id.required'=>'订单号不能为空',
        'order_id.size'=>'订单号格式不正确',


        'timestamp.required'=>'时间不能为空',
        'timestamp.size'=>'时间格式不正确',
        'sign.required'=>'签名不能为空',
        'sign.size'=>'签名格式不正确',
        'token.required'=>'用户ID不能为空',
        'token.size'=>'用户ID格式不正确',
    ];


    protected  $scene = [
        'draw' => ['act_id','timestamp','sign','token'], //开始抽奖

        'get_draw_result' => ['order_id','timestamp','sign','token'], //获取抽奖结果


    ];



}