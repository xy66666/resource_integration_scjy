<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class CompetiteActivityWorksTypeValidate extends  BaseValidate
{
     //验证规则
  protected $rule = [
    'id' => 'bail|required|integer',
    'con_id' => 'bail|required|integer',
    'type_name' => 'bail|required',
    'letter' => 'bail|required|size:1',
    'node' => 'bail|required',

  ];
  //自定义验证信息
  protected $message = [
    'id.required' => 'ID不能为空',
    'id.integer' => 'ID格式不正确',
    'con_id.required' => '大赛ID不能为空',
    'con_id.integer' => '大赛ID格式不正确',
    'type_name.required' => '名称不能为空',
    'letter.required' => '编号前缀不能为空',
    'letter.size' => '编号前缀规则不正确',
    'node.required' => '上传类型不能为空',
  ];


  //自定义场景
  protected $scene = [
    'filter_lists' => ['con_id'], //筛选数据列表
    'lists' => ['con_id'], //数据列表
    'detail' => ['id'], //详情
    'add' => ['con_id','type_name','letter','node'], //添加
    'change' => ['con_id','id','type_name','letter','node'], //修改
    'del' => ['id'], //删除


  ];


}