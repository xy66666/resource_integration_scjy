<?php

namespace App\Validate;

use App\Validate\BaseValidate;

class AppInviteCodeValidate extends  BaseValidate
{
    protected  $rule = [
        'id' => 'bail|required|integer',
        'name' => 'bail|required',
        'content' => 'bail|required|integer|digits:6',
    ];

    # 错误提示语
    protected $message = [
        'id.required' => 'ID不能为空',
        'id.integer' => 'ID格式不正确',
        'name.required' => '名称不能为空',
        'content.required' => '邀请码不能为空',
        'content.integer' => '邀请码格式不正确',
        "content.digits" => "邀请码格式不正确",
    ];


    protected  $scene = [
        'add' => ['name','content'], //新增验证
        'change' => ['id', 'name','content'], //更新验证
        'detail' => ['id'], //详情
        'del' => ['id'] //删除验证
    ];
}
