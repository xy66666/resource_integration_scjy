<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * 书店新书模块
 */
class ShopBook extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = null;

    protected $table = 'shop_book';

    /**
     * 获取书店名称  （关联）
     */
    public function getShopName()
    {
        return $this->belongsTo(Shop::class, 'shop_id', 'id');
    }

    /**
     * 获取类型名称  （关联）
     */
    public function getTypeName()
    {
        return $this->belongsTo(ShopBookType::class, 'type_id', 'id');
    }



    /**
     * 根据书籍id获取书店名称
     * @param book_id 书籍id
     */
    public static function getShopNameByBookid($book_id)
    {
        $shop_id = self::where('id', $book_id)->value('shop_id');
        return Shop::where('id', $shop_id)->value('name');
    }

    /**
     * 线下借书时新增书籍信息
     * @param $data 书籍信息 
     * @param $shop_id 书店id 
     */
    public function offlineBorrowInsertBook($data, $shop_id)
    {
        //查看书店是否有此书
        $shop_identifier = md5($data['book_name'] . $data['isbn'] . $data['price']);
        $is_exists = $this->where('shop_identifier', $shop_identifier)->where('shop_id', $shop_id)->first();
        if ($is_exists) {
            return $is_exists['id'];
        }
        $insert['shop_id'] = $shop_id;
        $insert['shop_identifier'] = $shop_identifier; //唯一标识
        $insert['book_name'] = $data['book_name'];
        $insert['author'] = $data['author'];
        $insert['isbn'] = $data['isbn'];
        $insert['press'] = $data['press'];
        $insert['pre_time'] = $data['pre_time'];
        $insert['price'] = $data['price'];
        $insert['book_num'] = $data['book_num'];
        $insert['number'] = 1;
        $insert['create_time'] = date('Y-m-d H:i:s');

        return $this->insertGetId($insert);
    }


    /**
     * 新书列表
     * @param shop_id  书店id   0、全部
     * @param keywords_type  检索条件   0、全部  1、书名  2、作者 3 isbn   默认 0 
     * @param keywords  检索条件
     * @param book_selector  是否套书  0、全部 1 是  2 否   默认 0
     * @param is_recom  是否为推荐图书  0、全部 1 是 2 否   默认 0
     * @param is_plane	是否在架   0、全部 1 在架   2 已下架
     * @param type_id  类型id   0 为其他（type_id 为0的时候，书店id必须存在）  空为全部
     * @param limit  条数，默认显示 10条
     * @param not_allow_year  年限限制
     * @param shop_all_id  管辖书籍书籍限制
     * @param is_stock  是否需要有库存的数据，  1 需要，其余数字查询无库存数据   null，不做限制（都显示）
     */
    public function lists($field = null, $shop_id = null, $keywords = null, $keywords_type = null, $type_id = null, $book_selector = null, $is_recom = null, $is_plane = null, $is_stock = null, $not_allow_year = null, $shop_all_id = null, $limit = 10)
    {
        $type_id = is_null($type_id) ? '' : $type_id;
        if (empty($field)) {
            $field = [
                'n.id', 'n.book_name', 'n.author', 'n.press', 'n.pre_time', 'n.isbn', 'n.price', 'n.img', 'n.is_recom', 'n.is_plane',
                'n.book_selector', 'n.create_time', 'n.number', 's.name as shop_name', 't.type_name', DB::raw("if(number > 0,1,0) as order_num")
            ];
        } else {
            $field[] = DB::raw("if(number > 0,1,0) as order_num"); //追加一个元素
        }
      //  DB::enableQueryLog();
        $res = $this->from($this->getTable() . ' as n')
            ->select($field)
            ->join('shop_book_type as t', 't.id', '=', 'n.type_id')
            ->join('shop as s', 's.id', '=', 'n.shop_id')
            ->where(function ($query) use ($keywords, $keywords_type) {
                if (!empty($keywords)) {
                    if (!empty($keywords_type)) {
                        switch ($keywords_type) {
                            case 1:
                                $query->where('n.book_name', 'like', "%$keywords%");
                                break;
                            case 2:
                                $query->where('n.author', 'like', "%$keywords%");
                                break;
                            case 3:
                                $query->where('n.isbn', 'like', "%$keywords%");
                                break;
                        }
                    } else {
                        $query->where('n.book_name', 'like', "%$keywords%")
                            ->orWhere('n.author', 'like', "%$keywords%")
                            ->orWhere('n.isbn', 'like', "%$keywords%");
                    }
                }
            })
            ->where(function ($query) use ($shop_id, $book_selector, $type_id, $is_recom, $is_plane, $is_stock, $shop_all_id, $not_allow_year) {
                if (!empty($shop_id)) {
                    $query->where('n.shop_id', $shop_id);
                }
                if (!empty($book_selector)) {
                    $query->where('n.book_selector', $book_selector);
                }
                if (!empty($is_recom)) {
                    $query->where('n.is_recom', $is_recom);
                }
                if (!empty($is_plane)) {
                    $query->where('n.is_plane', $is_plane);
                }
                if ($type_id !== '') {
                    $query->where('n.type_id', $type_id);
                }

                if ($shop_all_id) {
                    $query->whereIn('n.shop_id', $shop_all_id);
                }
                if ($is_stock) {
                    if ($is_stock == 1) {
                        $query->where('n.number', '>', 0);
                    } else {
                        $query->where('n.number', 0);
                    }
                }
                if ($not_allow_year && $not_allow_year != '0.0' && $not_allow_year != '0.00') {
                    $query->whereRaw('left(pre_time, 4) >= ' . $not_allow_year); //排除不符合日期的数据
                }
            })
            ->where(function ($query) {
                $query->whereRaw('way = 1 or way = 3');
            })
            ->where('s.is_del', 1)
            ->where('n.is_del', 1)
           // ->where('n.is_plane', 1)
            //  ->where('book_num', '<>', '01020111')  //显示获取数据
            //  ->where('book_num', 'not like', '02%')
            ->orderBy('n.is_recom')
            ->orderByDesc('order_num')
            ->orderBy('is_plane')
            ->orderByDesc('n.id')
            ->paginate($limit)
            ->toArray();
        // dump(DB::getQueryLog());die;
        return $res;
    }


    /**
     * 新书详情
     * @param id  书籍id
     * @param shop_all_id  限制书店id
     */
    public function detail($id, $shop_all_id = null)
    {
        $res = $this->from($this->getTable() . ' as n')
            ->select('n.id', 'n.book_name', 'n.shop_id', 'n.type_id', 'n.author', 'n.press', 'n.pre_time', 'n.isbn', 'n.price', 'n.img', 'n.is_recom', 'n.is_plane', 'n.book_selector', 'n.book_num', 'n.create_time', 'n.number', 'n.intro', 's.name as shop_name', 't.type_name', 'n.access_number')
            ->join('shop_book_type as t', 't.id', '=', 'n.type_id')
            ->join('shop as s', 's.id', '=', 'n.shop_id')
            ->where(function ($query) use ($shop_all_id) {
                if ($shop_all_id) {
                    $query->whereIn('n.shop_id', $shop_all_id);
                }
            })
            ->where('n.id', $id)
            ->first();
        return $res;
    }


    /**
     * 获取同类数据
     */
    public function getLikeBook($id, $shop_id, $type_id)
    {
        $rand_id =  $this->where('type_id', $type_id)
            ->where('shop_id', $shop_id)
            ->where('id', '<>', $id)
            ->where('is_del', 1)
            ->where('is_plane', 1)
            ->where('number', '>', 0)
            ->pluck('id')
            ->toArray();

        //获取同类书籍
        if (!empty($rand_id)) {
            $other_data =  $this->select('id', 'book_name', 'author', 'isbn', 'press', 'pre_time', 'price', 'img', 'intro', 'type_id', 'number', 'shop_id')
                ->where('id', $rand_id[floor(rand(0, count($rand_id) - 1))])
                ->with(['getShopName' => function ($query) {
                    $query->select('id', 'name as shop_name');
                }, 'getTypeName' => function ($query) {
                    $query->select('id', 'type_name');
                }])
                ->first();
        } else {
            $other_data = [];
        }
        return $other_data;
    }



    /**
     * 判断新书借阅权限
     * @param string $user_id 用户id
     * @param string $book_id 书籍id  多个用逗号拼接
     * @param $is_admin 是否是后台
     * @param $is_order_view 是否是订单页面提交的
     */
    public function  checkNewBookBorrowAuth($account_id, $book_id, $is_admin = false, $is_order_view = false)
    {
        if (!is_array($book_id)) {
            $book_id = explode(",", $book_id);
        }
        $book_id = array_unique(array_filter($book_id));
        if (empty($book_id)) {
            throw new \Exception('书籍id不能为空');
        }
        //获取书籍信息
        $book_info = $this->getBookInfo($book_id, ['book_name', 'price', 'pre_time', 'isbn', 'shop_id', 'book_selector']);
        if (empty($book_info)) {
            throw new \Exception('书籍信息不能为空');
        }
        //判断是否是只有一个书店的书
        $this->checkShopIdUnique($book_info);
        //判断此书是否在采购流程中
        $this->checkBookHomePurchaseIng($book_info, $is_admin, $is_order_view);
        //判断当前书籍本数，是否在邮费设置的上限内
        $this->checkPostageSet($book_id);
        //判断图书馆采购金额是否上限
        $this->checkBookHomePurchaseBudget($book_info);

        //判断书籍年限上限
        $this->checkNotAllowYear($book_info);
        //判断单本书籍最大金额
        $this->checkOneMoney($book_info);
        //判断单次采购金额上限
        $this->checkPersonTimeMoney($book_info);
        //判断单次采购本书上限
        $this->checkPersonTimeNumber($book_info);
        //判断本系统复本书是否满足要求
        $this->thisSystemDuplicateNumber($book_info);

        //获取读者证号信息
        $account_lib_info = UserLibraryInfo::find($account_id);

        //判断读者是否有书逾期未归还或归还后
        // $this->checkReaderBookHomePurchaseState($account_id);
        //判断读者在图书馆 和 本地荐购是否有逾期金额未缴纳
        // $this->checkReaderOverdueMoney($account_id);

        //判断当前书籍是否已经采购过（改为：每人同种书不允许超过 几本（增对新书有效）0或空 表示不限）
        $this->checkNowIsBookHomePurchase($book_info, $account_id, $is_admin, $is_order_view);

        //判断每月采购金额上限
        $this->checkPersonMonthMoney($book_info, $account_lib_info);
        //判断每月采购本书上限
        $this->checkPersonMonthNumber($book_info, $account_lib_info);
        //判断每年采购金额上限
        $this->checkPersonYearMoney($book_info, $account_lib_info);
        //判断每年采购本书上限
        $this->checkPersonYearNumber($book_info, $account_lib_info);


        if (config('other.is_validate_lib_api')) {
            //相同书籍可存在数量(图书馆系统)
            $this->checkDuplicateNumber($book_info, $is_admin, $is_order_view);
            //判断读者在图书馆借阅权限
            $this->validateBorrowLibSystem($book_info, $account_lib_info, $is_admin, $is_order_view);
        }



        $shop_id = array_column($book_info, 'shop_id');
        $shop_id = array_unique($shop_id);
        return ['book_id' => $book_id, 'shop_id' => $shop_id[0], 'account_id' => $account_id, 'account' => $account_lib_info['account']];
    }


    /**
     * 判断书店id唯一
     */
    public function checkShopIdUnique($book_info)
    {
        $shop_id = array_column($book_info, 'shop_id');
        $shop_id = array_unique($shop_id);

        if (count($shop_id) > 1) {
            throw new \Exception('不允许多个书店的书同时荐购');
        }
    }
    /**
     * 判断当前书籍本数，是否在邮费设置的上限内
     */
    public function checkPostageSet($book_id)
    {
        $postage_count = BookHomePostageSet::count();
        if (count($book_id) > $postage_count) {
            throw new \Exception('当前书籍本数，超过单次最大借阅量，不允许采购');
        }
    }
    /**
     * 判断图书馆采购金额是否上限
     */
    public function checkBookHomePurchaseBudget($book_info)
    {
        $price = array_column($book_info, 'price');
        $price = array_sum($price);

        $BookHomePurchaseSetModelObj = new BookHomePurchaseSet();
        $BookHomePurchase_budget = $BookHomePurchaseSetModelObj->where('type', 1)->value('number');
        $already_purchase_money = $BookHomePurchaseSetModelObj->getBookHomePurchaseMoney();
        if (($BookHomePurchase_budget - $already_purchase_money) < $price) {
            throw new \Exception('图书采购预算已达上限不允许采购');
        }
    }
    /**
     * 判断书籍复本数
     */
    public function checkDuplicateNumber($book_info, $is_admin, $is_order_view)
    {
        $duplicate_number = BookHomePurchaseSet::where('type', 10)->value('number');
        $bookHomeOrderModel = new BookHomeOrder();
        $controllerObj = new Controller();
        $libApi = $controllerObj->getLibApiObj();

        //TODO
        $notInInitLocalList = BookHomePurchaseSet::where('type', 18)->value('content');
        if (!empty($notInInitLocalList)) {
            $notInInitLocalList = explode(",", $notInInitLocalList);
        }
        foreach ($book_info as $key => $val) {
            //限制只允许借阅非套书
            if ($val['book_selector'] == 1) {
                throw new \Exception('《' . cut_str($val['book_name'], 5) . '》 已达到图书馆复本数上限，不允许采购');
            }

            //这里需要增加排除限制 TODO
            $res = $libApi->getBookInfo($val['isbn']);
            $num = 0;
            if ($res['code'] == 200) {
                //在查询馆藏数
                if (!empty($notInInitLocalList)) {
                    //排除查询
                    foreach ($res['content']['data'] as $v) {
                        $countAssetNum = $libApi->countAssetNum($v['metatable'], $v['metaid'], [], [], [], [], [], $notInInitLocalList);
                        if ($countAssetNum['code'] == 200) {
                            $num += $countAssetNum['content']['count'];
                        } else {
                            throw new \Exception($countAssetNum['msg']);
                        }
                    }
                } else {
                    //不排除直接查询
                    foreach ($res['content']['data'] as $v) {
                        $num += $v['holding_num'];
                    }
                }
                if ($num >= $duplicate_number) {
                    throw new \Exception('《' . cut_str($val['book_name'], 5) . '》 已达到图书馆复本数上限，不允许采购');
                }
            }
            //获取已采购的书籍本数
            $result = $bookHomeOrderModel->from($bookHomeOrderModel->getTable() . ' as o')
                ->join('book_home_order_book as b', 'b.order_id', '=', 'o.id')
                ->join('shop_book as s', 's.id', '=', 'b.book_id')
                ->where('s.isbn', $val['isbn'])
                ->where(function ($query) use ($is_order_view) {
                    $query->orWhere('o.is_pay', 1)
                        ->orWhere('o.is_pay', 2)
                        ->orWhere('o.is_pay', 6);
                    //->orWhere('o.is_pay', 8);
                })->count();
            if ($is_order_view || $is_admin) {
                $number = $result <= 0 ? $result : $result - 1; //后台需要减去一本

                $num += $number;
            } else {
                $num += $result;
            }

            if ($num >= $duplicate_number) {
                throw new \Exception('《' . cut_str($val['book_name'], 5) . '》 已达到图书馆复本数上限，不允许采购');
            }
        }
    }
    /**
     * 判断书籍年限上限
     */
    public function checkNotAllowYear($book_info)
    {
        $not_allow_year = BookHomePurchaseSet::where('type', 11)->value('number');
        if (!empty($not_allow_year)) {
            foreach ($book_info as $key => $val) {
                if (mb_strlen($val['pre_time']) < 4) {
                    continue; //直接通过
                }
                if ((int)mb_substr($val['pre_time'], 0, 4) < $not_allow_year) {
                    throw new \Exception('《' . cut_str($val['book_name'], 5) . '》 当前书籍出版年限不允许采购');
                }
            }
        }
    }

    /**
     * 判断单本书籍最大金额上限
     */
    public function checkOneMoney($book_info)
    {
        $one_money = BookHomePurchaseSet::where('type', 9)->value('number');
        if (empty($one_money)) {
            throw new \Exception('单本书籍最大金额设置不正确，请联系管理员处理');
        }
        foreach ($book_info as $key => $val) {
            if ($val['price'] > $one_money) {
                throw new \Exception('《' . cut_str($val['book_name'], 5) . '》 书籍金额，超过最大金额限制，不允许采购');
            }
        }
    }

    /**
     * 判断单次采购金额上限
     */
    public function checkPersonTimeMoney($book_info)
    {
        $time_money = BookHomePurchaseSet::where('type', 4)->value('number');

        $price = array_column($book_info, 'price');
        $price = array_sum($price);

        if ($time_money < $price) {
            throw new \Exception('单次采购金额达到上限，不允许采购');
        }
    }
    /**
     * 判断单次采购本书上限
     */
    public function checkPersonTimeNumber($book_info)
    {
        $one_number = BookHomePurchaseSet::where('type', 3)->value('number');

        if ($one_number < count($book_info)) {
            throw new \Exception('单次采购本数达到上限，不允许采购');
        }
    }

    /**
     * 判断读者是否有书逾期未归还或归还后
     */
    public function checkReaderBookHomePurchaseState($account_id)
    {
        //判断是否有书逾期未归还
        $res = BookHomePurchase::where('account_id', $account_id)
            ->where('expire_time', 'lt', date('Y-m-d H:i:s'))
            ->where('return_state', 'neq', 2)
            ->first();

        if ($res) {
            throw new \Exception('您之前采购的书存在逾期未归还的情况，暂不允许采购');
        }
        //判断是否归还后未处理逾期金额  在 checkReaderOverdueMoney 已经处理了
        /* BookHomePurchaseModel::where('account_id' , $account_id)
            ->where('return_state' , 2)
            ->where('is_pay' , 3)
            ->first();

        if($res){
            throw new \Exception('您之前采购的书存在逾期未缴费的情况，暂不允许采购');
        }*/
    }

    /**
     * 判断读者是否有书逾期未归还或归还后
     */
    public function checkReaderOverdueMoney($account_id)
    {
        //判断是否有书逾期未归还
        $account_info = UserLibraryInfo::find($account_id);

        if (empty($account_info)) {
            throw new \Exception('读者信息获取失败');
        }

        if (!empty($account_info['diff_money']) && $account_info['diff_money'] != '0.00') {
            throw new \Exception('您在图书馆，存在欠费情况，暂不允许采购');
        }
        if (!empty($account_info['recom_diff_money']) && $account_info['recom_diff_money'] != '0.00') {
            throw new \Exception('您在书店采购的图书，存在欠费情况，暂不允许采购');
        }
    }
    /**
     * 判断当前书籍是否已经采购过
     */
    public function checkNowIsBookHomePurchase($book_info, $account_id, $is_admin = false, $is_order_view = false)
    {
        $BookHomePurchaseModelObj = new BookHomePurchase();
        $BookHomeOrderModel = new BookHomeOrder();
        $number = BookHomePurchaseSet::where('type', 17)->value('number');
        $number = empty($number) ? $number : 1; //0 或空 标识同一本书只能采购一本
        foreach ($book_info as $key => $val) {
            $res = $BookHomePurchaseModelObj->where('isbn', $val['isbn'])->where('account_id', $account_id)->count();

            if ($is_admin && $res > $number) {
                throw new \Exception('《' . cut_str($val['book_name'], 5) . '》 此书籍已采购，不允许重复采购');
            } elseif (!$is_admin && $res >= $number) {
                throw new \Exception('《' . cut_str($val['book_name'], 5) . '》 此书籍已采购，不允许重复采购');
            }

            //如果是后台就不验证此参数
            if ($is_admin) {
                return true;
            }
            $result = $BookHomeOrderModel->from($BookHomeOrderModel->getTable() . ' as o')
                ->select('o.is_pay')
                ->join('book_home_order_book as b', 'b.order_id', '=', 'o.id')
                ->join('shop_book as s', 's.id', '=', 'b.book_id')
                ->where('s.isbn', $val['isbn'])
                ->where('o.account_id', $account_id)
                ->where(function ($query) use ($is_order_view) {
                    if ($is_order_view) {
                        $query->orWhere('o.is_pay', 2)
                            ->orWhere('o.is_pay', 6)
                            ->orWhere('o.is_pay', 8);
                    } else {
                        $query->orWhere('o.is_pay', 1)
                            ->orWhere('o.is_pay', 2)
                            ->orWhere('o.is_pay', 6)
                            ->orWhere('o.is_pay', 8);
                    }
                })->first();

            if ($result) {
                $msg = $result['is_pay'] == 1 ? '支付' : '查看';
                throw new \Exception('《' . cut_str($val['book_name'], 5) . '》 此书已经在采购流程中 请到个人中心-物流订单' . $msg);
            }
        }
    }


    /**
     * 判断每月采购金额上限
     */
    public function checkPersonMonthMoney($book_info, $account_lib_info)
    {
        $month_money = BookHomePurchaseSet::where('type', 6)->value('number');
        if (empty($month_money)) {
            throw new \Exception('每月可采购金额设置不正确，请联系管理员处理');
        }
        if ($account_lib_info['last_mmm'] == date('Ym')) {

            $price = array_column($book_info, 'price');
            $price = array_sum($price);
            $price += $account_lib_info['last_money']; //加上本月采购金额
            if ($price > $month_money) {
                throw new \Exception('本月可采购金额已达上限，请联系管理员处理');
            }
        }
    }
    /**
     * 判断每月采购本书上限
     */
    public function checkPersonMonthNumber($book_info, $account_lib_info)
    {
        $month_number = BookHomePurchaseSet::where('type', 5)->value('number');
        if (empty($month_number)) {
            throw new \Exception('每月可采购本数设置不正确，请联系管理员处理');
        }
        if ($account_lib_info['last_mmm'] == date('Ym')) {

            $count = count($book_info);
            $count += $account_lib_info['last_num']; //加上本月采购金额
            if ($count > $month_number) {
                throw new \Exception('本月可采购本数已达上限，请联系管理员处理');
            }
        }
    }

    /**
     * 判断每年采购金额上限
     */
    public function checkPersonYearMoney($book_info, $account_lib_info)
    {
        $year_money = BookHomePurchaseSet::where('type', 8)->value('number');
        if (empty($year_money)) {
            throw new \Exception('每年可采购金额设置不正确，请联系管理员处理');
        }

        if ($account_lib_info['e_time'] >= date('Ym')) {
            $price = array_column($book_info, 'price');
            $price = array_sum($price);
            $price += $account_lib_info['recomed_money']; //加上本年采购金额
            if ($price > $year_money) {
                throw new \Exception('本年可采购金额已达上限，请联系管理员处理');
            }
        }
    }

    /**
     * 判断每年采购本书上限
     */
    public function checkPersonYearNumber($book_info, $account_lib_info)
    {
        $year_number = BookHomePurchaseSet::where('type', 7)->value('number');
        if (empty($year_number)) {
            throw new \Exception('每年可采购本数设置不正确，请联系管理员处理');
        }

        if ($account_lib_info['e_time'] >= date('Ym')) {

            $count = count($book_info);
            $count += $account_lib_info['recomed_num']; //加上本年采购本数
            if ($count > $year_number) {
                throw new \Exception('本年可采购本数已达上限，请联系管理员处理');
            }
        }
    }

    /**
     * 判断读者在图书馆借阅权限（新书判断）新书都是“非一卡通”书籍
     */
    public function validateBorrowLibSystem($book_info, $account_info, $is_admin, $is_order_view)
    {
        //最后判断读者借阅权限
        $controllerObj = new Controller();
        $libApi = $controllerObj->getLibApiObj();
        //新书判断一次就好了，因为接口不支持一次性判断多本
        $res = $libApi->validateBorrow($account_info['account'], null, $book_info[0]['shop_id']);
        if ($res['code'] != 200) {
            throw new \Exception($res['msg']);
        }

        //自己判断借阅本书是否上限
        $result = $libApi->getReaderInfo($account_info['account']); //获取读者借阅本书
        if ($result['code'] != 200) {
            throw new \Exception($result['msg']);
        }

        $count = count($book_info);
        /* if ($result['content']['reader']['eCardType']) {
             $num = $result['content']['readerBorrowInfo']['cloudAbleBorrowNum'] - $result['content']['readerBorrowInfo']['cloudCurrentBorrowNum'] - $count;
         }else{
             $num = $result['content']['readerBorrowInfo']['ableBorrowNum'] - $result['content']['readerBorrowInfo']['currentBorrowNum'] - $count;
         }*/

        $num = $result['content']['readerBorrowInfo']['ableBorrowNum'] - $count;

        if ($num < 0) {
            throw new \Exception('此次借阅书籍本数过多，超过用户借阅上限，暂不允许借阅');
        }

        //查询用户在借阅流程中的数据本数
        if (!$is_admin && !$is_order_view) {
            $borrowing_data = BookHomeOrder::select('id', 'type')
                ->where('account_id', $account_info['id'])
                ->where(function ($query) {
                    $query->orWhere('is_pay', 1)
                        ->orWhere('is_pay', 2)
                        ->orWhere('is_pay', 6);
                })->select();

            $borrowing_able_borrow_num = 0;
            $BookHomeOrderBookModelObj = new BookHomeOrderBook();
            $libBookBarcodeModelObj = new LibBookBarcode();
            foreach ($borrowing_data as $key => $val) {
                $book_home_order_book_data = $BookHomeOrderBookModelObj->select('type', 'barcode_id')->where('order_id', $val['id'])->get();
                foreach ($book_home_order_book_data as $k => $v) {
                    //新书为非一卡通
                    if ($v['type'] == 1) {
                        $borrowing_able_borrow_num++;
                    } else {
                        $barcode = $libBookBarcodeModelObj->where('id', $v['barcode_id'])->value('barcode');

                        //验证当前条形码 是一卡通还是非一卡通
                        $barcode_info = $libApi->getAssetByBarcode($barcode);
                        if ($barcode_info['code'] === 200) {
                            //ecardflag 为1 则为一卡通。其它情况为非一卡通。没返回就是 非一卡通
                            if (!isset($barcode_info['content']['ecardFlag']) || $barcode_info['content']['ecardFlag'] != 1) {
                                $borrowing_able_borrow_num++;
                            }
                        } else {
                            throw new \Exception('数据获取失败'); //第一个失败就全部失败
                        }
                    }
                }
            }

            $num -= $borrowing_able_borrow_num;
            if ($num < 0) {
                throw new \Exception('申请借阅书籍本数过多，超过用户借阅上限，请等待后台管理员审核');
            }
        }
    }

    /**
     * 判断此数据是否在采购流程中
     */
    public function checkBookHomePurchaseIng($book_info, $is_admin, $is_order_view)
    {
        //如果是后台  或者 是订单界面就不验证
        if ($is_admin || $is_order_view) {
            return true;
        }
        $obj = new BookHomePurchase();
        foreach ($book_info as $key => $val) {
            $res = $obj->where('isbn', $val['isbn'])->where(function ($query) {
                $query->orWhere('is_pay', 1)
                    ->orWhere('is_pay', 2)
                    ->orWhere('is_pay', 6);
            })->first();
            if ($res) {
                throw new \Exception('《' . cut_str($val['book_name'], 5) . '》 已有用户提交申请，暂不允许借阅');
            }
        }
    }

    /**
     * 根据书籍id 获取书店id
     * @param $book_id 书籍id   多个数组形式
     */
    public function getBookInfo($book_id, $field = null)
    {
        if (empty($field)) {
            $field = ['id', 'book_name', 'author', 'price', 'press', 'img', 'pre_time', 'isbn', 'shop_id', 'book_selector'];
        }
        return $this->select($field)->find($book_id)->toArray();
    }


    /**
     * 增加和减少书籍库存
     * @param book_id  书籍id
     * @param state  1 退回  2 购买
     */
    public static function modifyRepertoryNumber($book_id, $state)
    {
        $book_info = self::select('book_name', 'number', 'borrow_number')->where('id', $book_id)->first();
        $number = $book_info['number'];
        $borrow_number = $book_info['borrow_number'];
        if ($state == 2) {
            $number--;
            $borrow_number++;
            if ($number < 0) {
                return '《' . $book_info['book_name'] . '》 库存不足，暂不允许采购';
            }
        } else {
            $number++;
            $borrow_number--;
        }
        // $book_info->number = $number;
        // $book_info->borrow_number = $borrow_number;
        $res = $book_info->where('id', $book_id)->update(['number' => $number, 'borrow_number' => $borrow_number]);
        if ($res) {
            return true;
        }
        return '库存修改失败';
    }

    /**
     *  判断本系统复本书是否满足要求
     */
    public static function thisSystemDuplicateNumber($book_info)
    {
        $BookHomePurchaseModelObj = new BookHomePurchase();
        $number = BookHomePurchaseSet::where('type', 16)->value('number');
        if (empty($number)) {
            throw new \Exception('本系统复本书设置不正确，请联系管理员处理');
        }

        foreach ($book_info as $key => $val) {
            $res = $BookHomePurchaseModelObj->where('isbn', $val['isbn'])->count();
            if ($res >= $number) {
                throw new \Exception('《' . cut_str($val['book_name'], 5) . '》 此书籍在本系统限制复本书已到上限，不允许采购');
            }
        }
    }
    /**
     * 根据状态获取时间状态名称
     */
    public static function getStateNameAndTime($data)
    {
        switch ($data['is_pay']) {
            case 2:
                $res['state_name'] = '已支付';
                $res['time'] = $data['change_time'];
                break;
            case 5:
                $res['state_name'] = '已退款';
                $res['time'] = $data['refund_time'];
                break;
            case 6:
                $res['state_name'] = '已同意';
                $res['time'] = $data['agree_time'];
                break;
            case 7:
                $res['state_name'] = '已拒绝';
                $res['time'] = $data['agree_time'];
                break;
            case 8:
                $res['state_name'] = '已发货';
                $res['time'] = $data['deliver_time'];
                break;
            case 9:
                $res['state_name'] = '无法发货';
                $res['time'] = $data['deliver_time'];
                break;
        }
        return $res;
    }



    /**
     * 获取热门书籍推荐，按类型
     */
    public function hotBorrow($limit = 6, $not_allow_year = null)
    {
        $limit = $limit + 6; //后面需要多返回一些类型
        //获取借阅次数最多的条形码
        $borrow_type_id_arr = BookHomePurchase::from('book_home_purchase as p')
            ->select(DB::raw("count(`type_id`) as number"), 'type_id')
            ->join('shop_book as b', 'b.id', '=', 'p.book_id')
            ->join('shop_book_type as t', 't.id', '=', 'b.type_id')
            ->join('shop as s', 's.id', '=', 'b.shop_id')
            ->where('s.is_del', 1)
            ->where('p.type', 1)
            ->where('t.is_del', 1)
            ->where('b.is_del', 1)
            ->groupBy('b.type_id')
            ->orderByDesc('number')
            ->limit($limit)
            ->get()
            ->toArray();
            $borrow_type_id_arr = array_column($borrow_type_id_arr, 'type_id');
        $count = count($borrow_type_id_arr);
        if ($count < $limit) {
            $other_borrow_type_id_arr = ShopBookType::where('is_del', 1)->whereNotIn('id', $borrow_type_id_arr)->orderBy('id')->limit($limit - $count)->pluck('id')->toArray();
            $borrow_type_id_arr = array_merge($borrow_type_id_arr, $other_borrow_type_id_arr);
        }
        $type_name_arr = ShopBookType::select('id', 'type_name')->where('is_del', 1)->whereIn('id', $borrow_type_id_arr)
                            //->groupBy('type_name')
                            ->get()->toArray();
        $type_name = [];
        $data = [];
        $i = 0;
        foreach ($type_name_arr as $key => $val) {
            if (count($data) < 3) {
                $book_info = ShopBook::from('shop_book as b')
                    ->select('b.id', 'b.book_name', 'b.author', 'b.img', 'b.number')
                    ->join('shop_book_type as t', 't.id', '=', 'b.type_id')
                    ->join('shop as s', 's.id', '=', 'b.shop_id')
                    ->where('s.is_del', 1)
                    ->where('t.is_del', 1)
                    ->where('b.is_del', 1)
                    ->where('b.is_plane', 1)
                    ->where('b.book_selector', 2)
                    ->where('b.type_id', $val['id'])
                    ->whereIn('way', [1, 3])
                    ->where(function ($query) use ($not_allow_year) {
                        if ($not_allow_year && $not_allow_year != '0.0' && $not_allow_year != '0.00') {
                            $query->whereRaw('left(pre_time, 4) >= ' . $not_allow_year); //排除不符合日期的数据
                        }
                    })
                    ->orderByDesc('number')
                    ->limit(4)
                    ->get()
                    ->toArray();
                if ($book_info) {
                    $data[$i]['type_id'] = $val['id'];
                    $data[$i]['type_name'] = $val['type_name'];
                    $data[$i]['data'] =  $book_info;
                    $i++;
                }
            } else {
                $type_name[] = $val;
            }
        }

        return [$type_name, $data];
    }
}
