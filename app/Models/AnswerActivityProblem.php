<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * 获取题目表
 */
class AnswerActivityProblem extends BaseModel
{
    use HasFactory;


    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'answer_activity_problem';


    /*关联答案*/
    public function conAnswer()
    {
        return $this->hasMany(AnswerActivityProblemAnswer::class, 'pro_id', 'id');
    }

    /*关联单位*/
    public function conUnit()
    {
        return $this->hasOne(AnswerActivityUnit::class, 'id', 'unit_id');
    }

    /**
     * 是否存在问题
     * @param act_id 
     */
    public static function isHaveProblem($act_id)
    {
        $problem_info = self::where('act_id', $act_id)->where('is_del', 1)->where('state', 1)->count();
        if ($problem_info < 10) {
            return '正常使用的题目少于10个，禁止发布';
        }
        return true;
    }

    /**
     * 列表
     * @param act_id int 活动id
     * @param unit_id int 单位id   null 为全部  0 为 公共题库
     * @param state int 1正常 2被禁用
     * @param is_show_analysis int 是否显示解析  1、显示 2、不显示
     * @param page int 当前页
     * @param limit int 分页大小
     * @param start_time datetime 创建时间范围搜索(开始) 
     * @param end_time datetime 创建时间范围搜索(结束) 
     * @param keywords string 搜索关键词(类型题目)
     */
    public function lists($act_id, $keywords, $unit_id, $state, $is_show_analysis, $start_time, $end_time, $page, $limit)
    {
        $res = $this->select('id', 'act_id', 'unit_id', 'title', 'img', 'type', 'state', 'analysis', 'is_show_analysis', 'create_time')
            ->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('title', 'like', '%' . $keywords . '%');
                }
            })->where(function ($query) use ($act_id, $unit_id, $state, $is_show_analysis, $start_time, $end_time) {
                if ($act_id) {
                    $query->where('act_id', $act_id);
                }
                if ($unit_id || $unit_id === '0' || $unit_id === 0) {
                    $query->where('unit_id', $unit_id);
                }
                if ($state) {
                    $query->where('state', $state);
                }
                if ($is_show_analysis) {
                    $query->where('is_show_analysis', $is_show_analysis);
                }
                if ($start_time && $end_time) {
                    $query->whereBetween('create_time', [$start_time, $end_time]);
                }
            })
            ->with(['conUnit' => function ($query) {
                $query->select('id', 'name');
            }])
            ->with(['conAnswer' => function ($query) {
                $query->select('id', 'pro_id', 'content', 'status', 'create_time');
            }])
            ->where('is_del', 1)
            ->orderByDesc('id')
            ->paginate($limit)
            ->toArray();

        $controllerObj = new Controller();
        $res = $controllerObj->disPageData($res);

        foreach ($res['data'] as $key => $val) {
            $res['data'][$key]['success_answer'] = '';
            foreach ($val['con_answer'] as $k => $v) {
                if ($v['status'] == 1)
                    $res['data'][$key]['success_answer'] = $v['content'];
                continue;
            }
        }

        return $res;
    }
    /**
     * 详情
     * @param id int 问题id
     */
    public function detail($id)
    {
        $res = $this->select('id', 'act_id', 'unit_id', 'title', 'img', 'type', 'state', 'analysis', 'is_show_analysis', 'create_time')
            ->where('id', $id)
            ->where('is_del', 1)
            ->with(['conUnit' => function ($query) {
                $query->select('id', 'name');
            }])
            ->with(['conAnswer' => function ($query) {
                $query->select('id', 'pro_id', 'content', 'status', 'create_time');
            }])
            ->first();
        //获取单位区域
        if (!empty($res)) {
            $res['unit_name'] = !empty($res['conUnit']['name']) ?  $res['conUnit']['name'] : '';
            unset($res['conUnit']);
        }
        return $res;
    }


    /**
     * 删除多个题目
     * @param act_id int 活动id 
     * @param ids int id all 表示全部   多个 逗号拼接
     */
    public function delMany($act_id, $ids)
    {
        if ($ids == 'all') {
            $this->where('act_id', $act_id)->update(['is_del' => 2, 'change_time' => date('Y-m-d H:i:s')]);
        } else {
            $ids = explode(',', $ids);
            $this->where('act_id', $act_id)->whereIn('id', $ids)->update(['is_del' => 2, 'change_time' => date('Y-m-d H:i:s')]);
        }
    }



    /**
     * 获取题库
     * @param user_guid  用户guid
     * @param act_id  活动id
     * @param unit_id  单位id
     * @param act_info  活动信息
     * @param           answer_rule  答题规则   1、专属题和公共题 混合  2、优先回答专属题 
     * @param           pattern  答题模式  1  馆内答题形式  2 轮次排名形式 （按轮次排名）  3 爬楼梯形式
     * @param           node  活动类型    1 独立活动  2 单位联盟   3 区域联盟   
     * @param           answer_number  答题数量  轮次存在多道题的情况
     */
    public function getProblem($user_guid, $act_id, $unit_id, $act_info)
    {
        switch ($act_info['pattern']) {
            case 1:
                return $this->getMuseumAnswerProblem($user_guid, $act_id, $unit_id, $act_info['answer_rule'], $act_info['answer_time'], $act_info['node']);
                break;
            case 2:
                return $this->getCorrectAnswerProblem($user_guid, $act_id, $unit_id, $act_info['answer_rule'], $act_info['answer_time'], $act_info['answer_number'], $act_info['node']);
                break;
            case 3:
                return $this->getStairsAnswerProblem($user_guid, $act_id, $unit_id, $act_info['answer_rule'], $act_info['answer_time'], $act_info['node']);
                break;
        }
        return false;
    }

    /**
     * 回答问题
     * @param user_guid  用户guid
     * @param answer 回答问题数据  
     * @param act_info  活动信息
     * @param           answer_rule  答题规则   1、专属题和公共题 混合  2、优先回答专属题 
     * @param           pattern  答题模式  1  馆内答题形式  2 轮次排名形式 （按轮次排名）  3 爬楼梯形式
     * @param           answer_number  答题数量  轮次存在多道题的情况
     */
    public function replyProblem($user_guid, $answer, $act_info)
    {
        switch ($act_info['pattern']) {
            case 1:
                return $this->replyMuseumAnswerProblem($user_guid, $answer, $act_info);
                break;
            case 2:
                return $this->replyCorrectAnswerProblem($user_guid, $answer, $act_info);
                break;
            case 3:
                return $this->replyStairsAnswerProblem($user_guid, $answer, $act_info);
                break;
        }
        return false;
    }



    /**
     * 获取馆内答题形式题库
     * @param user_guid  用户guid
     * @param act_id  活动id
     * @param unit_id  单位id
     * @param answer_rule  答题规则   1、专属题和公共题 混合  2、优先回答专属题 
     */
    public function getMuseumAnswerProblem($user_guid, $act_id, $unit_id, $answer_rule, $answer_time, $node)
    {
        $model = new AnswerActivityUserAnswerRecord();
        //获取已答过的题库 编号
        $except_answer = $model->getHaveAnswer($act_id, $user_guid, $unit_id);
        $except_answer = $except_answer ? $except_answer->toArray() : [];
        $except_answer = array_unique($except_answer);
        $data = [];
        if ($answer_rule == 2) {
            //判断这个馆的专属题是否已经答完
            $exclusive_problem = $this->getProblemLists($act_id, $unit_id, false, $except_answer, 1);
            $data = $exclusive_problem;
        }
        if (empty($data)) {
            //随机获取公众题库
            $common_problem = $this->getProblemLists($act_id, $unit_id, true, $except_answer, 1);
            $data = $common_problem;
            if (empty($data)) {
                //随机获取所有题库（不管答没答过）
                $data = $this->getProblemLists($act_id, $unit_id, true, null, 1);
            }

            if (empty($data)) {
                return [];
            }
        }

        //写入馆内答题记录
        $activityUnitUserNumber = new AnswerActivityUnitUserNumber();
        $activityUnitUserNumber->addUnitNumber($act_id, $unit_id, $user_guid);

        //添加用户答题总记录和概率
        $activityUserAnswerTotalNumber = new AnswerActivityUserAnswerTotalNumber();
        $activityUserAnswerTotalNumber->addUserAnswerTotalNumber($act_id, $unit_id, $user_guid, $answer_time, $node);

        //添加答题记录
        $problem_guid = $model->addUserAnswerRecord($data, $unit_id, $user_guid, $answer_time);

        $data[0]['guid'] = $problem_guid;
        return $data;
    }
    /**
     * 轮次排名形式题库 （按轮次排名）
     * @param user_guid  用户guid
     * @param act_id  活动id
     * @param unit_id  单位id
     * @param answer_rule  答题规则   1、专属题和公共题 混合  2、优先回答专属题 
     * @param answer_number  答题数量  轮次存在多道题的情况
     */
    public function getCorrectAnswerProblem($user_guid, $act_id, $unit_id, $answer_rule, $answer_time, $answer_number, $node = 1)
    {

        $model = new AnswerActivityUserCorrectAnswerRecord();
        //获取已答过的题库 编号
        $except_answer = $model->getHaveAnswer($act_id, $user_guid, $unit_id, [1]); //只排除回答正确的
        $total_answer_number = $answer_number; //重新赋值答题次数，不然下面会重置

        $data = [];
        $problem_number = 0;
        if ($answer_rule == 2) {
            //判断这个馆的专属题是否已经答完
            $exclusive_problem = $this->getProblemLists($act_id, $unit_id, false, $except_answer, $answer_number); //优先专属题
            if (!empty($exclusive_problem)) {
                $data = $exclusive_problem;
                $problem_number = count($exclusive_problem);
            }
        }

        if ($problem_number != $answer_number) {
            $answer_number = $answer_number - $problem_number;
            //随机获取公众题库
            $common_problem = $this->getProblemLists($act_id, $unit_id, true, $except_answer, $answer_number);
            if (!empty($common_problem)) {
                $problem_number = count($common_problem);
                $data = array_merge($data, $common_problem);
            }

            if ($problem_number != $answer_number) {
                $answer_number = $answer_number - $problem_number;
                //随机获取公众题库,不排除任何题目
                $common_problem = $this->getProblemLists($act_id, $unit_id, true, null, $answer_number);
                if (!empty($common_problem)) {
                    $problem_number = count($common_problem);
                    $data = array_merge($data, $common_problem);
                }
                if ($problem_number != $answer_number) {
                    return [];
                }
            }
        }

        //写入馆内答题记录
        $activityUnitUserNumber = new AnswerActivityUnitUserNumber();
        $activityUnitUserNumber->addUnitNumber($act_id, $unit_id, $user_guid);

        //添加用户每日答题总记录和概率 
        $activityUserCorrectNumber = new AnswerActivityUserCorrectNumber();
        $correct_number_id = $activityUserCorrectNumber->addUserCorrectNumber($data, $unit_id, $user_guid, $answer_time, $total_answer_number);

        //添加答题记录
        $new_arr = $model->addUserCorrectRecord($data, $unit_id, $user_guid, $answer_time, $correct_number_id);

        //添加用户答题总概率记录
        $activityUserCorrectTotalNumber = new AnswerActivityUserCorrectTotalNumber();
        $activityUserCorrectTotalNumber->addUserCorrectTotalNumber($act_id, $unit_id, $user_guid, $total_answer_number, $answer_time, $node, $correct_number_id);

        return $new_arr;
    }

    /**
     * 爬楼梯形式题库
     * @param user_guid  用户guid
     * @param act_id  活动id
     * @param unit_id  单位id
     * @param answer_rule  答题规则   1、专属题和公共题 混合  2、优先回答专属题 
     */
    public function getStairsAnswerProblem($user_guid, $act_id, $unit_id, $answer_rule, $answer_time, $node)
    {
        $model = new AnswerActivityUserStairsAnswerRecord();
        //获取已答过的题库 编号
        $except_answer = $model->getHaveAnswer($act_id, $user_guid, $unit_id);
        $except_answer = array_unique($except_answer);
        $data = [];
        if ($answer_rule == 2) {
            //判断这个馆的专属题是否已经答完
            $exclusive_problem = $this->getProblemLists($act_id, $unit_id, false, $except_answer, 1);
            $data = $exclusive_problem;
        }
        if (empty($data)) {
            //随机获取公众题库
            $common_problem = $this->getProblemLists($act_id, $unit_id, true, $except_answer, 1);
            $data = $common_problem;
            if (empty($data)) {
                //随机获取所有题库（不管答没答过）
                $data = $this->getProblemLists($act_id, $unit_id, true, null, 1);
            }

            if (empty($data)) {
                return [];
            }
        }

        //写入馆内答题记录
        $activityUnitUserNumber = new AnswerActivityUnitUserNumber();
        $activityUnitUserNumber->addUnitNumber($act_id, $unit_id, $user_guid);

        //添加用户答题总记录和概率 
        $activityUserStairsTotalNumber = new AnswerActivityUserStairsTotalNumber();
        $floor = $activityUserStairsTotalNumber->addUserStairsTotalNumber($act_id, $unit_id, $user_guid, $answer_time, $node);

        //添加答题记录
        $problem_guid = $model->addUserStairsRecord($data, $unit_id, $user_guid, $answer_time, $floor);

        $data[0]['guid'] = $problem_guid;
        return $data;
    }

    /**
     * 列表
     * @param act_id int 活动id
     * @param unit_id int 单位id  有单位id就位专属题，没有就为公共题
     * @param is_common boole 是否查询公共的资源
     * @param except_id array 需要排除的id
     * @param limit int 限制返回条数
     */
    public function getProblemLists($act_id, $unit_id, $is_common = true, $except_id = [], $limit = 1)
    {
        //  DB::enableQueryLog();
        $res = $this->select('id', 'act_id', 'unit_id', 'title', 'img', 'type', 'create_time')
            ->where(function ($query) use ($act_id, $except_id) {
                if ($act_id) {
                    $query->where('act_id', $act_id);
                }
                if ($except_id) {
                    $query->whereNotIn('id', $except_id);
                }
            })
            ->where(function ($query) use ($unit_id, $is_common) {
                if ($unit_id && $is_common) {
                    $query->OrWhere('unit_id', $unit_id)->OrWhere('unit_id', 0);
                } else if ($unit_id) {
                    $query->where('unit_id', $unit_id);
                } else if ($is_common) {
                    $query->where('unit_id', 0);
                }
            })
            ->with(['conAnswer' => function ($query) {
                $query->select('id', 'pro_id', 'content', 'create_time');
            }])
            ->where('state', 1)
            ->where('is_del', 1)
            ->inRandomOrder()   //随机获取一条
            ->limit($limit)
            ->get()
            ->toArray();

        //   dump($res);
        //   dump(DB::getQueryLog());die;
        return $res;
    }


    /**
     * 回复馆内答题形式题库
     * @param user_guid  用户guid
     * @param answer  答案 json数据
     */
    public function replyMuseumAnswerProblem($user_guid, $answer, $act_info)
    {
        $model = new AnswerActivityUserAnswerRecord();
        //获取已答过的题库 编号
        $answer_arr = json_decode($answer, true);
        $answer_arr = $answer_arr[0]; //重新定义数组
        $answer_record = $model->getAnswerRecordByGuid($answer_arr['guid']);
        if (empty($answer_record) || $act_info['id'] != $answer_record['act_id'] || $user_guid != $answer_record['user_guid']) {
            throw new Exception('网路错误！');
        }
        if ($answer_record['status'] !== 0) {
            throw new Exception('此问题已作答，不能重复作答！');
        }

        //计算近5次的答题时间，太短，则屏蔽掉
        $several_times = $model->where('user_guid', $user_guid)->where('act_id', $answer_record['act_id'])->orderByDesc('id')->limit(6)->pluck('times')->toArray(); //使用了 limit sum无效
        if ($several_times) unset($several_times[0]); //去掉当前的
        if (count($several_times) == 5 && array_sum($several_times) < 20) {
            //throw new Exception('答题异常，请稍后重试！');
            $mt_times = mt_rand(2, 8);
            sleep($mt_times);
        }

        //判断答案是否正确
        $problem_answer = $this->getProblemAnswer($answer_record['problem_id']);

        //判断是否超时
        if ($answer_record['expire_time'] < date('Y-m-d H:i:s', strtotime("-3 second"))) {
            $status = 3; //超时回答
        } elseif ((empty($answer_arr['answer_id']) && empty(trim($answer_arr['content']))) ||
            ($problem_answer['type'] == 1 && empty($answer_arr['answer_id']))  ||
            ($problem_answer['type'] == 2 && empty(trim($answer_arr['content'])))
        ) {
            $status = 3; //超时回答
        } else {
            //获取问题的答案
            if (($problem_answer['type'] == 1 && $answer_arr['answer_id'] == $problem_answer['answer_id']) ||
                ($problem_answer['type'] == 2 && trim($answer_arr['content']) == $problem_answer['con_answer'][0]['content'])
            ) {
                $status = 1; //回答正确
            } else {
                $status = 2; //回答错误
            }
        }

        //修改答题记录
        $model->changeUserAnswerRecord($answer_record, $answer_arr['answer_id'], trim($answer_arr['content']), $status);

        //判断是否获得钥匙
        $is_get_prize = false; //是否获得钥匙

        if ($status == 1 && $act_info['prize_form'] == 2) {
            //获取当前答题次数
            $answer_record_number = $model->getHaveAnswer($act_info['id'], $user_guid, $answer_record['unit_id'], [1]);
            $answer_record_number = count($answer_record_number);

            if ($answer_record_number % $act_info['answer_number'] === 0) {
                //获得一把钥匙
                $activityUserPrize = new AnswerActivityUserPrize();
                $activityUserPrize->addUserPrize($act_info['id'], $user_guid);
                //记录获取钥匙的id
                $activityUserPrizeRecord = new AnswerActivityUserPrizeRecord();
                $activityUserPrizeRecord->addUserPrizeRecord($act_info['id'], $user_guid, $act_info['pattern'], $answer_record['id']);
                $is_get_prize = true; //是否获得钥匙
            }
        }

        //修改用户答题总记录和概率,只修改对的，错的之前已经添加
        // if ($status == 1) {
        $activityUserAnswerTotalNumber = new AnswerActivityUserAnswerTotalNumber();
        $activityUserAnswerTotalNumber->changeUserAnswerTotalNumber($act_info['id'], $answer_record['unit_id'], $user_guid, $act_info['node'], $status);
        // }

        return ['problem_answer' => $problem_answer, 'unit_id' => $answer_record['unit_id'], 'status' => $status, 'is_get_prize' => $is_get_prize];
    }

    /**
     * 轮次排名形式题库 （按轮次排名）
     * @param user_guid  用户guid
     * @param answer  答案 json数据
     */
    public function replyCorrectAnswerProblem($user_guid, $answer, $act_info)
    {

        $model = new AnswerActivityUserCorrectAnswerRecord();
        $activityUserCorrectNumber = new AnswerActivityUserCorrectNumber();
        //获取已答过的题库 编号
        $answer_arr = json_decode($answer, true);

        $answer_problem = $model->getCorrectRecordByGuid($answer_arr[0]['guid']);
        if (empty($answer_problem)) {
            throw new Exception('网络错误！');
        }
        $answer_record = $activityUserCorrectNumber->getCorrectRecordById($answer_problem['correct_number_id']);
        // dump($user_guid);
        // dump($act_info['id']);
        // dump($answer_record['act_id']);
        // dump($answer_record['user_guid']);
        // die;
        if (empty($answer_record) || $act_info['id'] != $answer_record['act_id'] || $user_guid != $answer_record['user_guid']) {
            throw new Exception('网路错误！');
        }
        if ($answer_record['status'] !== 0) {
            throw new Exception('此问题已作答，不能重复作答！');
        }

        //计算近5次的答题时间，太短，则屏蔽掉 ,轮次排名取不到时间，不限制
        // $several_times = $model->where('user_guid', $user_guid)->where('act_id', $answer_record['act_id'])->orderByDesc('id')->limit(5)->pluck('times')->toArray(); //使用了 limit sum无效
        // if (count($several_times) == 5 && array_sum($several_times) < 20) {
        //     throw new Exception('答题异常，请稍后重试！');
        // }


        //获取所有题
        $answer_problem_all = $model->getCorrectRecordByCorrectNumberId($answer_problem['correct_number_id']);

        //判断答案是否正确
        $answer_news_arr = array_column($answer_arr, null, 'guid');
        $status_arr = [];
        $success = 0;
        $error = 0;
        foreach ($answer_problem_all as $key => $val) {
            $guid = $val['guid'];
            //判断是否超时,允许30s的超时
            if ($answer_record['expire_time'] < date('Y-m-d H:i:s', strtotime("-30 second"))) {
                $status_arr[$guid]['status'] = 3; //超时回答
                $status_arr[$guid]['answer_id'] = empty($answer_news_arr[$guid]['answer_id']) ? null : $answer_news_arr[$guid]['answer_id']; //超时回答
                $status_arr[$guid]['content'] = empty(trim($answer_news_arr[$guid]['content'])) ? null : trim($answer_news_arr[$guid]['content']); //超时回答
                $error++;
                continue;
            }
            $problem_answer = $this->getProblemAnswer($val['problem_id']);
            if ((empty($answer_news_arr[$guid]['answer_id']) && empty(trim($answer_news_arr[$guid]['content']))) ||
                ($problem_answer['type'] == 1 && empty($answer_news_arr[$guid]['answer_id']))  ||
                ($problem_answer['type'] == 2 && empty(trim($answer_news_arr[$guid]['content'])))
            ) {
                $status_arr[$guid]['status'] = 3; //超时回答
                $status_arr[$guid]['answer_id'] = empty($answer_news_arr[$guid]['answer_id']) ? null : $answer_news_arr[$guid]['answer_id']; //超时回答
                $status_arr[$guid]['content'] = empty(trim($answer_news_arr[$guid]['content'])) ? null : trim($answer_news_arr[$guid]['content']); //超时回答
                $error++;
            } else {
                //获取问题的答案
                if (($problem_answer['type'] == 1 && $answer_news_arr[$guid]['answer_id'] == $problem_answer['answer_id']) ||
                    ($problem_answer['type'] == 2 && trim($answer_news_arr[$guid]['content']) == $problem_answer['con_answer'][0]['content'])
                ) {
                    $status_arr[$guid]['status'] = 1; //回答正确
                    $status_arr[$guid]['answer_id'] = empty($answer_news_arr[$guid]['answer_id']) ? null : $answer_news_arr[$guid]['answer_id']; //超时回答
                    $status_arr[$guid]['content'] = empty(trim($answer_news_arr[$guid]['content'])) ? null : trim($answer_news_arr[$guid]['content']); //超时回答
                    $success++;
                } else {
                    $status_arr[$guid]['status'] = 2; //回答错误
                    $status_arr[$guid]['answer_id'] = empty($answer_news_arr[$guid]['answer_id']) ? null : $answer_news_arr[$guid]['answer_id']; //超时回答
                    $status_arr[$guid]['content'] = empty(trim($answer_news_arr[$guid]['content'])) ? null : trim($answer_news_arr[$guid]['content']); //超时回答
                    $error++;
                }
            }
        }

        //修改用户当次答题记录和概率 
        $activityUserCorrectNumber = new AnswerActivityUserCorrectNumber();
        $activityUserCorrectNumber->changeUserCorrectNumber($answer_record, $success, $error);


        //修改用户答题总记录和概率,只修改对的，错的之前已经添加
        //if ($success > 0) {//现在需要修改时间，所以都需要修改
        //若没有成功的数据，则不修改
        $activityUserCorrectTotalNumber = new AnswerActivityUserCorrectTotalNumber();
        $high_record = $activityUserCorrectTotalNumber->changeUserCorrectTotalNumber($act_info['id'], $answer_record['unit_id'], $user_guid, $act_info['answer_time'], $act_info['node']);
        //  }

        //添加答题记录
        $model->changeUserCorrectRecord($status_arr);

        return ['problem_answer' => null, 'unit_id' => $answer_record['unit_id'], 'status' => $status_arr, 'success' => $success, 'error' => $error, 'is_get_prize' => false];
    }


    /**
     * 回复爬楼梯形式题库
     * @param user_guid  用户guid
     * @param answer  问题guid
     * @param answer  答案 json数据
     */
    public function replyStairsAnswerProblem($user_guid, $answer, $act_info)
    {
        $model = new AnswerActivityUserStairsAnswerRecord();
        //获取已答过的题库 编号
        $answer_arr = json_decode($answer, true);
        $answer_arr = $answer_arr[0]; //重新定义数组
        $answer_record = $model->getStairsRecordByGuid($answer_arr['guid']);

        if (empty($answer_record) || $act_info['id'] != $answer_record['act_id'] || $user_guid != $answer_record['user_guid']) {
            throw new Exception('网路错误！');
        }
        if ($answer_record['status'] !== 0) {
            throw new Exception('此问题已作答，不能重复作答！');
        }

        //计算近5次的答题时间，太短，则屏蔽掉
        $several_times = $model->where('user_guid', $user_guid)->where('act_id', $answer_record['act_id'])->orderByDesc('id')->limit(6)->pluck('times')->toArray(); //使用了 limit sum无效
        if ($several_times) unset($several_times[0]); //去掉当前的
        if (count($several_times) == 5 && array_sum($several_times) < 20) {
            //throw new Exception('答题异常，请稍后重试！');
            $mt_times = mt_rand(2, 8);
            sleep($mt_times);
        }

        //判断答案是否正确
        $problem_answer = $this->getProblemAnswer($answer_record['problem_id']);

        //判断是否超时
        if ($answer_record['expire_time'] < date('Y-m-d H:i:s', strtotime("-3 second"))) {
            $status = 3; //超时回答
        } elseif ((empty($answer_arr['answer_id']) && empty(trim($answer_arr['content']))) ||
            ($problem_answer['type'] == 1 && empty($answer_arr['answer_id']))  ||
            ($problem_answer['type'] == 2 && empty(trim($answer_arr['content'])))
        ) {
            $status = 3; //超时回答
        } else {
            //获取问题的答案
            if (($problem_answer['type'] == 1 && $answer_arr['answer_id'] == $problem_answer['answer_id']) ||
                ($problem_answer['type'] == 2 && trim($answer_arr['content']) == $problem_answer['con_answer'][0]['content'])
            ) {
                $status = 1; //回答正确
            } else {
                $status = 2; //回答错误
            }
        }



        //添加答题记录
        $model->changeUserStairsRecord($answer_record, $answer_arr['answer_id'], trim($answer_arr['content']), $status);

        //判断是否获得钥匙
        $is_get_prize = false; //是否获得钥匙

        if ($status == 1 && $act_info['prize_form'] == 2) {
            //获取当前答题次数
            $answer_record_number = $model->getHaveAnswer($act_info['id'], $user_guid, $answer_record['unit_id'], [1]);
            $answer_record_number = count($answer_record_number);

            // Log::error($answer_record_number);
            // Log::error($act_info['answer_number']);
            // Log::error($answer_record_number % $act_info['answer_number']);
            if ($answer_record_number % $act_info['answer_number'] === 0) {
                //获得一把钥匙
                $activityUserPrize = new AnswerActivityUserPrize();
                $activityUserPrize->addUserPrize($act_info['id'], $user_guid);
                //记录获取钥匙的id
                $activityUserPrizeRecord = new AnswerActivityUserPrizeRecord();
                $activityUserPrizeRecord->addUserPrizeRecord($act_info['id'], $user_guid, $act_info['pattern'], $answer_record['id']);

                $is_get_prize = true; //是否获得钥匙
            }
        }

        //修改用户答题总记录和概率,只修改对的，错的之前已经添加
        // if ($status == 1) {
        $activityUserStairsTotalNumber = new AnswerActivityUserStairsTotalNumber();
        $activityUserStairsTotalNumber->changeUserStairsTotalNumber($act_info['id'], $answer_record['unit_id'], $user_guid, $act_info['node'], $status);
        //  }


        return ['problem_answer' => $problem_answer, 'unit_id' => $answer_record['unit_id'], 'status' => $status, 'is_get_prize' => $is_get_prize];
    }


    /**
     * 活动问题答案
     * @param problem_id int 问题id
     */
    public function getProblemAnswer($problem_id)
    {
        $res = $this->select('id', 'act_id', 'unit_id', 'title', 'img', 'type', 'answer_id', 'analysis', 'is_show_analysis', 'create_time')
            ->with(['conAnswer' => function ($query) {
                $query->select('id', 'pro_id', 'content', 'create_time')->where('status', 1);
            }])
            ->where('id', $problem_id)
            ->where('is_del', 1)
            ->first();

        if ($res) {
            $res = $res->toArray();
        }
        return $res;
    }

    /**
     * 获取用户答题数量
     */
    public function userAnswerSuccessNumber($user_guid, $start_time, $end_time)
    {
        $answerActivityUserAnswerRecordModel = new AnswerActivityUserAnswerRecord();
        $answerActivityUserCorrectAnswerRecordModel = new AnswerActivityUserCorrectAnswerRecord();
        $answerActivityUserStairsAnswerRecordModel = new AnswerActivityUserStairsAnswerRecord();
        $number1 = $answerActivityUserAnswerRecordModel->where('user_guid', $user_guid)->whereBetween('create_time', [$start_time, $end_time])->where('status', 1)->count();
        $number2 = $answerActivityUserCorrectAnswerRecordModel->where('user_guid', $user_guid)->whereBetween('create_time', [$start_time, $end_time])->where('status', 1)->count();
        $number3 = $answerActivityUserStairsAnswerRecordModel->where('user_guid', $user_guid)->whereBetween('create_time', [$start_time, $end_time])->where('status', 1)->count();
        return $number1 + $number2 + $number3;
    }
}
