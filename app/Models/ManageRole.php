<?php
namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * 管理员角色表
 */
class ManageRole extends BaseModel
{
    use HasFactory;

    const CREATED_AT='create_time';
    const UPDATED_AT= null;

    protected $table = 'manage_role';


    /**
     * 获取管理员拥有的所有角色
     * @param  $manage_id 管理员id
     */
    public function getManageRole($manage_id){
        $role_id_arr = $this->where(function($query) use($manage_id){
            if($manage_id != 1){
                $query->where('manage_id' , $manage_id);
            }
        })->pluck('role_id')->toArray();
      
        $roles = Role::select('id' , 'role_name')->whereIn('id' , $role_id_arr)->where('is_del' , 1)->get()->toArray();//去除删除的角色

        return $roles;
    }



 
    /**
     * 添加（修改）管理员角色，删除多余的，增加新增的
     * @param manage_id 管理员id
     * @param role_ids 角色id  多个 逗号 拼接
     * @param $type 类型  add 新增，直接增加   change 需要查询
     */
    public function manageRoleChange($manage_id , $role_ids , $type = 'change'){
        $role_ids = empty($role_ids) ? [] : explode(',' , $role_ids);
        if($type == 'add'){
            $role_ids_ed = [];
        }else{
            $role_ids_ed = $this->where('manage_id' , $manage_id)->groupBy('role_id')->pluck('role_id')->toArray();
        }
        $role_ids_del = array_diff($role_ids_ed,$role_ids); //对比返回在 $role_ids_ed 中但是不在 $role_ids 的数据删除
        $role_ids_add = array_diff($role_ids,$role_ids_ed); //对比返回在 $role_ids 中但是不在 $role_ids_ed 的数据添加
        $this->whereIn('role_id' , $role_ids_del)->where('manage_id' , $manage_id)->delete();
        $data = [];
        foreach($role_ids_add as $key=>$val){
            $data[$key]['manage_id'] = $manage_id; 
            $data[$key]['role_id'] = $val; 
            $data[$key]['create_time'] = date('Y-m-d H:i:s'); 
        }
        if(!empty($data)) $this->insert($data);
    }




}
