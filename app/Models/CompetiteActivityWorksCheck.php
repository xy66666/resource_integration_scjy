<?php

namespace App\Models;

use App\Http\Controllers\PdfController;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;

/**
 * 线上大赛作品审核模型
 * Class ArticleModel
 * @package app\common\model
 */
class CompetiteActivityWorksCheck extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'competite_activity_works_check';

    /**
     * 获取下一个审核管理员(需要排除当前这个)
     * @param con_id  大赛id
     */
    public function getNextCheckManageId($con_id, $works_id, $manage_id)
    {
        $check_manage_id = CompetiteActivity::where('id', $con_id)->where('is_del', 1)->value('check_manage_id');
        if (empty($check_manage_id)) {
            return false;
        }
        $check_manage_id = explode(',', $check_manage_id);
        //获取所有已审核的管理员人
        $check_manage_id_ed = $this->where('works_id', $works_id)->pluck('check_manage_id')->toArray();
        if (empty($check_manage_id_ed)) {
            if ($manage_id) {
                return $check_manage_id[0] != $manage_id ? $check_manage_id[0]  : (isset($check_manage_id[1]) ? $check_manage_id[1] : null);
            } else {
                return $check_manage_id[0];
            }
        }
        foreach ($check_manage_id as $key => $val) {
            if (!in_array($val, $check_manage_id_ed)) {
                if ($manage_id) {
                    return $val != $manage_id ? $val  : (isset($check_manage_id[$key + 1]) ? $check_manage_id[$key + 1] : null);
                } else {
                    return $val;
                };
            }
        }
        return null; //已审核完毕
    }
    /**
     * 添加审核记录
     * @param con_id  大赛id
     */
    public function addCheckLog($manage_id, $works_id, $status, $reason)
    {
        $this->works_id = $works_id;
        $this->check_manage_id = $manage_id;
        $this->status = $status;
        $this->reason = $status == 1 ? null : $reason;
        $this->save();
    }
}
