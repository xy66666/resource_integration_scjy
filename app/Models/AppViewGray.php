<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * 应用实现显示灰色
 */
class AppViewGray extends BaseModel
{

    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'app_view_gray';


    /**
     * 列表
     * @param page int 页码
     * @param limit int 分页大小
     * @param keywords string 搜索关键词
     * @param start_time datetime 开始时间    数据格式  年月日
     * @param end_time datetime 结束时间
     */
    public function lists($keywords, $start_time, $end_time, $limit)
    {

        $res = $this->select('id', 'name', 'start_time', 'end_time', 'manage_id', 'create_time')
            ->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('name', 'like', "%$keywords%");
                }
            })->where(function ($query) use ($start_time, $end_time) {
                if ($start_time && $end_time) {
                    $query->whereBetween('start_time', [$start_time, $end_time]);
                }
            })
            ->where('is_del', 1)
            ->orderByDesc('id')
            ->paginate($limit)
            ->toArray();

        return $res;
    }



    /**
     * 是否显示灰色
     */
    public function isViewGray()
    {
        $res = $this->select('id', 'name', 'start_time', 'end_time')
            ->where('start_time', '<=', date('Y-m-d H:i:s'))
            ->where('end_time', '>', date('Y-m-d H:i:s'))
            ->where('is_del', 1)
            ->orderByDesc('id')
            ->first();
        return  $res ? true : false;
    }
}
