<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * 活动答案表
 */
class AnswerActivityProblemAnswer extends BaseModel
{
    use HasFactory;


    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'answer_activity_problem_answer';



    /**
     *  新增答案
     * @param $answer 答案  json数据
     * @param $pro_id 标题id
     */
    public function add($answer, $pro_id = '', $type = '')
    {
        $data = [];
        $success = 0; //定义正确答案个数，必须是一个
        foreach ($answer as $key => $val) {
            $content = trim($val['content']);
            if(empty($content)){
                throw new Exception('答案不能为空');
            }
            if ($type == 2) {
                if (mb_strlen($content) > 30) {
                    throw new Exception('填空题答案字数不能超过30个字');
                }
            }

            $data[$key]['pro_id'] = $pro_id;
            $data[$key]['content'] = $content;
            $data[$key]['status'] = $type == 2 ? 1 : $val['status'];
            $data[$key]['create_time'] = date('Y-m-d H:i:s');
            if ($data[$key]['status'] == 1) {
                $success++;
            }
        }
        if ($success != 1) {
            throw new Exception('正确答案个数有且仅有一个，请检查当前题目的答案是否设置正确');
        }

        $this->insert($data);
        //获取正确答案的id
        $success_id = $this->where('pro_id', $pro_id)->where('status', 1)->value('id');

        return  $success_id;
    }

    /**
     * 修改答案  （不允许修改答案个数，只允许修改文字和正确答案）
     * @param $answer 答案  json数据
     * @param $pro_id 标题id
     */
    public function change($answer, $pro_id = '', $type = '')
    {
        $success = 0; //定义正确答案个数，必须是一个
        foreach ($answer as $key => $val) {
            $status = $type == 2 ? 1 : $val['status'];
            $content = trim($val['content']);
            if(empty($content)){
                throw new Exception('答案不能为空');
            }
            if ($type == 2) {
                if (mb_strlen($content) > 30) {
                    throw new Exception('填空题答案字数不能超过30个字');
                }
            }

            $this->where('id', $val['id'])->where('pro_id', $pro_id)->update([
                'content' => $content,
                'status' => $status,
                'create_time' => date('Y-m-d H:i:s'),
            ]);

            if ($status == 1) {
                $success++;
            }
        }
        if ($success != 1) {
            throw new Exception('正确答案个数有且仅有一个，请检查当前题目的答案是否设置正确');
        }

        //获取正确答案的id
        $success_id = $this->where('pro_id', $pro_id)->where('status', 1)->value('id');

        return  $success_id;
    }

    /**
     * 导入图片修改答案  （不允许修改答案个数，只允许修改文字和正确答案）
     * @param $answer 答案  json数据
     * @param $pro_id 标题id
     */
    public function importChange($answer, $pro_id = '', $type = '')
    {
        //获取所有答案
        $answer_all_array = $this->select('id', 'content')->where('pro_id', $pro_id)->get()->toArray();
        $answer_all = array_column($answer_all_array, 'id', 'content');
        $old_answer_content = array_column($answer_all_array, 'content');

        foreach ($answer as $key => $val) {
            $status = $type == 2 ? 1 : $val['status'];
            $content = trim($val['content']);
            if(empty($content)){
                throw new Exception('答案不能为空');
            }
            if ($type == 2) {
                if (mb_strlen($content) > 30) {
                    throw new Exception('填空题答案字数不能超过30个字');
                }
                //填空题直接改答案
                $this->where('pro_id', $pro_id)->update([
                    'content' => $content,
                    'status' => $status,
                    'create_time' => date('Y-m-d H:i:s'),
                ]);
            } else {
                if (in_array($val['content'], $old_answer_content)) {
                    unset($answer_all[$val['content']]);
                } else {
                    $obj = new self();
                    $obj->pro_id = $pro_id;
                    $obj->content = $content;
                    $obj->status = $status;
                    $obj->save();
                }
            }
        }
        if ($type == 1 && $answer_all) {
            $del_answer_id = array_column($answer_all, 'id');
            $this->whereIn('id', $del_answer_id)->delete();
        }

        //获取正确答案的id
        $success_id = $this->where('pro_id', $pro_id)->where('status', 1)->value('id');

        return  $success_id;
    }
}
