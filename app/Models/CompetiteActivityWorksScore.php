<?php

namespace App\Models;

use App\Http\Controllers\PdfController;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;

/**
 * 线上大赛作品审批打分模型
 * Class ArticleModel
 * @package app\common\model
 */
class CompetiteActivityWorksScore extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'competite_activity_works_score';

    /**
     * 获取作品各自的打分情况
     * @param works_id 作品id
     */
    public function getWorksScoreList($works_id)
    {
        $res = $this->select('works_id', 'score_manage_id', 'review_score', 'change_time')->where('works_id', $works_id)->get()->toArray();
        $works_score_string = '';
        foreach ($res as $key => $val) {
            $score = isset($val['review_score']) ? $val['review_score'] : '未打';
            $res[$key]['manage_name'] = Manage::getManageNameByManageId($val['score_manage_id']);
            $works_score_string .= '、' . $res[$key]['manage_name'] . ' (' . $score . '分)';
        }
        $works_score_string = trim($works_score_string, '、');
        return [$res, $works_score_string];
    }
}
