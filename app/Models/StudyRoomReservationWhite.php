<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * 空间预约预约白名单表
 * Class ReservationModel
 * @package app\common\model
 */
class StudyRoomReservationWhite extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    public $table = 'study_room_reservation_white';


    /** 
     * 白名单列表
     * @param page int 页码
     * @param limit int 分页大小
     * @param keywords string 搜索关键词
     * @param start_time date 搜索开始时间
     * @param end_time date 搜索结束时间
     */
    public function lists($field = null, $keywords = null, $start_time = null, $end_time = null, $limit = 10)
    {
        if (empty($field)) {
            $field = ['id', 'username', 'account', 'start_time', 'end_time', 'create_time'];
        }
        $res = $this->select($field)
            ->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('username', 'like', "%$keywords%")->orWhere('account', 'like', "%$keywords%");
                }
            })->where(function ($query) use ($start_time, $end_time) {
                if ($start_time && $end_time) {
                    $query->whereBetween('create_time', [$start_time, $end_time]);
                }
            })
            ->where('is_del', 1)
            ->orderByDesc('id')
            ->paginate($limit)
            ->toArray();

        return $res;
    }

    /**
     * 查询账号是否在白名单类
     * @param account 账号
     */
    public function getWhiteByAccount($account)
    {
        return $this->where('account', $account)->where('start_time', '<', date('Y-m-d H:i:s'))
            ->where('end_time', '>', date('Y-m-d H:i:s'))
            ->where('is_del', 1)
            ->first();
    }
}
