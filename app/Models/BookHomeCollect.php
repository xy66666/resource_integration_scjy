<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * 书籍收藏
 */
class BookHomeCollect extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = null;

    protected $table = 'book_home_collect';

    /**
     * 新书书籍收藏关联
     */
    public function newBookCollect()
    {
        return $this->belongsTo(ShopBook::class, 'book_id', 'id')
            ->select('id', 'book_name', 'author', 'press', 'pre_time', 'isbn', 'price', 'img', 'intro', 'shop_id');
    }
    /**
     * 馆藏书籍收藏关联
     */
    public function libBookCollect()
    {
        return $this->belongsTo(LibBook::class, 'book_id', 'id')
            ->select('id', 'book_name', 'author', 'press', 'pre_time', 'isbn', 'price', 'img', 'intro', 'metaid', 'metatable');
    }

    /**
     * 判断是否加入收藏
     * @param book_id  书籍id
     * @param user_id  书籍id
     * @param type  类型 1 新书   2 馆藏书
     */
    public static function isCollect($book_id, $user_id, $type)
    {
        return self::where('book_id', $book_id)
            ->where('user_id', $user_id)
            ->where('type', $type)
            ->first();
    }
}
