<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * 新闻收藏模型
 * Class ArticleTypeModel
 * @package app\common\model
 */
class NewsCollect extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'news_collect';

    /**
     * 判断新闻是否收藏
     * @param user_id 用户id 
     * @param news_id 新闻id 
     */
    public static function isCollect($news_id , $user_id){
        $res = self::where('news_id' , $news_id)->where('user_id' , $user_id)->value('id');
        return $res;
    }

      /**
     * 新闻收藏与取消收藏
     * @param token  用户token  用户筛选用户是否收藏  
     * @param id  新闻id 
     */
    public function collectAndCancel($news_id , $user_id){
        $is_collect = self::isCollect($news_id , $user_id);
        if($is_collect){
            $res = $this->where('id' , $is_collect)->delete();
            $msg = '取消收藏';
        }else{
            $this->user_id = $user_id;
            $this->news_id = $news_id;
            $res =$this->save();
            $msg = '收藏';
        }
        return [$res , $msg];
    }

}