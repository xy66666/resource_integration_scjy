<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class CompetiteActivityGroupLoginLog extends BaseModel
{
    use HasFactory;

    public $timestamps = FALSE;

    protected $table = 'competite_activity_group_login_log';

    protected $guarded = []; //$guarded属性里面的字段是不可以赋值，其他的所有属性都能被赋值,需要注意的是，fillable 与 guarded 只限制了 create 方法，而不会限制 save。

    /**
     * 添加登录日志
     */
    public function insertLoginLog($group_id, $login_ip, $is_success = 1, $is_lock = 1)
    {
        $this->group_id = $group_id;
        $this->login_time = date('Y-m-d H:i:s');
        $this->login_ip = $login_ip;
        $this->is_success = $is_success;
        $this->is_lock = $is_lock;
        $this->save();
    }

    /**
     * 是否已经锁定（10分钟内 连续输入错误5次锁定 10分钟）
     * @param group_id 管理员id
     */
    public function isLockEd($group_id)
    {
        $res = $this->where('group_id', $group_id)
            ->whereBetween('login_time', [date('Y-m-d H:i:s', strtotime('-10 min')), date('Y-m-d H:i:s')])
            ->orderByDesc('id')->value('is_lock');

        return $res == 2 ? true : false;
    }

    /**
     * 密码输入错误，判断是否该锁定账号（10分钟内 连续输入错误5次锁定 10分钟）
     * @param group_id 管理员id
     */
    public function isLock($group_id)
    {
        $res = $this->where('group_id', $group_id)
            ->select('is_success', 'is_lock')
            ->whereBetween('login_time', [date('Y-m-d H:i:s', strtotime('-10 min')), date('Y-m-d H:i:s')])
            ->orderByDesc('id')
            ->limit(4) //取4次，因为这次也是错误的
            ->get();

        if (count($res) < 4) {
            return false;
        }
        foreach ($res as $key => $val) {
            if ($val['is_success'] == 1) {
                return false; //不该锁定，有成功记录，不够5次
            }
            if ($val['is_lock'] == 2) {
                return false; //不该锁定，有锁定记录，不够5次
            }
        }
        return true;
    }
}
