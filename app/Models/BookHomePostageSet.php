<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;



/**
 * 邮费设置
 * Class PostageSetModel
 * @package app\common\model
 */
class BookHomePostageSet extends BaseModel
{

    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'book_home_postage_set';
  
    


}