<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * 文旅打卡作品图片模型
 * Class ArticleTypeModel
 * @package app\common\model
 */
class ScenicWorksImg extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'scenic_works_img';

    /**
     * 添加打卡图片
     * $content 打卡内容
     * $scenic_works_id 作品id
     */
    public function add($content, $scenic_works_id = '')
    {
        $data = [];
        foreach ($content as $key => $val) {
            if (empty($val['img']) || empty($val['width']) || empty($val['height'])) {
                throw new Exception('上传内容有误，请检查' , 2002);
            }
            $data[$key]['scenic_works_id'] = $scenic_works_id;
            $data[$key]['img'] = $val['img'];
            $data[$key]['thumb_img'] = str_replace('.', '_thumb.', $val['img']);
            $data[$key]['width'] = $val['width'];
            $data[$key]['height'] = $val['height'];
            $data[$key]['create_time'] = date('Y-m-d H:i:s');
        }
        return $this->insert($data);
    }

    
    /**
     * 添加打卡图片
     * $content 打卡内容
     * $scenic_works_id 作品id
     * $field ['scenic_id'=> $scenic_id,'user_id'=>$user_id]
     */
    public function change($content, $scenic_works_id = '' , $field =[])
    {
       //删除之前所有的图片作品
       $this->where('scenic_works_id' , $scenic_works_id)->delete();
       $this->add($content, $scenic_works_id);

    }

}
