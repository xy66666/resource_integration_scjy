<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * 用户获取抽奖机会记录表
 */

class AnswerActivityUserPrizeRecord extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'answer_activity_user_prize_record';

    /**
     * 添加钥匙获得记录
     * @param act_id 活动id
     * @param user_guid 用户guid
     * @param pattern 答题模式（获奖方式）  1  馆内答题形式    3 爬楼梯形式
     * @param answer_record_id 答题记录id
     */
    public function addUserPrizeRecord($act_id, $user_guid, $pattern, $answer_record_id)
    {
        $this->act_id = $act_id;
        $this->user_guid = $user_guid;
        $this->pattern = $pattern;
        $this->answer_record_id = $answer_record_id;
        return $this->save();
    }

 
}
