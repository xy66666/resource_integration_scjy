<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * 积分商城商品兑换记录模型
 * Class GoodsOrderModel
 * @package app\common\model
 */
class GoodsOrderRefund extends BaseModel
{

    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'goods_order_refund';






}