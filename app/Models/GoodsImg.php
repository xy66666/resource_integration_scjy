<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * 积分商城商品图片模型
 * Class GoodsImgModel
 * @package app\common\model
 */
class GoodsImg extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'goods_img';


    /**
     * 添加(修改)相应数据 ，删除多余的，增加新增的  （对于数据库设计第三范式，删除中间表记录）
     * @param goods_id 商品id
     * @param img_arr    条图片数组
     * @param $type 类型  add 新增，直接增加   change 需要查询
     */
    public function goodsImgChange($goods_id,$img_arr,$type = 'change'){
        $img_arr = empty($img_arr) ? [] : explode('|' , $img_arr);
        $img_arr = array_filter(array_unique($img_arr));

        if($type == 'add'){
            $img_arr_ed = [];
        }else{
            $img_arr_ed = $this->where('goods_id' , $goods_id)->groupBy('img')->pluck('img')->toArray();
        }
        $img_arr_del = array_diff($img_arr_ed,$img_arr); //对比返回在 $img_arr_ed 中但是不在 $img_arr 的数据删除
        $img_arr_add = array_diff($img_arr,$img_arr_ed); //对比返回在 $img_arr 中但是不在 $img_arr_ed 的数据添加
        if(!empty($img_arr_del)){
            $this->whereIn('img' , $img_arr_del)->where('goods_id' , $goods_id)->delete();
        } 
        $data = [];
        foreach($img_arr_add as $key=>$val){
            $data[$key]['goods_id'] = $goods_id; 
            $data[$key]['img'] = $val; 
            $data[$key]['thumb_img'] =  str_replace('.' , '_thumb.' , $val);
            $data[$key]['create_time'] = date('Y-m-d H:i:s'); 
        }
        if(!empty($data)) $this->insert($data);

        return $img_arr_del;//返回需要删除的图片
    }


}