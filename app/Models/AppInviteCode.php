<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * 应用邀请码
 */

class AppInviteCode extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'app_invite_code';

    /**
     * 验证码邀请码是否有效
     * @param invite_code 邀请码
     */
    public function isExistsInviteCode()
    {
        $data = $this->where('is_del', 1)->first();
        return $data;
    }


    /**
     * 验证码邀请码是否有效
     * @param invite_code 邀请码
     */
    public function checkInviteCode($invite_code)
    {
        $data = $this->where('content', $invite_code)->where('is_del', 1)->first();
        if (empty($data)) {
            return '邀请码错误，请重新输入';
        }
        if (!empty($data['start_time']) && $data['start_time'] > date('Y-m-d H:i:s')) {
            return '邀请码错误，请重新输入';
        }
        if (!empty($data['end_time']) && $data['end_time'] < date('Y-m-d H:i:s')) {
            return '邀请码已过期，请重新输入';
        }
        return $data->id;
    }
}
