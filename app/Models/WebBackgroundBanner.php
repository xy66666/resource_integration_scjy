<?php
namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * 官网首页背景banner管理
 */
class WebBackgroundBanner extends BaseModel
{
    use HasFactory;


  //  public $incrementing = false;//主键不自增

    const CREATED_AT='create_time';
    const UPDATED_AT='change_time';

    
    protected $table = 'web_background_banner';

 


    
}