<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * 视频类作品
 */
class Video extends BaseModel
{
    use HasFactory;


    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'video';


    /**
     * 关联作者名
     */
    public function author()
    {
        return $this->belongsTo('App\Models\Authors', 'author_id');
    }
    /**
     * 关联类型名
     */
    public function typeName()
    {
        return $this->belongsTo('App\Models\ProductionType', 'type_id');
    }
    /**
     * 管理员名
     */
    public function manageInfo()
    {
        return $this->belongsTo('App\Models\Manage', 'manage_id');
    }

    /**
     * 修改作品名称，加上书名号
     */
    public function getVideoNameAttribute($video_name)
    {
        if (strpos($video_name, '《') === false || strpos($video_name, '》') === false) {
            return '《' . $video_name . '》';
        }
        return $video_name;
    }

    /**
     * 根据所有的作品视频id  获取所有的作品信息 (作者及类型信息)
     * @param production_ids
     */
    public function getVideo($video_ids)
    {
        return $this->whereIn('id', $video_ids)->with(['author' => function ($query) {
            $query->select('id', 'username', 'username_en', 'birthday', 'sex', 'intro', 'intro_en');
        }, 'typeName' => function ($query) {
            $query->select('id', 'name', 'intro');
        }])->where('is_del', 1)->get();
    }


    /**
     * 作品添加
     * @param $data 添加的数据
     */
    public function add($data, $field = [])
    {
        $this->video_name = $data->video_name;
        $this->video_name_en = $data->video_name_en;
        $this->year  = $data->year;
        $this->author_id = $data->author_id;
        $this->type_id  = $data->type_id;
        $this->intro  = $data->intro;
        $this->intro_en  = $data->intro_en;
        $this->img  = $data->img;
        $this->img_thumb = str_replace('.', '_thumb.', $data->img);
        $this->video_addr = $data->video_addr;
        $this->is_del = 1;
        $this->manage_id = $data->manage_id;

        return  $this->save();
    }

    /**
     *  作品修改
     * @param $data 添加的数据
     */
    public function change($data, $field = [], $findWhere = [])
    {
        $res = $this->where('is_del', 1)->find($data['id']);
        if (!$res) {
            return false;
        }

        $res->video_name = $data->video_name;
        $res->video_name_en = $data->video_name_en;
        $res->year  = $data->year;
        $res->author_id = $data->author_id;
        $res->type_id  = $data->type_id;
        $res->intro  = $data->intro;
        $res->intro_en  = $data->intro_en;
        $res->img  = $data->img;
        $res->img_thumb = str_replace('.', '_thumb.', $data->img);
        $res->video_addr = $data->video_addr;

        return  $res->save();
    }


    /**
     * 判断是否已经存在
     * @param video_name 作品名称
     * @param id 作品id   可选，主要是用于修改
     */
    public function videoNameIsExists($name, $id = null)
    {
        $res = $this->where('video_name', $name)->where(function ($query) use ($id) {
            if (!empty($id)) {
                $query->where('id', '<>', $id);
            }
        })->where('is_del', 1)->first();
        return $res;
    }
}
