<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;



/**
 * 图书到家书籍归还记录
 * Class PostalReturnModel
 * @package app\common\model
 */
class BookHomeReturnRecord extends BaseModel
{

    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'book_home_return_record';

 


}