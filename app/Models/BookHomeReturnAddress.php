<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Log;

/**
 * 图书到家归还地址管理
 * Class PostalReturnModel
 * @package app\common\model
 */
class BookHomeReturnAddress extends BaseModel
{

    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'book_home_return_address';

    /**
     * 获取地址说明
     */
    public function addressList()
    {
        $res = $this
            ->select('id', 'type', 'way', 'username', 'tel', 'province', 'city', 'district', 'street', 'address', 'intro', 'dispark_time', 'create_time')
            ->whereIn('way', [1, 2])
            ->where('is_del', 1)
            ->get();

        $data['scene_new'] = []; //现场归还新书
        $data['postal_new'] = []; //邮递归还新书
        $data['scene_lib'] = []; //现场归还馆藏书
        $data['postal_lib'] = []; //邮递归还馆藏书
        foreach ($res as $key => &$val) {
            if ($val['type'] == 2) {
                if ($val['way'] == 1) {
                    $data['scene_lib'][] = $val;
                } elseif ($val['way'] == 2) {
                    $data['postal_lib'][] = $val;
                } else {
                    $data['scene_lib'][] = $val;
                    $data['postal_lib'][] = $val;
                }
            } else {
                if ($val['way'] == 1) {
                    $data['scene_new'][] = $val;
                } elseif ($val['way'] == 2) {
                    $data['postal_new'][] = $val;
                } else {
                    $data['scene_new'][] = $val;
                    $data['postal_new'][] = $val;
                }
            }
            unset($val['type']);
            unset($val['way']);
        }
        return $data;
    }

    /**
     * 获取地址说明
     * @param keywords 检索
     * @param way 方式  1 现场归还   2 邮递归还
     * @param type 方式  类型 1 新书（默认）   2 馆藏书
     */
    public function lists($keywords, $way, $type, $limit = 10)
    {
        $res = $this
            ->select('id', 'type', 'way', 'username', 'tel', 'province', 'city', 'district', 'street', 'address', 'intro', 'dispark_time', 'create_time')
            ->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->orwhere('username', 'like', '%' . $keywords . '%')->orwhere('tel', 'like', '%' . $keywords . '%');
                }
            })
            ->where(function ($query) use ($way, $type) {
                if ($way) {
                    $way = !is_array($way) ? explode(',', $way) : '';
                    $query->whereIn('way', $way);
                } else {
                    $query->whereIn('way', [1, 2]);
                }

                if ($type) {
                    $query->where('type', $type);
                }
            })
            ->where('is_del', 1)
            ->paginate($limit)
            ->toArray();
        return $res;
    }

    /**
     * 获取地址说明详情
     * @param id 记录id
     */
    public function detail($id)
    {
        $res = $this
            ->select('id', 'type', 'way', 'username', 'tel', 'province', 'city', 'district', 'street', 'address', 'intro', 'dispark_time', 'create_time')
            ->where('id', $id)
            ->where('is_del', 1)
            ->first();
        return $res;
    }


    /**
     * 获取发货地址
     * @param type 类型 1 新书（默认）   2 馆藏书
     */
    public function getDeliverAddress($type, $shop_id = null)
    {
        if ($type == 1 && empty($shop_id)) {
            return false;
        }
        $res = $this->select('id', 'username', 'tel', 'province', 'city', 'district', 'street', 'address')
            ->where('type', $type)
            ->where(function ($query) use ($type, $shop_id) {
                if ($type == 1) {
                    $query->where('shop_id', $shop_id);
                }
            })
            ->where('way', 3) //  2 发货地址
            ->where('is_del', 1)
            ->first();
        return $res;
    }


    /**
     * 书店、图书馆发货地址设置
     * @param shop_id  书店id  书店设置必须，图书馆设置不需要
     * @param username  发货人名称
     * @param tel  发货人联系电话号码
     * @param province 省
     * @param city 市
     * @param district 区
     * @param street 街道
     * @param address 详细地址
     */
    public function setDeliverAddress($data)
    {
        $shop_id = 0;
        if ($data['type'] == 1) {
            $shop_id = $data['shop_id'];
        }
        $postal_address_info = $this->getDeliverAddress($data['type'], $shop_id); //书店邮递地址
        if (empty($postal_address_info)) {
            $data['way'] = 3; //发货地址   式  1 现场归还   2 邮递归还   3 发货地址（只能有一个）
            $res = $this->add($data);
            $msg = '添加';
        } else {
            $postal_address_info->username = $data['username'];
            $postal_address_info->tel = $data['tel'];
            $postal_address_info->province = $data['province'];
            $postal_address_info->city = $data['city'];
            $postal_address_info->district = $data['district'];
            $postal_address_info->street = $data['street'];
            $postal_address_info->address = $data['address'];
            $res = $postal_address_info->save();
            $msg = '修改';
        }

        return [$res, $msg];
    }
}
