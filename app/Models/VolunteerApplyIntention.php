<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * 志愿者服务意向表
 */
class VolunteerApplyIntention extends BaseModel
{

    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'volunteer_apply_intention';



    /**
     * 根据服务意向id获取，瞒住条件的志愿者id
     * @param $intention_id
     */
    public function getVolunteerIdByIntentionId($intention_id){
        if(empty($intention_id)){
            return [];
        }
        $intention_id = !is_array($intention_id) ? explode(',' , $intention_id) : $intention_id;
        return $this->whereIn('intention_id' , $intention_id)->pluck('volunteer_id')->toArray();
    }



    /**
     * 添加(修改)相应数据 ，删除多余的，增加新增的  （对于数据库设计第三范式，删除中间表记录）
     * @param by_id_name 条件字段名
     * @param by_id_value 条件字段值
     * @param change_id_name 需要修改的字段名
     * @param change_id_value 需要修改的字段值，多个 逗号 拼接
     * @param $type 类型  add 新增，直接增加   change 需要查询
     */
    public function interTableChange($by_id_name, $by_id_value, $change_id_name, $change_id_value, $type = 'change')
    {
        $change_id_value = empty($change_id_value) ? [] : explode(',', $change_id_value);
        $change_id_value = array_filter(array_unique($change_id_value));
        if ($type == 'add') {
            $change_id_value_ed = [];
        } else {
            $change_id_value_ed = $this->where($by_id_name, $by_id_value)->groupBy($change_id_name)->pluck($change_id_name)->toArray();
        }
        $change_id_value_del = array_diff($change_id_value_ed, $change_id_value); //对比返回在 $change_id_value_ed 中但是不在 $change_id_value 的数据删除
        $change_id_value_add = array_diff($change_id_value, $change_id_value_ed); //对比返回在 $change_id_value 中但是不在 $change_id_value_ed 的数据添加
        if (!empty($change_id_value_del)) $this->whereIn($change_id_name, $change_id_value_del)->where($by_id_name, $by_id_value)->delete();
        $data = [];
        foreach ($change_id_value_add as $key => $val) {
            $data[$key][$by_id_name] = $by_id_value;
            $data[$key][$change_id_name] = $val;
            $data[$key]['create_time'] = date('Y-m-d H:i:s');
        }
        if (!empty($data)) $this->insert($data);
    }
   

}
