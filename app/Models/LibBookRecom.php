<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;

/**
 * 馆藏推荐书籍表
 * Class LibBookBarcodeModel
 * @package app\common\model
 */
class LibBookRecom extends BaseModel
{

    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'lib_book_recom';


    /**
     * 关联类型
     */
    public function conType()
    {
        return $this->hasOne(LibBookType::class, 'id', 'type_id');
    }
    /**
     * 关联收藏
     */
    public function conCollect()
    {
        return $this->hasMany(BookCollectModel::class, 'book_id', 'id');
    }

    /**
     * 馆藏书推荐列表
     * @param keywords_type  检索条件   0、全部  1、书名  2、作者 3 ISBN  4.索书号 默认 0
     * @param keywords  检索条件
     * @param type_id   空字符串、代表全部    不能用  0 ，因为 0 表示 其他类型
     * @param is_recom  是否为推荐图书  0、全部 1 是 2 否   默认 0
     * @param page  页数，默认为1，
     * @param limit  条数，默认显示 10条
     */
    public function lists($field, $type_id = null, $keywords = null, $keywords_type = null, $is_recom = null, $limit = 10)
    {
        $type_id = is_null($type_id) ? '' : $type_id;

        if (empty($field)) {
            $field = ['id', 'metaid', 'metatable', 'book_name', 'barcode', 'author', 'press', 'pre_time', 'type_id', 'isbn', 'book_num', 'price', 'intro', 'img', 'is_recom', 'create_time'];
        }
        $res = $this
            ->select($field)
            ->with(['conType' => function ($query) {
                $query->where('is_del', 1);
            }])
            ->where(function ($query) use ($keywords, $keywords_type) {
                if (!empty($keywords)) {
                    if (empty($keywords_type) || $keywords_type == 'all') {
                        $query->where('book_name', 'like', "%$keywords%")
                            ->orWhere('author', 'like', "%$keywords%")
                            ->orWhere('barcode', 'like', "%$keywords%")
                            ->orWhere('isbn', 'like', "%$keywords%");
                    } else {
                        switch ($keywords_type) {
                            case 1:
                            case 'title':
                                $query->where('book_name', 'like', "%$keywords%");
                                break;
                            case 2:
                            case 'author':
                                $query->where('author', 'like', "%$keywords%");
                                break;
                            case 3:
                            case 'isbn':
                                $query->where('isbn', 'like', "%$keywords%");
                                break;
                            case 4:
                            case 'callno':
                                $query->where('callno', 'like', "%$keywords%");
                                break;
                        }
                    }
                }
            })
            ->where(function ($query) use ($type_id, $is_recom) {
                if ($type_id !== '') {
                    $query->where('type_id', $type_id);
                }
                if (!empty($is_recom)) {
                    $query->where('is_recom', $is_recom);
                }
            })
            ->where('is_del', 1)
            ->orderByDesc('id')
            ->paginate($limit)
            ->toArray();
        return $res;
    }

    /**
     * 馆藏书热门借阅列表
     * @param limit 需要返回的数据条数
     */
    public function hotBorrow($limit = 6)
    {
        //获取借阅次数最多的条形码
        $borrow_barcode_isbn_arr = BookHomePurchase::select(DB::raw("count(`id`) as number"), 'barcode', 'isbn')
            ->where('type', 2)
            ->groupBy('barcode')
            ->orderByDesc('number')
            ->limit($limit)
            ->get()
            ->toArray();
        $borrow_barcode_arr = array_column($borrow_barcode_isbn_arr, 'barcode');
        $borrow_isbn_arr = array_column($borrow_barcode_isbn_arr, 'isbn');
        $libBookModel = new LibBook();
        $hot_book_info = $libBookModel->from('lib_book as l')->select('l.id', 'metaid', 'metatable', 'l.img', 'l.book_name', 'l.author', 'b.barcode', 'isbn', 'access_number')
            ->join('lib_book_barcode as b', 'b.book_id', '=', 'l.id')
            ->whereIn('l.isbn', $borrow_isbn_arr)
            ->groupBy('book_name')
            ->get()
            ->toArray();
        $count = count($hot_book_info);
        if ($count < $limit) {
            $rand_book_info = $libBookModel->from('lib_book as l')->select('l.id', 'metaid', 'metatable', 'l.img', 'l.book_name', 'l.author', 'b.barcode', 'isbn', 'access_number')
                ->join('lib_book_barcode as b', 'b.book_id', '=', 'l.id')
                ->whereNotIn('b.barcode', $borrow_barcode_arr)
                ->whereNotIn('l.isbn', $borrow_isbn_arr)
                ->groupBy('book_name')
                ->limit($limit - $count)
                ->get()
                ->toArray();
            $hot_book_info = $hot_book_info ? array_merge($hot_book_info, $rand_book_info) : $rand_book_info;
        }
        foreach ($hot_book_info as $key => $val) {
            if ($val['access_number'] <= 5 && ($val['img'] == 'default/default_lib_book.png' || $val['img'] == '/default/default_lib_book.png')) {
                $img = LibBookRecom::where('isbn', $val['isbn'])->value('img');
                if ($img) {
                    $controllerObj = new Controller();
                    $img = $controllerObj->getImgByIsbn($val['isbn']);
                }
                if ($img && $img != 'default/default_lib_book.png' && $img != '/default/default_lib_book.png') {
                    $libBookModel->where('id', $val['id'])->update(['img' => $img, 'access_number' => $val['access_number'] + 1]);
                } else {
                    $libBookModel->where('id', $val['id'])->update(['access_number' => $val['access_number'] + 1]);
                }
            }
        }


        return $hot_book_info;
    }


    /**
     * 馆藏书推荐详情
     * @param keywords_type  检索条件   0、全部  1、书名  2、作者 3 ISBN  4.条形码 默认 0
     * @param keywords  检索条件
     * @param type_id   空字符串、代表全部    不能用  0 ，因为 0 表示 其他类型
     * @param is_recom  是否为推荐图书  0、全部 1 是 2 否   默认 0
     * @param page  页数，默认为1，
     * @param limit  条数，默认显示 10条
     */
    public function detail($id)
    {

        $res = $this
            ->select('id', 'metaid', 'metatable', 'book_name', 'barcode', 'author', 'press', 'pre_time', 'type_id', 'isbn', 'book_num', 'price', 'intro', 'img', 'is_recom', 'create_time')
            ->with('conType')
            ->where('is_del', 1)
            ->where('id', $id)
            ->first();
        return $res;
    }
}
