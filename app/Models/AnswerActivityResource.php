<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/*活动资源model*/

class AnswerActivityResource extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'answer_activity_resource';


    /**
     * 获取资源图片模板
     */
    public static function getResourceTemplateParam()
    {
        return [
            ['id' => 1, 'field' => 'default_activity_resource', 'value' => '通用模板'],
            ['id' => 2, 'field' => 'default_activity_resource11', 'value' => '元旦节'],
            ['id' => 3, 'field' => 'default_activity_resource423', 'value' => '世界读书日'],
            ['id' => 4, 'field' => 'default_activity_resource51', 'value' => '劳动节'],
            ['id' => 5, 'field' => 'default_activity_resource55', 'value' => '端午节'],
            ['id' => 6, 'field' => 'default_activity_resource815', 'value' => '中秋节'],
            ['id' => 7, 'field' => 'default_activity_resource101', 'value' => '国庆节'],
        ];
    }


    /**
     * 获取默认活动资源路径
     * @param act_id
     * 
     *   //node   活动类型  1 独立活动  2 单位联盟   3 区域联盟   
     *   //pattern   答题模式  1  馆内答题形式  2 轮次排名形式 （按轮次排名）  3 爬楼梯形式
     *   //prize_form   获奖情况  1 排名  2 抽奖
     */
    public function getDefaultResourceAddr($act_id)
    {
        $res = AnswerActivity::select('id', 'node', 'pattern', 'prize_form')->where('id', $act_id)->where('is_del', 1)->first();

        if (empty($res)) {
            return '';
        }
        if ($res['node'] == 1) {
            if ($res['pattern'] == 1) {
                if ($res['prize_form'] == 1) {
                    $addr = 'one_lib_rank_default.zip';
                } else {
                    $addr = 'one_lib_draw_default.zip';
                }
            } else if ($res['pattern'] == 2) {
                $addr = 'one_turn_rank_default.zip';
            } else {
                if ($res['prize_form'] == 1) {
                    $addr = 'one_stairs_rank_default.zip';
                } else {
                    $addr = 'one_stairs_draw_default.zip';
                }
            }
        } else if ($res['node'] == 2) {
            if ($res['pattern'] == 1) {
                if ($res['prize_form'] == 1) {
                    $addr = 'unit_lib_rank_default.zip';
                } else {
                    $addr = 'unit_lib_draw_default.zip';
                }
            } else if ($res['pattern'] == 2) {
                $addr = 'unit_turn_rank_default.zip';
            } else {
                if ($res['prize_form'] == 1) {
                    $addr = 'unit_stairs_rank_default.zip';
                } else {
                    $addr = 'unit_stairs_draw_default.zip';
                }
            }
        } else {
            if ($res['pattern'] == 1) {
                if ($res['prize_form'] == 1) {
                    $addr = 'area_lib_rank_default.zip';
                } else {
                    $addr = 'area_lib_draw_default.zip';
                }
            } else if ($res['pattern'] == 2) {
                $addr = 'area_turn_rank_default.zip';
            } else {
                if ($res['prize_form'] == 1) {
                    $addr = 'area_stairs_rank_default.zip';
                } else {
                    $addr = 'area_stairs_draw_default.zip';
                }
            }
        }
        // return '/default_activity_resource/'.$addr;
        return $addr;
    }

    /**
     * 获取资源文件夹
     * @param act_id 活动id
     */
    public function getImgStoreDir($act_id)
    {
        $resource_path = $this->where('act_id', $act_id)->value('resource_path');
        if ($resource_path) {
            return public_path('uploads') . '/' . $resource_path;
        }

        $dir_name = sprintf("%06d", $act_id);
        $dir_name = $dir_name . '00001';
        $dir_addr = public_path('uploads') . '/answer_activity/ui_resource/' . $dir_name;
        return $dir_addr;
    }
    /**
     * 升级资源文件夹名称
     * @param act_id 活动id
     */
    public function getNewImgStoreName($act_id)
    {
        $resource_path = $this->where('act_id', $act_id)->value('resource_path');
        $resource_path = explode('/', $resource_path);
        $resource_path = end($resource_path);
        if ($resource_path) {
            $front = substr($resource_path, 0, 6);
            $postfix = substr($resource_path, 6);

            $postfix = (int)$postfix + 1;
            $postfix = sprintf("%05d", $postfix);

            return  'answer_activity/ui_resource/' . $front . $postfix;
        }

        $dir_name = sprintf("%06d", $act_id);
        $dir_name = $dir_name . '00001';
        return 'answer_activity/ui_resource/' . $dir_name;
    }


    /**
     * 生成存放图片路径
     * @param act_id 活动id
     */
    public function createImgStoreDir($act_id)
    {
        $dir_addr = $this->getImgStoreDir($act_id);
        $res = true;
        if (!file_exists($dir_addr)) {
            $res = mkdir($dir_addr, 0777, true);
        }
        if (!$res) {
            throw new Exception('活动资源存放文件夹创建失败，请联系管理员处理！');
        }
    }

    /**
     * 获取基本信息
     * @param act_id 活动id
     */
    public function detail($act_id)
    {
        return $this->where('act_id', $act_id)->first();
    }

    /**
     * 修改资源文件
     * @parma $data 数据资源
     */
    public function change($data, $field = [], $findWhere = [])
    {
        $res = $this->detail($data['act_id']);
        if (empty($res)) {
            $res = $this;
        }
        $res->act_id = $data['act_id'];
        $res->theme_color = $data['theme_color'];
        $res->progress_color = $data['progress_color'];
        $res->invite_color = $data['invite_color'];
        $res->theme_text_color = $data['theme_text_color'];
        $res->warning_color = $data['warning_color'];
        $res->error_color = $data['error_color'];
        $res->is_change = 2;
        return $res->save();
    }
}
