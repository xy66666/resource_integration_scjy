<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * 新书速递模型
 */
class NewBookRecommend extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = null;

    protected $table = 'new_book_recommend';


    /**
     * 关联商品类型
     */
    public function conType()
    {
        return $this->hasOne(NewBookRecommendType::class, 'id', 'type_id');
    }

    /**
     * 推荐列表
     * @param limit int 分页大小
     * @param type_id int 书籍类型(不传查询所有)
     */
    public function recomLists($type_id = null, $limit = 10)
    {

        $res = $this->select('id', 'img', 'book_name', 'author', 'type_id', 'isbn', 'price',  'press', 'pre_time', 'intro', 'qr_url', 'create_time')
            ->with('conType')
            ->where(function ($query) use ($type_id) {
                if ($type_id) {
                    $query->where('type_id', $type_id);
                }
            })
            ->where('is_del', 1)
            ->orderBy('is_recom')
            ->orderByDesc('id')
            ->limit($limit)
            ->get()
            ->toArray();

        foreach ($res as $key => $val) {
            $res[$key]['intro'] = str_replace('&nbsp;', '', strip_tags($val['intro']));
        }
        return $res;
    }

    /**
     * 列表
     * @param page int 当前页
     * @param limit int 分页大小
     * @param keywords string 搜索关键词
     * @param keywords_type string 搜索关键词类型 0或空表示全部  1 书名 2 作者名 3 ISBN号
     * @param type_id int 书籍类型(不传查询所有)
     * @param is_recom int 是否推荐 1是 2否
     */
    public function lists($type_id = null, $keywords = null, $keywords_type = null, $is_recom = null, $limit = 10)
    {

        $res = $this->select('id', 'img', 'book_name', 'author', 'type_id', 'isbn', 'price', 'intro', 'press', 'pre_time', 'is_recom', 'qr_url', 'create_time')
            ->with('conType')
            ->where(function ($query) use ($keywords, $keywords_type) {
                if ($keywords) {
                    if ($keywords_type) {
                        switch ($keywords_type) {
                            case 1:
                                $query->where('book_name', 'like', "%$keywords%");
                                break;
                            case 2:
                                $query->where('author', 'like', "%$keywords%");
                                break;
                            case 3:
                                $query->where('isbn', 'like', "%$keywords%");
                                break;
                        }
                    } else {
                        $query->orWhere('book_name', 'like', "%$keywords%")
                            ->orWhere('author', 'like', "%$keywords%")
                            ->orWhere('isbn', 'like', "%$keywords%");
                    }
                }
            })->where(function ($query) use ($type_id, $is_recom) {
                if ($type_id) {
                    $query->where('type_id', $type_id);
                }
                if ($is_recom) {
                    $query->where('is_recom', $is_recom);
                }
            })
            ->where('is_del', 1)
            ->orderByDesc('id')
            ->paginate($limit)
            ->toArray();
        return $res;
    }
}
