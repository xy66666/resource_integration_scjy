<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * 活动邀请码
 */
class AnswerActivityInvite extends BaseModel
{
    use HasFactory;


    const CREATED_AT = 'create_time';
    const UPDATED_AT = null;


    protected $table = 'answer_activity_invite';

    /**
     * 判断用户是否输入邀请码 且 邀请码是否正确
     * @param user_guid    用户guid
     * @param act_id       活动id
     * @param act_type_id  活动类型id
     * @param invite_code  邀请码
     */
    public function checkInviteCode($user_guid, $act_id, $invite_code)
    {
        $res = $this->select('invite_code')
            ->where('user_guid', $user_guid)
            ->where('act_id', $act_id)
            ->orderByDesc('id')
            ->first();

        if (empty($res)) {
            return false;
        }
        if ($res['invite_code'] != $invite_code) {
            return false;
        }
        return true;
    }
}
