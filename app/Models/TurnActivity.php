<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

/*在线抽奖活动model*/

class TurnActivity extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'turn_activity';

    protected $guarded = []; //使用模型  update 更新时，允许被全部更新，若不写，则会报错 或 没任何数据变化






    /**
     * 更新浏览量
     *
     * @param act_id 活动id  
     * @param number 多少次更新一次
     * @param second 大于秒更新一次（取决于后一次请求事件）
     */
    public function updateBrowseNumber($act_id, $number = 100, $second = 300)
    {
        if ($number == 1) {
            return $this->where('act_id', $act_id)->increment('browse_num', 1);
        }

        $key = $act_id . 'turn_activity_browse_number';
        $res = Cache::pull($key); //从缓存中获取缓存项然后删除，使用 pull 方法，如果缓存项不存在的话返回 null
        $data = [];
        if (empty($res)) {
            $data['number'] = 1;
            $data['create_time'] = time();
            Cache::put($key, $data, 3600 * 24);
        } else {
            //数据大于50或者时间超过5分钟，则更新一次数据
            if ($res['number'] >= $number || time() - $res['create_time'] >= $second) {
                return $this->where('id', $act_id)->increment('browse_num', $res['number'] + 1);
            } else {
                $data['number'] = $res['number'] + 1;
                $data['create_time'] = $res['create_time'];
                Cache::put($key, $data, 3600 * 24);
            }
        }
        return true;
    }


    /**
     * 获取活动列表
     * @param field int 需要获取的字段 数组形式
     * @param page int 页码
     * @param limit int 分页大小
     * @param is_play string 是否发布   1 已发布  2 未发布
     * @param keywords string 搜索关键词
     * @param start_time datetime 活动开始时间    数据格式  年月日
     * @param end_time datetime 活动结束时间
     * 
     * @param create_start_time datetime 活动创建开始时间   数据格式 年月日时分秒
     * @param create_end_time datetime 活动创建结束时间
     */
    public function lists($field = [], $keywords = null, $is_play = null, $start_time = null, $end_time = null, $create_start_time = null, $create_end_time = null, $page = 1, $limit = 10)
    {
        if (empty($field)) {
            $field = [
                'id', 'title', 'img', 'node', 'execute_day', 'number', 'round_number', 'execute_day', 'real_info',
                'share_number', 'rule_type', 'rule', 'start_time', 'end_time', 'end_time', 'turn_start_time',
                'turn_end_time', 'vote_way', 'invite_code', 'is_play', 'create_time', 'manage_id', 'browse_num'
            ];
        }
        $res = $this->select($field)
            ->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('title', 'like', '%' . $keywords . '%');
                }
            })->where(function ($query) use ($is_play, $start_time, $end_time, $create_start_time, $create_end_time) {
                if ($is_play) {
                    $query->where('is_play', $is_play);
                }
                if ($start_time && $end_time) {
                    $query->whereBetween('start_time', [$start_time, $end_time]);
                }
                if ($create_start_time && $create_end_time) {
                    $query->whereBetween('create_time', [$create_start_time, $create_end_time]);
                }
            })->where('is_del', 1)
            ->orderByDesc('start_time')
            ->paginate($limit)
            ->toArray();

        $controllerObj = new Controller();
        $res = $controllerObj->disPageData($res);

        return $res;
    }

    /**
     * 获取活动详情
     * @param $id
     */
    public function detail($id, $field = null, $is_play = 1)
    {
        if (empty($field)) {
            $field = [
                'id', 'title', 'img', 'node', 'execute_day', 'number', 'round_number', 'execute_day', 'real_info',
                'share_number', 'rule_type', 'rule', 'start_time', 'end_time', 'end_time', 'turn_start_time',
                'turn_end_time', 'vote_way', 'invite_code', 'is_play', 'create_time', 'manage_id', 'share_title','is_show_address',
                'technical_support'
            ];
        }

        $act_info = $this->select($field)
            ->where('id', $id)
            ->where('is_del', 1)
            ->where(function ($query) use ($is_play) {
                if ($is_play) {
                    $query->where('is_play', $is_play);
                }
            })
            ->first();

        return $act_info;
    }


    /**
     * 用户活动报名填写信息
     */
    public static function getAnswerActivityApplyParam()
    {
        return [
            ['id' => 1, 'field' => 'username', 'value' => '姓名'],
            ['id' => 2, 'field' => 'tel', 'value' => '电话号码'],
            ['id' => 3, 'field' => 'id_card', 'value' => '身份证号码'],
            ['id' => 4, 'field' => 'reader_id', 'value' => '读者证号'],
        ];
    }

    /**
     * 获取活动执行天数
     */
    public function getActExecuteDay($act_data)
    {
        $execute_day = explode(',', $act_data['execute_day']);
        $execute_day_arr = [];
        if ($act_data['node'] == 2) {
            foreach ($execute_day as $key => $val) {
                $execute_day_arr[] = number_to_week($val);
            }
        } elseif ($act_data['node'] == 3) {
            foreach ($execute_day as $key => $val) {
                $execute_day_arr[] = $val . '号';
            }
        }
        return $execute_day_arr;
    }


    /**
     * 活动列表状态
     * status 活动状态 1 活动未开始 2.抽奖未开始 3.进行中 4.抽奖已结束  5 活动已结束 
     **/
    public function getActListStatus($act_data)
    {
        $now_date = date('Y-m-d H:i:s');
        if ($act_data['start_time'] > $now_date) {
            $status = 1;
        } elseif ($act_data['end_time'] < $now_date) {
            $status = 5;
        } elseif ($act_data['start_time'] < $now_date && $act_data['turn_start_time'] > $now_date) {
            $status = 2;
        } elseif ($act_data['turn_start_time'] < $now_date && $act_data['turn_end_time'] > $now_date) {
            $status = 3;
        } else {
            $status = 4;
        }
        return $status;
    }
    /**
     * 判断是否还可以抽奖
     */
    public function isCanDraw($act_info)
    {
        $status = $this->getActListStatus($act_info);

        switch ($status) {
            case 1:
                return '活动未开始';
                break;
            case 2:
                return '抽奖未开始';
                break;
            case 4:
                return '抽奖已结束';
                break;
            case 5:
                return '活动已结束';
                break;
        }

        return true;
    }

    /**
     * 判断是否是最后一天,提示不同的话
     * $answer_end_time  答题结束时间
     */
    public function isEndTime($answer_end_time)
    {
        if (date('Y-m-d', strtotime($answer_end_time)) == date('Y-m-d')) {
            return '感谢您的参与';
        }

        return '请明日再来！';
    }


    /**
     * @param array $real_info
     * @param $username
     * @param $id_card
     * @param $tel
     * @param $reader_id
     * @param $img
     * @return array
     */
    public function checkApplyParam(array $real_info, $username, $id_card, $tel, $reader_id, $img)
    {
        $where = [];
        foreach ($real_info as $key => $val) {
            if ($val == 1) {
                if (empty($username)) {
                    throw new Exception('用户名不能为空');
                } else {
                    $where['username'] = $username;
                }
            } elseif ($val == 2) {
                if (empty($id_card)) {
                    throw new Exception('用户名不能为空');
                } else {
                    $where['id_card'] = $id_card;
                }
            } elseif ($val == 3) {
                if (empty($tel)) {
                    throw new Exception('电话号码不能为空');
                } else {
                    $where['tel'] = $tel;
                }
            } elseif ($val == 4) {
                if (empty($reader_id)) {
                    throw new Exception('读者证不能为空');
                } else {
                    $where['reader_id'] = $reader_id;
                }
            } elseif ($val == 5) {
                if (empty($img)) {
                    throw new Exception('头像不能为空');
                } else {
                    $where['img'] = $img;
                }
            }
        }
        return $where;
    }


    /**
     * 是否满足发布的条件
     * @param act_id 活动id
     */
    public function isCanPlay($act_id)
    {
        //  $act_info = $this->where('id', $act_id)->first();
        //判断是否配置颜色资源
        $turnActivityResource = new TurnActivityResource();
        $color_info = $turnActivityResource->detail($act_id);

        if (empty($color_info) || empty($color_info['theme_color'])) {
            return '活动主题颜色未设置，请先设置';
        } elseif (empty($color_info['resource_path'])) {
            return '活动图片资源未设置，请先设置';
        }

        //判断是否配置奖品
        $gift_info = TurnActivityGift::isHaveGift($act_id);
        if ($gift_info !== true) {
            return $gift_info;
        }

        return true;
    }
}
