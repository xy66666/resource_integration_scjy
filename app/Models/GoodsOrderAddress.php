<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * 积分商城用户收货地址模型
 * Class GoodsTypeModel
 * @package app\common\model
 */
class GoodsOrderAddress extends BaseModel
{

    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'goods_order_address';


    /**
     * 发货修改订单地址信息
     * @param  $id 兑换商品id
     */
    public function sendGoodsSaveAddress($id)
    {
        $goodsAddressObj = new GoodsAddress();
        $address = $goodsAddressObj->find(1);
        if (!$address) {
            throw new Exception('请先设置商品自提地址');
        }

        $res = $this->where('order_id', $id)->first();
        if (!$res) {
            throw new Exception('订单地址获取失败');
        }

        $res->send_username = $address->contacts;
        $res->send_tel = $address->tel;
        $res->send_province = $address->province;
        $res->send_city = $address->city;
        $res->send_district = $address->district;
        $res->send_address = $address->address;
        $res->manage_id = request()->manage_id;
        $res->save();

        return $res;
    }






    
}
