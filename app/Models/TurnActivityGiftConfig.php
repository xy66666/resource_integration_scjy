<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * 礼品领取总配置表
 */
class TurnActivityGiftConfig extends BaseModel
{
    use HasFactory;


    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'turn_activity_gift_config';

    /**
     * 获取现有奖品的总概率
     * @param $act_id 活动id 
     */
    public function getActTotalPercent($act_id)
    {
        return AnswerActivityGift::where('act_id', $act_id)->where('is_del', 1)->sum('percent');
    }
}
