<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Http\Controllers\RedisServiceController;

/**
 * 用户使用抽奖机会表
 */
class AnswerActivityUserUsePrizeRecord extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'answer_activity_user_use_prize_record';



    /**
     * 获取抽奖订单号
     */
    public function getDrawOrderId($number,$order_key)
    {
        $redisObj = RedisServiceController::getInstance();

        while ($number >= 0) {
            try {
                $order_id = get_order_id();
                $is_exists = $this->where('order_id', $order_id)->first();
                if ($is_exists) {
                    throw new \Exception("The order number already exists");
                }

                //判断订单号是否存在
                $is_exists = $redisObj->valueExists($order_key.$order_id);
                if ($is_exists) {
                    throw new \Exception("The order number already exists");
                }

                return $order_id;
            } catch (\Exception $e) {
                --$number;
            }
        }
        return false;
    }
}
