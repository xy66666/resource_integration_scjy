<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * 翻奖活动红包转账记录表
 */
class TurnActivityPacketTransfer extends BaseModel
{
    use HasFactory;


    const CREATED_AT = 'create_time';
    const UPDATED_AT = null;


    protected $table = 'turn_activity_packet_transfer';



}
