<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * 书店新书类型模块
 */
class ShopBookType extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = null;

    protected $table = 'shop_book_type';


    /*关联书店*/
    public function conShop()
    {
        return $this->hasOne(Shop::class, 'id', 'shop_id')->select('id', 'name');
    }

    /**
     * 书籍类型筛选
     */
    public function bookTypeFiltrateList($shop_id = null)
    {
        $data = $this->from($this->getTable() . ' as t')
            ->select('s.name as shop_name', 't.id', 't.type_name')
            ->join('shop as s', 's.id', '=', 't.shop_id')
            ->where('t.is_del', 1)
            ->where('s.is_del', 1)
            ->where(function ($query) use ($shop_id) {
                if (!empty($shop_id)) {
                    $query->where('t.shop_id', $shop_id);
                }
            })
           // ->groupBy('t.type_name')
            ->orderByDesc('order_num')
            ->get()
            ->toArray();
            return $data;
    }

    /**
     * 获取类型列表
     * @param keywords 检索关键字
     * @param shop_id 书店id
     * @param limit 类型
     */
    public function lists($field, $shop_id = null, $keywords = null, $shop_all_id = null, $limit = 10)
    {
        if (empty($field)) {
            $field = ['id', 'shop_id', 'type_name', 'create_time'];
        }
        $res = $this->select($field)
            ->with('conShop')
            ->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('type_name', 'like', "%$keywords%");
                }
            })
            ->where(function ($query) use ($shop_id) {
                if ($shop_id) {
                    $query->where('shop_id', $shop_id);
                }
            })
            ->where('is_del', 1)
            ->whereIn('shop_id', $shop_all_id)
            ->paginate($limit)
            ->toArray();
        return $res;
    }

    /**
     * 获取类型详情
     * @param id 类型id
     * @param shop_all_id 
     */
    public function detail($id, $shop_all_id = null)
    {
        if (empty($field)) {
            $field = ['id', 'shop_id', 'type_name', 'create_time'];
        }
        $res = $this->select($field)
            ->with('conShop')
            ->where('id', $id)
            ->where('is_del', 1)
            ->whereIn('shop_id', $shop_all_id)
            ->first();
        return $res;
    }

    /**
     * 判断类型是否存在，如果存在则直接返回类型id，不存在则添加
     * @param shop_id 图书馆id
     * @param type_name 类型名
     */
    public function getTypeId($shop_id, $type_name)
    {
        if (empty($shop_id) || empty($type_name)) {
            return '参数错误';
        }
        $res = $this->where('shop_id', $shop_id)->where('type_name', $type_name)->first();
        if ($res) {
            return $res->id;
        }
        $this->shop_id = $shop_id;
        $this->type_name = $type_name;
        $this->save();
        return $this->id;
    }
}
