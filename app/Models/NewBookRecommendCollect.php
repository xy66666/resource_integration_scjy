<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * 书籍收藏列表
 */
class NewBookRecommendCollect extends BaseModel
{
    use HasFactory;

    const CREATED_AT='create_time';
    const UPDATED_AT=null;

    protected $table = 'new_book_recommend_collect';

    /**
     * 新书书籍收藏关联
     */
    public function newBookRecommendCollect(){
        return $this->belongsTo(NewBookRecommend::class,'book_id' , 'id')
            ->select('id','book_name','author','press','pre_time','isbn','price','img','intro');
    }


    /**
     * 判断是否加入收藏
     * @param book_id  书籍id
     * @param user_id  书籍id
     */
    public static function isCollect($book_id , $user_id){
        return self::where('book_id' , $book_id)
            ->where('user_id' , $user_id)
            ->first();
    }

}