<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Cache;

/**
 * 图片直播浏览过的活动表
 * Class ArticleModel
 * @package app\common\model
 */
class PictureLiveBrowse extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'picture_live_browse';

     /**
     * 添加修改过的记录
     * @param user_id 用户id
     * @param act_id 活动id
     */
    public function modifyBrowseLog($act_id , $user_id){
        $res = $this->where('act_id' , $act_id)->where('user_id' , $user_id)->first();
        if($res){
            $res->save();//只修改时间
        }
        $this->user_id = $user_id;
        $this->act_id = $act_id;
        $this->save();
    }




}
