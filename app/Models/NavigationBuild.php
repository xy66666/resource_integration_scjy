<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/*建筑model*/

class NavigationBuild extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'navigation_build';

    protected $guarded = []; //使用模型  update 更新时，允许被全部更新，若不写，则会报错 或 没任何数据变化
    /**
     * 列表
     * @param limit int 分页大小
     * @param keywords string 搜索关键词
     */
    public function lists($keywords, $is_play =null,$limit = 10)
    {
        $res = $this->select('id','name','province','city','district','address','is_play','remark','create_time')->where(function ($query) use ($keywords) {
            if ($keywords) {
                $query->where('name', 'like', "%$keywords%");
            }
        })->where(function ($query) use ($is_play) {
            if ($is_play) {
                $query->where('is_play', $is_play);
            }
        })
            ->where('is_del', 1)
            ->orderByDesc('id')
            ->paginate($limit)
            ->toArray();
        return $res;
    }
}
