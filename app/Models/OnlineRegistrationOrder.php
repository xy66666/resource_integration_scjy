<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * 在线办证订单
 * Class BookHomeOrderModel
 * @package app\common\model
 */
class OnlineRegistrationOrder extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'online_registration_order';




}