<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * 志愿者服务
 * Class ArticleTypeModel
 * @package app\common\model
 */
class VolunteerNotice extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'volunteer_notice';

    /**
     * 设置志愿者服务
     */
    public function setVolunteerNotice($content){
        $res = $this->where('id' , 1)->first();

        if (!$res) {
            $this->content = $content;
            $result = $this->save();
        }else{
            $res->content = $content;
            $result = $res->save();
        }
        return $result;
    }

}