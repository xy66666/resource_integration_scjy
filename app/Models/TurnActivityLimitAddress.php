<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * 在线抽奖活动限制地址
 */
class TurnActivityLimitAddress extends BaseModel
{
    use HasFactory;


    const CREATED_AT = 'create_time';
    const UPDATED_AT = null;


    protected $table = 'turn_activity_limit_address';

    /**
     * 获取限制列表
     * @param field int 需要获取的字段 数组形式
     * @param page int 页码
     * @param limit int 分页大小
     * @param start_time datetime 活动开始时间    数据格式  年月日
     * @param end_time datetime 活动结束时间
     */
    public function lists($field = [], $act_id, $start_time = null, $end_time = null, $limit = 10)
    {
        if (empty($field)) {
            $field = ['id', 'act_id', 'province', 'city', 'create_time'];
        }
        $res = $this->select($field)
            ->where(function ($query) use ($start_time, $end_time) {
                if ($start_time && $end_time) {
                    $query->whereBetween('create_time', [$start_time, $end_time]);
                }
            })
            ->where('is_del', 1)
            ->where('act_id', $act_id)
            ->orderByDesc('id')
            ->paginate($limit)
            ->toArray();
        return $res;
    }

    /**
     * 获取限制详情
     * @param field int 需要获取的字段 数组形式
     * @param id int id
     */
    public function detail($id, $field = [])
    {
        if (empty($field)) {
            $field = ['id', 'act_id', 'province', 'city', 'create_time'];
        }
        $res = $this->select($field)
            ->where('id', $id)
            ->first();
        return $res;
    }

    /**
     * 判断当前经纬度是否符合参赛要求
     * @param act_id 活动id 
     * @param lon
     * @param lat
     */
    public function checkLonLat($act_id, $lon, $lat)
    {
        $data = $this->lists(['province', 'city'], $act_id, null, null, 99999);
        if (empty($data['data'])) {
            return true; //可以参数
        }
        //获取经纬度对应地址
        $controllerObj = new Controller();
        $map = $controllerObj->getAddressByLonLat($lon, $lat);
        if (is_string($map)) {
            return $map;
        }
        //排除特殊账号
        $exclude_address_limit_guid = config('other.exclude_address_limit_guid');
        if ($exclude_address_limit_guid && in_array(request()->token, $exclude_address_limit_guid)) {
            return true; //特殊账号都可以参加
        }

        $flag = false; //默认不能参数
        foreach ($data['data'] as $key => $val) {
            //特殊处理
            if (($val['province'] == '重庆' && $map['province'] == '重庆市') ||
                ($val['province'] == '上海' && $map['province'] == '上海市') ||
                ($val['province'] == '天津' && $map['province'] == '天津市') ||
                ($val['province'] == '北京' && $map['province'] == '北京市')
            ) {
                $flag = true;
                break;
            }

            if ($val['province'] == $map['province']) {
                if (!empty($val['city'])) {
                    if ($val['city'] == $map['city']) {
                        $flag = true;
                        break;
                    }
                } else {
                    $flag = true;
                    break;
                }
            }
        }
        return $flag;
    }

    /**
     * 判断当前地址是否符合参赛要求
     * @param act_id 活动id 
     * @param province
     * @param city
     */
    public function checkAddress($act_id, $province, $city)
    {
        $data = $this->lists(['province', 'city'], $act_id, null, null, 99999);
        if (empty($data['data'])) {
            return true; //可以参数
        }
        //排除特殊账号
        $exclude_address_limit_guid = config('other.exclude_address_limit_guid');
        if ($exclude_address_limit_guid && in_array(request()->token, $exclude_address_limit_guid)) {
            return true; //特殊账号都可以参加
        }

        $flag = false; //默认不能参数
        foreach ($data['data'] as $key => $val) {
            //特殊处理
            if (($val['province'] == '重庆' && $province == '重庆市') ||
                ($val['province'] == '上海' && $province == '上海市') ||
                ($val['province'] == '天津' && $province == '天津市') ||
                ($val['province'] == '北京' && $province == '北京市')
            ) {
                $flag = true;
                break;
            }

            if ($val['province'] == $province) {
                if (!empty($val['city'])) {
                    if ($val['city'] == $city) {
                        $flag = true;
                        break;
                    }
                } else {
                    $flag = true;
                    break;
                }
            }
        }
        return $flag;
    }
}
