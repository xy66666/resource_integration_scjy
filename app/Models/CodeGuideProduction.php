<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/*作品model*/

class CodeGuideProduction extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'code_guide_production';

    protected $guarded = []; //使用模型  update 更新时，允许被全部更新，若不写，则会报错 或 没任何数据变化


    /*关联展览名称*/
    public function conExhibition()
    {
        return $this->hasOne(CodeGuideExhibition::class, 'id', 'exhibition_id');
    }

    /*关联作品类型*/
    public function conType()
    {
        return $this->hasOne(CodeGuideProductionType::class, 'id', 'type_id');
    }

    /**
     * 列表
     * @param page int 页码
     * @param limit int 分页大小
     * @param exhibition_id string 展览id
     * @param type_id string 类型id
     * @param keywords string 搜索关键词
     * @param start_time datetime 创建开始时间   数据格式 年月日时分秒
     * @param end_time datetime 创建结束时间
     */
    public function lists($field, $keywords = null, $exhibition_id = null, $type_id = null, $start_time = null, $end_time = null, $limit = 10)
    {
        if (empty($field)) {
            $field = ['id', 'name', 'exhibition_id', 'type_id', 'intro', 'img', 'thumb_img', 'content', 'voice', 'voice_name', 'video', 'video_name', 'qr_url', 'browse_num', 'create_time'];
        }
        $res = $this->select($field)
            ->with([
                'conExhibition' => function ($query) {
                    $query->select('id', 'name')->where('is_del', 1);
                }, 'conType' => function ($query) {
                    $query->select('id', 'type_name')->where('is_del', 1);
                }
            ])
            ->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('name', 'like', "%$keywords%");
                }
            })->where(function ($query) use ($start_time, $end_time, $exhibition_id, $type_id) {
                if ($start_time && $end_time) {
                    $query->whereBetween('time', [$start_time, $end_time]);
                }

                //展览
                if (!empty($exhibition_id)) {
                    $query->where('exhibition_id', $exhibition_id);
                }
                //类型
                if (!empty($type_id)) {
                    $query->where('type_id', $type_id);
                }
            })
            ->where('is_del', 1)
            ->orderByDesc('id')
            ->paginate($limit)
            ->toArray();
        return $res;
    }

    /**
     * 详情
     * @param id int 展览id
     * @param qr_code int 二维码code 二选一
     */
    public function detail($id, $qr_code = null, $field = null)
    {
        if (empty($field)) {
            $field = ['id', 'name', 'exhibition_id', 'type_id', 'intro', 'img', 'thumb_img', 'content', 'voice', 'voice_name', 'video', 'video_name', 'qr_code', 'qr_url', 'browse_num', 'create_time'];
        }
        $res = $this->select($field)
            ->with([
                'conExhibition' => function ($query) {
                    $query->select('id', 'name')->where('is_del', 1);
                }, 'conType' => function ($query) {
                    $query->select('id', 'type_name')->where('is_del', 1);
                }
            ])
            // ->with('conExhibition', 'conType')
            ->where(function ($query) use ($id, $qr_code) {
                if ($id) {
                    $query->where('id', $id);
                } else {
                    $query->where('qr_code', $qr_code);
                }
            })
            ->where('is_del', 1)
            ->first();
        return $res;
    }

    /**
     * 根据展览作品id，获取作品数量
     * @param exhibition_id
     */
    public function getProductionNum($exhibition_id)
    {
        return $this->where('exhibition_id', $exhibition_id)->where('is_del', 1)->count();
    }
}
