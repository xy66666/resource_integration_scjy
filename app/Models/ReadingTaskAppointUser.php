<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Cache;

/**
 * 阅读任务限制上传用户
 * Class ArticleModel
 * @package app\common\model
 */
class ReadingTaskAppointUser extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'reading_task_appoint_user';


    /**
     * 用户列表
     * @param limit int 分页大小
     * @param task_id string 活动id
     * @param keywords string 搜索关键词
     * @param start_time datetime 活动开始时间    数据格式  年月日
     * @param end_time datetime 活动结束时间
     */
    public function lists($task_id, $keywords = null, $start_time = null, $end_time = null, $limit = null)
    {
        $res = $this->from('user_info as u')
            ->select('b.id', 'u.id as user_id', 'u.account_id', 'b.create_time', 'w.nickname', 'w.head_img')
            ->join('user_wechat_info as w', 'w.id', '=', 'u.wechat_id')
            ->join('picture_live_appoint_user as b', 'b.user_id', '=', 'u.id')
            ->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('w.nickname', 'like', "%$keywords%");
                }
            })
            ->where(function ($query) use ($start_time, $end_time) {
                if ($start_time && $end_time) {
                    $query->whereBetween('b.create_time', [$start_time, $end_time]);
                }
            })
            ->where('b.task_id', $task_id)
            ->orderByDesc('b.create_time')
            ->paginate($limit)
            ->toArray();
        return $res;
    }

    /**
     * 判断指定用户是否可以参加
     * @param task_id
     * @param user_id
     */
    public function getAppointUser($task_id, $user_id)
    {
        return $this->where('task_id', $task_id)->where('user_id', $user_id)->first();
    }

    /**
     * 获取指定用户
     * @param task_id 活动id
     */
    public function getAppointUserInfo($task_id, $keywords, $limit = 10)
    {
        $res = $this->from('user_info as u')
            ->select('b.id', 'u.id as user_id', 'u.account_id', 'b.create_time', 'w.nickname', 'w.head_img')
            ->join('user_wechat_info as w', 'w.id', '=', 'u.wechat_id')
            ->join('reading_task_appoint_user as b', 'b.user_id', '=', 'u.id')
            ->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('w.nickname', 'like', "%$keywords%");
                }
            })
            // ->where(function ($query) use ($start_time, $end_time) {
            //     if ($start_time && $end_time) {
            //         $query->whereBetween('b.create_time', [$start_time, $end_time]);
            //     }
            // })
            ->where('b.task_id', $task_id)
            ->orderByDesc('b.create_time')
            ->paginate($limit)
            ->toArray();
        return $res;
    }

    /**
     * 添加
     * @param $data 数据
     */
    public function add($data, $field = [])
    {
        $res = $this->where('task_id', $data['task_id'])->where('user_id', $data['user_id'])->first();
        if ($res) {
            return true;
        }

        $this->task_id = $data['task_id'];
        $this->user_id = $data['user_id'];
        return $this->save();
    }

    /**
     * 获取用户需完成的任务总数
     * @param user_id
     */
    public function getUserReadingTaskNumebr($user_id)
    {
        $appoint_number = $this->where('user_id', $user_id)->count(); //获取指定任务数量
        $not_appoint_number = ReadingTask::where('is_appoint', 2)->where('is_del', 1)->count(); //获取所有人都可以参加的用户数量
        return $appoint_number + $not_appoint_number;
    }
}
