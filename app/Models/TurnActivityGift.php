<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * 在线抽奖活动礼物模型
 */
class TurnActivityGift extends BaseModel
{
    use HasFactory;


    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'turn_activity_gift';

    /**
     * 是否配置礼物
     * @param act_id 
     */
    public static function isHaveGift($act_id)
    {
        $gift_info = self::where('act_id', $act_id)->where('is_del', 1)->first();
        if (empty($gift_info)) {
            return '未配置发放礼品，请先配置';
        }
        return true;
    }


    /**
     * 获取奖品数量
     * @param act_id 活动id
     * @param type 1文化红包 2精美礼品
     */
    public function getGiftNumber($act_id, $type = null)
    {
        $res = $this->select('id', DB::raw("sum(total_number) as total_number"), DB::raw("sum(use_number) as use_number"))
            ->where(function ($query) use ($type) {
                //false 全查询
                if ($type) {
                    $query->where('type', $type);
                }
            })->where('act_id', $act_id)
            ->where('is_del', 1)
            ->get()
            ->toArray();

        return $res;
    }


    /**
     * 判断是否存在邮递的礼物
     * @param act_id
     */
    public function isExistsSendGift($act_id)
    {
        $res = $this->where('act_id', $act_id)->where('way', 2)->where('is_del', 1)->first();
        if (empty($res)) {
            return false;
        }
        return true;
    }

    /**
     * 获取现在还剩余的可中奖的奖品
     * @param act_id 活动id
     * @param type 1文化红包 2精美礼品
     * @param start_time 此阶段活动开始时间
     * @param end_time 此阶段活动结束时间
     */
    public function getSurplusGift($act_id, $type = null, $start_time = null, $end_time = null)
    {
        $res = $this->select('id', 'act_id', 'name', 'img', 'total_number', 'percent', 'type', 'way', 'price')
            ->where(function ($query) use ($type) {
                //false 全查询
                if ($type) {
                    $query->where('type', $type);
                }
            })->where('act_id', $act_id)
            ->where('is_del', 1)
            ->orderByDesc('percent')
            ->get()
            ->toArray();

        //获取已中奖的奖品数量
        $has_winning_gift = TurnActivityUserGift::select(DB::raw('count(id) as count'), 'gift_id')
            ->where('act_id', $act_id)
            ->where(function ($query) use ($type, $start_time, $end_time) {
                if ($type) {
                    $query->where('type', $type);
                }
                if ($start_time && $end_time) {
                    $query->whereBetween('create_time', [$start_time, $end_time]);
                }
            })
            ->orderByDesc('count')
            ->groupBy('gift_id')
            ->get()
            ->toArray();

        $has_winning_gift = array_column($has_winning_gift, 'count', 'gift_id');
        $data = [];
        foreach ($res as $key => $val) {
            if (!empty($has_winning_gift[$val['id']])) {
                if ($val['total_number'] > $has_winning_gift[$val['id']]) {
                    $val['use_number'] = $has_winning_gift[$val['id']]; //已中奖数量
                    $data[] = $val;
                }
            } else {
                $val['use_number'] = 0; //已中奖数量
                $data[] = $val;
            }
        }

        return $data;
    }



    /**
     * 获取公示列表
     * @param field int 获取字段信息  数组
     * @param act_id int 活动id
     * @param page int 当前页
     * @param limit int 分页大小
     * @param type int 0或空表示  1文化红包 2精美礼品
     * @param way int 0或空表示  领取方式   1 自提  2邮递   红包无此选项
     * @param start_time datetime 创建时间范围搜索(开始) 
     * @param end_time datetime 创建时间范围搜索(结束) 
     * @param keywords string 搜索关键词(类型名称)
     */
    public function lists($field, $act_id, $keywords, $type, $way, $start_time, $end_time, $page, $limit)
    {
        if (empty($field)) {
            $field = [
                'id', 'act_id', 'name', 'img', 'intro', 'total_number', 'price', 'percent', 'type', 'way', 'start_time', 'end_time', 'tel', 'contacts', 'province', 'city', 'district', 'address', 'remark', 'create_time'
            ];
        }
        $res = $this->select($field)
            ->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('title', 'like', '%' . $keywords . '%');
                }
            })->where(function ($query) use ($act_id, $type, $way, $start_time, $end_time) {
                if ($act_id) {
                    $query->where('act_id', $act_id);
                }
                if ($type) {
                    $query->where('type', $type);
                }
                if ($way) {
                    $query->where('way', $way);
                }
                if ($start_time && $end_time) {
                    $query->whereBetween('create_time', [$start_time, $end_time]);
                }
            })
            ->where('is_del', 1)
            ->orderByDesc('id')
            ->paginate($limit)
            ->toArray();

        $controllerObj = new Controller();
        $res = $controllerObj->disPageData($res);

        return $res;
    }

    /**
     * 获取单位详情
     * @param act_id int 活动id
     */
    public function detail($id, $field = [])
    {
        if (empty($field)) {
            $field = [
                'id', 'act_id', 'name', 'img', 'intro', 'total_number', 'price', 'percent', 'type', 'way', 'start_time', 'end_time', 'tel', 'contacts', 'province', 'city', 'district', 'address', 'remark', 'create_time'
            ];
        }
        $res = $this->select($field)
            ->where('is_del', 1)
            ->where('id', $id)
            ->first();

        return $res;
    }


    /**
     * 判断是否允许添加礼物 或 查询礼物
     * @param act_id
     */
    public function isAllowAddGift($act_id)
    {
        $res = TurnActivity::where('id', $act_id)->where('is_del', 1)->first();
        if (empty($res)) {
            return '此活动不存在';
        }
        return true;
    }

    /**
     * 判断如果是自提礼物，自提点不能为空
     * @param  $data 数据
     */
    public function pickInfoIsFull($data)
    {
        if ($data['way'] != 1) {
            return true;
        }
        if (empty($data['start_time']) || empty($data['end_time'])) {
            return '自提时间不能为空';
        }

        if (empty($data['province']) || empty($data['city']) || empty($data['district']) || empty($data['address'])) {
            return '自提地址不能为空';
        }
        return true;
    }
}
