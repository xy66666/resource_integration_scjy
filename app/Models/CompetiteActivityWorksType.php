<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * 线上大赛类型模型
 * Class ArticleModel
 * @package app\common\model
 */
class CompetiteActivityWorksType extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'competite_activity_works_type';


    /**
     * 获取作品类型
     */
    public function worksType()
    {
        $data = [
            ['name' => '图片类', 'field_name' => 'img'],
            ['name' => '文字类', 'field_name' => 'content'],
            ['name' => '音频类', 'field_name' => 'voice'],
            ['name' => '视频类', 'field_name' => 'video']
        ];
        return $data;
    }
}
