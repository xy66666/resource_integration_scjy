<?php
namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * 问卷调查答案表
 * Class SurveyAnswerModel
 * @package app\common\model
 */
class Survey extends BaseModel
{
    use HasFactory;

    const CREATED_AT='create_time';
    const UPDATED_AT= 'change_time';

    public $table = "survey";

    /**
     * 获取问卷调查状态
     * 1.未开始    2.进行中    3.已参加    4.已结束
     * @param $id ：问卷id
     * @param $start_time ：开始时间
     * @param $end_time ：结束时间
     * @param $user_id ：用户id
     * @return int
     */
    public static function getSurveyStatus($id, $start_time, $end_time, $user_id)
    {
        // 当前时间
        $time = date("Y-m-d H:i:s");

        if ($start_time > $time) {
            return  '问卷调查暂未开始';
        } elseif ($end_time < $time) {
            return  '问卷调查已结束';
        } else {
            $attend = SurveyReply::where(["sur_id" => $id, "user_id" => $user_id])->value("id");
            if($attend) return '您已答过此问卷，请勿重复参加';
        }
        return true;
    }

    /**
     * 处理数据
     *
     * @param $statistics 是否需要统计数据 false  不需要  true 需要
     * @param $reply_num 回答数量  与 $statistics 必须同时存在
     */
    public static function disSurveyData($problem , $answer , $statistics = false , $reply_num = 1){
        $surveyReplyObj = new SurveyReply();

        // 重组数据
        foreach ($problem as $key => $v) {
            // 存储用户当前题所选择的答案
            $problem[$key]["answers"] = [];
            
            // 添加问题
            foreach ($answer as $a) {
                if ($v["pro_id"] == $a["pro_id"]) {
                    // 将答案内容写入问题中
                    $problem[$key]["answers"][$a["ans_id"]] = $a;

                    $answer_reply_num = $a['reply_num'];

                    // 销毁数组
                    unset($problem[$key]["answers"][$a["ans_id"]]["pro_id"]);
                    unset($problem[$key]["answers"][$a["ans_id"]]["reply_num"]);

                    if($statistics){
                        $problem[$key]["answers"][$a["ans_id"]]['reply_num'] = $answer_reply_num;
                        $problem[$key]["answers"][$a["ans_id"]]['percentum'] = empty($reply_num) ? "0%" : sprintf("%.2f",($answer_reply_num/$reply_num)*100).'%';

                        //获取用户自定义答题数据
                        if(empty($a['answer'])){
                            $problem[$key]["answers"][$a["ans_id"]]['user_defined_reply'] = $surveyReplyObj->getUserDefinedAnswer($a['pro_id']);
                        }else{
                            $problem[$key]["answers"][$a["ans_id"]]['user_defined_reply'] = null;//表示不是用户自定义的题目
                        }
                    }
                }
            }
            // 序列化下标
            $problem[$key]["answers"] = array_values($problem[$key]["answers"]);

            // 销毁数组
            unset($problem[$key]["ans"]);
        }
        return $problem;
    }

 
    /**
     * 获取问卷数量
     */
    public function surveyNumber(){
        return $this->where('is_del' , 1)->count();
    }
    /**
     * 获取参与数量
     * @param sur_id
     */
    public function surveyPerson($sur_id =null){
        $surveyReplyModel = new SurveyReply();
         $number = $surveyReplyModel->select('id',DB::raw("CONCAT_WS('-',pro_id,user_id) as pro_user_id"))->where(function($query) use($sur_id){
            if($sur_id){
                $query->where('sur_id' , $sur_id);
            }
        })
        ->groupBy('pro_user_id') //使用2个条件联合分组  这里使用 get，这里就不会报错，使用 count等聚合函数就会报错
        ->get()
        ->toArray();

        return count($number);
    }




}