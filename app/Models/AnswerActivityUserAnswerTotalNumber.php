<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * 用户答题排名表（馆内形式答题）
 */

class AnswerActivityUserAnswerTotalNumber extends BaseModel

{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'answer_activity_user_answer_total_number';




    /**
     * 写入馆内答题总记录
     */
    public function addUserAnswerTotalNumber($act_id, $unit_id, $user_guid, $answer_time, $node = 1)
    {
        $res = $this->getAnswerTotelData($act_id, $unit_id, $user_guid);
        if ($res) {
            $res->answer_number = $res->answer_number + 1;

            $accuracy = $res->correct_number / $res->answer_number;
            $accuracy = sprintf("%.2f", $accuracy * 100);
            $res->accuracy = $accuracy;

            $res->times = $res->times + $answer_time;
            $res->save();
        } else {
            $obj = new self();
            $obj->act_id = $act_id;
            $obj->unit_id = $unit_id;
            $obj->user_guid = $user_guid;
            $obj->answer_number = 1;
            $obj->correct_number = 0;
            $obj->accuracy = 0;
            $obj->times = $answer_time;

            $obj->save();
        }
        if ($node != 1 && !empty($unit_id)) {
            $this->addUserAnswerTotalNumber($act_id, 0, $user_guid, $answer_time, 1); //如果不是独立活动，还要在记录一次总排名
        }

        //在上面统一执行了
        //  if ($node != 1) {
        //     //如果不是独立活动，还要在记录一次总排名
        //     $res = $this->getAnswerTotelData($act_id, 0, $user_guid);

        //     if ($res) {
        //         $res->answer_number = $res->answer_number + 1;

        //         $accuracy = $res->correct_number / $res->answer_number;
        //         $accuracy = sprintf("%.2f", $accuracy * 100);

        //         $res->accuracy = $accuracy;
        //         $res->save();
        //     } else {
        //         $obj = new self();
        //         $obj->act_id = $act_id;
        //         $obj->unit_id = 0;
        //         $obj->user_guid = $user_guid;
        //         $obj->answer_number = 1;
        //         $obj->correct_number = 0;
        //         $obj->accuracy = 0;
        //         $obj->save();
        //     }
        // } 

        return true;
    }

    /**
     * 修改馆内答题总记录
     */
    public function changeUserAnswerTotalNumber($act_id, $unit_id, $user_guid, $node = 1, $status = 1)
    {
        $res = $this->getAnswerTotelData($act_id, $unit_id, $user_guid);
        if (!$res) {
            throw new Exception('获取答题总数据失败');
        }

        if ($status == 1) {
            $res->correct_number = $res->correct_number + 1;
        }

        $accuracy = $res->correct_number / $res->answer_number;
        $accuracy = sprintf("%.2f", $accuracy * 100);

        $res->accuracy = $accuracy;
        $res->times = AnswerActivityUserAnswerRecord::getAnswerTotalTimes($user_guid, $act_id, $unit_id); //获取总执行时间
        $res->save();

        if ($node != 1 && !empty($unit_id)) {
            $this->changeUserAnswerTotalNumber($act_id, 0, $user_guid, 1, $status); //如果不是独立活动，还要在记录一次总排名
        }
        return true;
    }

    /**
     * 获取用户单位答题总记录
     */
    public function getAnswerTotelData($act_id, $unit_id, $user_guid)
    {
        $unit_id = $unit_id ?: 0;
        return $this->where('user_guid', $user_guid)->where('act_id', $act_id)->where('unit_id', $unit_id)->first();
    }
}
