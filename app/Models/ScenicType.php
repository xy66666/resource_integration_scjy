<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * 文旅打卡类型模型
 * Class ArticleTypeModel
 * @package app\common\model
 */
class ScenicType extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'scenic_type';
    /**
     * 判断这个类型下是否有景点
     */
    public function isHaveScenic($type_id)
    {
        $res = $this->from('scenic as s')
            ->join("scenic_type as t", 't.id', '=', 's.type_id')
            ->where('s.type_id', $type_id)
            ->where('s.is_del', 1)
            ->where('t.is_del', 1)
            ->where('s.is_play', 1)
            ->first();
        return $res ? true : false;
    }
}
