<?php

namespace App\Models;

use app\common\model\ReservationApplyModel;
use app\common\model\ReservationScheduleModel;
use app\common\model\ReservationSpecialScheduleModel;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ScoreRuleController;
use Exception;
use http\Env\Request;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

/**
 * 空间预约收藏表
 * Class ReservationModel
 * @package app\common\model
 */
class StudyRoomReservationCollect extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    public $table = 'study_room_reservation_collect';



     /**
     * 关联排班表
     */
    public function reservationCollect()
    {
        return $this->hasOne(StudyRoomReservationModel::class, 'reservation_id', 'id');
    }

    /**
     * 判断自己是否收藏了此物品
     * $$user_id
     */
    public static function isCollect($user_id , $reservation_id){
        $res = self::where('user_id' , $user_id)->where('reservation_id' , $reservation_id)->first();
        return $res ? true : false;
    }


}
