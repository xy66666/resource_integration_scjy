<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * 检索热词
 * Class HotSearchModel
 * @package app\common\model
 */
class HotSearch extends BaseModel
{

    
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = null;

    protected $table = 'hot_search';


    /**
     * 添加检索次数
     * @param $content
     * @param $type
     */
    public function hotSearchAdd($content ,$type){
        $res = $this->where('content' , $content)->where('type' , $type)->first();
        if(!empty($res)){
            $res->increment('number');
        }else{
            $this->add([
                'content'=>$content,
                'number'=>1,
                'type'=>$type,
            ]);
        }
    }



}