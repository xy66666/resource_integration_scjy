<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * 活动基础单位表
 */

class AnswerActivityBaseUnit extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'answer_activity_base_unit';


    /**
     * 获取单位性质
     * $type 单位性质    1 图书馆  2 文化馆  3 博物馆
     */
    public function getNatureName($type = null)
    {
        $data = ['1' => '图书馆', '文化馆', '博物馆'];
        if ($type) {
            return $data[$type];
        }
        return $data;
    }
    /**
     * 获取单位性质 id
     * $type 单位性质    1 图书馆  2 文化馆  3 博物馆
     */
    public function getNatureIdByName($name)
    {
        $data = ['图书馆' => 1, '文化馆' => 2, '博物馆' => 3];
        return $data[$name];
    }


    /**
     * 限制单位赠言个数
     * @param $content
     */
    public function limitWords($content)
    {
        if (!empty($content) && mb_strlen($content) > 50) {
            return '单位赠言最多50字，请重新输入';
        }
        return true;
    }

    /**
     * 根据单位id获取单位名称
     * @param $content
     */
    public function getUnitNameByUnitId($unit_id)
    {
        return $this->where('id', $unit_id)->value('name');
    }

    /**
     * 列表(用于下拉框选择)
     * @param except_act_id 活动id
     */
    public function filterList($field, $except_act_id = null)
    {
        $unit_name = [];
        if ($except_act_id) {
            $answerActivityUnitModel = new AnswerActivityUnit();
            $unit_name = $answerActivityUnitModel->where('act_id', $except_act_id)->where('is_del', 1)->pluck('name');
        }
        if (empty($field)) {
            $field = ['id', 'name', 'img', 'nature'];
        }
        $res = $this->select($field)->where(function ($query) use ($unit_name) {
            if ($unit_name) {
                $query->whereNotIn('name', $unit_name);
            }
        })
            ->where('is_del', 1)
            ->orderBy('order')
            ->orderBy('id')
            ->get()
            ->toArray();
        return $res;
    }

    /**
     * 获取单位列表
     * @param field int 获取字段信息  数组
     * @param page int 当前页
     * @param limit int 分页大小
     * @param nature int 单位性质   1 图书馆  2 文化馆  3 博物馆
     * @param start_time datetime 创建时间范围搜索(开始)
     * @param end_time datetime 创建时间范围搜索(结束)
     * @param keywords string 搜索关键词(类型名称)
     *
     */
    public function lists($field, $keywords, $nature, $start_time = null, $end_time = null, $limit = 10)
    {
        if (empty($field)) {
            $field = [
                'id', 'nature', 'name', 'img', 'intro', 'words', 'letter', 'province',
                'city', 'district', 'address', 'order', 'create_time', 'lon', 'lat'
            ];
        }
        $res = $this->select($field)
            ->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('name', 'like', '%' . $keywords . '%');
                }
            })->where(function ($query) use ($nature, $start_time, $end_time) {
                if ($nature) {
                    $query->where('nature', $nature);
                }
                if ($start_time && $end_time) {
                    $query->whereBetween('create_time', [$start_time, $end_time]);
                }
            })
            ->where('is_del', 1)
            ->orderBy('order')
            ->orderBy('id')
            ->paginate($limit)
            ->toArray();

        $controllerObj = new Controller();
        $res = $controllerObj->disPageData($res);

        return $res;
    }


    /**
     * 详情
     * @param id int 单位id
     */
    public function detail($id, $field = [])
    {
        if (empty($field)) {
            $field = ['id', 'nature', 'name', 'img', 'intro', 'words', 'letter', 'province', 'city', 'district', 'address', 'order', 'create_time'];
        }
        $res = $this->select($field)
            ->where('is_del', 1)
            ->where(function ($query) use ($id) {
                if ($id) {
                    $query->where('id', $id);
                }
            })
            ->first();
        return $res;
    }

    /**
     * 更新到基础数据库
     * @param data 数据信息
     */
    public function changeBaseUnit($data)
    {
        $base_unit_info = $this->where('name', $data['name'])->where('is_del', 1)->first();
        if (empty($base_unit_info)) {
            $base_unit_info = $this;
        }
        $base_unit_info->nature = $data['nature'];
        $base_unit_info->name = $data['name'];
        $base_unit_info->img = $data['img'];
        $base_unit_info->intro = $data['intro'];
        $base_unit_info->tel = !empty($data['tel']) ? $data['tel'] : null;
        $base_unit_info->contacts =  !empty($data['contacts']) ? $data['contacts'] : null;
        $base_unit_info->words = $data['words'];
        $base_unit_info->letter = $data['letter'];
        $base_unit_info->province = $data['province'];
        $base_unit_info->city = $data['city'];
        $base_unit_info->district = $data['district'];
        $base_unit_info->address = $data['address'];
        $base_unit_info->lon = $data['lon'];
        $base_unit_info->lat = $data['lat'];
        $base_unit_info->save();
    }
}
