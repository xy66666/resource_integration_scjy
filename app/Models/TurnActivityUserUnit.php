<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * 在线抽奖用户单位管理
 */
class TurnActivityUserUnit extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'turn_activity_user_unit';


    /**
     * 获取用户填写的信息
     * @param user_guid 用户guid
     * @param act_id 活动id
     */
    public function getUserUnitInfo($user_guid, $act_id)
    {
        $res = $this->select('username', 'tel', 'id_card', 'reader_id', 'unit_id', 'create_time')->where('user_guid', $user_guid)->where('act_id', $act_id)->first();

        if (empty($res)) {
            return [];
        }

        return $res->toArray();
    }

    /**
     * 判断是否选择了所属图书馆 和 是否需要用户信息
     * @param user_guid 用户guid
     * @param act_id 活动id
     * @param unit_id 单位id  如果传了，检索是就不要单位，没传（并且不为 0）就要   
     */
    public function isSelectUnit($user_guid, $act_id, $unit_id = null)
    {
        $res = $this->where('user_guid', $user_guid)->where('act_id', $act_id)
            ->where(function ($query) use ($unit_id) {
                // 如果传了，检索是就不要单位，没传就要
                if ($unit_id) {
                    $query->whereNull('unit_id');
                } elseif (empty($unit_id) && $unit_id !== 0) {
                    //unit_id 0 表示 第一次获取信息时判断
                    $query->whereNotNull('unit_id');
                }
            })->first();

        if (empty($res)) {
            return false;
        }

        return $res;
    }

    /**
     * 判断是否选择了所属图书馆 和 是否需要用户信息
     * @param user_guid 用户guid
     * @param act_id 活动id
     * @param real_info 活动所需参数
     */
    public function needRealInfoAndUnit($user_guid, $act_info)
    {
        if ($act_info['is_ushare'] == 1) {

            return ['real_info' => null, 'unit_id' => null]; //不需要输入信息，也不需要输入单位

        } else if ($act_info['is_ushare'] == 2) {
            //需要查看是否填写了用户信息
            $isSelectUnit = $this->isSelectUnit($user_guid, $act_info['id'], 0);

            //是否需要其他信息
            if (empty($act_info['real_info'])) {
                $real_info = null; //不需要任何信息
            } else {
                $real_info_arr = explode('|', $act_info['real_info']);
                $activityModel = new TurnActivity();
                $activityApplyParam = $activityModel->getAnswerActivityApplyParam();
                $real_info = null;

                foreach ($real_info_arr as $key => $val) {
                    //判断需要那些 参数，或者之前是输入了信息，现在增加了信息
                    if (empty($isSelectUnit[$activityApplyParam[$val - 1]['field']])) {
                        $real_info[] = $activityApplyParam[$val - 1];
                    }
                }
            }
            return ['real_info' => $real_info, 'unit_id' => null]; //直接不需要单位
        }
    }
}
