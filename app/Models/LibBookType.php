<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * 馆藏推荐书籍表
 * Class LibBookBarcodeModel
 * @package app\common\model
 */
class LibBookType extends BaseModel
{

    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'lib_book_type';



    /**
     * 判断类型是否存在，如果存在则直接返回类型id，不存在则添加
     * @param type_name 类型名
     */
    public function getTypeId($type_name)
    {
        if (empty($type_name)) {
            return '参数错误';
        }
        $res = $this->where('type_name', $type_name)->first();
        if ($res) {
            return $res->id;
        }
        $this->type_name = $type_name;
        $this->save();
        return $this->id;
    }
}
