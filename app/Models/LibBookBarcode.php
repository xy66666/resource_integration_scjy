<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * 馆藏书条形码
 * Class LibBookBarcodeModel
 * @package app\common\model
 */
class LibBookBarcode extends BaseModel
{

    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'lib_book_barcode';


    /**
     * 添加书籍数据对应的条形码信息
     * @param $bar_code_info  对应的条形码信息id
     */
    public function libBookBarcodeAdd($book_id, $bar_code_info)
    {
        $where = [];
        $i = 0;
        foreach ($bar_code_info as $key => $val) {
            //判断条形码是否存在
            $res = $this->where('book_id', $book_id)->where('barcode', $val['barcode'])->first();
            if (empty($res)) {
                $where[$i]['book_id'] = $book_id;
                $where[$i]['barcode'] = $val['barcode'];
                $where[$i]['book_num'] = $val['book_num'];
                $where[$i]['init_sub_lib'] = $val['initSublib'];
                $where[$i]['init_local'] = $val['initLocal'];
                $where[$i]['cur_sub_lib'] = $val['curSublib'];
                $where[$i]['cur_local'] = $val['curLocal'];
                $where[$i]['init_sub_lib_note'] = $val['initSublibNote'];
                $where[$i]['init_local_note'] = $val['initLocalNote'];
                $where[$i]['cur_sub_lib_note'] = $val['curSublibNote'];
                $where[$i]['cur_local_note'] = $val['curLocalNote'];
                $where[$i]['reg_date'] = $val['regDate'];
                $where[$i]['ecard_flag'] = $val['ecardFlag'];
                $where[$i]['status'] = $val['status'];
                $i++;
            } else {
                //更新一次记录  2022.9.22 新增
                $res->book_id = $book_id;
                $res->barcode = $val['barcode'];
                $res->book_num = $val['book_num'];
                $res->init_sub_lib = $val['initSublib'];
                $res->init_local = $val['initLocal'];
                $res->cur_sub_lib = $val['curSublib'];
                $res->cur_local = $val['curLocal'];
                $res->init_sub_lib_note = $val['initSublibNote'];
                $res->init_local_note = $val['initLocalNote'];
                $res->cur_sub_lib_note = $val['curSublibNote'];
                $res->cur_local_note = $val['curLocalNote'];
                $res->reg_date = $val['regDate'];
                $res->ecard_flag = $val['ecardFlag'];
                $res->status = $val['status'];
                $res->save();
            }
        }
        if (!empty($where)) {
            $this->insert($where);
        }
    }

    /**
     * 获取条形码id
     */
    public function getBarCodeId($book_id, $barcode, $metatable = null, $metaid = null)
    {
        $barcode_id = $this->where('book_id', $book_id)->where('barcode', $barcode)->value('id');
        if ($barcode_id) {
            return $barcode_id;
        }
        if (empty($metatable) || empty($metaid)) {
            return false;
        }
        $controllerObj = new Controller();
        $libApi = $controllerObj->getLibApiObj();
        //检索的馆藏地
        $curLocalList = BookHomePurchaseSet::where('type', 19)->value('content');
        if ($curLocalList) {
            $curLocalList = explode(',', $curLocalList);
        }

        $bar_code_info = $libApi->getAssetPageInfoForOpac($metatable, $metaid, $curLocalList);
        if ($bar_code_info['code'] == 200) {
            $this->libBookBarcodeAdd($book_id, $bar_code_info['content']);
        } else {
            return false;
        }
        //重新获取数据
        return $this->where('book_id', $book_id)->where('barcode', $barcode)->value('id');
    }

    /**
     * 根据数据id，获取所有的条形码id
     */
    public function getBarcodeIdAll($book_id)
    {
        return $this->where('book_id', $book_id)->pluck('id');
    }
}
