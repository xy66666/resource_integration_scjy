<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/*在线抽奖活动资源model*/

class TurnActivityResource extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'turn_activity_resource';

      
    /**
     * 获取活动资源路径
     * @param act_id
     * 
     */
    public function getDefaultResourceAddr($act_id = null)
    {
        $addr = '/default_activity_resource/turn_double_default.zip';
        return $addr;
    }
    
    /**
     * 获取资源文件夹
     * @param act_id 活动id
     */
    // public function getImgStoreDir($act_id){
    //     $dir_name = sprintf("%08d", $act_id);
    //     $dir_addr = public_path('uploads') . '/turn_activity/ui_resource/' . $dir_name;
    //     return $dir_addr;
    // }

      /**
     * 获取资源文件夹
     * @param act_id 活动id
     */
    public function getImgStoreDir($act_id){
        $resource_path = $this->where('act_id' , $act_id)->value('resource_path');
        if($resource_path){
            return public_path('uploads') .'/'. $resource_path;
        }

        $dir_name = sprintf("%06d", $act_id);
        $dir_name = $dir_name.'00001';
        $dir_addr = public_path('uploads') . '/turn_activity/ui_resource/' . $dir_name;
        return $dir_addr;
    }
     /**
     * 升级资源文件夹名称
     * @param act_id 活动id
     */
    public function getNewImgStoreName($act_id){
        $resource_path = $this->where('act_id' , $act_id)->value('resource_path');
        $resource_path = explode('/' , $resource_path);
        $resource_path = end($resource_path);
        if($resource_path){
            $front = substr($resource_path , 0 , 6);
            $postfix = substr($resource_path , 6);

            $postfix = (int)$postfix + 1;
            $postfix = sprintf("%05d", $postfix);

            return  'turn_activity/ui_resource/'.$front.$postfix;
        }

        $dir_name = sprintf("%06d", $act_id);
        $dir_name = $dir_name.'00001';
        return 'turn_activity/ui_resource/'.$dir_name;
    }


    /**
     * 生成存放图片路径
     * @param act_id 活动id
     */
    public function createImgStoreDir($act_id){
        $dir_addr = $this->getImgStoreDir($act_id);
        $res = true;
        if(!file_exists($dir_addr)){
            $res = mkdir($dir_addr,0777,true);
        }
        if(!$res){
            throw new Exception('活动资源存放文件夹创建失败，请联系管理员处理！');
        }
    }

    /**
     * 获取基本信息
     * @param act_id 活动id
     */
    public function detail($act_id){
        return $this->where('act_id' , $act_id)->first();
    }

    /**
     * 修改资源文件
     * @parma $data 数据资源
     */
    public function change($data , $field = [], $findWhere = []){
        $res = $this->detail($data['act_id']);
        if(empty($res)){
            $res = $this;
        }
        $res->act_id = $data['act_id'];
        $res->theme_color = $data['theme_color'];
        $res->progress_color = $data['progress_color'];
        $res->invite_color = $data['invite_color'];
        $res->theme_text_color = $data['theme_text_color'];
        $res->warning_color = $data['warning_color'];
        $res->error_color = $data['error_color'];
        
        $res->is_change = 2;
        return $res->save();
    }

   
}
