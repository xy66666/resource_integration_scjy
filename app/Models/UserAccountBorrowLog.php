<?php
namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * 接口返回的借还数据记录
 */
class UserAccountBorrowLog extends BaseModel
{

    protected $table = 'user_account_borrow_log';

}