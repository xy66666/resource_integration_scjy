<?php

namespace App\Models;

use app\common\model\ReservationApplyModel;
use app\common\model\ReservationScheduleModel;
use app\common\model\ReservationSpecialScheduleModel;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ScoreRuleController;
use Exception;
use http\Env\Request;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

/**
 * 空间预约进入日志
 * Class ReservationModel
 * @package app\common\model
 */
class StudyRoomReservationEnterLog extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = null;

    public $table = 'study_room_reservation_enter_log';

}
