<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;



/**
 * 图书到家归还方式说明
 * Class PostalReturnModel
 * @package app\common\model
 */
class BookHomeReturnAddressExplain extends BaseModel
{

    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'book_home_return_address_explain';

    /**
     * 地址说明
     */
    public function addressExplain(){
        $res = $this->select('way' , 'content')->get();
        $data['scene'] = '';
        $data['postal'] = '';
        foreach($res as $key=>$val){
            if($val['way'] == 1){
                $data['scene'] = $val['content'];
            }else{
                $data['postal'] = $val['content'];
            }
        } 
        return $data;
    }

     /**
     * 设置图书到家归还地址说明
     * @param way 方式  1 现场归还   2 邮递归还
     * @param content 说明内容 
     */
    public function setAddressExplain($way ,$content){
        $res = $this->where('way', $way)->first();
        if($res){
            $res->content = $content;
            $result = $res->save();
        }else{
            $this->way = $way;
            $this->content = $content;
            $result = $this->save();
        }
        return $result;
    }

}