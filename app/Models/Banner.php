<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * banner管理
 */
class Banner extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'banner';


    /**
     * 获取banner 图片
     * @param 类型  显示位置 0全部  1 首页     2 商城
     */
    public static function getBannerList($type)
    {
        return self::select('name', 'img', 'link')
            ->where('is_play', 1)
            ->where('is_del', 1)
            ->where(function ($query) use ($type) {
                if ($type) {
                    $query->where('type', $type);
                }
            })
            ->orderByDesc('sort')
            ->get()
            ->toArray(); //banenr图片
    }
}
