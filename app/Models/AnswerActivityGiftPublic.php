<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;

/**
 * 活动礼物公示表
 */

class AnswerActivityGiftPublic extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'answer_activity_gift_public';


    /*关联活动单位*/
    public function conUnit()
    {
        return $this->hasOne(AnswerActivityUnit::class, 'id', 'unit_id');
    }

    /**
     * 是否配置公开礼物
     * @param act_id 
     */
    public static function isHaveGiftPublic($act_id)
    {
        $gift_info = self::where('act_id', $act_id)->where('is_del', 1)->first();
        if (empty($gift_info)) {
            return '未配置公示礼品，请先配置';
        }
        return true;
    }


    /**
     * 获取礼物和红包总份数
     * @param act_id 活动id
     * @param unit_id 单位id
     */
    public function getTypeTotalNumber($act_id, $unit_id = null)
    {
        $res = $this->select("type", DB::Raw('sum(number) as number'))->where('act_id', $act_id)->where(function ($query) use ($unit_id) {
            if ($unit_id) {
                $query->where('unit_id', $unit_id);
            }
        })->where('is_del', 1)->groupBy('type')
            ->get();

        return $res;
    }

    /**
     * 获取公示列表
     * @param field int 获取字段信息  数组
     * @param act_id int 活动id
     * @param page int 当前页
     * @param limit int 分页大小
     * @param unit_id int 区域id
     * @param start_time datetime 创建时间范围搜索(开始) 
     * @param end_time datetime 创建时间范围搜索(结束) 
     * @param keywords string 搜索关键词(类型名称)
     */
    public function lists($field, $act_id, $keywords, $unit_id, $start_time, $end_time, $page, $limit)
    {
        if (empty($field)) {
            $field = ['id', 'act_id', 'unit_id', 'type', 'name', 'way', 'number', 'create_time'];
        }
        $res = $this->select($field)
            ->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('name', 'like', '%' . $keywords . '%');
                }
            })->where(function ($query) use ($act_id, $unit_id, $start_time, $end_time) {
                if ($act_id) {
                    $query->where('act_id', $act_id);
                }
                if ($unit_id) {
                    $query->where('unit_id', $unit_id);
                }
                if ($start_time && $end_time) {
                    $query->whereBetween('create_time', [$start_time, $end_time]);
                }
            })
            ->with('conUnit')
            ->where('is_del', 1)
            ->orderByDesc('id')
            ->paginate($limit)
            ->toArray();

        $controllerObj = new Controller();
        $res = $controllerObj->disPageData($res);

        return $res;
    }

    /**
     * 获取单位详情
     * @param act_id int 活动id
     */
    public function detail($id, $field = [])
    {
        if (empty($field)) {
            $field = ['id', 'act_id', 'unit_id', 'name', 'way', 'number', 'create_time'];
        }
        $res = $this->select($field)
            ->with('conUnit')
            ->where('is_del', 1)
            ->where('id', $id)
            ->first();

        //获取单位区域
        if (!empty($res)) {
            $res['unit_name'] = !empty($res['conUnit']['name']) ?  $res['conUnit']['name'] : '';
            unset($res['conUnit']);
        }

        return $res;
    }
}
