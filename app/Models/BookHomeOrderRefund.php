<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use App\Http\Controllers\PayInfo\WxReturnMoneyController;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * 订单退款日志
 */
class BookHomeOrderRefund extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = null;

    protected $table = 'book_home_order_refund';


    /**
     * 审核拒绝或无法发货，进行退款操作
     * @param order_id 订单id
     * @param refund_remark 退款备注
     */
    public function refund($order_id, $refund_remark)
    {
        $order_info = BookHomeOrder::find($order_id);
        if($order_info['payment'] != 1){
            return '此订单无需退款';//系统预算扣除
        }
        if (empty($order_info)) {
            return '订单获取失败';
        }
        if ($order_info['is_pay'] == 5) {
            return '此订单已退款，无需重复操作';
        }
        if ($order_info['is_pay'] != 7 && $order_info['is_pay'] != 9) {
            return '此订单状态异常，请联系技术处理';
        }
        $refund_no = get_order_id();
        $trade_no = BookHomeOrderPay::where('order_number', $order_info->order_number)->value('trade_no');  //微信支付订单号
        $wxReturnMoneyObj = new WxReturnMoneyController();

        $return_info = $wxReturnMoneyObj->doRefund($order_info->dis_price, $order_info->dis_price, $refund_no, $trade_no, $order_info->order_number, $refund_remark);

        $time = date('Y-m-d H:i:s');
        if ($return_info['code'] === 200) {
            $this->refund_number = $refund_no;
            $this->refund_id = $return_info['refund_id'];
            $this->refund_time = $time;
            $this->price = $order_info->price;
            $this->payment = 1;
            $this->status = 1;
            $this->manage_id = request()->manage_id;
            $this->save();
            //修改订单状态
            $order_info->is_pay = 5;
            $order_info->refund_number = $refund_no;
            $order_info->refund_time = $time;
            $order_info->refund_remark = $refund_remark;
            $order_info->refund_manage_id = request()->manage_id;
            $order_info->save();


            //添加系统消息
            $type = $order_info['type'] == 1 ? 44 : 45;
            $controllerObj = new Controller();
            $controllerObj->systemAdd('图书到家：邮费已成功退回', $order_info->user_id, $order_info->account_id, $type, $this->request->order_id, '邮费已成功退回；退回理由: ' . $this->request->refund_remark);

            return true;
        } else {
            //添加退款日志
            $this->refund_number = $refund_no;
            $this->refund_time = $time;
            $this->price = $order_info->price;
            $this->payment = 1;
            $this->status = 2;
            $this->manage_id = request()->manage_id;
            if (isset($return_info['err_code'])) $this->error_code = $return_info['err_code'];

            $this->error_msg = $return_info['msg'];
            $this->save();

            //退款失败
            return $return_info['msg'];
        }
    }
}
