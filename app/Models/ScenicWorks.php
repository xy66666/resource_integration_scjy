<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;

/**
 * 文旅打卡作品模型
 * Class ArticleTypeModel
 * @package app\common\model
 */
class ScenicWorks extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'scenic_works';

    /*关联打卡信息*/
    public function conScenic()
    {
        return $this->hasOne(Scenic::class, 'id', 'scenic_id');
    }

    /**
     * 关联作品图片
     */
    public function conImg()
    {
        return $this->hasMany(ScenicWorksImg::class, 'scenic_works_id', 'id');
    }

    /**
     * 获取总参与用户人数
     * @param scenic_id 大赛id
     * @param status 状态    1.已通过   2.未通过   3.未审核(默认)  4.已撤销， 5已删除 
     * @param is_violate 是否违规  1 正常  2 违规
     */
    public function getTotalUserNumber($scenic_id, $status = [1, 2, 3], $is_violate = null)
    {
        $user_id = $this->where('scenic_id', $scenic_id)
            ->whereIn('status', $status)
            ->where(function ($query) use ($is_violate) {
                if ($is_violate) {
                    $query->where('is_violate', $is_violate);
                }
            })
            ->groupBy('user_id')
            ->pluck('user_id'); //分组联合count查询后，分组无效
        return count($user_id);
    }

    /**
     * 获取总参与作品数量
     * @param scenic_id 大赛id
     * @param status 状态    1.已通过   2.未通过   3.未审核(默认)  4.已撤销， 5已删除 
     * @param is_violate 是否违规  1 正常  2 违规
     */
    public function getTotalWorksNumber($scenic_id, $status = [1, 2, 3], $is_violate = null)
    {
        return $this->where('scenic_id', $scenic_id)
            ->whereIn('status', $status)
            ->where(function ($query) use ($is_violate) {
                if ($is_violate) {
                    $query->where('is_violate', $is_violate);
                }
            })->count();
    }

    /**
     * 获取总参与用户头像
     * @param scenic_id 大赛id
     * @param status 状态    1.已通过   2.未通过   3.未审核(默认)  4.已撤销， 5已删除 
     * @param is_violate 是否违规  1 正常  2 违规
     */
    public function getTotalUserHeadImg($scenic_id, $status = [1, 2, 3], $is_violate = null, $limit = 3)
    {
        $user_id = $this->where('scenic_id', $scenic_id)->whereIn('status', $status)->where(function ($query) use ($is_violate) {
            if ($is_violate) {
                $query->where('is_violate', $is_violate);
            }
        })->groupBy('user_id')
            ->limit($limit)
            ->pluck('user_id');
        $user_wechat_id = UserInfo::whereIn('id', $user_id)->pluck('wechat_id');
        return UserWechatInfo::select('head_img')->whereIn('id', $user_wechat_id)->get();
    }



    /**
     * 获取总投票量
     * @param scenic_id 大赛id
     */
    public function getTotalVoteNumber($scenic_id)
    {
        return $this->where(function ($query) use ($scenic_id) {
            if ($scenic_id) {
                $query->where('scenic_id', $scenic_id);
            }
        })->where('status', 1)->sum('vote_num');
    }


    /**
     * 列表
     * @param scenic_id int 景点id
     * @param user_id int 用户id
     * @param account_id int 读者证号id
     * @param limit int 分页大小
     * @param keywords string 搜索关键词(作品名称，微信昵称)
     * @param status int 审核状态 作品状态 1.已通过   2.未通过   3.未审核(默认)   0全部
     * @param is_violate int 是否违规  1 正常  2 违规
     * @param start_time 筛选开始时间 2020-10-10 23:59:59
     * @param end_time 筛选结束时间 2020-10-12 23:59:59
     */
    public function lists($scenic_id, $user_id = null, $account_id = null, $keywords = null, $status = null, $is_violate = null, $start_time = null, $end_time = null, $limit = 10)
    {
        // DB::enableQueryLog();
        $res = $this->select('id', 'user_id', 'account_id', 'scenic_id', /* 'cover', */ 'title', 'intro', 'browse_num', 'vote_num', 'status', 'is_violate', 'violate_reason', 'create_time', 'change_time', 'is_look')
            ->with(['conImg' => function ($query) {
                $query->select('id', 'scenic_works_id', 'img', 'thumb_img', 'width', 'height');
            }])
            ->where(function ($query) use ($scenic_id, $user_id, $account_id, $status, $is_violate, $start_time, $end_time) {

                if ($scenic_id) {
                    $query->where('scenic_id', $scenic_id);
                }
                if ($status) {
                    $query->whereIn('status', $status);
                }
                if ($is_violate) {
                    $query->where('is_violate', $is_violate);
                }
                if ($user_id) {
                    $query->where('user_id', $user_id);
                }
                if ($account_id) {
                    $query->where('account_id', $account_id);
                }
                if ($start_time && $end_time) {
                    $query->whereBetween('create_time', [$start_time, $end_time]);
                }
            })->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('title', 'like', '%' . $keywords . '%');
                }
            })
            ->where('status', '<>', 5) //5为已删除
            ->orderByDesc('id')
            ->paginate($limit)
            ->toArray();

        //  dd(DB::getQueryLog());
        return $res;
    }


    /**
     * 详情
     * @param id 景点打卡id
     */
    public function detail($id)
    {
        $res = $this->select(
            'id',
            'scenic_id',
            'user_id',
            'account_id',/*  'cover',  */
            'title',
            'intro',
            'browse_num',
            'vote_num',
            'status',
            'reason',
            'create_time',
            'change_time',
            'is_look',
            'is_violate',
            'violate_reason',
            'violate_time'
        )
            ->with(['conImg' => function ($query) {
                $query->select('id', 'scenic_works_id', 'img', 'thumb_img', 'width', 'height');
            }])
            ->where('id', $id)
            ->first();

        return $res;
    }
}
