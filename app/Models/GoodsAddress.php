<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * 积分商城商品自提地址模型
 * Class GoodsImgModel
 * @package app\common\model
 */
class GoodsAddress extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'goods_pick_address';

}