<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * 志愿者服务时间表
 */
class VolunteerServiceTime extends BaseModel
{

    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'volunteer_service_time';



    /**
     * 获取所有的服务意向
     */
    public static function getServiceTimeAll(){
        return self::select('id' , 'times')->where('is_del' , 1)->orderByDesc('id')->get()->toArray();
    }
   

}
