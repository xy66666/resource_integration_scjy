<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * 荐购图书
 */
class RecommendUser extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = null;


    protected $table = 'recommend_user';


    /**
     * 获取荐购人次
     */
    public function recomPerson()
    {
        $res = $this->groupBy('user_id')->get();
        return count($res);
    }
    /**
     * 获取荐购数量
     */
    public function recomNumber()
    {
        return $this->count();
    }
}
