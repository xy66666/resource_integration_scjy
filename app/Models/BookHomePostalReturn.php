<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;



/**
 * 邮递归还书籍
 * Class PostalReturnModel
 * @package app\common\model
 */
class BookHomePostalReturn extends BaseModel
{

  use HasFactory;

  const CREATED_AT = 'create_time';
  const UPDATED_AT = 'change_time';


  protected $table = 'book_home_postal_return';

  //  protected $insert =['create_time' , 'status'=>3];



  /**
   * 馆藏书书籍采购清单
   * @param keywords_type  检索条件   0、全部  1、书名  2、作者 3 ISBN 4 读者证号 5 条形码 默认 0
   * @param keywords  检索条件
   * @param start_time 发货开始时间   数据格式    2020-05-12 12:00:00
   * @param end_time 发货结束时间     数据格式    2020-05-12 12:00:00
   * @param status  归还状态   1 已确认归还  2 已取消归还(管理员取消)  3 归还中  
   * @param page  页数，默认为1，
   * @param limit  条数，默认显示 10条
   */
  public function lists($params)
  {
    $field = $params['field'] ?? null;
    $keywords = $params['keywords'] ?? null;
    $keywords_type = $params['keywords_type'] ?? null;
    $start_time = $params['start_time'] ?? null;
    $end_time = $params['end_time'] ?? null;
    $status = $params['status'] ?? null;
    $limit = $params['limit'] ?? 10;
    $page = $params['page'] ?? 10;
    if (empty($field)) {
      $field = ['r.id as postal_id', 'courier_id','r.tracking_number', 'r.create_time', 'r.change_time', 'r.status', 'r.barcode', 'u.account',
       'u.username', 'r.borrow_time', 'r.expire_time', 'r.book_name', 'r.author', 'r.isbn', 'r.img', 'r.press','r.return_manage_id'];
    }
    $res = $this->from($this->getTable() . ' as r')
      ->select($field)
      ->join('user_account_lib as u', 'u.id', '=', 'r.account_id')
      ->where(function ($query) use ($keywords_type, $keywords) {
        if ($keywords) {
          if ($keywords_type) {
            switch ($keywords_type) {
              case 1:
                $query->where('u.username', 'like', "%$keywords%");
                break;
              case 2:
                $query->where('u.account', 'like', "%$keywords%");
                break;
              case 3:
                $query->where('r.barcode', 'like', "%$keywords%");
                break;
              case 4:
                $query->where('r.tracking_number', 'like', "%$keywords%");
                break;;
            }
          } else {
            $query->where('u.username', 'like', "%$keywords%")
              ->orWhere('u.account', 'like', "%$keywords%")
              ->orWhere('r.barcode', 'like', "%$keywords%")
              ->orWhere('r.tracking_number', 'like', "%$keywords%");
          }
        }
      })
      ->where(function ($query) use ($status, $start_time, $end_time) {
        if ($status) {
          $query->where('r.status', $status);
        }
        if (!empty($start_time) && !empty($end_time)) {
          $query->whereBetween('r.create_time', [$start_time, $end_time]);
        }
      })
      ->orderByDesc('r.status')
      ->orderByDesc('r.create_time')
      ->where('r.status', '<>', 4) //去掉用户已撤销的数据;
      ->paginate($limit)
      ->toArray();
    return $res;
  }
}
