<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * 阅享精选模型
 * Class ArticleModel
 * @package app\common\model
 */
class News extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'news';

    /**
     * 关联新闻类型
     */
    public function conType()
    {
        return $this->hasOne(NewsType::class, 'id', 'type_id');
    }

    /**
     * 关联新闻收藏
     */
    public function conCollect()
    {
        return $this->hasMany(NewsCollect::class, 'news_id', 'id');
    }

    /**
     * 根据新闻类型获取新闻列表
     * @param page int 当前页
     * @param limit int 分页大小
     * @param keywords string 搜索关键词(文章标题)
     * @param type_id int 类型id
     * @param start_time int 开始时间
     * @param end_time int 结束时间
     */
    public function lists($keywords = null, $type_id = null, $start_time = null, $end_time = null, $limit = 10)
    {
        $res = $this->select('id', 'type_id', 'img', 'title', 'browse_num', 'add_time','create_time', 'change_time', 'content', 'is_top')
            ->where(function ($query) use ($type_id, $start_time, $end_time) {
                if ($type_id) {
                    $query->where('type_id', $type_id);
                }
                if ($start_time && $end_time) {
                    $query->whereBetween('add_time', [$start_time, $end_time]);
                }
            })->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('title', 'like', "%$keywords%");   //使用 orWhere 就需要单独写一个 闭包，不使用则可以写在一起
                }
            })->with('conType')
            // ->whereHas('conType', function ($query) {
            //     $query->where('is_del', 1);
            // })
            ->where('is_del', 1)
            ->orderByDesc('is_top')
            ->orderByDesc('add_time')
            ->paginate($limit)
            ->toArray();
        return $res;
    }

    /**
     * 获取详情
     */
    public function detail($id)
    {
        $res = $this->select('id', 'type_id', 'img', 'title', 'browse_num', 'add_time', 'change_time', 'content', 'is_top')
            ->with('conType')
            ->find($id);
        return $res;
    }


    /**
     * 置顶与取消置顶
     * @param $id 新闻id
     */
    public function topAndCancel($id)
    {
        $res = $this->find($id);
        if (!$res) {
            throw new Exception('参数传递错误');
        }

        //取消置顶
        if ($res->is_top > 0) {
            $res->is_top = 0;
            $res->save();
            $msg = '取消置顶';
        } else {
            //置顶
            $res->is_top = 4; //只能置顶3个，下面会全部在减少一次，就刚好是 3 了
            $res->save();

            $this->where('is_top', '>', 0)->decrement('is_top', 1);

            $msg = '置顶';
        }
        return $msg;
    }
    /**
     * 用户收藏新闻列表
     * @param user_id 用户id 
     * @param type_id  新闻类型id  未传表示全部
     * @param keywords 关键字搜索
     * @param page 页数    默认第一页
     * @param limit 限制条数   默认 10 条
     */
    public function collectList($user_id, $keywords = null, $type_id = null, $limit = 10)
    {
        $res = $this->model->select('id', 'type_id', 'title', 'add_time', 'img', 'content', 'browse_num')
            ->where(function ($query) use ($type_id) {
                if ($type_id) {
                    $query->where('type_id', $type_id);
                }
            })->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('title', 'like', "%$keywords%");   //使用 orWhere 就需要单独写一个 闭包，不使用则可以写在一起
                }
            })->with(['conType', 'conCollect'])
            ->whereHas('conType', function ($query) {
                $query->where('is_del', 1);
            })->whereHas('conCollect', function ($query) use ($user_id) {
                $query->where('user_id', $user_id);
            })
            ->where('is_del', 1)
            ->orderByDesc('add_time')
            ->paginate($limit)
            ->toArray();
        return $res;
    }
}
