<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;



/**
 * 志愿者服务意向表
 */
class VolunteerServiceIntention extends BaseModel
{

    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'volunteer_service_intention';


    /**
     * 获取所有的服务意向
     */
    public static function getServiceIntentionAll(){
        return self::select('id' , 'name')->where('is_del' , 1)->orderByDesc('id')->get()->toArray();
    }
   

}
