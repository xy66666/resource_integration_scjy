<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/*活动文字资源model*/

class AnswerActivityWordResource extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'answer_activity_word_resource';


    /**
     * 文字资源配置
     * @param act_id  活动id
     * @param invitation_prompt 邀请码提示
     * @param relevant_information 所属馆填写信息
     * @param unit_detail_answer  单位详情答题数提示
   //  * @param unit_detail_answer2  单位详情答题进度 //1 当前进度  2  当前进度/总进度  3进度条
     * @param stair_display  楼梯显示数据
     * @param answer_success1  答题成功提示
     * @param answer_success2  答题成功2
     * @param answer_success3  答题成功3
     * @param answer_error1  错误界面1
     * @param answer_error2  错误界面2
     * @param answer_error3  错误界面3
     * @param answer_error4  是否显示解析  1、显示 2、不显示
     * @param grade1  我的成绩1
     * @param grade2  我的成绩2
     * @param grade3  我的成绩3
     * @param ranking1 排名1
     * @param ranking2 排名2
     */
    public function modify($data)
    {
        $res = $this->where('act_id', $data['act_id'])->first();
        if (empty($res)) {
            $res = $this;
        }
        $res->act_id = $data['act_id'];
        $res->invitation_prompt = $data['invitation_prompt'];
        $res->relevant_information = $data['relevant_information'];
        $res->unit_detail_answer  = $data['unit_detail_answer'];
        //      $res->unit_detail_answer2  = $data['unit_detail_answer2'];
        $res->stair_display = $data['stair_display'];
        $res->answer_success1  = $data['answer_success1'];
        $res->answer_success2  = $data['answer_success2'];
        $res->answer_success3  = $data['answer_success3'];
        $res->answer_error1  = $data['answer_error1'];
        $res->answer_error2  = $data['answer_error2'];
        $res->answer_error3  = $data['answer_error3'];
        $res->answer_error4  = $data['answer_error4'];
        $res->grade1 = $data['grade1'];
        $res->grade2 = $data['grade2'];
        $res->grade3 = $data['grade3'];
        $res->ranking1 = $data['ranking1'];
        $res->ranking2 = $data['ranking2'];

        $res->save();
        return $res->id;
    }


    /**
     * 获取活动文字资源路径
     * @param act_id
     */
    public function getWordResource($act_id)
    {
        $res = $this->where('act_id', $act_id)->first();

        $data = [];
        $data['act_id'] = $act_id;
        $data['invitation_prompt'] = empty($res->invitation_prompt) ? '(活动尚未开始，可输入邀请码进去)' : $res->invitation_prompt;
        $data['relevant_information'] = empty($res->relevant_information) ? '注:部分精美礼物需要到馆领取，请如实填写相关信息，避免出现无法领取的情况。信息填写后无法更改，请谨慎填写!' : $res->relevant_information;
        $data['unit_detail_answer'] = empty($res->unit_detail_answer) ? '当前馆答题数：' : $res->unit_detail_answer;
        //    $data['unit_detail_answer2'] = empty($res->unit_detail_answer2) ? '1' : $res->unit_detail_answer2;//1 当前进度  2  当前进度/总进度  3进度条
        $data['stair_display'] = empty($res->stair_display) ? '第x关' : $res->stair_display;
        //   $data['answer_success1'] = empty($res->answer_success1) ? '回答正确' : $res->answer_success1;
        $data['answer_success1'] = empty($res->answer_success1) ? '知识的金锄头可开天辟地！' : $res->answer_success1;
        $data['answer_success2'] = empty($res->answer_success2) ? '放弃答题' : $res->answer_success2;
        $data['answer_success3'] = empty($res->answer_success3) ? '继续答题' : $res->answer_success3;
        //    $data['answer_error1'] = empty($res->answer_error1) ? '回答错误' : $res->answer_error1;
        $data['answer_error1'] = empty($res->answer_error1) ? '继续加油哦' : $res->answer_error1;
        $data['answer_error2'] = empty($res->answer_error2) ? '放弃答题' : $res->answer_error2;
        $data['answer_error3'] = empty($res->answer_error3) ? '继续答题' : $res->answer_error3;
        $data['answer_error4'] = empty($res->answer_error4) ? '1' : $res->answer_error4;
        $data['grade1'] = empty($res->grade1) ? '' : $res->grade1; //空表示显示年份
        $data['grade2'] = empty($res->grade2) ? '读书破万卷，下笔如有神，没错，是你了~' : $res->grade2;
        $data['grade3'] = empty($res->grade3) ? '立即扫码，获得您的阅读成绩' : $res->grade3;
        $data['ranking1'] = empty($res->ranking1) ? '排行榜' : $res->ranking1;
        $data['ranking2'] = empty($res->ranking2) ? '答题数' : $res->ranking2;

        return $data;
    }

    /**
     * 获取资源文件夹
     * @param act_id 活动id
     */
    public function getImgStoreDir($act_id)
    {
        $resource_path = $this->where('act_id', $act_id)->value('resource_path');
        if ($resource_path) {
            return public_path('uploads') . '/' . $resource_path;
        }

        $dir_name = sprintf("%06d", $act_id);
        $dir_name = $dir_name . '00001';
        $dir_addr = public_path('uploads') . '/answer_activity/ui_resource/' . $dir_name;
        return $dir_addr;
    }
    /**
     * 升级资源文件夹名称
     * @param act_id 活动id
     */
    public function getNewImgStoreName($act_id)
    {
        $resource_path = $this->where('act_id', $act_id)->value('resource_path');
        $resource_path = explode('/', $resource_path);
        $resource_path = end($resource_path);
        if ($resource_path) {
            $front = substr($resource_path, 0, 6);
            $postfix = substr($resource_path, 6);

            $postfix = (int)$postfix + 1;
            $postfix = sprintf("%05d", $postfix);

            return  'answer_activity/ui_resource/' . $front . $postfix;
        }

        $dir_name = sprintf("%06d", $act_id);
        $dir_name = $dir_name . '00001';
        return 'answer_activity/ui_resource/' . $dir_name;
    }


    /**
     * 生成存放图片路径
     * @param act_id 活动id
     */
    public function createImgStoreDir($act_id)
    {
        $dir_addr = $this->getImgStoreDir($act_id);
        $res = true;
        if (!file_exists($dir_addr)) {
            $res = mkdir($dir_addr, 0777, true);
        }
        if (!$res) {
            throw new Exception('活动资源存放文件夹创建失败，请联系管理员处理！');
        }
    }

    /**
     * 获取基本信息
     * @param act_id 活动id
     */
    public function detail($act_id)
    {
        return $this->where('act_id', $act_id)->first();
    }

    /**
     * 修改资源文件
     * @parma $data 数据资源
     */
    public function change($data, $field = [], $findWhere = [])
    {
        $res = $this->detail($data['act_id']);
        if (empty($res)) {
            $res = $this;
        }
        $res->act_id = $data['act_id'];
        $res->theme_color = $data['theme_color'];
        $res->progress_color = $data['progress_color'];
        $res->invite_color = $data['invite_color'];
        $res->theme_text_color = $data['theme_text_color'];
        $res->warning_color = $data['warning_color'];
        $res->error_color = $data['error_color'];
        $res->is_change = 2;
        return $res->save();
    }
}
