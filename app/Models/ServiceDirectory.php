<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * 服务指南模型
 * Class ArticleModel
 * @package app\common\model
 */
class ServiceDirectory extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'service_directory';

    /**
     * 类型
     *  type: 1.本馆概况 2.开馆时间 3.入馆须知 4.读者须知 5.办证指南 6.馆内导航 7.馆内导航 8.交通指南 9.联系图书馆
     */
    public function serviceTypeList()
    {
        $data = [
            ['type' => 1, 'title' => '本馆概况', 'content' => ''],
            ['type' => 2, 'title' => '开馆时间', 'content' => ''],
            ['type' => 3, 'title' => '入馆须知', 'content' => ''],
            ['type' => 4, 'title' => '读者须知', 'content' => ''],
            ['type' => 5, 'title' => '办证指南', 'content' => ''],
            ['type' => 6, 'title' => '馆内导航', 'content' => ''],
            ['type' => 7, 'title' => '分馆服务', 'content' => ''],
            ['type' => 8, 'title' => '交通指南', 'content' => ''],
            ['type' => 9, 'title' => '联系图书馆', 'content' => ''],
        ];
        return $data;
    }


    /**
     * 获取新闻列表（不分页）
     * @param $exclude_empty 排除空数据  1 排除 其余不排除
     */
    public function lists($limit, $exclude_empty = 2)
    {
        $res = $this
            ->select('id', 'title', 'type', /* 'img', */ 'content','lon','lat', 'create_time','change_time')
            ->where('is_del', 1)
            ->where(function ($query) use ($exclude_empty) {
                if ($exclude_empty == 1) {
                    $query->where('content', '<>', '');
                }
            })
            ->paginate($limit)
            ->toArray();

        return $res;
    }
}
