<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * 问卷调查答案表
 * Class SurveyAnswerModel
 * @package app\common\model
 */
class SurveyAnswer extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    public $table = "survey_answer";


    /**
     * 新增答案
     * @param $answer
     * @param $sur_id
     * @param $pro_id
     * @param $type 题目类型  1  多选  2 选择 （默认）3 填空题
     * @throws \Exception
     */
    public function insertAnswer($answer, $sur_id, $pro_id , $type)
    {
        if($type == 3){
           return $this->addAnswer($sur_id, $pro_id, ''); //直接添加一道填空题
        }
        //判断数组是否存在空数据
        $answer_answer = array_column($answer, 'answer_name');
        //是否有一道填空题
        $is_gap_filling = false;
        // Log::error(count($answer));
        // Log::error(count(array_filter($answer_answer)));die;
        if (count($answer) != count(array_filter($answer_answer))) {
            $is_gap_filling = true;
        }

        foreach ($answer as $k => $v) {
            if (empty($v['answer_name'])) {
                continue;
            }
            $this->addAnswer($sur_id, $pro_id, $v['answer_name']);
        }
        if ($is_gap_filling) {
            $this->addAnswer($sur_id, $pro_id, ''); //添加一道填空题
        }
    }

    /**
     * 编辑答案
     * @param $answer
     * @param $sur_id
     * @param $pro_id
     * @throws \Exception
     */
    public function changeAnswer($answer, $sur_id, $pro_id)
    {
        //判断数组是否存在空数据
        // $answer_answer = array_column($answer, 'answer_name');
        // //是否有一道填空题
        // $is_gap_filling = false;
        // if (count($answer) != count(array_filter($answer_answer))) {
        //     $is_gap_filling = true;
        // }
        foreach ($answer as $k => $v) {
            if (empty($v['answer_name'])) {
                continue;//填空题不允许改删除答案，也不允许添加，所以这个不管
            }

            if (empty($v['answer_id'])) {
                //新增答案
                //判断问题是否存在，已存在就不允许在次添加了
                $answer_state = $this
                    ->where('answer', $v['answer_name'])
                    ->where('sur_id', $sur_id)
                    ->where('pro_id', $pro_id)
                    ->first();

                if (!empty($answer_state)) {
                    continue;
                }
                $this->addAnswer($sur_id, $pro_id, $v['answer_name']);
                continue;
            }

            //判断问题是否存在，已存在就不允许在次添加了
            $answer_state = $this
                ->where('answer', $v['answer_name'])
                ->where('id', '<>', $v['answer_id'])
                ->where('sur_id', $sur_id)
                ->where('pro_id', $pro_id)
                ->first();
            if (!empty($answer_state)) {
                throw new \Exception('同一个问题的答案必须唯一');
            }
            $this->where('id', $v['answer_id'])->update([
                'answer' => $v['answer_name'],
                'change_time' => date('Y-m-d H:i:s'),
            ]);
        }

        // if ($is_gap_filling) {
        //     $this->addAnswer($sur_id, $pro_id, ''); //添加一道填空题
        // }
    }

    /**
     * 添加答案
     */
    public function addAnswer($sur_id, $pro_id, $answer_name)
    {
        $this->insert([
            'sur_id' => $sur_id,
            'pro_id' => $pro_id,
            'answer' => $answer_name,
            'create_time' => date('Y-m-d H:i:s'),
        ]);
    }
}
