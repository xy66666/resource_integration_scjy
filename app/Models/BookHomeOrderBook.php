<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * 新书、馆藏书订单书籍表
 */
class BookHomeOrderBook extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = null;

    protected $table = 'book_home_order_book';

    /**
     * 获取订单书籍
     * @param order_id 订单号
     * @param $data 订单数组
     */
    public function getOrderBookInfoList($order_id, $data = [])
    {

        if ($data['type'] == 1) {
            $book_id = $this->where('order_id', $order_id)->pluck('book_id');
            if (empty($book_id)) {
                return [];
            }
            $shopBookModel = new ShopBook();
            $bookHomePurchaseModel = new BookHomePurchase();
            $book_info = $shopBookModel->getBookInfo($book_id);
            //匹配生成的条形码
            if (isset($data['is_pay'])) {
                foreach ($book_info as $k => $v) {
                    if (stripos($data['is_pay'], '8') !== false || stripos($data['is_pay'], '6') !== false) {
                        $book_info[$k]['barcode'] = $bookHomePurchaseModel->getBarcode($v['id'], $data['id'], $data['account_id']);
                    } else {
                        $book_info[$k]['barcode'] = '';
                    }
                }
            }
        } else {
            $libBookModel = new  LibBook();
            $book_info = $libBookModel->getBookInfoList($order_id);
        }
        return $book_info;
    }
}
