<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * 码上借 借阅记录
 */
class CodeBorrowLog extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'code_borrow_log';


    /*关联场所码*/
    public function conCodePlace()
    {
        return $this->hasOne(CodePlace::class, 'id', 'place_code_id');
    }

    /**
     * 书籍借阅
     * @param $data 用户数据
     * @param $user_id 用户id
     * @param $account 读者证号
     * @param $place_code_id 场所码id
     */
    public function addData($data, $user_id, $account, $place_code_id)
    {
        $this->rdrrecno = !empty($data['rdrrecno']) ? $data['rdrrecno'] : null;
        $this->place_code_id = $place_code_id;
        $this->lib_id = !empty($data['lib_id']) ? $data['lib_id'] : null;
        $this->lib_name = !empty($data['lib_name']) ? $data['lib_name'] : null;
        $this->cur_local = '中心馆';
        $this->cur_local_name =  '中心馆';
        $this->cur_lib =  '中心馆';
        $this->cur_lib_name =  '中心馆';
        $this->owner_local = '中心馆';
        $this->owner_local_name =  '中心馆';
        $this->recno = $data['metaid'];
        $this->bibrecno = !empty($data['metatable']) ? $data['metatable'] : null;
        $this->book_name = $data['book_name'];
        $this->author = $data['author'];
        $this->price =  !empty($data['price']) ? $data['price'] : '';
        $this->press = $data['press'];
        $this->pre_time =  !empty($data['pre_time']) ? $data['pre_time'] : '';
        $this->callno = $data['book_num'];
        $this->barcode = $data['barcode'];
        $this->isbn = $data['isbn'];
        $this->classno = !empty($data['classno']) ? $data['classno'] : null;
        $this->user_id = $user_id;
        $this->account = $account; //读者证号

        $this->save();
        return $this->id;
    }


    /**
     * 借阅操作列表
     * @param user_id  用户id
     * @param place_code_id  场所码id
     * @param keywords  检索条件
     * @param start_time 申请开始时间   数据格式    2020-05-12 12:00:00
     * @param end_time 申请结束时间     数据格式    2020-05-12 12:00:00
     * @param page  页数，默认为1，
     * @param limit  条数，默认显示 10条
     */

    public function lists($user_id, $place_code_id, $keywords, $start_time, $end_time, $limit)
    {
        $res = $this->select(['place_code_id', 'book_name', 'author', 'price', 'press', 'isbn', 'barcode', 'pre_time', 'callno', 'classno', 'create_time as borrow_time', 'account'])
            ->with(['conCodePlace' => function ($query) {
                $query->select('id', 'name');
            }])
            ->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('book_name', 'like', "%$keywords%")
                        ->Orwhere('barcode', 'like', "%$keywords%")
                        ->Orwhere('isbn', 'like', "%$keywords%")
                        ->Orwhere('account', 'like', "%$keywords%");
                }
            })->where(function ($query) use ($user_id, $place_code_id, $start_time, $end_time) {
                if ($user_id) {
                    $query->where('user_id', $user_id);
                }
                if ($place_code_id) {
                    $query->where('place_code_id', $place_code_id);
                }
                if ($start_time && $end_time) {
                    $query->whereBetween('create_time', [$start_time, $end_time]);
                }
            })->orderByDesc('id')
            ->paginate($limit)
            ->toArray();
        return $res;
    }

    /**
     * 获取用户借阅数量和最喜欢的类型
     */
    public function userBorrowData($user_id, $start_time, $end_time)
    {
        $code_borrow_classno = $this->where('user_id', $user_id)->whereBetween('create_time', [$start_time, $end_time])->pluck('classno')->toArray();
        if ($code_borrow_classno) {
            $code_borrow_number = count($code_borrow_classno);
            $classno = [];
            $classno_prefix_arr = [];
            foreach ($code_borrow_classno as $key => $val) {
                $classno_prefix = substr($val, 0, 1);
                if (in_array($classno_prefix, $classno_prefix_arr)) {
                    $classno[$classno_prefix]['number'] = $classno[$classno_prefix]['number'] + 1;
                } else {
                    $classno[$classno_prefix]['number'] = 1;
                    $classno[$classno_prefix]['prefix'] = $classno_prefix;
                    $classno_prefix_arr[] = $classno_prefix;
                }
            }
            rsort($classno); //最多的一个排在前面
            $classno_prefix = $classno[0]['prefix'];
            $code_borrow_type = BookTypes::where('classify', $classno_prefix)->value('title');
            $code_borrow_type = $code_borrow_type ? $code_borrow_type : '未知类型';
        } else {
            $code_borrow_number = 0;
            $code_borrow_type = '未知类型';
        }
        return [$code_borrow_number, $code_borrow_type];
    }
}
