<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * 志愿者岗位发布
 * Class ArticleTypeModel
 * @package app\common\model
 */
class VolunteerPosition extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'volunteer_position';


    /**
     * 志愿者报名，必选参数
     * 1、姓名 2、性别 3、生日  4、身份证号码  5、民族 6、政治面貌  7、健康状态  8、联系电话  9、毕业院校  10、专业  11、工作单位 12、教育程度  13、个人简介  14、特长  15、用户地址 
     */
    public function volunteerApplyParam()
    {
        $data = [
            ['id' => 1, 'field' => 'username', 'value' => '姓名'],
            ['id' => 2, 'field' => 'sex', 'value' => '性别'],
            ['id' => 3, 'field' => 'birth', 'value' => '生日'],
            ['id' => 4, 'field' => 'id_card', 'value' => '身份证号码'],
            ['id' => 5, 'field' => 'nation', 'value' => '民族'],
            ['id' => 6, 'field' => 'politics', 'value' => '政治面貌'],
            ['id' => 7, 'field' => 'health', 'value' => '健康状态'],
            ['id' => 8, 'field' => 'tel', 'value' => '联系电话'],
            ['id' => 9, 'field' => 'school', 'value' => '毕业院校'],
            ['id' => 10, 'field' => 'major', 'value' => '专业'],
            ['id' => 11, 'field' => 'unit', 'value' => '工作单位'],
            ['id' => 12, 'field' => 'degree', 'value' => '教育程度'],
            ['id' => 13, 'field' => 'intro', 'value' => '个人简介'],
            ['id' => 14, 'field' => 'specialty', 'value' => '特长'],
            ['id' => 15, 'field' => 'address', 'value' => '用户地址']
        ];
        return $data;
    }


    /**
     * 获取对应参数 的那么
     * @param real_info
     */
    public function getRealInfoName($real_info)
    {
        if (empty($real_info)) {
            return '';
        }
        $real_info = explode('|', $real_info);
        $name_value = $this->volunteerApplyParam();
        $name_value = array_column($name_value, 'value', 'id');
        $real_info_name = '';
        foreach ($real_info as $key => $val) {
            $real_info_name .= '；' . $name_value[$val];
        }

        return trim($real_info_name, '；');
    }

    /**
     * 服务岗位列表
     * @param page
     * @param limit
     * @param keywords 检索
     */
    public function lists($keywords, $limit)
    {
        $res = $this->select('id', 'name', 'intro', 'real_info', 'real_info_must', 'expire_time', 'create_time', 'is_play')
            ->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('name', 'like', "%$keywords%");
                }
            })
            ->where('is_del', 1)
            ->orderByDesc('id')
            ->paginate($limit)
            ->toArray();

        return $res;
    }


    /**
     * 服务岗位详情
     * @param id
     */
    public function detail($id)
    {
        $res = $this->select('id', 'name', 'intro', 'real_info', 'real_info_must', 'expire_time', 'create_time', 'is_play','is_reader')
            ->where('id', $id)
            ->first();

        return $res;
    }
}
