<?php
namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**征集活动与征集活动标签关系model */
class CompetiteActivityTag extends BaseModel
{

    use HasFactory;

    const CREATED_AT='create_time';
    const UPDATED_AT='change_time';

    
    protected $table = 'competite_activity_tag';


    /**
     * 根据 标签id获取标签名称
     * @param $tag_id  多个  逗号拼接
     * @return array
     */
    public static function getTagNameByTagId($tag_id){
        if(empty($tag_id)) return [];
        if(!is_array($tag_id)) $tag_id = explode(',' , $tag_id);
        $res = self::whereIn('id' , $tag_id)->where('is_del' , 1)->pluck('tag_name');
        if(empty($res)) return [];
        return $res;
    }


}