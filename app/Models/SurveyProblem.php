<?php
namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * 问卷问题表
 * Class SurverProblem
 * @package app\common\model
 */
class SurveyProblem extends BaseModel
{
    use HasFactory;

    const CREATED_AT='create_time';
    const UPDATED_AT= 'change_time';

    public $table = "survey_problem";


    /**
     * 新增问题
     * @param $sur_id
     * @param $title
     * @param $val
     * @return int|string
     */
    public function insertProblem($sur_id, $title, $val)
    {
        $pro_id = $this->insertGetId([
            'sur_id' => $sur_id,
            'problem' => $title,
            'type' => $val['type'],
            'create_time' => date('Y-m-d H:i:s'),
        ]);
        return $pro_id;
    }

}