<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * 答题活动红包转账记录表
 */
class AnswerActivityPacketTransfer extends BaseModel
{
    use HasFactory;


    const CREATED_AT = 'create_time';
    const UPDATED_AT = null;


    protected $table = 'answer_activity_packet_transfer';



}
