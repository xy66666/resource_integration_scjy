<?php
namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * 用户抽奖收货地址表
 */
class UserDrawAddress extends BaseModel
{
    use HasFactory;

    const CREATED_AT='create_time';
    const UPDATED_AT='change_time';

    protected $table = 'user_draw_address';

    /**
     * 新增一条地址
     * type 类型  1、答题活动   2、在线抽奖 
     */
    public function inserts($data ,$type =1,$act_id=null){
        $this->insert([
            'act_id' => $act_id,
            'type' => $type,
            'user_guid' => $data['user_guid'],
            'username' => $data['username'],
            'tel' => $data['tel'],
            'province' => $data['province'],
            'city' => $data['city'],
            'district' => $data['district'],
            'street' => !empty($data['street']) ? $data['street'] : '',
            'address' => $data['address'],
            'area_code' => !empty($data['area_code']) ? $data['area_code'] : '',
            'create_time'=>date('Y-m-d H:i:s'),
        ]);
        return true;
    }

    /**
     * 获取用户地址信息
     * @param user_guid 用户guid
     * @param type 类型  1、答题活动   2、在线抽奖 
     */
    public function detail($user_guid ,$type =1, $act_id = null){
        $res = $this->select('id','act_id','user_guid','username','tel','province','city','district','address','create_time')
        ->where(function($query) use($act_id , $type){
            if($act_id){
                $query->where('act_id', $act_id);
            }
             if($type){
                $query->where('type', $type);
            }
        })
        ->where('user_guid', $user_guid)
        ->orderByDesc('id')
        ->first();
        return $res ? $res->toArray() : null;
    }

     /**
     * 判断地址是否有关键字
     */
    public function checkKeywords($data)
    {
        $keywords = ['图书馆', '博物馆','文化馆'];
        foreach ($keywords as $key => $val) {
            if (stripos($data, $val) !== false) {
                return false; //带有关键字
            }
        }
        return true; //不带有关键字
    }



}