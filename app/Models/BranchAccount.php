<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * 分馆账号
 */
class BranchAccount extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = null;


    protected $table = 'branch_account';

  /**
     *  管理员添加
     * @param $data 添加的数据
     */
    public function add($data, $field = [])
    {
        $this->branch_id = $data->branch_id;
        $this->account = $data->account;
        $this->username = $data->username;
        $this->password = md5($data->password);
        $this->manage_id = $data->manage_id;

        $this->save();
        return  $this->id;
    }

    /**
     * 修改管理员
     * @param $data 添加的数据
     */
    public function change($data, $field = [], $findWhere = [])
    {
        $res = $this->where('is_del', 1)->find($data['id']);
        if (!$res) {
            return false;
        }
        $res->username = $data->username;
        $res->save();

        return  $res->id;
    }


}
