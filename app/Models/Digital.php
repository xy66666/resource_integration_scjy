<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * 数字阅读
 */
class Digital extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'digital_read';


    /**
     * 关联数字资源类型
     */
    public function conType()
    {
        return $this->hasOne(DigitalType::class, 'id', 'type_id');
    }



    /**
     * 获取数字资源列表
     * @param is_play int 是否发布 1.发布 2.未发布    默认1
     * @param limit int 分页大小
     * @param field [] 需要获取的字段
     * @param keywords string 搜索关键词(文章标题)
     * @param type_id 类型id
     * @param start_time datetime 比赛开始时间(开始)
     * @param end_time datetime 比赛结束时间(开始)
     */
    public function lists($field = null, $keywords = null, $type_id = null, $is_play = null, $start_time = null, $end_time = null, $order = 'sort desc', $limit = 1)
    {
        if (empty($field)) {
            $field = ['id', 'is_play', 'title', 'img', 'url', 'type_id', 'sort', 'browse_num', 'create_time'];
        }

        $res = $this
            ->select($field)
            ->with('conType')
            ->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('title', 'like', '%' . $keywords . '%');
                }
            })->where(function ($query) use ($type_id, $is_play, $start_time, $end_time) {
                if ($type_id) {
                    $query->where('type_id', $type_id);
                }
                if ($is_play) {
                    $query->where('is_play', $is_play);
                }
                if ($start_time && $end_time) {
                    $query->whereBetween('create_time', [$start_time, $end_time]);
                }
            })
            ->where('is_del', 1)
            ->orderByRaw($order)
            ->paginate($limit)
            ->toArray();
        if ($res['data']) {
            foreach ($res['data'] as $key => $val) {
                $res['data'][$key]['type_name'] = $val['con_type']['type_name'];
                unset($res['data'][$key]['con_type']);
            }
        }
        return $res;
    }

    /**
     * 获取数字资源详情
     * @param id int 数字资源id
     */
    public function detail($id, $field = null)
    {
        if (empty($field)) {
            $field = ['id', 'title', 'img', 'url', 'type_id', 'sort', 'browse_num', 'create_time'];
        }
        $res = $this
            ->select($field)
            ->with('conType')
            ->where('is_del', 1)
            ->find($id);

        if ($res) {
            $res['type_name'] = $res['conType'] ? $res['conType']['type_name'] : null;
            unset($res['conType']);
        }

        return $res;
    }

    /**
     * 数字资源(获取最新上传的几个数字资源)
     */
    public function digital($limit)
    {
        return $this->select('id', 'title', 'img', 'url')->where('is_del', 1)->orderByDesc('sort')->limit($limit)->get()->toArray();
    }
}
