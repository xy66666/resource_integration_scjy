<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * 在线办证订单
 * Class BookHomeOrderModel
 * @package app\common\model
 */
class OnlineRegistrationOrderPay extends BaseModel
{
    use HasFactory;

    const CREATED_AT = null;
    const UPDATED_AT = null;


    protected $table = 'online_registration_order_pay';




}