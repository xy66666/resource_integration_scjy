<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * 采购预算增加的历史表
 * Class PostageSetModel
 * @package app\common\model
 */
class BookHomeBudgetLog extends BaseModel
{

    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = null;

    protected $table = 'book_home_budget_log';

    /**
     * 收货管理员 （关联）
     */
    public function conManage()
    {
        return $this->hasOne(Manage::class, 'id', 'manage_id');
    }

    /**
     * 采购预算变更历史
     */
    public function lists($field, $kind, $limit)
    {
        if (empty($field)) {
            $field = ['id', 'type', 'money', 'total_money', 'after_total_money', 'change_before_money', 'change_after_money', 
            'manage_id', 'create_time', 'start_time', 'end_time', 'times','month_number','start_month'];
        }

        $res = $this->select($field)
            ->with(['conManage' => function ($query) {
                $query->select('id', 'username', 'account');
            }])
            ->where('kind', $kind)
            ->orderByDesc('id')
            ->paginate($limit)
            ->toArray();

        return $res;
    }
}
