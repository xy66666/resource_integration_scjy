<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;

/**
 * 用户答题总楼层表（楼梯形式答题）
 */
class AnswerActivityUserStairsTotalNumber extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'answer_activity_user_stairs_total_number';


    /**
     * 写入爬楼梯答题记录
     */
    public function addUserStairsTotalNumber($act_id, $unit_id, $user_guid, $answer_time, $node = 1)
    {
        $res = $this->getStairsTotelData($act_id, $unit_id, $user_guid);
        $floor = 1;
        if ($res) {
            $res->answer_number = $res->answer_number + 1;

            $accuracy = $res->correct_number / $res->answer_number;
            $accuracy = sprintf("%.2f", $accuracy * 100);

            $res->accuracy = $accuracy;
            $res->times = $res->times + $answer_time;
            $res->save();

            $floor = $res->correct_number + 1;
        } else {
            $obj = new self();
            $obj->act_id = $act_id;
            $obj->unit_id = $unit_id;
            $obj->user_guid = $user_guid;
            $obj->answer_number = 1;
            $obj->correct_number = 0;
            $obj->accuracy = 0;
            $obj->times = $answer_time;

            $obj->save();
        }

        if ($node != 1 && !empty($unit_id)) {
            $this->addUserStairsTotalNumber($act_id, 0, $user_guid, $answer_time, 1); //如果不是单位活动，还需要单独新建一条总活动的排名
        }

        // if ($pattern != 1) {
        //     //如果不是独立活动，还要在记录一次总排名
        //     $res = $this->getStairsTotelData($act_id, 0, $user_guid);

        //     if ($res) {
        //         $res->answer_number = $res->answer_number + 1;

        //         $accuracy = $res->correct_number / $res->answer_number;
        //         $accuracy = sprintf("%.2f", $accuracy * 100);

        //         $res->accuracy = $accuracy;
        //         $res->save();
        //     } else {
        //         $obj = new self();
        //         $obj->act_id = $act_id;
        //         $obj->unit_id = 0;
        //         $obj->user_guid = $user_guid;
        //         $obj->answer_number = 1;
        //         $obj->correct_number = 0;
        //         $obj->accuracy = 0;
        //         $obj->save();
        //     }
        // }

        return $floor;
    }

    /**
     * 获取用户单位答题总记录
     */
    public function getAnswerTotelData($act_id, $unit_id, $user_guid)
    {
        $unit_id = $unit_id ?: 0;
        return $this->where('user_guid', $user_guid)->where('act_id', $act_id)->where('unit_id', $unit_id)->first();
    }

    /**
     * 修改爬楼梯答题总记录
     */
    public function changeUserStairsTotalNumber($act_id, $unit_id, $user_guid, $node = 1, $status = 1)
    {
        $res = $this->getStairsTotelData($act_id, $unit_id, $user_guid);
        if (!$res) {
            throw new Exception('获取答题总数据失败');
        }
        if ($status == 1) {
            $res->correct_number = $res->correct_number + 1;
        }

        $accuracy = $res->correct_number / $res->answer_number;
        $accuracy = sprintf("%.2f", $accuracy * 100);

        $res->accuracy = $accuracy;
        $res->times = AnswerActivityUserStairsAnswerRecord::getAnswerTotalTimes($user_guid, $act_id, $unit_id); //获取总执行时间
        $res->save();

        if ($node != 1 && !empty($unit_id)) {
            $this->changeUserStairsTotalNumber($act_id, 0, $user_guid, 1, $status); //如果不是单位活动，还需要单独新建一条总活动的排名
        }
        return true;
    }


    /**
     * 获取用户单位答题总记录
     */
    public function getStairsTotelData($act_id, $unit_id, $user_guid)
    {
        $unit_id = $unit_id ?: 0;
        return $this->where('user_guid', $user_guid)->where('act_id', $act_id)->where('unit_id', $unit_id)->first();
    }
}
