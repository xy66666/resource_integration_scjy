<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * 线上大赛类型模型
 * Class ArticleModel
 * @package app\common\model
 */
class ContestActivityType extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'contest_activity_type';

 


  
}
