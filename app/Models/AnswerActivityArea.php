<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;



/**
 * 活动区域模型
 */
class AnswerActivityArea extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'answer_activity_area';

    
     /**
     * 是否配置区域
     * @param act_id 
     */
    public static function isHaveArea($act_id){
        $area_info = self::where('act_id', $act_id)->where('is_del', 1)->first();
        if (empty($area_info)) {
            return '未配置区域，请先配置';
        }
        return true;
    }


    /**
     * 判断活动是否允许添加区域
     * @param act_id 活动id
     */
    public function isAllowAddArea($act_id){
        $res = AnswerActivity::where('id', $act_id)->where('is_del', 1)->value('node');//node  2 单位联盟   3 区域联盟  
        if (empty($res)) {
            return '此活动不存在';
        }
        if ($res != 3) {
            return '此活动不是区域联盟活动，不允许设置区域';
        }
        return true;
    }





}
