<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * 本馆简介
 */
class BranchInfo extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'branch_info';

    /**
     * 列表
     * @param page int 当前页
     * @param limit int 分页大小
     * @param is_main string 是否总馆  1 总馆 2 分馆 
     * @param keywords string 搜索关键词
     */
    public function lists($field = null, $keywords = null, $is_main = null, $limit = 10)
    {
        if (empty($field)) {
            $field = ['id', 'is_main', 'branch_name', 'intro', 'img', 'dispark_time', 'transport_line', 'province', 'city', 'district', 'address','lon','lat', 'tel', 'contacts', 'img', 'borrow_notice', 'create_time'];
        }
        $res = $this->select($field)
            ->where(function ($query) use ($is_main) {
                if ($is_main) {
                    $query->where('is_main', $is_main);
                }
            })
            ->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('branch_name', 'like', "%$keywords%");
                }
            })->where('is_del', 1)
            ->orderBy('is_main')
            ->orderBy('id')
            ->paginate($limit)
            ->toArray();
        return $res;
    }

    /**
     * 详情
     * @param id
     * @param field 
     */
    public function detail($id, $field = [])
    {
        if (empty($field)) {
            $field = ['id', 'is_main', 'branch_name', 'intro', 'img', 'dispark_time', 'transport_line', 'province', 'city', 'district', 'address','lon','lat', 'tel', 'contacts', 'img', 'borrow_notice', 'create_time'];
        }
        return $this->select($field)->where('id', $id)->first();
    }
}
