<?php

namespace App\Models;

use App\Http\Controllers\Admin\CommonController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use DateTimeInterface;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BaseModel extends Model
{
    use HasFactory;

    public $commonObj = null;
    public function __construct()
    {
        $this->commonObj = new CommonController();
    }
    /**
     * //重写时间格式
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    /**
     * 获取筛选列表  (暂不支持 or 查询)
     * @param   $field 获取字段
     * @param   $findWhere 筛选字段
     * @param   $order 排序字段 默认id 倒叙
     */
    public function getFilterList($field = [], $findWhere = [], $order = 'id DESC')
    {
        $res = $this->select($field)->where($findWhere)->orderByRaw($order)->get();

        if ($res->isEmpty()) {
            return $this->commonObj->returnApi(203, "暂无数据");
        }
        return $this->commonObj->returnApi(200, "获取成功", true, $res->toArray());
    }

    /**
     * 获取简单数据列表
     * @param   $field 获取字段
     * @param   $findWhere 筛选字段
     * @param   $order 排序字段 默认id 倒叙
     */
    public function getSimpleList($field = [], $findWhere = [], $page = 1, $limit = 10, $order = 'id DESC')
    {
        $res = $this->select($field)->where($findWhere)->orderByRaw($order)->paginate($limit)->toArray();
        if ($res['data']) {
            $res = $this->commonObj->disPageData($res);
            //增加序号

            foreach ($res['data'] as $key => $val) {
                if (!empty($val['intro'])) {
                    $res['data'][$key]['intro'] = strip_tags($val['intro']);
                }
                $res['data'][$key][$this->commonObj->list_index_key] = $this->commonObj->addSerialNumberOne($key, $page, $limit);
            }

            return $this->commonObj->returnApi(200, '获取成功', true, $res);
        }

        return $this->commonObj->returnApi(203, '暂无数据');
    }


    /**
     *  新增
     * @param $data 添加的数据
     * @param $field 要添加的字段名  空数据表示全部都添加
     */
    public function add($data, $field = [])
    {
        if ($field) {
            foreach ($field as $key => $val) {
                if ($val === 'token' || $val === 's' || $val === 'app_invite_code' || ($val == 'img' && empty($val))) continue; //图片，如果未传，使用数据库默认地址
                $this->$val = $data[$val];
            }
        } else {
            foreach ($data as $key => $val) {
                if ($key === 'token' || $key === 's' || $key === 'app_invite_code' || ($key == 'img' && empty($key))) continue; //图片，如果未传，使用数据库默认地址
                $this->$key = $val;
            }
        }
        return  $this->save();
    }

    /**
     * 修改
     * @param $data 修改的数据
     * @param $field 要修改的字段名  空数据表示全部都修改
     * @param $findWhere 查询条件  如果为空，则用数据里面的id 和 id值作为检索条件
     */
    public function change($data, $field = [], $findWhere = [])
    {
        $res = $this->where(function ($query) use ($findWhere, $data) {
            if ($findWhere) {
                $query->where($findWhere);
            } else {
                $query->where('id', $data['id']);
            }
        })->first();

        if (!$res) {
            return false;
        }

        if ($field) {
            foreach ($field as $key => $val) {
                if ($val === 'token' || $val === 's' || $val === 'app_invite_code' || ($val == 'img' && empty($val))) continue; //图片，如果未传，使用数据库默认地址
                $res->$val = $data[$val];
            }
        } else {
            foreach ($data as $key => $val) {
                if ($key === 'token' || $key === 's' || $key === 'app_invite_code' || ($key == 'img' && empty($key))) continue; //图片，如果未传，使用数据库默认地址
                $res->$key = $val;
            }
        }

        return  $res->save();
    }

    /**
     * 删除（软删除）
     * @param $field 字段名 默认id
     * @param $value 字段值
     * @param $is_del 是否需要软删除字段 null  不需要
     * @param $findWhere 增加条件 二维数组
     */
    public function del($value, $field = 'id', $is_del = 1, $findWhere = [])
    {
        $res = $this->where(function ($query) use ($is_del, $findWhere) {
            if (!empty($is_del)) {
                $query->where('is_del', $is_del);
            }
            if (!empty($findWhere)) {
                $query->where($findWhere);
            }
        })->where($field, $value)->first();

        if (!$res) {
            return "参数传递错误";
        }

        $res->is_del = 2;
        $result = $res->save();

        if (!$result) {
            return  "删除失败";
        }

        return true;
    }


    /**
     * 判断名称是否已经存在
     * @param $field 字段名 默认id
     * @param $value 字段值
     * @param $except_id 排除id   可选，主要是用于修改 
     * @param $is_del 是否需要软删除字段 null  不需要
     * @param $findWhere 增加条件 二维数组
     */
    public function nameIsExists($value, $field = 'name', $except_id = null, $is_del = 1, $findWhere = [])
    {
        DB::enableQueryLog();
        $res = $this->where($field, $value)->where(function ($query) use ($except_id, $is_del, $findWhere) {
            if (!empty($except_id)) {
                $query->where('id', '<>', $except_id);
            }
            if (!empty($is_del)) {
                $query->where('is_del', $is_del);
            }
            if (!empty($findWhere)) {
                $query->where($findWhere);
            }
        })->first();
        return $res;
    }



    /**
     * 添加(修改)相应数据 ，删除多余的，增加新增的  （对于数据库设计第三范式，删除中间表记录）
     * @param by_id_name 条件字段名
     * @param by_id_value 条件字段值
     * @param change_id_name 需要修改的字段名
     * @param change_id_value 需要修改的字段值，多个 逗号 拼接
     * @param $type 类型  add 新增，直接增加   change 需要查询
     */
    public function interTableChange($by_id_name, $by_id_value, $change_id_name, $change_id_value, $type = 'change')
    {
        $change_id_value = empty($change_id_value) ? [] : explode(',', $change_id_value);
        $change_id_value = array_filter(array_unique($change_id_value));
        if ($type == 'add') {
            $change_id_value_ed = [];
        } else {
            $change_id_value_ed = $this->where($by_id_name, $by_id_value)->groupBy($change_id_name)->pluck($change_id_name)->toArray();
        }
        $change_id_value_del = array_diff($change_id_value_ed, $change_id_value); //对比返回在 $change_id_value_ed 中但是不在 $change_id_value 的数据删除
        $change_id_value_add = array_diff($change_id_value, $change_id_value_ed); //对比返回在 $change_id_value 中但是不在 $change_id_value_ed 的数据添加
        if (!empty($change_id_value_del)) $this->whereIn($change_id_name, $change_id_value_del)->where($by_id_name, $by_id_value)->delete();
        $data = [];
        foreach ($change_id_value_add as $key => $val) {
            $data[$key][$by_id_name] = $by_id_value;
            $data[$key][$change_id_name] = $val;
            $data[$key]['create_time'] = date('Y-m-d H:i:s');
        }
        if (!empty($data)) $this->insert($data);
    }


    /**
     * 添加和取消热门 推荐 （发布与取消发布）  返回的数组
     * @param $ids id    all 全部 其余不是
     * @param $field  字段名
     * @param $type  是否XX  1 XX  2 XX
     * @param $is_del 是否需要软删除字段 null  不需要
     * @param $condition 其他修改条件
     */
    public function resumeAndCancel($ids, $field = 'is_recom', $type = null, $is_del = 1, $condition = [])
    {
        if ($ids == 'all') {
            $result = $this->where(function ($query) use ($is_del, $condition) {
                if (!empty($is_del)) {
                    $query->where('is_del', $is_del);
                }
                if (!empty($condition)) {
                    $query->where($condition);
                }
            })->update([$field => $type, 'change_time' => date('Y-m-d H:i:s')]);
        } else {
            $ids = explode(',', $ids);
            $result = $this->where(function ($query) use ($is_del, $condition) {
                if (!empty($is_del)) {
                    $query->where('is_del', $is_del);
                }
                if (!empty($condition)) {
                    $query->where($condition);
                }
            })->whereIn('id', $ids)
                ->update([$field => $type, 'change_time' => date('Y-m-d H:i:s')]);
        }
        if ($result === false) {
            return [202, "失败"];
        }

        return [200, "成功"];
    }

    /**
     * 违规 与 取消违规操作
     * @param id int 申请id
     * @param violate_reason string 违规原因
     */
    public function violateAndCancel($id, $violate_reason = null)
    {

        $res = $this->where('id', $id)->first();
        if (!$res) {
            throw new Exception("参数传递错误");
        }
        if ($res->is_violate == 1 && empty($violate_reason)) {
            throw new Exception("违规理由不能为空");
        }

        if ($res->is_violate == 1) {
            $res->is_violate = 2;
            $res->violate_reason = $violate_reason;
            $res->violate_time = date('Y-m-d H:i:s');
        } else {
            $res->is_violate = 1;
            $res->violate_reason = null;
        }
        $res->save();
        return $res;
    }


    /**
     * 次数统计
     * @param $start_time   开始时间
     * @param $end_time  结束时间
     * @param $time_field  时间字段
     * @param $count_field  统计字段
     * @param $is_del 是否需要软删除字段 null  不需要
     * @param $condition 其他修改条件
     */
    public function timesStatistics($year = null, $month = null, $start_time = null, $end_time = null, $time_field = 'create_time', $count_field = 'id', $is_del = 1, $condition = [])
    {
        if (empty($start_time)) {
            $start_time = date('Y-m-d 00:00:00');
            $end_time = date('Y-m-d H:i:s');
        }
        //先按年统计
        if (empty($month) && $year) {
            $field = ['id', DB::raw("DATE_FORMAT($time_field,'%Y-%m') as times"), DB::raw("count($count_field) as number")];
        } elseif (date('Y-m-d', strtotime($start_time)) == date('Y-m-d', strtotime($end_time))) {
            $field = ['id', DB::raw("DATE_FORMAT($time_field,'%Y-%m-%d %H') as times"), DB::raw("count($count_field) as number")];
        } else {
            $field = ['id', DB::raw("DATE_FORMAT($time_field,'%Y-%m-%d') as times"), DB::raw("count($count_field) as number")];
        }
        $res = $this->select($field)->where(function ($query) use ($start_time, $end_time, $time_field) {
            if ($start_time && $end_time) {
                $query->whereBetween($time_field, [$start_time, $end_time]);
            }
        })->where(function ($query) use ($is_del, $condition) {
            if (!empty($is_del)) {
                $query->where('is_del', $is_del);
            }
            if (!empty($condition)) {
                $query->where($condition);
            }
        })
            ->orderBy('times')
            ->groupBy('times')
            ->get()
            ->toArray();

        return CommonController::disCartogramDayOrHourData($res, empty($month) ? $year : null, $start_time, $end_time, 'times', 'number');
    }

    /**
     * 总数数统计
     * @param $start_time   开始时间
     * @param $end_time  结束时间
     * @param $is_del 是否需要软删除字段 null  不需要
     * @param $condition 其他修改条件
     */
    public function totalNumberStatistics($start_time = null, $end_time = null, $time_field = 'create_time', $count_field = 'id', $is_del = 1, $condition = [])
    {
        if (empty($start_time)) {
            $start_time = date('Y-m-d 00:00:00');
            $end_time = date('Y-m-d H:i:s');
        }
        $res = $this->where(function ($query) use ($start_time, $end_time, $time_field) {
            if ($start_time && $end_time) {
                $query->whereBetween($time_field, [$start_time, $end_time]);
            }
        })->where(function ($query) use ($is_del, $condition) {
            if (!empty($is_del)) {
                $query->where('is_del', $is_del);
            }
            if (!empty($condition)) {
                $query->where($condition);
            }
        })
            ->count($count_field);
        return $res;
    }
    /**
     * 修改排序
     * @param orders 排序   json格式数据  里面2个参数  id id   order 序号
     * 
     * json 格式数据  [{"id":1,"order":1},{"id":2,"order":2},{"id":3,"order":3}]
     * 数组 格式数据  [['id'=>1 , 'order'=>1] , ['id'=>2 , 'order'=>2] , ['id'=>3 , 'order'=>3]]
     */
    public function orderChange($orders)
    {
        $orders = json_decode($orders, true);
        foreach ($orders as $key => $val) {
            $this->where('id', $val['id'])->update(['order' => $val['order']]);
        }
        return true;
    }
}
