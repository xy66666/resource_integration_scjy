<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * 线上大赛原创申明模型
 * Class ArticleModel
 * @package app\common\model
 */
class CompetiteActivityDeclare extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'competite_activity_declare';

    /**
     * 获取原创申明
     * @param con_id 大赛id
     * @param lib_id 图书馆id
     */
    public function detail($con_id , $lib_id = null){
        return $this->where('con_id' , $con_id)->where(function($query) use($lib_id){
            if($lib_id){
                $query->where('lib_id' , $lib_id);
            }
        })->first();
    }


    
    /**
     * 原创申明修改（新增）
     * @param $data 修改的数据
     * @param $field  array 要修改的字段名  空数据表示全部都修改
     * @param $findWhere 查询条件  如果为空，则用数据里面的id 和 id值作为检索条件
     */
    public function change($data, $field = [], $findWhere = [])
    {
        $res = $this->detail($data['id'] , $data['lib_id']);
        if($res){
            $res->content = $data['content'];
            return $res->save();
        }
        $this->con_id = $data['id'];
        $this->lib_id = $data['lib_id'] ?: null;
        $this->content = $data['content'];
        return $this->save();
    }
    
}
