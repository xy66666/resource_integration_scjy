<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class RegisterVerify extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = null;


    protected $table = 'register_verify';
    /**
     * 获取短信模板
     * @param $state
     * @return string
     */
    public function getTemplate($state)
    {
        switch ($state) {
            case '1': //在线办证
                $template = '1215778';
                break;
        }
        return $template;
    }

    /**
     * 添加验证码
     * 
     */
    public function addVerify($tel, $number, $expir_time)
    {
        //发送成功之后，先删除之前此电话号码的验证码
        $this->where('phone', $tel)->delete();

        $obj = new self();
        $obj->code = $number;
        $obj->phone = $tel;
        $obj->expir_time = $expir_time; //储蓄验证码
        $obj->save();
        return $obj->id;
    }
}
