<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * 用户答题表
 * Class SurveyApplyModel
 * @package app\common\model
 */
class SurveyReply extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = null;

    public $table = "survey_reply";

    /*关联用户*/
    public function conUser()
    {
        return $this->hasOne(UserInfo::class, 'id', 'user_id');
    }

    /**
     * 获取自定义文件问题答案
     * @param pro_id 问题id
     */
    public function getUserDefinedAnswer($pro_id)
    {
        $res = $this->with(['conUser' => function ($query) {
            $query->select('id', 'wechat_id', 'account_id');
        }])->where('pro_id', $pro_id)
            ->where(function($query){
                $query->whereNotNull('content')->where('content' , '!=' , '');
            })
            ->get()
            ->toArray();
        $data = [];
        foreach ($res as  $key => $val) {
            if ($val['con_user']['wechat_id']) {
                $data[$key]['nickname'] = UserWechatInfo::where('id', $val['con_user']['wechat_id'])->value('nickname');
            } elseif ($val['con_user']['account_id']) {
                $data[$key]['nickname'] = UserLibraryInfo::where('id', $val['con_user']['account_id'])->value('username');
            } else {
                $data[$key]['nickname'] = '';
            }
            $data[$key]['content'] = $val['content'];
            $data[$key]['create_time'] = $val['create_time'];
            // unset($data[$key]['con_user']);
        }
        return $data;
    }

    /**
     * 获取用户回答问卷调查信息
     * @param $sur_id  问卷id
     * @param $keywords  检索问题
     */

    public function getReplyList($sur_id, $keywords = null)
    {
        $res = $this->from('survey_reply as r')->select('r.answer', 'r.content', 'r.create_time', 'r.user_id', 'p.problem', 'p.type')
            ->join('survey_problem as p', 'p.id', '=', 'r.pro_id')
            ->where('r.sur_id', $sur_id)
            ->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('p.problem', 'like', "%$keywords%");
                }
            })
            ->orderBy('r.create_time')
            ->get()
            ->toArray();

        $answerModel = new SurveyAnswer();
        foreach ($res as $key => &$val) {
            if ($val['type'] == 2 || $val['type'] == 3) {
                if (empty($val['content'])) {
                    $val['content'] = $answerModel->where('id', $val['answer'])->value('answer');
                }

                $val['type'] = $val['type'] == 2 ? '选择' : '填空题';
            } elseif ($val['type'] == 1) {
                $answer_id = explode('|', $val['answer']);
                $content = '';
                for ($i = 1; $i <= count($answer_id); $i++) {
                    $answer = $answerModel->where('id', $answer_id[$i - 1])->value('answer');
                    $answer = empty($answer) ? $val['content'] : $answer;
                    $content .= '回复' . $i . '：' . $answer . '；';
                }
                $val['content'] = trim($content, '；');
                $val['type'] = '多选';
            }


            $val['nickname'] = UserInfo::getWechatField($val['user_id'], 'nickname');
        }
        return $res;
    }
}
