<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * 图书到家管理员管辖书店表
 * @package app\common\model
 */
class BookHomeManageShop extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = null;

    protected $table = 'book_home_manage_shop';

    /**
     * 根据书店id，获取同组管理员id
     * @param shop_id 书店id 数组，只要满足一个即可
     */
    public function getMangeIdByShopId($shop_id)
    {
        if (empty($shop_id)) {
            return [];
        }
        $shop_id = !is_array($shop_id) ? explode(',', $shop_id) : $shop_id;
        $data = $this->whereIn('shop_id', $shop_id)
            ->groupBy('manage_id')
            ->pluck('manage_id')
            ->get()
            ->toArray();
        return $data;
    }

    /**
     * 增加管理员 书店的权限
     * @param mange_id 管理员id
     * @param shop_id 书店id
     */
    public function addManageShopAuth($manage_id, $shop_id)
    {
        $this->manage_id = $manage_id;
        $this->shop_id = $shop_id;
        $res = $this->save();
        return $res;
    }
}
