<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * 本馆简介统计访问量
 */
class BranchAccessNum extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = null;


    protected $table = 'branch_access_num';



    /**
     * 记录每日浏览量
     * @param id 数字阅读id
     */
    public function dayBrowse($id)
    {
        //获取当前年月日
        $yyy = date('Y');
        $mmm = intval(date('m'));
        $ddd = intval(date('d'));

        //先更新
        $where['branch_id'] = $id;
        $where['yyy'] = $yyy;
        $where['mmm'] = $mmm;
        $where['ddd'] = $ddd;
        $res = $this->where($where)->first();
        if ($res) {
            //添加数据库
            $res->number = $res->number + 1;
            $res->save();
        }else{
            $this->branch_id = $id;
            $this->yyy = $yyy;
            $this->mmm = $mmm;
            $this->ddd = $ddd;
            $this->save();
        }
        return true;
    }
}
