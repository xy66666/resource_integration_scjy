<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/*活动model*/

class AccessNum extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = null;


    protected $table = 'access_num';

    /**
     * 记录浏览量
     * @param type  类型  1 微信  2 web 
     * @return void
     */
    public function add($type = 1, $data = [])
    {
        $time = time();
        $year = date("Y", $time);
        $month = date("m", $time);
        $day = date("d", $time);

        $browse = $this->where('yyy', $year)->where('mmm', $month)->where('ddd', $day)->first();

        $number = mt_rand(1, 5);
        $type = $type == 1 ? 'wx_num' : 'web_num';
        if ($browse) {
            $browse->$type = $browse->$type + $number;
            $browse->total_num =  $browse->total_num + $number;
            $browse->save();
        } else {
            $this->yyy = $year;
            $this->mmm = $month;
            $this->ddd = $day;
            $this->$type = 1;
            $this->total_num = 1;
            $this->save();
        }
    }

    /**
     * 访问人数
     * @param $start_time 开始时间
     * @param $end_time 结束时间
     */
    public function getAccessNumber($start_time = null, $end_time = null)
    {
        $number = $this->where(function ($query) use ($start_time, $end_time) {
            if ($start_time && $end_time) {
                $query->whereBetween('create_time', [$start_time, $end_time]);
            }
        })->sum('total_num');
        return $number;
    }

    /**
     * 访问统计(折线图)
     * @param start_time  开始时间
     * @param end_time  结束时间
     * @param node  方式  1 年  2 月   3 日  如果为空，则根据实际来
     * @param access_field  访问字段 web_num   wx_num  total_num
     */
    public function getAccessStatistics($start_time = null, $end_time = null,  $node = null, $access_field = 'total_num')
    {
        if (empty($start_time)) {
            $start_time = date('Y-m-d 00:00:00');
            $end_time = date('Y-m-d H:i:s');
        }
        if ($node == 3 || (empty($node) && date('Y-m-d', strtotime($start_time)) == date('Y-m-d', strtotime($end_time)))) {
            $field = ['id', DB::raw("DATE_FORMAT(create_time,'%Y-%m-%d %H') as times"), DB::raw("sum($access_field) as count")];
        } elseif ($node == 2 || (empty($node) && date('Y-m', strtotime($start_time)) == date('Y-m', strtotime($end_time)))) {
            $field = ['id', DB::raw("DATE_FORMAT(create_time,'%Y-%m-%d') as times"), DB::raw("sum($access_field) as count")];
        } else {
            $field = ['id', DB::raw("DATE_FORMAT(create_time,'%Y-%m') as times"), DB::raw("sum($access_field) as count")];
        }
        $res = $this->select($field)->where(function ($query) use ($start_time, $end_time) {
            if ($start_time && $end_time) {
                $query->whereBetween('create_time', [$start_time, $end_time]);
            }
        })
            ->orderBy('times')
            ->groupBy('times')
            ->get()
            ->toArray();
        return $res;
    }
}
