<?php
namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**活动类型model */
class ActivityType extends BaseModel
{

    use HasFactory;

    const CREATED_AT='create_time';
    const UPDATED_AT='change_time';

    
    public $table = 'activity_type';

    /*关联签到信息*/
    public function activitylistByTypeId(){
        return $this->hasMany(Activity::class,'type_id','id');
    }

    /**
     * 活动类型统计(折线图)
     * @param $act_id 活动id
     * @param start_time 单位id 开始时间
     * @param end_time 单位id 结束时间
     */
    public function actTypeStatistics($start_time = null, $end_time = null)
    {
        $res = $this->select('id' , 'type_name')->where('is_del' , 1)->get();
        $activityModel = new Activity();
        $total_number = $activityModel->where('is_del' , 1)->whereBetween('create_time' , [$start_time , $end_time])->count();
        foreach($res as $key=>$val){
            if($total_number){
            $number = $activityModel->where('type_id' , $val['id'])->where('is_del' , 1)->whereBetween('create_time' , [$start_time , $end_time])->count();
            $res[$key]['number'] = $number;
            $res[$key]['proportion'] = sprintf("%.2f" , $number / $total_number * 100).'%';
            }else{
                $res[$key]['number'] = 0;
                $res[$key]['proportion'] = '0%';
            }
        }

        return $res;
    }


}