<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Live\ServerAPIController;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Cache;

/**
 * 视频直播点赞模型
 * Class ArticleModel
 * @package app\common\model
 */
class VideoLiveVote extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'video_live_vote';

    /**
     * 判断是否点赞
     * @param user_id 用户id
     * @param act_id 活动id
     */
    public function isVote($user_id, $act_id)
    {
        $res = $this->where('user_id', $user_id)->where('act_id', $act_id)->first();
        return $res;
    }

    /**
     * 点赞和取消点赞
     * @param user_id 用户id
     * @param act_id 活动id
     */
    public function voteAndCancel($user_id, $act_id)
    {
        $res = $this->where('user_id', $user_id)->where('act_id', $act_id)->first();
        if ($res) {
            $result = $this->where('id', $res->id)->delete();
            $msg = '取消点赞';
        } else {
            $this->user_id = $user_id;
            $this->act_id = $act_id;
            $result = $this->save();
            $msg = '点赞';
        }
        if ($result) {
            return [200, $msg . '成功'];
        }
        return [202, $msg . '失败'];
    }
}
