<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * 用户订单地址
 * Class OrderAddressModel
 * @package app\common\model
 */
class BookHomeOrderAddress extends BaseModel
{

    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'book_home_order_address';


    /**
     * 获取订单收货地址
     * @param order_id 订单id
     */
    public function getOrderAddress($order_id)
    {
        return $this->select('username', 'tel', 'province', 'city', 'district', 'street', 'address')
            ->where('order_id', $order_id)
            ->first();
    }


    /**
     * 修改真实发货地址信息
     * @param order_id 订单id
     * @param $now_address  当前地址 
     * @param $send_address  发货地址 
     */
    public function changeOrderAddress($order_id, $now_address, $send_address)
    {
        $this->where('order_id', $order_id)
            ->update([
                'username' => $now_address['username'],
                'province' => $now_address['province'],
                'tel' => $now_address['tel'],
                'city' => $now_address['city'],
                'district' => $now_address['district'],
                'street' => $now_address['street'],
                'address' => $now_address['address'],
                'send_username' => $send_address['username'],
                'send_province' => $send_address['province'],
                'send_tel' => $send_address['tel'],
                'send_city' => $send_address['city'],
                'send_district' => $send_address['district'],
                'send_street' => $send_address['street'],
                'send_address' => $send_address['address'],
                'manage_id' => request()->manage_id,
                'change_time' => date('Y-m-d H:i:s'),
            ]);
    }
}
