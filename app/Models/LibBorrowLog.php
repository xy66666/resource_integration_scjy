<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * 图书馆 借阅记录（纯演示用）
 */
class LibBorrowLog extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = null;

    protected $table = 'lib_borrow_log';


    /**
     * 书籍借阅
     * @param $data 用户数据
     * @param $user_id 用户id
     * @param $account 读者证号
     * @param $place_code_id 场所码id
     */
    public function borrowBook($data, $account)
    {
        $this->lib_id = !empty($data['lib_id']) ?  $data['lib_id'] : "0";
        $this->lib_name = config('other.lib_name');
        $this->cur_local = '中心馆';
        $this->cur_local_name = '中心馆';
        $this->cur_lib = '中心馆';
        $this->cur_lib_name = '中心馆';
        $this->owner_local = '中心馆';
        $this->owner_local_name = '中心馆';
        $this->book_name = $data['book_name'];
        $this->author = $data['author'];
        $this->pre_time = $data['pre_time'];
        $this->press = $data['press'];
        $this->price = $data['price'];
        $this->callno = $data['callno'];
        $this->barcode = $data['barcode'];
        $this->isbn = $data['isbn'];
        $this->classno = $data['classno'];
        $this->borrow_time = date('Y-m-d H:i:s');
        $this->expire_time = date('Y-m-d H:i:s', strtotime('+1 month'));
        $this->status = 2;

        //获取用户id
        if (!empty($this->request->user_info['id'])) {
            $user_id = $this->request->user_info['id'];
        } else {
            $account_id = UserLibraryInfo::where('account', $account)->value('id');
            $user_id = UserInfo::where('account_id', $account_id)->value('id');
        }
        $this->user_id = $user_id;
        $this->account = $account; //读者证号

        return $this->save();
    }
    /**
     * 书籍借阅
     * @param $data 用户数据
     * @param $user_id 用户id
     * @param $account 读者证号
     * @param $place_code_id 场所码id
     */
    public function returnBook($barcode , $returnDate =null)
    {
        $res = $this->where('barcode', $barcode)->orderByDesc('id')->where('status', 2)->first();
        if ($res) {
            $res->status = 1;
            $res->return_time = !empty($returnDate) ? $returnDate : date('Y-m-d H:i:s');
            $result =  $res->save();
            if ($result) {
                return true;
            }
        }
        return false;
    }


    /**
     * 借阅列表
     * @param keywords  检索条件
     * @param start_time 申请开始时间   数据格式    2020-05-12 12:00:00
     * @param end_time 申请结束时间     数据格式    2020-05-12 12:00:00
     * @param status  状态  1 已归还  2 借阅中
     * @param page  页数，默认为1，
     * @param limit  条数，默认显示 10条
     */
    public function lists($keywords, $status = null, $start_time = null, $end_time = null, $limit = 10)
    {
        $res = $this->select(['id','book_name', 'author', 'price', 'press','img', 'isbn', 'barcode', 'pre_time', 'callno as book_num', 'classno', 'borrow_time','create_time', 'expire_time', 'return_time','account'])
            ->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('book_name', 'like', "%$keywords%")
                        ->Orwhere('barcode', 'like', "%$keywords%")
                        ->Orwhere('isbn', 'like', "%$keywords%")
                        ->Orwhere('account', 'like', "%$keywords%");
                }
            })->where(function ($query) use ($status, $start_time, $end_time) {
                if ($status) {
                    $query->where('status', $status);
                }
                if ($start_time && $end_time) {
                    $query->whereBetween('create_time', [$start_time, $end_time]);
                }
            })->orderByDesc('id')
            ->paginate($limit)
            ->toArray();
        return $res;
    }
}
