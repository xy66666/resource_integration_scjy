<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * 书籍类型（只有22大类）
 */
class BookTypes extends BaseModel
{
    use HasFactory;

    const CREATED_AT=null;
    const UPDATED_AT=null;

    
    protected $table = 'book_types';


}
