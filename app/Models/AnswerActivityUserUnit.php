<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * 答题活动单位管理
 */
class AnswerActivityUserUnit extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'answer_activity_user_unit';


    /**
     * 获取用户填写的信息
     * @param user_guid 用户guid
     * @param act_id 活动id
     */
    public function getUserUnitInfo($user_guid, $act_id)
    {
        $res = $this->select('username','tel','id_card','reader_id','unit_id','create_time')->where('user_guid', $user_guid)->where('act_id', $act_id)->first();

        if (empty($res)) {
            return [];
        }

        return $res->toArray();
    }

    
   /**
     * 添加用户单位
     * @param user_guid 用户guid
     * @param act_id 活动id
     * @param unit_id 单位id 
     */
    public function addUserUnit($user_guid , $act_id ,$unit_id){
        $res = $this->where('user_guid', $user_guid)->where('act_id', $act_id)->first();
        if($res){
            $res->unit_id = $unit_id;
            return $res->save();
        }else{
            $this->user_guid = $user_guid;
            $this->act_id = $act_id;
            $this->unit_id = $unit_id;
            return $this->save();
        }
    }

    /**
     * 判断是否选择了所属图书馆 和 是否需要用户信息
     * @param user_guid 用户guid
     * @param act_id 活动id
     * @param unit_id 单位id  如果传了，检索是就不要单位，没传（并且不为 0）就要   
     * @param is_need_unit 是否需要选择图书馆   1 需要  2 不需要 （单活动模式，默认不需要）
     */
    public function isSelectUnit($user_guid, $act_id, $unit_id = null, $is_need_unit = 2)
    {
        $res = $this->where('user_guid', $user_guid)->where('act_id', $act_id)
            ->where(function ($query) use ($is_need_unit,$unit_id) {
                // 如果传了，检索是就不要单位，没传就要
                if ($is_need_unit == 1) {
                    if ($unit_id) {
                        $query->whereNull('unit_id');
                    } elseif (empty($unit_id) && $unit_id !== 0) {
                        //unit_id 0 表示 第一次获取信息时判断
                        $query->whereNotNull('unit_id');
                    }
                }
            })->first();

        if (empty($res)) {
            return false;
        }

        return $res;
    }

    /**
     * 判断是否选择了所属图书馆 和 是否需要用户信息
     * @param user_guid 用户guid
     * @param act_id 活动id
     * @param real_info 活动所需参数
     */
    public function needRealInfoAndUnit($user_guid, $act_info)
    {
        if ($act_info['is_ushare'] == 1) {
            //只需要查看是否选择图书馆即可
            if ($act_info['is_need_unit'] == 2) {
                return ['real_info' => null, 'unit_id' => null]; //不需要输入信息，也不需要输入单位
            } else {
                //判断是否选择了所属图书馆
                if ($act_info['node'] == 1) {
                    return ['real_info' => null, 'unit_id' => null];
                }

                $isSelectUnit = $this->isSelectUnit($user_guid, $act_info['id'], 0);
                if (!empty($isSelectUnit['unit_id'])) {
                    return ['real_info' => null, 'unit_id' => null]; //不需要输入信息，也不需要输入单位
                }
                return ['real_info' => null, 'unit_id' => true]; //需要填写单位，不需要其他信息
            }
        } else if ($act_info['is_ushare'] == 2) {
            //需要查看是否选择图书馆，还需要查看是否填写了用户信息
            //判断是否选择了所属图书馆
            $isSelectUnit = $this->isSelectUnit($user_guid, $act_info['id'], 0);
            if ($act_info['node'] == 1 || !empty($isSelectUnit['unit_id']) || $act_info['is_need_unit'] == 2) {
                $unit_id = null; //不需要填写单位
            } else {
                $unit_id = true; //需要填写单位，不需要其他信息
            }

            //是否需要其他信息
            if (empty($act_info['real_info'])) {
                $real_info = null; //不需要任何信息
            } else {
                $real_info_arr = explode('|', $act_info['real_info']);
                $activityModel = new AnswerActivity();
                $activityApplyParam = $activityModel->getAnswerActivityApplyParam();
                $real_info = null;

                foreach ($real_info_arr as $key => $val) {
                    //判断需要那些 参数，或者之前是输入了信息，现在增加了信息
                    if (empty($isSelectUnit[$activityApplyParam[$val - 1]['field']])) {
                        $real_info[] = $activityApplyParam[$val - 1];
                    }
                }
            }
            return ['real_info' => $real_info, 'unit_id' => $unit_id];
        }
    }

    /**
     * 根据活动和用户id，获取单位名称
     * @param user_guid 用户guid
     * @param act_id 活动id
     */
    public function getUnitName($act_id , $user_guid){
        $res = $this->where('user_guid' , $user_guid)->where('act_id' , $act_id)->pluck('unit_id');
        if(empty($res)){
            return '';
        }
        $unit_name_all = '';
        foreach($res as $key=>$val){
            $unit_name = AnswerActivityUnit::where('id' , $val)->value('name');
            $unit_name_all .= '；'.$unit_name;
        }
        return trim($unit_name_all , '；');
    }
}
