<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * 大赛单位表
 */
class ContestActivityUnit extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'contest_activity_unit';


    /**
     * 获取单位性质 
     * $type 单位性质    1 图书馆  2 文化馆  3 博物馆
     */
    public function getNatureName($type = null)
    {
        $data = ['1' => '图书馆', '文化馆', '博物馆'];
        if ($type) {
            return $data[$type];
        }
        return $data;
    }
    /**
     * 获取单位性质 id
     * $type 单位性质    1 图书馆  2 文化馆  3 博物馆
     */
    public function getNatureIdByName($name)
    {
        $data = ['图书馆' => 1, '文化馆' => 2, '博物馆' => 3];
        return $data[$name];
    }


    /**
     * 根据单位id获取单位名称
     * @param $content
     */
    public function getUnitNameByUnitId($unit_id)
    {
        return $this->where('id', $unit_id)->value('name');
    }


    /**
     * 判断活动是否允许添加单位
     * @param con_id 活动id
     */
    public function isAllowAddUnit($con_id)
    {
        $res = ContestActivity::where('id', $con_id)->where('is_del', 1)->value('node');
        if (empty($res)) {
            return '此活动不存在';
        }
        if ($res == 1) {
            return '此活动为独立活动模式，不允许操作单位';
        }
        return true;
    }

    /**
     * 获取单位列表
     * @param field int 获取字段信息  数组
     * @param con_id int 活动id
     * @param page int 当前页
     * @param limit int 分页大小
     * @param nature int 单位性质   1 图书馆  2 文化馆  3 博物馆
     * @param start_time datetime 创建时间范围搜索(开始) 
     * @param end_time datetime 创建时间范围搜索(结束) 
     * @param keywords string 搜索关键词(类型名称)
     */
    public function lists($field, $con_id, $keywords, $nature, $start_time, $end_time, $page, $limit)
    {
        if (empty($field)) {
            $field = ['id', 'con_id', 'nature', 'name', 'img', 'intro', 'originals', 'letter', 'order', 'create_time'];
        }
        $res = $this->select($field)
            ->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('name', 'like', '%' . $keywords . '%');
                }
            })->where(function ($query) use ($con_id, $nature, $start_time, $end_time) {
                if ($con_id) {
                    $query->where('con_id', $con_id);
                }
                if ($nature) {
                    $query->where('nature', $nature);
                }
                if ($start_time && $end_time) {
                    $query->whereBetween('create_time', [$start_time, $end_time]);
                }
            })
            ->where('is_del', 1)
            ->orderBy('order')
            ->paginate($limit)
            ->toArray();

        $controllerObj = new Controller();
        $res = $controllerObj->disPageData($res);

        return $res;
    }

    /**
     * 获取单位详情
     * @param con_id int 活动id
     */
    public function detail($id, $field = [])
    {
        if (empty($field)) {
            $field = ['id', 'con_id', 'nature', 'name', 'img', 'intro', 'originals', 'letter', 'order', 'create_time'];
        }
        $res = $this->select($field)
            ->where('is_del', 1)
            ->where('id', $id)
            ->first();


        if (!empty($res)) {
            $res['nature_name'] = $this->getNatureName($res['nature']);
        }

        return $res;
    }


    /**
     * 获取单位统计数据
     *  //馆内答题   答题进度：  1/5      爬楼梯：答题进度 52/100        轮次：已答轮次：5轮     
     */
    // public function unitAnswerNumber($pattern, $user_guid, $con_id, $unit_id)
    // {
    //     switch ($pattern) {
    //         case 1:
    //             $model = new AnswerActivityUserAnswerTotalNumber();
    //             break;
    //         case 2:
    //             $model = new AnswerActivityUserCorrectTotalNumber();
    //             break;
    //         case 3:
    //             $model = new AnswerActivityUserStairsTotalNumber();
    //             break;
    //     }
    //     $unit_id = $unit_id ?: 0;
    //     return $model->where('user_guid', $user_guid)->where('con_id', $con_id)->where('unit_id', $unit_id)->value('correct_number');
    // }


}
