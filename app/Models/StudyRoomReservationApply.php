<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * 用户预约表
 * Class ReservationApplyModel
 * @package app\common\model
 */
class StudyRoomReservationApply extends BaseModel
{

    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    public $table = 'study_room_reservation_apply';

    /**
     * 关联用户数据
     */
    public function conUser()
    {
        return $this->hasOne(UserInfo::class, 'id', 'user_id');
    }

    /**
     * 关联排班表
     */
    public function conSchedule()
    {
        return $this->hasOne(StudyRoomReservationSchedule::class, 'id', 'schedule_id');
    }

    /**
     * 关联特殊排班表
     */
    public function conSpecialSchedule()
    {
        return $this->hasOne(StudyRoomReservationSpecialSchedule::class, 'id', 'schedule_id');
    }

    /*关联预约信息*/
    public function conReservation()
    {
        return $this->hasOne(StudyRoomReservation::class, 'id', 'reservation_id');
    }

    /**
     * 判断当前未过期已预约次数
     * @param user_id   用户id
     * @param reservation_id  预约id
     */
    public function getReservationNotOutNumber($user_id, $reservation_id)
    {
        $number = $this->where('reservation_id', $reservation_id)
            ->where('user_id', $user_id)
            ->where(function ($query) {
                $query->whereIn('status', [1, 3]);
            })->count();
        return $number;
    }

    /**
     * 获取未审核预约个数
     * @param act_id 活动id  空表示全部
     */
    public function getUncheckedReservationNumber($reservation_id = null)
    {
        return $this->where(function ($query) use ($reservation_id) {
            if ($reservation_id) {
                $query->where('reservation_id', $reservation_id);
            }
        })->where('status', 3)->count();
    }

    /**
     * 获取预约数量
     * @param reservation_id int 预约id
     * @param status int 用于筛选用户列表  预约状态   1.已通过  3待审核，4已拒绝  5 已过期 , 6 已签到
     */
    public function getMakeNumber($reservation_id = '', $status = [1, 3, 5, 6, 7])
    {
        $res = $this->where('reservation_id', $reservation_id)
            ->where(function ($query) use ($status) {
                if ($status) {
                    $query->whereIn('status', $status);
                }
            })
            ->count();
        return $res;
    }

    /**
     * 预约申请列表
     * @param reservation_id int 预约id
     * @param limit int 分页大小
     * @param keywords string 搜索关键词
     * @param start_date date 搜索开始时间
     * @param end_date date 搜索结束时间
     * @param status int 用于筛选用户列表  预约状态   1.已通过  3待审核，4已拒绝  5 已过期 , 6 已签到 7 已签退
     * @param is_violate int 是否违规  1正常 2违规 默认1
     */
    public function lists($reservation_id = '', $user_id = '', $status = [], $is_violate = '', $keywords = '', $start_date = '', $end_date = '', $limit = 10)
    {
        $res = $this->from($this->getTable() . ' as a')
            ->leftJoin('user_info as b', 'a.user_id', '=', 'b.id')
            ->leftJoin('user_wechat_info as w', 'b.wechat_id', '=', 'w.id')
            ->leftJoin('manage as m', 'a.manage_id', '=', 'm.id')
            ->select(
                'a.id',
                'a.user_id',
                'a.account_id',
                'a.number',
                'a.reservation_id',
                'a.schedule_id',
                'a.schedule_type',
                //    'reservation_violate_num as violate_num',
                'a.create_time',
                'a.make_time',
                'a.status',
                'a.reason',
                'a.change_time',
                'a.is_violate',
                'a.violate_reason',
                'a.tel',
                'a.status',
                'a.sign_time',
                'a.sign_end_time',
                'a.unit',
                'a.remark',
                'a.id_card',
                'a.img',
                'a.sex',
                'a.age',
                'a.tel',
                'a.reader_id',
                'a.username',
                'w.head_img', 
                'w.nickname',
                'm.' . Manage::$manage_name . ' as manage_name',
                DB::raw('if(status = 3,0,1) as sort')
            )
            ->with(['conSchedule' => function ($query) {
                $query->select('id', 'week', 'start_time', 'end_time');
            }, 'conSpecialSchedule' => function ($query) {
                $query->select('id', 'date', 'start_time', 'end_time');
            }])
            ->where(function ($query) use ($keywords) {
                $query->where('a.tel', 'like', "%$keywords%")
                    ->orWhere('a.id_card', 'like', "%$keywords%")
                    ->orWhere('a.unit', 'like', "%$keywords%")
                    ->orWhere('w.nickname', 'like', "%$keywords%");
            })
            ->where(function ($query) use ($reservation_id, $user_id, $status, $is_violate, $start_date, $end_date) {
                if ($start_date && $end_date) {
                    $query->whereBetween('make_time', [$start_date, $end_date]);
                }
                if ($status) {
                    $status = !is_array($status) ? explode(',', $status) : $status;
                    $query->whereIn('status', $status);
                }
                if ($reservation_id) {
                    $query->where('reservation_id', $reservation_id);
                }
                if ($user_id) {
                    $query->where('user_id', $user_id);
                }
                if ($is_violate) {
                    $query->where('is_violate', $is_violate);
                }
            })
            ->whereIn('status', [1, 3, 4, 5, 6, 7])
            ->orderBy('sort')
            ->orderByDesc('id')
            ->paginate($limit)
            ->toArray();

        foreach ($res['data'] as $key => $val) {
            if ($val['schedule_type'] == 2) {
                $res['data'][$key]['con_schedule'] = $val['con_special_schedule'];
                $week = date('w', strtotime($val['con_special_schedule']['date']));
                $res['data'][$key]['con_schedule']['week'] = get_week_name($week);
                unset($res['data'][$key]['con_special_schedule']);
            } else {
                $res['data'][$key]['con_schedule']['week'] = get_week_name($val['con_schedule']['week']);
            }

            $reservation_info = Reservation::where('id', $val['reservation_id'])->first();
            //判断是否可以取消
            $is_can_cancel = $this->isCanCancel($reservation_info, $val);
            $res['data'][$key]['is_can_cancel'] = $is_can_cancel === true ? true : false;

            //重置用户填写信息
            if ($val['account_id']) {
                $user_account_info = UserLibraryInfo::where('id', $val['account_id'])->first();
                if ($user_account_info) {
                    $res['data'][$key]['id_card'] = $val['id_card'] ? $val['id_card'] : $user_account_info['id_card'];
                    $res['data'][$key]['sex'] = $val['sex'] ? $val['sex'] : $user_account_info['sex'];
                    $res['data'][$key]['tel'] = $val['tel'] ? $val['tel'] : $user_account_info['tel'];
                    $res['data'][$key]['age'] = $val['age'] ? $val['age'] : ($user_account_info['id_card'] ?  get_user_age_by_id_card($user_account_info['id_card']) : '');
                    $res['data'][$key]['reader_id'] = $val['reader_id'] ? $val['reader_id'] : $user_account_info['account'];
                    $res['data'][$key]['username'] = $val['username'] ? $val['username'] : $user_account_info['username'];
                }
            }
        }

        return $res;
    }


    /**
     * 判断当前时间段是否已被预约完毕
     * @param $reservation_id  商品guid
     * @param $schedule_id  时间段id
     * @param $schedule_type 预约类型 1正常  2特殊日期
     * @param $make_time  预约日期
     * @param $number  多物品数量
     * @param $status  状态
     */
    public function makeStatus($reservation_id, $schedule_id, $schedule_type, $make_time, $number, $status = [1, 3, 6])
    {
        $make_time = date('Y-m-d', strtotime($make_time));
        $status = $this->where('reservation_id', $reservation_id)
            ->select(DB::raw('sum(number) as number'))
            ->where('schedule_id', $schedule_id)
            ->where('schedule_type', $schedule_type)
            ->where('make_time', $make_time)
            ->where(function ($query) use ($status) {
                if ($status) {
                    $query->whereIn('status', $status);
                }
            })->first();

        if (empty($status['number'])) {
            return false;
        }
        return $number - $status['number']; //返回剩余数量

    }

    /**
     * 判断当前时间段自己是否已预约
     * @param $user_id   用户id
     * @param $reservation_id  商品id
     * @param $schedule_id  时间段id
     * @param $schedule_type  预约类型 1正常 2特殊时间
     * @param $make_time  预约日期
     */
    public function selfMakeStatus($user_id, $reservation_id, $schedule_id, $schedule_type, $make_time,  $status = [1, 3, 6])
    {
        $make_time = date('Y-m-d', strtotime($make_time));

        $status = $this->where('reservation_id', $reservation_id)
            ->select('id', 'number', 'change_time')
            ->where('user_id', $user_id)
            ->where('schedule_id', $schedule_id)
            ->where('schedule_type', $schedule_type)
            ->where('make_time', $make_time)
            ->where(function ($query) use ($status) {
                if ($status) {
                    $query->whereIn('status', $status);
                }
            })->first();
        return $status ? $status->toArray() : null;
    }

    /**
     * 判断当前时间里自己是否有，有效预约
     * @param $user_id   用户id
     * @param $reservation_id  商品id
     * @param $schedule_id  时间段id
     * @param $schedule_type  预约类型 1正常 2特殊时间
     * @param $make_time  预约日期
     */
    public function nowTimeMakeInfo($user_id, $reservation_id, $schedule_id, $schedule_type, $status = [1, 3, 6], $make_time = null)
    {
        $res = $this->where('reservation_id', $reservation_id)
            ->where('user_id', $user_id)
            ->where('schedule_id', $schedule_id)
            ->where('schedule_type', $schedule_type)
            // ->where('expire_time', '>', date('Y-m-d H:i:s'))
            ->where(function ($query) use ($status, $make_time) {
                if ($status) {
                    $query->whereIn('status', $status);
                }
                if ($make_time) {
                    $query->where('make_time', $make_time); //预约时间，默认是当天
                }
            })->first();
        if (empty($res)) {
            return [];
        }
        // if ($res['status'] == 1 && $res['expire_time'] < date('Y-m-d H:i:s')) {
        //     return []; //已过期，返回[]
        // }

        return $res;
    }


    /**
     * 判断当前时间段所有有效预约
     * @param $user_id   用户id
     * @param $reservation_id  预约id
     * @param $schedule_id  时间段id
     * @param $make_time  预约日期
     */
    public function validMake($reservation_id = null, $schedule_id = null, $schedule_type = null, $make_time = null, $status = [1, 3, 6])
    {
        $make_time = date('Y-m-d', strtotime($make_time));

        $res = $this->select('number', 'user_id')
            ->where(function ($query) use ($reservation_id, $schedule_id, $schedule_type, $make_time, $status) {
                if ($reservation_id) {
                    $query->where('reservation_id', $reservation_id);
                }
                if ($schedule_id) {
                    $query->where('schedule_id', $schedule_id);
                }
                if ($schedule_type) {
                    $query->where('schedule_type', $schedule_type);
                }
                if ($make_time) {
                    $query->where('make_time', $make_time);
                }
                if ($status) {
                    $query->whereIn('status', $status);
                }
            })->get()->toArray();

        return $res;
    }


    /**
     * 获取预约信息
     * @param $make_id   预约id
     */
    public function getMakeInfo($make_id)
    {
        $res = $this->from('reservation_apply as a')->select(
            'a.reservation_id',
            'a.schedule_type',
            'a.make_time',
            'a.remark',
            'a.status',
            'a.reason',
            'a.number',
            'a.create_time',
            '(case when a.schedule_type=2 then p.start_time else s.start_time end ) as start_time',
            '(case when a.schedule_type=2 then p.end_time else s.end_time end ) as end_time'
        )
            ->leftJoin('study_room_reservation_schedule as s', 's.id', '=', 'a.schedule_id')
            ->leftJoin('study_room_reservation_special_schedule t', 't.id = a.schedule_id')
            ->where('a.id', $make_id)
            // ->where('s.is_del' , 1)
            ->first();
        return $res;
    }

    /**
     * 根据条件获取座位信息
     * @param  $reservation_id 预约id
     * @param  $date  某个时间的预约
     * @param  $symbol  符号，是大于这个时间，还是等于这个时间 '>' , '='
     * @param  $status  预约状态  数组
     * @param  $schedule_type  排版类型 1正常排班 2特殊日期排
     * @param  $schedule_id  排版id 查询具体的排版信息
     */
    public function getApplyInfo($reservation_id = '',  $date = '', $symbol = '>', $status = [], $schedule_type = 1, $schedule_id = null)
    {
        return $this->where(function ($query) use ($reservation_id, $date, $symbol, $status, $schedule_type, $schedule_id) {
            if ($reservation_id) {
                $query->where('reservation_id', $reservation_id);
            }
            if ($date) {
                $query->where('make_time', $symbol, $date);
            }
            if ($status) {
                $query->whereIn('status', $status);
            }
            if ($schedule_type) {
                $query->where('schedule_type', $schedule_type);
            }
            if ($schedule_id) {
                $query->where('schedule_id', $schedule_id);
            }
        })->get()->toArray();
    }

    public static function getApplyObj()
    {
        return new self();
    }

    /**
     * 审核通过 和 拒绝
     * @param id int 申请id
     * @param status int 状态  1.已通过 3.已拒绝
     */
    public function agreeAndRefused($id, $status, $reason = null)
    {
        $res = $this->where('id', $id)->first();
        if (!$res) {
            throw new Exception("参数传递错误");
        }

        if ($res->status != 3) {
            throw new Exception("不处于未审核状态");
        }

        $res->status = $status;
        if ($status == 4) {
            $res->reason = $reason;
        }
        $res->manage_id = request()->manage_id;
        $res->save();
        return $res;
    }

    //获取
    public function getConSchedule($con_schedule, $make_time)
    {
        if (!empty($con_schedule)) {
            $reservation_time = $make_time . '
                    ' . $con_schedule['week'] . '
                    ' . $con_schedule['start_time'] . ' 至 ' . $con_schedule['end_time'];
        } else {
            $reservation_time = $make_time;
        }
        return $reservation_time;
    }

    /*座位预约状态*/
    public function getStatusName($status)
    {
        switch ($status) {
            case 1:
                return '已通过';
                break;
            case 2:
                return '已取消';
                break;
            case 3:
                return '待审核';
                break;
            case 4:
                return '已拒绝';
                break;
            case 5:
                return '已过期';
                break;
            case 6:
                return '已签到';
                break;
            case 7:
                return '管理员已取消预约';
                break;
            default:
                return '未知状态';
                break;
        }
    }

    /**
     * 整理schedule时间以及是否可预约
     * @param  $user_id 用户id
     * @param  $reservation_data 预约的值
     * @param  $data 要处理的schedule原数据
     * @param  $cur_date 当前日期
     * @param  $schedule_type int 查询类型 1 普通排版，2特殊日期排版
     */
    public function getScheduleDetail($user_id, $reservation_info, $data, $cur_date, $schedule_type)
    {
        $temp_temp['schedule_type'] = $schedule_type;
        $temp_temp['schedule_id'] = $data['id'];
        $temp_temp['stage'] = get_hour_minute_by_time($data['start_time']) . "-" . get_hour_minute_by_time($data['end_time']);

        $temp_temp['is_make'] = true; //都是不可预约状态
        $temp_temp['is_self_make'] = false; //是否自己预约
        if (date('Y-m-d') == $cur_date && date('H:i:s', strtotime("+30 min")) > $data['end_time']) {
            $apply_number = $this->where('schedule_id', $data['id'])->where('reservation_id', $reservation_info['id'])->where('schedule_type', $schedule_type)->where('status', [1, 3, 6])->count();
            $temp_temp['surplus_number'] = $reservation_info['number'] - $apply_number;
        } else {
            //获取此时间段是否已被预约完毕
            $status = $this->makeStatus($reservation_info['id'], $data['id'], $schedule_type, $cur_date, $reservation_info['number']);
            if ($status === false || $status > 0) {
                //可以被预约
                if (!empty($user_id)) {
                    //判断自己是否已预约
                    $selfStatus = $this->selfMakeStatus($user_id, $reservation_info['id'], $data['id'], $schedule_type, $cur_date);
                    if ($selfStatus) {
                        $temp_temp['is_make'] = true; //已被预约，不能继续预约
                        $temp_temp['is_self_make'] = true; //已被预约，不能继续预约
                    } else {
                        $temp_temp['is_make'] = false; //没有被预约，可以继续预约
                    }
                } else {
                    $temp_temp['is_make']['is_make'] = false; //没有被预约，可以继续预约
                }
            }
            if ($temp_temp['is_make'] === false) {
                //判断剩余个数
                $temp_temp['surplus_number'] = $status === false ? $reservation_info['number'] : $status; //返回剩余个数
            } else {
                $temp_temp['surplus_number'] = $status === false ? $reservation_info['number'] : $status;; //返回剩余个数为 0
            }
        }
        return $temp_temp;
    }

    /**
     * 我的预约列表
     * @param  $authorrization ：用户 token   必选
     * @param  $status  预约状态   1.已通过（待使用）  2.已取消，3 待审核（待审核）  4已拒绝 ,5 已过期 , 6 已签到（已使用）  7 结束打卡（增对座位预约）（已使用）
     *                  状态可分为 3类  全部  待审核3   待使用1  已使用 67  其余展示在全部里面
     * @param  $page  页数  默认为 1
     * @param  $limit  分页限制  默认为 10
     */
    public function myMakeList($user_id, $status = null, $limit = 10)
    {
        $data = $this->from('study_room_reservation as r')
            ->select(
                'a.make_time',
                'a.status',
                'a.number as make_number',
                'a.create_time',
                'a.id as apply_id',
                'a.schedule_id',
                'a.schedule_type',
                'a.change_time',
                'a.expire_time',
                'a.sign_time',

                'a.tel as make_tel',
                'a.unit as make_unit',
                'a.id_card',
                'a.sex',
                'a.age',
                'a.reader_id',
                'a.img as make_img',
                'a.username',
                'a.remark',

                'r.id',
                'r.is_real',   //是否需要用户真实信息 1需要 2不需要 默认2
                'r.real_info',
                //  'r.serial_prefix',
                'r.is_cancel',
                'r.is_approval',
                'r.name',
                'r.img',
                'r.intro',
                'r.type_tag',
                'r.number',
                'r.province',
                'r.city',
                'r.district',
                'r.address',
                'r.tel',
                'r.cancel_end_time',
                's.week',
                's.start_time as normal_start_time',
                's.end_time as normal_end_time',
                't.date',
                't.start_time as special_start_time',
                't.end_time as special_end_time'
            )
            ->join('study_room_reservation_apply as a', 'a.reservation_id', '=', 'r.id')
            ->leftJoin('study_room_reservation_schedule as s', 's.id', '=', 'a.schedule_id')
            ->leftJoin('study_room_reservation_special_schedule as t', 't.id', '=', 'a.schedule_id')
            ->where(function ($query) use ($status) {
                if ($status) {
                    $status = !is_array($status) ? explode(',', $status) : $status;
                    $query->whereIn('a.status', $status);
                }
            })
            ->where('r.is_del', 1)
            ->where('r.is_play', 1)
            // ->where('s.is_del' , 1)
            ->where('a.user_id', $user_id)
            ->orderByDesc('a.make_time') //以预约的时间来排序
            ->orderByDesc('a.id') //在以预约的实际时间来排序
            ->paginate($limit)
            ->toArray();

        $reservationModel = new StudyRoomReservation();
        $controllerObj = new Controller();
        $real_info_arr = $reservationModel->getReservationApplyParam(); //获取所有的数据
        foreach ($data['data'] as $key => &$val) {
            $data['data'][$key]['type_tag'] = !empty($val['type_tag']) ? explode("|", $val['type_tag']) : [];
            $data['data'][$key]['intro'] = strip_tags($val['intro']);
            $data['data'][$key]['real_info'] = $controllerObj->getRealInfoArray($val['real_info'], $real_info_arr);


            $data['data'][$key]['make_info']['schedule_id'] = $val['schedule_id'];
            $data['data'][$key]['make_info']['schedule_type'] = $val['schedule_type'];
            $data['data'][$key]['make_info']['id'] = $val['apply_id'];
            $data['data'][$key]['make_info']['make_time'] = date("Y/m/d", strtotime($val['make_time']));

            $week = $val['schedule_type'] == 2 ? date('w', strtotime($val['date'])) : $val['week'];
            $data['data'][$key]['make_info']['week'] = get_week_name($week);
            $data['data'][$key]['make_info']['status'] = $val['status'];
            $data['data'][$key]['make_info']['number'] = $val['make_number'];
            $data['data'][$key]['make_info']['create_time'] = $val['create_time'];
            $data['data'][$key]['make_info']['start_time'] = $val['schedule_type'] == 1 ? $val['normal_start_time'] : $val['special_start_time'];
            $data['data'][$key]['make_info']['end_time'] = $val['schedule_type'] == 1 ? $val['normal_end_time'] : $val['special_end_time'];
            $data['data'][$key]['make_info']['change_time'] = $val['change_time'];
            $data['data'][$key]['make_info']['expire_time'] = $val['expire_time'];
            $data['data'][$key]['make_info']['sign_time'] = $val['sign_time'];

            $data['data'][$key]['make_info']['tel'] = $val['make_tel'];
            $data['data'][$key]['make_info']['unit'] = $val['make_unit'];
            $data['data'][$key]['make_info']['id_card'] = $val['id_card'];
            $data['data'][$key]['make_info']['sex'] = $val['sex'];
            $data['data'][$key]['make_info']['age'] = $val['age'];
            $data['data'][$key]['make_info']['reader_id'] = $val['reader_id'];
            $data['data'][$key]['make_info']['img'] = $val['make_img'];
            $data['data'][$key]['make_info']['username'] = $val['username'];
            $data['data'][$key]['make_info']['remark'] = $val['remark'];

            // $data['data'][$key]['make_info']['sign_end_time'] = $val['sign_end_time'];

            //判断是否可以取消
            $is_can_cancel = $this->isCanCancel($val, $data['data'][$key]['make_info']);
            $data['data'][$key]['make_info']['is_can_cancel'] = $is_can_cancel === true ? true : false;

            //是否能显示签到二维码
            $is_can_show_qr = $this->isCanShowQr($user_id, null, ['status' => $val['status'], 'make_time' => $val['make_time'], 'reservation_id' => $val['id']]);
            $data['data'][$key]['make_info']['is_can_show_qr'] = $is_can_show_qr === true ? true : false;

            $data['data'][$key]['make_info']['lib_name'] = config('other.lib_name');

            unset($val['reservation_type'], $val['make_time'], $val['remark'], $val['status'], $val['make_number'], $val['create_time'], $val['start_time'], $val['end_time'], $val['schedule_id']);
            unset($val['schedule_type'], $val['normal_start_time'], $val['special_start_time'], $val['normal_end_time'], $val['special_end_time']);
            unset($val['is_cancel'], $val['is_approval']);
        }
        return $data;
    }

    /**
     * 判断是否可以取消
     */
    public function isCanCancel($reservation_info, $make_info)
    {
        if ($reservation_info['is_cancel'] == 2) {
            return '该预约不允许取消';
        }

        $scheduleModel = $make_info['schedule_type'] == 2 ? new StudyRoomReservationSpecialSchedule() : new StudyRoomReservationSchedule();
        $schedule_info = $scheduleModel->where('id', $make_info['schedule_id'])->first();

        if ($reservation_info['is_approval'] == 1) {
            if ($make_info['status'] != 3) {
                return '当前状态不能取消预约';
            }
        } else {
            if ($reservation_info['is_cancel'] == 2) {
                return '当前状态不能取消预约';
            } else {
                if ($make_info['status'] != 1) {
                    return '当前状态不能取消预约';
                }
            }
        }
        if (strtotime($make_info['make_time'] . ' ' . $schedule_info['start_time']) < strtotime($make_info['create_time'])) {
            //预约开始才预约，以预约那一刻的时间计算
            if ($reservation_info['cancel_end_time'] && (time() - $reservation_info['cancel_end_time'] * 60 > strtotime($make_info['create_time']))) {
                return '已超过预约可取消时间';
            }
        } else {
            //预约开始前预约
            if ($reservation_info['cancel_end_time'] && (time() + $reservation_info['cancel_end_time'] * 60 > strtotime($make_info['make_time'] . ' ' . $schedule_info['start_time']))) {
                return '已超过预约可取消时间';
            }
        }
        return true;
    }

    /**
     * 判断是否可以显示二维码
     * @param $apply_info 预约信息
     * @param $apply_id 预约id
     * @param $user_id 用户id
     */
    public function isCanShowQr($user_id, $apply_id, $apply_info = null)
    {
        if (empty($apply_id) && empty($apply_info)) {
            return '预约信息不存在';
        }
        if (empty($apply_info)) {
            $apply_info = $this->where('id', $apply_id)->where('user_id', $user_id)->first();
        }
        if (empty($apply_info)) {
            return '预约信息不存在';
        }
        //已通过和已签到可以出示，其余不可以出示,有自动过期，所以不用考虑过期时间等
        if ($apply_info['status'] != 1 && $apply_info['status'] != 6) {
            return '当前状态不能出示二维码';
        }
        if ($apply_info['make_time'] != date('Y-m-d')) {
            return '当前时间不能出示二维码';
        }

        // 预约是否存在
        $res_info = StudyRoomReservation::where("id", $apply_info['reservation_id'])->where("is_del", 1)->where("is_play", 1)->first();

        if (empty($res_info)) return "预约不存在或已被删除";
        if ($res_info['sign_way'] == 2) return '请使用扫码功能进行扫码操作';

        return true;
    }


    /**
     * 判断当前时间段是否有预约信息 
     * @param $user_id  用户id
     * @param $reservation_id  预约id
     */
    public function userNowMakeInfo($user_id, $reservation_id = null)
    {
        //获取当前满足条件的排版id
        $day = date("w");
        if ($day == 0) {
            $day = 7;
        }
        $schedule_id_all = StudyRoomReservationSchedule::where('week', $day)
            ->where('start_time', '<', date('H:i:s', strtotime("+10 min"))) //可以提前10分钟出示二维码进去
            ->where('end_time', '>', date('H:i:s'))
            ->where(function ($query) use ($reservation_id) {
                if ($reservation_id) {
                    $query->where('reservation_id', $reservation_id);
                }
            })
            ->where('is_del', 1)
            ->pluck('id');
        if (empty($schedule_id_all)) {
            return false;
        }

        $res = $this->whereIn('schedule_id', $schedule_id_all)
            ->where(function ($query) use ($reservation_id) {
                if ($reservation_id) {
                    $query->where('reservation_id', $reservation_id);
                }
            })
            ->where('user_id', $user_id)
            ->whereIn('status', [1, 6])
            ->find();
        if ($res) {
            return $res;
        }

        return false;
    }
}
