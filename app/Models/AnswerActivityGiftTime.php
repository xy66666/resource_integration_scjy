<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * 礼品领取排班表
 */
class AnswerActivityGiftTime extends BaseModel
{
    use HasFactory;


    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';


    protected $table = 'answer_activity_gift_time';


    /**
     * 获取时间段数量
     * @param act_id 活动id
     * @param user_guid 用户id
     * @param type 1文化红包 2精美礼品 
     * @param date 日期
     * @param hour 小时
     */
    public static function getTimeQuantumData($act_id, $type = null)
    {
        $date_time = date('Y-m-d H:i:s');
        $res = self::where(function ($query) use ($type) {
            if ($type) {
                $query->where('type', $type);
            }
        })->where('start_time', '<=', $date_time)->where('end_time', '>=', $date_time)
            ->where('act_id', $act_id)
            ->where('is_del', 1)
            ->first();

        return $res;
    }



    /**
     * 判断时间是否叠加
     * @param $data 数据
     */
    public function isTimeOverlay($data, $except_id = null)
    {
        $res = $this->where('act_id', $data['act_id'])
            ->where('type', $data['type'])
            ->where('start_time', '<=', $data['start_time'])
            ->where('end_time', '>', $data['start_time'])
            ->where('is_del', 1)
            ->where(function ($query) use ($except_id) {
                if (!empty($except_id)) {
                    $query->where('id', '<>', $except_id);
                }
            })
            ->first();
        return $res;
    }



    /**
     * 获取发放列表
     * @param field int 获取字段信息  数组
     * @param act_id int 活动id
     * @param page int 当前页
     * @param limit int 分页大小
     * @param type int 0或空表示  1文化红包 2精美礼品
     * @param start_time datetime 创建时间范围搜索(开始) 
     * @param end_time datetime 创建时间范围搜索(结束) 
     */
    public function lists($field, $act_id, $type, $start_time, $end_time, $page, $limit)
    {
        if (empty($field)) {
            $field = ['id', 'act_id', 'start_time', 'end_time', 'type', 'number', 'user_gift_number', 'create_time'];
        }
        $res = $this->select($field)
            ->where(function ($query) use ($act_id, $type, $start_time, $end_time) {
                if ($act_id) {
                    $query->where('act_id', $act_id);
                }
                if ($type) {
                    $query->where('type', $type);
                }
                if ($start_time && $end_time) {
                    $query->whereBetween('start_time', [$start_time, $end_time]);
                }
            })
            ->where('is_del', 1)
            ->orderBy('id')
            ->paginate($limit)
            ->toArray();

        $controllerObj = new Controller();
        $res = $controllerObj->disPageData($res);

        return $res;
    }


    /**
     * 获取单位详情
     * @param act_id int 活动id
     */
    public function detail($id, $field = [])
    {
        if (empty($field)) {
            $field = ['id', 'act_id', 'start_time', 'end_time', 'type', 'number', 'user_gift_number', 'create_time'];
        }
        $res = $this->select($field)
            ->where('id', $id)
            ->where('is_del', 1)
            ->first();

        //获取单位区域
        if (!empty($res)) {
            $res['type_name'] = $res['type'] == 1 ? '文化红包' : '精美礼品';
        }

        return $res;
    }
}
