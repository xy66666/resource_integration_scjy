<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * 码上借 归还记录
 */
class CodeReturnLog extends BaseModel
{
    use HasFactory;

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'change_time';

    protected $table = 'code_return_log';

    /*关联场所码*/
    public function conCodePlace()
    {
        return $this->hasOne(CodePlace::class, 'id', 'place_code_id');
    }

    /**
     * 书籍归还
     */
    public function addData($data, $user_id, $place_code_id)
    {
        $this->rdrrecno = !empty($data['rdrrecno']) ? $data['rdrrecno'] : null;
        $this->place_code_id = $place_code_id;
        $this->lib_id = !empty($data['lib_id']) ? $data['lib_id'] : null;
        $this->lib_name = !empty($data['lib_name']) ? $data['lib_name'] : null;
        $this->cur_local = '中心馆';
        $this->cur_local_name =  '中心馆';
        $this->cur_lib =  '中心馆';
        $this->cur_lib_name =  '中心馆';
        $this->owner_local = '中心馆';
        $this->owner_local_name = '中心馆';
        $this->recno = $data['metaid'];
        $this->bibrecno = !empty($data['metatable']) ? $data['metatable'] : null;
        $this->book_name = $data['book_name'];
        $this->author = $data['author'];
        $this->price = !empty($data['price']) ? $data['price'] : '';
        $this->callno = $data['book_num'];
        $this->isbn = $data['isbn'];
        $this->borrow_time = !empty($data['user_info']['borrow_time']) ? $data['user_info']['borrow_time'] : null;
        $this->press = $data['press'];
        $this->pre_time = !empty($data['pre_time']) ? $data['pre_time'] : '';
        $this->classno = !empty($data['classno']) ? $data['classno'] : null;
        $this->barcode = $data['barcode'];
        $this->user_id = $user_id;
        //  $this->account =  !empty($data['user_info']['reader_id']) ? $data['user_info']['reader_id'] : null;

        $codeBorrowLogModel = new CodeBorrowLog();
        $account = $codeBorrowLogModel->where('barcode', $data['barcode'])->where('status', 2)->orderByDesc('id')->value('account'); //查询最后一条未归还的数据记录
        if ($account) {
            $this->account = $account;
        }

        $this->save();
        return $this->id;
    }

    /**
     * 归还操作列表
     * @param user_id  用户id
     * @param place_code_id  场所码id
     * @param keywords  检索条件
     * @param start_time 申请开始时间   数据格式    2020-05-12 12:00:00
     * @param end_time 申请结束时间     数据格式    2020-05-12 12:00:00
     * @param page  页数，默认为1，
     * @param limit  条数，默认显示 10条
     */

    public function lists($user_id, $place_code_id, $keywords, $start_time, $end_time, $limit)
    {
        $res = $this->select([
            'place_code_id',
            'book_name',
            'author',
            'price',
            'press',
            'isbn',
            'barcode',
            'pre_time',
            'callno',
            'classno',
            'borrow_time',
            'create_time as return_time',
            'account'
        ])
            ->with(['conCodePlace' => function ($query) {
                $query->select('id', 'name');
            }])
            ->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('book_name', 'like', "%$keywords%")
                        ->Orwhere('barcode', 'like', "%$keywords%")
                        ->Orwhere('isbn', 'like', "%$keywords%")
                        ->Orwhere('account', 'like', "%$keywords%");
                }
            })->where(function ($query) use ($user_id, $place_code_id, $start_time, $end_time) {
                if ($user_id) {
                    $query->where('user_id', $user_id);
                }
                if ($place_code_id) {
                    $query->where('place_code_id', $place_code_id);
                }
                if ($start_time && $end_time) {
                    $query->whereBetween('create_time', [$start_time, $end_time]);
                }
            })->orderByDesc('id')
            ->paginate($limit)
            ->toArray();
        return $res;
    }
}
