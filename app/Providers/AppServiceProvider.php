<?php

namespace App\Providers;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //

        //手机号格式验证
        \Illuminate\Support\Facades\Validator::extend('check_tel', function ($attribute, $value, $parameters) {
            return preg_match('/^1[3456789][0-9]{9}$/', $value);
        });
        //手机号格式 和 电话号码格式 验证
        \Illuminate\Support\Facades\Validator::extend('check_tel_and_phone', function ($attribute, $value, $parameters) {
            return verify_tel_and_phone($value);
        });

        //验证账号只能是字母或数字
        \Illuminate\Support\Facades\Validator::extend('self_alpha_num', function ($attribute, $value, $parameters) {
            return !is_include_chinese($value);
        });

        //ISBN格式验证
        \Illuminate\Support\Facades\Validator::extend('check_isbn', function ($attribute, $value, $parameters) {
            return  check_isbn($value);
        });
        //身份证号码格式验证
        \Illuminate\Support\Facades\Validator::extend('check_id_card', function ($attribute, $value, $parameters) {
            return  is_legal_no($value);
        });
        //验证手机验证码
        \Illuminate\Support\Facades\Validator::extend('check_tel_verify', function ($attribute, $value, $parameters) {
            //加几个特殊的电话号码start
            if($value === "000000"){
                //如果验证码为000000，就判断是否为特殊的几个号码
                $special_tel = array(
                    '18081838416',
                    '18280483813',//唐大龙
                    '15102892664',//莫坤
                );
                if(!in_array(request()->tel , $special_tel)){
                    return '手机验证码输入错误';
                }
            }else{
                $res= \App\Models\RegisterVerify::where('phone' , request()->tel)->orderByDesc("id")->first();
                //下面就正常判断验证
                if(!$res || $value !== $res['code']){
                    return '手机验证码输入错误';
                }
                //在判断验证码是否过期
                $now_time  = strtotime("now");
                $expir_time = strtotime($res['expir_time']);
                //如果当前时间大于过期时间，代表验证码已过期
                if($now_time > $expir_time){
                    return '手机验证码已过期';
                }
            }
            return true;
        });
        //验证手机号的key
        \Illuminate\Support\Facades\Validator::extend('check_tel_key', function ($attribute, $value, $parameters) {
            $key = md5(request()->tel.config('other.tel_key'));
            if(strtoupper($value) !== strtoupper($key)){
                return false;
            }
            return true;
        });




        //记录SQL执行时间
        DB::listen(
            function ($query) {

                $tmp = str_replace('?', '"' . '%s' . '"', $query->sql);

                $qBindings = [];

                foreach ($query->bindings as $key => $value) {

                    if (is_numeric($key)) {

                        $qBindings[] = $value;
                    } else {

                        $tmp = str_replace(':' . $key, '"' . $value . '"', $tmp);
                    }
                }


                try {
                    $tmp = str_ireplace(["%Y", '%m', '%d', '%H', '%i'], ["%%Y", '%%m', '%%d', '%%H', '%%i'], $tmp);
                    $tmp = vsprintf($tmp, $qBindings); //会出问题 local.ERROR: vsprintf(): Too few arguments
                } catch (\Exception $e) {
                    $tmp = str_ireplace("%", "%%", $tmp);
                    $tmp = vsprintf($tmp, $qBindings);
                }

                $tmp = str_replace("\\", "", $tmp);
                Log::channel('sqllog')->info(' execution time: ' . $query->time . 'ms; ' . $tmp . "\n\t");
            }
        );
    }
}
