<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\ActivityApply;
use App\Models\ActivityTag;
use App\Models\ActivityType;
use App\Models\AnswerActivity;
use App\Models\AnswerActivityBrowseCount;
use App\Models\AnswerActivityGift;
use App\Models\AnswerActivityGiftPublic;
use App\Models\AnswerActivityGiftRecord;
use App\Models\AnswerActivityGiftTime;
use App\Models\AnswerActivityInvite;
use App\Models\AnswerActivityLimitAddress;
use App\Models\AnswerActivityProblem;
use App\Models\AnswerActivityProblemAnswer;
use App\Models\AnswerActivityResource;
use App\Models\AnswerActivityShare;
use App\Models\AnswerActivityUnit;
use App\Models\AnswerActivityUnitUserNumber;
use App\Models\AnswerActivityUserAnswerRecord;
use App\Models\AnswerActivityUserAnswerTotalNumber;
use App\Models\AnswerActivityUserCorrectAnswerRecord;
use App\Models\AnswerActivityUserCorrectNumber;
use App\Models\AnswerActivityUserCorrectTotalNumber;
use App\Models\AnswerActivityUserGift;
use App\Models\AnswerActivityUserPrize;
use App\Models\AnswerActivityUserPrizeRecord;
use App\Models\AnswerActivityUserStairsAnswerRecord;
use App\Models\AnswerActivityUserStairsTotalNumber;
use App\Models\AnswerActivityUserUnit;
use App\Models\AnswerActivityUserUsePrizeRecord;
use App\Models\AnswerActivityWordResource;
use App\Models\Banner;
use App\Models\Digital;
use App\Models\DigitalType;
use App\Models\PictureLive;
use App\Models\PictureLiveAppointUser;
use App\Models\PictureLiveBlackUser;
use App\Models\PictureLiveBrowse;
use App\Models\PictureLiveVote;
use App\Models\PictureLiveWorks;
use App\Models\UserAppletInfo;
use App\Models\UserInfo;
use App\Models\UserWechatInfo;
use Illuminate\Support\Facades\DB;

/**
 * 同步gy_weixin 的数据
 */
class MigratedProcessingDataController extends Controller
{

    public $system_unit_id = 517; //需要同步的图书馆id，在数据库 resource_integration_promote  system_unit 表的id
    public $old_file_path = 'D:/phpstudy_pro/WWW/resource_integration_promote/public/uploads/'; //旧图片路径
    public $new_file_path = 'D:/phpstudy_pro/WWW/resource_integration_scjy/public/uploads/'; //新图片路径

    /**
     * 处理用户数据
     */
    public function userInfo()
    {
        ini_set('memory_limit', 1024 * 1024 * 1024 * 10);
        set_time_limit(0);
        error_reporting(0);

        dump('开始处理用户数据');

        if (empty($this->system_unit_id)) {
            dd('单位ID不能为空');
        }

        $res = DB::connection('mysql2')->table('user_system_unit')->where('system_unit_id', $this->system_unit_id)->where('user_id', '<>', 0)->orderBy('user_id')->get()->toArray();

        //处理读者证数据
        foreach ($res as $key => $val) {
            $val = object_array($val);
            if ($val['user_id']) {
                $user_info = DB::connection('mysql2')->table('user_info')->where('id', $val['user_id'])->first();
                if (empty($user_info)) {
                    continue;
                }
                $user_info = object_array($user_info);
                $user_info['create_time'] = $val['create_time']; //替换时间

                $union_id = null;
                if ($user_info['wechat_id']) {
                    $userWechatInfoModel = new UserWechatInfo();
                    $user_wechat_info_exists =  $userWechatInfoModel->where('id', $user_info['wechat_id'])->first();
                    if (empty($user_wechat_info_exists)) {
                        $user_wechat_info = DB::connection('mysql2')->table('user_wechat_info')->where('id', $user_info['wechat_id'])->first();
                        if ($user_wechat_info) {
                            $user_wechat_info = object_array($user_wechat_info);
                            $user_wechat_info['create_time'] = $val['create_time']; //替换时间
                            $union_id = $user_wechat_info['union_id'];
                            unset($user_wechat_info['invite_number'], $user_wechat_info['source_invite'], $user_wechat_info['old_open_id']);

                            $userWechatInfoModel->insert($user_wechat_info);
                        }
                    } else {
                        $union_id = $user_wechat_info_exists['union_id'];
                    }
                }
                if ($user_info['applet_id']) {
                    $userAppletInfoModel = new UserAppletInfo();
                    $user_applet_info_exists =  $userAppletInfoModel->where('id', $user_info['applet_id'])->first();
                    if (empty($user_applet_info_exists)) {
                        $user_applet_info = DB::connection('mysql2')->table('user_applet_info')->where('id', $user_info['applet_id'])->first();
                        if ($user_applet_info) {
                            $user_applet_info = object_array($user_applet_info);
                            $user_applet_info['create_time'] = $val['create_time']; //替换时间
                            $union_id = $user_applet_info['union_id'];
                            unset($user_applet_info['invite_number'], $user_applet_info['source_invite']);
                            $userAppletInfoModel->insert($user_applet_info);
                        }
                    } else {
                        $union_id = $user_applet_info_exists['union_id'];
                    }
                }

                $userInfoModel = new UserInfo();
                $user_info_exists =  $userInfoModel->where('id', $val['user_id'])->first();
                if (empty($user_info_exists)) {
                    $user_info['union_id'] = $union_id;
                    unset($user_info['nickname'], $user_info['head_img'], $user_info['gold']);
                    $userInfoModel->insert($user_info);

                    if ($val['qr_url']) {
                        $a = copy_file($this->old_file_path . $val['qr_url'], $this->new_file_path . $val['qr_url']); //移动图片到新地址
                        // dump($a);die;
                    }

                    //https://www.cdbottle.cn/uploads/img/2024-05-22/02UepCRAf30zg4uhegrNfF9Q9CjLpjDIYtEMEdUu.jpg
                    if ($val['head_img'] && strrpos('www.cdbottle.cn', $val['head_img']) !== false) {
                        $head_img = str_replace('https://www.cdbottle.cn/uploads/', '', $val['head_img']);
                        copy_file($this->old_file_path . $head_img, $this->new_file_path . $head_img); //移动图片到新地址
                    }
                }
                echo ($val['user_id'] . '、');
            }
        }
        dump('用户数据处理完毕');
    }


    /**
     * 同步活动数据与报名数据
     */
    public function activity()
    {
        ini_set('memory_limit', 1024 * 1024 * 1024 * 10);
        set_time_limit(0);
        error_reporting(0);

        dump('开始处理活动类型数据');

        //TODO  获取类型数据
        $res = DB::connection('mysql2')->table('activity_type')->where(function ($query) {
            $query->orwhere('system_unit_id', $this->system_unit_id)->orwhere('system_unit_id', 0);
        })->get()->toArray();
        foreach ($res as $key => $val) {
            $val = object_array($val);
            $activityTypeModel = new ActivityType();
            $activity_type_info = $activityTypeModel->where('id', $val['id'])->first();
            if (!empty($activity_type_info)) {
                continue;
            }
            $activityTypeModel->id = $val['id'];
            $activityTypeModel->type_name = $val['type_name'];
            $activityTypeModel->is_del = $val['is_del'];
            $activityTypeModel->create_time = $val['create_time'];
            $activityTypeModel->change_time = $val['change_time'];
            $activityTypeModel->save();

            echo ($val['id'] . '、');
        }
        dump('活动类型数据处理完毕');


        dump('开始处理活动标签数据');

        //TODO  获取标签数据
        $res = DB::connection('mysql2')->table('activity_tag')->where(function ($query) {
            $query->orwhere('system_unit_id', $this->system_unit_id)->orwhere('system_unit_id', 0);
        })->get()->toArray();
        foreach ($res as $key => $val) {
            $val = object_array($val);
            $activityTagModel = new ActivityTag();
            $activity_tag_info = $activityTagModel->where('id', $val['id'])->first();
            if (!empty($activity_tag_info)) {
                continue;
            }
            $activityTagModel->id = $val['id'];
            $activityTagModel->tag_name = $val['tag_name'];
            $activityTagModel->is_del = $val['is_del'];
            $activityTagModel->create_time = $val['create_time'];
            $activityTagModel->change_time = $val['change_time'];
            $activityTagModel->save();

            echo ($val['id'] . '、');
        }
        dump('活动标签数据处理完毕');

        dump('开始处理活动数据');
        //报名数据
        $res = DB::connection('mysql2')->table('activity')->where('system_unit_id', $this->system_unit_id)->get()->toArray();
        foreach ($res as $key => $val) {
            $val = object_array($val);
            $activityModel = new Activity();
            $activity_info = $activityModel->where('id', $val['id'])->first();
            if (empty($activity_info)) {
                unset($val['system_unit_id']);
                unset($val['qr_url'],$val['qr_code']);
                unset($val['sign_qr_url'],$val['sign_qr_code']);//修改活动时，从新生成
                $activityModel->insert($val);

                if ($val['img'] && $val['img'] != 'default/default_activity.png') {
                    copy_file($this->old_file_path . $val['img'], $this->new_file_path . $val['img']); //移动图片到新地址
                }
            }
            echo ($val['id'] . '、');

            dump('开始处理活动' . $val['id'] . '的报名数据');
            //处理活动报名数据
            $res_apply = DB::connection('mysql2')->table('activity_apply')->where('act_id', $val['id'])->get()->toArray();
            foreach ($res_apply as $apply_key => $apply_val) {
                $apply_val = object_array($apply_val);
                $activityApplyModel = new ActivityApply();
                $activity_apply_info = $activityApplyModel->where('id', $apply_val['id'])->first();
                if (empty($activity_apply_info)) {
                    unset($apply_val['system_unit_id']);
                    $activityApplyModel->insert($apply_val);

                    if ($apply_val['img']) {
                        copy_file($this->old_file_path . $apply_val['img'], $this->new_file_path . $apply_val['img']); //移动图片到新地址
                    }
                }
                echo ($apply_val['id'] . '、');
            }
            dump('活动' . $val['id'] . '的报名数据处理完毕');
        }
        dump('活动数据处理完毕');
    }
    /**
     * 处理数字阅读数据
     */
    public function digitalReadData()
    {
        dump('开始处理数字资源类型数据');

        //TODO  获取类型数据
        $res = DB::connection('mysql2')->table('digital_type')->where(function ($query) {
            $query->orwhere('system_unit_id', $this->system_unit_id)->orwhere('system_unit_id', 0);
        })->get()->toArray();
        foreach ($res as $key => $val) {
            $val = object_array($val);
            $digitalTypeModel = new DigitalType();
            $digital_type_info = $digitalTypeModel->where('id', $val['id'])->first();
            if (!empty($digital_type_info)) {
                continue;
            }
            $digitalTypeModel->id = $val['id'];
            $digitalTypeModel->type_name = $val['type_name'];
            $digitalTypeModel->is_del = $val['is_del'];
            $digitalTypeModel->create_time = $val['create_time'];
            $digitalTypeModel->change_time = $val['change_time'];
            $digitalTypeModel->save();

            if (!empty($val['img'])) {
                copy_file($this->old_file_path . $val['img'], $this->new_file_path . $val['img']); //移动图片到新地址
            }

            echo ($val['id'] . '、');
        }
        dump('数字资源类型数据处理完毕');

        dump('开始处理数字资源数据');

        //报名数据
        $digital_read_id = DB::connection('mysql2')->table('digital_unit')->orwhere('system_unit_id', $this->system_unit_id)->pluck('digital_id');
        $res = DB::connection('mysql2')->table('digital_read')->where(function ($query) use ($digital_read_id) {
            $query->orwhere('system_unit_id', $this->system_unit_id)->orwhere('system_unit_id', 'in', $digital_read_id);
        })->get()->toArray();
        foreach ($res as $key => $val) {
            $val = object_array($val);
            $digitalModels = new Digital();
            $is_exists = $digitalModels->where('id', $val['id'])->first();
            if (empty($is_exists)) {
                unset($val['system_unit_id'], $val['manage_id'], $val['node']);
                $digitalModels->insert($val);
            }
            echo ($val['id'] . '、');
        }
        dump('数字资源数据处理完毕');
    }

    /**
     * 处理答题活动数据
     */
    public function answerActivityData()
    {
        dump('开始处理答题活动数据');

        //TODO  获取类型数据
        $res = DB::connection('mysql2')->table('answer_activity')->where('system_unit_id', $this->system_unit_id)->get()->toArray();
        foreach ($res as $key => $val) {
            $val = object_array($val);
            $answerActivityModel = new AnswerActivity();
            $answer_activity_info = $answerActivityModel->where('id', $val['id'])->first();
            if (empty($answer_activity_info)) {
                unset($val['system_unit_id']);
                $answerActivityModel->insert($val);

                if ($val['img']) {
                    copy_file($this->old_file_path . $val['img'], $this->new_file_path . $val['img']); //移动图片到新地址
                }
                if ($val['share_img']) {
                    copy_file($this->old_file_path . $val['share_img'], $this->new_file_path . $val['share_img']); //移动图片到新地址
                }
                if ($val['qr_url']) {
                    copy_file($this->old_file_path . $val['qr_url'], $this->new_file_path . $val['qr_url']); //移动图片到新地址
                }
            }
            echo ($val['id'] . '、');
        }
        dump('答题活动数据处理完毕');

        $answer_activity_id = array_column($res, 'id');

        dump('开始处理答题活动单位浏览数据');
        //报名数据
        $answer_activity_browse_count_info = DB::connection('mysql2')->table('answer_activity_browse_count')->whereIn('act_id', $answer_activity_id)->where('unit_id', $this->system_unit_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_browse_count_info) {
            $answerActivityBrowseCountModel = new AnswerActivityBrowseCount();
            foreach ($answer_activity_browse_count_info as $key => $val) {
                $val = object_array($val);
                $answerActivityBrowseCountModel->insert($val);
            }
        }
        dump('答题活动单位浏览数据处理完毕');

        dump('开始处理答题活动礼品数据');
        //报名数据
        $answer_activity_gift_info = DB::connection('mysql2')->table('answer_activity_gift')->whereIn('act_id', $answer_activity_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_gift_info) {
            $answerActivityGiftModel = new AnswerActivityGift();
            foreach ($answer_activity_gift_info as $key => $val) {
                $val = object_array($val);
                $answerActivityGiftModel->insert($val);

                if ($val['img']) {
                    copy_file($this->old_file_path . $val['img'], $this->new_file_path . $val['img']); //移动图片到新地址
                }
            }
        }
        dump('答题活动礼品数据处理完毕');

        dump('开始处理答题活动礼品配置数据');
        //报名数据
        $answer_activity_gift_public_info = DB::connection('mysql2')->table('answer_activity_gift_public')->whereIn('act_id', $answer_activity_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_gift_public_info) {
            $answerActivityGiftPublicModel = new AnswerActivityGiftPublic();
            foreach ($answer_activity_gift_public_info as $key => $val) {
                $val = object_array($val);
                $answerActivityGiftPublicModel->insert($val);
            }
        }
        dump('答题活动礼品配置数据处理完毕');

        dump('开始处理答题活动礼品设置记录数据');
        //报名数据
        $answer_activity_gift_record_info = DB::connection('mysql2')->table('answer_activity_gift_record')->whereIn('act_id', $answer_activity_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_gift_record_info) {
            $answerActivityGiftRecordModel = new AnswerActivityGiftRecord();
            foreach ($answer_activity_gift_record_info as $key => $val) {
                $val = object_array($val);
                $answerActivityGiftRecordModel->insert($val);
            }
        }
        dump('答题活动礼品设置记录数据处理完毕');

        dump('开始处理答题活动礼品时间设置记录数据');
        //报名数据
        $answer_activity_gift_time_info = DB::connection('mysql2')->table('answer_activity_gift_time')->whereIn('act_id', $answer_activity_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_gift_time_info) {
            $answerActivityGiftTimeModel = new AnswerActivityGiftTime();
            foreach ($answer_activity_gift_time_info as $key => $val) {
                $val = object_array($val);
                $answerActivityGiftTimeModel->insert($val);
            }
        }
        dump('答题活动礼品时间设置记录数据处理完毕');

        dump('开始处理答题活动邀请码记录数据');
        //报名数据
        $answer_activity_invite_info = DB::connection('mysql2')->table('answer_activity_invite')->whereIn('act_id', $answer_activity_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_invite_info) {
            $answerActivityInviteModel = new AnswerActivityInvite();
            foreach ($answer_activity_invite_info as $key => $val) {
                $val = object_array($val);
                $answerActivityInviteModel->insert($val);
            }
        }
        dump('答题活动邀请码记录数据处理完毕');

        dump('开始处理答题活动限制地址数据');
        //报名数据
        $answer_activity_limit_address_info = DB::connection('mysql2')->table('answer_activity_limit_address')->whereIn('act_id', $answer_activity_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_limit_address_info) {
            $answerActivityLimitAddressModel = new AnswerActivityLimitAddress();
            foreach ($answer_activity_limit_address_info as $key => $val) {
                $val = object_array($val);
                $answerActivityLimitAddressModel->insert($val);
            }
        }
        dump('答题活动限制地址数据处理完毕');


        dump('开始处理答题活动题目数据');
        //报名数据
        $answer_activity_problem_info = DB::connection('mysql2')->table('answer_activity_problem')->whereIn('act_id', $answer_activity_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_problem_info) {
            $answerActivityProblemModel = new AnswerActivityProblem();
            foreach ($answer_activity_problem_info as $key => $val) {
                $val = object_array($val);
                $answerActivityProblemModel->insert($val);

                if ($val['img']) {
                    copy_file($this->old_file_path . $val['img'], $this->new_file_path . $val['img']); //移动图片到新地址
                }
            }
        }
        dump('答题活动题目数据处理完毕');

        dump('开始处理答题活动题目答案数据');
        //报名数据
        $answer_activity_problem_answer_info = DB::connection('mysql2')->table('answer_activity_problem_answer')->whereIn('pro_id', array_column($answer_activity_problem_info, 'id'))->orderBy('id')->get()->toArray();
        if ($answer_activity_problem_answer_info) {
            $answerActivityProblemAnswerModel = new AnswerActivityProblemAnswer();
            foreach ($answer_activity_problem_answer_info as $key => $val) {
                $val = object_array($val);
                $answerActivityProblemAnswerModel->insert($val);
            }
        }
        dump('答题活动题目答案数据处理完毕');

        dump('开始处理答题活动资源数据');
        //报名数据
        $answer_activity_resource_info = DB::connection('mysql2')->table('answer_activity_resource')->whereIn('act_id', $answer_activity_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_resource_info) {
            $answerActivityResourceModel = new AnswerActivityResource();
            foreach ($answer_activity_resource_info as $key => $val) {
                $val = object_array($val);
                $answerActivityResourceModel->insert($val);
            }
        }
        dump('答题活动资源数据处理完毕');

        dump('开始处理答题活动分享数据');
        //报名数据
        $answer_activity_share_info = DB::connection('mysql2')->table('answer_activity_share')->whereIn('act_id', $answer_activity_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_share_info) {
            $answerActivityShareModel = new AnswerActivityShare();
            foreach ($answer_activity_share_info as $key => $val) {
                $val = object_array($val);
                $answerActivityShareModel->insert($val);
            }
        }
        dump('答题活动分享数据处理完毕');

        dump('开始处理答题活动单位数据');
        //报名数据
        $answer_activity_unit_info = DB::connection('mysql2')->table('answer_activity_unit')->whereIn('act_id', $answer_activity_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_unit_info) {
            $answerActivityUnitModel = new AnswerActivityUnit();
            foreach ($answer_activity_unit_info as $key => $val) {
                $val = object_array($val);
                $answerActivityUnitModel->insert($val);
            }
        }
        dump('答题活动单位数据处理完毕');

        dump('开始处理答题活动单位用户数据');
        //报名数据
        $answer_activity_unit_user_number_info = DB::connection('mysql2')->table('answer_activity_unit_user_number')->whereIn('act_id', $answer_activity_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_unit_user_number_info) {
            $answerActivityUnitUserNumberModel = new AnswerActivityUnitUserNumber();
            foreach ($answer_activity_unit_user_number_info as $key => $val) {
                $val = object_array($val);
                $answerActivityUnitUserNumberModel->insert($val);
            }
        }
        dump('答题活动单位用户数据处理完毕');



        dump('开始处理答题活动用户答题记录数据');
        //报名数据
        $answer_activity_user_answer_record_info = DB::connection('mysql2')->table('answer_activity_user_answer_record')->whereIn('act_id', $answer_activity_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_user_answer_record_info) {
            $answerActivityUserAnswerRecordModel = new AnswerActivityUserAnswerRecord();
            foreach ($answer_activity_user_answer_record_info as $key => $val) {
                $val = object_array($val);
                $answerActivityUserAnswerRecordModel->insert($val);
            }
        }
        dump('答题活动用户答题记录数据处理完毕');

        dump('开始处理答题活动用户答题统计数据');
        //报名数据
        $answer_activity_user_answer_total_number_info = DB::connection('mysql2')->table('answer_activity_user_answer_total_number')->whereIn('act_id', $answer_activity_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_user_answer_total_number_info) {
            $answerActivityUserAnswerTotalNumberModel = new AnswerActivityUserAnswerTotalNumber();
            foreach ($answer_activity_user_answer_total_number_info as $key => $val) {
                $val = object_array($val);
                $answerActivityUserAnswerTotalNumberModel->insert($val);
            }
        }
        dump('答题活动用户答题统计数据处理完毕');


        dump('开始处理答题活动用户答题记录2数据');
        //报名数据
        $answer_activity_user_correct_answer_record_info = DB::connection('mysql2')->table('answer_activity_user_correct_answer_record')->whereIn('act_id', $answer_activity_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_user_correct_answer_record_info) {
            $answerActivityUserCorrectAnswerRecordModel = new AnswerActivityUserCorrectAnswerRecord();
            foreach ($answer_activity_user_correct_answer_record_info as $key => $val) {
                $val = object_array($val);
                $answerActivityUserCorrectAnswerRecordModel->insert($val);
            }
        }
        dump('答题活动用户答题记录2数据处理完毕');

        dump('开始处理答题活动用户答题数量2数据');
        //报名数据
        $answer_activity_user_correct_number = DB::connection('mysql2')->table('answer_activity_user_correct_number')->whereIn('act_id', $answer_activity_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_user_correct_number) {
            $answerActivityUserCorrectNumberModel = new AnswerActivityUserCorrectNumber();
            foreach ($answer_activity_user_correct_number as $key => $val) {
                $val = object_array($val);
                $answerActivityUserCorrectNumberModel->insert($val);
            }
        }
        dump('答题活动用户答题数量2数据处理完毕');

        dump('开始处理答题活动用户答题统计2数据');
        //报名数据
        $answer_activity_user_correct_total_number = DB::connection('mysql2')->table('answer_activity_user_correct_total_number')->whereIn('act_id', $answer_activity_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_user_correct_total_number) {
            $answerActivityUserCorrectTotalNumberModel = new AnswerActivityUserCorrectTotalNumber();
            foreach ($answer_activity_user_correct_total_number as $key => $val) {
                $val = object_array($val);
                $answerActivityUserCorrectTotalNumberModel->insert($val);
            }
        }
        dump('答题活动用户答题统计2数据处理完毕');

        dump('开始处理答题活动用户礼品数据');
        //报名数据
        $answer_activity_user_gift = DB::connection('mysql2')->table('answer_activity_user_gift')->whereIn('act_id', $answer_activity_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_user_gift) {
            $answerActivityUserGiftModel = new AnswerActivityUserGift();
            foreach ($answer_activity_user_gift as $key => $val) {
                $val = object_array($val);
                $answerActivityUserGiftModel->insert($val);
            }
        }
        dump('答题活动用户礼品数据处理完毕');

        dump('开始处理答题活动用户答题钥匙次数数据');
        //报名数据
        $answer_activity_user_prize = DB::connection('mysql2')->table('answer_activity_user_prize')->whereIn('act_id', $answer_activity_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_user_prize) {
            $answerActivityUserPrizeModel = new AnswerActivityUserPrize();
            foreach ($answer_activity_user_prize as $key => $val) {
                $val = object_array($val);
                $answerActivityUserPrizeModel->insert($val);
            }
        }
        dump('答题活动用户答题钥匙次数数据处理完毕');


        dump('开始处理答题活动用户答题钥匙次数记录数据');
        //报名数据
        $answer_activity_user_prize_record = DB::connection('mysql2')->table('answer_activity_user_prize_record')->whereIn('act_id', $answer_activity_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_user_prize_record) {
            $answerActivityUserPrizeRecordModel = new AnswerActivityUserPrizeRecord();
            foreach ($answer_activity_user_prize_record as $key => $val) {
                $val = object_array($val);
                $answerActivityUserPrizeRecordModel->insert($val);
            }
        }
        dump('答题活动用户答题钥匙次数记录数据处理完毕');



        dump('开始处理答题活动用户答题记录3数据');
        //报名数据
        $answer_activity_user_stairs_answer_record = DB::connection('mysql2')->table('answer_activity_user_stairs_answer_record')->whereIn('act_id', $answer_activity_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_user_stairs_answer_record) {
            $answerActivityUserStairsAnswerRecordModel = new AnswerActivityUserStairsAnswerRecord();
            foreach ($answer_activity_user_stairs_answer_record as $key => $val) {
                $val = object_array($val);
                $answerActivityUserStairsAnswerRecordModel->insert($val);
            }
        }
        dump('答题活动用户答题记录3数据处理完毕');


        dump('开始处理答题活动用户答题统计3数据');
        //报名数据
        $answer_activity_user_stairs_total_number = DB::connection('mysql2')->table('answer_activity_user_stairs_total_number')->whereIn('act_id', $answer_activity_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_user_stairs_total_number) {
            $answerActivityUserStairsTotalNumberModel = new AnswerActivityUserStairsTotalNumber();
            foreach ($answer_activity_user_stairs_total_number as $key => $val) {
                $val = object_array($val);
                $answerActivityUserStairsTotalNumberModel->insert($val);
            }
        }
        dump('答题活动用户答题统计3数据处理完毕');


        dump('开始处理答题活动用户单位数据');
        //报名数据
        $answer_activity_user_unit = DB::connection('mysql2')->table('answer_activity_user_unit')
            ->select(['id', 'act_id', 'user_guid', 'username', 'tel', 'id_card', 'reader_id', 'unit_id', 'create_time', 'change_time'])
            ->whereIn('act_id', $answer_activity_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_user_unit) {
            $answerActivityUserUnitModel = new AnswerActivityUserUnit();
            foreach ($answer_activity_user_unit as $key => $val) {
                $val = object_array($val);
                $answerActivityUserUnitModel->insert($val);
            }
        }
        dump('答题活动用户单位数据处理完毕');


        dump('开始处理答题活动用户试用钥匙记录数据');
        //报名数据
        $answer_activity_user_use_prize_record = DB::connection('mysql2')->table('answer_activity_user_use_prize_record')->whereIn('act_id', $answer_activity_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_user_use_prize_record) {
            $answerActivityUserUsePrizeRecordModel = new AnswerActivityUserUsePrizeRecord();
            foreach ($answer_activity_user_use_prize_record as $key => $val) {
                $val = object_array($val);
                $answerActivityUserUsePrizeRecordModel->insert($val);
            }
        }
        dump('答题活动用户试用钥匙记录数据处理完毕');


        dump('开始处理答题活动用户文字描述数据');
        //报名数据
        $answer_activity_word_resource = DB::connection('mysql2')->table('answer_activity_word_resource')->whereIn('act_id', $answer_activity_id)->orderBy('id')->get()->toArray();
        if ($answer_activity_word_resource) {
            $answerActivityWordResourceModel = new AnswerActivityWordResource();
            foreach ($answer_activity_word_resource as $key => $val) {
                $val = object_array($val);
                $answerActivityWordResourceModel->insert($val);
            }
        }
        dump('答题活动用户文字描述数据处理完毕');
    }

    /**
     * 处理图片直播数据
     */
    public function pictureLiveData()
    {

        dump('开始处理图片直播数据');

        //TODO  获取类型数据
        $res = DB::connection('mysql2')->table('picture_live')->where('system_unit_id', $this->system_unit_id)->get()->toArray();
        foreach ($res as $key => $val) {
            $val = object_array($val);
            $pictureLiveModel = new PictureLive();
            $picture_live_info = $pictureLiveModel->where('id', $val['id'])->first();
            if (empty($picture_live_info)) {
                unset($val['system_unit_id']);
                $pictureLiveModel->insert($val);

                if ($val['img']) {
                    copy_file($this->old_file_path . $val['img'], $this->new_file_path . $val['img']); //移动图片到新地址
                }
                if ($val['main_img']) {
                    copy_file($this->old_file_path . $val['main_img'], $this->new_file_path . $val['main_img']); //移动图片到新地址
                }
                if ($val['watermark_img']) {
                    copy_file($this->old_file_path . $val['watermark_img'], $this->new_file_path . $val['watermark_img']); //移动图片到新地址
                    copy_file($this->old_file_path . str_replace('.', '1.', $val['watermark_img']), $this->new_file_path . str_replace('.', '1.', $val['watermark_img'])); //移动图片到新地址
                    copy_file($this->old_file_path . str_replace('.', '2.', $val['watermark_img']), $this->new_file_path . str_replace('.', '2.', $val['watermark_img'])); //移动图片到新地址
                }
                if ($val['qr_url']) {
                    copy_file($this->old_file_path . $val['qr_url'], $this->new_file_path . $val['qr_url']); //移动图片到新地址
                }
            }
            echo ($val['id'] . '、');
        }
        dump('图片直播数据处理完毕');

        $picture_live_id = array_column($res, 'id');

        dump('开始处理图片直播用户数据');
        //报名数据
        $picture_live_appoint_user_info = DB::connection('mysql2')->table('picture_live_appoint_user')->whereIn('act_id', $picture_live_id)->orderBy('id')->get()->toArray();
        if ($picture_live_appoint_user_info) {
            $pictureLiveAppointUserModel = new PictureLiveAppointUser();
            foreach ($picture_live_appoint_user_info as $key => $val) {
                $val = object_array($val);
                $pictureLiveAppointUserModel->insert($val);
            }
        }
        dump('图片直播用户数据处理完毕');

        dump('开始处理图片直播黑名单数据');
        //报名数据
        $picture_live_black_user_info = DB::connection('mysql2')->table('picture_live_black_user')->whereIn('act_id', $picture_live_id)->orderBy('id')->get()->toArray();
        if ($picture_live_black_user_info) {
            $pictureLiveBlackUserModel = new PictureLiveBlackUser();
            foreach ($picture_live_black_user_info as $key => $val) {
                $val = object_array($val);
                $pictureLiveBlackUserModel->insert($val);
            }
        }
        dump('图片直播黑名单数据处理完毕');

        dump('开始处理图片直播浏览数据');
        //报名数据
        $picture_live_browse_info = DB::connection('mysql2')->table('picture_live_browse')->whereIn('act_id', $picture_live_id)->orderBy('id')->get()->toArray();
        if ($picture_live_browse_info) {
            $pictureLiveBrowseModel = new PictureLiveBrowse();
            foreach ($picture_live_browse_info as $key => $val) {
                $val = object_array($val);
                $pictureLiveBrowseModel->insert($val);
            }
        }
        dump('图片直播浏览数据处理完毕');

        dump('开始处理图片直播点赞数据');
        //报名数据
        $picture_live_vote_info = DB::connection('mysql2')->table('picture_live_vote')->whereIn('act_id', $picture_live_id)->orderBy('id')->get()->toArray();
        if ($picture_live_vote_info) {
            $pictureLiveVoteModel = new PictureLiveVote();
            foreach ($picture_live_vote_info as $key => $val) {
                $val = object_array($val);
                $pictureLiveVoteModel->insert($val);
            }
        }
        dump('图片直播点赞数据处理完毕');

        dump('开始处理图片直播作品数据');
        //报名数据
        $picture_live_works_info = DB::connection('mysql2')->table('picture_live_works')
            ->select([
                'id',
                'serial_number',
                'act_id',
                'user_id',
                'title',
                'intro',
                'cover',
                'img',
                'thumb_img',
                'width',
                'height',
                'size',
                'ratio',
                'browse_num',
                'vote_num',
                'status',
                'reason',
                'way',
                'is_del',
                'create_time',
                'change_time'
            ])
            ->whereIn('act_id', $picture_live_id)->orderBy('id')->get()->toArray();
        if ($picture_live_works_info) {
            $pictureLiveWorksModel = new PictureLiveWorks();
            foreach ($picture_live_works_info as $key => $val) {
                $val = object_array($val);
                $pictureLiveWorksModel->insert($val);
                if ($val['cover']) {
                    copy_file($this->old_file_path . $val['cover'], $this->new_file_path . $val['cover']); //移动图片到新地址
                }
                if ($val['img']) {
                    copy_file($this->old_file_path . $val['img'], $this->new_file_path . $val['img']); //移动图片到新地址
                }
                if ($val['thumb_img']) {
                    copy_file($this->old_file_path . $val['thumb_img'], $this->new_file_path . $val['thumb_img']); //移动图片到新地址
                }
            }
        }
        dump('图片直播作品数据处理完毕');
    }

    /**
     * 处理banner数据
     */
    public function bannerData()
    {
        dump('开始处理banner数据');

        //TODO  获取类型数据
        $res = DB::connection('mysql2')->table('banner')->where('system_unit_id', $this->system_unit_id)->get()->toArray();
        foreach ($res as $key => $val) {
            $val = object_array($val);
            $bannerModel = new Banner();
            $banner_info = $bannerModel->where('id', $val['id'])->first();
            if (!empty($banner_info)) {
                continue;
            }
            $bannerModel->id = $val['id'];
            $bannerModel->name = $val['name'];
            $bannerModel->img = $val['img'];
            $bannerModel->link = $val['link'];
            $bannerModel->type = $val['type'];
            $bannerModel->is_play = $val['is_play'];
            $bannerModel->sort = $val['sort'];
            $bannerModel->is_del = $val['is_del'];
            $bannerModel->create_time = $val['create_time'];
            $bannerModel->change_time = $val['change_time'];
            $bannerModel->save();

            echo ($val['id'] . '、');

            if ($val['img']) {
                copy_file($this->old_file_path . $val['img'], $this->new_file_path . $val['img']); //移动图片到新地址
            }
        }
        dump('banner数据处理完毕');
    }
}
