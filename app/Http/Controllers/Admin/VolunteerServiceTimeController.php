<?php

namespace App\Http\Controllers\Admin;

use App\Models\VolunteerServiceTime;
use App\Validate\VolunteerServiceTimeValidate;

/**
 * 志愿者服务时间类
 */
class VolunteerServiceTimeController extends CommonController
{
    protected $model;
    protected $validate;

    public function __construct()
    {
        parent::__construct();
        $this->model = new VolunteerServiceTime();
        $this->validate = new VolunteerServiceTimeValidate();
    }


    /**
     * 服务时间(用于下拉框选择)
     */
    public function filterList()
    {

        $condition[] = ['is_del', '=', 1];

        return $this->model->getFilterList(['id', 'times'], $condition);
    }

    /**
     * 列表
     * @param page int 当前页
     * @param limit int 分页大小
     * @param keywords string 搜索关键词(服务时间名称)
     */
    public function lists()
    {
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;

        $condition[] = ['is_del', '=', 1];

        if ($keywords) {
            $condition[] = ['times', 'like', "%$keywords%"];
        }

        return $this->model->getSimpleList(['id', 'times', 'create_time'], $condition, $page, $limit);
    }

    /**
     * 详情
     * @param id int 服务时间id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->select(['id', 'times', 'create_time'])->find($this->request->id);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }

        return $this->returnApi(200, "获取成功", true, $res->toArray());
    }

    /**
     * 新增
     * @param times string 服务时间名称
     */
    public function add()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('add')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $times = $this->request->times;

        $is_exists = $this->model->nameIsExists($times, 'times');

        if ($is_exists) {
            return $this->returnApi(202, "该时间已存在");
        }

        $res = $this->model->add($this->request->all());

        if (!$res) {
            return $this->returnApi(202, "添加失败");
        }
        return $this->returnApi(200, "添加成功", true);
    }

    /**
     * 修改
     * @param id int 服务时间id
     * @param times string 服务时间名称
     */
    public function change()
    {

        //增加验证场景进行验证
        if (!$this->validate->scene('change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $id = $this->request->id;
        $times = $this->request->times;

        $is_exists = $this->model->nameIsExists($times, 'times', $id);

        if ($is_exists) {
            return $this->returnApi(202, "此名称已存在");
        }

        $res = $this->model->change($this->request->all());

        if (!$res) {
            return $this->returnApi(202, "修改失败");
        }
        return $this->returnApi(200, "修改成功", true);
    }

    /**
     * 删除
     * @param id int 服务时间id
     */
    public function del()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $res = $this->model->del($this->request->id);

        if ($res === true) {
            return $this->returnApi(200, "删除成功", true);
        }
        return $this->returnApi(202, $res);
    }
}
