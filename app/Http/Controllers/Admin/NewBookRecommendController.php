<?php

namespace App\Http\Controllers\Admin;

use App\Models\NewBookRecommend;
use App\Validate\NewBookRecommendValidate;
use Illuminate\Support\Facades\DB;

/**
 * 新书速递
 */
class NewBookRecommendController extends CommonController
{
    public $model = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new NewBookRecommend();
        $this->validate = new NewBookRecommendValidate();
    }


    /**
     * 列表
     * @param page int 当前页
     * @param limit int 分页大小
     * @param keywords string 搜索关键词
     * @param keywords_type string 搜索关键词类型 0或空表示全部  1 书名 2 作者名 3 ISBN号
     * @param type_id int 书籍类型(不传查询所有)
     * @param is_recom int 是否推荐 1是 2否
     */
    public function lists()
    {
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;
        $keywords_type = $this->request->keywords_type;
        $type_id = $this->request->type_id;
        $is_recom = $this->request->is_recom;

        $res = $this->model->lists($type_id, $keywords, $keywords_type, $is_recom, $limit);

        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }

        $res = $this->disPageData($res);
        $res['data'] = $this->addSerialNumber($res['data'], $page, $limit);
        $res['data'] = $this->disDataSameLevel($res['data'], 'con_type', ['type_name' => 'type_name']);

        foreach ($res['data'] as $key => $val) {
            $res['data'][$key]['intro'] = str_replace('&nbsp;', '', strip_tags($val['intro']));
        }

        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 详情
     * @param id int 书籍id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->with('conType')->find($this->request->id);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误", "YES");
        }
        $res = $res->toArray();

        $res = $this->disDataSameLevel($res, 'con_type', ['type_name' => 'type_name']);

        unset($res['con_type']);

        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 新增
     * @param book_name string 书名 必填
     * @param img string 封面 必填
     * @param type_id string 书籍类型id 必填
     * @param author string 作者 必填
     * @param isbn string isbn号 必填
     * @param press string 出版社
     * @param press_time string 出版时间
     * @param book_num string 索书号
     * @param price string 价格
     * @param intro string 简介
   //  * @param qr_url string 书籍二维码链接
     */
    public function add()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('add')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $condition[] = ['isbn', '=', $this->request->isbn]; //增加检索条件

        $is_exists = $this->model->nameIsExists($this->request->book_name, 'book_name', null, 1, $condition);

        if ($is_exists) {
            return $this->returnApi(202, "该书籍已存在");
        }
        $data = $this->request->all();
        if (empty($data['img'])) unset($data['img']);
        $res = $this->model->add($data);

        if (!$res) {
            return $this->returnApi(202, "新增失败");
        }
        return $this->returnApi(200, "新增成功", true);
    }

    /**
     * 编辑
     * @param id int 书籍id 必传
     * @param name string 书名 必填
     * @param img string 封面 必填
     * @param type_id string 书籍类型id 必填
     * @param author string 作者 必填
     * @param isbn string isbn号 必填
     * @param press string 出版社
     * @param press_time string 出版时间
     * @param number string 索书号
     * @param price string 价格
     * @param intro string 简介
  //   * @param qr_url string 书籍二维码链接
     */
    public function change()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $condition[] = ['isbn', '=', $this->request->isbn]; //增加检索条件
        $is_exists = $this->model->nameIsExists($this->request->book_name, 'book_name', $this->request->id, 1, $condition);

        if ($is_exists) {
            return $this->returnApi(202, "该书籍已存在");
        }
        $data = $this->request->all();
        if (empty($data['img'])) unset($data['img']);
        $res = $this->model->change($data);

        if (!$res) {
            return $this->returnApi(202, "修改失败");
        }
        return $this->returnApi(200, "修改成功", true);
    }

    /**
     * 删除
     * @param id int 类型id
     */
    public function del()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->del($this->request->id);

        if ($res === true) {
            return $this->returnApi(200, "删除成功", true);
        }
        return $this->returnApi(202, $res);
    }


    /**
     * 推荐与取消推荐
     * @param id int 书籍id
     * @param is_recom int 推荐   1 推荐  2 撤销
     */
    public function cancelAndRelease()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('cancel_and_release')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        DB::beginTransaction();
        try {
            list($code, $msg) = $this->model->resumeAndCancel($this->request->id, 'is_recom', $this->request->is_recom);

            $is_recom = $this->request->is_recom == 1 ? '推荐' : '撤销';

            DB::commit();
            return $this->returnApi($code, $is_recom . $msg, $code === 200 ? true : false);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, $e->getMessage());
        }
    }
}
