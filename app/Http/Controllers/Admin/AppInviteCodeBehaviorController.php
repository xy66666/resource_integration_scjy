<?php

namespace App\Http\Controllers\Admin;


/**
 * 应用邀请码行为类
 */
class AppInviteCodeBehaviorController extends CommonController
{
    protected $model;

    public function __construct()
    {
        parent::__construct();
        $this->model = new \App\Models\AppInviteCodeBehavior();
    }


    /**
     * 类型id(用于下拉框选择)
     */
    public function filterList()
    {
        $data = $this->model->getTypeName();
        if ($data) {
            return $this->returnApi(200, "查询成功", true, $data);
        }
        return $this->returnApi(203, "暂无数据");
    }

    /**
     * 列表
     * @param page int 当前页
     * @param limit int 分页大小
     * @param start_time  开始时间
     * @param end_time  结束时间
     * @param type_id_id string 类型id
     * @param invite_code_id string 邀请码id
     */
    public function lists()
    {
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        $type_id = $this->request->type_id;
        $invite_code_id = $this->request->invite_code_id;

        $res = $this->model->lists($invite_code_id, $type_id, $start_time, $end_time, $limit);

        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }

        $res['data'] = $this->addSerialNumber($res['data'], $page, $limit);
        $res = $this->disPageData($res);
        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 应用访问总统计图
     * @param invite_code_id string 邀请码id
     * @param type_id string 类型id
     * @param year   年  例如  2022   2021
     * @param month  月份  1,2,3,4,5   默认 空 按年筛选
     */
    public function accessStatistics()
    {
        $invite_code_id = $this->request->invite_code_id;
        $type_id = $this->request->type_id;
        $year = request()->year;
        $month = request()->month;
        if (empty($year)) {
            $year = date('Y');
        }
        list($start_time, $end_time) = get_start_end_time_by_year_month($month, $year);

        $start_time = date('Y-m-d 00:00:00', strtotime($start_time));
        $end_time = date('Y-m-d 23:59:59', strtotime($end_time));

        //访问人数
        $get_access_statistics = $this->model->getAccessStatistics($invite_code_id, $type_id, $start_time, $end_time, empty($month) ? 1 : 2);

        $data = self::disCartogramDayOrHourData($get_access_statistics, empty($month) ? $year : null, $start_time, $end_time, 'times');

        $data['access_total_number'] = $this->model->getAccessNumber($invite_code_id, $type_id);
        $data['access_number'] = $this->model->getAccessNumber($invite_code_id, $type_id, $start_time, $end_time);

        return $this->returnApi(200, "查询成功", true, $data);
    }
}
