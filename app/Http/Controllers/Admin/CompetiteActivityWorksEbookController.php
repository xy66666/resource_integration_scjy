<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\ImgController;
use App\Http\Controllers\PdfController;
use App\Models\CompetiteActivityWorks;
use App\Models\CompetiteActivityWorksEbook;
use App\Validate\CompetiteActivityWorksEbookValidate;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * 线上大赛作品电子书（作品管理）
 */
class CompetiteActivityWorksEbookController extends CommonController
{

    public $model = null;
    public $competiteActivityWorks = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new CompetiteActivityWorksEbook();
        $this->competiteActivityWorks = new CompetiteActivityWorks();
        $this->validate = new CompetiteActivityWorksEbookValidate();
    }

    /**
     * 获取已收录的作品id
     * @param ebook_id int 电子书id
     */
    public function filterList()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('filter_list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $condition[] = ['ebook_id', '=', $this->request->ebook_id];
        $condition[] = ['is_del', '=', 1];
        $condition[] = ['type', '=', 1];

        return $this->model->getFilterList(['id', 'works_id'], $condition);
    }

    /**
     * 列表
     * @param ebook_id int 电子书id
     * @param page int 当前页
     * @param limit int 分页大小
     * @param start_time datetime 创建时间(开始)
     * @param end_time datetime 创建时间(截止)
     * @param type datetime 类型  1 作品  2 电子书
     */
    public function lists()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $ebook_id = $this->request->ebook_id;
        $type = $this->request->type;
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;

        $res = $this->model->lists($ebook_id, $type, $start_time, $end_time, $limit);

        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }
        foreach ($res['data'] as $key => $val) {
            $res['data'][$key][$this->list_index_key] = $this->addSerialNumberOne($key, $page, $limit);
        }
        $res = $this->disPageData($res);
        return $this->returnApi(200, "查询成功", "YES", $res);
    }


    /** 
     * 收录作品（大赛作品）
     * @param ebook_id int 电子书id
     * @param works_ids int 作品id 多个逗号拼接  一次最多选30个
     * @param ebook_img int 电子书图片 多个 , 拼接
     * @param pdf_address int 电子书pdf 一次只能上传一个
     */
    public function add()
    {
        // $imgObj = new ImgController();
        // $a = $imgObj->textToImg(11);
        /*   $data['title'] = '我就是来试试的';
        $data['content'] = '我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的
        ，我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的
        ，我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的
        ，我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的
        ，我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的
        ，我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的
        ，我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的
        ，我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的，我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的
        ，我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的
        ，我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的
        ，我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的我就是来试试的';
        $data['serial_number'] = mt_rand(100, 500);
        $pdfObj = new PdfController();
        //保存文字转换为PDF格式
        $pdf_path = $pdfObj->setPdf(
            '',
            [
                '<div style="padding-top: 20px;text-align:center;">
            <h2 style="text-align: center;">' . $data['title'] . '</h1>
            <img src="' . public_path() . '/uploads/img/2023-11-20/2CS1CzRCyI4E1QEJeERV7PLYuvxeds1jZqnG7RK9.jpg">
            <div style="line-height:20px;color:#666666;text-align:center;text-indent: 20px;">
            ' . $data['content'] . '</div></div>'
            ],
            null,
            $data['serial_number'],
            60,
            false,
        );

        $pdfObj = new PdfController();
        $pdf = $pdfObj->pdf2png2($pdf_path, '/ebook_img/');

        dump($pdf);
        die; */
        //增加验证场景进行验证
        if (!$this->validate->scene('add')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        if (empty($this->request->works_ids) && empty($this->request->ebook_img) && empty($this->request->pdf_address)) {
            return $this->returnApi(201,  '电子书作品不能为空');
        }
        // 启动事务
        DB::beginTransaction();
        try {
            $data = $this->request->all();
            $data['ebook_id'] = $this->request->ebook_id;
            if ($this->request->works_ids) {
                $this->model->recordAdd($data);
            }
            if ($this->request->ebook_img) {
                $this->model->recordAddImg($data);
            }
            if ($this->request->pdf_address) {
                $this->model->recordAddPdf($data);
            }

            // 提交事务
            DB::commit();
            return $this->returnApi(200, "收录成功", true);
        } catch (\Exception $e) {
            // 回滚事务
            DB::rollBack();
            return $this->returnApi(202, "收录失败" . $e->getMessage().$e->getFile().$e->getLine());
        }
    }

    /**
     * 删除收录的作品
     * @param ids int 收录id 多个逗号拼接
     */
    public function del()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $res = $this->model->recordDel($this->request->ids);

        if ($res) {
            return $this->returnApi(200, "删除成功", true);
        }
        return $this->returnApi(202, $res);
    }



    /**
     * 排序
     * content json格式数据  [{"id":1,"sort":2},{"id":2,"sort":2},{"id":3,"sort":3},{"id":4,"sort":4},{"id":5,"sort":5}]   第一个 sort 最大
     */
    public function sortChange()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('sort_change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $content = $this->request->input('content', '');
        if (empty($content)) {
            return $this->returnApi(201, "参数传递错误");
        }
        $content = json_decode($content, true);

        DB::beginTransaction();
        try {
            foreach ($content as $key => $val) {
                $this->model->where('id', $val['id'])->update(['sort' => $val['sort']]);
            }
            DB::commit();
            return $this->returnApi(200, "修改成功", true);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, $e->getMessage());
        }
    }
}
