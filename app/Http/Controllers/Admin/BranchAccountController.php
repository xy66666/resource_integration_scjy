<?php

namespace App\Http\Controllers\Admin;

use App\Models\BranchAccount;
use App\Models\Manage;
use App\Validate\BranchAccountValidate;
use App\Validate\ManageInfoValidate;
use Illuminate\Support\Facades\DB;

/**
 * 分馆账号管理
 */
class BranchAccountController extends CommonController
{
    public $model = null;
    protected $validateObj = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new BranchAccount();
        $this->validateObj = new BranchAccountValidate();
    }

    /**
     * 用户账号管理列表
     * @param branch_id 分馆id
     * @param keywords 关键字搜索
     * @param page 页数    默认第一页
     * @param limit 限制条数  默认 10
     * @param start_time 开始时间
     * @param end_time 结束时间
     */
    public function lists()
    {
        //增加验证场景进行验证
        if (!$this->validateObj->scene('lists')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validateObj->getError());
        }

        $branch_id = $this->request->input('branch_id', '');
        $keywords = $this->request->input('keywords', '');
        $start_time = $this->request->input('start_time', '');
        $end_time = $this->request->input('end_time', '');
        $limit = $this->request->input('limit', 10);
        $page = $this->request->input('page', 1);


        $res = $this->model->select(['id', 'account', 'username', 'create_time', 'branch_id'])
            ->where(function ($query) use ($keywords) {
                $query->where('account', 'like', "%$keywords%")->Orwhere('username', 'like', "%$keywords%");
            })->where(function ($query) use ($start_time, $end_time) {
                if ($start_time && $end_time) {
                    $query->whereBetween('create_time', [$start_time, $end_time]);
                }
            })
            ->where('is_del', 1)
            ->where('branch_id', $branch_id)
            ->orderBy('id')
            ->paginate($limit)
            ->toArray();

        if ($res['data']) {
            $res = $this->disPageData($res);
            foreach ($res['data'] as $key => &$val) {
                //增加序号
                $res['data'][$key][$this->list_index_key] = $this->addSerialNumberOne($key, $page, $limit);
            }

            return $this->returnApi(200, '获取成功', true, $res);
        }

        return $this->returnApi(203, '暂无数据');
    }

    /**
     * 用户管理账号添加
     * @param branch_id 分馆账号
     * @param username 姓名至少2位数         必填
     * @param account 账号，不能存在中文  只能是中文或数字    必填
     * @param password 密码    6-20 位之间     必填
     * @param confirm_password 密码   6-20 位之间    必填
     */
    public function add()
    {
        //增加验证场景进行验证
        if (!$this->validateObj->scene('add')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validateObj->getError());
        }

        $is_exists = $this->model->nameIsExists($this->request->account, 'account',null,1,[['branch_id' , '=' , $this->request->branch_id]]);

        if ($is_exists) {
            return $this->returnApi(202, "该账号已存在");
        }


        DB::beginTransaction();
        try {
            $res = $this->model->add($this->request);

            DB::commit();
            return $this->returnApi(200, '添加成功', true);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, '添加失败');
        }
    }

    /**
     * 用户管理账号修改
     * @param id 用户管理账号id
     * @param username 姓名至少2位数   必填
     */
    public function change()
    {
        //增加验证场景进行验证
        if (!$this->validateObj->scene('change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validateObj->getError());
        }

        $is_exists = $this->model->nameIsExists($this->request->account, 'account', $this->request->id,1,[['branch_id' , '=' , $this->request->branch_id]]);

        if ($is_exists) {
            return $this->returnApi(202, "该账号已存在");
        }

        DB::beginTransaction();
        try {
            $this->model->change($this->request);

            DB::commit();
            return $this->returnApi(200, '编辑成功', true);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, '编辑失败');
        }
    }

    /**
     * 用户管理账号删除
     * @param id  用户管理账号id  必选
     */
    public function del()
    {
        //增加验证场景进行验证
        if (!$this->validateObj->scene('del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validateObj->getError());
        }
        $res = $this->model->where('is_del', 1)->find($this->request->id);
        if (empty($res)) {
            return $this->returnApi(202, '删除失败');
        }
        $res->is_del = 2;
        $result = $res->save();

        if ($result) {
            return $this->returnApi(200, '删除成功', true);
        }

        return $this->returnApi(202, '删除失败');
    }

    /**
     * 用户管理账号修改密码
     * @param id  用户管理账号id  必选
     * @param password 密码 中文加英文   6-20 位之间     必填
     * @param confirm_password 密码 中文加英文   6-20 位之间    必填
     */
    public function changePwd()
    {
        //增加验证场景进行验证
        if (!$this->validateObj->scene('change_pwd')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validateObj->getError());
        }
        $res = $this->model->where('is_del', 1)->find($this->request->id);
        if (empty($res)) {
            return $this->returnApi(202, '修改失败');
        }
        $res->password = md5($this->request->password);
        $result = $res->save();

        if ($result) {
            return $this->returnApi(200, '修改成功', true);
        }

        return $this->returnApi(202, '修改失败');
    }


}
