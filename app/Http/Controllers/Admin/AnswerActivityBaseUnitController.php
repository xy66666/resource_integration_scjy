<?php

namespace App\Http\Controllers\Admin;


use App\Models\AnswerActivityBaseUnit;
use App\Models\AnswerActivityUnit;
use App\Validate\AnswerActivityBaseUnitValidate;
use Illuminate\Support\Facades\DB;


/**
 * 活动单位基础数据管理
 */
class AnswerActivityBaseUnitController extends CommonController
{

    public $model = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new AnswerActivityBaseUnit();
        $this->validate = new AnswerActivityBaseUnitValidate();
    }

    /**
     * 获取单位性质
     */
    public function getNatureName()
    {
        $data = $this->model->getNatureName();
        $config_node = config('other.activity_unit_nature');

        $new_data = [];
        $i = 0;
        foreach ($data as $key => $val) {
            if (in_array($key, $config_node)) {
                $new_data[$i]['id'] = $key;
                $new_data[$i]['name'] = $val;
                $i++;
            }
        }

        if ($new_data) {
            return $this->returnApi(200, "获取成功", true, $new_data);
        }
        return $this->returnApi(203, "暂无数据");
    }


    /**
     * 列表(用于下拉框选择)
     * @param except_act_id 活动id
     */
    public function filterList()
    {
        $except_act_id = $this->request->except_act_id;
        $res = $this->model->filterList(['id', 'name', 'img', 'nature'], $except_act_id);
        if ($res) {
            return $this->returnApi(200, "获取成功", true , $res);
        }
        return $this->returnApi(203, "暂无数据");
    }

    /**
     * 列表
     * @param page int 当前页
     * @param limit int 分页大小
     * @param nature int 单位性质   1 图书馆  2 文化馆  3 博物馆
     * @param start_time datetime 创建时间范围搜索(开始)
     * @param end_time datetime 创建时间范围搜索(结束)
     * @param keywords string 搜索关键词(名称)
     */
    public function lists()
    {
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        $keywords = $this->request->keywords;

        $res = $this->model->lists(null, $keywords, null, $start_time, $end_time, $limit);

        if ($res['total'] == 0 || !$res['data']) {
            return $this->returnApi(203, "暂无数据");
        }

        foreach ($res['data'] as $key => $val) {
            $res['data'][$key]['intro'] = strip_tags($val['intro']);
            $res['data'][$key]['nature_name'] = $this->model->getNatureName($val['nature']);
            $res['data'][$key][$this->list_index_key] = $this->addSerialNumberOne($key, $page, $limit);
        }

        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 详情
     * @param id int id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->detail($this->request->id);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }

        return $this->returnApi(200, "获取成功", true, $res->toArray());
    }

    /**
     * 新增
     * @param nature int单位性质   1 图书馆  2 文化馆  3 博物馆
     * @param name string 名称
     * @param img string 图片
     * @param intro string 简介
     * @param tel string 联系电话
     * @param contacts string 联系人
     * @param words string 单位赠言，最多50字
     * @param province string 省    选填
     * @param city string 市           选填
     * @param district string 区(县)      选填
     * @param address string 详细地址      选填
     */
    public function add()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('add')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $limitWords = $this->model->limitWords($this->request->words);
        if ($limitWords !== true) {
            return $this->returnApi(202, $limitWords);
        }

        $is_exists = $this->model->nameIsExists($this->request->name, 'name', null, 1);
        if ($is_exists) {
            return $this->returnApi(202, "此名称已存在");
        }
        $data = $this->request->all();

        $data['letter'] = get_first_charter($this->request->name);
        if (empty($data['img'])) {
            unset($data['img']); //使用默认图
        }
        /*生成经纬度*/
        if (!empty($this->request->province) && !empty($this->request->city) && !empty($this->request->district) && !empty($this->request->address)) {
            $map = $this->getLonLatByAddress($this->request->province . $this->request->city . $this->request->district . $this->request->address);
            if (is_string($map)) {
                return $this->returnApi(202, $map);
            }

            $data['lon'] = $map['lon'];
            $data['lat'] = $map['lat'];
        } else {
            $data['lon'] = null;
            $data['lat'] = null;
        }

        $res = $this->model->add($data);
        if (!$res) {
            return $this->returnApi(202, "新增失败");
        }
        return $this->returnApi(200, "新增成功", true);
    }

    /**
     * 修改
     * @param id int 单位id
     * @param nature int单位性质   1 图书馆  2 文化馆  3 博物馆
     * @param name string 名称   最多 3个字符
     * @param img string 图片
     * @param intro string 简介
     * @param tel string 联系电话
     * @param contacts string 联系人
     * @param words string 单位赠言，最多50字
     * @param province string 省    选填
     * @param city string 市           选填
     * @param district string 区(县)      选填
     * @param address string 详细地址      选填
     */
    public function change()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $limitWords = $this->model->limitWords($this->request->words);
        if ($limitWords !== true) {
            return $this->returnApi(202, $limitWords);
        }

        $result = $this->model->where('id', $this->request->id)->first();
        if (empty($result)) {
            return $this->returnApi(201, "参数传递错误");
        }
        $is_exists = $this->model->nameIsExists($this->request->name, 'name', $this->request->id, 1);
        if ($is_exists) {
            return $this->returnApi(202, "此名称已存在");
        }

        $data = $this->request->all();
        $data['img'] = $data['img'] ? $data['img'] : 'default/default_readshare_unit.png';
        $data['letter'] = get_first_charter($this->request->name);

        /*生成经纬度*/
        if (
            !empty($this->request->province) && !empty($this->request->city) && !empty($this->request->district) && !empty($this->request->address)
            && $this->request->province . $this->request->city . $this->request->district . $this->request->address != $result->province . $result->city . $result->district . $result->address
        ) {
            $map = $this->getLonLatByAddress($this->request->province . $this->request->city . $this->request->district . $this->request->address);
            if (is_string($map)) {
                return $this->returnApi(202, $map);
            }
            $data['lon'] = $map['lon'];
            $data['lat'] = $map['lat'];
        } else {
            $data['lon'] = null;
            $data['lat'] = null;
        }
        $res = $this->model->change($data);
        if (!$res) {
            return $this->returnApi(202, "修改失败");
        }
        return $this->returnApi(200, "修改成功", true);
    }

    /**
     * 删除
     * @param id int id
     */
    public function del()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->del($this->request->id);

        if ($res === true) {
            return $this->returnApi(200, "删除成功", true);
        }
        return $this->returnApi(202, $res);
    }

    /**
     * 修改单位排序
     * @param orders 排序   json格式数据  里面2个参数  id 单位id   order 序号
     *
     * json 格式数据  [{"id":1,"order":1},{"id":2,"order":2},{"id":3,"order":3}]
     * 数组 格式数据  [['id'=>1 , 'order'=>1] , ['id'=>2 , 'order'=>2] , ['id'=>3 , 'order'=>3]]
     */
    public function orderChange()
    {
        if (!$this->validate->scene('order')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $orders = $this->request->orders;
        DB::beginTransaction();
        try {
            $this->model->orderChange($orders);
            DB::commit();
            return $this->returnApi(200, '修改成功', true);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, $e->getMessage());
        }
    }
}
