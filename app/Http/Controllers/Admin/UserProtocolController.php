<?php

namespace App\Http\Controllers\Admin;

use App\Models\UserProtocol;
use App\Validate\UserProtocolValidate;

/**
 * 协议规则
 */
class UserProtocolController extends CommonController
{

    public $model = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new UserProtocol();
        $this->validate = new UserProtocolValidate();
    }



    /**
     * 获取用户协议
     * @param type  不传表示获取全部 1 用户协议    2  大赛原创声明  3 景点打卡  4 在线办证  5、扫码借借阅规则
     */
    public function getProtocol()
    {
        $type = $this->request->type;
        $res = UserProtocol::select('id','type', 'content')
            ->where(function ($query) use ($type) {
                if ($type) {
                    $query->where('type', $type);
                }
            })->get()->toArray();

            if ($res) {
                return $this->returnApi(200, '获取成功', true ,$res);
            }
            return $this->returnApi(202, '暂无数据');
    }


    /**
     * 设置用户协议
     * @param type  1 用户协议    2  大赛原创声明  3 景点打卡  4 在线办证  5、扫码借借阅规则
     * @param content  内容
     */
    public function setProtocol()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $type = $this->request->type;
        $content = $this->request->content;
        $userProtocolModel = new UserProtocol();
        $res = $userProtocolModel->select('id', 'content')->where('type', $type)->first();
        if ($res) {
            $res->content = $content;
            $result = $res->save();
        } else {
            $userProtocolModel->type = $type;
            $userProtocolModel->content = $content;
            $result = $userProtocolModel->save();
        }
        if ($result) {
            return $this->returnApi(200, '配置成功', true);
        }
        return $this->returnApi(202, '配置失败');
    }
}
