<?php

namespace App\Http\Controllers\Admin;


use App\Models\PictureLive;
use App\Models\PictureLiveWorks;


/**
 * 图片展播活动 数据总分析
 * Class AnswerType
 * @package app\admin\controller
 */
class PictureLiveToatlDataAnalysisController extends CommonController
{
    public $model = null;
    public $worksModel = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new PictureLive();
        $this->worksModel = new PictureLiveWorks();
    }


    /**
     * 图片展播总数据统计
     */
    public function pictureLiveDataStatistics()
    {
        //作品数量
        $works_number = $this->worksModel->getWorksNumber(null);
        //作品数量
        $vote_number = $this->worksModel->getTotalVoteNumber(null);
        //总点击量
        $browse_number = $this->model->getBrowseNumber(null);

        $data = [
            'works_number' => $works_number,
            'vote_number' => $vote_number,
            'browse_number' => $browse_number,
        ];

        return $this->returnApi(200, "获取成功", true, $data);
    }

    /**
     * 图片直播-最新活动排行榜
     */
    public function pictureLiveDataRanking()
    {
        $data = $this->model->select('id', 'title', 'browse_num')
            ->where('is_del', 1)
            ->orderByDesc('id')
            ->limit(10)
            ->get()
            ->toArray();
        foreach ($data as $key => $val) {
            $data[$key]['works_number'] = $this->worksModel->getWorksNumber($val['id']);
            $data[$key]['vote_number'] = $this->worksModel->getTotalVoteNumber($val['id']);
        }
        return $this->returnApi(200, "获取成功", true, $data);
    }
}
