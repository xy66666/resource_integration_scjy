<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\PdfController;
use App\Http\Controllers\ScoreRuleController;
use App\Http\Controllers\ZipFileDownloadController;
use App\Models\CompetiteActivity;
use App\Models\CompetiteActivityWorksType;
use App\Models\CompetiteActivityGroup;
use App\Models\CompetiteActivityWorks;
use App\Models\CompetiteActivityWorksCheck;
use App\Models\CompetiteActivityWorksScore;
use App\Models\Manage;
use App\Models\UserInfo;
use App\Validate\CompetiteActivityValidate;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * 线上大赛作品
 */
class CompetiteActivityWorksScoreController extends CommonController
{

    public $score_type = 24;
    public $model = null;
    public $competiteActivityModel = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new CompetiteActivityWorks();
        $this->competiteActivityModel = new CompetiteActivity();
        $this->validate = new CompetiteActivityValidate();
    }




    /**
     * 参赛作品打分列表
     * @param type 类型  1 个人账号  2 团队账号  当前是团队查询还是个人查询
     * @param con_id id
     * @param page int 当前页
     * @param limit int 分页大小
     * @param keywords string 搜索关键词(名称)
     * @param is_review_score string 是否评审打分  1 已打分  2 未打分
     * @param score_manage_id string 打分审核人id  默认是自己
     */
    public function scoreLists()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('score_list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $type = $this->request->type;
        $con_id = $this->request->con_id;
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;
        $is_review_score = $this->request->is_review_score;
        $score_manage_id = request()->manage_id; //自己的id

        $res = $this->model->scoreLists($con_id, $type, $keywords, $is_review_score, $score_manage_id, $limit);
        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }

        foreach ($res['data'] as $key => $val) {
            $res['data'][$key][$this->list_index_key] = $this->addSerialNumberOne($key, $page, $limit);

            if (isset($val['review_score'])) {
                $res['data'][$key]['review_score_status'] = 1; //已打分 
            } else {
                $res['data'][$key]['review_score_status'] = 3; //待打分
            }
        }

        $res = $this->disPageData($res);
        return $this->returnApi(200, "查询成功", "YES", $res);
    }


    /**
     * 详情
     * @param id int 作品id
     */
    public function scoreDetail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('production_detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $score_manage_id = request()->manage_id; //自己的id
        $res = $this->model->scoreDetail($this->request->id, $score_manage_id);
        if (empty($res)) {
            return $this->returnApi(203, "暂无数据");
        }
        $res = $res->toArray();
        if (isset($res['review_score'])) {
            $res['review_score_status'] = 1; //已打分
        } else {
            $res['review_score_status'] = 3; //待打分
        }

        return $this->returnApi(200, "获取成功", true, $res);
    }



    /**
     * 管理员自动获取下一个未打分作品
     * @param type 类型  1 个人账号  2 团队账号  当前是团队查询还是个人查询
     * @param con_id id
     * @param keywords string 搜索关键词(名称)
     * @param score_manage_id string 打分审核人id  默认是自己
     */
    public function getNextUnScoreWorks()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('get_next_unscoreed')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        //用于获取下一个作品
        $type = $this->request->type;
        $con_id = $this->request->con_id;
        $keywords = $this->request->keywords;
        $score_manage_id = request()->manage_id; //自己的id

        //获取下一个未打分的活动
        $res = $this->model->getNextUnScoreWorks($con_id, $type, $keywords, $score_manage_id);
        if (empty($res)) {
            return $this->returnApi(203, "暂无数据");
        }
        return $this->returnApi(200, "获取成功", true, $res);
    }
}
