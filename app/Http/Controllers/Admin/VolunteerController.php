<?php

namespace App\Http\Controllers\Admin;

use App\Models\VolunteerApply;
use App\Models\VolunteerNotice;
use App\Models\VolunteerPosition;
use App\Models\VolunteerServiceIntention;
use App\Models\VolunteerServiceTime;
use App\Validate\VolunteerValidate;
use Illuminate\Support\Facades\DB;

/**
 * 志愿者服务
 */
class VolunteerController extends CommonController
{

    public $model = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new VolunteerPosition();
        $this->validate = new VolunteerValidate();
    }

    /**
     * 志愿者报名参数(用于下拉框选择)
     */
    public function volunteerApplyParam()
    {
        $data = $this->model->volunteerApplyParam();
        return $this->returnApi(200, "获取成功", true, $data);
    }

    /**
     * 服务岗位列表(用于下拉框选择)
     */
    public function filterList()
    {
        $condition[] = ['is_del', '=', 1];
        return $this->model->getFilterList(['id', 'name'], $condition);
    }
    /**
     * 参加志愿者所需下拉选项(用于下拉框选择)
     * node 类型 1 服务意向  2 服务时间 3 教育程度 4 健康状态 5 性别    多个参数 自由组合，比如都需要则node为 12345
     */
    public function getPullFilterList()
    {
        $node = request()->input('node');
        if(empty($node)){
            return $this->returnApi(200, "获取成功", true, null);  
        }
        $node = str_split($node);

        $volunteerApplyModel = new VolunteerApply();

        $data = [];
        if (in_array(1, $node)) {
            $data['service_intention'] = VolunteerServiceIntention::getServiceIntentionAll();
        }
        if (in_array(2, $node)) {
            $data['service_time'] = VolunteerServiceTime::getServiceTimeAll();
        }
        if (in_array(3, $node)) {
            $degree = $volunteerApplyModel->getDegree();//教育程度
            foreach($degree as $key=>$val){
                $data['degree'][$key - 1]['id'] = $key;
                $data['degree'][$key - 1]['name'] = $val;
            }
        }
        if (in_array(4, $node)) {
            $health = $volunteerApplyModel->getHealth();//健康状态
            foreach($health as $key=>$val){
                $data['health'][$key - 1]['id'] = $key;
                $data['health'][$key - 1]['name'] = $val;
            }
        }
        if (in_array(5, $node)) {
            $sex = $volunteerApplyModel->getSex(); //性别
            foreach($sex as $key=>$val){
                $data['sex'][$key - 1]['id'] = $key;
                $data['sex'][$key - 1]['name'] = $val;
            }
        }

        return $this->returnApi(200, "获取成功", true, $data);
    }

    /**
     * 服务岗位列表
     * @param page int 当前页
     * @param limit int 分页大小
     * @param keywords string 搜索关键词
     */
    public function lists()
    {
        $limit = $this->request->input('limit', 10);
        $page = $this->request->input('page', 1);
        $keywords = $this->request->input('keywords', '');

        $data = $this->model->lists($keywords, $limit);
        if (empty($data['data'])) {
            return $this->returnApi(203, '暂无数据');
        }

        $data = $this->disPageData($data);

        foreach($data['data'] as $key=>$val){
            $data['data'][$key]['real_info_name'] = $this->model->getRealInfoName($val['real_info']);
            $data['data'][$key]['real_info_must_name'] = $this->model->getRealInfoName($val['real_info_must']);
        }

        //增加序号
        $data['data'] = $this->addSerialNumber($data['data'], $page, $limit);
        return $this->returnApi(200, '获取成功', true, $data);
    }

    /**
     * 服务岗位详情
     * @param id int 服务岗位id
     */
    public function detail()
    {
        //增加验证场景进行验证
        // if (!$this->validate->scene('detail')->check($this->request->all())) {
        //     return $this->returnApi(201,  $this->validate->getError());
        // }
        $id = $this->request->id ?? 1;

        $res = $this->model->detail($id);

        if (!$res) {
            return $this->returnApi(203, "暂无数据");
        }
        return $this->returnApi(200, "获取成功", true, $res->toArray());
    }
    /**
     * 服务岗位添加
     * @param is_reader int 打卡是否需要绑定读者证  1 是  2 否
  //   * @param name int 岗位名称
   //  * @param intro int 岗位简介
     * @param real_info int 报名所需信息 多个 | 链接    1、姓名 2、性别 3、生日  4、身份证号码  5、民族 6、政治面貌  7、健康状态  8、联系电话  9、毕业院校  10、专业  11、工作单位 12、教育程度  13、个人简介  14、特长  15、地址  
     * @param real_info_must int 报名必填信息 多个 | 链接    1、姓名 2、性别 3、生日  4、身份证号码  5、民族 6、政治面貌  7、健康状态  8、联系电话  9、毕业院校  10、专业  11、工作单位 12、教育程度  13、个人简介  14、特长  15、地址  
   //  * @param expire_time 岗位有效期结束时间
     */
    public function add()
    {
        // if (!$this->validate->scene('add')->check($this->request->all())) {
        //     return $this->returnApi(201, $this->validate->getError());
        // }
        // $is_exists = $this->model->nameIsExists($this->request->name, 'name');

        // if ($is_exists) {
        //     return $this->returnApi(202, "此名称已存在");
        // }
        
        $id = $this->request->id ?? 1;

        DB::beginTransaction();
        try {
            $data = $this->request->all();
            $data['id'] = $id;//只能新增一个，多个需删除
            $this->model->add($data);

            DB::commit();
            return $this->returnApi(200, "新增成功", true);
        } catch (\Exception $e) {
          //  echo $e->getMessage();
            DB::rollBack();
            return $this->returnApi(202, "新增失败");
        }
    }

    /**
     * 服务岗位编辑
     * @param id int 服务岗位id
     * @param is_reader int 打卡是否需要绑定读者证  1 是  2 否
   //  * @param name int 岗位名称
   //  * @param intro int 岗位简介
     * @param real_info int 报名所需信息 多个 | 链接    1、姓名 2、性别 3、生日  4、身份证号码  5、民族 6、政治面貌  7、健康状态  8、联系电话  9、毕业院校  10、专业  11、工作单位 12、教育程度  13、个人简介  14、特长  15、地址  
     * @param real_info_must int 报名必填信息 多个 | 链接    1、姓名 2、性别 3、生日  4、身份证号码  5、民族 6、政治面貌  7、健康状态  8、联系电话  9、毕业院校  10、专业  11、工作单位 12、教育程度  13、个人简介  14、特长  15、地址  
   //  * @param expire_time 岗位有效期结束时间
     */
    public function change()
    {
        // if (!$this->validate->scene('change')->check($this->request->all())) {
        //     return $this->returnApi(201, $this->validate->getError());
        // }
        $id = $this->request->id ?? 1;

        // $is_exists = $this->model->nameIsExists($this->request->name, 'name', $this->request->id);

        // if ($is_exists) {
        //     return $this->returnApi(202, "此名称已存在");
        // }

        $res = $this->model->where('is_del', 1)->find($id);

        if (!$res) {
            $this->returnApi(201, "参数传递错误");
        }
        if ($res->is_play == 1) {
            $this->returnApi(201, "请先撤销在进行修改");
        }

        DB::beginTransaction();
        try {
            $this->model->change($this->request->all());

            DB::commit();
            return $this->returnApi(200, "修改成功", true);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, "修改失败");
        }
    }

    /**
     * 删除
     * @param id int 岗位id
     */
    public function del()
    {
        return $this->returnApi(202, '不支持删除');


        //增加验证场景进行验证
        if (!$this->validate->scene('del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $volumteerApplyModel = new VolunteerApply();
        $apply_status = $volumteerApplyModel->getVolunteerNumber($this->request->id,[1 , 4]);
        if ($apply_status) {
            return $this->returnApi(201, "已有用户参加，不能删除");
        }

        $res = $this->model->del($this->request->id);

        if ($res === true) {
            return $this->returnApi(200, "删除成功", true);
        }
        return $this->returnApi(202, $res);
    }

    /**
     * 发布与取消发布
     * @param ids 岗位id，多个用逗号拼接   all 是，其余字符  ids 必传
     * @param is_play 是否发布  1.发布  2.未发布  默认2
     */
    public function playAndCancel()
    {
        //增加验证场景进行验证
        // if (!$this->validate->scene('play_and_cancel')->check($this->request->all())) {
        //     return $this->returnApi(201,  $this->validate->getError());
        // }

        $ids = $this->request->ids ?? 1;
        if ($ids == 1) {
            $res = $this->model->detail(1);
            if (!$res) {
                return $this->returnApi(202, "请先提交基本信息");
            }
        }

        list($code, $msg) = $this->model->resumeAndCancel($ids, 'is_play', $this->request->is_play);

        $is_play = $this->request->is_play == 1 ? '开启招募' : '关闭招募';
        if ($code === 200) {
            return $this->returnApi(200, $is_play . $msg, true);
        }
        return $this->returnApi($code, $is_play . $msg);
    }

    /**
     * 获取志愿者须知
     */
    public function getVolunteerNotice()
    {
        $volunteerNoticeModel = new VolunteerNotice();
        $res = $volunteerNoticeModel->where('id', 1)->first();

        if (!$res) {
            return $this->returnApi(203, "暂无数据");
        }
        return $this->returnApi(200, "获取成功", true, $res->toArray());
    }

    /**
     * 设置志愿者须知
     * @param  content 志愿者须知内容
     */
    public function setVolunteerNotice()
    {
        $volunteerNoticeModel = new VolunteerNotice();
        $res = $volunteerNoticeModel->setVolunteerNotice($this->request->content);

        if ($res) {
            return $this->returnApi(200, "设置成功", true);
        }
        return $this->returnApi(202, "设置失败");
    }
}
