<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Shop;
use Illuminate\Http\Request;

/**
 * 公用类
 */
class CommonController extends Controller
{
    public $list_index_key = '';

    public function __construct()
    {
        parent::__construct();
        $this->list_index_key = config('other.list_index_key');
    }

    /**
     * 图书到家获取管辖书店id
     */
    public function getAdminShopIdAll()
    {
        $manage_id = request()->manage_id;
        if ($manage_id == 1) {
            $data = Shop::where('is_del', 1)->pluck('id')->toArray();
        } else {
            $shop_all_id = request()->manage_info['shop_id'];
            if (empty($shop_all_id)) {
                $data = null;
            } else {
                $data = !is_array($shop_all_id) ? explode(',', $shop_all_id) : $shop_all_id;
            }
        }
        return $data;
    }



    /**
     * 数据循环增加 序号
     * @param $data 总数据
     * @param $page 页数
     * @param $limit 限制条数
     */
    public function addSerialNumber($data, $page = 1, $limit = 10, $list_index_key = '')
    {
        if (empty($list_index_key)) $list_index_key = $this->list_index_key;

        foreach ($data as $key => $val) {
            $data[$key][$list_index_key] = ($page - 1) * $limit + $key + 1;
        }
        return $data;
    }

    /**
     * 单个添加排序
     * @param key int 数据数组键值  从 0 开始
     * @param page int 当前页数
     * @param limit int 分页大小
     */
    public function addSerialNumberOne($key, $page = 1, $limit = 1)
    {
        return $key + 1 + ($page - 1) * $limit;
    }


    /**
     * 处理统计图数据，把没有小时或者天或月的数据补上 (按日期统计) (有year 按照月统计，同一天按小时统计，不同天，按天统计)
     * @param $data  二维数据，必须包括   count  计数   day 日期参数
     * @param $start_time   开始时间
     * @param $end_time  结束时间
     */
    public static function disCartogramDayOrHourData($data, $year, $start_time, $end_time, $field = 'times', $count = 'count')
    {
        if ($start_time && $end_time) {
            $start_time = date('Y-m-d', strtotime($start_time));
            $end_time = date('Y-m-d', strtotime($end_time));
        }
        $res = array_column($data, $count, $field); //组装数据   day为健名   count为键值
        //获取时间段类的所有日期
        $time = ''; //是否加上前缀匹配
        if ($year) {
            $time_arr = get_month_by_year($year); //有年优先用年统计
            $postfix = '';
        } elseif ($start_time == $end_time) {
            $time_arr = pr_day_hour($start_time, $end_time); //按小时统计
            $postfix = ':00';
            $time = $start_time;
        } else {
            $time_arr = pr_dates($start_time, $end_time); //按天统计
            $postfix = '';
        }
        $return_data = self::disStatisticsData($time_arr, $res, $postfix, $time);
        return  $return_data;
    }

    /**
     * 计算统计数据
     * $postfix  拼接后缀，主要用于  小时，拼接后缀
     * $time  //按小时统计时 增加前缀判断 时间点里面不带时间，但是数据里面带了时间
     */
    public static function disStatisticsData($time_arr, $data, $postfix = '', $time = '')
    {
        $return_data = [];
        foreach ($time_arr as $key => $val) {
            $return_data['time'][] = $val . $postfix;
            if (isset($data[$val])) {
                $return_data['count'][] = (int)$data[$val];
            } elseif (!empty($time) && isset($data[$time . ' ' . $val])) {
                $return_data['count'][] = (int)$data[$time . ' ' . $val]; //按小时统计时 增加前缀判断
            } else {
                $return_data['count'][] = 0;
            }
        }
        return  $return_data;
    }

    /**
     * 获取文本里面的图片和资源 临时文件还需要放在指定目录
     * @param content 
     * @param $video_address  视频存放地址
     */
    function getSourceByContent($content, $video_address)
    {
        $contentVideoAddress = get_source_by_content($content, 'video');
        $getImgAddrUrl = $this->getImgAddrUrl();
        $host_addr = public_path('uploads');
        $video = '';
        foreach ($contentVideoAddress as $key => $val) {
            if (stripos($val, $getImgAddrUrl) !== false) {
                $temp_video = str_replace($getImgAddrUrl, '', $val); //只需要一个视频

                //如果是临时图片，移动到指定位置
                if (stripos($val, 'temp_video') !== false) {
                    $extension = explode('.', $val);
                    $move_file_address = $video_address . '/' . date('Y-m-d') . '/' . uniqid() . '.' . end($extension);
                    $move_file = move_uploaded_files($host_addr . '/' . $temp_video,  $host_addr . '/' . $move_file_address);
                    if (!$move_file) {
                        throw new \Exception('音频保存失败');
                    }
                    $temp_video = $move_file_address;
                }
                if (empty($video)) {
                    $video = $temp_video; //只需要一个视频
                }
            }
        }
        $img = [];
        if (empty($video)) {
            $contentImgAddress = get_source_by_content($this->request->content);
            $getImgAddrUrl = $this->getImgAddrUrl();
            foreach ($contentImgAddress as $key => $val) {
                if (stripos($val, $getImgAddrUrl) !== false && count($img) < 3) {
                    $img[] = str_replace($getImgAddrUrl, '', $val);
                }
            }
            $img = join('|', $img);
        }
        $img = $img ? $img : '';
        $source = $video ? $video : $img;
        return $source;
    }
}
