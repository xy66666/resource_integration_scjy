<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\UploadsFileController;
use App\Models\TurnActivity;
use App\Models\TurnActivityResource;
use App\Validate\TurnActivityResourceValidate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * 活动资源类
 */
class TurnActivityResourceController extends CommonController
{
  protected $model;
  protected $validate;

  public function __construct()
  {
    parent::__construct();

    $this->model = new TurnActivityResource();
    $this->validate = new  TurnActivityResourceValidate();
  }


  /**
   * 获取系统资源配置 （颜色配置）
   * @param act_id 活动id
   */
  public function getColorResource()
  {
    //增加验证场景进行验证
    if (!$this->validate->scene('get_color_resource')->check($this->request->all())) {
      return $this->returnApi(201,  $this->validate->getError());
    }
    $res = $this->model->detail($this->request->act_id);
    if ($res) {
      return $this->returnApi(200, '获取成功', true, $res);
    }
    return $this->returnApi(203, '获取失败');
  }

  /**
   * 设置系统资源配置 （颜色配置）
   * @param act_id 活动id
   * @param theme_color 主题色
 //  * @param progress_color 进度条颜色
   * @param invite_color 邀请码颜色
   * @param theme_text_color 主题文字色
   * @param warning_color 警告颜色
   * @param error_color 错误颜色
   */
  public function setColorResource()
  {
    //增加验证场景进行验证
    if (!$this->validate->scene('set_color_resource')->check($this->request->all())) {
      return $this->returnApi(201,  $this->validate->getError());
    }
    $res = $this->model->change($this->request->all());
    if ($res) {
      return $this->returnApi(200, '修改成功', true);
    }
    return $this->returnApi(200, '修改失败');
  }

  /**
   * 下载ui资源文件 
   * @param act_id 活动id
   */
  public function resourceDownload()
  {
    //增加验证场景进行验证
    if (!$this->validate->scene('resource_download')->check($this->request->all())) {
      return $this->returnApi(201,  $this->validate->getError());
    }
    $resource_addr = $this->model->getImgStoreDir($this->request->act_id);

    if (!file_exists($resource_addr)) {
      return $this->returnApi(202, "资源不存在，下载失败");
    }
    //获取活动名称
    $act_name = TurnActivity::where('id', $this->request->act_id)->where('is_del', 1)->value('title');
    if (empty($act_name)) {
      return $this->returnApi(202, "活动不存在，不支持下载此资源");
    }
    $act_name = replace_special_char($act_name);

    //初始化zip 名字
    $zipname = $resource_addr . '.zip'; //要下载的zip的地址，圈地自

    // if (file_exists($zipname)) {
    //    return response()->download($zipname);
    // }

    $zip = new \ZipArchive();
    $res = $zip->open($zipname, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

    if ($res === TRUE) {
      //获取文件夹下的所有资源
      $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($resource_addr));
      $number = 0;
      foreach ($files as $name => $file) {
        $number++;
        // 我们要跳过所有子目录
        if (!$file->isDir()) {
          $filePath     = $file->getRealPath();
          // 用 substr/strlen 获取文件扩展名
          $relativePath = substr($filePath, strlen($resource_addr) + 1);
          $zip->addFile($filePath, $relativePath);
        }
      }
      if ($number <= 2) {
        return $this->returnApi(202, "资源不存在，下载失败");
      }
    } else {
      return $this->returnApi(202, "压缩资源准备失败,请联系开发人员");
    }

    $zip->close();

    return response()->download($zipname, $act_name . '-UI资源.zip');
  }

  /**
   * 上传ui资源文件 
   * @param act_id 活动id
   * @param ui_resource 文件名   
   */
  public function resourceUpload(Request $request)
  {
    //增加验证场景进行验证
    if (!$this->validate->scene('resource_upload')->check($this->request->all())) {
      return $this->returnApi(201,  $this->validate->getError());
    }
    $resource_addr = $this->model->getImgStoreDir($this->request->act_id);
    $resource_addr_arr = explode('/', $resource_addr);

    // if(!file_exists($resource_addr)){
    //     return $this->returnApi(202, "资源不存在，上传失败");
    // }
    $act_name = TurnActivity::where('id', $this->request->act_id)->where('is_del', 1)->value('title');
    if (empty($act_name)) {
      return $this->returnApi(202, "活动不存在，不支持上传此资源");
    }

    //验证文件类型和大小
    $uploadsFileController = new UploadsFileController();
    $file_validate = $uploadsFileController->fileValidate($this->request, 'ui_resource');

    if ($file_validate !== true) {
      return $this->returnApi(202, $file_validate);
    }
    //放到临时文件夹下
    $request->file('ui_resource')->move(dirname($resource_addr) . '/temp', end($resource_addr_arr) . '.zip'); //上传文件资源

    $temp_file_path = $this->unzip(dirname($resource_addr) . '/temp/' . end($resource_addr_arr) . '.zip',  dirname($resource_addr) . '/temp/', true);

    //把文件移动到指定文件夹下
    //move_dir_file($temp_file_path, $resource_addr . '/');

    //把文件移动到指定文件夹下
    $temp_explode_name = explode('/temp/', $temp_file_path);
    $temp_explode_name = explode('/', end($temp_explode_name));
    $temp_explode_name = $temp_explode_name[0];

    //解决有些文件多一级的问题
    if (strpos($temp_explode_name, '.')) {
      move_dir_file(dirname($resource_addr) . '/temp/', $resource_addr . '/');
      $temp_file_path_arr = explode('uploads', dirname($resource_addr) . '/temp/');
    } else {
      move_dir_file(dirname($resource_addr) . '/temp/' . $temp_explode_name . '/', $resource_addr . '/');
      $temp_file_path_arr = explode('uploads', dirname($resource_addr) . '/temp/' . $temp_explode_name . '/');
    }

    //删除临时文件
    $this->deleteFile([
      end($temp_file_path_arr),
      '/turn_activity/ui_resource/temp/' . end($resource_addr_arr) . '.zip',
      '/turn_activity/ui_resource/' . end($resource_addr_arr) . '/' . end($resource_addr_arr) . '.zip',
      '/turn_activity/ui_resource/temp'
    ]);

    //修改当前信息为已修改状态
    $res = $this->model->where('act_id', $this->request->act_id)->first();

    //重新获取一个新名称 ,解决前端，图片不能及时更新问题
    $new_resource_addr = $this->model->getNewImgStoreName($this->request->act_id);
    if ($resource_addr != public_path('uploads') . '/' . $new_resource_addr) {
      rename($resource_addr, public_path('uploads') . '/' . $new_resource_addr); //修改名称
    }

    if ($res) {
      $this->model->where('act_id', $this->request->act_id)->update(['is_change' => 2, 'resource_path' => $new_resource_addr, 'change_time' => date('Y-m-d H:i:s')]);
    } else {
      $this->model->insert(['act_id' => $this->request->act_id, 'is_change' => 2, 'resource_path' => $new_resource_addr, 'create_time' => date('Y-m-d H:i:s'), 'change_time' => date('Y-m-d H:i:s')]);
    }

    return $this->returnApi(200, "上传成功", true);
  }

  /**
   * 获取默认活动资源路径
  // * @param act_id
   * 
   *   //node   活动类型  1 独立活动  2 单位联盟   3 区域联盟   
   *   //pattern   答题模式  1  馆内答题形式  2 轮次排名形式 （按轮次排名）  3 爬楼梯形式
   *   //prize_form   获奖情况  1 排名  2 抽奖
   */
  public function getDefaultResourceAddr()
  {
    //增加验证场景进行验证
    // if (!$this->validate->scene('default_resource_addr')->check($this->request->all())) {
    //   return $this->returnApi(201,  $this->validate->getError());
    // }
    $addr = $this->model->getDefaultResourceAddr($this->request->act_id);

    if ($addr) {
      $addr = public_path('uploads') . $addr;
      return response()->download($addr); //使用原名，负责解压后会多一层
      // return response()->download($addr, date('YmdHis') . '默认-UI资源.zip');
    }
  }


  /**
   * 解压文件到指定目录
   *
   * @param  string  zip压缩文件的路径
   * @param  string  解压文件的目的路径
   * @param  boolean 是否以压缩文件的名字创建目标文件夹
   * @param  boolean 是否重写已经存在的文件
   *
   * @return boolean 返回成功 或失败
   */
  public function unzip($src_file, $dest_dir = false, $create_zip_name_dir = true, $overwrite = true)
  {
    if ($zip = zip_open($src_file)) {
      if ($zip) {
        $splitter = ($create_zip_name_dir === true) ? "." : "/";
        if ($dest_dir === false) {
          $dest_dir = substr($src_file, 0, strrpos($src_file, $splitter)) . "/";
        }
        // 如果不存在 创建目标解压目录
        $this->create_dirs($dest_dir);
        // 对每个文件进行解压
        while ($zip_entry = zip_read($zip)) {
          // 文件不在根目录
          $pos_last_slash = strrpos(zip_entry_name($zip_entry), "/");
          if ($pos_last_slash !== false) {
            // 创建目录 在末尾带 /
            $this->create_dirs($dest_dir . substr(zip_entry_name($zip_entry), 0, $pos_last_slash + 1));
          }
          // 打开包
          if (zip_entry_open($zip, $zip_entry, "r")) {
            // 文件名保存在磁盘上
            $file_name = $dest_dir . zip_entry_name($zip_entry);
            // 检查文件是否需要重写
            if ($overwrite === true || $overwrite === false && !is_file($file_name)) {
              // 读取压缩文件的内容
              $fstream = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
              @file_put_contents($file_name, $fstream);
              // 设置权限
              chmod($file_name, 0777);
              //  echo "save: ".$file_name."<br />";
            }
            // 关闭入口
            zip_entry_close($zip_entry);
          }
        }
        // 关闭压缩包
        zip_close($zip);
      }
    } else {
      return false;
    }
    return $file_name;
  }


  /**
   * 创建目录
   */
  public function create_dirs($directory_path)
  {
    if (!is_dir($directory_path)) {
      mkdir($directory_path, 0777, true);
    }
  }
}
