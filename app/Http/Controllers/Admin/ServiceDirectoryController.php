<?php

namespace App\Http\Controllers\Admin;


use App\Models\ServiceDirectory;
use App\Validate\ServiceDirectoryValidate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * 服务指南
 */
class ServiceDirectoryController extends CommonController
{

    public $model = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new ServiceDirectory();
        $this->validate = new ServiceDirectoryValidate();
    }


    /**
     * 列表
     * @param page int 当前页
     * @param limit int 分页大小
     */
    public function lists()
    {
        $page = request()->page ? intval(request()->page) : 1;
        $limit = request()->limit ? intval(request()->limit) : 10;

        $res = $this->model->lists($limit, 2);

        if (empty($res['data'])) {
            $data = $this->model->serviceTypeList(); //新增一次数据
            foreach ($data as $key => $val) {
                $data[$key]['create_time'] = date('Y-m-d H:i:s');
            }
            $this->model->insert($data);
        }

        $res = $this->model->lists($limit, 2);
        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }

        foreach ($res['data'] as $key => $val) {
            $res['data'][$key][$this->list_index_key] = $this->addSerialNumberOne($key, $page, $limit);
            $res['data'][$key]['content'] = str_replace('&nbsp;', '', strip_tags($val['content']));
        }

        $res = $this->disPageData($res);
        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 详情
     * @param id int 文章id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->select('id', 'title', 'type', 'content', 'create_time')->find(request()->id);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }

        return $this->returnApi(200, "查询成功", true, $res->toArray());
    }

    /**
     * 新增 (弃用)
     * @param title string 标题 必填
     * @param img string 封面 必填
     * @param content string 内容 必填
     */
    public function add()
    {
        return $this->returnApi(201, "暂停使用此功能");

        //增加验证场景进行验证
        if (!$this->validate->scene('add')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        if (empty(trim($this->request->content))) {
            return $this->returnApi(201, "请输入内容");
        }
        if ($this->request->type == 2 && !verify_tel_and_phone($this->request->content)) {
            return $this->returnApi(202, "电话号码格式不正确");
        }

        if ($this->request->type == 6 || $this->request->type == 9) {
            $content = trim($this->request->content);
            if ($this->request->type == 9 && !verify_tel_and_phone($content)) {
                return $this->returnApi(202, "电话号码格式不正确");
            }

            if ($this->request->type == 6 && $this->request->content != $content) {
                //获取经纬度
                $map = $this->getLonLatByAddress($content);
                if (is_string($map)) {
                    return $this->returnApi(202, $map);
                }
                $this->request->merge(['lon' => $map['lon'], 'lat' => $map['lat']]);
            }
            $this->request->merge(['content' => $content]); //重新赋值content
        }

        $is_exists = $this->model->nameIsExists($this->request->title, 'title');

        if ($is_exists) {
            return $this->returnApi(202, "该标题已存在");
        }

        DB::beginTransaction();
        try {
            $data = $this->request->all();
            // if (empty($data['img'])) {
            //     unset($data['img']);
            // }

            $this->model->add($data);

            DB::commit();
            return $this->returnApi(200, "新增成功", true);
        } catch (\Exception $e) {

            DB::rollBack();
            return $this->returnApi(202, "新增失败");
        }
    }

    /**
     * 编辑
     * @param id int 书籍id 必传
     * @param title string 标题 必填
    // * @param type string 1.本馆概况 2.开馆时间 3.入馆须知 4.读者须知 5.办证指南 6.馆内导航 7.分馆服务 8.交通指南 9.联系图书馆
    // * @param img string 封面 必填
     * @param content string 文章内容 必填
     */
    public function change()
    {

        //增加验证场景进行验证
        if (!$this->validate->scene('change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        if (empty(trim($this->request->content))) {
            return $this->returnApi(201, "请输入内容");
        }

        $res = $this->model->select('id', 'title', 'type', 'content', 'create_time')->find(request()->id);
        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }

        if ($res->type == 6 || $res->type == 9) {
            $content = trim($this->request->content);
            if ($res->type == 9 && !verify_tel_and_phone($content)) {
                return $this->returnApi(202, "电话号码格式不正确");
            }

            if ($res->type == 6 && $res->content != $content) {
                //获取经纬度
                $map = $this->getLonLatByAddress($content);
                if (is_string($map)) {
                    return $this->returnApi(202, $map);
                }
                $this->request->merge(['lon' => $map['lon'], 'lat' => $map['lat']]);
            }
            $this->request->merge(['content' => $content]); //重新赋值content
        }

        $is_exists = $this->model->nameIsExists($this->request->title, 'title', $this->request->id);

        if ($is_exists) {
            return $this->returnApi(202, "该标题已存在");
        }

        $data = $this->request->all();
        // if (empty($data['img'])) {
        //     unset($data['img']);
        // }

        $res = $this->model->change($data);

        if (!$res) {
            return $this->returnApi(202, "修改失败");
        }
        return $this->returnApi(200, "修改成功", true);
    }

    /**
     * 删除(弃用)
     * @param id int 新闻id
     */
    public function del()
    {
        return $this->returnApi(201, "暂停使用此功能");

        //增加验证场景进行验证
        if (!$this->validate->scene('del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->del($this->request->id);

        if ($res === true) {
            return $this->returnApi(200, "删除成功", true);
        }
        return $this->returnApi(202, $res);
    }
}
