<?php

namespace App\Http\Controllers\Admin;


use App\Models\CompetiteActivity;
use App\Models\CompetiteActivityWorks;
use App\Models\CompetiteActivityWorksAppointScore;
use App\Models\Manage;
use App\Validate\CompetiteActivityWorksAppointScoreValidate;
use Illuminate\Support\Facades\DB;

/**
 * 线上大赛作品指定打分人
 */
class CompetiteActivityWorksAppointScoreController extends CommonController
{

    public $model = null;
    public $worksModel = null;
    public $competiteActivityModel = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new CompetiteActivityWorksAppointScore();
        $this->worksModel = new CompetiteActivityWorks();
        $this->competiteActivityModel = new CompetiteActivity();
        $this->validate = new CompetiteActivityWorksAppointScoreValidate();
    }

    /**
     * 根据打分人获取所有的作品id，或名字  简单列表
     * @param con_id int 大赛id
     * @param score_manage_id 打分人
     */
    public function filterList()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('works_score_works_simple')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $appraise_way = $this->competiteActivityModel->where('is_del', 1)->value('appraise_way');
        if ($appraise_way == 2) {
            return $this->returnApi(202, "此作品无需专家打分");
        }

        $con_id = $this->request->con_id;
        $score_manage_id = $this->request->score_manage_id;
        $res =  $this->model->getScoreWorksSimple($con_id, $score_manage_id);
        if (empty($res)) {
            return $this->returnApi(203, "暂无数据");
        }

        return $this->returnApi(200, "查询成功", true, $res);
    }
    /**
     * 列表
     * @param page int 当前页
     * @param limit int 分页大小
     * @param con_id int 大赛id
     */
    public function lists()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('works_appoint_score')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $appraise_way = $this->competiteActivityModel->where('is_del', 1)->value('appraise_way');
        if ($appraise_way == 2) {
            return $this->returnApi(202, "此作品无需专家打分");
        }
        $con_id = $this->request->con_id;
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;

        $res = $this->model->lists($con_id, $limit);
        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }
        $res['data'] = $this->addSerialNumber($res['data'], $page, $limit);
        $res = $this->disPageData($res);
        return $this->returnApi(200, "查询成功", true, $res);
    }



    /**
     * 根据打分人获取所有的作品id，或名字 分页列表
     * @param con_id int 大赛id
     * @param score_manage_id 打分人
     * @param page int 当前页
     * @param limit int 分页大小
     */
    public function getScoreWorks()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('works_score_works')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $appraise_way = $this->competiteActivityModel->where('is_del', 1)->value('appraise_way');
        if ($appraise_way == 2) {
            return $this->returnApi(202, "此作品无需专家打分");
        }
        $con_id = $this->request->con_id;
        $score_manage_id = $this->request->score_manage_id;
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;

        $res =  $this->model->getScoreWorks($con_id, $score_manage_id, $limit);
        if (empty($res)) {
            return $this->returnApi(203, "暂无数据");
        }
        $res['data'] = $this->addSerialNumber($res['data'], $page, $limit);

        $res['manage_name'] = Manage::getManageNameByManageId($score_manage_id);
        $res = $this->disPageData($res);
        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 添加、修改打分作品
     * @param con_id int 大赛id
     * @param score_manage_id 打分人
     * @param works_ids int 选择的作品  只传最后的作品 多个逗号拼接
     */
    public function modify()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('works_score_works_modify')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $appraise_way = $this->competiteActivityModel->where('is_del', 1)->value('appraise_way');
        if ($appraise_way == 2) {
            return $this->returnApi(202, "此作品无需专家打分");
        }

        $con_id = $this->request->con_id;
        $score_manage_id = $this->request->score_manage_id;
        $works_ids = $this->request->works_ids;
        DB::beginTransaction();
        try {
            $this->model->modify($con_id, $score_manage_id, $works_ids);

            DB::commit();
            return $this->returnApi(200, "操作成功", true);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, $e->getMessage());
        }
    }



    /**
     * 删除打分作品
     * @param con_id int 大赛id
     * @param score_manage_id 打分人
     * @param works_ids int 选择的作品  需要删除的作品，没有表示删除所有 多个逗号拼接
     */
    public function del()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('works_score_works_del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $appraise_way = $this->competiteActivityModel->where('is_del', 1)->value('appraise_way');
        if ($appraise_way == 2) {
            return $this->returnApi(202, "此作品无需专家打分");
        }

        $con_id = $this->request->con_id;
        $score_manage_id = $this->request->score_manage_id;
        $works_ids = $this->request->works_ids;
        DB::beginTransaction();
        try {
            $this->model->delScoreManage($con_id, $score_manage_id, $works_ids);
            DB::commit();
            return $this->returnApi(200, "操作成功", true);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, "操作失败");
        }
    }
}
