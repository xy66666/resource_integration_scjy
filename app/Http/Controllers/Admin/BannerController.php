<?php

namespace App\Http\Controllers\Admin;

use App\Models\Banner;
use App\Validate\BannerValidate;
use Illuminate\Support\Facades\DB;

/**
 * banner 设置
 * Class Banner
 * @package app\api\controller
 */
class BannerController extends CommonController
{
    public $model;
    public $validate = null;


    public function __construct()
    {
        parent::__construct();

        $this->model = New Banner();
        $this->validate = New BannerValidate();
    }


    /**
     * 列表
     * @param page int 页码
     * @param limit int 分页大小
     * @param type 显示位置  0或空 全部 1  首页  2 商城
     * @param is_play    是否发布   1 发布  2 撤销
     * @param keywords string 搜索关键词
     */
    public function lists(){
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? $this->request->limit : 10;
        $keywords = $this->request->keywords;
        $type = $this->request->type;
        $is_play = $this->request->is_play;

        $condition[] = ['is_del' , '=' , 1];
        if($keywords){
            $condition[] = ['name','like',"%$keywords%"];
        }
        if($type){
            $condition[] = ['type' ,'=', $type];
        }
        if($is_play){
            $condition[] = ['is_play' ,'=', $is_play];
        }

        $res = $this->model
            ->where($condition)
            ->orderByDesc('sort')
            ->paginate($limit)
            ->toArray();

        if(!$res['data']){
            return $this->returnApi(203, "暂无数据");
        }
        $res = $this->disPageData($res);
        $res['data'] = $this->addSerialNumber($res['data'] , $page , $limit);

        return $this->returnApi(200, "查询成功", true, $res);
    }


    /** 
     * 详情
     * @param id int
     */
    public function detail(){
          //增加验证场景进行验证
          if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->where('id',$this->request->id)->where('is_del', 1)->first();

        if(!$res){
            return $this->returnApi(201, "参数传递错误");
        }

        return $this->returnApi(200, "查询成功", true, $res->toArray());
    }

    /**
     * 新增
     * @param name string 名称
     * @param img string 图片
     * @param link string 链接
     * @param type init 显示位置   1 首页     2 商城
     * @param is_play 是否发布   1 发布  2 撤销
     */
    public function add()
    {
          //增加验证场景进行验证
          if (!$this->validate->scene('add')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $is_exists = $this->model->nameIsExists($this->request->name , 'name');

        if ($is_exists) {
            return $this->returnApi(202, "此名称已存在");
        }
        $this->request->merge(['sort' => $this->model->max('sort') + 1]);
        $res = $this->model->add($this->request->all());

        if (!$res) {
            return $this->returnApi(202, "新增失败");
        }
        return $this->returnApi(200, "新增成功", true);
    }


    /**
     * 修改
     * @param id int banner id
     * @param name string 名称
     * @param img string 图片
     * @param link string 链接
     * @param type init 显示位置   1 首页     2 商城
     * @param is_play 是否发布   1 发布  2 撤销
     */
    public function change()
    {
         //增加验证场景进行验证
         if (!$this->validate->scene('change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $is_exists = $this->model->nameIsExists($this->request->name , 'name' , $this->request->id);

        if ($is_exists) {
            return $this->returnApi(202, "此名称已存在");
        }

        $res = $this->model->change($this->request->all());

        if (!$res) {
            return $this->returnApi(202, "修改失败");
        }
        return $this->returnApi(200, "修改成功", true);
    }

    /**
     * 删除
     * @param id int
     */
    public function del()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->del($this->request->id);

        if ($res === true) {
            $this->deleteFile($this->model->img);//删除图片
            return $this->returnApi(200, "删除成功", true);
        }
        return $this->returnApi(202, $res);
    }
    /**
     * 切换显示方式
     * @param id int
     * @param is_play 是否发布   1 发布  2 撤销
     */
    public function cancelAndRelease(){

        //增加验证场景进行验证
        if (!$this->validate->scene('cancel_and_release')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $id = $this->request->id;
        $is_play = $this->request->is_play;

        $banner_info = $this->model->where('id',$id)->first();

        if(!$banner_info){
            return $this->returnApi(201, "参数传递错误");
        }

        $banner_info->is_play = $is_play;
        $res = $banner_info->save();
        if(!$res){
            return $this->returnApi(202, "修改失败");
        }

        $is_play = $this->request->is_play == 1 ? '发布' : '撤销';

        return $this->returnApi(200, $is_play."成功", true);
    }

    /**
     * banner 图排序
     * content json格式数据  [{"id":1,"sort":2},{"id":2,"sort":2},{"id":3,"sort":3},{"id":4,"sort":4},{"id":5,"sort":5}]   第一个 sort 最大
     */
    public function sortChange(){
        // $data = [
        //   ['id'=>1 , 'sort'=>2],
        //   ['id'=>2 , 'sort'=>2],
        //   ['id'=>3 , 'sort'=>3],
        //   ['id'=>4 , 'sort'=>4],
        //   ['id'=>5 , 'sort'=>5],
        // ];
        // dump(json_encode($data));die;
        //增加验证场景进行验证
        if (!$this->validate->scene('sort_change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $content = $this->request->input('content' , '');
        if(empty($content)){
            return $this->returnApi(201, "参数传递错误");
        }
        $content = json_decode($content , true);

        DB::beginTransaction();
        try {
            foreach ($content as $key=>$val){
                $this->model->where('id',$val['id'])->update(['sort'=>$val['sort']]);
            }
            DB::commit();
            return $this->returnApi(200, "修改成功" , true);
        }catch (\Exception $e){
            DB::rollBack();
            return $this->returnApi(202,$e->getMessage());
        }
    }

}