<?php

namespace App\Http\Controllers\Admin;

use App\Models\CompetiteActivityGroup;
use App\Validate\CompetiteActivityGroupValidate;
use Illuminate\Support\Facades\DB;

/**
 * 大赛单位管理
 */
class CompetiteActivityGroupController extends CommonController
{

    public $model = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new CompetiteActivityGroup();
        $this->validate = new CompetiteActivityGroupValidate();
    }

    /**
     * 列表(用于下拉框选择)
     * @param con_id 大赛id
     */
    public function filterList()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('filter_list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        //判断是否是单位联盟大赛
        $isAllowAddGroup = $this->model->isAllowAddGroup($this->request->con_id);
        if ($isAllowAddGroup !== true) {
            return $this->returnApi(202, $isAllowAddGroup);
        }

        $condition[] = ['is_del', '=', 1];
        $condition[] = ['con_id', '=', $this->request->con_id];

        return $this->model->getFilterList(['id', 'con_id', 'name', 'account'], $condition);
    }

    /**
     * 列表
     * @param con_id int 大赛id
     * @param page int 当前页
     * @param limit int 分页大小
     * @param start_time datetime 创建时间范围搜索(开始) 
     * @param end_time datetime 创建时间范围搜索(结束) 
     * @param keywords string 搜索关键词(类型名称)
     */
    public function lists()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        $keywords = $this->request->keywords;
        $con_id = $this->request->con_id;

        //判断是否是单位联盟大赛
        $isAllowAddGroup = $this->model->isAllowAddGroup($this->request->con_id);
        if ($isAllowAddGroup !== true) {
            return $this->returnApi(202, $isAllowAddGroup);
        }

        $res = $this->model->lists(null, $con_id, $keywords, $start_time, $end_time, $page, $limit);

        if ($res['total'] == 0 || !$res['data']) {
            return $this->returnApi(203, "暂无数据");
        }

        foreach ($res['data'] as $key => $val) {
            $res['data'][$key]['intro'] = strip_tags($val['intro']);
            $res['data'][$key][$this->list_index_key] = $this->addSerialNumberOne($key, $page, $limit);
        }

        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 详情
     * @param id int 类型id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->detail($this->request->id);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }

        return $this->returnApi(200, "获取成功", true, $res->toArray());
    }

    /**
     * 新增
     * @param con_id int大赛id
     * @param account varchar 团队账号  2~20 个字符，不允许填特殊符号
     * @param password varchar 密码     6~20个支付 ，不允许填特殊符号
     * @param confirm_password varchar 确认密码
     * @param name string 名称   最多 3个字符  必填
     * @param img string 图片
     * @param intro string 简介
     * @param tel string 联系电话
     * @param contacts string 联系人
     */
    public function add()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('add')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        //验证密码
        $validate_password = validate_password($this->request->password);
        if ($validate_password !== true) {
            return $this->returnApi(201,  $validate_password);
        }
        //判断是否是单位联盟大赛
        $isAllowAddGroup = $this->model->isAllowAddGroup($this->request->con_id);
        if ($isAllowAddGroup !== true) {
            return $this->returnApi(202, $isAllowAddGroup);
        }

        $condition[] = ['con_id', '=', $this->request->con_id];
        $is_exists = $this->model->nameIsExists($this->request->name, 'account', null, 1, $condition);
        if ($is_exists) {
            return $this->returnApi(202, "此团队账号已存在");
        }
        $data = $this->request->all();
        unset($data['confirm_password']);
        $data['password'] = md5($data['password']);
        if(empty($data['img'])){
            $data['img'] = 'default/default_head_img.png';
        }
        $res = $this->model->add($data);

        if (!$res) {
            return $this->returnApi(202, "新增失败");
        }
        return $this->returnApi(200, "新增成功", true);
    }

    /**
     * 修改
     * @param id int 单位id
     * @param con_id int大赛id
     * @param name string 名称   最多 3个字符  必填
     * @param img string 图片
     * @param intro string 简介
     * @param tel string 联系电话
     * @param contacts string 联系人
     */
    public function change()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $result = $this->model->where('id', $this->request->id)->first();
        if (empty($result)) {
            return $this->returnApi(201, "参数传递错误");
        }
        //判断是否是单位联盟大赛
        $isAllowAddGroup = $this->model->isAllowAddGroup($result->con_id);
        if ($isAllowAddGroup !== true) {
            return $this->returnApi(202, $isAllowAddGroup);
        }

        // $condition[] = ['con_id', '=', $result->con_id];
        // $is_exists = $this->model->nameIsExists($this->request->name, 'name', $this->request->id, 1, $condition);
        // if ($is_exists) {
        //     return $this->returnApi(202, "此名称已存在");
        // }

        $data = $this->request->all();
        unset($data['account']);
        unset($data['password']);
        unset($data['confirm_password']);
        if(empty($data['img'])){
            $data['img'] = 'default/default_head_img.png';
        }
        $res = $this->model->change($data);

        if (!$res) {
            return $this->returnApi(202, "修改失败");
        }
        return $this->returnApi(200, "修改成功", true);
    }

    /**
     * 删除
     * @param id int 类型id
     */
    public function del()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->del($this->request->id);

        if ($res === true) {
            return $this->returnApi(200, "删除成功", true);
        }
        return $this->returnApi(202, $res);
    }

    /**
     * 修改团队账号密码
     * @param id int 团队账号id
     * @param password varchar 密码     6~20个支付 ，不允许填特殊符号
     * @param confirm_password varchar 确认密码
     */
    public function changeGroupPassword()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('change_group_password')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        //验证密码
        $validate_password = validate_password($this->request->password);
        if ($validate_password !== true) {
            return $this->returnApi(201,  $validate_password);
        }
        $res = $this->model->where('id', $this->request->id)->where('is_del', 1)->first();
        if (empty($res)) {
            return $this->returnApi(201, "团队账号不存在");
        }
        $res->password = md5($this->request->password);
        $result = $res->save();

        if ($result) {
            return $this->returnApi(200, "修改成功", true);
        }
        return $this->returnApi(202, $res);
    }



    /**
     * 修改单位排序
     * @param orders 排序   json格式数据  里面2个参数  id 单位id   order 序号
     * 
     * json 格式数据  [{"id":1,"order":1},{"id":2,"order":2},{"id":3,"order":3}]
     * 数组 格式数据  [['id'=>1 , 'order'=>1] , ['id'=>2 , 'order'=>2] , ['id'=>3 , 'order'=>3]]
     */
    public function orderChange()
    {
        if (!$this->validate->scene('order')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $orders = $this->request->orders;
        DB::beginTransaction();
        try {
            $this->model->orderChange($orders);
            DB::commit();
            return $this->returnApi(200, '修改成功', true);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, '修改失败');
        }
    }
}
