<?php

namespace App\Http\Controllers\Admin;

use App\Models\AccessNum;
use App\Models\ActivityApply;
use App\Models\CompetiteActivityWorks;
use App\Models\Feedback;
use App\Models\Volunteer;
use App\Models\VolunteerApply;
use Illuminate\Support\Facades\DB;

/**
 * 后台首页
 */
class IndexController extends CommonController
{
    /**
     * 首页数据统计
     */
    public function index()
    {
        $manage_id = request()->manage_id;
        //未审核活动个数
        $activityApplyObj = new ActivityApply();
        $data['activity'] = $activityApplyObj->getUncheckedActivityNumber();
        //预约
        $data['reservation'] = 0;
        //志愿者
        $volunteerApplyObj = new VolunteerApply();
        $data['volunteer'] = $volunteerApplyObj->getVolunteerNumber(null, [4]); //未审核

        //读者留言
        $feedbackObj = new Feedback();
        $data['feedback'] = $feedbackObj->getUncheckeFeedbackNumber();

        //待自己审核的大赛活动
        $competiteActivityWorksModel = new CompetiteActivityWorks();
        $data['competite_activity_check'] = $competiteActivityWorksModel->getNotCheckWorksList($manage_id);
        $data['competite_activity_review_score'] = $competiteActivityWorksModel->getNotReviewScoreWorksList($manage_id);


        return $this->returnApi(200, '获取成功', true, $data);
    }

    /**
     * 公众号访问统计
     * @param year 年    例：2021
     * @param month 月   0~12 的数字   0表示只按年查 例：1、12
     */
    public function accessStatistics()
    {
        $year = $this->request->year ?: date('Y');
        $month = $this->request->month;
        if (empty($month)) {
            $data['web_num'] = $this->accessStatisticsMonth($year, 'web_num');
            $data['wx_num'] = $this->accessStatisticsMonth($year, 'wx_num');
        } else {
            $month = $month > 9 ? $month : '0' . $month;
            $data['web_num'] = $this->accessStatisticsDay($year . '-' . $month, 'web_num');
            $data['wx_num'] = $this->accessStatisticsDay($year . '-' . $month, 'wx_num');
        }

        return $this->returnApi(200, '获取成功', true, $data);
    }


    /**
     * 公众号访问统计量（按天统计）
     * @param month 月
     * @param type  web_num web数量   wx_num 微信数量
     */
    // public function accessStatisticsDay($month , $type)
    // {       
    //     $browseModel = new AccessNum();

    //     $res = $browseModel->select('id',$type.' as count',DB::raw("CONCAT_WS('-',yyy,mmm,ddd) as day"))
    //         ->where('yyy', date('Y', strtotime($month)))
    //         ->where('mmm', date('m', strtotime($month)))
    //         ->groupBy('day')
    //         ->orderByDesc('day')
    //         ->get()
    //         ->toArray();

    //     if (!$res) {
    //       //  return $this->returnApi(203, "暂无数据");
    //     }
    //     foreach ($res as $key => $val) {
    //         list($yyy, $mmm, $ddd) = explode('-', $val['day']);
    //         if ($mmm < 10) $mmm = '0' . (int)$mmm;
    //         if ($ddd < 10) $ddd = '0' . (int)$ddd;
    //         $res[$key]['day'] = $yyy . '-' . $mmm . '-' . $ddd;
    //     }
    //     if ($month == date('Y-m')) {
    //         $days =  date('d');
    //     } else {
    //         $days = get_day_by_year_month($month); //获取本月天数
    //     }
    //     return self::disCartogramData($res, date('Y-m-01 00:00:00', strtotime($month)), date($month . '-' . $days . ' 23:59:59'));
    // }

    /**
     * 公众号访问统计量（按月统计）
     * @param year 年
     * @param type  web_num web数量   wx_num 微信数量
     */
    // public function accessStatisticsMonth($year , $type)
    // {
    //     $browseModel = new AccessNum();

    //     $res = $browseModel->select("id",DB::raw("sum($type) as count"),DB::raw("CONCAT_WS('-',yyy,mmm) as month"))
    //         ->where('yyy', date('Y', strtotime($year)))
    //         ->groupBy('month')
    //         ->orderByDesc('month')
    //         ->get()
    //         ->toArray();

    //     if (!$res) {
    //        // return $this->returnApi(203, "暂无数据");
    //     }
    //     foreach ($res as $key => $val) {
    //         list($yyy, $mmm) = explode('-', $val['month']);
    //         if ($mmm < 10) $mmm = '0' . (int)$mmm;
    //         $res[$key]['month'] = $yyy . '-' . $mmm;
    //     }
    //     return self::disCartogramMonthData($res, $year);
    // }



}
