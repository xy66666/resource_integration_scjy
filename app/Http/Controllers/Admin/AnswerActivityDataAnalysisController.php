<?php

namespace App\Http\Controllers\Admin;

use App\Models\AnswerActivity;
use App\Models\AnswerActivityBrowseCount;
use App\Models\AnswerActivityGift;
use App\Models\AnswerActivityUnit;
use App\Models\AnswerActivityUserGift;
use App\Models\AnswerActivityUserUnit;
use App\Validate\AnswerActivityDataAnalysisValidate;
use Illuminate\Support\Facades\Log;

/**
 * 线上答题活动 数据分析
 * Class AnswerType
 * @package app\admin\controller
 */
class AnswerActivityDataAnalysisController extends CommonController
{
    public $model = null;
    public $validate = null;
    public $worksModel = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new AnswerActivity();
        $this->validate = new AnswerActivityDataAnalysisValidate();
    }


    /**
     * 数据统计
     * @param act_id int 活动id  
     * @param area_id int 区域id  空表示全部 
     * @param unit_id int 单位id  空表示全部 
     * @param start_time datetime 比赛开始时间(开始)
     * @param end_time datetime 比赛开始时间(截止)
     */
    public function dataStatistics()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('statistics')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $act_id = request()->act_id;
        $area_id = request()->area_id;
        $unit_id = request()->unit_id;
        $start_time = request()->start_time;
        $end_time = request()->end_time;

        if (empty($start_time) || empty($end_time)) {
            $start_time = date('Y-m-d 00:00:00');
            $end_time = date('Y-m-d H:i:s');
        }

        $act_info = $this->model->detail($act_id, ['id', 'pattern', 'prize_form', 'node'], null);

        if (empty($act_info)) {
            return $this->returnApi(201, '参数错误');
        }
        if ($act_info['node'] == 1 && (!empty($area_id) || !empty($unit_id))) {
            return $this->returnApi(201, '参数错误');
        }
        if ($act_info['node'] == 2 && !empty($area_id)) {
            return $this->returnApi(201, '参数错误');
        }

        $answerActivityUnitModel = new AnswerActivityUnit();
      
        if ($area_id) {
            if (empty($unit_id)) {
                $unit_id = $answerActivityUnitModel->getUnitIdByAreaId($act_id, $area_id);
            } else {
                $unit_id = explode(',', $unit_id);
            }
        } else {
            $unit_id = $unit_id ? explode(',', $unit_id) : null;
        }

        $model = $this->model->getAnswerRecordModel($act_info['pattern']);

        //获取活动总参与人次
        $part_number = $model->partNumberStatistics($act_id, $unit_id, $start_time, $end_time, 1);
        //总答题次数
        $answer_number = $model->partNumberStatistics($act_id, $unit_id, $start_time, $end_time, 2);

        //总点击量
        $answerActivityBrowseCountModel = new AnswerActivityBrowseCount();
        $browse_number = $answerActivityBrowseCountModel->getBrowseNumber($act_id, $unit_id, $start_time, $end_time);

        $data = [
            'part_number' => $part_number,
            'answer_number' => $answer_number,
            'browse_number' => $browse_number,
        ];

        //礼品发送数量
        if ($act_info['prize_form'] == 2) {
            $answerActivityUserGiftModel = new AnswerActivityUserGift();
            $answer_activity_gift_type = config('other.answer_activity_gift_type');
        
            if (in_array(2, $answer_activity_gift_type)) {
                $red_number = $answerActivityUserGiftModel->getWinNumber($act_id, null, 1, null, $start_time, $end_time, $unit_id); //红包发放数量
            } else {
                $red_number = 0;
            }

            if (in_array(1, $answer_activity_gift_type)) {
                $gift_number = $answerActivityUserGiftModel->getWinNumber($act_id, null, 2, null, $start_time, $end_time, $unit_id); //礼物发放数量
            } else {
                $gift_number = 0;
            }

            $data['gift_number'] = $gift_number;
            $data['red_number'] = $red_number;
        }

        return $this->returnApi(200, "获取成功", true, $data);
    }

    /**
     * 答题次数统计
     * @param act_id int 活动id  
     * @param area_id int 区域id  空表示全部 
     * @param unit_id int 单位id  空表示全部 
     * @param start_time datetime 比赛开始时间(开始)
     * @param end_time datetime 比赛开始时间(截止)
     */
    public function answerStatistics()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('statistics')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $act_id = request()->act_id;
        $area_id = request()->area_id;
        $unit_id = request()->unit_id;
        $start_time = request()->start_time;
        $end_time = request()->end_time;

        if (empty($start_time) || empty($end_time)) {
            $start_time = date('Y-m-d 00:00:00');
            $end_time = date('Y-m-d H:i:s');
        }

        $act_info = $this->model->detail($act_id, ['id', 'pattern', 'node'], null);

        if (empty($act_info)) {
            return $this->returnApi(201, '参数错误');
        }
        if ($act_info['node'] == 1 && (!empty($area_id) || !empty($unit_id))) {
            return $this->returnApi(201, '参数错误');
        }
        if ($act_info['node'] == 2 && !empty($area_id)) {
            return $this->returnApi(201, '参数错误');
        }

        if ($area_id) {
            if (empty($unit_id)) {
                $answerActivityUnitModel = new AnswerActivityUnit();
                $unit_id = $answerActivityUnitModel->getUnitIdByAreaId($act_id, $area_id);
            } else {
                $unit_id = explode(',', $unit_id);
            }
        } else {
            $unit_id = $unit_id ? explode(',', $unit_id) : null;
        }

        $model = $this->model->getAnswerRecordModel($act_info['pattern']);

        $res = $model->partStatistics($act_id, $unit_id, $start_time, $end_time);
        $res = self::disCartogramDayOrHourData($res, null, $start_time, $end_time, 'dates');

        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 礼物发放统计
     * @param act_id int 活动id  
     * @param area_id int 区域id  空表示全部 
     * @param unit_id int 单位id  空表示全部 
     * @param start_time datetime 比赛开始时间(开始)
     * @param end_time datetime 比赛开始时间(截止)
     */
    public function giftGrantStatistics()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('statistics')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $act_id = request()->act_id;
        $area_id = request()->area_id;
        $unit_id = request()->unit_id;
        $start_time = request()->start_time;
        $end_time = request()->end_time;

        if (empty($start_time) || empty($end_time)) {
            $start_time = date('Y-m-d 00:00:00');
            $end_time = date('Y-m-d H:i:s');
        }

        $act_info = $this->model->detail($act_id, ['id', 'pattern', 'node'], null);

        if (empty($act_info)) {
            return $this->returnApi(201, '参数错误');
        }
        if ($act_info['node'] == 1 && (!empty($area_id) || !empty($unit_id))) {
            return $this->returnApi(201, '参数错误');
        }
        if ($act_info['node'] == 2 && !empty($area_id)) {
            return $this->returnApi(201, '参数错误');
        }

        if ($act_info['prize_form'] == 1) {
            return $this->returnApi(201, '此活动为排名活动');
        }

        if ($area_id) {
            if (empty($unit_id)) {
                $answerActivityUnitModel = new AnswerActivityUnit();
                $unit_id = $answerActivityUnitModel->getUnitIdByAreaId($act_id, $area_id);
            } else {
                $unit_id = explode(',', $unit_id);
            }
        } else {
            $unit_id = $unit_id ? explode(',', $unit_id) : null;
        }

        $answerActivityUserGiftModel = new AnswerActivityUserGift();
        $red_number = $answerActivityUserGiftModel->giftStatistics($act_id, $unit_id, 1, $start_time, $end_time); //红包统计
        $gift_number = $answerActivityUserGiftModel->giftStatistics($act_id, $unit_id, 2, $start_time, $end_time); //礼物统计

        $gift_number = self::disCartogramDayOrHourData($gift_number, null, $start_time, $end_time, 'times');
        $red_number = self::disCartogramDayOrHourData($red_number, null, $start_time, $end_time, 'times');

        return $this->returnApi(200, "查询成功", true, [
            'gift_number' => $gift_number,
            'red_number' => $red_number,
        ]);
    }

    /**
     * 单位数据统计列表
     * @param page int 当前页
     * @param limit int 分页大小
     * @param act_id int 活动id  
     * @param area_id int 区域id  空表示全部 
     * @param unit_id int 单位id  空表示全部 
     * @param start_time datetime 比赛开始时间(开始)
     * @param end_time datetime 比赛开始时间(截止)
     */
    public function unitStatisticsList()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('statistics')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $page = request()->page ? intval(request()->page) : 1;
        $limit = request()->limit ? intval(request()->limit) : 10;
        $act_id = request()->act_id;
        $area_id = request()->area_id;
        $unit_id = request()->unit_id;
        $start_time = request()->start_time;
        $end_time = request()->end_time;

        if (empty($start_time) || empty($end_time)) {
            $start_time = date('Y-m-d 00:00:00');
            $end_time = date('Y-m-d H:i:s');
        }


        $act_info = $this->model->detail($act_id, ['id', 'pattern', 'node', 'is_need_unit'], null);

        if (empty($act_info)) {
            return $this->returnApi(201, '参数错误');
        }
        if ($act_info['node'] == 1 && (!empty($area_id) || !empty($unit_id))) {
            return $this->returnApi(201, '参数错误');
        }
        if ($act_info['node'] == 2 && !empty($area_id)) {
            return $this->returnApi(201, '参数错误');
        }

        if ($area_id) {
            if (empty($unit_id)) {
                $answerActivityUnitModel = new AnswerActivityUnit();
                $unit_id = $answerActivityUnitModel->getUnitIdByAreaId($act_id, $area_id);
            } else {
                $unit_id = explode(',', $unit_id);
            }
        } else {
            $unit_id = $unit_id ? explode(',', $unit_id) : null;
        }

        $answerActivityUnitModel = new AnswerActivityUnit();
        $res = $answerActivityUnitModel->lists(['id', 'name'], $act_id, null, $area_id, null, $unit_id, null, null, null, null, $limit);
        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }

        $model = $this->model->getAnswerRecordModel($act_info['pattern']);
        $answerActivityGiftModel = new AnswerActivityGift();
        foreach ($res['data'] as $key => $val) {
            //所属人数
            $res['data'][$key]['belong_number'] = AnswerActivityUserUnit::where('act_id', $act_id)->where('unit_id', $val['id'])->count();//即使后台不需要，也要统计
          /*   if ($act_info['is_need_unit'] == 1) {
                $res['data'][$key]['belong_number'] = AnswerActivityUserUnit::where('act_id', $act_id)->where('unit_id', $val['id'])->count();
            } else {
                $res['data'][$key]['belong_number'] = -1; //表示后台不需要此参数
            } */
            // 答题次数
            $res['data'][$key]['answer_number'] = $model->partNumberStatistics($act_id, [$val['id']], $start_time, $end_time, 2);

            //投放礼物数量
            //只有抽奖活动才返回
            if ($act_info['prize_form'] == 2) {
                $gift_number = $answerActivityGiftModel->getGiftNumber($act_id, 1, $val['id']);

                if ($gift_number) {
                    $res['data'][$key]['gift_total_number'] = $gift_number[0]['total_number'];
                    $res['data'][$key]['gift_use_number'] = $gift_number[0]['use_number'];
                } else {
                    $res['data'][$key]['gift_total_number'] = 0;
                    $res['data'][$key]['gift_use_number'] = 0;
                }
                //投放红包数量
                $red_number = $answerActivityGiftModel->getGiftNumber($act_id, 2, $val['id']);

                if ($red_number) {
                    $res['data'][$key]['red_total_number'] = $red_number[0]['total_number'];
                    $res['data'][$key]['red_use_number'] = $red_number[0]['use_number'];
                } else {
                    $res['data'][$key]['red_total_number'] = 0;
                    $res['data'][$key]['red_use_number'] = 0;
                }
            }

            $res['data'][$key][$this->list_index_key] = $this->addSerialNumberOne($key, $page, $limit);
        }
        return $this->returnApi(200, "查询成功", true, $res);
    }



    /**
     * 系统投放礼物数据
     * @param act_id int 活动id  
     */
    public function systemGiftGrantStatistics()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('statistics')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $act_id = request()->act_id;
        $act_info = $this->model->detail($act_id, ['id', 'pattern', 'node'], null);

        if (empty($act_info)) {
            return $this->returnApi(201, '参数错误');
        }

        //投放礼物数量
        $answerActivityGiftModel = new AnswerActivityGift();
        $gift_number = $answerActivityGiftModel->getGiftNumber($act_id, 2, 0);
        if ($gift_number) {
            $res['gift_total_number'] = $gift_number[0]['total_number'];
            $res['gift_use_number'] = $gift_number[0]['use_number'];
        } else {
            $res['gift_total_number'] = 0;
            $res['gift_use_number'] = 0;
        }
        //投放红包数量
        $gift_number = $answerActivityGiftModel->getGiftNumber($act_id, 1, 0);
        if ($gift_number) {
            $res['red_total_number'] = $gift_number[0]['total_number'];
            $res['red_use_number'] = $gift_number[0]['use_number'];
        } else {
            $res['red_total_number'] = 0;
            $res['red_use_number'] = 0;
        }

        return $this->returnApi(200, "查询成功", true, $res);
    }
}
