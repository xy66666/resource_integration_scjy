<?php

namespace App\Http\Controllers\Admin;

use App\Models\BookHomeReturnAddress;
use App\Models\BookHomeReturnAddressExplain;
use App\Validate\BookHomeReturnAddressValidate;
use Illuminate\Support\Facades\DB;

/**
 * 邮递、现场归还地址管理
 * Class SystemSet
 * @package app\api\controller
 */
class BookHomeReturnAddressController extends CommonController
{
    private $validate = null;
    private $model = null;
    public function __construct()
    {
        parent::__construct();
        $this->model = new BookHomeReturnAddress();
        $this->validate = new BookHomeReturnAddressValidate();
    }
    /**
     * 列表
     * @param keywords  检索条件
     * @param way  方式  1 现场归还   2 邮递归还 
     * @param type   类型 1 新书（默认）   2 馆藏书
     * @param page  页数，默认为1，
     * @param limit  条数，默认显示 10条
     */
    public function lists()
    {
        $keywords = $this->request->input('keywords', '');
        $way = $this->request->input('way', '');
        $type = $this->request->input('type', '');
        $page = intval($this->request->input('page', 1)) ?: 1;
        $limit = intval($this->request->input('limit', 10)) ?: 10;
        $keywords = addslashes($keywords);

        $res = $this->model->lists($keywords, $way, $type, $limit);
        if (empty($res['data'])) {
            return $this->returnApi(203, '暂无数据'); //无权限访问
        }

        $res['data'] = $this->addSerialNumber($res['data'], $page, $limit);
        $res = $this->disPageData($res);
        return $this->returnApi(200, '获取成功', true, $res);
    }

    /**
     * 详情
     * @param id int 地址id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }


        $res = $this->model->detail($this->request->id);
        if (!$res) {
            return $this->returnApi(201, "参数传递错误", "YES");
        }

        $res = $res->toArray();

        return $this->returnApi(200, "查询成功", true, $res);
    }


    /**
     * 添加
     * @param way  方式  1 现场归还   2 邮递归还   
     * @param type  类型 1 新书（默认）   2 馆藏书
     * @param username  发货人名称
     * @param tel  发货人联系电话号码
     * @param province 省
     * @param city 市
     * @param district 区
     * @param street 街道
     * @param address 详细地址
     * @param dispark_time 开放时间
     */
    public function add()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('add')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $condition[] = ['username', '=', $this->request->username]; //增加检索条件
        $condition[] = ['type', '=', $this->request->type]; //增加检索条件
        $condition[] = ['way', '=', $this->request->way]; //增加检索条件
        $is_exists = $this->model->nameIsExists($this->request->username, 'username', null, 1, $condition);

        if ($is_exists) {
            return $this->returnApi(202, "该地址信息已存在");
        }

        $res = $this->model->add($this->request->all());

        if (!$res) {
            return $this->returnApi(202, "新增失败");
        }
        return $this->returnApi(200, "新增成功", true);
    }

    /**
     * 修改
     * @param id int 记录id 必传
     * @param way  方式  1 现场归还   2 邮递归还   
     * @param type  类型 1 新书（默认）   2 馆藏书
     * @param username  发货人名称
     * @param tel  发货人联系电话号码
     * @param province 省
     * @param city 市
     * @param district 区
     * @param street 街道
     * @param address 详细地址
     * @param dispark_time 开放时间
     */
    public function change()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $condition[] = ['username', '=', $this->request->username]; //增加检索条件
        $condition[] = ['tel', '=', $this->request->tel]; //增加检索条件
        $condition[] = ['way', '=', $this->request->way]; //增加检索条件
        $is_exists = $this->model->nameIsExists($this->request->username, 'username', $this->request->id, 1, $condition);

        if ($is_exists) {
            return $this->returnApi(202, "该地址信息已存在");
        }

        $res = $this->model->change($this->request->all());

        if (!$res) {
            return $this->returnApi(202, "修改失败");
        }
        return $this->returnApi(200, "修改成功", true);
    }

    /**
     * 删除
     * @param id int 类型id
     */
    public function del()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->detail($this->request->id);
        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }

        $res = $this->model->del($this->request->id);

        if ($res === true) {
            return $this->returnApi(200, "删除成功", true);
        }
        return $this->returnApi(202, $res);
    }

    /**
     * 获取图书到家归还地址说明
     * @param way 方式  1 现场归还   2 邮递归还
     */
    public function getAddressExplain()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('get_address_explain')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $way = $this->request->way;
        $bookHomeReturnAddressExplainModel = new BookHomeReturnAddressExplain();
        $res = $bookHomeReturnAddressExplainModel->where(function ($query) use ($way) {
            if (!empty($way)) {
                $query->where('way', $way);
            }
        })->first();
        if ($res) {
            return $this->returnApi(200, "获取成功", true, $res->toArray());
        }
        return $this->returnApi(202, '暂无数据');
    }

    /**
     * 设置图书到家归还地址说明
     * @param way 方式  1 现场归还   2 邮递归还
     * @param content 说明内容   \n 代表换行
     */
    public function setAddressExplain()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('set_address_explain')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $bookHomeReturnAddressExplainModel = new BookHomeReturnAddressExplain();
        $res = $bookHomeReturnAddressExplainModel->setAddressExplain($this->request->way, $this->request->content);
        if ($res) {
            return $this->returnApi(200, "设置成功", true, $res);
        }
        return $this->returnApi(202, '设置失败');
    }
}
