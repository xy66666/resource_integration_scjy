<?php

namespace App\Http\Controllers\Admin;

use App\Models\CodeBorrowLog;
use App\Models\CodeReturnLog;

/**
 * 码上借系统
 */
class CodeOnBorrowLogController extends CommonController
{


    /**
     * 归还操作列表
     * @param keywords  检索条件
     * @param place_code_id  场所码id
     * @param start_time 开始时间   数据格式    2020-05-12 12:00:00
     * @param end_time 结束时间     数据格式    2020-05-12 12:00:00
     * @param page  页数，默认为1，
     * @param limit  条数，默认显示 10条
     */
    public function borrowList()
    {
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        $keywords = $this->request->keywords;
        $place_code_id = $this->request->place_code_id;
        $page = intval($this->request->page, 1) ?: 1;
        $limit = intval($this->request->limit, 1) ?: 10;
        $codeBorrowLogModel = new CodeBorrowLog();
        $res = $codeBorrowLogModel->lists(null,$place_code_id, $keywords, $start_time, $end_time, $limit);

        if ($res['data']) {
            $res['data'] = $this->disDataSameLevel($res['data'], 'con_code_place', ['name' => 'code_place_name']);

            $res['data'] = $this->addSerialNumber($res['data'], $page, $limit);
            $res = $this->disPageData($res);
            return $this->returnApi(200, '获取成功', true, $res);
        }

        return $this->returnApi(203, '暂无数据');
    }

    /**
     * 归还操作列表
     * @param keywords  检索条件
     * @param place_code_id  场所码id
     * @param start_time 开始时间   数据格式    2020-05-12 12:00:00
     * @param end_time 结束时间     数据格式    2020-05-12 12:00:00
     * @param page  页数，默认为1，
     * @param limit  条数，默认显示 10条
     */
    public function returnList()
    {
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        $keywords = $this->request->keywords;
        $place_code_id = $this->request->place_code_id;
        $page = intval($this->request->page, 1) ?: 1;
        $limit = intval($this->request->limit, 1) ?: 10;

        $codeReturnLogModel = new CodeReturnLog();
        $res = $codeReturnLogModel->lists(null,$place_code_id, $keywords, $start_time, $end_time, $limit);

        if ($res['data']) {
            $res['data'] = $this->disDataSameLevel($res['data'], 'con_code_place', ['name' => 'code_place_name']);
            $res['data'] = $this->addSerialNumber($res['data'], $page, $limit);
            $res = $this->disPageData($res);
            return $this->returnApi(200, '获取成功', true, $res);
        }

        return $this->returnApi(203, '暂无数据');
    }
}
