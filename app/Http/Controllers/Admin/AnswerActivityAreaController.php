<?php

namespace App\Http\Controllers\Admin;

use App\Models\AnswerActivityArea;
use App\Validate\AnswerActivityAreaValidate;
use Illuminate\Support\Facades\DB;

/**
 * 活动区域管理
 */
class AnswerActivityAreaController extends CommonController
{

    public $model = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new AnswerActivityArea();
        $this->validate = new AnswerActivityAreaValidate();
    }

    /**
     * 类型(用于下拉框选择)
     * @param act_id 活动id
     */
    public function filterList()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('filter_list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        //判断是否是区域联盟活动
        $isAllowAddArea = $this->model->isAllowAddArea($this->request->act_id);
        if ($isAllowAddArea !== true) {
            return $this->returnApi(202, $isAllowAddArea);
        }

        $condition[] = ['is_del', '=', 1];
        $condition[] = ['act_id', '=', $this->request->act_id];

        return $this->model->getFilterList(['id', 'act_id', 'name', 'img'], $condition, '`order` asc');
    }

    /**
     * 列表
     * @param act_id int 活动id
     * @param page int 当前页
     * @param limit int 分页大小
     * @param keywords string 搜索关键词(类型名称)
     */
    public function lists()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;
        $act_id = $this->request->act_id;

        //判断是否是区域联盟活动
        $isAllowAddArea = $this->model->isAllowAddArea($this->request->act_id);
        if ($isAllowAddArea !== true) {
            return $this->returnApi(202, $isAllowAddArea);
        }

        $condition[] = ['is_del', '=', 1];

        if ($keywords) {
            $condition[] = ['name', 'like', "%$keywords%"];
        }
        if ($act_id) {
            $condition[] = ['act_id', '=', $act_id];
        }

        return $this->model->getSimpleList(['id', 'act_id', 'name', 'img', 'intro', 'create_time'], $condition, $page, $limit, '`order` asc');
    }

    /**
     * 详情
     * @param id int 类型id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->select('id', 'act_id', 'name', 'img', 'intro', 'create_time')->find($this->request->id);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }

        return $this->returnApi(200, "获取成功", true, $res->toArray());
    }

    /**
     * 新增
     * @param act_id int活动id
     * @param name string 名称   最多 3个字符
     * @param img string 图片
     * @param intro string 简介
     */
    public function add()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('add')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        //判断是否是区域联盟活动
        $isAllowAddArea = $this->model->isAllowAddArea($this->request->act_id);
        if ($isAllowAddArea !== true) {
            return $this->returnApi(202, $isAllowAddArea);
        }

        $condition[] = ['act_id', '=', $this->request->act_id];

        $is_exists = $this->model->nameIsExists($this->request->name, 'name', null, 1, $condition);
        if ($is_exists) {
            return $this->returnApi(202, "此名称已存在");
        }

        $res = $this->model->add($this->request->all());

        if (!$res) {
            return $this->returnApi(202, "新增失败");
        }
        return $this->returnApi(200, "新增成功", true);
    }

    /**
     * 修改
     * @param id int 类型id
     * @param name string 名称  最多 3个字符
     * @param img string 图片
     * @param intro string 简介
     */
    public function change()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $result = $this->model->where('id', $this->request->id)->first();
        if (empty($result)) {
            return $this->returnApi(201, "参数传递错误");
        }

        //判断是否是区域联盟活动
        $isAllowAddArea = $this->model->isAllowAddArea($result->act_id, 3);
        if ($isAllowAddArea !== true) {
            return $this->returnApi(202, $isAllowAddArea);
        }

        $condition[] = ['act_id', '=', $result->act_id];

        $is_exists = $this->model->nameIsExists($this->request->name, 'name', $this->request->id, 1, $condition);

        if ($is_exists) {
            return $this->returnApi(202, "此名称已存在");
        }

        $res = $this->model->change($this->request->all(), ['name', 'img', 'intro']);

        if (!$res) {
            return $this->returnApi(202, "修改失败");
        }
        return $this->returnApi(200, "修改成功", true);
    }

    /**
     * 删除
     * @param id int 类型id
     */
    public function del()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->del($this->request->id);

        if ($res === true) {
            return $this->returnApi(200, "删除成功", true);
        }
        return $this->returnApi(202, $res);
    }

    /**
     * 修改区域排序
     * @param orders 排序   json格式数据  里面2个参数  id 区域id   order 序号
     * 
     * json 格式数据  [{"id":1,"order":1},{"id":2,"order":2},{"id":3,"order":3}]
     * 数组 格式数据  [['id'=>1 , 'order'=>1] , ['id'=>2 , 'order'=>2] , ['id'=>3 , 'order'=>3]]
     */
    public function orderChange()
    {
        if (!$this->validate->scene('order')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $orders = $this->request->orders;
        DB::beginTransaction();
        try {
            $this->model->orderChange($orders);
            DB::commit();
            return $this->returnApi(200, '修改成功', true);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, '修改失败');
        }
    }
}
