<?php

namespace App\Http\Controllers\Admin;


/**
 * 应用邀请码类
 */
class AppInviteCodeController extends CommonController
{
    protected $model;
    protected $validate;

    public function __construct()
    {
        parent::__construct();
        $this->model = new \App\Models\AppInviteCode();
        $this->validate = new  \App\Validate\AppInviteCodeValidate();
    }


    /**
     * 邀请码(用于下拉框选择)
     */
    public function filterList()
    {

        $condition[] = ['is_del', '=', 1];

        return $this->model->getFilterList(['id', 'name'], $condition);
    }

    /**
     * 列表
     * @param page int 当前页
     * @param limit int 分页大小
     * @param keywords string 搜索关键词(邀请码名称)
     */
    public function lists()
    {
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;

        $condition[] = ['is_del', '=', 1];

        if ($keywords) {
            $condition[] = ['name', 'like', "%$keywords%"];
        }

        return $this->model->getSimpleList(['id', 'name', 'content', 'intro', 'start_time', 'end_time', 'create_time', 'change_time'], $condition, $page, $limit);
    }

    /**
     * 详情
     * @param id int 邀请码id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->select('id', 'name', 'content', 'intro', 'start_time', 'end_time', 'create_time', 'change_time')->where('is_del' , 1)->find($this->request->id);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }

        return $this->returnApi(200, "获取成功", true, $res->toArray());
    }

    /**
     * 新增
     * @param name string 名称   必填
     * @param content string 邀请码  只能6位  必填
     * @param intro string 备注  选填
     * @param start_time string 有效期开始时间  选填   若不填，则表示立即生效    需红色字样提示
     * @param end_time string 有效期结束时间   选填  若不填，则表示永久有效    需红色字样提示
     */
    public function add()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('add')->check($this->request->all())) {
           return $this->returnApi(201,  $this->validate->getError());
        }
        $content = $this->request->content;
        $is_exists = $this->model->nameIsExists($content, 'content');

        if ($is_exists) {
            return $this->returnApi(202, "此邀请码已存在");
        }

        $res = $this->model->add($this->request->all());

        if (!$res) {
            return $this->returnApi(202, "新增失败");
        }
        return $this->returnApi(200, "新增成功", true);
    }

    /**
     * 修改
     * @param id int id
     * @param name string 名称   必填
     * @param content string 邀请码  只能6位  必填
     * @param intro string 备注  选填
     * @param start_time string 有效期开始时间  选填   若不填，则表示立即生效    需红色字样提示
     * @param end_time string 有效期结束时间   选填  若不填，则表示永久有效    需红色字样提示
     */
    public function change()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $id = $this->request->id;
        $content = $this->request->content;

        $is_exists = $this->model->nameIsExists($content, 'content', $id);

        if ($is_exists) {
            return $this->returnApi(202, "此邀请码已存在");
        }

        $res = $this->model->change($this->request->all());

        if (!$res) {
            return $this->returnApi(202, "修改失败");
        }
        return $this->returnApi(200, "修改成功", true);
    }

    /**
     * 删除
     * @param id int 邀请码id
     */
    public function del()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->del($this->request->id);

        if ($res === true) {
            return $this->returnApi(200, "删除成功", true);
        }
        return $this->returnApi(202, $res);
    }
}
