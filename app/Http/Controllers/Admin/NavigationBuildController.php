<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\TempInfoController;
use App\Models\NavigationBuild;
use App\Validate\NavigationBuildValidate;
use Illuminate\Support\Facades\DB;

/**
 * 室内导航建筑配置
 */
class NavigationBuildController extends CommonController
{
    protected $model;
    protected $validate;

    public function __construct()
    {
        parent::__construct();

        $this->model = new NavigationBuild();
        $this->validate = new  NavigationBuildValidate();
    }

    /**
     * 列表
     * @param page int 页码
     * @param limit int 分页大小
     * @param keywords string 搜索关键词
     */
    public function lists()
    {
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;

        $res = $this->model->lists($keywords, null,$limit);
        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }
        $res = $this->disPageData($res);
        // $navigation_index = config('other.navigation_index');
        // foreach ($res['data'] as $k => $v) {
        //     //拼接返回链接
        //     $res['data'][$k]['url'] = $navigation_index . $v['id'];
        // }
        $res['data'] = $this->addSerialNumber($res['data'], $page, $limit);
        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 详情
     * @param id int 建筑id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201, $this->validate->getError());
        }

        $id = $this->request->id;
        $res = $this->model->where('id', $id)->where('is_del', 1)->first();
        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }
        return $this->returnApi(200, "查询成功", true, $res->toArray());
    }

    /**
     * 新增
     * @param name string 建筑名称
     * @param province string 省
     * @param city string 市
     * @param district string 区
     * @param address string 详细地址
     * @param remark string 备注
     */
    public function add()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('add')->check($this->request->all())) {
            return $this->returnApi(201, $this->validate->getError());
        }
        $is_exists = $this->model->nameIsExists($this->request->name, 'name');

        if ($is_exists) {
            return $this->returnApi(202, "该建筑名称已存在");
        }
        $res = $this->model->add($this->request->all());
        if (!$res) {
            return $this->returnApi(202, "新增失败");
        }
        return $this->returnApi(200, "新增成功", true);
    }


    /**
     * 编辑
     * @param id int 建筑id
     * @param name string 建筑名称
     * @param province string 省
     * @param city string 市
     * @param district string 区
     * @param address string 详细地址
     * @param remark string 备注
     */
    public function change()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('change')->check($this->request->all())) {
            return $this->returnApi(201, $this->validate->getError());
        }
        $name = $this->request->name;
        $id = $this->request->id;

        $is_exists = $this->model->nameIsExists($name, 'name', $this->request->id);
        if ($is_exists) {
            return $this->returnApi(202, "该区域名称已存在");
        }

        $data = $this->model->where('id', $id)->where('is_del', 1)->first();
        if (!$data) {
            return $this->returnApi(201, "参数传递错误");
        }
        // 启动事务
        DB::beginTransaction();
        try {
            $this->model->change($this->request->all());
            // 提交事务
            DB::commit();
            return $this->returnApi(200, "保存成功", true);
        } catch (\Exception $e) {
            // 回滚事务
            DB::rollBack();
            return $this->returnApi(202, $e->getMessage());
        }
    }

    /**
     * 删除
     * @param id int 建筑id
     */
    public function del()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('del')->check($this->request->all())) {
            return $this->returnApi(201, $this->validate->getError());
        }
        $res = $this->model->del($this->request->id);
        if ($res) {
            return $this->returnApi(200, "删除成功", true);
        }
        return $this->returnApi(202, $res);
    }


    /**
     * 发布与取消发布
     * @param ids 建筑id，多个用逗号拼接   all 是，其余字符  ids 必传
     * @param is_play 是否发布  1.发布  2.未发布  默认2
     */
    public function playAndCancel()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('play_and_cancel')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        list($code, $msg) = $this->model->resumeAndCancel($this->request->ids, 'is_play', $this->request->is_play);

        $is_play = $this->request->is_play == 1 ? '启用' : '停用';
        if ($code === 200) {
            return $this->returnApi(200, $is_play . $msg, true);
        }
        return $this->returnApi($code, $is_play . $msg);
    }
}
