<?php

namespace App\Http\Controllers\Admin;

use App\Models\ShopBook;
use App\Validate\NewBookRecomValidate;
use Illuminate\Support\Facades\DB;


/**
 * （图书到家）书店书籍管理
 * Class NewBookRecom
 * @package app\api\controller
 */
class NewBookRecomController extends CommonController
{
    private $model = null;
    private $validate = null;

    public function __construct()
    {
        parent::__construct();
        $this->model = new ShopBook();
        $this->validate = new NewBookRecomValidate();
    }


    /**
     * 列表
     * @param shop_id  书店id   0、全部
     * @param keywords_type 检索条件   0、全部  1、书名  2、作者 3 ISBN   默认 0
     * @param keywords  检索条件
     * @param book_selector  是否套书  0、全部 1 是  2 否   默认 0
     * @param type_id   空字符串、代表全部    不能用  0 ，因为 0 表示 其他类型
     * @param is_recom  是否为推荐图书  0、全部 1 是 2 否   默认 0
     * @param is_plane	是否在架   0、全部 1 在架   2 已下架
     * @param is_stock	是否有库存   0、全部 1 有库存   2 无库存
     * @param page  页数，默认为1，
     * @param limit  条数，默认显示 10条
     */
    public function lists()
    {
        $shop_id = $this->request->input('shop_id', '');
        $keywords_type = $this->request->input('keywords_type', '');
        $keywords = $this->request->input('keywords', '');
        $book_selector = $this->request->input('book_selector', '');
        $is_recom = $this->request->input('is_recom', '');
        $is_plane = $this->request->input('is_plane', '');
        $is_stock = $this->request->input('is_stock', '');
        $type_id = $this->request->input('type_id', '');
        $page = intval($this->request->input('page', 1)) ?: 1;
        $limit = intval($this->request->input('limit', 10)) ?: 10;

        $keywords = addslashes($keywords);

        $shop_all_id = $this->getAdminShopIdAll();
        if (empty($shop_all_id)) {
            return $this->returnApi(203, '暂无数据'); //无权限访问
        }

        $res = $this->model->lists(null, $shop_id, $keywords, $keywords_type, $type_id, $book_selector, $is_recom, $is_plane, $is_stock, null, $shop_all_id, $limit);
        if (empty($res['data'])) {
            return $this->returnApi(203, '暂无数据'); //无权限访问
        }

        foreach ($res['data'] as $key => $val) {
            $res['data'][$key][$this->list_index_key] = $this->addSerialNumberOne($key, $page, $limit);
            if (empty($val['type_name'])) {
                $res['data'][$key]['type_name'] = '暂无分类';
                $res['data'][$key]['type_id'] = 0; //防止查询数据为null
            }
        }
        $res = $this->disPageData($res);
        return $this->returnApi(200, '获取成功', true, $res);
    }

    /**
     * 详情
     * @param id int 书籍id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $shop_all_id = $this->getAdminShopIdAll();
        if (empty($shop_all_id)) {
            return $this->returnApi(203, '暂无数据'); //无权限访问
        }

        $res = $this->model->detail($this->request->id, $shop_all_id);
        if (!$res) {
            return $this->returnApi(201, "参数传递错误", "YES");
        }

        $res = $res->toArray();

        return $this->returnApi(200, "查询成功", true, $res);
    }


    /**
     * 添加
     * @param shop_id  书店id
     * @param book_name  书名
     * @param author  作者
     * @param press  出版社     选填
     * @param pre_time  出版时间     选填
     * @param isbn  ISBN号
     * @param price  价格
     * @param img  图片
     * @param book_num  索书号 
     * @param type_id   书籍分类
     * @param intro  简介      选填
     * @param is_recom  是否为推荐图书   1 是 2 否   默认2
     * @param is_plane  是否在架  1 在架   2 已下架   默认1 
     * @param book_selector  是否套书  1 是  2 否   默认 2
     * @param number  库存数量
     */
    public function add()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('add')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $condition[] = ['isbn', '=', $this->request->isbn]; //增加检索条件
        $condition[] = ['shop_id', '=', $this->request->shop_id]; //增加检索条件
        $is_exists = $this->model->nameIsExists($this->request->book_name, 'book_name', null, 1, $condition);

        if ($is_exists) {
            return $this->returnApi(202, "该书籍已存在");
        }
        $data = $this->request->all();
        if(empty($data['img'])) unset($data['img']);
        $res = $this->model->add($data);

        if (!$res) {
            return $this->returnApi(202, "新增失败");
        }
        return $this->returnApi(200, "新增成功", true);
    }

    /**
     * 修改
     * @param shop_id  书店id
     * @param id int 书籍id 必传
     * @param book_name  书名
     * @param author  作者
     * @param press  出版社     选填
     * @param pre_time  出版时间     选填
     * @param isbn  ISBN号
     * @param price  价格
     * @param img  图片
     * @param book_num  索书号 
     * @param type_id   书籍分类
     * @param intro  简介      选填
     * @param is_recom  是否为推荐图书   1 是 2 否   默认2
     * @param is_plane  是否在架  1 在架   2 已下架   默认1 
     * @param book_selector  是否套书  1 是  2 否   默认 2
     * @param number  库存数量
     */
    public function change()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $condition[] = ['isbn', '=', $this->request->isbn]; //增加检索条件
        $condition[] = ['shop_id', '=', $this->request->shop_id]; //增加检索条件
        $is_exists = $this->model->nameIsExists($this->request->book_name, 'book_name', $this->request->id, 1, $condition);

        if ($is_exists) {
            return $this->returnApi(202, "该书籍已存在");
        }
        $data = $this->request->all();
        if(empty($data['img'])) unset($data['img']);
        $res = $this->model->change($data);

        if (!$res) {
            return $this->returnApi(202, "修改失败");
        }
        return $this->returnApi(200, "修改成功", true);
    }

    /**
     * 删除
     * @param id int 类型id
     */
    public function del()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $shop_all_id = $this->getAdminShopIdAll();
        if (empty($shop_all_id)) {
            return $this->returnApi(203, '暂无数据'); //无权限访问
        }
        $res = $this->model->detail($this->request->id, $shop_all_id);
        if (!$res) {
            return $this->returnApi(201, "数据获取失败");
        }

        $res = $this->model->del($this->request->id);

        if ($res === true) {
            return $this->returnApi(200, "删除成功", true);
        }
        return $this->returnApi(202, $res);
    }

    /**
     * 推荐与取消推荐
     * @param ids int 书籍id
     * @param is_recom int 推荐   1 推荐  2 撤销
     */
    public function cancelAndRelease()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('cancel_and_release')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $shop_all_id = $this->getAdminShopIdAll();
        if (empty($shop_all_id)) {
            return $this->returnApi(203, '暂无数据'); //无权限访问
        }

        DB::beginTransaction();
        try {
            $condition[] = [function ($query) use ($shop_all_id) {
                $query->whereIn('shop_id', $shop_all_id);
            }];
            list($code, $msg) = $this->model->resumeAndCancel($this->request->ids, 'is_recom', $this->request->is_recom, 1, $condition);

            $is_recom = $this->request->is_recom == 1 ? '推荐' : '撤销';

            DB::commit();
            return $this->returnApi($code, $is_recom . $msg, $code === 200 ? true : false);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, $e->getMessage());
        }
    }

    /**
     * 上架和下架
     * @param ids int 书籍id  多个逗号拼接  all 表示全部
     * @param is_plane int  状态   1 上架   2 下架
     */
    public function upAndDown()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('up_and_down')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $shop_all_id = $this->getAdminShopIdAll();
        if (empty($shop_all_id)) {
            return $this->returnApi(203, '暂无数据'); //无权限访问
        }
        DB::beginTransaction();
        try {
            $condition[] = [function ($query) use ($shop_all_id) {
                $query->whereIn('shop_id', $shop_all_id);
            }];

            list($code, $msg) = $this->model->resumeAndCancel($this->request->ids, 'is_plane', $this->request->is_plane, 1, $condition);

            $is_plane = $this->request->is_plane == 1 ? '上架' : '下架';

            DB::commit();
            return $this->returnApi($code, $is_plane . $msg, $code === 200 ? true : false);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, $e->getMessage());
        }
    }
}
