<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Authors;
use App\Validate\AuthorsValidate;
use Illuminate\Http\Request;


/**
 * 作者管理
 */
class AuthersController extends CommonController
{
    public $authorModelObj = null;
    public $validateObj = null;

    public function __construct()
    {
        parent::__construct();

        $this->validateObj = new AuthorsValidate();
        $this->authorModelObj = new Authors();
    }

    /**
     * 作者列表
     * @param keywords 关键字搜索
     * @param page 页数    默认第一页
     * @param limit 限制条数   默认 10 条
     * @param start_time 开始时间
     * @param end_time 结束时间
     */
    public function authorList()
    {
        $keywords = $this->request->input('keywords', '');
        $start_time = $this->request->input('start_time', '');
        $end_time = $this->request->input('end_time', '');
        $page = $this->request->input('page', 1);
        $limit = $this->request->input('limit', 10);
        $res = $this->authorModelObj->select(['id', 'username', 'username_en', 'birthday', 'sex', 'intro', 'intro_en', 'create_time','img'])
            ->where(function ($query) use ($keywords, $start_time, $end_time) {
                if ($keywords) {
                    $query->where('username', 'like', '%' . $keywords . '%');
                }

                if (!empty($start_time) && !empty($end_time)) {
                    $query->whereBetween('create_time', [$start_time, $end_time]);
                }
            })
            ->where('is_del', 1)
            ->orderByDesc('id')
            ->paginate($limit)
            ->toArray();

        if ($res['data']) {
            $res = $this->disPageData($res);
            $res['data'] = $this->addSerialNumber($res['data'], $page , $limit);

            return $this->returnApi(200, '获取成功', true, $res);
        }

        return $this->returnApi(203, '暂无数据');
    }

    /**
     * 作者列表（用于筛选）
     * @param keywords 关键字搜索
     */
    public function authorFilterList()
    {
        $keywords = $this->request->input('keywords', '');
        $res = $this->authorModelObj->select(['id', 'username', 'username_en'])
            ->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('username', 'like', '%' . $keywords . '%');
                }
            })
            ->where('is_del', 1)
            ->orderByDesc('id')
            ->get();

        if ($res) {
            return $this->returnApi(200, '获取成功', true, $res);
        }

        return $this->returnApi(203, '暂无数据');
    }

    /**
     * 作者详情
     * @param id 作者id
     */
    public function authorInfo()
    {
        //增加验证场景进行验证
        if (!$this->validateObj->scene('author_info')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validateObj->getError());
        }
        $res = $this->authorModelObj->select(['id', 'username', 'username_en', 'birthday', 'sex', 'intro', 'intro_en', 'create_time','img'])->where('is_del', 1)->find($this->request->id);
        if (empty($res)) {
            return $this->returnApi(202, '获取失败');
        }
        return $this->returnApi(200, '获取成功', true, $res);
    }

    /**
     * 作者添加
     * @param username 作者名称    必选
     * @param username_en 作者名称英文    
     * @param birthday 生日   日期格式  2021-06-03
     * @param sex 性别   1 男  2 女
     * @param img 图片
     * @param intro 作者简介
     * @param intro_en 作者简介英文
     */

    public function authorAdd()
    {
        //增加验证场景进行验证
        if (!$this->validateObj->scene('author_add')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validateObj->getError());
        }

        $is_exists = $this->authorModelObj->authorNameIsExists($this->request->username);
        if ($is_exists) {
            return $this->returnApi(202, '此名称已存在，请重新添加');
        }

        $res = $this->authorModelObj->add($this->request);


        if ($res) {
            return $this->returnApi(200, '添加成功', true);
        }

        return $this->returnApi(202, '添加失败');
    }

    /**
     * 作者修改
     * @param id  作者id  必选
     * @param username 作者名称    必选
     * @param username_en 作者名称英文    
     * @param birthday 生日   日期格式  2021-06-03
     * @param sex 性别   1 男  2 女
     * @param img 图片
     * @param intro 作者简介
     * @param intro_en 作者简介英文
     */

    public function authorChange()
    {
        //增加验证场景进行验证
        if (!$this->validateObj->scene('author_change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validateObj->getError());
        }
        $is_exists = $this->authorModelObj->authorNameIsExists($this->request->name, $this->request->id);
        if ($is_exists) {
            return $this->returnApi(202, '此名称已存在，请重新修改');
        }

        $res = $this->authorModelObj->change($this->request);

        if ($res) {
            return $this->returnApi(200, '修改成功', true);
        }

        return $this->returnApi(202, '修改失败');
    }

    /**
     * 作者删除
     * @param id  作者id  必选
     */

    public function authorDel()
    {
        //增加验证场景进行验证
        if (!$this->validateObj->scene('author_del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validateObj->getError());
        }
        $res = $this->authorModelObj->where('is_del', 1)->find($this->request->id);
        if (empty($res)) {
            return $this->returnApi(202, '删除失败');
        }
        $res->is_del = 2;
        $result = $res->save();

        if ($result) {
            return $this->returnApi(200, '删除成功', true);
        }

        return $this->returnApi(202, '删除失败');
    }
}
