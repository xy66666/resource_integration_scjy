<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ExhibitionVideo;
use App\Models\Manage;
use App\Models\Video;
use App\Validate\VideoValidate;
use Illuminate\Http\Request;


/**
 * 视频 - 视频类 管理
 */
class VideoController extends CommonController
{
    public $videoModelObj = null;
    public $validateObj = null;

    public function __construct(){
        parent::__construct();

        $this->validateObj = new VideoValidate();
        $this->videoModelObj = new Video();
    }

    /**
     * 视频列表
     * @param keywords 关键字搜索
     * @param page 页数    默认第一页
     * @param limit 限制条数   默认 10 条
     * @param start_time 开始时间
     * @param end_time 结束时间
     * @param author_id 作者id 空或 0 表示全部
     * @param type_id 类型id  空或 0 表示全部
     * @param exhibition_id 展览id  空或 0 表示全部
     * @param except_ids 排除视频id  空或 0 表示不排除  多个逗号拼接
     */
    public function videoList(){
        $keywords = $this->request->input('keywords' , '');
        $start_time = $this->request->input('start_time' , '');
        $end_time = $this->request->input('end_time' , '');
        $limit = $this->request->input('limit' , 10);
        $page = $this->request->input('page' , 1);
        $author_id = $this->request->input('author_id' , '0');
        $type_id = $this->request->input('type_id' , '0');
        $exhibition_id = $this->request->input('exhibition_id' , '0');
        $except_ids = $this->request->input('except_ids' , '0');
        if($exhibition_id){
            $exhibitionVideo = new ExhibitionVideo();
            $video_ids = $exhibitionVideo->getVideoIdByExhibitionId($exhibition_id , 2);
        }else{
            $video_ids = [];
        }

        $res = $this->videoModelObj->select(['id','video_name','author_id','type_id','year','intro','intro_en','video_addr','img','img_thumb','manage_id','create_time'])
        ->with(['author'=>function($query){
            $query->select('id' ,'username');
        },'typeName' , 'manageInfo'])
        ->where(function($query) use($keywords , $start_time , $end_time , $author_id , $type_id , $exhibition_id , $video_ids,$except_ids){
                if($keywords){
                    $query->where('video_name' , 'like' , '%'.$keywords.'%');
                }
                if($author_id){
                    $query->where('author_id' , $author_id);
                }
                if($type_id){
                    $query->where('type_id' , $type_id);
                }
                if($exhibition_id){
                    $query->whereIn('id' , $video_ids);
                }

                if(!empty($start_time) && !empty($end_time)){
                    $query->whereBetween('create_time' , [$start_time , $end_time]);
                }

                if($except_ids){
                    $except_ids = explode(',' , $except_ids);
                    $query->whereNotIn('id' , $except_ids);
                }
        })
        ->where('is_del' , 1)
        ->orderByDesc('id')
        ->paginate($limit)
        ->toArray();

        if($res['data']){
            $res = $this->disPageData($res);
            //自己处理数据
            $manage_name = Manage::$manage_name;
            foreach($res['data'] as $key=>&$val){
                $val['author_name'] = $val['author']['username'] ?: null;
                $type_name = $val['type_name']['name'] ?: null;
                $val['manage_name'] = $val['manage_info'][$manage_name] ?: null;

                unset($val['author']);
                unset($val['type_name']);
                unset($val['manage_info']);

                $val['type_name'] = $type_name;//最后赋值
            }


            //增加序号
            $res['data'] = $this->addSerialNumber($res['data'], $page , $limit);
            
            return $this->returnApi(200 , '获取成功' , true , $res);
        }

        return $this->returnApi(203 , '暂无数据');
    }

    /**
     * 视频详情
     * @param id 作者id
     */
    public function videoInfo()
    {
        //增加验证场景进行验证
        if (!$this->validateObj->scene('video_info')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validateObj->getError());
        }
        $res = $this->videoModelObj->with(['author'=>function($query){
                    $query->select('id' ,'username');
                },'typeName' , 'manageInfo'])
                ->where('is_del', 1)
                ->find($this->request->id);
        if (empty($res)) {
            return $this->returnApi(202, '获取失败');
        }

        $res = $res->toArray();

        $res['author_name'] = $res['author']['username'] ?: null;
        $type_name = $res['type_name']['name'] ?: null;
        $res['manage_name'] = $res['manage_info'][Manage::$manage_name] ?: null;

        unset($res['author'] , $res['type_name'] , $res['manage_info']);

        $res['type_name'] = $type_name;//最后赋值

        return $this->returnApi(200, '获取成功', true, $res);
    }

    /**
     * 视频添加
     * @param video_name 视频名称    必选
     * @param video_name_en 视频名称英文    
     * @param year 年份
     * @param author_id 作者id
     * @param type_id 类型id
     * @param intro 简介
     * @param intro_en 简介英文
     * @param img 图片
     * @param video_addr 视频地址
     */
     public function videoAdd(){
        //增加验证场景进行验证
        if (!$this->validateObj->scene('video_add')->check($this->request->all())) {
            return $this->returnApi(201 ,  $this->validateObj->getError());
        }

        $is_exists = $this->videoModelObj->videoNameIsExists($this->request->video_name);
        if($is_exists){
            return $this->returnApi(202 , '此名称已存在，请重新添加'); 
        }

        $res = $this->videoModelObj->add($this->request);


        if($res){
            return $this->returnApi(200 , '添加成功' , true);
        }

        return $this->returnApi(202 , '添加失败');
     }

     /**
     * 视频修改
     * @param id  视频id  必选
     * @param video_name 视频名称    必选
     * @param video_name_en 视频名称英文   
     * @param year 年份
     * @param author_id 作者id
     * @param type_id 类型id
     * @param intro 简介
     * @param intro_en 简介英文
     * @param img 图片
     * @param video_addr 视频地址
     */

    public function videoChange(){
        //增加验证场景进行验证
        if (!$this->validateObj->scene('video_change')->check($this->request->all())) {
            return $this->returnApi(201 ,  $this->validateObj->getError());
        }
        $is_exists = $this->videoModelObj->videoNameIsExists($this->request->video_name ,$this->request->id);
        if($is_exists){
            return $this->returnApi(202 , '此名称已存在，请重新修改'); 
        }

        $res = $this->videoModelObj->change($this->request);

        if($res){
            return $this->returnApi(200 , '修改成功' , true);
        }

        return $this->returnApi(202, '修改失败');
     }

    /**
     * 视频删除
     * @param id  视频id  必选
     */
    public function videoDel(){
        //增加验证场景进行验证
        if (!$this->validateObj->scene('video_del')->check($this->request->all())) {
            return $this->returnApi(201 ,  $this->validateObj->getError());
        }
   
        $res = $this->videoModelObj->where('is_del' , 1)->find($this->request->id);

        if(empty($res)){
            return $this->returnApi(202 , '删除失败');
        }

        //判断此视频是否已在选择展览，已选择不允许删除
        $exhibitionVideo = new ExhibitionVideo();
        $is_exhibition = $exhibitionVideo->videoIsExhibition($this->request->id , 2);
        if($is_exhibition){
            return $this->returnApi(202 , '此视频已被选中在《'.$is_exhibition.'》的展览中，请先取消在进行删除');
        }

        $res->is_del = 2;
        $result = $res->save();

        if($result){
            return $this->returnApi(200 , '删除成功' , true);
        }

        return $this->returnApi(202 , '删除失败');
     }

}
