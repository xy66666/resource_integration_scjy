<?php

namespace App\Http\Controllers\Admin;

use App\Models\BookTypes;
use App\Models\LibBookType;
use App\Validate\LibBookTypeValidate;

/**
 * （图书到家）图书馆书籍类型管理
 */
class LibBookRecomTypeController extends CommonController
{

    public $model = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new LibBookType();
        $this->validate = new LibBookTypeValidate();
    }


    /**
     * 类型(用于下拉框选择)
     */
    public function filterList()
    {

        $condition[] = ['is_del', '=', 1];

        return $this->model->getFilterList(['id', 'type_name'], $condition);
    }

    /**
     * 列表
     * @param page int 当前页
     * @param limit int 分页大小
     * @param keywords string 搜索关键词(类型名称)
     */
    public function lists()
    {
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;
        //获取中途分类法的书籍类型
        $bookTypesObj = new BookTypes();
        $res = $bookTypesObj->select('id', 'title as type_name')->where('level', 1)->where(function ($query) use ($keywords) {
            if ($keywords) {
                $query->where('title', 'like', "%$keywords%");
            }
        })->orderBy('id')
            ->paginate($limit)
            ->toArray();
        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }
        //增加序号
        $res['data'] = $this->addSerialNumber($res['data'], $page, $limit);
        return $this->returnApi(200, "获取成功", true, $res);
    }

    /**
     * 详情
     * @param id int 类型id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->select('id', 'type_name', 'create_time')->find($this->request->id);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }

        return $this->returnApi(200, "获取成功", true, $res->toArray());
    }

    /**
     * 新增
     * @param type_name string 类型名称
     */
    public function add()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('add')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $is_exists = $this->model->nameIsExists($this->request->type_name, 'type_name');

        if ($is_exists) {
            return $this->returnApi(202, "此名称已存在");
        }

        $res = $this->model->add($this->request->all());

        if (!$res) {
            return $this->returnApi(202, "新增失败");
        }
        return $this->returnApi(200, "新增成功", true);
    }

    /**
     * 修改
     * @param id int 类型id
     * @param type_name string 类型名称
     */
    public function change()
    {

        //增加验证场景进行验证
        if (!$this->validate->scene('change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $is_exists = $this->model->nameIsExists($this->request->type_name, 'type_name', $this->request->id);

        if ($is_exists) {
            return $this->returnApi(202, "此名称已存在");
        }

        $res = $this->model->change($this->request->all());

        if (!$res) {
            return $this->returnApi(202, "修改失败");
        }
        return $this->returnApi(200, "修改成功", true);
    }

    /**
     * 删除
     * @param id int 类型id
     */
    public function del()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->del($this->request->id);

        if ($res === true) {
            return $this->returnApi(200, "删除成功", true);
        }
        return $this->returnApi(202, $res);
    }
}
