<?php

namespace App\Http\Controllers\Admin;

use App\Models\TurnActivityLimitAddress;
use App\Validate\TurnActivityLimitAddressValidate;

/**
 * 答题活动限制地址
 */
class TurnActivityLimitAddressController extends CommonController
{

    public $model = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new TurnActivityLimitAddress();
        $this->validate = new TurnActivityLimitAddressValidate();
    }


    /**
     * 列表
     * @param act_id int 活动id
     * @param page int 当前页
     * @param limit int 分页大小
     * @param keywords string 搜索关键词(地址管理地址)
     * @param start_time datetime 活动开始时间    数据格式  年月日
     * @param end_time datetime 活动结束时间
     */
    public function lists()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        // $keywords = $this->request->keywords;
        $act_id = $this->request->act_id;
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;

        $res = $this->model->lists(null, $act_id, $start_time, $end_time, $limit);
        if (empty($res['data'])) {
            return $this->returnApi(203, '暂无数据');
        }


        $res = $this->disPageData($res);
        //增加序号
        $res['data'] = $this->addSerialNumber($res['data'], $page, $limit);

        return $this->returnApi(200, '获取成功', true, $res);
    }

    /**
     * 详情
     * @param id int 地址管理id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->detail($this->request->id);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }

        return $this->returnApi(200, "获取成功", true, $res->toArray());
    }

    /**
     * 新增
     * @param act_id int 活动id
     * @param province string 省  必填
     * @param city string 市  选填
     */
    public function add()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('add')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $city = $this->request->city;
        $city = $city ?? null;
        $findWhere[] = ['city', $city];
        $findWhere[] = ['act_id', '=', $this->request->act_id];
        $is_exists = $this->model->nameIsExists($this->request->province, 'province', null, 1, $findWhere);

        if ($is_exists) {
            return $this->returnApi(202, "该地址已存在");
        }
        $res = $this->model->add($this->request->all());

        if (!$res) {
            return $this->returnApi(202, "新增失败");
        }
        return $this->returnApi(200, "新增成功", true);
    }

    /**
     * 修改
     * @param id int 地址管理id
     * @param act_id int 活动id
     * @param province string 省  必填
     * @param city string 市  选填
     */
    public function change()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        // $node = $this->request->node;
        // $nodes = explode(',', $node);
        // if(in_array(3 , $nodes) && in_array(4 , $nodes)){
        //     return $this->returnApi(201, "视频和音频不能同时存在");
        // }

        $city = $this->request->city;
        $city = $city ?? null;
        $findWhere[] = ['city', $city];
        $findWhere[] = ['act_id', '=', $this->request->act_id];
        $is_exists = $this->model->nameIsExists($this->request->province, 'province',  $this->request->id, 1, $findWhere);

        if ($is_exists) {
            return $this->returnApi(202, "该地址已存在");
        }
        $res = $this->model->change($this->request->all());

        if (!$res) {
            return $this->returnApi(202, "修改失败");
        }
        return $this->returnApi(200, "修改成功", true);
    }

    /**
     * 删除
     * @param id int 地址管理id
     */
    public function del()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->del($this->request->id);

        if ($res === true) {
            return $this->returnApi(200, "删除成功", true);
        }
        return $this->returnApi(202, $res);
    }
}
