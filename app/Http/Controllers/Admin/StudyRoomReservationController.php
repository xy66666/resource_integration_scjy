<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\QrCodeController;
use App\Models\StudyRoomReservation;
use App\Models\StudyRoomReservationApply;
use App\Models\StudyRoomReservationSchedule;
use App\Models\StudyRoomReservationSeat;
use App\Validate\StudyRoomReservationValidate;
use Illuminate\Support\Facades\DB;

/**
 * 文化配送类
 */
class StudyRoomReservationController extends CommonController
{
    protected $model;
    protected $scheduleModel;
    protected $validate;

    public function __construct()
    {
        parent::__construct();

        $this->model = new StudyRoomReservation();
        $this->scheduleModel = new StudyRoomReservationSchedule();
        $this->validate = new StudyRoomReservationValidate();
    }

    /**
     * 获取预约参数
     */
    public function getReservationApplyParam()
    {
        $data = $this->model->getReservationApplyParam();
        return $this->returnApi(200, "获取成功", true, $data);
    }

    /**
     * 预约标签列表（预定义）
     */
    public function filterTagList()
    {
        $res = $this->definedReservationTag();

        if (!$res) {
            return $this->returnApi(203, "无数据");
        }

        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 获取筛选列表
     * @param start_time int 开始时间
     * @param end_time int 结束时间
     * @param keywords string 搜索筛选
     */
    public function getFilterList()
    {
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        $keywords = $this->request->keywords;


        $res = $this->model->getFilterLists($keywords, $start_time, $end_time);

        if (empty($res)) {
            return $this->returnApi(203, "无数据");
        }
        return $this->returnApi(200, "查询成功", "YES", $res);
    }

    /**
     *  列表
     * @param page int 当前页数
     * @param limit int 分页大小
     * @param is_play int 是否发布  1.发布  2.未发布  默认2
     * @param start_time int 开始时间
     * @param end_time int 结束时间
     * @param keywords string 搜索筛选
     */
    public function lists()
    {
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? $this->request->limit : 10;
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        $keywords = $this->request->keywords;
        $is_play = $this->request->is_play;

        $res = $this->model->lists(null, $keywords, $start_time, $end_time, $is_play, $limit);

        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }

        $res = $this->disPageData($res);
        $res['data'] = $this->addSerialNumber($res['data'], $page, $limit);

        return $this->returnApi(200, "查询成功", true, $res);
    }


    /** 
     * 详情
     * @param id int 预约id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201, $this->validate->getError());
        }

        $res = $this->model->with(['conSchedule' => function ($query) {
            $query->where('is_del', 1);
        }])
            ->where('id', $this->request->id)
            ->first();

        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }

        $real_info_array = $this->model->getReservationApplyParam();

        $res->real_info_value = $this->getRealInfoArray($res->real_info, $real_info_array, 'value');
        $res->real_info_value = join("|", $res->real_info_value);

        $res = $res->toArray();
        $res['type_tag'] = $res['type_tag'] ? explode('|', $res['type_tag']) : [];
        $res['con_schedule'] = $res['con_schedule'] ? $res['con_schedule'] : [];

        /*构建排班数据*/
        if ($res['con_schedule']) {
            $week = [];
            for ($i = 1; $i <= 7; $i++) {
                $temp = [];
                $temp['week'] = $i;
                foreach ($res['con_schedule'] as $key => $value) {
                    $time_temp = [];
                    if ($i == $value['week']) {
                        $time_temp['start_time'] = $value['start_time'];
                        $time_temp['end_time'] = $value['end_time'];
                        $temp['time'][] = $time_temp;
                    }
                }
                $week[] = $temp;
            }
            $res['con_schedule'] = $week;
        }
        //判断是否可以更改是否需要绑定读者证，有人预约就不允许修改
        $apply_status = $this->model->resIsMake($this->request->id);
        $res['is_can_is_reader'] = $apply_status ? false : true; //false 有人报名，不允许修改

        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 新增
     * @param img string 图片
     * @param name string 名称
     * @param intro text 简介
     * @param type_tag string 补充类型(多个|连接)
     * @param contacts string 联系人姓名
     * @param tel string 联系方式
     * @param start_age int 年龄限制开始年龄  年龄限制了才能填
     * @param end_age int 年龄限制结束年龄
     *
     * @param is_real int 是否需要用户真实信息 1需要 2不需要 默认2
     * @param astrict_sex int 性别限制   1.限男性  2.限女性  3.不限(默认)
     * @param real_info string 需要验证的真实信息id用|连接起来   1、姓名   2 、身份证号码  3、电话号码   4、读者证号码    5、真实人物头像   6、备注  7 单位名称 8 性别 9 年龄  10、附件
     * @param number int 可预约量 默认1   多数量预约才有    5到馆预约 为总量   6 座位预约为座位个数
     * @param day_make_time time 每日开始预约时间，这个时间之前不能预约  默认都可以预约
     * @param clock_time int 预约开始后，多少分钟之内可打卡  单位分钟
     * @param is_sign int 是否需要签到 1需要 2不需要
     * @param is_reader int 报名是否需要绑定读者证   1.是   2.否(默认）
     * @param is_approval int 预约是否需要审核 1需要 2不需要
     * @param is_cancel int 预约是否可以取消 1是 2否
     * @param cancel_end_time int 预约可取消最后期限(单位分钟) 等于0时没有时间限制
     * @param province string 省
     * @param city string 市
     * @param district string 区(县)
     * @param address string 详细地址
     * @param week string 排版信息  json 格式数据
     * @param display_day 展示可预约天数，默认7天
     * @param sign_way int 扫码方式  1 、2者都可以  2、用户自主扫码  3、管理员设备扫码  
     * @param limit_num 限制用户可预约次数
     *         数组样式  ['day'=>1,'time'=>[['start_time'=>'15:12:12' , 'end_time'=>'16:12:12'],['start_time'=>'15:12:12' , 'end_time'=>'16:12:12']]];
     */
    public function add()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('add')->check($this->request->all())) {
            return $this->returnApi(201, $this->validate->getError());
        }

        if (strlen($this->request->cancel_end_time) > 4) {
            return $this->returnApi(201, "最后取消期限过大,请重新填写");
        }

        $is_exists = $this->model->nameIsExists($this->request->name, 'name');

        if ($is_exists) {
            return $this->returnApi(202, "此名称已存在");
        }

        /*生成经纬度*/
        $map = $this->getLonLatByAddress($this->request->province . $this->request->city . $this->request->district . $this->request->address);

        if (is_string($map)) {
            return $this->returnApi(202, $map);
        }

        //处理报名参数
        list($is_real, $real_info) = $this->model->disRealInfo($this->request->all());

        $real_info_arr = explode('|', $real_info);
        $week = $this->request->week;

        if (in_array(2, $real_info_arr) || in_array(8, $real_info_arr) && $this->request->astrict_sex) {
            $astrict_sex = $this->request->astrict_sex;
        } else {
            $astrict_sex = 3;
        }
        if (in_array(2, $real_info_arr) || in_array(9, $real_info_arr) && $this->request->start_age) {
            $start_age = $this->request->start_age;
        } else {
            $start_age = 0;
        }
        if (in_array(2, $real_info_arr) || in_array(9, $real_info_arr) && $this->request->end_age) {
            $end_age = $this->request->end_age;
        } else {
            $end_age = 0;
        }
        $data = [
            'stack_number' => 'S' . get_rnd_number(7),
            'img' => $this->request->img ? $this->request->img : $this->getDefaultImg($this->request->node),
            'name' => $this->request->name,
            'intro' => $this->request->intro,
            'type_tag' => $this->request->type_tag,
            'province' => $this->request->province,
            'city' => $this->request->city,
            'district' => $this->request->district,
            'address' => $this->request->address,
            'contacts' => $this->request->contacts,
            'tel' => $this->request->tel,
            'astrict_sex' => $astrict_sex,
            'is_real' => $is_real,
            'real_info' => $real_info,
            'start_age' => $start_age,
            'end_age' => $end_age,
            'number' => $this->request->number,
            'day_make_time' => $this->request->day_make_time,
            'clock_time' => $this->request->clock_time,
            'is_reader' => $this->request->is_reader,
            'is_approval' => $this->request->is_approval,
            'is_cancel' => $this->request->is_cancel,
            'cancel_end_time' => $this->request->cancel_end_time,
            'display_day' => !empty($this->request->display_day) ? $this->request->display_day : 7,
            'limit_num' => $this->request->limit_num,
            'sign_way' => $this->request->sign_way ? $this->request->sign_way : 2,
            'is_play' => $this->request->is_play ? $this->request->is_play : 2,
            'lon' => $map['lon'],
            'lat' => $map['lat'],
        ];


        $qrCodeObj = new QrCodeController();
        $qr_code = $qrCodeObj->getQrCode('study_room_reservation');
        if ($qr_code === false) {
            return $this->returnApi(201, "二维码生成失败,请重新添加");
        }
        $qr_url = $qrCodeObj->setQr($qr_code, true);
        if (!$qr_url) {
            return $this->returnApi(202, "二维码生成失败");
        }

        $data['qr_url'] = $qr_url;
        $data['qr_code'] = $qr_code;

        // 启动事务
        DB::beginTransaction();
        try {
            //保存预约
            $this->model->add($data);

            $this->model->cab_code = sprintf("%06d", $this->model->id); //生成6位数，不足前面补0;
            $this->model->save(); //更新编号


            /*维护预约排班表*/
            $this->scheduleModel->add($week, $this->model->id);

            // 提交事务
            DB::commit();
            return $this->returnApi(200, "新增成功", true);
        } catch (\Exception $e) {
            // var_dump($e);exit;

            // 回滚事务
            DB::rollBack();
            return $this->returnApi(202, $e->getMessage());
        }
    }

    /** 
     * 修改
     * @param id int id
     * @param img string 图片
     * @param name string 名称
     * @param intro text 简介
     * @param type_tag string 补充类型(多个|连接)
     * @param contacts string 联系人姓名
     * @param tel string 联系方式
     * @param start_age int 年龄限制开始年龄  年龄限制了才能填
     * @param end_age int 年龄限制结束年龄
     *
     * @param is_real int 是否需要用户真实信息 1需要 2不需要 默认2
     * @param astrict_sex int 性别限制   1.限男性  2.限女性  3.不限(默认)
     * @param real_info string 需要验证的真实信息id用|连接起来   1、姓名   2 、身份证号码  3、电话号码   4、读者证号码    5、真实人物头像   6、备注  7 单位名称 8 性别 9 年龄  10、附件
     * @param unit string 单位信息    物品所属单位（node为2才有）
     * @param number int 可预约量 默认1   多数量预约才有    5到馆预约 为总量   6 座位预约为座位个数
     * @param serial_prefix  座位预约 编号前缀 （只允许 26个字母） node 为 6 才有
     * @param day_make_time time 每日开始预约时间，这个时间之前不能预约  默认都可以预约
     * @param clock_time int 预约开始后，多少分钟之内可打卡  单位分钟
     * @param is_sign int 是否需要签到 1需要 2不需要
     * @param is_reader int 报名是否需要绑定读者证   1.是   2.否(默认）
     * @param is_approval int 预约是否需要审核 1需要 2不需要
     * @param is_cancel int 预约是否可以取消 1是 2否
     * @param cancel_end_time int 预约可取消最后期限(单位分钟) 等于0时没有时间限制
     * @param province string 省
     * @param city string 市
     * @param district string 区(县)
     * @param address string 详细地址
     * @param week string 排版信息  json 格式数据
     * @param display_day 展示可预约天数，默认7天
     * @param sign_way int 扫码方式  1 、2者都可以  2、用户自主扫码  3、管理员设备扫码  
     * @param limit_num 限制用户可预约次数
     *         数组样式  ['day'=>1,'time'=>[['start_time'=>'15:12:12' , 'end_time'=>'16:12:12'],['start_time'=>'15:12:12' , 'end_time'=>'16:12:12']]];//固定是 7个时间段，不存在的时间段，也必须要有
     */
    public function change()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('change')->check($this->request->all())) {
            return $this->returnApi(201, $this->validate->getError());
        }

        if (strlen($this->request->cancel_end_time) > 4) {
            return $this->returnApi(201, "最后取消期限过大,请重新填写");
        }
        $is_exists = $this->model->nameIsExists($this->request->name, 'name', $this->request->id);

        if ($is_exists) {
            return $this->returnApi(202, "此名称已存在");
        }
        $reservation_info = $this->model->find($this->request->id);
        if (empty($reservation_info)) {
            return $this->returnApi(201, "参数传递错误");
        }

        if ($reservation_info->is_play == 1) {
            return $this->returnApi(201, "请先撤销，在进行修改！");
        }

        //已有用户预约，不允许修改
        if ($reservation_info->is_reader !=  $this->request->is_reader) {
            //判断是否有人预约
            $reservationApplyModel = new StudyRoomReservationApply();
            $apply_status = $reservationApplyModel->getMakeNumber($this->request->id, [1, 3, 6, 7]);
            if ($apply_status) {
                return $this->returnApi(201, "已有用户预约，不能修改是否需要绑定读者证参数");
            }
        }

        /*生成经纬度*/
        if (
            !empty($this->request->province) && !empty($this->request->city) && !empty($this->request->district) && !empty($this->request->address)
            && $this->request->province . $this->request->city . $this->request->district . $this->request->address != $reservation_info->province . $reservation_info->city . $reservation_info->district . $reservation_info->address
        ) {
            $map = $this->getLonLatByAddress($this->request->province . $this->request->city . $this->request->district . $this->request->address);
            if (is_string($map)) {
                return $this->returnApi(202, $map);
            }
        } else {
            $map['lon'] = null;
            $map['lat'] = null;
        }


        //处理报名参数
        list($is_real, $real_info) = $this->model->disRealInfo($this->request->all());
        $id = $this->request->id;
        $week = $this->request->week;

        $real_info_arr = explode('|', $real_info);
        $week = $this->request->week;

        if (in_array(2, $real_info_arr) || in_array(8, $real_info_arr) && $this->request->astrict_sex) {
            $astrict_sex = $this->request->astrict_sex;
        } else {
            $astrict_sex = 3;
        }
        if (in_array(2, $real_info_arr) || in_array(9, $real_info_arr) && $this->request->start_age) {
            $start_age = $this->request->start_age;
        } else {
            $start_age = 0;
        }
        if (in_array(2, $real_info_arr) || in_array(9, $real_info_arr) && $this->request->end_age) {
            $end_age = $this->request->end_age;
        } else {
            $end_age = 0;
        }

        $data = [
            'id' => $this->request->id,
            'img' => $this->request->img ? $this->request->img : $this->getDefaultImg($this->request->node),
            'name' => $this->request->name,
            'intro' => $this->request->intro,
            'type_tag' => $this->request->type_tag,
            'province' => $this->request->province,
            'city' => $this->request->city,
            'district' => $this->request->district,
            'address' => $this->request->address,
            'contacts' => $this->request->contacts,
            'tel' => $this->request->tel,
            'astrict_sex' => $astrict_sex,
            'is_real' => $is_real,
            'real_info' => $real_info,
            'start_age' => $start_age,
            'end_age' => $end_age,
            'number' => $this->request->number,
            'day_make_time' => $this->request->day_make_time,
            'clock_time' => $this->request->clock_time,
            'is_reader' => $this->request->is_reader,
            'is_approval' => $this->request->is_approval,
            'is_cancel' => $this->request->is_cancel,
            'cancel_end_time' => $this->request->cancel_end_time,
            'display_day' => !empty($this->request->display_day) ? $this->request->display_day : 7,
            'limit_num' => $this->request->limit_num,
            'sign_way' => $this->request->sign_way ? $this->request->sign_way : 2,
            'is_play' => $this->request->is_play ? $this->request->is_play : 2,
            'lon' => $map['lon'],
            'lat' => $map['lat'],
        ];


        // 启动事务
        DB::beginTransaction();
        try {
            //保存预约
            $this->model->change($data);

            /**取消已预约 */
            list($weeks, $schedule_id_arr) = $this->scheduleModel->checkScheduleIsChange($week, $id);
            $applyModel = new StudyRoomReservationApply();
            $schedule_table = $this->scheduleModel->getTable();

            if ($weeks) {
                $reservation_apply = $applyModel
                    ->from($applyModel->getTable() . ' as a')
                    ->select('a.id', 'a.user_id', 'a.account_id', 'a.create_time', 'a.schedule_id', 'a.make_time')
                    ->join("$schedule_table as b", 'a.schedule_id', '=', 'b.id')
                    ->where('a.reservation_id', $id)
                    ->whereIn('week', $weeks)
                    ->whereIn('a.schedule_id', $schedule_id_arr) //只动修改过的排版
                    ->whereIn('status', [1, 3])
                    ->get()
                    ->toArray();

                if (!empty($reservation_apply)) {
                    $reservation_apply_ids = array_column($reservation_apply, 'id');
                    $applyModel->whereIn('id', $reservation_apply_ids)->update(['status' => 2, 'change_time' => date('Y-m-d H:i:s')]);
                }
            }

            /* 维护预约排班表*/
            $this->scheduleModel->change($week, $id);


            /*消息推送*/
            if (!empty($reservation_apply)) {
                $reservation_name = $reservation_info->name;
                foreach ($reservation_apply as $key => $val) {
                    $this->systemAdd("空间预约：申请失效", $val['user_id'], $val['account_id'], 53, intval($val['id']), '您申请的空间预约：【' . $reservation_name . '】排班已被管理员更改,您的预约已失效,请重新预约');
                }
            }
            // 提交事务
            DB::commit();
            return $this->returnApi(200, "编辑成功", true);
        } catch (\Exception $e) {
            // 回滚事务
            DB::rollBack();
            return $this->returnApi(202, "编辑失败");
        }
    }

    /**
     * 删除
     * @param id int
     */
    public function del()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('del')->check($this->request->all())) {
            return $this->returnApi(201, $this->validate->getError());
        }
        $reservation_info = $this->model->where('id', $this->request->id)->first();
        if (empty($reservation_info)) {
            return $this->returnApi(201, "参数传递错误");
        }
        if ($reservation_info->is_play == 1) {
            return $this->returnApi(201, "此预约已发布，撤销失败");
        }

        DB::beginTransaction();
        try {

            $res = $this->model->del($this->request->id);

            //撤销已删除的预约
            $applyModel = new StudyRoomReservationApply();
            $applyInfo = $applyModel->getApplyInfo($this->request->id, date('Y-m-d'), '>', [1, 3]);
            $apply_ids = array_column($applyInfo, 'id');
            $applyModel->whereIn('id', $apply_ids)->update(['status' => 2, 'change_time' => date('Y-m-d H:i:s')]);


            foreach ($applyInfo as $key => $val) {
                $this->systemAdd("空间预约：申请失效", $val->user_id, $val->account_id, 53, intval($val->id), '您申请的空间预约：【' . $reservation_info->name . '】当前预约已被管理员删除,您的预约已失效,请重新预约');
            }

            DB::commit();
            return $this->returnApi(200, "删除成功", true);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, $e->getMessage());
        }
    }

    /**
     * 发布与取消发布
     * @param ids 书籍id，多个用逗号拼接   all 是全部
     * @param is_play 是否发布  1.发布  2.未发布  默认2
     */
    public function playAndCancel()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('play_and_cancel')->check($this->request->all())) {
            return $this->returnApi(201, $this->validate->getError());
        }
        if ($this->request->is_all != 'all' && empty($this->request->ids)) {
            return $this->returnApi(201, "数据ID不能为空");
        }
        list($code, $msg) = $this->model->resumeAndCancel($this->request->ids, 'is_play', $this->request->is_play);

        $is_play = $this->request->is_play == 1 ? '发布' : '取消发布';
        if ($code === 200) {
            return $this->returnApi(200, $is_play . $msg, true);
        }
        return $this->returnApi($code, $is_play . $msg);
    }

    /**
     * 获取默认占位图
     * @param node int 预约类型
     */
    public function getDefaultImg()
    {
        return 'default/default_make.png';
    }
}
