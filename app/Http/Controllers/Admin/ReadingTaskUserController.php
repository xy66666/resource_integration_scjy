<?php

namespace App\Http\Controllers\Admin;

use App\Models\ReadingTaskAppointUser;
use App\Models\ReadingTaskDatabase;
use App\Models\ReadingTaskExecute;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * 用户阅读任务管理
 */
class ReadingTaskUserController extends CommonController
{
    protected $model;
    protected $validate;

    public function __construct()
    {
        parent::__construct();

        $this->model = new \App\Models\ReadingTaskExecute();
        $this->validate = new  \App\Validate\ReadingTaskValidate();
    }

    /**
     * 用户阅读任务列表
     * @param task_id int 阅读任务id
     * @param page int 页码
     * @param limit int 分页大小
     * @param is_finish string 是否完成   1 已完成  2 未完成
     * @param keywords string 搜索关键词（用户昵称检索）
     * @param sort string 排序  1 默认排序  2 完成时间排序
     */
    public function lists()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('reading_task_user_execute_list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $task_id = $this->request->task_id;
        $keywords = $this->request->keywords;
        $is_finish = $this->request->is_finish;
        $sort = $this->request->sort;

        $res = $this->model->readingTaskUserExecuteList($task_id, $keywords, $is_finish, $sort, $limit);
        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }

        $completed_number = $res['completed_number'];
        $unfinished_number = $res['unfinished_number'];
        $res = $this->disPageData($res);
        $res['data'] = $this->addSerialNumber($res['data'], $page, $limit);

        $res['completed_number'] = $completed_number;
        $res['unfinished_number'] = $unfinished_number;
        return $this->returnApi(200, "获取成功", true, $res);
    }
}
