<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\PdfController;
use App\Http\Controllers\ScoreRuleController;
use App\Http\Controllers\ZipFileDownloadController;
use App\Models\CompetiteActivity;
use App\Models\CompetiteActivityWorksType;
use App\Models\CompetiteActivityGroup;
use App\Models\CompetiteActivityWorks;
use App\Models\CompetiteActivityWorksCheck;
use App\Models\CompetiteActivityWorksScore;
use App\Models\Manage;
use App\Models\UserInfo;
use App\Validate\CompetiteActivityValidate;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * 线上大赛作品
 */
class CompetiteActivityWorksController extends CommonController
{

    public $score_type = 24;
    public $model = null;
    public $competiteActivityModel = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new CompetiteActivityWorks();
        $this->competiteActivityModel = new CompetiteActivity();
        $this->validate = new CompetiteActivityValidate();
    }


    /**
     * 列表
     * @param type 类型  1 个人账号  2 团队账号  当前是团队查询还是个人查询
     * @param page int 当前页
     * @param limit int 分页大小
     * @param con_id int 大赛id
     * @param group_id int 团队id  
     * @param status int 是否审核 状态  1.已通过   2.未通过   3.未审核(默认)   不传为全部
     * @param type_id int 类型id
     * @param keywords string 搜索关键词(作品名称|姓名|编号)
     * @param start_time datetime 投稿时间(开始)
     * @param end_time datetime 投稿时间(截止)
     * @param sort int 排序方式  1 时间排序（默认）  2 投票排序  3 评分排序
     * @param except_ids string 需要排除的id ，多个逗号链接
     * @param is_review_score string 是否评审打分  1 已打分  2 未打分
     * @param is_add_ebook string 是否是加入电子书 1 是 
     * @param is_violate string 是否违规  1正常 2违规 
     * @param is_show string 是否显示  1显示 2不显示 
     * @param except_database_id string 排除数据库对应产品id
     * 
     * @param score_manage_id string 打分审核人id  （con_id 必须存在）
     * @param score_manage_status string 打分审核人选择状态  0 查询所有（score_manage_id 无效） 1 查看所有已选择的作品  2 查询所有未选中的作品 
     */
    public function lists()
    {
        //增加验证场景进行验证
        // if (!$this->validate->scene('production_list')->check($this->request->all())) {
        //     return $this->returnApi(201,  $this->validate->getError());
        // }

        $type = $this->request->type;
        $con_id = $this->request->con_id;
        $group_id = $this->request->group_id;
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        $status = $this->request->status;
        $type_id = $this->request->type_id;
        $except_ids = $this->request->except_ids;
        $is_review_score = $this->request->is_review_score;
        $is_add_ebook = $this->request->is_add_ebook;
        $is_violate = $this->request->is_violate;
        $is_show = $this->request->is_show;
        $except_database_id = $this->request->except_database_id;
        $score_manage_id = $this->request->score_manage_id;
        $score_manage_status = $this->request->score_manage_status;
        $sort = $this->request->input('sort', '1');
        $sort = $sort == 2 ? 'vote_num DESC,id DESC' : ($sort == 3 ? 'review_score DESC,id DESC' : 'id DESC'); //vote_num ASC,id ASC 解决点赞量全为0时，数据错乱问题

        $status = empty($status) ? [1, 2, 3] : [$status];
        $res = $this->model->lists($con_id, $type, $group_id, null, $type_id, $keywords,  $status, $except_ids, $is_review_score, $is_add_ebook, $is_violate, $is_show, $start_time, $end_time, $sort, $except_database_id, $score_manage_id, $score_manage_status, $limit);
        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }

        $competiteActivityGroupModel = new CompetiteActivityGroup();
        $competiteActivityWorksType = new CompetiteActivityWorksType();
        $userInfoModel = new UserInfo();
        $ids = [];
        foreach ($res['data'] as $key => $val) {
            $ids[] = $val['id'];
            $res['data'][$key][$this->list_index_key] = $this->addSerialNumberOne($key, $page, $limit);
            $res['data'][$key]['type_name'] = $competiteActivityWorksType->where('id', $val['type_id'])->value('type_name');

            if ($val['type'] == 1) {
                $user_info = $userInfoModel->getWechatField($val['user_id'], ['head_img', 'nickname']);
                $res['data'][$key]['head_img'] = $user_info['head_img'];
                $res['data'][$key]['nickname'] = $user_info['nickname'];
            } else {
                $group_info = $competiteActivityGroupModel->getGroupNameByGroupId($val['group_id'], ['name', 'img']);
                $res['data'][$key]['head_img'] = $group_info['img'];
                $res['data'][$key]['group_name'] = $group_info['name'];
            }

            //判断自己对当前作品的审核状态
            $res['data'][$key]['self_status'] = $this->model->getSelfCheckStatus($val['id'], $val);
            //判断自己对当前作品的评审打分状态
            $res['data'][$key]['review_score_status'] = $this->model->getSelfReviewScoreStatus($val['id'], $val);
            //是否可以一键拒绝  
            $res['data'][$key]['is_refuse_directly'] = $this->model->isRefuseDirectly($val);
        }

        $this->model->where('con_id', $con_id)->whereIn('id', $ids)->update(['is_look' => 1]);

        $total_vote_num = $res['total_vote_num'];
        $res = $this->disPageData($res);

        //获取总投票量
        // $res['total_vote_num'] = $this->model->getTotalVoteNumber($con_id);
        $res['total_vote_num'] = (string)$total_vote_num; //前端把int 0给屏蔽了

        return $this->returnApi(200, "查询成功", "YES", $res);
    }


    /**
     * 详情
     * @param id int 作品id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('production_detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->detail($this->request->id);
        if (empty($res)) {
            return $this->returnApi(203, "暂无数据");
        }
        $res = $this->getDetail($res);

        //是否可以一键拒绝  
        $res['is_refuse_directly'] = $this->model->isRefuseDirectly($res);
        return $this->returnApi(200, "获取成功", true, $res);
    }



    /**
     * 管理员自动获取下一个未审核作品
     * @param con_id int 大赛id
     * @param group_id int 团队id
     * @param type_id int 类型id
     * @param status int 是否审核 状态  1.已通过   2.未通过   3.未审核(默认)   不传为全部
     * @param keywords string 搜索关键词
     * @param start_time datetime 投稿时间(开始)
     * @param end_time datetime 投稿时间(截止)
     * @param sort int 排序方式  1 时间排序（默认）  2 投票排序  3 评分排序
     */
    public function getNextUnchecked()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('get_next_unchecked')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        //用于获取下一个作品
        $keywords = $this->request->keywords;
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        $con_id = $this->request->con_id;
        $type_id = $this->request->type_id;
        $group_id = $this->request->group_id;
        $sort = $this->request->input('sort', '1');
        $sort = $sort == 2 ? 'vote_num DESC,id ASC' : 'id ASC'; //vote_num ASC,id ASC 解决点赞量全为0时，数据错乱问题

        //获取下一个未审核的活动
        $res = $this->model->getNextUnchecked($con_id, $group_id, $type_id, $keywords, $start_time, $end_time, $sort);
        if (empty($res)) {
            return $this->returnApi(203, "暂无数据");
        }
        $res = $this->getDetail($res);

        return $this->returnApi(200, "获取成功", true, $res);
    }

    /**
     * 管理员自动获取下一个未打分作品
     * @param con_id int 大赛id
     * @param group_id int 团队id
     * @param type_id int 类型id
   //  * @param status int 是否审核 状态  1.已通过   2.未通过   3.未审核(默认)   不传为全部
     * @param keywords string 搜索关键词(作品名称|姓名|编号)
     * @param start_time datetime 投稿时间(开始)
     * @param end_time datetime 投稿时间(截止)
     * @param sort int 排序方式  1 时间排序（默认）  2 投票排序  3 评分排序
     */
    public function getNextUnScoreed()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('get_next_unscoreed')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        //用于获取下一个作品
        $keywords = $this->request->keywords;
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        $con_id = $this->request->con_id;
        $type_id = $this->request->type_id;
        $group_id = $this->request->group_id;
        $sort = $this->request->input('sort', '1');
        $sort = $sort == 2 ? 'vote_num DESC,id ASC' : 'id ASC'; //vote_num ASC,id ASC 解决点赞量全为0时，数据错乱问题

        //获取下一个未打分的活动
        $res = $this->model->getNextUnScoreed($con_id, $group_id, $type_id, $keywords, $start_time, $end_time, $sort);
        if (empty($res)) {
            return $this->returnApi(203, "暂无数据");
        }

        $res = $this->getDetail($res);

        return $this->returnApi(200, "获取成功", true, $res);
    }
    /**
     * 获取详情信息
     */
    public function getDetail($res)
    {
        $res['head_img'] = UserInfo::getWechatField($res['user_id'], 'head_img');

        $competiteActivityModel = new CompetiteActivity();
        $competite_activity_info = $competiteActivityModel->select('real_info', 'check_manage_id')->where('id', $res->con_id)->first();
        $real_info = $competite_activity_info['real_info'];
        $real_info_array = $competiteActivityModel->getCompetiteActivityApplyParam();

        $res->real_info_value = $this->getRealInfoArray($real_info, $real_info_array, 'value');
        $res->real_info_value = join("|", $res->real_info_value);

        $competiteActivityGroupModel = new CompetiteActivityGroup();
        $res['group_name'] = $res['group_id'] ? $competiteActivityGroupModel->getGroupNameByGroupId($res['group_id']) : '';
        $res = $this->disDataSameLevel($res->toArray(), 'con_type', ['type_name' => 'type_name']);

        //判断自己对当前作品的审核状态
        $res['self_status'] = $this->model->getSelfCheckStatus($res['id'], $res);
        $res['review_score_status'] = $this->model->getSelfReviewScoreStatus($res['id'], $res);

        $check_manage_id = explode(',', $competite_activity_info['check_manage_id']);
        $con_check_manage_id = array_column($res['con_check'], 'check_manage_id');
        foreach ($check_manage_id as $key => $val) {
            $res['con_check'][$key]['check_manage_id'] = $val;
            if (in_array($val, $con_check_manage_id)) {
                foreach ($res['con_check'] as $k => $v) {
                    if ($v['check_manage_id'] == $val) {
                        $res['con_check'][$key]['manage_name'] = $v[manage::$manage_name];
                        $res['con_check'][$key]['status'] = $v['status'];
                        $res['con_check'][$key]['reason'] = $v['reason'];
                        $res['con_check'][$key]['create_time'] = $v['create_time'];

                        unset($res['con_check'][$key]['account'], $res['con_check'][$key]['username'], $res['con_check'][$key]['pivot']);
                        break;
                    }
                }
            } else {
                $manage_name = Manage::getManageNameByManageId($val);
                $res['con_check'][$key]['manage_name'] = $manage_name;
                $res['con_check'][$key]['status'] = 3; //未审核
                $res['con_check'][$key]['reason'] = null; //未审核
                $res['con_check'][$key]['create_time'] = null; //未审核
            }
        }

        if ($res['type'] == 1) {
            $user_info = UserInfo::getWechatField($res['user_id'], ['head_img', 'nickname']);
            $res['head_img'] = $user_info['head_img'];
            $res['nickname'] = $user_info['nickname'];
        } else {
            $group_info = $competiteActivityGroupModel->getGroupNameByGroupId($res['group_id'], ['name', 'img']);
            $res['head_img'] = $group_info['img'];
            $res['group_name'] = $group_info['name'];
        }


        return $res;
    }


    /**
     * 审核作品
     * @param id int 作品id
     * @param status int 状态    1.通过   2.未通过
     * @param reason string 拒绝原因
     */
    public function worksCheck()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('works_check')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $id = $this->request->id;
        $status = $this->request->status;
        $reason = $this->request->reason ? $this->request->reason : null;

        if ($status == 2 && !$reason) {
            return $this->returnApi(202, '请填写拒绝原因');
        }

        $works = $this->model->find($id);
        $old_status =  $works->status;
        if (!$works) {
            return $this->returnApi(202, '参数传递错误');
        }
        if ($works->is_violate == 2) {
            return $this->returnApi(202, '当前作品已违规，无需审核');
        }
        if ($works->status != 3) {
            return $this->returnApi(202, '当前作品审核状态有误');
        }
        if ($works->next_check_manage_id != request()->manage_id) {
            return $this->returnApi(202, '您当前无权审核此作品');
        }

        DB::beginTransaction();
        try {
            /*消息推送*/
            $check_msg = $works->status == 1 ? '审核通过' : '审核未通过';
            if ($works->status == 1) {
                $system_id = $this->systemAdd('线上大赛：您投稿的作品' . $check_msg, $works->user_id, $works->account_id, 65, intval($works->id), '作品：【' . $works->title . '】' . $check_msg);
            } else {
                $system_id = $this->systemAdd('线上大赛：您投稿的作品' . $check_msg, $works->user_id, $works->account_id, 65, intval($works->id), '作品：【' . $works->title . '】' . $check_msg . '，拒绝理由为：' . $this->request->reason);
            }

            // /**执行积分规则 */
            // //如果现在状态不是待审核，都不管积分，积分统一采用审核时操作，
            if ($old_status == 3 && $status == 1) {
                // $scoreRuleObj = new ScoreRuleController();
                // $score_msg = $scoreRuleObj->getScoreMsg($works->score);
                // $scoreRuleObj->scoreReturn(7, $works->score, $works->user_id, $works->account_id, $check_msg . '，' . $score_msg . ' ' . abs($works->score) . ' 积分', $system_id);

                $is_reader = CompetiteActivity::where('id', $works->con_id)->value('is_reader');
                //团队账号不加分
                if (config('other.is_need_score') === true  && $is_reader == 1 && $works['type'] == 1) {
                    $scoreRuleObj = new ScoreRuleController();
                    $score_status = $scoreRuleObj->checkScoreStatus($this->score_type, $works->user_id, $works->account_id);
                    if ($score_status['code'] == 202 || $score_status['code'] == 203) throw new Exception($score_status['msg']);
                    if ($score_status['code'] == 200) {
                        $scoreRuleObj->scoreChange($score_status, $works->user_id, $works->account_id, $system_id); //添加积分消息
                    }
                }
            }
            if ($status == 1) {
                //修改为下一个审核人
                $check_status = $this->model->changeWorksCheckManage($works->con_id, $id, request()->manage_id);
                //如果还存在审核人，则不修改
                if ($check_status === null) {
                    $works->status = 1;

                    //审核通过，添加打分人id
                    //添加评审打分人信息
                    $competiteActivityModel = new CompetiteActivity();
                    $appraise_way = $competiteActivityModel->where('is_del', 1)->value('appraise_way');
                    if ($appraise_way != 2) {
                        $score_manage_id = CompetiteActivityWorksType::where('id', $works->type_id)->value('score_manage_id');
                        // $works->next_score_manage_id = $score_manage_id;

                        //多个打分人
                        $score_manage_id = explode(',', $score_manage_id);
                        //审批表新增数据
                        foreach ($score_manage_id as $key => $val) {
                            $competiteAcivityWorksScoreModel = new CompetiteActivityWorksScore();
                            $competiteAcivityWorksScoreModel->add([
                                'works_id' => $id,
                                'review_score' => null,
                                'score_manage_id' => $val,
                                'type' => 1, //系统自动分配 
                            ]);
                        }
                    }
                    $works->save();
                }
            } else {
                $works->reason = $reason;
                $works->status = 2;
                $works->save();
            }

            //添加审核记录
            $competiteActivityWorksCheckModel = new CompetiteActivityWorksCheck();
            $competiteActivityWorksCheckModel->addCheckLog(request()->manage_id, $id, $status, $reason);

            $check_msg = $status == 1 ? '已通过' : '已拒绝';

            DB::commit();
            return $this->returnApi(200, $check_msg, true);
        } catch (\Exception $e) {
            // 回滚事务
            DB::rollBack();
            return $this->returnApi(202, $e->getMessage());
        }
    }


    /**
     * 作品评审打分
     * @param id int 作品id
     * @param review_score int 分值  保留2位小数
     */
    public function worksReviewScore()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('works_review_score')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $id = $this->request->id;
        $review_score = $this->request->review_score;

        $works = $this->model->find($id);
        if (!$works) {
            return $this->returnApi(202, '参数传递错误');
        }
        if ($works->is_violate == 2) {
            return $this->returnApi(202, '当前作品已违规，无需打分');
        }
        //审批表新增数据
        $competiteAcivityWorksScoreModel = new CompetiteActivityWorksScore();
        $works_score = $competiteAcivityWorksScoreModel->where('works_id', $id)->where('score_manage_id', request()->manage_id)->first();
        if (empty($works_score)) {
            return $this->returnApi(202, '当前作品您无打分权限');
        }
        if (isset($works_score['review_score'])) {
            return $this->returnApi(202, '当前作品已打分，请勿重复打分');
        }

        DB::beginTransaction();
        try {
            $works_score->review_score = $review_score;
            $works_score->save();

            $total_review_score_list = $competiteAcivityWorksScoreModel->where('works_id', $id)->whereNotNull('review_score')->pluck('review_score')->toArray();
            $total_review_score = array_sum($total_review_score_list);
            $works->review_score = $total_review_score;
            $works->average_score = sprintf("%.2f", $total_review_score / count($total_review_score_list));
            $works->save();

            DB::commit();
            return $this->returnApi(200, '打分成功', true);
        } catch (\Exception $e) {
            // 回滚事务
            DB::rollBack();
            return $this->returnApi(202, $e->getMessage());
        }
    }

    /**
     * 违规 与 取消违规操作
     * @param id int 申请id
     * @param reason string 违规原因
     */
    public function violateAndCancel()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('violate_and_cancel')->check($this->request->all())) {
            return $this->returnApi(201, $this->validate->getError());
        }

        // 启动事务
        DB::beginTransaction();
        try {
            $res = $this->model->violateAndCancel($this->request->id, $this->request->reason);

            /*消息推送*/
            if ($res->is_violate == 2) {
                $system_id = $this->systemAdd('在线投票作品已违规', $res->user_id, $res->account_id, 66, $res->id, '您发布的在线投票作品：【' . $res->title . '】已违规，违规理由为：' . $this->request->reason);

                // if (!empty($res->score)) {
                //     $score_rule = new ScoreRuleController();
                //     $score_msg = $score_rule->getScoreMsg($res->score);
                //     $score_rule->scoreReturn($this->score_type, $res->score, $res->user_id, $res->account_id, '在线投票作品已违规，' . $score_msg . ' ' . abs($res->score) . ' 积分', $system_id);
                // }
            } else {
                $system_id = $this->systemAdd('在线投票作品已取消违规', $res->user_id, $res->account_id, 66, $res->id, '您发布的在线投票作品：【' . $res->title . '】已取消违规');

                // if (!empty($res->score)) {
                //     $score_rule = new ScoreRuleController();
                //     $score_msg = $score_rule->getScoreMsg(-$res->score); //相反积分
                //     $score_rule->scoreReturn($this->score_type, $res->score, $res->user_id, $res->account_id, '在线投票作品已取消违规，' . $score_msg . ' ' . abs($res->score) . ' 积分', $system_id);
                // }
            }

            // 提交事务
            DB::commit();
            return $this->returnApi(200, "操作成功", true);
        } catch (\Exception $e) {
            // 回滚事务
            DB::rollBack();
            return $this->returnApi(202, $e->getMessage());
        }
    }

    /**
     * 单个下载作品
     * @param id int 作品id
     */
    public function downloadWorks()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('production_list_download_img')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $id = $this->request->id;
        $field = [
            'id', 'con_id', 'serial_number', 'img', 'title', 'content', 'voice', 'voice_name', 'video', 'video_name', 'intro', 'pdf_works_address', 'authorizations_address', 'promise_address'
        ];
        $res = $this->model->detail($id, $field);
        if (empty($res)) {
            return $this->returnApi(203, "暂无数据");
        }
        $res = $res->toArray();
        $zipFileDownloadObj = new ZipFileDownloadController();
        $file_name = CompetiteActivity::where('id', $res['con_id'])->value('title');
        //图片作品生成pdf文件
        $path_arr[] = $this->model->disData($res, $file_name);
        $zip_path = $zipFileDownloadObj->addPackage($path_arr, null, $res['serial_number'] . '-征集活动作品下载-' . date('Y-m-d'), 10); //缓存时间长了，筛选数据还是之前的
        return response()->download($zip_path);
    }

    /**
     * 批量下载作品
     * @param con_id int 大赛id
     * @param group_id int 大赛团队id  
     * @param status int 是否审核 状态  1.已通过   2.未通过   3.未审核(默认)   不传为全部
     * @param type_id int 类型id
     * @param keywords string 搜索关键词(作品名称|姓名|编号)
     * @param start_time datetime 投稿时间(开始)
     * @param end_time datetime 投稿时间(截止)
     * @param is_review_score string 是否评审打分  1 已打分  2 未打分
     * @param is_add_ebook string 是否是加入电子书 1 是 
     * @param is_violate string 是否违规  1正常 2违规 
     * @param is_show string 是否显示  1显示 2不显示 
     */
    public function downloadWorksAll()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('production_list_download_img_all')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $con_id = $this->request->con_id;
        $group_id = $this->request->group_id;
        $keywords = $this->request->keywords;
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        $status = $this->request->status;
        $type_id = $this->request->type_id;
        $is_review_score = $this->request->is_review_score;
        $is_add_ebook = $this->request->is_add_ebook;
        $is_violate = $this->request->is_violate;
        $is_show = $this->request->is_show;

        $status = empty($status) ? [1, 2, 3] : [$status];
        $res = $this->model->lists($con_id, null, $group_id, null, $type_id, $keywords,  $status, '', $is_review_score, $is_add_ebook, $is_violate, $is_show, $start_time, $end_time, 'id desc', null, null, null, 99999);
        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }

        $zipFileDownloadObj = new ZipFileDownloadController();
        $path_arr = [];
        $file_name = CompetiteActivity::where('id', $con_id)->value('title');
        //图片作品生成pdf文件
        foreach ($res['data'] as $key => $val) {
            $path_arr[$key] = $this->model->disData($val, $file_name);
        }
        $zip_path = $zipFileDownloadObj->addPackage($path_arr, null, $file_name . '-征集活动作品下载-' . date('Y-m-d'), 10); //缓存时间长了，筛选数据还是之前的
        return response()->download($zip_path);
    }

    /**
     * 显示与取消显示
     * @param ids 作品id，多个用逗号拼接   all 是，其余字符  ids 必传
     * @param is_show 是否显示  1.显示  2.未显示  默认2
     */
    public function showAndCancel()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('show_and_cancel')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        list($code, $msg) = $this->model->resumeAndCancel($this->request->ids, 'is_show', $this->request->is_show, null);

        $is_show = $this->request->is_show == 1 ? '显示' : '取消显示';
        if ($code === 200) {
            return $this->returnApi(200, $is_show . $msg, true);
        }
        return $this->returnApi($code, $is_show . $msg);
    }

    /**
     * 一键拒绝作品
     * @param id 作品id
     * @param reason 拒绝理由
     */
    public function worksRefuseDirectly()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('works_refuse_directly')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $res = $this->model->where('id', $this->request->id)->first();
        if (empty($res) || !$this->model->isRefuseDirectly($res)) {
            return $this->returnApi(202, '当前状态不允许拒绝');
        }

        DB::beginTransaction();
        try {
            $res->status = 2; //直接改为拒绝
            $res->reason = $this->request->reason; //拒绝理由
            $res->save();

            $competiteActivityWorksCheckModel = new CompetiteActivityWorksCheck();
            $competiteActivityWorksCheckModel->add([
                'works_id' => $this->request->id,
                'check_manage_id' => request()->manage_id,
                'status' => 2,
                'reason' => $this->request->reason,
            ]);
            DB::commit();
            return $this->returnApi(200, "拒绝成功", true);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, $e->getMessage());
        }
    }

    /**
     * 删除作品
     * @param id 作品id
     */
    public function worksDelete()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('works_delete')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $res = $this->model->where('id', $this->request->id)->first();
        if (empty($res) || $res->status != 2) {
            return $this->returnApi(202, '当前状态不允许删除');
        }
        $res->status = 5;
        $result = $res->save();

        if ($result) {
            return $this->returnApi(200, '删除成功', true);
        }
        return $this->returnApi(202, '删除失败');
    }
}
