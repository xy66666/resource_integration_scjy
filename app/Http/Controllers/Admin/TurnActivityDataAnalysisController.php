<?php

namespace App\Http\Controllers\Admin;

use App\Models\TurnActivity;
use App\Models\TurnActivityBrowseCount;
use App\Models\TurnActivityGift;
use App\Models\TurnActivityUnit;
use App\Models\TurnActivityUserGift;
use App\Models\TurnActivityUserUnit;
use App\Models\TurnActivityUserUsePrizeRecord;
use App\Validate\TurnActivityDataAnalysisValidate;


/**
 * 线上答题活动 数据分析
 * Class TurnType
 * @package app\admin\controller
 */
class TurnActivityDataAnalysisController extends CommonController
{
    public $model = null;
    public $validate = null;
    public $worksModel = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new TurnActivity();
        $this->validate = new TurnActivityDataAnalysisValidate();
    }


    /**
     * 数据统计
     * @param act_id int 活动id  
     * @param start_time datetime 比赛开始时间(开始)
     * @param end_time datetime 比赛开始时间(截止)
     */
    public function dataStatistics()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('statistics')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $act_id = request()->act_id;
        $start_time = request()->start_time;
        $end_time = request()->end_time;

        if (empty($start_time) || empty($end_time)) {
            $start_time = date('Y-m-d 00:00:00');
            $end_time = date('Y-m-d H:i:s');
        }

        $act_info = $this->model->detail($act_id, ['id'], null);

        if (empty($act_info)) {
            return $this->returnApi(201, '参数错误');
        }

        //获取活动总参与人次
        $turnActivityUserUsePrizeRecordModel = new TurnActivityUserUsePrizeRecord();
        $part_number = $turnActivityUserUsePrizeRecordModel->partNumberStatistics($act_id, $start_time, $end_time, 1);
 
        //抽奖次数
        $draw_number = $turnActivityUserUsePrizeRecordModel->partNumberStatistics($act_id, $start_time, $end_time, 2);

        //总点击量
        $turnActivityBrowseCountModel = new TurnActivityBrowseCount();
        $browse_number = $turnActivityBrowseCountModel->getBrowseNumber($act_id, $start_time, $end_time);

        $data = [
            'part_number' => $part_number,
            'draw_number' => $draw_number,
            'browse_number' => $browse_number,
        ];

        //礼品发送数量
        $turnActivityUserGiftModel = new TurnActivityUserGift();
        $turn_activity_gift_type = config('other.turn_activity_gift_type');
        if (in_array(2, $turn_activity_gift_type)) {
            $red_number = $turnActivityUserGiftModel->getWinNumber($act_id, null, 1, null,null, $start_time, $end_time); //红包发放数量
        } else {
            $red_number = null;
        }
        if (in_array(1, $turn_activity_gift_type)) {
            $gift_number = $turnActivityUserGiftModel->getWinNumber($act_id, null, 2, null,null, $start_time, $end_time); //礼物发放数量
        } else {
            $gift_number = null;
        }
 
        $data['gift_number'] = $gift_number;
        $data['red_number'] = $red_number;


        return $this->returnApi(200, "获取成功", true, $data);
    }

    /**
     * 抽奖次数统计
     * @param act_id int 活动id  
     * @param start_time datetime 比赛开始时间(开始)
     * @param end_time datetime 比赛开始时间(截止)
     */
    public function drawStatistics()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('statistics')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $act_id = request()->act_id;
        $start_time = request()->start_time;
        $end_time = request()->end_time;

        if (empty($start_time) || empty($end_time)) {
            $start_time = date('Y-m-d 00:00:00');
            $end_time = date('Y-m-d H:i:s');
        }

        $act_info = $this->model->detail($act_id, ['id'], null);

        if (empty($act_info)) {
            return $this->returnApi(201, '参数错误');
        }

        $turnActivityUserUsePrizeRecordModel = new TurnActivityUserUsePrizeRecord();

        if (empty($start_time)) {
            $start_time = date('Y-m-d');
            $end_time = date('Y-m-d');
        }
        $res = $turnActivityUserUsePrizeRecordModel->partStatistics($act_id, $start_time, $end_time);
        $res = self::disCartogramDayOrHourData($res, null, $start_time, $end_time, 'times');

        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 礼物发放统计
     * @param act_id int 活动id  
     * @param start_time datetime 比赛开始时间(开始)
     * @param end_time datetime 比赛开始时间(截止)
     */
    public function giftGrantStatistics()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('statistics')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $act_id = request()->act_id;
        $start_time = request()->start_time;
        $end_time = request()->end_time;

        if (empty($start_time) || empty($end_time)) {
            $start_time = date('Y-m-d 00:00:00');
            $end_time = date('Y-m-d H:i:s');
        }

        $act_info = $this->model->detail($act_id, ['id'], null);

        if (empty($act_info)) {
            return $this->returnApi(201, '参数错误');
        }

        $turnActivityUserGiftModel = new TurnActivityUserGift();
        $gift_number = $turnActivityUserGiftModel->giftStatistics($act_id, 2, $start_time, $end_time); //礼物统计
        $red_number = $turnActivityUserGiftModel->giftStatistics($act_id, 1, $start_time, $end_time); //红包统计

        $gift_number = self::disCartogramDayOrHourData($gift_number, null, $start_time, $end_time, 'times','count');
        $red_number = self::disCartogramDayOrHourData($red_number, null, $start_time, $end_time, 'times','count');

        return $this->returnApi(200, "查询成功", true, [
            'gift_number' => $gift_number,
            'red_number' => $red_number,
        ]);
    }

    /**
     * 系统投放礼物数据
     * @param act_id int 活动id  
     */
    public function systemGiftGrantStatistics()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('statistics')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $act_id = request()->act_id;
        $act_info = $this->model->detail($act_id, ['id'], null);

        if (empty($act_info)) {
            return $this->returnApi(201, '参数错误');
        }

        //投放礼物数量
        $turnActivityGiftModel = new TurnActivityGift();
        $gift_number = $turnActivityGiftModel->getGiftNumber($act_id, 1, 0);
        if ($gift_number) {
            $res['gift_total_number'] = $gift_number[0]['total_number'];
            $res['gift_use_number'] = $gift_number[0]['use_number'];
        } else {
            $res['gift_total_number'] = 0;
            $res['gift_use_number'] = 0;
        }
        //投放红包数量
        $gift_number = $turnActivityGiftModel->getGiftNumber($act_id, 2, 0);
        if ($gift_number) {
            $res['red_total_number'] = $gift_number[0]['total_number'];
            $res['red_use_number'] = $gift_number[0]['use_number'];
        } else {
            $res['red_total_number'] = 0;
            $res['red_use_number'] = 0;
        }

        return $this->returnApi(200, "查询成功", true, $res);
    }
}
