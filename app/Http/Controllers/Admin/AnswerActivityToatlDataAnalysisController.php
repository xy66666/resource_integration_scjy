<?php

namespace App\Http\Controllers\Admin;

use App\Models\AnswerActivity;
use App\Models\AnswerActivityBrowseCount;
use App\Models\AnswerActivityGift;
use App\Models\AnswerActivityUnit;
use App\Models\AnswerActivityUserGift;
use App\Models\AnswerActivityUserUnit;
use App\Validate\AnswerActivityDataAnalysisValidate;


/**
 * 在线答题活动 数据总分析
 * Class AnswerType
 * @package app\admin\controller
 */
class AnswerActivityToatlDataAnalysisController extends CommonController
{
    public $model = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new AnswerActivity();
    }


    /**
     * 答题总数据统计
     */
    public function answerDataStatistics()
    {

        $part_number = 0;
        $answer_number = 0;
        $browse_number = 0;
        for ($i = 1; $i <= 3; $i++) {
            $model = $this->model->getAnswerRecordModel($i);
            //获取活动总参与人次
            $part_number += $model->partNumberStatistics(null, null, null, null, 1);
            //总答题次数
            $answer_number += $model->partNumberStatistics(null, null, null, null, 2);
            //总点击量
            $answerActivityBrowseCountModel = new AnswerActivityBrowseCount();
            $browse_number += $answerActivityBrowseCountModel->getBrowseNumber(null, null, null, null);
        }

        $data = [
            'part_number' => $part_number,
            'answer_number' => $answer_number,
            'browse_number' => $browse_number,
        ];

        //礼品发送数量
        $answerActivityUserGiftModel = new AnswerActivityUserGift();
        $answer_activity_gift_type = config('other.answer_activity_gift_type');
        if (in_array(1, $answer_activity_gift_type)) {
            $gift_number = $answerActivityUserGiftModel->getWinNumber(null, null, 2); //礼物发放数量
        } else {
            $gift_number = 0; //礼物发放数量
        }
        if (in_array(2, $answer_activity_gift_type)) {
            $red_number = $answerActivityUserGiftModel->getWinNumber(null, null, 1); //红包发放数量
        } else {
            $red_number = 0; //礼物发放数量
        }

        $data['gift_number'] = $gift_number;
        $data['red_number'] = $red_number;


        return $this->returnApi(200, "获取成功", true, $data);
    }

    /**
     * 答题次数总统计
     * @param year   年  例如  2022   2021
     * @param month  月份  1,2,3,4,5   默认 空 按年筛选
     */
    public function answerTimeStatistics()
    {

        $year = request()->year;
        $month = request()->month;

        if (empty($year)) {
            $year = date('Y');
        }

        list($start_time, $end_time) = get_start_end_time_by_year_month($month, $year);

        $res1 = [];
        $res2 = [];
        $res3 = [];
        for ($i = 1; $i <= 3; $i++) {
            $model = $this->model->getAnswerRecordModel($i);

            ${'res' . $i} = $model->partStatistics(null, null, $start_time, $end_time, empty($month) ? true : false);
            ${'res' . $i} = self::disCartogramDayOrHourData(${'res' . $i}, empty($month) ? $year : null, $start_time, $end_time, 'dates');
        }

        foreach ($res1['count'] as $key => $val) {
            $res1['count'][$key] = $res1['count'][$key] + $res2['count'][$key] + $res3['count'][$key];
        }

        return $this->returnApi(200, "查询成功", true, $res1);
    }

    /**
     * 最新d答题活动排行榜
     */
    public function answerDataRanking()
    {
        $data = $this->model->select('id', 'title', 'browse_num', 'pattern')->where('is_del', 1)->orderByDesc('id')->limit(10)->get()->toArray();
        foreach ($data as $key => $val) {
            $model = $this->model->getAnswerRecordModel($val['pattern']);

            $data[$key]['part_number'] = $model->partNumberStatistics($val['id'], null, null, null, 1);
            $data[$key]['answer_number'] = $model->partNumberStatistics($val['id'], null, null, null, 2);
        }

        return $this->returnApi(200, "获取成功", true, $data);
    }
}
