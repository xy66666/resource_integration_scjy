<?php

namespace App\Http\Controllers\Admin;

use App\Models\ServiceTime;
use App\Validate\ServiceTimeValidate;

/**
 * 服务时间
 */
class ServiceTimeController extends CommonController
{

    public $model = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new ServiceTime();
        $this->validate = new ServiceTimeValidate();
    }

    /**
     * 类型(用于下拉框选择)
     */
    public function filterList()
    {

        $condition[] = ['is_del', '=', 1];

        return $this->model->getFilterList(['id', 'times'], $condition);
    }

    /**
     * 列表
     * @param page int 当前页
     * @param limit int 分页大小
     * @param keywords string 搜索关键词(类型名称)
     */
    public function lists()
    {
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;

        $condition[] = ['is_del', '=', 1];

        if ($keywords) {
            $condition[] = ['times', 'like', "%$keywords%"];
        }

        return $this->model->getSimpleList(['id', 'times', 'create_time'], $condition, $page, $limit);
    }

    /**
     * 详情
     * @param id int 类型id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->select('id' , 'times' , 'create_time')->find($this->request->id);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }

        return $this->returnApi(200, "获取成功", true, $res->toArray());
    }

    /**
     * 新增
     * @param times string 类型名称
     */
    public function add()
    {
          //增加验证场景进行验证
          if (!$this->validate->scene('add')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $is_exists = $this->model->nameIsExists($this->request->times , 'times');

        if ($is_exists) {
            return $this->returnApi(202, "该时间已存在");
        }

        $res = $this->model->add($this->request->all());

        if (!$res) {
            return $this->returnApi(202, "新增失败");
        }
        return $this->returnApi(200, "新增成功", true);
    }

    /**
     * 修改
     * @param id int 类型id
     * @param times string 类型名称
     */
    public function change()
    {
         //增加验证场景进行验证
         if (!$this->validate->scene('change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $is_exists = $this->model->nameIsExists($this->request->times , 'times' , $this->request->id);

        if ($is_exists) {
            return $this->returnApi(202, "该时间已存在");
        }

        $res = $this->model->change($this->request->all());

        if (!$res) {
            return $this->returnApi(202, "修改失败");
        }
        return $this->returnApi(200, "修改成功", true);
    }

    /**
     * 删除
     * @param id int 类型id
     */
    public function del()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->del($this->request->id);

        if ($res === true) {
            return $this->returnApi(200, "删除成功", true);
        }
        return $this->returnApi(202, $res);
    }



}
