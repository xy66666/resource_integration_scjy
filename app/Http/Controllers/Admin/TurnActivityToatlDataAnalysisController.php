<?php

namespace App\Http\Controllers\Admin;

use App\Models\TurnActivity;
use App\Models\TurnActivityBrowseCount;
use App\Models\TurnActivityUserGift;
use App\Models\TurnActivityUserUsePrizeRecord;

/**
 * 在线抽奖活动 数据总分析
 * Class AnswerType
 * @package app\admin\controller
 */
class TurnActivityToatlDataAnalysisController extends CommonController
{
    public $model = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new TurnActivity();
    }


    /**
     * 翻奖总数据统计
     */
    public function turnDataStatistics()
    {

        //获取活动总参与人次
        $turnActivityUserUsePrizeRecordModel = new TurnActivityUserUsePrizeRecord();
        $part_number = $turnActivityUserUsePrizeRecordModel->partNumberStatistics(null, null, null, 1);
        //抽奖次数
        $draw_number = $turnActivityUserUsePrizeRecordModel->partNumberStatistics(null, null, null, 2);
        //总点击量
        $turnActivityBrowseCountModel = new TurnActivityBrowseCount();
        $browse_number = $turnActivityBrowseCountModel->getBrowseNumber(null, null, null);

        $data = [
            'part_number' => $part_number,
            'draw_number' => $draw_number,
            'browse_number' => $browse_number,
        ];

        //礼品发送数量
        $turnActivityUserGiftModel = new TurnActivityUserGift();
        $turn_activity_gift_type = config('other.turn_activity_gift_type');
        if (in_array(1, $turn_activity_gift_type)) {
            $gift_number = $turnActivityUserGiftModel->getWinNumber(null, null, 2); //礼物发放数量
        } else {
            $gift_number = 0;
        }
        if (in_array(2, $turn_activity_gift_type)) {
            $red_number = $turnActivityUserGiftModel->getWinNumber(null, null, 1); //红包发放数量
        } else {
            $red_number = 0;
        }
        $data['gift_number'] = $gift_number;
        $data['red_number'] = $red_number;

        return $this->returnApi(200, "获取成功", true, $data);
    }

    /**
     * 翻奖次数总统计
     * @param year   年  例如  2022   2021
     * @param month  月份  1,2,3,4,5   默认 空 按年筛选
     */
    public function turnTimeStatistics()
    {

        $year = request()->year;
        $month = request()->month;

        if (empty($year)) {
            $year = date('Y');
        }

        list($start_time, $end_time) = get_start_end_time_by_year_month($month, $year);

        $turnActivityUserUsePrizeRecordModel = new TurnActivityUserUsePrizeRecord();

        $res = $turnActivityUserUsePrizeRecordModel->partStatistics(null, $start_time, $end_time, empty($month) ? true : false);
        $res = self::disCartogramDayOrHourData($res, empty($month) ? $year : null, $start_time, $end_time, 'times');

        return $this->returnApi(200, "查询成功", true, $res);
    }

    
    /**
     * 最新活动排行榜
     */
    public function turnDataRanking()
    {
        $data = $this->model->select('id' , 'title' ,'browse_num')->where('is_del' , 1)->orderByDesc('id')->limit(10)->get()->toArray();

        $turnActivityUserUsePrizeRecordModel = new TurnActivityUserUsePrizeRecord();
        foreach($data as $key=>$val){
            $data[$key]['part_number'] = $turnActivityUserUsePrizeRecordModel->partNumberStatistics($val['id'], null, null, 1);//答题次数
            $data[$key]['draw_number'] = $turnActivityUserUsePrizeRecordModel->partNumberStatistics($val['id'], null, null, 2);//抽奖次数
        }
       
        return $this->returnApi(200, "获取成功", true, $data);
    }

}
