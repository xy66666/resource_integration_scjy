<?php

namespace App\Http\Controllers\Admin;

use App\Models\ContestActivityUnit;
use App\Validate\ContestActivityUnitValidate;
use Illuminate\Support\Facades\DB;

/**
 * 大赛单位管理
 */
class ContestActivityUnitController extends CommonController
{

    public $model = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new ContestActivityUnit();
        $this->validate = new ContestActivityUnitValidate();
    }


    
    /**
     * 获取单位性质
     */
    public function getNatureName()
    {
        $data = $this->model->getNatureName();
        $config_node = config('other.contest_unit_nature');
 
        $new_data = [];
        $i = 0;
        foreach($data as $key=>$val){
            if(in_array($key , $config_node)){
                $new_data[$i]['id'] = $key;
                $new_data[$i]['name'] = $val;
                $i++;
            }
        }

        if ($new_data) {
            return $this->returnApi(200, "获取成功", true,$new_data);
        }
        return $this->returnApi(203, "暂无数据");
    }


    /**
     * 列表(用于下拉框选择)
     * @param con_id 大赛id
     */
    public function filterList()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('filter_list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        //判断是否是单位联盟大赛
        $isAllowAddUnit = $this->model->isAllowAddUnit($this->request->con_id);
        if ($isAllowAddUnit !== true) {
            return $this->returnApi(202, $isAllowAddUnit);
        }

        $condition[] = ['is_del', '=', 1];
        $condition[] = ['con_id', '=', $this->request->con_id];

        return $this->model->getFilterList(['id', 'con_id', 'name', 'img','nature'], $condition);
    }

    /**
     * 列表
     * @param con_id int 大赛id
     * @param page int 当前页
     * @param limit int 分页大小
     * @param nature int 单位性质   1 图书馆  2 文化馆  3 博物馆
     * @param start_time datetime 创建时间范围搜索(开始) 
     * @param end_time datetime 创建时间范围搜索(结束) 
     * @param keywords string 搜索关键词(类型名称)
     */
    public function lists()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        $nature = $this->request->nature;
        $keywords = $this->request->keywords;
        $con_id = $this->request->con_id;

        //判断是否是单位联盟大赛
        $isAllowAddUnit = $this->model->isAllowAddUnit($this->request->con_id);
        if ($isAllowAddUnit !== true) {
            return $this->returnApi(202, $isAllowAddUnit);
        }

        $res = $this->model->lists(null,$con_id,$keywords,$nature,$start_time,$end_time,$page,$limit);
           
        if ($res['total'] == 0 || !$res['data']) {
            return $this->returnApi(203, "暂无数据");
        }

        foreach ($res['data'] as $key => $val) {
            $res['data'][$key]['intro'] = strip_tags($val['intro']);
            $res['data'][$key]['nature_name'] = $this->model->getNatureName($val['nature']);
            $res['data'][$key][$this->list_index_key] = $this->addSerialNumberOne($key, $page, $limit);
        }

        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 详情
     * @param id int 类型id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->detail($this->request->id);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }

        return $this->returnApi(200, "获取成功", true, $res->toArray());
    }

    /**
     * 新增
     * @param con_id int大赛id
     * @param nature int单位性质   1 图书馆  2 文化馆  3 博物馆
     * @param name string 名称   最多 3个字符
     * @param img string 图片
     * @param intro string 简介
     * @param tel string 联系电话
     * @param contacts string 联系人
     * @param originals string 原创申明  可为空，空则用大赛设置的申明
     */
    public function add()
    {   
        //增加验证场景进行验证
        if (!$this->validate->scene('add')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        //判断是否是单位联盟大赛
        $isAllowAddUnit = $this->model->isAllowAddUnit($this->request->con_id);
        if ($isAllowAddUnit !== true) {
            return $this->returnApi(202, $isAllowAddUnit);
        }

        $condition[] = ['con_id', '=', $this->request->con_id];
        $is_exists = $this->model->nameIsExists($this->request->name, 'name', null, 1, $condition);
        if ($is_exists) {
            return $this->returnApi(202, "此名称已存在");
        }
        $data = $this->request->all();
        $data['letter'] = get_first_charter($this->request->name);
        $res = $this->model->add($data);

        if (!$res) {
            return $this->returnApi(202, "新增失败");
        }
        return $this->returnApi(200, "新增成功", true);
    }

    /**
     * 修改
     * @param id int 单位id
     * @param nature int单位性质   1 图书馆  2 文化馆  3 博物馆
     * @param name string 名称   最多 3个字符
     * @param img string 图片
     * @param intro string 简介
     * @param tel string 联系电话
     * @param contacts string 联系人
     * @param originals string 原创申明  可为空，空则用大赛设置的申明
     */
    public function change()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $result = $this->model->where('id', $this->request->id)->first();
        if (empty($result)) {
            return $this->returnApi(201, "参数传递错误");
        }


        //判断是否是单位联盟大赛
        $isAllowAddUnit = $this->model->isAllowAddUnit($result->con_id);
        if ($isAllowAddUnit !== true) {
            return $this->returnApi(202, $isAllowAddUnit);
        }

        $condition[] = ['con_id', '=', $result->con_id];
        $is_exists = $this->model->nameIsExists($this->request->name, 'name', $this->request->id, 1, $condition);
        if ($is_exists) {
            return $this->returnApi(202, "此名称已存在");
        }

        $data = $this->request->all();
        $data['letter'] = get_first_charter($this->request->name);
        $res = $this->model->change($data);

        if (!$res) {
            return $this->returnApi(202, "修改失败");
        }
        return $this->returnApi(200, "修改成功", true);
    }

    /**
     * 删除
     * @param id int 类型id
     */
    public function del()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->del($this->request->id);

        if ($res === true) {
            return $this->returnApi(200, "删除成功", true);
        }
        return $this->returnApi(202, $res);
    }

    /**
     * 修改单位排序
     * @param orders 排序   json格式数据  里面2个参数  id 单位id   order 序号
     * 
     * json 格式数据  [{"id":1,"order":1},{"id":2,"order":2},{"id":3,"order":3}]
     * 数组 格式数据  [['id'=>1 , 'order'=>1] , ['id'=>2 , 'order'=>2] , ['id'=>3 , 'order'=>3]]
     */
    public function orderChange()
    {
        if (!$this->validate->scene('order')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $orders = $this->request->orders;
        DB::beginTransaction();
        try {
            $this->model->orderChange($orders);
            DB::commit();
            return $this->returnApi(200, '修改成功', true);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, '修改失败');
        }
    }
}
