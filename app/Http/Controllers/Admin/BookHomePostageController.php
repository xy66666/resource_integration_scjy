<?php

namespace App\Http\Controllers\Admin;

use App\Models\BookHomeOrder;
use App\Validate\BookHomeNewBookValidate;
use Illuminate\Support\Facades\DB;

/**
 * 邮费统计  书店为确认方，图书馆为 发起方
 * Class Postage
 * @package app\api\controller
 */
class BookHomePostageController extends CommonController
{

    private $model = null;
    private $validate = null;

    public function __construct()
    {
        parent::__construct();
        $this->model = new BookHomeOrder();
        $this->validate = new BookHomeNewBookValidate();
    }

    /**
     *（发起方、确认方） 邮费统计列表
     * @param shop_id 书店id 默认全部
     * @param type	  类型 0、全部 1 新书（默认）   2 馆藏书 （如果type为2，shop_id无效，前端变为不可选状态）
     * @param keywords_type  检索条件   0、全部  1、读者证号  2、订单号 3 快递单号 默认 0
     * @param keywords  检索条件
     * @param start_time 发货开始时间   数据格式    2020-05-12 12:00:00
     * @param end_time 发货结束时间     数据格式    2020-05-12 12:00:00
     * @param settle_state		结算状态  0、全部 1 已结算  2 结算中  3 未结算
     * @param way		邮费支付方式  0、全部 1 微信支付 2 系统预算扣除
     * @param page  页数，默认为1
     * @param limit  条数，默认显示 10条
     */
    public function lists()
    {
        $shop_all_id = $this->getAdminShopIdAll();
        // if (empty($shop_all_id)) {
        //     return $this->returnApi(202, '您无权操作此书店'); //无权限访问
        // }
        $shop_id = $this->request->shop_id;
        $keywords_type = $this->request->keywords_type;
        $keywords = $this->request->keywords;
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        $settle_state = $this->request->settle_state;
        $way = $this->request->way;
        $type = $this->request->type;
        $page = intval($this->request->page, 1) ?: 1;
        $limit = intval($this->request->limit, 10) ?: 10;

        $param = [
            'shop_id' => $shop_id,
            'keywords_type' => $keywords_type,
            'keywords' => $keywords,
            'start_time' => $start_time,
            'end_time' => $end_time,
            'settle_state' => $settle_state,
            'way' => $way,
            'type' => $type,
            'shop_all_id' => $shop_all_id,
            'page' => $page,
            'limit' => $limit,
            'select_way' => 1,
        ];
        //列表
        $res = $this->model->sponsorPostageList($param);

        if ($res['data']) {
            return $this->returnApi(200, '获取成功', true, $res);
        }
        return $this->returnApi(203, '暂无数据');
    }


    /**
     * (发起方、确认方)邮费统计 金额及书籍本数 统计
     * @param shop_id 书店id 默认全部
     * @param type	  类型 0、全部 1 新书（默认）   2 馆藏书 （如果type为2，shop_id无效，前端变为不可选状态）
     * @param keywords_type  检索条件   0、全部  1、读者证号  2、订单号 3 快递单号 默认 0
     * @param keywords  检索条件
     * @param start_time 发货开始时间   数据格式    2020-05-12 12:00:00
     * @param end_time 发货结束时间     数据格式    2020-05-12 12:00:00
    // * @param settle_state		结算状态  0、全部 1 已结算  2 结算中  3 未结算
     * @param way		邮费支付方式  0、全部 1 微信支付 2 系统预算扣除
     */
    public function statistics()
    {
        $shop_all_id = $this->getAdminShopIdAll();
        // if (empty($shop_all_id)) {
        //     return $this->returnApi(202, '您无权操作此书店'); //无权限访问
        // }
        $shop_id = $this->request->shop_id;
        $keywords_type = $this->request->keywords_type;
        $keywords = $this->request->keywords;
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        //   $settle_state = $this->request->settle_state;
        $way = $this->request->way;
        $type = $this->request->type;


        $param = [
            'shop_id' => $shop_id,
            'keywords_type' => $keywords_type,
            'keywords' => $keywords,
            'start_time' => $start_time,
            'end_time' => $end_time,
            //     'settle_state' => $settle_state,
            'way' => $way,
            'type' => $type,
            'shop_all_id' => $shop_all_id,
        ];


        // 1 已结算
        $param['settle_state'] = 1;
        $param['select_way'] = 4;
        $res['settled_number'] = $this->model->sponsorPostageList($param);

        $param['select_way'] = 3;
        $res['settled_money'] = $this->model->sponsorPostageList($param);
        // 1 结算中
        $param['settle_state'] = 2;
        $param['select_way'] = 4;
        $res['settling_number'] = $this->model->sponsorPostageList($param);
        $param['select_way'] = 3;
        $res['settling_money'] = $this->model->sponsorPostageList($param);

        // 1 未结算
        $param['settle_state'] = 3;
        $param['select_way'] = 4;
        $res['settlno_number'] = $this->model->sponsorPostageList($param);
        $param['select_way'] = 3;
        $res['settlno_money'] = $this->model->sponsorPostageList($param);


        return $this->returnApi(200, '获取成功', true, $res);
    }


    /**
     * (发起方)标记邮费状态为结算中
     * @param order_ids 多个 , 逗号拼接
     */
    public function sponsorSettleState()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('sponsor_settle_state')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
     //   $shop_all_id = $this->getAdminShopIdAll();

        $order_ids = $this->request->order_ids;
        $order_ids = explode(',', $order_ids);
        $order_ids = array_unique(array_filter($order_ids));

        DB::beginTransaction();
        try {
            $this->model->where('settle_state', 3)
            //    ->whereIn('shop_id', $shop_all_id)//新书和馆藏书同时都可以操作
                ->whereIn('id', $order_ids)
                ->update([
                    'settle_state' => 2,
                    'settle_sponsor_time' => date('Y-m-d H:i:s'),
                    'settle_sponsor_manage_id' => request()->manage_id,
                ]);
            DB::commit();
            return $this->returnApi(200, '操作成功', true);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, '操作失败');
        }
    }

    /**
     * （确认方）标记邮费状态为已结算 
     * @param order_ids 多个 , 逗号拼接
     */
    public function affirmSettleState()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('affirm_settle_state')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
     //   $shop_all_id = $this->getAdminShopIdAll();

        $order_ids = $this->request->order_ids;
        $order_ids = explode(',', $order_ids);
        $order_ids = array_unique(array_filter($order_ids));

        DB::beginTransaction();
        try {
            $this->model->where('settle_state', 2)
              //  ->whereIn('shop_id' , $shop_all_id)
                ->whereIn('id', $order_ids)
                ->update([
                    'settle_state' => 1,
                    'settle_affirm_time' => date('Y-m-d H:i:s'),
                    'settle_affirm_manage_id' =>  request()->manage_id,
                ]);
            DB::commit();
            return $this->returnApi(200, '操作成功', true);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, '操作失败');
        }
    }
}
