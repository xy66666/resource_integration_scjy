<?php

namespace App\Http\Controllers\Admin;

use App\Models\AnswerActivity;
use App\Models\AnswerActivityGift;
use App\Models\AnswerActivityGiftPublic;
use App\Validate\AnswerActivityGiftPublicValidate;
use Illuminate\Support\Facades\DB;

/**
 * 活动礼物公示管理
 */
class AnswerActivityGiftPublicController extends CommonController
{

    public $model = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new AnswerActivityGiftPublic();
        $this->validate = new AnswerActivityGiftPublicValidate();
    }

    /**
     * 列表
     * @param act_id int 活动id
     * @param unit_id int 区域id
     * @param page int 当前页
     * @param limit int 分页大小
     * @param start_time datetime 创建时间范围搜索(开始) 
     * @param end_time datetime 创建时间范围搜索(结束) 
     * @param keywords string 搜索关键词(类型名称)
     */
    public function lists()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        //判断是否可以添加礼物
        $activityGiftModel = new AnswerActivityGift();
        $isAllowAddGift = $activityGiftModel->isAllowAddGift($this->request->act_id);
        if ($isAllowAddGift !== true) {
            return $this->returnApi(202, $isAllowAddGift);
        }

        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        $keywords = $this->request->keywords;
        $act_id = $this->request->act_id;
        $unit_id = $this->request->unit_id;

        $res = $this->model->lists(null, $act_id, $keywords, $unit_id, $start_time, $end_time, $page, $limit);

        if ($res['total'] == 0 || !$res['data']) {
            return $this->returnApi(203, "暂无数据");
        }

        foreach ($res['data'] as $key => $val) {
            $res['data'][$key]['type_name'] = $val['type'] == 1 ?  '文化红包' : '精美礼品';
            $res['data'][$key]['unit_name'] = !empty($val['con_unit']['name']) ?  $val['con_unit']['name'] : config('other.unit_name');
            $res['data'][$key][$this->list_index_key] = $this->addSerialNumberOne($key, $page, $limit);

            unset($res['data'][$key]['con_unit']);
        }

        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 详情
     * @param id int 类型id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->detail($this->request->id);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }
        $res->type_name = $res['type'] == 1 ?  '文化红包' : '精美礼品';
        return $this->returnApi(200, "获取成功", true, $res->toArray());
    }

    /**
     * 新增
     * @param act_id int活动id
     * @param unit_id int单位id 选了代表是单位专属，不选代表为公共
     * @param type tinyint 1文化红包 2精美礼品
     * @param name string 名称   最多 30个字符
     * @param way tinyint 操作方式  1 投放  2追加
     * @param number int 数量
     */
    public function add()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('add')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        //判断礼物类型
        if (!in_array($this->request->type, config('other.answer_activity_gift_type'))) {
            return $this->returnApi(201, '礼物类型错误');
        }
        //判断是否可以添加礼物
        $activityGiftModel = new AnswerActivityGift();
        $isAllowAddGift = $activityGiftModel->isAllowAddGift($this->request->act_id);
        if ($isAllowAddGift !== true) {
            return $this->returnApi(202, $isAllowAddGift);
        }

        //是否允许选单位id
        $node = AnswerActivity::where('id', $this->request->act_id)->value('node');
        if ($node == 1 && !empty($this->request->unit_id)) {
            return $this->returnApi(202, '此活动为独立活动，不能选择单位');
        }


        // $condition[] = ['act_id', '=', $this->request->act_id];

        // $is_exists = $this->model->nameIsExists($this->request->name, 'name', null, 1, $condition);
        // if ($is_exists) {
        //     return $this->returnApi(202, "此名称已存在");
        // }

        $res = $this->model->add($this->request->all());

        if (!$res) {
            return $this->returnApi(202, "新增失败");
        }
        return $this->returnApi(200, "新增成功", true);
    }

    /**
     * 修改
     * @param id int 礼物公示id
     * @param unit_id int单位id 选了代表是单位专属，不选代表为公共
     * @param type tinyint 1文化红包 2精美礼品
     * @param name string 名称   最多 30个字符
     * @param way tinyint 操作方式  1 投放  2追加
     * @param number int 数量
     */
    public function change()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        //判断礼物类型
        if (!in_array($this->request->type, config('other.answer_activity_gift_type'))) {
            return $this->returnApi(201, '礼物类型错误');
        }

        $result = $this->model->find($this->request->id);
        if (empty($result)) {
            return $this->returnApi(202, '参数传递错误');
        }

        //判断是否可以添加礼物
        $activityGiftModel = new AnswerActivityGift();
        $isAllowAddGift = $activityGiftModel->isAllowAddGift($this->request->act_id);
        if ($isAllowAddGift !== true) {
            return $this->returnApi(202, $isAllowAddGift);
        }

        //是否允许选单位id
        $node = AnswerActivity::where('id', $result->act_id)->value('node');
        if ($node == 1 && !empty($this->request->unit_id)) {
            return $this->returnApi(202, '此活动为独立活动，不能选择单位');
        }
        // $condition[] = ['act_id', '=', $this->request->act_id];

        // $is_exists = $this->model->nameIsExists($this->request->name, 'name', null, 1, $condition);
        // if ($is_exists) {
        //     return $this->returnApi(202, "此名称已存在");
        // }
        $res = $this->model->change($this->request->all());

        if (!$res) {
            return $this->returnApi(202, "修改失败");
        }
        return $this->returnApi(200, "修改成功", true);
    }

    /**
     * 删除
     * @param id int 类型id
     */
    public function del()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $res = $this->model->del($this->request->id);

        if ($res === true) {
            return $this->returnApi(200, "删除成功", true);
        }
        return $this->returnApi(202, $res);
    }
}
