<?php

namespace App\Http\Controllers\Admin;

use App\Models\CompetiteActivity;
use App\Models\CompetiteActivityWorks;

/**
 * 在线投票活动 数据总分析
 * Class AnswerType
 * @package app\admin\controller
 */
class CompetiteToatlDataAnalysisController extends CommonController
{
    public $model = null;
    public $worksModel = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new CompetiteActivity();
        $this->worksModel = new CompetiteActivityWorks();
    }


    /**
     * 在线投票总数据统计
     */
    public function competiteDataStatistics()
    {

        //获取活动总参与人次
        $part_number = $this->worksModel->getTakeNumber(null);
        //作品数量
        $works_number = $this->worksModel->getWorksNumber(null);
         //作品数量
         $vote_number = $this->worksModel->getTotalVoteNumber(null);
        //总点击量
        $browse_number = $this->model->getBrowseNumber(null, null, null);

        $data = [
            'part_number' => $part_number,
            'works_number' => $works_number,
            'vote_number' => $vote_number,
            'browse_number' => $browse_number,
        ];

        return $this->returnApi(200, "获取成功", true, $data);
    }

    /**
     * 最新活动排行榜
     */
    public function competiteDataRanking()
    {
        $data = $this->model->select('id' , 'title' ,'browse_num')->where('is_del' , 1)->orderByDesc('id')->limit(10)->get()->toArray();
        foreach($data as $key=>$val){
            $data[$key]['works_number'] = $this->worksModel->getWorksNumber($val['id']);
            $data[$key]['part_number'] = $this->worksModel->getTakeNumber($val['id']);
            $data[$key]['vote_number'] = $this->worksModel->getTotalVoteNumber($val['id']);
        }

        return $this->returnApi(200, "获取成功", true, $data);
    }


}
