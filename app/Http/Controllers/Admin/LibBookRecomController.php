<?php

namespace App\Http\Controllers\Admin;

use App\Models\LibBookRecom;
use App\Validate\LibBookRecomValidate;
use Illuminate\Support\Facades\DB;

/**
 * 馆藏书推荐表
 * Class LibraryRecom
 * @package app\api\controller
 */
class LibBookRecomController extends CommonController
{
    private $model = null;
    private $validate = null;

    public function __construct()
    {
        parent::__construct();
        $this->model = new LibBookRecom();
        $this->validate = new LibBookRecomValidate();
    }

    /**
     * 馆藏书推荐列表
     * @param keywords_type  检索条件   0、全部  1、书名  2、作者 3 ISBN  4.条形码 默认 0
     * @param keywords  检索条件
     * @param type_id   空字符串、代表全部    不能用  0 ，因为 0 表示 其他类型
     * @param is_recom  是否为推荐图书  0、全部 1 是 2 否   默认 0
     * @param page  页数，默认为1，
     * @param limit  条数，默认显示 10条
     */
    public function lists()
    {
        $keywords_type = $this->request->input('keywords_type', '');
        $keywords = $this->request->input('keywords', '');
        $is_recom = $this->request->input('is_recom', '');
        $type_id = $this->request->input('type_id', '');
        $page = intval($this->request->input('page', 1)) ?: 1;
        $limit = intval($this->request->input('limit', 10)) ?: 10;

        $keywords = addslashes($keywords);
        $res = $this->model->lists(null,$type_id, $keywords, $keywords_type, $is_recom, $limit);
        if (empty($res['data'])) {
            return $this->returnApi(203, '暂无数据');
        }

        foreach ($res['data'] as $key => $val) {
            $res['data'][$key][$this->list_index_key] = $this->addSerialNumberOne($key, $page, $limit);
            if (empty($val['con_type'])) {
                $res['data'][$key]['type_name'] = '暂无分类';
                $res['data'][$key]['type_id'] = 0; //防止查询数据为null
            } else {
                $res['data'][$key]['type_name'] = $val['con_type']['type_name'];
                $res['data'][$key]['type_id'] = $val['id']; //防止查询数据为null
            }
            unset($res['data'][$key]['con_type']);
        }
        $res = $this->disPageData($res);
        return $this->returnApi(200, '获取成功', true, $res);
    }


    /**
     * 详情
     * @param id int 书籍id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->detail($this->request->id);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误", "YES");
        }
        $res = $res->toArray();

        $res = $this->disDataSameLevel($res, 'con_type', ['type_name' => 'type_name']);

        unset($res['con_type']);

        return $this->returnApi(200, "查询成功", true, $res);
    }



    /**
     * 馆藏书书籍信息修改
     * @param id  馆藏书id
     * @param book_name  书名
     * @param author  作者
     * @param press  出版社     选填
     * @param pre_time  出版时间     选填
     * @param isbn  ISBN号
     * @param barcode  条形码
     * @param book_num  索书号
     * @param price  价格
     * @param img  图片
     * @param type_id   书籍分类
     * @param intro  简介      选填
     */
    public function change()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $res = $this->model->detail($this->request->id);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误", "YES");
        }

        $condition[] = ['metaid', '=', $res->metaid]; //增加检索条件
        $condition[] = ['metatable', '=', $res->metatable]; //增加检索条件
        $is_exists = $this->model->nameIsExists($this->request->barcode, 'barcode', $this->request->id, 1, $condition);

        if ($is_exists) {
            return $this->returnApi(202, "该书籍已存在");
        }
        $data = $this->request->all();
        if(empty($data)) unset($data['img']);
        $res = $this->model->change($data);

        if (!$res) {
            return $this->returnApi(202, "修改失败");
        }
        return $this->returnApi(200, "修改成功", true);
    }

    /**
     * 删除
     * @param id int 类型id
     */
    public function del()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $res = $this->model->detail($this->request->id);
        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }

        $res = $this->model->del($this->request->id);

        if ($res === true) {
            return $this->returnApi(200, "删除成功", true);
        }
        return $this->returnApi(202, $res);
    }

    /**
     * 推荐与取消推荐
     * @param ids int 书籍id
     * @param is_recom int 推荐   1 推荐  2 撤销
     */
    public function cancelAndRelease()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('cancel_and_release')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $shop_all_id = $this->getAdminShopIdAll();
        if (empty($shop_all_id)) {
            return $this->returnApi(203, '暂无数据'); //无权限访问
        }

        DB::beginTransaction();
        try {
            list($code, $msg) = $this->model->resumeAndCancel($this->request->ids, 'is_recom', $this->request->is_recom);

            $is_recom = $this->request->is_recom == 1 ? '推荐' : '撤销';

            DB::commit();
            return $this->returnApi($code, $is_recom . $msg, $code === 200 ? true : false);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, $e->getMessage());
        }
    }
}
