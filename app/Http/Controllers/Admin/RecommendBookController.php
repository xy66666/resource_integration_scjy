<?php

namespace App\Http\Controllers\Admin;


use App\Models\ProductionType;
use App\Models\RecommendBook;
use App\Models\RecommendUser;
use App\Validate\ProductionTypeValidate;
use App\Validate\RecommendBookVallidate;
use Illuminate\Support\Facades\DB;

/**
 * 在线荐购
 */
class RecommendBookController extends CommonController
{
    public $model = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->validate = new RecommendBookVallidate();
        $this->model = new RecommendBook();
    }

    /** 
     * 用户荐购列表
     * @param page int 当前页数
     * @param limit int 分页大小
     * @param start_time string 搜索开始时间
     * @param end_time string 搜索结束时间
     * @param keywords string 搜索关键词
     * @param keywords_type string 选择搜索的字段
     * @param type  int 审核状态  0 或 不传 为全部 1 已审核   2 未审核
     * @param status 状态 1待审核 2审核通过（购买中） 3审核不通过  4 已购买 5 购买失败 
     */
    public function lists()
    {
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? $this->request->limit : 10;
        $start_time = $this->request->start_time ? date("Y-m-d H:i:s", strtotime($this->request->start_time)) : null;
        $end_time = $this->request->end_time ? date("Y-m-d H:i:s", strtotime($this->request->end_time)) : null;
        $keywords_type = $this->request->keywords_type;
        $keywords = $this->request->keywords;
        $type = $this->request->type ? $this->request->type : 0;
        $status = $this->request->status;

        $res = $this->model->lists(null, $keywords, $keywords_type, $type, $status, $start_time, $end_time, $limit);

        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }

        $res = $this->disPageData($res);
        $res['data'] = $this->addSerialNumber($res['data'], $page, $limit);

        $handle_num = $this->model->where('status', '>', 1)->count(); //已处理个数
        $untreated_num = $this->model->where('status', '=', 1)->count(); //未处理个数

        $res['count']['handle_num'] = $handle_num;
        $res['count']['untreated_num'] = $untreated_num;

        return $this->returnApi(200, "获取成功", true, $res);
    }



    /**
     * 审核
     * @param ids int 申请id  多个逗号拼接 或数组格式
     * @param status int 状态 1待审核 2审核通过 3审核不通过   4 已购买 5 购买失败 
     * @param reason int 拒绝理由 status 为 3 或 5 必填
     */
    public function agreeAndRefused()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('check')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        if (($this->request->status == 3 || $this->request->status == 5) &&  empty($this->request->reason)) {
            return $this->returnApi(201, "拒绝理由不能为空");
        }

        DB::beginTransaction();
        try {
            $book_recommend = $this->model->agreeAndRefused($this->request->ids, $this->request->status, $this->request->reason);

            $status_name = $this->model->getStatusName($this->request->status,$this->request->reason);
            $recommendUserModel = new RecommendUser();
            foreach ($book_recommend as $key => $val) {
                /*消息推送*/
                $recommend_user = $recommendUserModel->where('recom_id', $val['id'])->select('id', 'user_id', 'create_time')->get()->toArray();

                foreach ($recommend_user as $k => $v) {
                    $this->systemAdd('线上荐购：'.$status_name, $v['user_id'], null, 35, $val['id'], '您线上荐购的书籍：' . $val['book_name'] . ''.$status_name);
                }
            }

            // 提交事务
            DB::commit();
            return $this->returnApi(200, "操作成功", true);
        } catch (\Exception $e) {
            // 回滚事务
            DB::rollBack();
            return $this->returnApi(202, "操作失败，请确认数据当前状态");
        }
    }
}
