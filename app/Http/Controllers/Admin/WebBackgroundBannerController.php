<?php

namespace App\Http\Controllers\Admin;


use App\Models\NewsType;
use App\Models\WebBackgroundBanner;
use App\Validate\NewsTypeValidate;
use App\Validate\WebBackgroundBannerValidate;

/**
 * 官网首页背景图片
 */
class WebBackgroundBannerController extends CommonController
{

    public $model = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new WebBackgroundBanner();
        $this->validate = new WebBackgroundBannerValidate();
    }

    /**
     * 列表
     */
    public function lists()
    {
        $res = $this->model->get();

        return $this->returnApi(200, "获取成功", true, $res->toArray());    
    }

    /**
     * 修改封面图片
     * @param type int 1 馆藏背景    2 观图动态背景  3 活动日历背景  4 读者服务背景  5 数字资源背景
     * @param img int 图片   可为空，表示删除
     */
    public function change()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->where('type' , $this->request->type)->first();

        if (!$res) {
            $res = $this->model;
        }
        $res->type = $this->request->type;
        $res->img = $this->request->img;
        $result = $res->save();

        if (!$result) {
            return $this->returnApi(201, "设置失败");
        }

        return $this->returnApi(200, "设置成功", true);
    }

   


}
