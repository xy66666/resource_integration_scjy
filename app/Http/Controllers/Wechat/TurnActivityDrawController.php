<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Controllers\RedisServiceController;
use App\Jobs\TurnActivityDrawJob;
use App\Models\TurnActivity;
use App\Models\TurnActivityGift;
use App\Models\TurnActivityLimitAddress;
use App\Models\TurnActivityUserGift;
use App\Models\TurnActivityUserUsePrizeRecord;
use App\Validate\TurnActivityDrawValidate;
use Illuminate\Support\Facades\Log;

/**
 * 答题活动抽奖管理
 */
class TurnActivityDrawController extends CommonController
{

    public $not_winning_msg = '很遗憾，未中奖'; //未中奖文案
    public $turn_activity_draw_key = 're_turn_activity_draw';

    public $activityModel = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->activityModel = new TurnActivity();
        $this->validate = new TurnActivityDrawValidate();
    }

    /**
     * 开始抽奖
     * @param timestamp  时间戳  13位
     * @param sign  签名  token 拼接 sign_key 拼接 时间戳  然后  md5后值 在全部转大写的值
     * @param token  用户guid
     * @param act_id int 活动id
     * 
     * @param lon 经度
     * @param lat 纬度
     */
    public function draw()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('draw')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        //判断经纬度是否符合参赛要求
        if (empty($this->request->lon) || empty($this->request->lat)) {
            return $this->returnApi(211, "请开启手机定位功能");
        }
        $turnActivityLimitAddressModel = new TurnActivityLimitAddress();
        $lon_lat_info = $turnActivityLimitAddressModel->checkLonLat($this->request->act_id, $this->request->lon, $this->request->lat);
        if ($lon_lat_info !== true) {
            return $this->returnApi(210, "您当前位置不在活动范围");
        }

        //验证签名是否正确
        if (!$this->verifySignature($this->request->timestamp, $this->request->token, $this->request->sign)) {
            return $this->returnApi(201, '签名错误');
        }

        $act_info = $this->activityModel->detail($this->request->act_id);

        if (empty($act_info)) {
            return $this->returnApi(201, '参数错误');
        }

        //判断是否输入邀请码，输入邀请码就跳过此验证
        if ($this->request->token && $act_info['turn_start_time'] > date('Y-m-d H:i:s')) {
            $userDrawInviteObj = new UserDrawInviteController();
            $checkInviteCode = $userDrawInviteObj->checkInviteCode(2, $this->request->token, $act_info['id'], $act_info['invite_code']);

            if ($checkInviteCode !== true) {
                return $this->returnApi(201, '不在抽奖时间段内');
            }
        }

        //活动结束需要判断
        if ($act_info['turn_end_time'] < date('Y-m-d H:i:s')) {
            return $this->returnApi(201, '不在抽奖时间段内');
        }
        //查看当前用户是否有正在抽奖的任务，有禁止继续抽奖
        $redisObj = RedisServiceController::getInstance();
        //判断订单号是否存在
        $is_exists = $redisObj->valueExists($this->turn_activity_draw_key . $this->request->token);
        if ($is_exists) {
            return $this->returnApi(201, '正在抽奖中，请勿重复操作');
        }
        //获取剩余抽奖机会
        $turnActivityUserUsePrizeRecordModel = new TurnActivityUserUsePrizeRecord();
        $isHavePrizeNumber = $turnActivityUserUsePrizeRecordModel->getSurplusPrizeNumber($this->request->act_id, $this->request->token, $act_info);
        if ($isHavePrizeNumber <= 0) {
            return $this->returnApi(201, '无抽奖次数');
        }

        //获取订单号
        $order_id = $turnActivityUserUsePrizeRecordModel->getDrawOrderId(10, $this->turn_activity_draw_key);

        if (empty($order_id)) {
            return $this->returnApi(201, '当前抽奖人数过多，请稍后重试');
        }
        //写入队列
        $data = [
            'act_id' => $this->request->act_id,
            'user_guid' => $this->request->token,
            'order_key' => $this->turn_activity_draw_key,
            'order_id' => $order_id,
            'create_time' => date("Y-m-d H:i:s"),
            'date' => date("Y-m-d"),
            'hour' => date("H")
        ];

        //数据写入 队列，然后再监听程序（php artisan queue:listen），
        //会自动找到 对应控制器进行处理，这里的参数与 控制器 构造方法参数对应，即使同时有多个队列，也会找到对应控制器去处理，不用我们代码处理，都为这个为主（new turnActivityDrawJob）
        //在监听后，程序会自动把参数放到对应位置
        //  $queue_id = $this->dispatch(new TurnActivityDrawJob('turn_activity_draw', $data)); //执行完后就不用管了，也不用管另外一个控制器怎么拿的到这些参数，各写各的逻辑即可

        $queue_id = dispatch(new TurnActivityDrawJob($this->turn_activity_draw_key, $data))->onQueue($this->turn_activity_draw_key); //执行完后就不用管了，也不用管另外一个控制器怎么拿的到这些参数，各写各的逻辑即可
        // $queue_id = TurnActivityDrawJob::dispatch($data)->onQueue('turn_activity_draw'); //执行完后就不用管了，也不用管另外一个控制器怎么拿的到这些参数，各写各的逻辑即可

        if (!$queue_id) {
            return $this->returnApi(202, '网络错误,请稍后重试');
        }

        //用户正在抽奖中
        $redisObj->setRedisValue($this->turn_activity_draw_key . $this->request->token, $order_id, 3600 * 2);
        $redisObj->setRedisValue($this->turn_activity_draw_key . $order_id, $order_id, 3600 * 2);

        sleep(1); //等1秒

        return $this->returnApi(200, '操作成功', true, ['order_id' => $order_id]);
    }

    /**
     * 获取抽奖结果
     * @param timestamp  时间戳  13位
     * @param sign  签名  token 拼接 sign_key 拼接 时间戳  然后  md5后值 在全部转大写的值
     * @param order_id 订单号
     * @param token 用户guid
     */
    public function getDrawResult()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('get_draw_result')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        //验证签名是否正确
        if (!$this->verifySignature($this->request->timestamp, $this->request->token, $this->request->sign)) {
            return $this->returnApi(201, '签名错误');
        }

        $order_id = $this->request->order_id;
        $token = $this->request->token;
        $redisObj = RedisServiceController::getInstance();

        //判断订单号是否存在,不存在，直接未中奖
        $is_exists = $redisObj->valueExists($this->turn_activity_draw_key . $order_id);
        if (!$is_exists) {
            return $this->returnApi(209, $this->not_winning_msg);
        }

        $turnActivityUserGiftModel = new TurnActivityUserGift();
        $turnActivityUserUsePrizeRecordModel = new turnActivityUserUsePrizeRecord();
        $userPrizeRecord = $turnActivityUserUsePrizeRecordModel->where('order_id', $order_id)->first();
        if (empty($userPrizeRecord)) {
            //sleep(1);//未获取到结果，先等一段时间在获取第二次 前端已经等待 500 毫秒
            return $this->returnApi(208, '正在获取抽奖结果，请稍等...');
        }

        //删除redis
        $redisObj->delRedisValue($this->turn_activity_draw_key . $order_id);

        if ($userPrizeRecord['user_guid'] != $token) {
            return $this->returnApi(202, '网络错误');
        }
        if ($userPrizeRecord['status'] == 2) {
            return $this->returnApi(209, $this->not_winning_msg);
        }
        //获取奖品
        $userGift = $turnActivityUserGiftModel->getUserGiftInfo($order_id);

        if (empty($userGift)) {
            return $this->returnApi(209, $this->not_winning_msg);
        }

        // $redisObj->delRedisValue($order_id);//删除订单号,抽奖结束后已删除，无需再次删除

        return $this->returnApi(200, '已中奖', true, [
            'name' => $userGift['name'], //礼物名称
            'img' => $userGift['img'], //礼物图片
            'intro' => $userGift['intro'], //礼物简介
            'price' => $userGift['price'], //金额
            'type' => $userGift['type'], //类型 1文化红包 2精美礼品
            'create_time' => $userGift['create_time'], //中奖时间
        ]);
    }
}
