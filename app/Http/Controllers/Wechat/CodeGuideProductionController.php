<?php

namespace App\Http\Controllers\Wechat;

use App\Models\CodeGuideExhibition;


/**
 * 作品配置类
 */
class CodeGuideProductionController extends CommonController
{
    protected $model;
    protected $validate;

    public function __construct()
    {
        parent::__construct();

        $this->model = new \App\Models\CodeGuideProduction();
        $this->validate = new  \App\Validate\CodeGuideProductionValidate();
    }

    /**
     * 展览列表
     * @param page int 页码
     * @param limit int 分页大小
     * @param keywords string 搜索关键词
     */
    public function exhibitionList()
    {
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;

        $exhibitionModel = new CodeGuideExhibition();
        $field = ['id', 'name', 'img', 'intro', 'browse_num', 'create_time'];
        $res = $exhibitionModel->lists($field, $keywords, 1, 1, null, null, $limit);
        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }
        $res = $this->disPageData($res);
        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 展览详情
     * @param exhibition_id int 展览id 
     */
    public function exhibitionDetail()
    {
        if (!$this->validate->scene('wx_exhibition_detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $exhibition_id = $this->request->exhibition_id;

        $exhibitionModel = new CodeGuideExhibition();
        $field = ['id', 'name', 'img', 'intro', 'browse_num', 'create_time'];
        $res = $exhibitionModel->detail($exhibition_id, null, 1, $field);
        if (empty($res)) {
            return $this->returnApi(203, "暂无数据");
        }
        return $this->returnApi(200, "查询成功", true, $res);
    }


    /**
     * 扫码获取作品列表
     * @param page int 页码
     * @param limit int 分页大小
     * @param exhibition_id int 展览id  与 qr_code 二选一
     * @param qr_code int 扫码后的信息   二维码 token  19开头
     * @param keywords string 搜索关键词
     */
    public function lists()
    {
        //增加验证场景进行验证
        // if (!$this->validate->scene('wx_lists')->check($this->request->all())) {
        //     return $this->returnApi(201,  $this->validate->getError());
        // }
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $exhibition_id = $this->request->exhibition_id;
        $qr_code = $this->request->qr_code;
        $keywords = $this->request->keywords;
        if (empty($exhibition_id) && empty($qr_code)) {
            return $this->returnApi(203, "参数错误");
        }

        $exhibitionModel = new CodeGuideExhibition();
        $exhibition_info = $exhibitionModel->detail($exhibition_id, $qr_code, 1, ['id']);
        if (empty($exhibition_info)) {
            return $this->returnApi(203, "暂无数据");
        }
        $exhibition_info->where('id', $exhibition_info['id'])->increment('browse_num'); //增加浏览量

        $res = $this->model->lists(null, $keywords, $exhibition_info['id'], null, null, null, $limit);

        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }
        //获取作品数量
        foreach ($res['data'] as $key => $val) {
            $res['data'][$key]['exhibition_name'] = !empty($val['con_exhibition']['name']) ?  $val['con_exhibition']['name'] : '';
            $res['data'][$key]['type_name'] = !empty($val['con_type']['type_name']) ?  $val['con_type']['type_name'] : '';

            $img_arr = [];
            if ($val['img']) {
                $img = explode("|", $val['img']);
                $thumb_img = explode("|", $val['thumb_img']);

                foreach ($img as $k => $v) {
                    if ($k > 0) {
                        break; //只需要第一个元素
                    }
                    $img_arr[$k]['img'] = $v;
                    $img_arr[$k]['thumb_img'] = $thumb_img[$k];
                }
            } else {
                $default_production = 'default/code_guide_production/default_code_guide_production' . mt_rand(1, 3) . '.png';
                $img_arr[0]['img'] = $default_production;
                $img_arr[0]['thumb_img'] = $default_production;
            }
            $res['data'][$key]['img'] = $img_arr[0]['img'];
            $res['data'][$key]['thumb_img'] = $img_arr[0]['thumb_img'];

            unset($res['data'][$key]['con_exhibition'], $res['data'][$key]['con_type']);
        }
        $res = $this->disPageData($res);
        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 详情
     * @param id int 作品id     二选一
     * @param qr_code int 扫码后的信息  二维码 token  20开头
     */
    public function detail()
    {
        $id = $this->request->id;
        $qr_code = $this->request->qr_code;
        if (empty($id) && empty($qr_code)) {
            return $this->returnApi(201, "参数错误");
        }
        $res = $this->model->detail($id, $qr_code);
        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }
        $res = $res->toArray();
        $res['exhibition_name'] = !empty($res['con_exhibition']['name']) ?  $res['con_exhibition']['name'] : '';
        $res['type_name'] = !empty($res['con_type']['type_name']) ?  $res['con_type']['type_name'] : '';

        $img_arr = [];
        if ($res['img']) {
            $img = explode("|", $res['img']);
            $thumb_img = explode("|", $res['thumb_img']);
            foreach ($img as $key => $val) {
                $img_arr[$key]['img'] = $val;
                $img_arr[$key]['thumb_img'] = $thumb_img[$key];
            }
        }
        $res['img_arr'] = $img_arr;
        unset($res['con_exhibition'], $res['con_type'], $res['img'], $res['thumb_img']);

        $this->model->where('id', $res['id'])->increment('browse_num'); //增加浏览量

        return $this->returnApi(200, "查询成功", true, $res);
    }
}
