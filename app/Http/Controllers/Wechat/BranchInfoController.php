<?php

namespace App\Http\Controllers\Wechat;

use App\Models\AccessNum;
use App\Models\BranchAccessNum;
use App\Models\BranchAccount;
use App\Models\BranchInfo;
use App\Validate\BranchInfoValidate;
use Illuminate\Support\Facades\DB;

/**
 * 本馆简介
 */
class BranchInfoController extends CommonController
{

    public $model = null;
    public $validate = null;
    public function __construct()
    {
        parent::__construct();

        $this->model = new BranchInfo();
        $this->validate = new BranchInfoValidate();
    }

    /**
     * 场馆列表
     * @param page int 当前页
     * @param limit int 分页大小
     * @param keywords string 搜索关键词
     */
    public function lists()
    {
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;

        $condition[] = ['id', '>=', 1];
        $condition[] = ['is_del', '=', 1];

        $res = $this->model->lists(null, $keywords, null, $limit);


        //增加行为分析
        $appInviteCodeBehaviorModel = new  \App\Models\AppInviteCodeBehavior();
        $appInviteCodeBehaviorModel->add(18);

        if (empty($res['data'])) {
            return $this->returnApi(203, '暂无数据');
        }

        foreach ($res['data'] as $key => $val) {
            $res['data'][$key]['intro'] = str_replace('&nbsp;', '', strip_tags(htmlspecialchars_decode($val['intro'])));
        }
        $res = $this->disPageData($res);
        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 详情
     * @param id int id   固定传  1
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->detail($this->request->id);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }
        $res->browse_num = $res->browse_num + 1;
        $res->save();
        $res = $res->toArray();

        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 记录访问分馆的浏览量
     * @param id 分馆id
     */
    // public function addBrowseNum()
    // {
    //     //增加验证场景进行验证
    //     if (!$this->validate->scene('add_browse')->check($this->request->all())) {
    //         return $this->returnApi(201,  $this->validate->getError());
    //     }

    //     $id = $this->request->id;
    //     $res = $this->model->where('id', $id)->first();
    //     if (empty($res)) {
    //         return $this->returnApi(201, '参数错误');
    //     }

    //     DB::beginTransaction();
    //     try {
    //         $res->browse_num = $res->browse_num + 1;
    //         $res->save();

    //         $branchAccessNum = new BranchAccessNum();
    //         $branchAccessNum->dayBrowse($id);

    //         DB::commit();
    //         return $this->returnApi(200, '操作成功', true);
    //     } catch (\Exception $e) {
    //         DB::rollBack();
    //         return $this->returnApi(202, '操作失败');
    //     }
    // }

    /**
     * 分馆用户登录账号登录
     * @param id 分馆id
     * @param account 账号
     * @param password 密码 
     */
    // public function branchAccountLogin()
    // {
    //     if (!$this->validate->scene('branch_account_login')->check($this->request->all())) {
    //         return $this->returnApi(201,  $this->validate->getError());
    //     }
    //     $id = $this->request->id;
    //     $res = $this->model->where('id', $id)->first();
    //     if (empty($res)) {
    //         return $this->returnApi(201, '参数错误');
    //     }
    //     $branchAccountModel = new BranchAccount();
    //     $branch_account = $branchAccountModel->where('branch_id', $id)->where('account', $this->request->account)->first();
    //     if (empty($branch_account)) {
    //         return $this->returnApi(202, '此账号不存在');
    //     }
    //     if ($branch_account['password'] != md5($this->request->password)) {
    //         return $this->returnApi(202, '密码输入错误');
    //     }
    //     return $this->returnApi(200, '登录成功', true);
    // }














    /**
     * 场馆简介 (未使用)
     * @param id int id   固定传  1
     */
    public function _detail()
    {

        //增加验证场景进行验证
        // if (!$this->validate->scene('detail')->check($this->request->all())) {
        //     return $this->returnApi(201,  $this->validate->getError());
        // }

        $id = $this->request->input('id', 1);

        $res = $this->model->select('id', 'intro', 'img', 'dispark_time', 'transport_line', 'province', 'city', 'district', 'address', 'tel', 'contacts', 'branch_img', 'lon', 'lat', 'email', 'borrow_notice')->find($id);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }

        $res = $res->toArray();
        $res['branch_img'] = $res['branch_img'] ? explode('|', $res['branch_img']) : null;

        $accessNumObj = new AccessNum();
        //获取浏览量
        $res['now_day'] =  $accessNumObj->where('yyy', date('Y'))->where('mmm', date('m'))->where('ddd', date('d'))->value('web_num');
        $res['now_day'] = $res['now_day'] ? $res['now_day'] : 0;
        $res['month_day'] =  $accessNumObj->where('yyy', date('Y'))->where('mmm', date('m'))->sum('web_num');
        $res['total_num'] =  $accessNumObj->sum('web_num');


        $res['color']['is_gray'] = false; //是否显示灰色  因为这个接口在任何地方都会请求

        return $this->returnApi(200, "查询成功", true, $res);
    }
}
