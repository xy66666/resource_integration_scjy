<?php

namespace App\Http\Controllers\Wechat;

use App\Models\UserAddress;
use App\Validate\UserAddressValidate;
use Illuminate\Support\Facades\DB;

/**
 * 用户地址管理
 */
class UserAddressController extends CommonController
{

    public $model = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new UserAddress();
        $this->validate = new UserAddressValidate();
    }

    /**
     * 列表
     * @param page int 当前页
     * @param limit int 分页大小
     * @param keywords string 搜索关键词(类型名称)
     */
    public function lists()
    {
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;
        $user_id = $this->request->user_info['id'];

        $condition[] = ['is_del', '=', 1];
        $condition[] = ['user_id', '=', $user_id];

        if ($keywords) {
            $condition[] = ['username', 'like', "%$keywords%"];
        }

        return $this->model->getSimpleList(['id', 'username', 'tel', 'province', 'city', 'district', 'street', 'address', 'is_default', 'create_time'], $condition, $page, $limit, 'is_default ASC');
    }

    /**
     * 详情
     * @param id int 类型id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('web_detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->select('id', 'username', 'tel', 'province', 'city', 'district', 'address', 'street', 'is_default', 'create_time')->find($this->request->id);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }

        return $this->returnApi(200, "获取成功", true, $res->toArray());
    }

    /** 
     * 新增
     * @param string username 姓名
     * @param string tel 电话
     * @param string province 省
     * @param string city 市
     * @param string district 区(县)
     * @param string street 街道
     * @param string address 详细地址
     * @param string is_default 是否为默认地址  是否默认   1 默认  2 非默认
     */
    public function add()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('web_add')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $user_id = $this->request->user_info['id'];
        // $is_exists = $this->model->nameIsExists($this->request->type_name , 'type_name');

        // if ($is_exists) {
        //     return $this->returnApi(202, "此名称已存在");
        // }
        $count = $this->model->where('user_id', $user_id)->where('is_del', 1)->count();
        if ($count > 10) {
            return $this->returnApi(202, "地址管理最多10条，请删除后在添加");
        }
        DB::beginTransaction();
        try {
            //修改所有的都为非默认
            if ($this->request->is_default == 1) {
                $this->model->where('user_id', $user_id)->where('is_del', 1)->update([
                    'is_default' => 2
                ]);
            }
            $this->request->merge(['user_id' => $user_id]);
            $this->model->add($this->request->all());

            DB::commit();
            return $this->returnApi(200, "新增成功", true);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, "新增失败");
        }
    }

    /** 
     * 修改
     * @param string id 
     * @param string username 姓名
     * @param string tel 电话
     * @param string province 省
     * @param string city 市
     * @param string district 区(县)
     * @param string street 街道
     * @param string address 详细地址
     * @param string is_default 是否为默认地址
     */
    public function change()
    {

        //增加验证场景进行验证
        if (!$this->validate->scene('web_change')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $user_id = $this->request->user_info['id'];
        // $is_exists = $this->model->nameIsExists($this->request->type_name , 'type_name' , $this->request->id);

        // if ($is_exists) {
        //     return $this->returnApi(202, "此名称已存在");
        // }

        DB::beginTransaction();
        try {
            //修改所有的都为非默认
            if ($this->request->is_default == 1) {
                $this->model->where('user_id', $user_id)->where('is_del', 1)->update([
                    'is_default' => 2
                ]);
            }
            $this->model->change($this->request->all());
            DB::commit();
            return $this->returnApi(200, "修改成功", true);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, "修改失败");
        }
    }

    /**
     * 删除
     * @param id int 类型id
     */
    public function del()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('web_del')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->del($this->request->id);

        if ($res === true) {
            return $this->returnApi(200, "删除成功", true);
        }
        return $this->returnApi(202, $res);
    }

    /**
     * 切换默认状态
     * @param id int 地址id
     */
    public function switchoverDefault()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('web_switchover_default')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $user_id = $this->request->user_info['id'];
        DB::beginTransaction();
        try {
            //修改所有的都为非默认
            $this->model->where('user_id', $user_id)->where('is_del', 1)->update([
                'is_default' => 2
            ]);

            $this->model->where('id', $this->request->id)->update(['is_default' => 1]);

            DB::commit();
            return $this->returnApi(200, "切换成功", true);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, "切换失败");
        }
    }
}
