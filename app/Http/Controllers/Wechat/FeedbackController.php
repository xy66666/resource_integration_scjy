<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Controllers\Controller;
use App\Models\Feedback;
use App\Validate\FeedbackValidate;
use Illuminate\Support\Facades\DB;

/**
 * 用户留言管理
 */
class FeedbackController extends CommonController
{
    public $model = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new Feedback();
        $this->validate = new FeedbackValidate();

    }


     /**
     * 用户留言
     * @param token  用户token 
     * @param username   用户名
     * @param tel  电话号码
     * @param feedback  反馈内容
     */
    public function addFeedback()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('feedback')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        if(empty(trim($this->request->feedback))){
            return $this->returnApi(201 , '反馈内容不能为空');
        }

        DB::beginTransaction();
        try{

            $this->request->merge(['user_id' => $this->request->user_info['id']]);//追加到request里面
            $this->model->add($this->request->all());

            DB::commit();
            return $this->returnApi(200 , '留言成功，等待后台管理员处理，请耐心等待' , true);
        }catch(\Exception $e){
            DB::rollBack();
            return $this->returnApi(202 , '留言失败');
        }
    }





}
