<?php

namespace App\Http\Controllers\Wechat;

use App\Models\AnswerActivityLimitAddress;
use App\Models\TurnActivityLimitAddress;
use App\Models\UserAddress;
use App\Models\UserDrawAddress;
use App\Models\UserInfo;
use App\Validate\UserDrawAddressValidate;

/**
 * 用户抽奖地址管理
 */
class UserDrawAddressController extends CommonController
{
    public $model = null;
    public $userDrawAddressModel = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new UserInfo();
        $this->userDrawAddressModel = new UserDrawAddress();
        $this->validate = new UserDrawAddressValidate();
    }

    /**
     * 获取用户地址信息
     * @param token 用户guid
     * @param type 类型  1、答题活动   2、在线抽奖
     * @param act_id 活动id
     */
    public function getUserAddress()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('get_user_address')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->userDrawAddressModel->detail($this->request->token, $this->request->type, $this->request->act_id);
        if (!empty($res)) {
            return $this->returnApi(200,  '获取成功', true, $res);
        } else {
            //获取一条最新的记录，然后复制在这个活动记录上
            $res = $this->userDrawAddressModel->detail($this->request->token);
            if ($res) {
                $this->userDrawAddressModel->inserts($res,  $this->request->type, $this->request->act_id);
            }
        }

        if (!empty($res)) {
            $res['act_id'] = $this->request->act_id;
            return $this->returnApi(200,  '获取成功', true, $res);
        }

        return $this->returnApi(203,  '暂无数据');
    }

    /**
     * 设置用户地址信息
     * @param type 类型  1、答题活动   2、在线抽奖
     * @param act_id 活动id
     * @param token 用户guid
     * @param username 用户名
     * @param tel 用户电话
     * @param province 省
     * @param city 市
     * @param district 区
     * @param address 详细地址
     */
    public function setUserAddress()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('set_user_address')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        //此次活动，只能参与
        if ($this->request->type == 1) {
            $activityLimitAddressModel = new AnswerActivityLimitAddress();
        } else {
            $activityLimitAddressModel = new TurnActivityLimitAddress();
        }
        $address_info = $activityLimitAddressModel->checkAddress($this->request->act_id, $this->request->province, $this->request->city);
        if ($address_info !== true) {
            return $this->returnApi(202, "收货地址不符合要求，详情请查看活动规则！");
        }

        $address = $this->request->address;
        if (!$this->userDrawAddressModel->checkKeywords($address)) {
            return $this->returnApi(201,  '详细地址不符合要求，详情请查看活动规则！');
        }

        $result = $this->userDrawAddressModel->detail($this->request->token, $this->request->type, $this->request->act_id);
        $this->request->merge(['user_guid' => $this->request->token]);
        if (!empty($result)) {
            $this->request->merge(['id' => $result['id']]);
            $res = $this->userDrawAddressModel->change($this->request->all());
        } else {
            $res = $this->userDrawAddressModel->add($this->request->all());
        }

        if ($res) {
            return $this->returnApi(200,  '设置成功', true);
        }
        return $this->returnApi(202,  '设置失败');
    }
}
