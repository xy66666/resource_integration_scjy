<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Controllers\Controller;
use App\Models\Scenic;
use App\Models\ScenicType;
use App\Models\ScenicWorks;
use App\Validate\ScenicValidate;

/**
 * 景点打卡
 */
class ScenicController extends Controller
{
    public $model = null;
    public $typeModel = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new Scenic();
        $this->typeModel = new ScenicType();
        $this->validate = new ScenicValidate();
    }

    /**
     * 景点打卡类型列表
     */
    public function typeList()
    {

        $condition[] = ['is_del', '=', 1];

        $result = $this->typeModel->select(['id', 'type_name', 'icon', 'color'])->where('is_del', 1)->get();
        $data = [];
        foreach ($result as $key => $val) {
            $res = $this->typeModel->isHaveScenic($val['id']);
            if ($res) {
                $data[] = $val;
            }
        }

        //增加行为分析
        $appInviteCodeBehaviorModel = new  \App\Models\AppInviteCodeBehavior();
        $appInviteCodeBehaviorModel->add(20);

        if ($data) {
            return $this->returnApi(200, "查询成功", true, $data);
        }
        return $this->returnApi(203, "暂无数据");
    }

    /**
     * 景点列表
     * @param page int 当前页
     * @param limit int 分页大小
     * @param keywords string 搜索关键词(景点名称)
     * @param type_id 类型id
     * @param lon float 经度
     * @param lat float 纬度
     */
    public function lists()
    {
        $page = request()->page ? intval(request()->page) : 1;
        $limit = request()->limit ? intval(request()->limit) : 10;
        $keywords = request()->keywords;
        $type_id = request()->type_id;
        $lon = request()->lon ? request()->lon : null;
        $lat = request()->lat ? request()->lat : null;

        if (empty($lon) || empty($lat)) {
            $res = $this->model->lists($keywords, $type_id, 1, $limit);
            foreach ($res['data'] as $key => $val) {
                $res['data'][$key]['distance']  = null;
                $res['data'][$key]['symbol']    = null;
                $res['data'][$key]['type_name'] = !empty($val['con_type']) ? $val['con_type']['type_name'] : '';

                $res['data'][$key]['is_clock']    = 2; //是否可以打卡  1 可以2 不可以
            }
        } else {
            $res = $this->model->listsByLonLat($keywords, $type_id, 1, $lon, $lat, $limit);
            foreach ($res['data'] as $key => $val) {
                if ($val['distance_old'] <= config('other.scenic_range')) {
                    $res['data'][$key]['is_clock']    = 1; //是否可以打卡  1 可以2 不可以
                } else {
                    $res['data'][$key]['is_clock']    = 2; //是否可以打卡  1 可以2 不可以
                }
            }
        }

        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }

        $res = $this->disPageData($res);
        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 景点详情
     * @param id int 景点id
     * @param lon float 经度
     * @param lat float 纬度
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $id = $this->request->id;
        $res = $this->model->detail($id);

        $res['is_clock']    = 2; //是否可以打卡  1 可以2 不可以
        if (empty($this->request->lon) || empty($this->request->lat)) {
            $res['distance']  = null;
            $res['symbol']    = null;
        } else {
            $distance = get_distance($this->request->lon, $this->request->lat, $res['lon'], $res['lat']);

            if ($distance <= config('other.scenic_range')) {
                $res['is_clock']    = 1; //是否可以打卡  1 可以2 不可以
            }

            $distance = m_switch_km($distance);
            $res['distance']  = sprintf("%.2f", $distance['num']);
            $res['symbol']    = $distance['symbol'];
        }
        //获取打卡用户人数
        $scenic_works_model = new ScenicWorks();
        $res['user_num']  = $scenic_works_model->getTotalUserNumber($id, [1], 1);
        $res['user_head_img'] = $scenic_works_model->getTotalUserHeadImg($id, [1], 1, 3);

        return $this->returnApi(200, "查询成功", true, $res);
    }
}
