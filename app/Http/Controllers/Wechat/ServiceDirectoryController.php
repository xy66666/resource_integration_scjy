<?php

namespace App\Http\Controllers\Wechat;


use App\Models\ServiceDirectory;
use App\Validate\ServiceDirectoryValidate;

/**
 * 服务指南
 */
class ServiceDirectoryController extends CommonController
{

    public $model = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new ServiceDirectory();
        $this->validate = new ServiceDirectoryValidate();
    }

    /**
     * 列表
     * @param page int 当前页
     * @param limit int 分页大小
     */
    public function lists()
    {
        $page = request()->page ? intval(request()->page) : 1;
        $limit = request()->limit ? intval(request()->limit) : 10;

        $res = $this->model->lists($limit, 1);

        //增加行为分析
        $appInviteCodeBehaviorModel = new  \App\Models\AppInviteCodeBehavior();
        $appInviteCodeBehaviorModel->add(21);

        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }
        foreach ($res['data'] as $key => $val) {
            $res['data'][$key]['img'] = 'default/service_directory/' . $val['type'] . '.png';
        }

        $res = $this->disPageData($res);
        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 详情
     * @param id int 文章id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->select('id', 'title', 'type', 'img', 'content', 'lon', 'lat', 'create_time')->find(request()->id);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }

        return $this->returnApi(200, "查询成功", true, $res->toArray());
    }
}
