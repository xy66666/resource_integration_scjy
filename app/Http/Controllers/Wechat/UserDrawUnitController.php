<?php

namespace App\Http\Controllers\Wechat;

use App\Models\AnswerActivityUserUnit;
use App\Models\TurnActivityUserUnit;
use App\Models\UserInfo;
use App\Validate\UserDrawUnitValidate;

/**
 * 用户单位信息
 */
class UserDrawUnitController extends CommonController
{
    public $model = null;
    public $userAddressModel = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new UserInfo();
        $this->validate = new UserDrawUnitValidate();
    }

    /**
     * 是否需要填写用户信息和单位信息 且 是否已经输入了用户信息
     * @param  token 用户guid
     * @param  act_id 活动id
     * @param  type   类型  1、答题活动   2、在线抽奖
     */
    public function checkRealInfo()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('check_real_info')->check($this->request->all())) {
            return $this->returnApi(201, $this->validate->getError());
        }
        //判断此活动是否需要邀请码
        $userDrawInviteObj = new UserDrawInviteController();
        $act_info = $userDrawInviteObj->getActInfo($this->request->type, $this->request->act_id);

        if (empty($act_info) || $act_info['end_time'] < date('Y-m-d H:i:s')) {
            return $this->returnApi(202, '活动不存在或已结束');
        }


        if ($act_info['start_time'] > date('Y-m-d H:i:s') && empty($act_info['invite_code'])) {
            return $this->returnApi(202, '活动未开始');
        }

        //判断是否输入邀请码 和邀请码是否正确,活动未开始才验证
        if ($act_info['start_time'] > date('Y-m-d H:i:s')) {
            $checkInviteCode = $userDrawInviteObj->checkInviteCode($this->request->type,$this->request->token, $this->request->act_id,  $act_info['invite_code']);
            if ($checkInviteCode === false) {
                return $this->returnApi(204, '请输入邀请码'); //请输入邀请码，才能进入活动
            }
        }

        $real_info_unit_id = $this->needRealInfoAndUnit($this->request->type, $this->request->token, $act_info);

        return $this->returnApi(200, '获取成功', true, ['real_info' => $real_info_unit_id['real_info'], 'unit_id' => $real_info_unit_id['unit_id']]); //是否需要填写单位，需要其他信息
    }

    /**
     * 输入用户信息 或 所属图书馆 
     * @param  token 用户guid
     * @param  act_id 活动id
     * @param  type   类型  1、答题活动   2、在线抽奖
     * @param  username   姓名
     * @param  tel   电话号码
     * @param  id_card   身份证
     * @param  reader_id   读者证
     * @param  unit_id   单位id
     */
    public function setRealInfo()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('set_real_info')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        //查看用户关注的图书馆个数，不能太多
        $count = $this->getUserUnitNumber($this->request->type , $this->request->token , $this->request->act_id);
        if ($count >= 5) {
            return $this->returnApi(202,  '系统检测到您数据存在异常，请联系管理员处理！');
        }

        //判断此活动是否需要邀请码
        $userDrawInviteObj = new UserDrawInviteController();
        $act_info = $userDrawInviteObj->getActInfo($this->request->type, $this->request->act_id);

        if (empty($act_info) || $act_info['end_time'] < date('Y-m-d H:i:s')) {
            return $this->returnApi(203, '活动不存在或已结束');
        }

        if (empty($act_info) || $act_info['end_time'] < date('Y-m-d H:i:s')) {
            return $this->returnApi(203,  '活动不存在或已结束');
        }

        //直接不验证邀请码
        // if ($act_info['start_time'] < date('Y-m-d H:i:s') && $act_info['end_time'] > date('Y-m-d H:i:s')) {
        //     return $this->returnApi(200,  '活动已开始', true); //活动已开始，直接进入活动
        // }

        if ($act_info['start_time'] > date('Y-m-d H:i:s') && empty($act_info['invite_code'])) {
            return $this->returnApi(203,  '活动未开始');
        }

        //判断参数是否输入完成 
        $real_info_unit_id = $this->needRealInfoAndUnit($this->request->type, $this->request->token, $act_info);

        if ($real_info_unit_id['unit_id'] === true && empty($this->request->unit_id)) {
            return $this->returnApi(202,  '单位不能为空');
        }

        $real_unit_info = $this->isSelectUnit($this->request->type,$this->request->token,$this->request->act_id,$this->request->unit_id, $act_info['is_need_unit']);

        if (empty($real_unit_info)) {
            $real_unit_info = $this->getModel($this->request->type);
            //首次添加这些信息
            $real_unit_info->act_id = $this->request->act_id;
            $real_unit_info->user_guid = $this->request->token;
        }
        if ($real_info_unit_id['unit_id'] === true) {
            $real_unit_info->unit_id = $this->request->unit_id; //添加或修改信息
        }

        if (isset($real_info_unit_id['real_info'])) {
            foreach ($real_info_unit_id['real_info'] as $key => $val) {
                $field = $val['field'];
                $value = $val['value'];
                if (empty($this->request->$field)) {
                    return $this->returnApi(202,  $value . '不能为空');
                }
                if ($field == 'username' && mb_strlen($this->request->$field) < 2) {
                    return $this->returnApi(202,  $value . '格式不正确');
                }
                if ($field == 'tel' && !verify_tel($this->request->$field)) {
                    return $this->returnApi(202,  $value . '格式不正确');
                }
                if ($field == 'id_card' && !is_legal_no($this->request->$field)) {
                    return $this->returnApi(202,  $value . '格式不正确');
                }
                $real_unit_info->$field = $this->request->$field; //添加或修改信息
            }
        }
        $res = $real_unit_info->save();

        if ($res) {
            return $this->returnApi(200,  '提交成功', true);
        } else {
            return $this->returnApi(202,  '提交失败');
        }
    }

    /**
     * 是否需要用户信息 
     * @param type 类型  1、答题活动   2、在线抽奖
     * @param user_guid 用户guid
     * @param act_info 活动信息
     */
    public function needRealInfoAndUnit($type,$user_guid,$act_info){
        if($type == 1){
            $model = new AnswerActivityUserUnit();
        }elseif($type == 2){
            $model = new TurnActivityUserUnit();
        }
        $real_info_unit_id = $model->needRealInfoAndUnit($user_guid, $act_info);

        return $real_info_unit_id;
    }
    /**
     * 是否需要用户信息 
     * @param type 类型  1、答题活动   2、在线抽奖
     * @param user_guid 用户guid
     */
    public function getUserUnitNumber($type,$user_guid,$act_id){
        $model = $this->getModel($type);
        $count = $model->where('user_guid',$user_guid)->where('act_id' , $act_id)->count();

        return $count;
    }
    /**
     * 是否需要用户信息 
     * @param type 类型  1、答题活动   2、在线抽奖
     * @param user_guid 用户guid
     */
    public function isSelectUnit($type,$user_guid,$act_id,$unit_id){
        $model = $this->getModel($type);
        $count = $model->isSelectUnit($user_guid, $act_id,$unit_id);

        return $count;
    }
     /**
     * 获取model 
     * @param type 类型  1、答题活动   2、在线抽奖
     */
    public function getModel($type){
        if($type == 1){
            $model = new AnswerActivityUserUnit();
        }elseif($type == 2){
            $model = new TurnActivityUserUnit();
        }

        return $model;
    }

}
