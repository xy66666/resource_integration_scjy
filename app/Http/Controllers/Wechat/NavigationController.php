<?php

namespace App\Http\Controllers\Wechat;

use App\Models\NavigationArea;
use App\Models\NavigationBuild;
use App\Models\NavigationPoint;
use App\Validate\NavigationValidate;
use Illuminate\Support\Facades\Log;

/**
 * 室内导航管理
 */
class NavigationController extends CommonController
{
    public $model = null;
    public $pointModel = null;
    public $buildModel = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new NavigationArea();
        $this->pointModel = new NavigationPoint();
        $this->buildModel = new NavigationBuild();
        $this->validate = new NavigationValidate();
    }

    /**
     * 建筑列表
     * @param limit int 分页大小
     * @param keywords string 搜索关键词
     */
    public function buildList()
    {
        $keywords = $this->request->keywords;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $data = $this->buildModel->lists($keywords, 1, $limit);

        //增加行为分析
        $appInviteCodeBehaviorModel = new  \App\Models\AppInviteCodeBehavior();
        $appInviteCodeBehaviorModel->add(24);

        if (empty($data['data'])) return $this->returnApi(203, '暂无数据');

        $data = $this->disPageData($data);
        return $this->returnApi(200, '查询成功', true, $data);
    }

    /**
     * 区域简单列表
     * @param build_id 建筑id
     */
    public function areaFilterList()
    {
        if (!$this->validate->scene('area_list')->check($this->request->all())) {
            return $this->returnApi(201, $this->validate->getError());
        }
        $build_id = $this->request->build_id;
        //查询建筑是否使用
        $is_build_exists = $this->buildModel->where('id', $build_id)->where('is_del', 1)->where('is_play', 1)->first();
        if (!$is_build_exists) return $this->returnApi(203, '暂无数据');
        //查询数据
        $res = $this->model->filterList($build_id);
        if (!$res) return $this->returnApi(203, '暂无数据');
        foreach ($res as $k => $v) {
            $res[$k]['name'] = !empty($v['alias_name']) ? $v['alias_name'] : $v['name'];
            unset($v['alias_name']);
        }
        return $this->returnApi(200, '查询成功', true, $res);
    }
    /**
     * 区域列表
     * @param build_id 建筑id
     */
    public function areaList()
    {
        if (!$this->validate->scene('area_list')->check($this->request->all())) {
            return $this->returnApi(201, $this->validate->getError());
        }
        $build_id = $this->request->build_id;
        //查询建筑是否使用
        $is_build_exists = $this->buildModel->where('id', $build_id)->where('is_del', 1)->where('is_play', 1)->first();
        if (!$is_build_exists) return $this->returnApi(203, '暂无数据');
        //查询数据
        $res = $this->model->filterList($build_id, null, null, 1);
        if (!$res) return $this->returnApi(203, '暂无数据');
        //先查询开始区域是否是第一个建筑
        $is_first = true;
        foreach ($res as $k => $v) {
            if ($k > 0 && $v['is_start'] == 1) {
                $is_first = false;
            }
            $res[$k]['name'] = !empty($v['alias_name']) ? $v['alias_name'] : $v['name'];
            unset($res[$k]['alias_name']);
        }
        //默认第一层，显示第一层的点位数
        foreach ($res as $k => $v) {
            if ($v['is_start'] == 1) {
                $choose = 1;
            } elseif ($is_first && $k == 0) {
                $choose = 1;
            } else {
                $choose = 2;
            }
            if ($choose == 1) {
                //查询点位信息
                $point_data = $this->pointModel->getFilterListByAreaId($v['id'], 1);
                $start_point = $point_data['start_point'];
                $res[$k]['end_point'] = $point_data['end_point'];
            } else {
                $point_data = $this->pointModel->getFilterListByAreaId($v['id'], 2);
                $start_point = $point_data['start_point'];
            }

            if (!empty($start_point['name'])) {
                $start_point['name'] = $start_point['name'] . "(" . $v['name'] . ")";
            }
            $res[$k]['start_point'] = $start_point;

            $res[$k]['choose'] = $choose;
            unset($res[$k]['is_start']);
        }
        return $this->returnApi(200, '查询成功', true, $res);
    }

    /**
     * 查询指定区域点位列表
     * @param $area_id 区域id
     */
    public function pointList()
    {
        if (!$this->validate->scene('point_list')->check($this->request->all())) {
            return $this->returnApi(201, $this->validate->getError());
        }
        $area_id = $this->request->area_id;
        //查询区域是否正常使用
        $is_area_exists = $this->model->where('id', $area_id)->where('is_del', 1)->where('is_play', 1)->first();
        if (!$is_area_exists) return $this->returnApi(203, '暂无数据');
        //查询点位列表信息
        $res = $this->pointModel->getFilterListByAreaId($area_id);
        if (empty($res['end_point'])) return $this->returnApi(203, '暂无数据');

        return $this->returnApi(200, '查询成功', true, $res['end_point']);
    }

    /**
     * 点位搜索接口
     * @param page int 页码
     * @param limit int 分页大小
     * @param $keywords string 关键字
     * @param $keywords_type int 搜索类型 1 起点搜索 2目的地搜索
     * @param $build_id int 搜索的建筑
     * @param $area_id int 区域id
     */
    public function pointSearchList()
    {
        if (!$this->validate->scene('search')->check($this->request->all())) {
            return $this->returnApi(201, $this->validate->getError());
        }
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $build_id = $this->request->build_id;
        $area_id = $this->request->area_id;
        $keywords = $this->request->keywords;
        $keywords_type = $this->request->keywords_type;

        $build_area_ids = [];
        //查询传递id是否存在
        if ($area_id) {
            $is_area_exists = $this->model->where('id', $area_id)->where('is_del', 1)->where('is_play', 1)->first();
            if (!$is_area_exists) return $this->returnApi(203, '暂无数据');
        }
        //查询建筑内区域信息
        $is_build_exists = $this->buildModel->where('id', $build_id)->where('is_del', 1)->where('is_play', 1)->first();
        if (!$is_build_exists) return $this->returnApi(203, '暂无数据');

        //查询建筑内所有区域Id
        $build_area_data = $this->model->filterList($build_id, null, $area_id);
        if ($build_area_data) {
            foreach ($build_area_data as $k => $v) {
                $build_area_ids[] = $v['id'];
            }
            $build_area_data = array_column($build_area_data, null, 'id');
        }
        //根据起点或者目的地，查找对应类型数据
        $res = $this->pointModel->getDataByType($build_area_ids, $keywords_type, $keywords, $limit);

        if (empty($res['data'])) return $this->returnApi(203, '暂无数据');
        //如果不是搜索指定区域，返回区域名称
        //如果是多个区域，返回区域名字
        foreach ($res['data'] as $k => $v) {
            $cur_area_name = "";
            $cur_build_area_data = !empty($build_area_data[$v['area_id']]) ? $build_area_data[$v['area_id']] : [];
            if ($cur_build_area_data) {
                $cur_area_name = !empty($cur_build_area_data['alias_name']) ? $cur_build_area_data['alias_name'] : $cur_build_area_data['name'];
            }
            $res['data'][$k]['area_name'] = $cur_area_name;
        }
        $res = $this->disPageData($res);
        return $this->returnApi(200, '查询成功', true, $res);
    }

    /**
     * 导航线路搜索
     * @param $start_area_id 起点楼层
     * @param $start_point_id 起点点位id
     * @param $end_point_id  目的地点位id
     * @param $end_area_id  目的地区域id
     */
    public function navigationLineList()
    {
        if (!$this->validate->scene('line_list')->check($this->request->all())) {
            return $this->returnApi(201, $this->validate->getError());
        }
        $start_point_id = $this->request->start_point_id;
        $start_area_id = $this->request->start_area_id;
        $end_point_id = $this->request->end_point_id;
        $end_area_id = $this->request->end_area_id;
        //统一调用方法
        $res = $this->pointModel->navigationLineData($start_area_id, $start_point_id, $end_area_id, $end_point_id);
        if (empty($res)) {
            return $this->returnApi(203, '暂无数据');
        }
        if (is_string(empty($res))) {
            return $this->returnApi(202, $res);
        }
        return $this->returnApi(200, '查询成功', true, $res);
    }
}
