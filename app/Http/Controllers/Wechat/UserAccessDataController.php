<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Controllers\RedisServiceController;


/**
 * 查看当前访问人数
 */
class UserAccessDataController extends CommonController
{
    public $limit_num = 300; //每分钟限制访问次数 300 次
    public $limit_time = 60; //限制秒数 单位秒 60


    public function __construct()
    {
        parent::__construct();

    }

    /**
     * 获取访问人数
    // * @param  act_id 活动id
     */
    public function limitAccess()
    {
        //放入redis，限制访问人数
        $redisServiceObj = RedisServiceController::getInstance()->getRedis();
 
        $data = $redisServiceObj->lrange('resource_integration', 0, -1);//返回的数据，最新的在第一个，最老的数据在最后一个

        if(empty($data)){
            return $this->returnApi(200 , '允许访问');
        }
        $data = array_reverse($data);
        //去掉过期的数据
        $time = strtotime('-'.$this->limit_time.' second');

        foreach($data as  $key=>$val){
            if($val < $time){
                unset($data[$key]);
                $redisServiceObj->rpop('resource_integration');//删除右边第一个元素
            }else{
                break;
            }
        }

        if(count($data) > $this->limit_num){
            return $this->returnApi(202 , '访问人数过多,正在排队中,请稍后...');
        }
        return $this->returnApi(200 , '允许访问');
    }
}
