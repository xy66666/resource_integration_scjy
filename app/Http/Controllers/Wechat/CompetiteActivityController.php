<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Controllers\PdfController;
use App\Models\CompetiteActivity;
use App\Models\CompetiteActivityGroup;
use App\Models\CompetiteActivityTag;
use App\Models\CompetiteActivityWorksType;
use App\Models\CompetiteActivityVote;
use App\Models\CompetiteActivityWorks;
use App\Models\CompetiteActivityWorksCheck;
use App\Models\UserInfo;
use App\Models\UserViolate;
use App\Validate\CompetiteActivityValidate;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * 线上大赛活动
 * Class CompetiteActivityWorksType
 * @package app\admin\controller
 */
class CompetiteActivityController extends CommonController
{
    public $model = null;
    public $validate = null;
    public $worksModel = null;
    public $score_type = 7;

    public function __construct()
    {
        parent::__construct();

        $this->model = new CompetiteActivity();
        $this->validate = new CompetiteActivityValidate();
        $this->worksModel = new CompetiteActivityWorks();
    }

    /**
     * 列表
     * @param page int 当前页
     * @param limit int 分页大小
     * @param type_id string 类型id
     * @param keywords string 搜索关键词(文章标题)
     * @param start_time datetime 比赛开始时间(开始)
     * @param end_time datetime 比赛截止时间(截止)
     */
    public function lists()
    {
        $page = request()->page ? intval(request()->page) : 1;
        $limit = request()->limit ? intval(request()->limit) : 10;
        $keywords = request()->keywords;
        $type_id = request()->type_id;
        $start_time = request()->start_time;
        $end_time = request()->end_time;

        $res = $this->model->lists(null, $type_id, $keywords, $start_time, $end_time, null, null, 1, $limit);

        if (empty($res['data'])) {
            return $this->returnApi(203, '暂无数据');
        }
        $competiteActivityTagModel = new CompetiteActivityTag();
        foreach ($res['data'] as $key => $val) {
            $res['data'][$key]['take_number'] = $this->worksModel->getTakeNumber($val['id']); //参与人数
            $res['data'][$key]['status'] = $this->model->getCompetiteStatus($val); //获取活动状态

            $res['data'][$key]['con_start_time'] = $val['con_start_time'] ? date('Y-m-d H:i', strtotime($val['con_start_time'])) : null;
            $res['data'][$key]['con_end_time'] = $val['con_end_time'] ? date('Y-m-d H:i', strtotime($val['con_end_time'])) : null;
            $res['data'][$key]['vote_start_time'] = $val['vote_start_time'] ? date('Y-m-d H:i', strtotime($val['vote_start_time'])) : null;
            $res['data'][$key]['vote_end_time'] = $val['vote_end_time'] ? date('Y-m-d H:i', strtotime($val['vote_end_time'])) : null;

            $res['data'][$key]['type_name'] = isset($val['con_type']['type_name']) ? $val['con_type']['type_name'] : '';
            $res['data'][$key]['tag_info'] = $competiteActivityTagModel->getTagNameByTagId($val['tag_id']); //获取征集活动标签
        }

        $res = $this->disPageData($res);
        return $this->returnApi(200, "查询成功", true, $res);
    }
    /**
     * 我的大赛列表
     * @param token 用户token
     * @param page int 当前页
     * @param limit int 分页大小
     * @param keywords string 搜索关键词(文章标题)
     * @param start_time datetime 比赛开始时间(开始)
     * @param end_time datetime 比赛截止时间(截止)
     */
    public function myCompetiteLists()
    {
        $page = request()->page ? intval(request()->page) : 1;
        $limit = request()->limit ? intval(request()->limit) : 10;
        $keywords = request()->keywords;
        $start_time = request()->start_time;
        $end_time = request()->end_time;
        $user_id = $this->request->user_info['id'];

        $res = $this->model->myCompetiteLists(1, null, $user_id, $keywords, $start_time, $end_time, 1, $limit);

        if (empty($res['data'])) {
            return $this->returnApi(203, '暂无数据');
        }
        $competiteActivityTagModel = new CompetiteActivityTag();
        foreach ($res['data'] as $key => $val) {
            $res['data'][$key]['take_number'] = $this->worksModel->getTakeNumber($val['id']); //参与人数
            $res['data'][$key]['status'] = $this->model->getCompetiteStatus($val); //获取活动状态

            $res['data'][$key]['con_start_time'] = $val['con_start_time'] ? date('Y-m-d H:i', strtotime($val['con_start_time'])) : null;
            $res['data'][$key]['con_end_time'] = $val['con_end_time'] ? date('Y-m-d H:i', strtotime($val['con_end_time'])) : null;
            $res['data'][$key]['vote_start_time'] = $val['vote_start_time'] ? date('Y-m-d H:i', strtotime($val['vote_start_time'])) : null;
            $res['data'][$key]['vote_end_time'] = $val['vote_end_time'] ? date('Y-m-d H:i', strtotime($val['vote_end_time'])) : null;

            $res['data'][$key]['tag_info'] = $competiteActivityTagModel->getTagNameByTagId($val['tag_id']); //获取征集活动标签
        }

        $res = $this->disPageData($res);
        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 详情
     * @param token 用户token
     * @param id int 比赛id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('wx_detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $user_id = $this->request->user_info['id'];
        $res = $this->model->detail($this->request->id);

        if (empty($res)) {
            return $this->returnApi(202, '参数传递错误');
        }

        $res->status = $this->model->getCompetiteStatus($res); //获取活动状态
        $res->take_number = $this->worksModel->getTakeNumber($res['id']); //参与人数

        $competiteActivityVoteObj = new CompetiteActivityVote();
        list($res->vote_msg, $res->vote_number) = $competiteActivityVoteObj->getVoteNum($res->vote_way, $res->number, $user_id, $this->request->id);

        //更新浏览量
        $this->model->updateBrowseNumber($this->request->id);

        $res = $res->toArray();
        unset($res['con_check']); //不需要用户审核信息

        $real_info_arr = $this->model->getCompetiteActivityApplyParam(); //获取所有的数据
        $res['real_info'] = $this->getRealInfoArray($res['real_info'], $real_info_arr);
        $res['con_start_time'] = date('Y-m-d H:i', strtotime($res['con_start_time']));
        $res['con_end_time'] = date('Y-m-d H:i', strtotime($res['con_end_time']));
        $res['vote_start_time'] = $res['vote_start_time'] ? date('Y-m-d H:i', strtotime($res['vote_start_time'])) : null;
        $res['vote_end_time'] = $res['vote_end_time'] ? date('Y-m-d H:i', strtotime($res['vote_end_time'])) : null;

        $res['tag_info'] = CompetiteActivityTag::getTagNameByTagId($res['tag_id']); //获取征集活动标签

        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 参赛作品列表
     * @param token 用户token
     * @param con_id 大赛id
     * @param page int 当前页
     * @param limit int 分页大小
     * @param type_id int 类型id
     * @param sort int 排序方式  1 时间排序（默认）  2 投票排序
     * @param keywords string 搜索关键词(名称)
     * @param except_ids string 需要排除的id ，多个逗号链接
     */
    public function productionList()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('wx_production_list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $user_id = $this->request->user_info['id'];
        $con_id = $this->request->con_id;
        $type_id = $this->request->type_id;
        $sort = $this->request->sort;
        $keywords = $this->request->keywords;
        $except_ids = $this->request->except_ids;
        $limit = $this->request->input('limit', 10);

        $sort = $sort == 2 ? 'vote_num DESC,id DESC' : 'id DESC';
        $res = $this->worksModel->lists($con_id, null, null, null, $type_id, $keywords, ['1'], $except_ids, null, null, 1, 1, null, null, $sort, null, null, null, $limit);

        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }

        $competiteActivityVoteObj = new CompetiteActivityVote();
        $data = $this->model->where('id', $con_id)->where('is_del', 1)->where('is_play', 1)->first();
        if (empty($data)) {
            return $this->returnApi(203, "暂无数据");
        }
        $vote_way = $data['vote_way'];

        foreach ($res['data'] as $key => $val) {
            if ($user_id) {
                $res['data'][$key]['user_vote_num'] = $competiteActivityVoteObj->userVoteNum($vote_way, $user_id, $con_id, $val['id']); //用户投票数
            } else {
                $res['data'][$key]['user_vote_num'] = 0; //获取活动状态
            }
        }

        $competiteActivity = CompetiteActivity::select('id', 'vote_way', 'number', 'con_start_time', 'con_end_time', 'vote_start_time', 'vote_end_time')->where('id', $con_id)->first();
        list($competiteActivity->vote_msg, $competiteActivity->vote_number) = $competiteActivityVoteObj->getVoteNum($competiteActivity->vote_way, $competiteActivity->number, $user_id, $con_id);
        $res = $this->disPageData($res);
        $res['competite_activity'] = $competiteActivity;
        $res['competite_activity']['status'] = $this->model->getCompetiteStatus($data); //获取活动状态

        return $this->returnApi(200, "获取成功", true, $res);
    }


    /**
     * 我的参赛作品列表
     * @param token 用户token
     * @param con_id 大赛id
     * @param page int 当前页
     * @param limit int 分页大小
     * @param type_id int 类型id
     * @param sort int 排序方式  1 时间排序（默认）  2 投票排序
     * @param keywords string 搜索关键词(名称)
     */
    public function myProductionList()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('wx_production_list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $user_id = $this->request->user_info['id'];
        $con_id = $this->request->con_id;
        $type_id = $this->request->type_id;
        $sort = $this->request->sort;
        $keywords = $this->request->keywords;
        $limit = $this->request->input('limit', 10);

        $sort = $sort == 2 ? 'vote_num DESC,id DESC' : 'id DESC'; //vote_num ASC,id ASC 解决点赞量全为0时，数据错乱问题
        $res = $this->worksModel->lists($con_id, null, null, $user_id, $type_id, $keywords, ['1', '2', '3', '4'], '', null, null, 1, null, null, null, $sort, null, null, null, $limit);


        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }

        $competiteActivityVoteObj = new CompetiteActivityVote();
        $competiteActivity = CompetiteActivity::select('id', 'appraise_way', 'vote_way', 'number', 'con_start_time', 'con_end_time', 'vote_start_time', 'vote_end_time')->where('id', $con_id)->first();
        //文案、可投票量
        //判断是否支持用户投票
        if ($competiteActivity['appraise_way'] == 3) {
            $competiteActivity->vote_msg = '当前活动不支持用户投票';
            $competiteActivity->vote_number = null;
        } else {
            list($competiteActivity->vote_msg, $competiteActivity->vote_number) = $competiteActivityVoteObj->getVoteNum($competiteActivity->vote_way, $competiteActivity->number, $user_id, $con_id);
        }
        $data = $this->model->where('id', $con_id)->first();
        $vote_way = $data['vote_way'];
        foreach ($res['data'] as $key => $val) {
            if ($competiteActivity['appraise_way'] == 3) {
                $res['data'][$key]['user_vote_num'] = null; //不支持用户投票
                continue;
            }
            if ($user_id) {
                $res['data'][$key]['user_vote_num'] = $competiteActivityVoteObj->userVoteNum($vote_way, $user_id, $con_id, $val['id']); //用户投票数
            } else {
                $res['data'][$key]['user_vote_num'] = 0; //获取活动状态
            }
        }

        $res = $this->disPageData($res);
        $res['competite_activity'] = $competiteActivity;
        $res['competite_activity']['status'] = $this->model->getCompetiteStatus($data); //获取活动状态

        return $this->returnApi(200, "获取成功", true, $res);
    }


    /**
     * 参赛作品详情+我的作品详情
     * @param token 用户token
     * @param id id
     */
    public function productionDetail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('wx_production_detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $user_id = $this->request->user_info['id'];

        $res = $this->worksModel->detail($this->request->id, [
            'id', 'con_id', 'serial_number', 'username', 'tel', 'id_card', 'user_id', 'group_id', 'unit', 'wechat_number', 'adviser', 'adviser_tel',
            'reader_id', 'title', 'type_id', 'img', 'content', 'voice', 'voice_name', 'video', 'video_name', 'intro', 'cover', 'width', 'height',
            'browse_num', 'vote_num', 'status', 'reason', 'is_violate', 'violate_reason', 'violate_time', 'create_time', 'change_time', 'type',
            'age', 'school', 'participant', 'original_work', 'authorizations_address', 'promise_address', 'pdf_works_address', 'pdf_works_address_img'
        ]);

        if (empty($res)) {
            return $this->returnApi(203, "暂无数据");
        }

        //增加访问量
        $res->browse_num = ++$res->browse_num;
        $res->save();

        $competiteActivityVoteObj = new CompetiteActivityVote();
        $vote_way = $this->model->where('id', $res['con_id'])->value('vote_way');
        if ($user_id) {
            $res['user_vote_num'] = $competiteActivityVoteObj->userVoteNum($vote_way, $user_id, $res->con_id, $res['id']); //用户投票数
        } else {
            $res['user_vote_num'] = 0; //获取活动状态
        }

        $competiteActivity = CompetiteActivity::select('id', 'vote_way', 'number', 'con_start_time', 'con_end_time', 'vote_start_time', 'vote_end_time')->where('id', $res->con_id)->first();
        list($competiteActivity->vote_msg, $competiteActivity->vote_number) = $competiteActivityVoteObj->getVoteNum($competiteActivity->vote_way, $competiteActivity->number, $user_id, $res->con_id);
        $res['competite_activity'] = $competiteActivity;

        if ($res['type'] == 1) {
            $user_info = UserInfo::getWechatField($res['user_id'], ['head_img', 'nickname']);
            $res['head_img'] = $user_info['head_img'];
            $res['nickname'] = $user_info['nickname'];
        } else {
            $competiteActivityGroupModel = new CompetiteActivityGroup();
            $group_info = $competiteActivityGroupModel->getGroupNameByGroupId($res['group_id'], ['name', 'img']);
            $res['head_img'] = $group_info['img'];
            $res['group_name'] = $group_info['name'];
        }

        //微信端pdf 转为图片
        if (!empty($res['pdf_works_address_img'])) {
            $res['pdf_works_address_img'] = explode('|', $res['pdf_works_address_img']);
        } elseif (!empty($res['pdf_works_address'])) {
            //pdf转换为多张图片
            $pdfObj = new PdfController();
            $pdf = $pdfObj->pdf2png($res['pdf_works_address'], '/competite_activity_pdf_img/');
            if ($pdf) {
                $pdf_img = join('|', $pdf);
                $this->worksModel->where('id', $this->request->id)->update(['pdf_works_address_img' => $pdf_img]); //修改资源
                $res['pdf_works_address_img'] = $pdf;
            }
        }

        return $this->returnApi(200, "获取成功", true, $res);
    }

    /**
     * 线上大赛类型
     * @param token 用户token
     * @param con_id  大赛id
     */
    public function getTypeList()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('wx_type_list')->check($this->request->all())) {
            return $this->returnApi(201, $this->validate->getError());
        }

        $user_id = $this->request->user_info['id'];

        $CompetiteActivityWorksTypeObj = new CompetiteActivityWorksType();
        $res = $CompetiteActivityWorksTypeObj->select(['id', 'type_name', 'limit_num', 'letter', 'node'])
            ->where('con_id', $this->request->con_id)
            ->where('is_del', 1)
            ->orderByDesc('id')
            ->get();

        if ($res->isEmpty()) {
            return $this->returnApi(203, "暂无数据");
        }

        foreach ($res as $key => $val) {
            $res[$key]->have_number = $this->worksModel->getUserWorksNumberByTypeId($this->request->con_id, null, null, $user_id, $val['id']);
        }

        return $this->returnApi(200, "获取成功", true, $res->toArray());
    }
    /**
     * 根据电话号码获取用户基本信息
     * @param tel  电话号码
     */
    public function getWorksUserInfo()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('wx_works_user_info')->check($this->request->all())) {
            return $this->returnApi(201, $this->validate->getError());
        }
        $user_id = $this->request->user_info['id'];
        $res = $this->worksModel->select(['tel', 'username', 'id_card', 'unit', 'wechat_number', 'adviser', 'adviser_tel', 'reader_id', 'age', 'school', 'participant', 'original_work'])
            ->where('tel', $this->request->tel)
            ->where('user_id', $user_id)
            ->orderByDesc('id') //获取最新的
            ->first();

        if (empty($res)) {
            return $this->returnApi(203, "暂无数据");
        }

        return $this->returnApi(200, "获取成功", true, $res->toArray());
    }

    /**
     * 作品投票
     * @param token  用户token
     * @param works_id 作品id
     */
    public function  worksVote()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('wx_works_vote')->check($this->request->all())) {
            return $this->returnApi(201, $this->validate->getError());
        }
        $user_id = $this->request->user_info['id'];


        Log::info(date('Y-m-d H:i:s') . '~用户id：' . $user_id . '；作品id：' . $this->request->works_id . ';ip地址：' . request()->ip());

        // $ip = [
        //     '112.98.169.75',
        //     '222.180.68.57',
        //     '223.104.251.49',
        //     '183.230.215.93',
        // ];
        // if(in_array(request()->ip() , $ip)){
        //     return $this->returnApi(201, "系统检测到你存在刷票行为，暂时取消你的投票资格");
        // }

        //限制用户多次点击
        $key = md5($user_id . $this->request->works_id . request()->ip());

        $oldKey =  Cache::get($key); //没有缓存返回false
        if ($oldKey) {
            return $this->returnApi(201, "您操作的太频繁了，请稍后重试");
        } else {
            Cache::put($key, 1, 3);
        }
        $worksInfo = $this->worksModel->find($this->request->works_id);
        if (empty($worksInfo)) {
            return $this->returnApi(201, "参数错误");
        }
        if ($worksInfo['status'] != 1) {
            Log::info('当前作品未审核通过，不允许投票-' . date('Y-m-d H:i:s') . '~用户id：' . $user_id . '；作品id：' . $this->request->works_id . ';ip地址：' . request()->ip());

            return $this->returnApi(201, "当前作品未审核通过，不允许投票");
        }

        if ($worksInfo['appraise_way']) {
            return $this->returnApi(201, "此作品不支持用户投票");
        }
        $competiteInfo = $this->model->select('id', 'vote_way', 'number', 'vote_start_time', 'vote_end_time')->where('is_del', 1)->find($worksInfo['con_id']);
        if (empty($competiteInfo)) {
            return $this->returnApi(201, "参数错误");
        }
        if ($competiteInfo['vote_start_time'] > date('Y-m-d H:i:s')) {
            return $this->returnApi(201, "投票未开始，暂不允许投票");
        }
        if ($competiteInfo['vote_end_time'] < date('Y-m-d H:i:s')) {
            return $this->returnApi(201, "投票已结束，不允许投票");
        }

        $competiteActivityVoteObj = new CompetiteActivityVote();
        if ($competiteInfo['vote_way'] == 1) {
            //查看用户共投了好多票
            $user_vote_num = $competiteActivityVoteObj->getVoteNumber($user_id, $competiteInfo['id']); //用户投票数量

            if ($competiteInfo['number'] !== 0 && $competiteInfo['number'] <= $user_vote_num) {
                return $this->returnApi(202, "投票次数已达到上限");
            }
        } else {
            $user_vote_num = $competiteActivityVoteObj->getVoteNumber($user_id, $competiteInfo['id'], null, date('Y-m-d')); //用户投票数量

            if ($competiteInfo['number'] !== 0 && $competiteInfo['number'] <= $user_vote_num) {
                return $this->returnApi(202, "今日投票次数已达到上限，请明日再来！");
            }
        }

        DB::beginTransaction();
        try {
            $worksInfo->vote_num = ++$worksInfo->vote_num;
            $worksInfo->save();

            $competiteActivityVoteObj->add([
                'con_id' => $competiteInfo['id'],
                'works_id' => $this->request->works_id,
                'yyy' => date('Y'),
                'mmm' => date('m'),
                'ddd' => date('d'),
                'date' => date('Y-m-d'),
                'user_id' => $user_id,
                'terminal' => 'wx',
            ]);

            DB::commit();
            return $this->returnApi(200, "投票成功", true, ['user_vote_num' => $user_vote_num + 1]); //返回用户已投票数量
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, "投票失败");
        }
    }

    /**
     * 撤销、删除作品
     * @param token  用户token
     * @param works_id 作品id
     * @param status 状态 4.已撤销， 5已删除 
     */
    public function  worksCancelAndDel()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('wx_works_cancel_del')->check($this->request->all())) {
            return $this->returnApi(201, $this->validate->getError());
        }
        if ($this->request->status != 4 && $this->request->status != 5) {
            return $this->returnApi(201, "参数错误");
        }
        $user_id = $this->request->user_info['id'];
        $account_id = $this->request->user_info['account_id'];

        $result = $this->worksModel->find($this->request->works_id);
        if (empty($result)) {
            return $this->returnApi(201, "参数错误");
        }
        if ($result['user_id'] != $user_id) {
            return $this->returnApi(201, "网络错误");
        }

        //判断是否有人已审核
        if ($result['status'] == 3) {
            $is_exists_check = CompetiteActivityWorksCheck::where('works_id', $this->request->works_id)->first();
            if ($is_exists_check) {
                return $this->returnApi(202, "当前作品已在审核流程中，不允许修改");
            }
        }

        if ($this->request->status == 4 &&  $result['status'] != 3) {
            return $this->returnApi(201, "此状态不允许撤销");
        } elseif ($this->request->status == 5 &&  ($result['status'] != 4 && $result['status'] != 2)) {
            return $this->returnApi(201, "此状态不允许删除");
        }
        DB::beginTransaction();
        try {
            $result->status = $this->request->status;
            $res = $result->save();

            /*消息推送*/
            $status_msg = $this->request->status == 5 ? '删除' : '撤销';
            $system_id = $this->systemAdd($status_msg . '在线投票,投稿的作品', $user_id, $account_id, 64, intval($result->id), '作品：【' . $result->title . '】' . $status_msg . '成功');

            /**执行积分规则 */  //撤销才执行，因为删除的作品，必须是撤销的
            // if (config('other.is_need_score') === true) {
            //     if (!empty($result->score) && $this->request->status == 4) {
            //         $scoreRuleObj = new ScoreRuleController();
            //         $score_msg = $scoreRuleObj->getScoreMsg($result->score);
            //         $scoreRuleObj->scoreReturn($this->score_type, $result->score, $result->user_id, $result->account_id, '在线投票撤销作品，' . $score_msg . ' ' . abs($result->score) . ' 积分', $system_id, '在线投票撤销作品');
            //     }
            // }

            DB::commit();
            return $this->returnApi(200, $this->request->status == 5 ? "删除成功" : '撤销成功', true);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, $this->request->status == 5 ? "删除失败" : '撤销失败');
        }
    }


    /**
     * 上传作品
     * @param token 用户token
     * @param con_id 大赛id
     * @param username 用户名
     * @param tel 用户电话
     * @param id_card 身份证号码
     * @param unit 单位、学校
     * @param wechat_number 微信号
     * @param reader_id 读者证号
     * @param adviser 指导老师
     * @param adviser_tel 指导老师联系方式
     * @param title 标题
     * @param type_id 类型id
     * @param img 作品图片地址 node=1
     * @param content 作品图片地址 node=2
     * @param voice 作品音频地址 node=3
     * @param voice_name 音频名称
     * @param video 视频资源 node=4
     * @param video_name 作品名称
     * @param pdf_works_address 作品PDF地址 node=5
     * @param intro 作品简介
     * @param cover 作品封面
     * @param width 宽度
     * @param height 高度
     */
    public function worksAdd()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('wx_works_add')->check($this->request->all())) {
            return $this->returnApi(201, $this->validate->getError());
        }
        //作品必须存在一个
        if (empty($this->request->img) && empty($this->request->content) && empty($this->request->voice) && empty($this->request->video) && empty($this->request->pdf_works_address)) {
            return $this->returnApi(201, "上传作品不能为空");
        }
        $user_id = $this->request->user_info['id'];
        $account_id = $this->request->user_info['account_id'];
        //验证封面
        if (empty($this->request->cover)) {
            if ($this->request->img) {
                $cover = str_replace('.', '_thumb.', $this->request->img);
                $this->request->merge(['cover' => $cover]);
            } else {
                return $this->returnApi(201, "请上传图片封面");
            }
        }

        /*检查是否违规*/
        $validateModel = new UserViolate();
        $isViolate = $validateModel->checkIsViolate($user_id, 'competite');
        if ($isViolate) {
            return $this->returnApi(201, '已达到违规次数上限,不能上传作品');
        }
        $competiteInfo = $this->model->where('is_del', 1)->find($this->request->con_id);
        if ($competiteInfo['node'] == 2 && empty($this->request->unit_id)) {
            return $this->returnApi(201, "单位id不能为空");
        }


        if (empty($competiteInfo)) {
            return $this->returnApi(201, "大赛不存在，不允许投稿");
        }
        if ($competiteInfo['con_start_time'] > date('Y-m-d H:i:s')) {
            return $this->returnApi(201, "投稿未开始，暂不允许投稿");
        }
        if ($competiteInfo['con_end_time'] < date('Y-m-d H:i:s')) {
            return $this->returnApi(201, "投稿已结束，不允许投稿");
        }
        if ($competiteInfo['deliver_way'] == 3) {
            return $this->returnApi(201,  '此大赛不支持个人账号上传作品');
        }

        $CompetiteActivityWorksTypeInfo = CompetiteActivityWorksType::where('is_del', 1)->find($this->request->type_id);
        if (empty($CompetiteActivityWorksTypeInfo)) {
            return $this->returnApi(201, "大赛类型不存在，不允许投稿");
        }

        //已上传数量，团队账号不受限
        if ($CompetiteActivityWorksTypeInfo['limit_num'] !== 0) {
            $have_number = $this->worksModel->getUserWorksNumberByTypeId($this->request->con_id, null, null, $user_id, $this->request->type_id);
            if ($CompetiteActivityWorksTypeInfo['limit_num'] <= $have_number) {
                return $this->returnApi(201, "此类作品，您上传的数量已达到上限，请在个人投稿记录中查看");
            }
        }

        //判断积分是否满足条件 后台审核统一增加
        // if (config('other.is_need_score') === true  && $competiteInfo->is_reader == 1) {
        //     $scoreRuleObj = new ScoreRuleController();
        //     $score_status = $scoreRuleObj->checkScoreStatus($this->score_type, $user_id, $account_id);
        //     if ($score_status['code'] == 202) return $this->returnApi(202, $score_status['msg']);
        // }


        DB::beginTransaction();
        try {
            //判断参数
            if (!empty($competiteInfo->real_info)) {
                $real_info = explode('|', $competiteInfo->real_info);
                $this->model->checkApplyParam($real_info, $this->request->all());
            }

            $score_manage_id = null;
            if ($competiteInfo['check_manage_id']) {
                $check_manage_id_all = explode(',', $competiteInfo['check_manage_id']);
                $status = 3;
                $check_manage_id = $check_manage_id_all[0];
            } else {
                $status = 1;
                $check_manage_id = null;

                //添加评审打分人信息
                if ($competiteInfo['appraise_way'] != 2) {
                    $score_manage_id = $CompetiteActivityWorksTypeInfo['score_manage_id'];
                }
            }

            $serial_number = $this->worksModel->getSerialNumber($CompetiteActivityWorksTypeInfo['letter']);

            $this->request->merge([
                'serial_number' => $serial_number,
                'user_id' => $user_id,
                'account_id' => $account_id,
                'status' => $status,
                'type' => 1, //固定个人上传
                'next_check_manage_id' => $check_manage_id,
                'next_score_manage_id' => $score_manage_id,
                'score' => !empty($score_status['score_info']['score']) ? $score_status['score_info']['score'] : 0
            ]);
            $this->worksModel->add($this->request->all());

            /*消息推送*/
            $system_id = $this->systemAdd('线上大赛投稿作品', $user_id, $account_id, 63, intval($this->worksModel->id), '线上大赛：【' . $competiteInfo->title . '】投稿成功，等待后台管理员审核！');

            // if (config('other.is_need_score') === true  && $competiteInfo->is_reader == 1) {
            //     if ($score_status['code'] == 200) {
            //         $scoreRuleObj->scoreChange($score_status, $user_id, $account_id, $system_id); //添加积分消息
            //     }
            // }

            DB::commit();
            return $this->returnApi(200, "投稿成功，等待后台管理员审核！", true); //返回用户已投稿数量
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, $e->getMessage());
        }
    }


    /**
     * 修改作品
     * @param token 用户token
     * @param works_id 作品id
     * @param con_id 大赛id
     * @param username 用户名
     * @param tel 用户电话
     * @param id_card 身份证号码
     * @param unit 用户单位/学校
     * @param wechat_number 微信号
     * @param reader_id 读者证号
     * @param adviser 指导老师
     * @param adviser_tel 指导老师联系方式
     * @param title 标题
     * @param type_id 类型id
     * @param img 作品图片地址 node=1
     * @param content 作品图片地址 node=2
     * @param voice 作品音频地址 node=3
     * @param voice_name 音频名称
     * @param video 视频资源 node=4
     * @param video_name 作品名称
     * @param pdf_works_address 作品PDF地址 node=5
     * @param intro 作品简介
     * @param cover 作品封面
     * @param width 宽度
     * @param height 高度
     */
    public function worksChange()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('wx_works_change')->check($this->request->all())) {
            return $this->returnApi(201, $this->validate->getError());
        }
        //作品必须存在一个
        if (empty($this->request->img) && empty($this->request->content) && empty($this->request->voice) && empty($this->request->video) && empty($this->request->pdf_works_address)) {
            return $this->returnApi(201, "上传作品不能为空");
        }
        //验证封面
        if (empty($this->request->cover)) {
            if ($this->request->img) {
                $cover = str_replace('.', '_thumb.', $this->request->img);
                $this->request->merge(['cover' => $cover]);
            } else {
                return $this->returnApi(201, "请上传图片封面");
            }
        }
        $res = $this->worksModel->find($this->request->works_id);
        if (empty($res)) {
            return $this->returnApi(201, "参数错误");
        }

        //    if ($res['status'] != 4 && $res['status'] != 2) {//审核未通过不允许撤销
        if ($res['status'] != 4) {
            return $this->returnApi(202, "请先撤销在进行修改！");
        }
        //判断是否有人已审核
        if ($res['status'] == 3) {
            $is_exists_check = CompetiteActivityWorksCheck::where('works_id', $this->request->works_id)->first();
            if ($is_exists_check) {
                return $this->returnApi(202, "当前作品已在审核流程中，不允许修改");
            }
        }
        $user_id = $this->request->user_info['id'];
        $competiteInfo = $this->model->where('is_del', 1)->find($this->request->con_id);

        if ($competiteInfo['node'] == 2 && empty($this->request->unit_id)) {
            return $this->returnApi(201, "单位id不能为空");
        }

        if (empty($competiteInfo)) {
            return $this->returnApi(201, "大赛不存在，不允许投稿");
        }
        if ($competiteInfo['con_start_time'] > date('Y-m-d H:i:s')) {
            return $this->returnApi(201, "投稿未开始，暂不允许投稿");
        }
        if ($competiteInfo['con_end_time'] < date('Y-m-d H:i:s')) {
            return $this->returnApi(201, "投稿已结束，不允许投稿");
        }
        if ($competiteInfo['deliver_way'] == 3) {
            return $this->returnApi(201,  '此大赛不支持个人账号上传作品');
        }
        $CompetiteActivityWorksTypeInfo = CompetiteActivityWorksType::where('is_del', 1)->find($this->request->type_id);
        if (empty($CompetiteActivityWorksTypeInfo)) {
            return $this->returnApi(201, "大赛类型不存在，不允许投稿");
        }

        //已上传数量
        if ($CompetiteActivityWorksTypeInfo['limit_num'] !== 0) {
            $have_number = $this->worksModel->getUserWorksNumberByTypeId($this->request->con_id, null, null, $user_id, $this->request->type_id);
            if ($CompetiteActivityWorksTypeInfo['limit_num'] <= $have_number - 1) {
                return $this->returnApi(201, "此类作品，您上传的数量已达到上线，请在个人投稿记录中查看");
            }
        }

        $old_pdf_works_address = $res['pdf_works_address'];
        $old_pdf_works_address_img = $res['pdf_works_address_img'];
        $old_img = $res['img'];
        $old_voice = $res['voice'];
        $old_video = $res['video'];
        DB::beginTransaction();
        try {
            //判断参数
            if (!empty($competiteInfo->real_info)) {
                $real_info = explode('|', $competiteInfo->real_info);
                $this->model->checkApplyParam($real_info, $this->request->all());
            }

            $data = $this->request->all();
            $data['id'] = $data['works_id'];
            $data['status'] = 3;
            unset($data['works_id']);
            $this->worksModel->change($data);

            //删除旧资源
            if ($this->request->pdf_works_address != $old_pdf_works_address) {
                $this->deleteFile($old_pdf_works_address);
                if ($old_pdf_works_address_img)
                    $pdf_works_address_img = explode('|', $old_pdf_works_address_img);
                $this->deleteFile($pdf_works_address_img);
            }
            if ($this->request->img != $old_img) {
                $this->deleteFile($old_img);
            }
            if ($this->request->voice != $old_voice) {
                $this->deleteFile($old_voice);
            }
            if ($this->request->video != $old_video) {
                $this->deleteFile($old_video);
            }

            DB::commit();
            return $this->returnApi(200, "修改成功，等待后台管理员审核！", true); //返回用户已投稿数量
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnApi(202, $e->getMessage() . $e->getFile() . $e->getLine());
        }
    }
}
