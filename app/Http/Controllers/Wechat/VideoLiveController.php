<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Controllers\Live\ServerAPIController;
use App\Models\VideoLiveVote;

/**
 * 视频直播管理
 */
class VideoLiveController extends CommonController
{
    protected $model;
    protected $validate;

    public function __construct()
    {
        parent::__construct();

        $this->model = new \App\Models\VideoLive();
        $this->validate = new  \App\Validate\VideoLiveValidate();
    }

    /**
     * 列表
     * @param page int 页码
     * @param limit int 分页大小
     * @param keywords string 搜索关键词
    //  * @param start_time datetime 活动开始时间    数据格式  年月日
    //  * @param end_time datetime 活动结束时间
     */
    public function lists()
    {
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;
        $res = $this->model->lists(null, $keywords, 1, null, null, null, null, $limit);

        if ($res['total'] == 0 || !$res['data']) {
            return $this->returnApi(203, "暂无数据");
        }
        $videoLiveVoteModel = new VideoLiveVote();
        foreach ($res['data'] as $key => $val) {
            $res['data'][$key]['intro'] = str_replace('&nbsp;', '', strip_tags($val['intro']));
            $res['data'][$key]['status'] = $this->model->getVideoLiveStatus($val);
            $res['data'][$key]['size'] = format_bytes($val['size']);
            $res['data'][$key]['total_time'] = format_bytes(substr($val['total_time'], 0, 10));
            $res['data'][$key]['vote_num'] = $videoLiveVoteModel->where('act_id', $val['id'])->count(); //点赞量
            //  $res['data'][$key]['manage_name'] = Manage::getManageNameByManageId($val['manage_id']);
            unset($res['data'][$key]['push_url']);
        }
        $res = $this->disPageData($res);
        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 详情
     * @param token 用户 token值 可选
     * @param id int 活动id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('wx_detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $user_id = $this->request->user_info['id'];

        $field = [
            'id', 'cid', 'title', 'img', 'host_handle', 'tel', 'browse_num', 'size', 'total_time', 'status', 'http_pull_url', 'hls_pull_url', 'rtmp_pull_url',
            'address', 'video_num', 'intro', 'create_time', 'start_time', 'end_time', 'qr_url'
        ];
        $res = $this->model->detail($this->request->id, 1, $field);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }

        $res->size = format_bytes($res->size);
        $res->total_time = format_bytes(substr($res->total_time, 0, 10));

        $res = $res->toArray();
        if ($res['status'] != 3) {
            unset($res['con_video_resource']);
        }
        $videoLiveVoteModel = new VideoLiveVote();
        if (!empty($user_id)) {
            $is_vote = $videoLiveVoteModel->isVote($user_id, $res['id']);
            $res['is_vote'] = $is_vote ? true : false;
        } else {
            $res['is_vote'] = false; //未点赞
        }
        $res['vote_num'] = $videoLiveVoteModel->where('act_id', $res['id'])->count(); //点赞量

        //添加浏览量
        $this->model->where('id', $res['id'])->increment('browse_num');
        unset($res['cid']);
        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 获取直播状态
     * @param id int 活动id
     */
    public function getVideoLiveStatus()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('wx_get_video_live_status')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $field = [
            'id', 'cid', 'title', 'browse_num', 'total_time', 'status', 'create_time', 'start_time', 'end_time'
        ];
        $res = $this->model->detail($this->request->id, 1, $field);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }
        $res = $res->toArray();
      
        $serverAPIObj = new ServerAPIController();
        $channelInfo = $serverAPIObj->channelStats($res['cid']);
        $wy_status = null;
        if ($channelInfo) {
            $wy_status = $channelInfo['ret']['status']; //0：空闲； 1：直播； 2：禁用； 3：直播录制。
        }
        unset($res['cid']);
        return $this->returnApi(200, "查询成功", true, ['status' => $res['status'], 'wy_status' => $wy_status]);
    }


    /**
     * 点赞与取消点赞
     * @param token 用户 token值 必选
     * @param id int 活动id
     */
    public function voteAndCancel()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('wx_vote_and_cancel')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $user_id = $this->request->user_info['id'];

        $res = $this->model->detail($this->request->id);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }

        $videoLiveVoteModel = new VideoLiveVote();
        list($code, $msg) = $videoLiveVoteModel->voteAndCancel($user_id, $this->request->id);

        return $this->returnApi($code, $msg, true);
    }
}
