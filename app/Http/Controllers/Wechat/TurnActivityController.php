<?php

namespace App\Http\Controllers\Wechat;

use App\Models\TurnActivity;
use App\Models\TurnActivityGift;
use App\Models\TurnActivityLimitAddress;
use App\Models\TurnActivityResource;
use App\Models\TurnActivityUserTurnRecord;
use App\Models\TurnActivityUserPrize;
use App\Models\TurnActivityUserUsePrizeRecord;
use App\Models\UserInfo;
use App\Validate\TurnActivityValidate;
use Illuminate\Support\Facades\Log;

/**
 * 在线抽奖活动管理
 */
class TurnActivityController extends CommonController
{
    public $model = null;
    public $applyModel = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new TurnActivity();
        $this->validate = new TurnActivityValidate();
    }

    /**
     * 在线抽奖活动列表 （根据类型不同，调用不同的接口  act_type_id 为 2 调用此接口）
    //  * @param act_type_id   类型id
     * @param page int 页码
     * @param limit int 分页大小
     * @param keywords string 搜索关键词
     */
    public function lists()
    {
        //增加验证场景进行验证
        // if (!$this->validate->scene('wx_list')->check($this->request->all())) {
        //     return $this->returnApi(201,  $this->validate->getError());
        // }
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;

        $res = $this->model->lists(['id', 'img', 'title', 'start_time', 'end_time', 'turn_start_time', 'turn_end_time', 'browse_num'], $keywords, 1, null, null, null, null, $page, $limit);

        //增加行为分析
        $appInviteCodeBehaviorModel = new  \App\Models\AppInviteCodeBehavior();
        $appInviteCodeBehaviorModel->add(27);

        if ($res['total'] == 0 || !$res['data']) {
            return $this->returnApi(203, "暂无数据");
        }

        foreach ($res['data'] as $key => $val) {
            $res['data'][$key]['status'] = $this->model->getActListStatus($val);
            $res['data'][$key]['start_time'] = date('Y-m-d H:i', strtotime($val['start_time']));
            $res['data'][$key]['end_time'] = date('Y-m-d H:i', strtotime($val['end_time']));
        }
        $res = $this->disPageData($res);
        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 获取在线抽奖活动详情 （根据类型不同，调用不同的接口  act_type_id 为 1 调用此接口）
     * @param id int id
     * @param token 用户token
     * 
     * @param lon 经度
     * @param lat 纬度
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('wx_detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        //判断经纬度是否符合参赛要求
        if (empty($this->request->lon) || empty($this->request->lat)) {
            return $this->returnApi(211, "请开启手机定位功能");
        }
        $turnActivityLimitAddressModel = new TurnActivityLimitAddress();
        $lon_lat_info = $turnActivityLimitAddressModel->checkLonLat($this->request->id, $this->request->lon, $this->request->lat);
        if ($lon_lat_info !== true) {
            return $this->returnApi(210, "您当前位置不在活动范围");
        }

        $field = [
            'id', 'title', 'img', 'node', 'number', 'round_number', 'execute_day', 'technical_support',
            'share_number', 'rule_type', 'rule', 'start_time', 'end_time', 'end_time', 'turn_start_time', 'is_show_address',
            'turn_end_time', 'vote_way', /* 'invite_code',  'is_play', */ 'create_time', 'manage_id', 'share_title', 'share_img'
        ];

        $res = $this->model->detail($this->request->id, $field, 1);

        if (!$res) {
            return $this->returnApi(202, "活动不存在或已结束");
        }
        $res = $res->toArray();

        //获取用户基本信息
        $userInfoModel = new UserInfo();
        $user_info = $userInfoModel->getWechatInfo($this->request->token);

        $res['user_info']['nickname'] = !empty($user_info) ? $user_info['nickname'] : '';
        $res['user_info']['head_img'] = !empty($user_info) ? $user_info['head_img'] : '';
        //获取用户今日抽奖次数
        $activityUserUsePrizeRecordModel = new TurnActivityUserUsePrizeRecord();
        $key_number = $activityUserUsePrizeRecordModel->getSurplusPrizeNumber($this->request->id, $this->request->token, $res);
        $res['user_info']['key_number'] = $key_number; //抽奖钥匙    -1 代表在抽奖时间段内但是今日不可抽奖 ， 0 表示今日无抽奖机会 ，其他正整数表示可以抽奖

        //活动状态
        $res['status'] = $this->model->getActListStatus($res);

        //判断是否需要地址
        $res['is_show_address'] = $res['is_show_address'] == 1 ? true : false;

        //更新浏览量
        $this->model->updateBrowseNumber($this->request->id);

        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 获取资源路径及颜色等信息
     * @param id 活动id
     */
    public function getResourceInfo()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('wx_resource_info')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $turnActivityResourceModel = new TurnActivityResource();
        $res = $turnActivityResourceModel->detail($this->request->id);
        if (empty($res)) {
            return $this->returnApi(202, "获取失败");
        }
        $res->act_title = $this->model->where('id', $this->request->id)->value('title');
        return $this->returnApi(200, "查询成功", true, $res);
    }
}
