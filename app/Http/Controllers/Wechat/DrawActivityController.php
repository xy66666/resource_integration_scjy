<?php

namespace App\Http\Controllers\Wechat;

use App\Models\AnswerActivity;
use App\Models\ContestActivity;
use App\Models\ContestActivityWorks;
use App\Models\TurnActivity;

/**
 * 抽奖活动推荐记录
 * Class ContestActivityType
 * @package app\admin\controller
 */
class DrawActivityController extends CommonController
{
    public $model = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 抽奖活动推荐列表(3种活动)
     */
    public function getRecomDrawActivity()
    {
        //获取答题
        $answerActivityModel = new AnswerActivity();
        $answer_activity = $answerActivityModel->lists(['id', 'img', 'title', 'start_time', 'end_time', 'answer_start_time', 'answer_end_time'], null, 1, null, null, null, null, null, null, null, 1, 3);
        
        if ($answer_activity['total'] == 0 || !$answer_activity['data']) {
            $answer_activity = [];
        } else {
            foreach ($answer_activity['data'] as $key => $val) {
                $answer_activity['data'][$key]['status'] = $answerActivityModel->getActListStatus($val);
                $answer_activity['data'][$key]['type'] = 1;
            }
        }

        $turnActivityModel = new TurnActivity();
        $turn_activity = $turnActivityModel->lists(['id', 'img', 'title', 'start_time', 'end_time', 'turn_start_time', 'turn_end_time'], null, 1, null, null, null, null, 1, 1);

        if ($turn_activity['total'] == 0 || !$turn_activity['data']) {
            $turn_activity = [];
        } else {
            foreach ($turn_activity['data'] as $key => $val) {
                $turn_activity['data'][$key]['status'] = $turnActivityModel->getActListStatus($val);
                $turn_activity['data'][$key]['type'] = 2;
            }
        }
        //获取大赛
        $contestActivityModel = new ContestActivity();
        $contest_activity = $contestActivityModel->lists(0, null, null, null, null,null,1, 2);

        if (empty($contest_activity['data'])) {
            $contest_activity = [];
        } else {
            $worksModel = new ContestActivityWorks();
            foreach ($contest_activity['data'] as $key => $val) {
                $contest_activity['data'][$key]['take_number'] = $worksModel->getTakeNumber($val['id']); //参与人数
                $contest_activity['data'][$key]['status'] = $contestActivityModel->getContestStatus($val); //获取活动状态
                $contest_activity['data'][$key]['type'] = 3;
            }
        }

        return $this->returnApi(
            200,
            "查询成功",
            true,
            [
                'answer_activity' => !empty($answer_activity['data']) ? $answer_activity['data'] : [],
                'turn_activity' => !empty($turn_activity['data']) ? $turn_activity['data'] : [],
                'contest_activity' => !empty($contest_activity['data']) ? $contest_activity['data'] : []
            ]
        );
    }
}
