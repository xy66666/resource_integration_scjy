<?php

namespace App\Http\Controllers\Wechat;

use App\Models\AnswerActivityArea;
use App\Validate\AnswerActivityAreaValidate;
use Illuminate\Support\Facades\DB;

/**
 * 活动区域管理
 */
class AnswerActivityAreaController extends CommonController
{

    public $model = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new AnswerActivityArea();
        $this->validate = new AnswerActivityAreaValidate();
    }

    /**
     * 列表
     * @param act_id int 活动id
     * @param page int 当前页
     * @param limit int 分页大小
     * @param keywords string 搜索关键词(类型名称)
     */
    public function lists()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('wx_list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;
        $act_id = $this->request->act_id;

        //判断是否是区域联盟活动
        $isAllowAddArea = $this->model->isAllowAddArea($this->request->act_id);
        if ($isAllowAddArea !== true) {
            return $this->returnApi(202, $isAllowAddArea);
        }

        $condition[] = ['is_del', '=', 1];

        if ($keywords) {
            $condition[] = ['name', 'like', "%$keywords%"];
        }
        if ($act_id) {
            $condition[] = ['act_id', '=', $act_id];
        }

        return $this->model->getSimpleList(['id', 'act_id', 'name', 'img', 'intro'], $condition, $page, $limit, '`order` asc');
    }

    
}
