<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Controllers\Admin\CommonController;
use App\Http\Controllers\Controller;
use App\Models\AnswerActivity;
use App\Models\AnswerActivityRanking;
use App\Models\AnswerActivityUnit;
use App\Models\AnswerActivityUserGift;
use App\Models\UserInfo;
use App\Validate\AnswerActivityRankingValidate;
use App\Validate\AnswerActivityUserGiftValidate;
use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

/**
 * 用户活动获取礼物
 */
class AnswerActivityUserGiftController extends CommonController
{

    public $model = null;
    public $activityModel = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new AnswerActivityUserGift();
        $this->activityModel = new AnswerActivity();
        $this->validate = new AnswerActivityUserGiftValidate();
    }

    /**
     * 获取用户礼物列表
     * @param token int 用户token
     * @param act_id int 活动id
     * @param page int 页码
     * @param limit int 分页大小
     */
    public function lists()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;

        $act_info = $this->activityModel->detail($this->request->act_id);

        if (empty($act_info)) {
            return $this->returnApi(201, '参数错误');
        }
        if ($act_info['prize_form'] == 1) {
            return $this->returnApi(201, '此活动为排名类活动');
        }
        $res = $this->model->lists($this->request->act_id, $this->request->token, null, null, null, null, null, $page, $limit);
        if (empty($res['data'])) {
            return $this->returnApi(203, '暂无数据');
        }

        foreach ($res['data'] as $key => $val) {
            if ($act_info['node'] != 1) {
                if ($val['unit_id']) {
                    $res['data'][$key]['unit_name'] = AnswerActivityUnit::where('act_id', $this->request->act_id)->where('id', $val['unit_id'])->value('name');
                }
            }
            $res['data'][$key]['img'] = $val['type'] == 1 ? 'default/default_red_img.jpg' : $val['img'];
        }

        return $this->returnApi(200, '获取成功', true, $res);
    }
}
