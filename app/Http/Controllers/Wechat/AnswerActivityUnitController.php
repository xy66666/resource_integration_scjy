<?php

namespace App\Http\Controllers\Wechat;

use App\Models\AnswerActivity;
use App\Models\AnswerActivityBrowseCount;
use App\Models\AnswerActivityUnit;
use App\Models\AnswerActivityUnitUserNumber;
use App\Models\AnswerActivityUserAnswerTotalNumber;
use App\Models\AnswerActivityUserCorrectTotalNumber;
use App\Models\AnswerActivityUserStairsTotalNumber;
use App\Validate\AnswerActivityUnitValidate;
use Illuminate\Support\Facades\DB;

/**
 * 活动单位管理
 */
class AnswerActivityUnitController extends CommonController
{

    public $model = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new AnswerActivityUnit();
        $this->validate = new AnswerActivityUnitValidate();
    }

    /**
     * 列表
     * @param act_id int 活动id
     * @param area_id int 区域id
     * @param page int 当前页
     * @param limit int 分页大小
    //  * @param nature int 单位性质   1 图书馆  2 文化馆  3 博物馆
    //  * @param start_time datetime 创建时间范围搜索(开始) 
    //  * @param end_time datetime 创建时间范围搜索(结束) 
     * @param keywords string 搜索关键词(类型名称)
     * 
     * @param lon int 经度 选填
     * @param lat int 纬度 选填
     */
    public function lists()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('wx_list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        // $start_time = $this->request->start_time;
        // $end_time = $this->request->end_time;
        // $nature = $this->request->nature;
        $keywords = $this->request->keywords;
        $area_id = $this->request->area_id;
        $act_id = $this->request->act_id;
        $lon = $this->request->lon;
        $lat = $this->request->lat;

        //判断是否是单位联盟活动
        $isAllowAddUnit = $this->model->isAllowAddUnit($this->request->act_id);
        if ($isAllowAddUnit !== true) {
            return $this->returnApi(202, $isAllowAddUnit);
        }

        $res = $this->model->lists(['id', 'act_id', 'area_id', 'name', 'img', 'intro', 'words', 'letter'], $act_id, $keywords, $area_id, null,null, null, null, $lon,$lat, $limit);

        if ($res['total'] == 0 || !$res['data']) {
            return $this->returnApi(203, "暂无数据");
        }
        //除轮次排名外（每次多少题）,单纯排名不需要此参数，其余都是多少题获得一次抽奖机会
        $activityInfo = AnswerActivity::select('is_loop', 'answer_number', 'total_floor', 'pattern')->where('id', $this->request->act_id)->first();

        foreach ($res['data'] as $key => $val) {
            $res['data'][$key]['intro'] = strip_tags($val['intro']);
            $res['data'][$key]['area_name'] = !empty($val['con_area']['name']) ?  $val['con_area']['name'] : '';

            //获取单位答题次数 如果是轮次答题，就是轮次次数
            $user_answer_number = $this->model->unitAnswerNumber($activityInfo['pattern'], $this->request->token, $val['act_id'], $val['id']);

            $user_answer_number = $user_answer_number ?: 0;

            $res['data'][$key]['total_floor'] = $activityInfo['total_floor']; //爬楼梯梯模式，总楼层
            $res['data'][$key]['answer_number'] = $activityInfo['answer_number'];
            $res['data'][$key]['user_answer_number'] = $activityInfo['is_loop'] == 1 ? $user_answer_number % $activityInfo['answer_number'] : $user_answer_number;

            unset($res['data'][$key]['con_area']);
        }

        return $this->returnApi(200, "查询成功", true, $res);
    }

 

    /**
     * 详情
     * @param token int 用户token
     * @param id int 单位id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->detail($this->request->id, ['id', 'act_id', 'img', 'name', 'nature', 'intro']);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }

        //除轮次排名外（每次多少题）,单纯排名不需要此参数，其余都是多少题获得一次抽奖机会
        $activityInfo = AnswerActivity::select('is_loop', 'answer_number', 'total_floor', 'pattern')->where('id', $res->act_id)->first();

        //获取单位答题次数 如果是轮次答题，就是轮次次数
        $user_answer_number = $this->model->unitAnswerNumber($activityInfo['pattern'],$this->request->token, $res['act_id'], $res['id']);
        $user_answer_number = $user_answer_number ?: 0;

        //馆内答题   答题进度：  1/5      爬楼梯：答题进度 52/100        轮次：已答轮次：5轮   
        $res['total_floor'] = $activityInfo['total_floor']; //爬楼梯梯模式，总楼层  
        $res['answer_number'] = $activityInfo['answer_number'];
        $res['user_answer_number'] = $activityInfo['is_loop'] == 1 ? $user_answer_number % $activityInfo['answer_number'] : $user_answer_number;


        $answerActivityBrowseCountModel = new AnswerActivityBrowseCount();
        $answerActivityBrowseCountModel->updateBrowseNumber($res['act_id'], $res['id'], 10, 30);

        return $this->returnApi(200, "获取成功", true, $res->toArray());
    }
}
