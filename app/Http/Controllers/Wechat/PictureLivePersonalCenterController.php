<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Controllers\PictureLiveUploadsFileController;
use App\Models\PictureLive;
use App\Models\PictureLiveVote;
use App\Models\PictureLiveWorks;
use App\Models\UserInfo;
use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * 图片直播个人中心管理
 */
class PictureLivePersonalCenterController extends CommonController
{
    protected $model;
    protected $validate;

    public function __construct()
    {
        parent::__construct();

        $this->model = new \App\Models\PictureLive();
        $this->validate = new  \App\Validate\PictureLiveValidate();
    }
    /**
     * 我浏览过的活动列表
     * @param page int 页码
     * @param limit int 分页大小
     * @param keywords string 搜索关键词
     */
    public function myBrowseLists()
    {
        //增加验证场景进行验证
        // if (!$this->validate->scene('wx_list')->check($this->request->all())) {
        //     return $this->returnApi(201,  $this->validate->getError());
        // }
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;
        $user_id = $this->request->user_info['id'];

        $res = $this->model->myBrowseLists($user_id, $keywords, null, null, $limit);

        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }
        $pictureLiveWorksModel = new PictureLiveWorks();
        foreach ($res['data'] as $key => $val) {
            $res['data'][$key]['status'] = $this->model->getPictureLiveStatus($val);
            $res['data'][$key]['works_num'] = $pictureLiveWorksModel->getWorksNumber($val['id'], [1]);

            $res['data'][$key]['con_start_time'] = date('Y-m-d H:i',  strtotime($val['con_start_time']));
            $res['data'][$key]['con_end_time'] = date('Y-m-d H:i',  strtotime($val['con_end_time']));
            $res['data'][$key]['vote_start_time'] = date('Y-m-d H:i',  strtotime($val['vote_start_time']));
            $res['data'][$key]['vote_end_time'] = date('Y-m-d H:i',  strtotime($val['vote_end_time']));
        }

        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 我的图片直播活动列表及图片
     * @param token int 用户token
     * @param page int 页码
     * @param limit int 分页大小
     */
    public function myPictureLiveLists()
    {

        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $user_id = $this->request->user_info['id'];

        $res = $this->model->myPictureLiveLists($user_id, $limit);

        if (empty($res) || empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }
        $res = $this->disPageData($res);
        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 我的图片列表
     * @param act_id int 活动id
     * @param token int 用户token
     * @param page int 页码
     * @param limit int 分页大小
     * @param status string 状态    1.已通过   2.未通过   3.未审核(默认)  4.已撤销
     * @param keywords string 搜索关键词(文章标题)
     * @param start_time datetime 开始时间(开始)
     * @param end_time datetime 结束时间(开始)
     * @param sort 字段排序  1 默认  2 浏览量  3 点赞 
     * @param order 排序方式  1 正序 2倒序
     */
    public function myPictureLivePhotoLists()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('wx_my_picture_live_photo_list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;
        $sort = $this->request->sort;
        $order = $this->request->order;
        $status = $this->request->status;
        $user_id = $this->request->user_info['id'];
        $pictureLiveWorksModel = new PictureLiveWorks();
        $res = $pictureLiveWorksModel->lists(
            ['id', 'cover', 'thumb_img', 'width', 'height', 'size', 'ratio', 'browse_num', 'vote_num', 'status', 'create_time'],
            $this->request->act_id,
            $user_id,
            $keywords,
            $status,
            null,
            null,
            2,
            $sort,
            $order,
            $limit
        );

        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }

        return $this->returnApi(200, "查询成功", true, $res);
    }



    /**
     * 我的点赞过的直播活动列表及图片
     * @param token int 用户token
     * @param page int 页码
     * @param limit int 分页大小
     */
    public function myVotePictureLiveLists()
    {

        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $user_id = $this->request->user_info['id'];

        $res = $this->model->myVotePictureLiveLists($user_id, $limit);

        if (empty($res) || empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }
        $res = $this->disPageData($res);
        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 我点赞过的图片列表
     * @param act_id int 活动id
     * @param token int 用户token
     * @param page int 页码
     * @param limit int 分页大小
    // * @param status string 状态    1.已通过 
     * @param keywords string 搜索关键词(文章标题)
     * @param start_time datetime 开始时间(开始)
     * @param end_time datetime 结束时间(开始)
     * @param sort 字段排序  1 默认  2 浏览量  3 点赞 
     * @param order 排序方式  1 正序 2倒序
     */
    public function myVotePictureLivePhotoLists()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('wx_my_vote_picture_live_photo_list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;
        $sort = $this->request->sort;
        $order = $this->request->order;
        $user_id = $this->request->user_info['id'];
        $pictureLiveWorksModel = new PictureLiveWorks();
        $res = $pictureLiveWorksModel->lists(
            ['id', 'cover', 'thumb_img', 'width', 'height', 'size', 'ratio', 'browse_num', 'vote_num', 'status', 'create_time'],
            $this->request->act_id,
            $user_id,
            $keywords,
            [1],
            null,
            null,
            1,
            $sort,
            $order,
            $limit
        );

        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }

        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 获取我的图片详情 
     * @param id int id
     * @param token 用户token
     */
    public function myPictureLiveDetail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('wx_my_picture_live_detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $user_id = $this->request->user_info['id'];
        $field = ['id', 'act_id', 'user_id', 'title', 'cover', 'thumb_img', 'width', 'height', 'size', 'ratio', 'browse_num', 'vote_num', 'create_time'];
        $res = $this->model->detail($this->request->id, 1, $field, $user_id);

        if (!$res) {
            return $this->returnApi(202, "图片不存在");
        }
        $res = $res->toArray();
        if ($res['user_id']) {
            $wechat_info = UserInfo::getWechatField($res['user_id'], ['nickname', 'head_img']);
            $res['nickname'] = $wechat_info['nickname'];
            $res['head_img'] = $wechat_info['head_img'];
        } else {
            $res['nickname'] = '';
            $res['head_img'] = '';
        }
        $res['size'] = format_bytes($res['size']); //格式化字节数

        $pictureLiveModel = new PictureLive();
        // $pictureLiveVoteModel = new PictureLiveVote();
        $act_info = $pictureLiveModel->detail($res['act_id'], 1, ['id', 'vote_way', 'number', 'con_start_time', 'con_end_time', 'vote_start_time', 'vote_end_time']);
        // if ($user_id) {
        //     $res['user_vote_num'] = $pictureLiveVoteModel->userVoteNum($act_info['vote_way'], $user_id, $res['act_id'], $res['id']); //用户投票数
        // } else {
        //     $res['user_vote_num'] = 0; //获取活动状态
        // }
        // list($act_info->vote_msg, $act_info->vote_number) = $pictureLiveVoteModel->getVoteNum($act_info->vote_way, $act_info->number, $user_id, $res['act_id']);
        $res['picture_live'] = $act_info;

        return $this->returnApi(200, "查询成功", true, $res);
    }
}
