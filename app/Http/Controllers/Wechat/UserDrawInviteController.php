<?php

namespace App\Http\Controllers\Wechat;

use App\Models\AnswerActivity;
use App\Models\AnswerActivityInvite;
use App\Models\TurnActivity;
use App\Models\TurnActivityInvite;
use App\Models\UserAddress;
use App\Models\UserInfo;
use App\Models\UserUnit;
use App\Validate\UserAddressValidate;
use App\Validate\UserDrawInviteValidate;

/**
 * 用户邀请码管理
 */
class UserDrawInviteController extends CommonController
{
    public $model = null;
    public $userAddressModel = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new UserInfo();
        $this->validate = new UserDrawInviteValidate();
    }

    /**
     * 是否需要邀请码 且 是否已经输入了邀请码
     * @param  token 用户guid
     * @param  type 类型  1、答题活动   2、在线抽奖
     * @param  act_id 活动id
     */
    public function checkInvitCode()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('check_invite_code')->check($this->request->all())) {
            return $this->returnApi(201, $this->validate->getError());
        }
        //获取数据信息 
        $act_info = $this->getActInfo($this->request->type, $this->request->act_id);

        if (empty($act_info) || $act_info['end_time'] < date('Y-m-d H:i:s')) {
            return $this->returnApi(202, '活动不存在或已结束');
        }

        if ($act_info['start_time'] < date('Y-m-d H:i:s') && $act_info['end_time'] > date('Y-m-d H:i:s')) {
            return $this->returnApi(200, '活动已开始', true, ['status' => 1]); //活动已开始，直接进入活动
        }
        if ($act_info['start_time'] > date('Y-m-d H:i:s') && empty($act_info['invite_code'])) {
            return $this->returnApi(202, '活动未开始');
        }

        //判断是否输入邀请码 和邀请码是否正确
        $checkInviteCode = $this->checkInviteCode($this->request->type, $this->request->token, $this->request->act_id, $act_info['invite_code']);
        if ($checkInviteCode === true) {
            return $this->returnApi(200, '邀请码输入正确', true, ['status' => 2]); //邀请码输入正确，直接进入活动
        }

        return $this->returnApi(200, '请输入邀请码', true, ['status' => 3, 'start_time' => $act_info['start_time']]); //请输入邀请码，才能进入活动
    }

    /**
     * 输入邀请码  
     * @param  token 用户guid
     * @param  act_id 活动id
     * @param  type 类型  1、答题活动   2、在线抽奖
     * @param  invite_code   输入邀请码
     */
    public function setInvitCode()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('set_invite_code')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        //获取数据信息 
        $act_info = $this->getActInfo($this->request->type, $this->request->act_id);

        if (empty($act_info) || $act_info['end_time'] < date('Y-m-d H:i:s')) {
            return $this->returnApi(202,  '活动不存在或已结束');
        }

        //直接不验证邀请码
        if ($act_info['start_time'] < date('Y-m-d H:i:s') && $act_info['end_time'] > date('Y-m-d H:i:s')) {
            return $this->returnApi(200,  '活动已开始', true, ['status' => 1]); //活动已开始，直接进入活动
        }

        if ($act_info['start_time'] > date('Y-m-d H:i:s') && empty($act_info['invite_code'])) {
            return $this->returnApi(202,  '活动未开始');
        }

        if ($act_info['invite_code'] !== $this->request->invite_code) {
            return $this->returnApi(200,  '邀请码输入错误', true, ['status' => 3, 'start_time' => $act_info['start_time']]); //重新输入邀请码，才能进入活动
        }

        //判断是否输入邀请码 和邀请码是否正确
        $res = $this->inviteAdd($this->request->type, [
            'user_guid' => $this->request->token,
            'act_id' => $this->request->act_id,
            'invite_code' => $this->request->invite_code
        ]);
        if ($res) {
            return $this->returnApi(200,  '邀请码输入正确', true, ['status' => 2]);
        }
        return $this->returnApi(200,  '邀请码输入错误', true, ['status' => 3, 'start_time' => $act_info['start_time']]); //重新输入邀请码，才能进入活动
    }

    /**
     * 获取活动信息
     * @param  type 类型  1、答题活动   2、在线抽奖
     * @param  act_id  活动id
     **/
    public function getActInfo($type, $id)
    {
        $act_info = [];
        if ($type == 1) {
            $model = new AnswerActivity();
            $act_info = $model->detail($id, ['id','node','share_number', 'title', 'start_time', 'end_time', 'is_ushare', 'real_info', 'is_need_unit', 'invite_code'], 1);
        } else if ($type == 2) {
            $model = new TurnActivity();
            $act_info = $model->detail($id, ['id','node','share_number', 'title', 'start_time', 'end_time', 'is_ushare', 'real_info', 'invite_code'], 1);
        }
        return $act_info;
    }

    /**
     * 判断邀请码是否正确
     * @param  type 类型  1、答题活动   2、在线抽奖
     * @param  act_id  活动id
     **/
    public function checkInviteCode($type, $user_guid, $act_id, $invite_code)
    {
        $checkInviteCode = false;
        if ($type == 1) {
            $model = new AnswerActivityInvite();
            $checkInviteCode = $model->checkInviteCode($user_guid, $act_id, $invite_code);
        } else if ($type == 2) {
            $model = new TurnActivityInvite();
            $checkInviteCode = $model->checkInviteCode($user_guid, $act_id, $invite_code);
        }
        return $checkInviteCode;
    }

    /**
     * 获取活动邀请码
     * @param  type 类型  1、答题活动   2、在线抽奖
     * @param  act_id  活动id
     **/
    public function inviteAdd($type, $data)
    {
        if ($type == 1) {
            $model = new AnswerActivityInvite();
            $res = $model->add($data);
        } else if ($type == 2) {
            $model = new TurnActivityInvite();
            $res = $model->add($data);
        }
        return $res;
    }
}
