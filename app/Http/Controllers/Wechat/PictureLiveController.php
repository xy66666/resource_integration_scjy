<?php

namespace App\Http\Controllers\Wechat;

use App\Models\PictureLiveBrowse;
use App\Models\PictureLiveWorks;
use App\Models\VideoLive;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * 图片直播
 */
class PictureLiveController extends CommonController
{
    protected $model;
    protected $validate;

    public function __construct()
    {
        parent::__construct();

        $this->model = new \App\Models\PictureLive();
        $this->validate = new  \App\Validate\PictureLiveValidate();
    }


    /**
     * 直播活动列表 （根据类型不同，调用不同的接口  act_type_id 为 4 调用此接口）
     * @param page int 页码
     * @param limit int 分页大小
     * @param keywords string 搜索关键词
     */
    public function lists()
    {
        //增加验证场景进行验证
        // if (!$this->validate->scene('wx_list')->check($this->request->all())) {
        //     return $this->returnApi(201,  $this->validate->getError());
        // }
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;

        $res = $this->model->lists(['id', 'img', 'title', 'con_start_time', 'con_end_time', 'vote_start_time', 'vote_end_time', 'create_time', 'browse_num'], $keywords, 1, null, null, null, null, $limit);

        //增加行为分析
        $appInviteCodeBehaviorModel = new  \App\Models\AppInviteCodeBehavior();
        $appInviteCodeBehaviorModel->add(29);

        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }

        foreach ($res['data'] as $key => $val) {
            $res['data'][$key]['status'] = $this->model->getPictureLiveStatus($val);
            $res['data'][$key]['con_start_time'] = date('Y-m-d H:i', strtotime($val['con_start_time']));
            $res['data'][$key]['con_end_time'] = date('Y-m-d H:i', strtotime($val['con_end_time']));
        }

        return $this->returnApi(200, "查询成功", true, $res);
    }

    /**
     * 直播活动（图片+视频）列表 （根据类型不同，调用不同的接口  act_type_id 为 4 调用此接口）
     * @param page int 页码
     * @param limit int 分页大小
     * @param keywords string 搜索关键词
     */
    public function liveLists()
    {
        //增加验证场景进行验证
        // if (!$this->validate->scene('wx_list')->check($this->request->all())) {
        //     return $this->returnApi(201,  $this->validate->getError());
        // }
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;

        $res = $this->model->liveLists($keywords, 1, null, null, null, null, $limit);
        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }
        $videoLiveModel = new VideoLive();
        foreach ($res['data'] as $key => $val) {
            $res['data'][$key]['status'] = $val['node'] == 1 ? $this->model->getPictureLiveStatusV2($val) : $videoLiveModel->getVideoLiveStatus($val);
            $res['data'][$key]['start_time'] = date('Y-m-d H:i', strtotime($val['start_time']));
            $res['data'][$key]['end_time'] = date('Y-m-d H:i', strtotime($val['end_time']));
        }

        $res['wechat_channel_link'] = config('other.wechat_channel_link'); //视频号直播链接，存在则显示
        return $this->returnApi(200, "查询成功", true, $res);
    }


    /**
     * 获取直播活动详情 （根据类型不同，调用不同的接口  act_type_id 为 4 调用此接口）
     * @param id int id   或 二维码 token值
     * @param token 用户token
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('wx_detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $user_id = $this->request->user_info['id'];
        $field = [
            'id', 'title', 'main_img', 'img', 'con_start_time', 'con_end_time', 'vote_start_time', 'vote_end_time', 'address',
            'uploads_number', 'uploads_way', 'is_appoint', 'intro', 'browse_num', 'qr_url', 'create_time', 'theme_color', 'animation', 'animation_time',
            'share_title','share_img','vote_way'
        ];
        $res = $this->model->detail($this->request->id, 1, $field);

        if (!$res) {
            return $this->returnApi(202, "活动不存在或已结束");
        }
        $res = $res->toArray();

        //活动状态
        $res['status'] = $this->model->getPictureLiveStatus($res);
        //判断用户是否有上传作品权限
        $res['is_can_uploads'] = $this->model->userIsCanUploads($res['id'], $user_id, $res); //true可以上传  false 不可以上传
        //更新浏览量
        $this->model->updateBrowseNumber($res['id'], 10, 30);
        //更新浏览记录
        $pictureLiveBrowseModel = new PictureLiveBrowse();
        $pictureLiveBrowseModel->modifyBrowseLog($res['id'], $user_id);
        //图片量
        $pictureLiveWorksModel = new PictureLiveWorks();
        $res['works_num'] = $pictureLiveWorksModel->getWorksNumber($res['id'], [1]);
        $res['intro_untag'] = strip_tags($res['intro']);

        unset($res['vote_start_time'], $res['vote_end_time'], $res['uploads_number'], $res['uploads_way'], $res['is_appoint']);
        return $this->returnApi(200, "查询成功", true, $res);
    }
}
