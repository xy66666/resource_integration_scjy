<?php

namespace App\Http\Controllers\Import;

use App\Http\Controllers\Admin\CommonController;
use App\Http\Controllers\Controller;
use App\Models\NewBookRecommend;
use App\Models\ShopBook;
use App\Models\ShopBookType;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;


/**
 * excel导入功能
 */
class ShopBookImport extends Controller
{
    public $model = null;
    public $request = null;
    public $commonObj = null;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->model = new ShopBook();
        $this->commonObj = new CommonController();
    }

    /**
     * 书店新书推荐管理导入
     * @param file  excel 文件名
     * @param shop_id   所属书店
     * @param type_id   所属类型id
     * @param is_update  遇到重复数据，是否覆盖更新  1 更新  2 不更新 （默认2）
     */
    public function index()
    {
        set_time_limit(500);
        ini_set('memory_limit', '1024M');

        $is_update = $this->request->input('is_update', 2);
        $type_id = $this->request->input('type_id', '');
        $shop_id = $this->request->input('shop_id', '');

        if (empty($shop_id) || !is_numeric($shop_id)) {
            return $this->returnApi(201, '书店id规则不正确');
        }
        $typeModel = new ShopBookType();
        DB::beginTransaction();
        try {
            $data = $this->getData('file', 'book_import');

            // $success = 0;//成功个数
            $error = 0; //失败个数
            $error_msg = ''; //失败原因
            foreach ($data as $key => $val) {
                $serial = $val[0];
                $book_name = $val[1];
                $author = $val[2];
                $isbn = $val[3];

                if (empty($book_name)) {
                    $error++;
                    $error_msg .= '[序号：“' . $serial . '”书名不能为空]';
                    continue;
                }
                if (empty($author)) {
                    $error++;
                    $error_msg .= '[序号：“' . $serial . '”作者不能为空]';
                    continue;
                }
                if (empty($isbn) || strlen($isbn) < 13) {
                    $error++;
                    $error_msg .= '[序号：“' . $serial . '”ISBN不能为空或规则不正确]';
                    continue;
                }

                if (strlen($isbn) > 13) {
                    $isbn = substr($isbn, 0, 13);
                }
                //判断isbn规则
                $isbn_state = check_isbn($isbn);
                if (!$isbn_state) {
                    $error++;
                    $error_msg .= '[序号：“' . $serial . '”ISBN规则不正确]';
                    continue;
                }

                $temp_data = $this->setBookAddData($val);
                $new_type_id = $typeModel->getTypeId($shop_id, $val['9']); //获取列表类型id
                $temp_data['type_id'] = $new_type_id ? $new_type_id : $type_id;
                //判断isbn是否存在
                $condition = [];
                $condition[] = ['shop_id', '=', $shop_id];
                $condition[] = ['shop_identifier', '=', $temp_data['shop_identifier']];
                $bookIsExists = $this->model->nameIsExists($isbn, 'isbn', null, 1, $condition);
                if (empty($bookIsExists)) {
                    $temp_data['shop_id'] = $shop_id;
                    $bookRecomModel = new ShopBook();
                    $bookRecomModel->add($temp_data); //覆盖更新数据
                } else {
                    if ($is_update == 1) {
                        $temp_data['id'] = $bookIsExists->id;
                        $this->model->change($temp_data); //覆盖更新数据
                    } else {
                        $error++;
                        $error_msg .= '[序号：“' . $serial . '” isbn:' . $isbn . ' 已存在,不允许重复添加]';
                        continue;
                    }
                }
            }
            DB::commit();
            $return_msg = "导入成功，" . '错误: ' . $error . ' 个';
            if ($error) $return_msg .= '错误原因:' . $error_msg;

            return $this->commonObj->returnApi(200, $return_msg, true);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->commonObj->returnApi(202, $e->getMessage());
        }
    }
    /**
     * 组装书籍数据
     * @param $data 数组数据
     */
    public function setBookAddData($data)
    {
        $temp_data = [];
        $temp_data['book_name'] = $data[1];
        $temp_data['author'] = $data[2];
        $temp_data['isbn'] = $data[3];
        $temp_data['price'] = $data[4];
        $temp_data['book_num'] = $data[5];
        $temp_data['press'] = $data[6];
        $temp_data['pre_time'] = $data[7];
        $temp_data['number'] = $data[8] ? $data[8] : 1;
        $temp_data['is_recom'] = $data[10] == '是' ? 1 : 2;
        $temp_data['intro'] = $data[11];
        $temp_data['shop_identifier'] = md5($temp_data['book_name'] . $temp_data['isbn']);

        return $temp_data;
    }




    /**
     * 处理数据
     * @param $file_name  上传文件名 
     * @param $file_dir  存放文件夹 
     */
    public function getData($file_name = 'file', $file_dir = 'file')
    {
        if ($this->request->file($file_name) == null) throw new Exception('请添加Excel表格......'); // return $this->returnApi(202, '请添加Excel表格......');

        //接收前台文件
        if (empty($_FILES[$file_name]['name'])) throw new Exception('上传文件不能为空'); //return $this->returnApi(202, "上传文件不能为空");
        if (empty($_FILES[$file_name]['size']) || $_FILES[$file_name]['size'] > 5242880) throw new Exception('上传文件不能大于5M'); //return $this->returnApi(202, "上传文件不能大于5M");

        //判断文件名
        $name = explode('.', $_FILES[$file_name]['name'])[1];
        if ($name !== 'xls' && $name !== 'xlsx') throw new Exception('上传文件后缀名不允许，只能上传 .xls 或 .xlsx 的文件'); //return $this->returnApi(202, "上传文件后缀名不允许，只能上传 .xls 或 .xlsx 的文件");

        // 这个方法是 自定义文件名 文件上传方法 。 将获取到的Excel文件 保存到/public/upload/book_import/ 文件中。
        $foldername =  $file_dir . '/' . date('Y-m-d'); //$file_name.'/'.date('Y-m-d')  存放路径，按日志存储
        $file = $this->request->file($file_name)->store($foldername);

        if ($file) {
            //$this->request->file($file_name)->getRealPath(); 获取临时文件地址
            $file_addr = public_path('uploads') . '/' . $file; //完整地址
            /*  Excel::load($file_addr, function ($reader) {return $reader->all();}); */
            $delTitle = 1; //指定头行数  删除它
            $excel = new Import($delTitle);
            Excel::import($excel, $file_addr);
            if ($excel->data) {
                return $excel->data->toArray(); //获取所有的数据（数组）
            }
            throw new Exception('导入失败');
        }
        throw new Exception('请添加Excel表格......'); //return $this->returnApi(202, "导入失败");
    }
}
