<?php

namespace App\Http\Controllers\AccessSystem;

use App\Http\Controllers\QrCodeController;
use App\Models\StudyRoomReservation;
use App\Models\StudyRoomReservationApply;
use App\Models\StudyRoomReservationEnterLog;
use App\Models\StudyRoomReservationRemoteOperation;
use App\Models\StudyRoomReservationSchedule;
use App\Models\StudyRoomReservationWhite;
use App\Models\StudyRoomReservationWhiteEnterLog;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class IndexController extends CommonController
{
    /**
     * 首页
     */
    public function checkCode()
    {
        $post = file_get_contents('php://input');
        $post_data = json_decode($post, true);
      //  Log::error($post_data);

        if ($this->checkSign($post_data) !== true) {
            return [
                'Status' => 0,
                'StatusDesc' => '网络错误，请重试！',
                'Relay1Time' => 0
            ];
        }

        //判断请求是否有效
        if ($post_data['ViewId'] !== 'resource_integration' && !is_numeric($post_data['ViewId'])) {
            return [
                'Status' => 0,
                'StatusDesc' => '无效请求！',
                'Relay1Time' => 0
            ];
        }

        if (isset($post_data['CodeVal'])) {
            $access_system_qr_secret_key = config('other.access_system_qr_secret_key');
            $post_data['CodeVal'] = rc4_decrypt(str_replace('CB01', '', $post_data['CodeVal']), $access_system_qr_secret_key); //对解密参数进行解密
            Log::error('解密后的 CodeVal');
            Log::error($post_data['CodeVal']);
        } else {
            return [
                'Status' => 0,
                'StatusDesc' => '无效请求！',
                'Relay1Time' => 0
            ];
        }
        $codeVal = str_replace(['[', ']'], '', $post_data['CodeVal']);
        $codeVal = explode(',', $codeVal);
        if (((isset($codeVal[2]) && $codeVal[2] > date('YmdHis')) || (empty($codeVal[3]) || $codeVal[3] < date('YmdHis', strtotime("-10 second"))))) { //允许10秒延时
            return [
                'Status' => 0,
                'StatusDesc' => '无效请求！',
                'Relay1Time' => 0
            ];
        }
        $cab_code = $post_data['UID'];
        $studyRoomReservationModel = new StudyRoomReservation();
        $study_room_reservation_info = $studyRoomReservationModel->getReservationInfoByCabCode($cab_code);
        if (empty($study_room_reservation_info)) {
            return [
                'Status' => 0,
                'StatusDesc' => '无效请求！',
                'Relay1Time' => 0
            ];
        }
        $user_id = $codeVal[0];
        if (strlen($user_id) == 8) {
            //查看当前阶段预约信息
            $week = date("w");
            $weeks = $week ? $week : 7;
            //查看指定日期是否有特殊时间设置
            $reservationScheduleModel = new StudyRoomReservationSchedule();
            list($schedule_type, $reservation_schedule) = $reservationScheduleModel->getReservationSchedule($study_room_reservation_info['id'], $weeks);
            if ($schedule_type === false) {
                return $this->returnApi(202, $reservation_schedule);
            }

            /**查询该时段时候有申请记录 */
            $studyRoomReservationApplyModel = new StudyRoomReservationApply();
            $apply = $studyRoomReservationApplyModel->nowTimeMakeInfo($user_id, $study_room_reservation_info['id'], $reservation_schedule['id'], $schedule_type, [1, 6]);
            if (empty($apply)) {
                return [
                    'Status' => 0,
                    'StatusDesc' => '请使用预约的书屋',
                    'Relay1Time' => 0
                ];
            } else {
                if ($apply->status == 1) {
                    $apply->change_time = date("Y-m-d H:i:s", time());
                    $apply->status = 6;
                    $apply->sign_time = date("Y-m-d H:i:s", time());
                    $apply->save(); //修改签到时间

                    //进行消息推送
                    $reservation_name = $study_room_reservation_info['name'];
                    $system_id = $this->systemAdd("空间预约签到成功", $apply->user_id,  $apply->account_id, 51, intval($apply->id), '【' . $reservation_name . '】空间预约签到成功');
                }
                //如果是 6 已签到，直接允许通过
            }

            $studyRoomReservationEnterLogModel = new StudyRoomReservationEnterLog();
            $studyRoomReservationEnterLogModel->user_id = $user_id;
            $studyRoomReservationEnterLogModel->save();
        } else {
            //检查白名单用户
            $studyRoomReservationWhiteModel = new StudyRoomReservationWhite();
            $studyRoomReservationWhite = $studyRoomReservationWhiteModel->getWhiteByAccount($user_id);//用户id固定8位为用户id，11为手机号码
            if (empty($studyRoomReservationWhite)) {
                Log::error('当前账号不属于白名单用户：' . $user_id);
                return [
                    'Status' => 0,
                    'StatusDesc' => '请使用预约的书屋',
                    'Relay1Time' => 0
                ];
            }

            $studyRoomReservationWhiteEnterLogModel = new StudyRoomReservationWhiteEnterLog();
            $studyRoomReservationWhiteEnterLogModel->account_id = $studyRoomReservationWhite->id;
            $studyRoomReservationWhiteEnterLogModel->save();
        }
        return [
            'Status' => 1,
            'StatusDesc' => '允许通行',
            'Relay1Time' => 6000
        ];
    }
    /**
     * 心跳 
     */
    public function queryCmd()
    {
        $post = file_get_contents('php://input');
        $post_data = json_decode($post, true);

        //签名错误，都不处理
        if ($this->checkSign($post_data) !== true) {
            return [
                'CmdID' => '',
                'CmdCode' => 0, //无命令
            ];
        }

        //默认返回命令
        $cmd = [
            'CmdID' => time(),
            'CmdCode' => 0,
            'CmdParams' => [
                'DateTime' => date('Y-m-d H:i:s')
            ]
        ];

        $uid = $post_data['UID'];
        $uid_data = Cache::get($uid);
        if ($uid_data === false) {
            Cache::put($uid, 0);
        }

        //每天重置一次
        $studyRoomReservationRemoteOperationModel = new StudyRoomReservationRemoteOperation();
        if (date('H:i:s') > '18:00:00' && date('H:i:s') < '18:01:00') {
            $studyRoomReservationRemoteOperationModel->where('status', '<>', 1)->update(['status' => 4]); //重置数据为失败，即使这条数据正在执行，后面收到回调会自动改成已完成
            $count = $studyRoomReservationRemoteOperationModel->where('cab_code', $uid)->where('status', 3)->count(); //重置一次数据
            Cache::put($uid, $count);
        }

        if ($uid_data !== false && ($uid_data !== 0 || $uid_data !== '0')) {
            //获取操作
            $remoteOperation = $studyRoomReservationRemoteOperationModel->where('cab_code', $uid)->where('status', 3)->orderBy('id')->first(); //一次只处理一个
            if (empty($remoteOperation)) {
                Cache::put($uid, 0);
            } else {
                //进行实际操作
                $cmd = $this->getCmd($remoteOperation['type'], $remoteOperation['order_number']);

                $remoteOperation->status = 2;
                $remoteOperation->save();
                Cache::decrement($uid); // -1  后面如果处理错误，才加回来
            }
        }
        return $cmd;
    }

    /**
     * 获取心跳下发命令
     * @param type 类型  1 远程开门   6 下载白名单； 7 清空本地所有白名单；
     */
    public function getCmd($type, $order_number = null)
    {
        switch ($type) {
            case 1:
                $cmd = [
                    'CmdID' => $order_number,
                    'CmdCode' => 1,
                    'CmdParams' => []
                ];
                break;
            case 6:
                $list = StudyRoomReservationWhite::where('is_del', 1)->get();
                $data = [];
                $account = '';
                foreach ($list as  $key => $val) {
                    $account .= $val['account'] . '，';
                    $data[$key]['CodeVal'] = $val['account'];
                    $data[$key]['CodeType'] = 'Q'; //二维码固定
                    $data[$key]['ValidityStart'] = $val['start_time'];
                    $data[$key]['ValidityEnd'] = $val['end_time'];
                    $data[$key]['DeleteFlag'] = 0;
                }
                $account = trim($account, '，');
                StudyRoomReservationRemoteOperation::where('order_number', $order_number)->update(['account' => $account]);
                $cmd = [
                    'CmdID' => $order_number,
                    'CmdCode' => 6,
                    'CmdParams' => $data
                ];
                break;
            case 7:
                $cmd = [
                    'CmdID' => $order_number,
                    'CmdCode' => 7,
                    'CmdParams' => []
                ];
                break;
            default:
                $data = [
                    'CmdID' => time(),
                    'CmdCode' => 0,
                    'CmdParams' => [
                        'DateTime' => date('Y-m-d H:i:s')
                    ]
                ];
        }
        return $cmd;
    }

    /**
     * 心跳响应
     */
    public function queryCmdPostData()
    {
        $post = file_get_contents('php://input');
        $post_data = json_decode($post, true);

        //Log::error($post_data);

        //签名错误，都不处理
        if ($this->checkSign($post_data) !== true) {
            return [
                'CmdID' => '',
                'CmdCode' => 0, //无命令
            ];
        }
        $order_number = $post_data['CmdID'];
        $remoteOperation = StudyRoomReservationRemoteOperation::where('order_number', $order_number)->first(); //一次只处理一个

        if (empty($remoteOperation)) {
            return [
                'CmdID' => '',
                'CmdCode' => 0, //无命令
            ];
        }

        if ($post_data['CmdStatus'] == 1) {
            //执行成功
            $remoteOperation->status = 1;
            $remoteOperation->change_time = date('Y-m-d H:i:s');
            $remoteOperation->save(); //处理完成
        } else {
            Cache::increment($remoteOperation->cab_code); //增加一次次数

            $remoteOperation->status = 3;
            $remoteOperation->save(); //处理完成

            return [
                'CmdID' => '',
                'CmdCode' => 0, //无命令
            ];
        }

        $data = [
            'CmdID' => time(),
            'CmdCode' => 0,
            'CmdParams' => [
                'DateTime' => date('Y-m-d H:i:s')
            ]
        ];
        return $data;
    }



    /**
     * 生成二维码
     * @param user_id 生成 二维码的用户id
     * @param area_id 生成 二维码的区域id  区域D 为 AIl_Viewld，表示不验证区域ID，即都为合法区域ID。二维码区域 ID 最长限制 200 字节
     * @param $expire 有效期  单位分钟
     */
    public function createQr($user_id, $area_id = 'AIl_Viewld', $expire = '3')
    {
        // $user_id = '99999999999';
        // $area_id = '2222222';
        $data = '[' . $user_id . ',' . $area_id . ',' . date('YmdHis') . ',' . date('YmdHis', strtotime("+$expire min")) . ']';
        $access_system_qr_secret_key = config('other.access_system_qr_secret_key');
        $rc4_encrypt = rc4_encrypt($data, $access_system_qr_secret_key);
        $rc4_encrypt = 'CB01' . $rc4_encrypt;
        $rc4_encrypt = strtoupper($rc4_encrypt);

        $qrCodeObj = new QrCodeController();

        //生成二维码
        $qr_url = $qrCodeObj->setQr($rc4_encrypt, false, 300, 1);
        //return $this->returnApi(200, "二维码生成成功", true, ['qr_url' => 'http://192.168.6.6/public_recom/public/uploads/' . $qr_url]);
        return $qr_url;
    }

    /**
     * 验证二维码值签名
     */
    public function checkSign($data)
    {
        if (empty($data['Sign'])) {
            return '签名错误';
        }

        $str = '';
        foreach ($data as $key => $val) {
            if ($key == 'Sign') {
                break;
            }
            if (empty($val)) {
                continue;
            }
            $str .= $key . '=' . $val;
        }
        $str = $str . 'SecretKey=' . config('other.access_system_secret_key'); //添加签名

        // $str = 'CodeVal=' . $data['CodeVal'] . 'CodeType=' . $data['CodeType'] . 'BrushTime=' . $data['BrushTime'] . 'ViewId=' . $data['ViewId'] . 'UID=' . $data['UID']
        //     . 'UKey=' . $data['UKey'] . 'SN=' . $data['SN'] . 'IsOnline=' . $data['IsOnline'] . 'Property=' . $data['Property'] . 'Timestamp=' . $data['Timestamp']
        //     . 'SecretKey=' . config('other.access_system_secret_key');

        $str_md5 = md5($str);
        if ($str_md5 !== $data['Sign']) {
            Log::error($str_md5);
            return '签名错误';
        }
        return true;
    }
}
