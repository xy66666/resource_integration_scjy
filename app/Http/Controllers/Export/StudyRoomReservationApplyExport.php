<?php

namespace App\Http\Controllers\Export;

use App\Http\Controllers\Controller;
use App\Models\StudyRoomReservation;
use App\Models\StudyRoomReservationApply;
use Maatwebsite\Excel\Facades\Excel;


/**
 * 空间预约导出
 */
class StudyRoomReservationApplyExport extends Controller
{
    private $limit = 999999;
    public $model = null;
    public $reservationModel = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new StudyRoomReservationApply();
        $this->reservationModel = new StudyRoomReservation();
    }

    /** 
     * 预约申请、预约人数列表导出
     * @param reservation_id int 预约id
     * @param keywords string 搜索关键词
     * @param start_date date 搜索开始时间
     * @param end_date date 搜索结束时间
     * @param status int 用于筛选用户列表  预约状态   1、已通过 3、待审核 4、已拒绝 5、已过期  6、已签到  7 已签退 
     * @param is_violate int 是否违规  1正常 2违规 默认1
     */
    public function index()
    {
        $keywords = $this->request->keywords;
        $start_date = $this->request->start_date;
        $end_date = $this->request->end_date;
        $reservation_id = $this->request->reservation_id;
        $status = $this->request->status;
        $is_violate = $this->request->is_violate;

        $reservation = $this->reservationModel->where('id', $reservation_id)->first();
        if (empty($reservation)) {
            $res['data'] = []; //"参数传递失败"
        } else {
            $res = $this->model->lists($reservation_id, null, $status, $is_violate, $keywords, $start_date, $end_date, $this->limit);
            if (empty($res['data'])) {
                $res['data'] = [];
            }
        }

        //设置表头
        $row = [[
            "index" => '序号',
            "nickname" => '微信昵称',
            "username" => '姓名',
            "tel" => '联系方式',
            "id_card" => '身份证信息',
            "unit" => '单位',
            "sex" => '性别',
            "age" => '年龄',
            "reader_id" => '读者证号',
            "address" => '常驻地址',
            "remark" => '备注',
            "number" => '预约数量',
            "con_schedule_time" => '预约申请时段',
            "schedule_type" => '排版类型',
            "status" => '申请状态',
            "reason" => '拒绝原因',
            "sign_time" => '签到时间',
            "sign_end_time" => '签退时间',
            "is_violate" => '是否违规',
            "violate_reason" => '违规原因',
            "create_time" => '申请时间',
        ]];

        $title = '预约人员列表导出-' . date('YmdHis');
        //执行导出
        $data = $this->setData($res['data']); //要导入的数据

        return $this->exportData($data, $title, $row);
    }


    /**
     * 导出数据
     *
     * @param [type] $data
     * @return void
     */
    public function exportData($data, $title, $row)
    {
        $header = $row; //导出表头
        $excel = new Export($data, $header, $title);
        $excel->setColumnWidth(['A' => 10, 'B' => 20, 'C' => 20, 'D' => 20, 'E' => 20, 'F' => 20, 'G' => 20, 'H' => 20, 'I' => 20, 'J' => 20, 'K' => 20, 'L' => 30, 'M' => 20, 'N' => 20, 'O' => 20, 'P' => 20, 'Q' => 20]);
        $excel->setRowHeight([1 => 30]);
        // $excel->setFreezePane('A2');
        // $excel->setFont(['A1:Z1265' => '宋体']);
        // $excel->setFontSize(['A1:I1' => 14,'A2:Z1265' => 10]);
        // $excel->setBold(['A1:Z2' => true]);
        // $excel->setBackground(['A1:A1' => '808080','C1:C1' => '708080']);
        // $excel->setMergeCells(['A1:I1']);
        // $excel->setBorders(['A2:D5' => '#000000']);
        return Excel::download($excel, $title . '.xlsx');
    }


    /**
     * 处理 终端书籍列表 数据
     */
    public function setData($data)
    {
        $excel_data = [];

        /*不需要填写的字段*/
        $no_data = ['username', 'tel', 'id_card', 'unit', 'sex', 'age', 'reader_id', 'address'];

        /*设置excel内容*/
        foreach ($data as $key => $val) {
            foreach ($no_data as $k => $v) {
                $cur_value = !empty($val[$v]) ? $val[$v] : '不需要填写';
                $val[$v] = $cur_value;
            }

            $excel_data[$key]['index'] = $key + 1;
            $excel_data[$key]['nickname'] = nickname_switch_no_sepcial($val['nickname']);

            $excel_data[$key]['username'] = $val['username'];
            $excel_data[$key]['tel'] = "\t" . $val['tel'];
            $excel_data[$key]['id_card'] = "\t" . $val['id_card'];
            $excel_data[$key]['unit'] = $val['unit'];
            $excel_data[$key]['sex'] = $val['sex'] == 1 ? '男' : ($val['sex'] == 2 ? '女' : '不需要填写');
            $excel_data[$key]['age'] = $val['age'];
            $excel_data[$key]['reader_id'] = "\t" . $val['reader_id'];
            $excel_data[$key]['address'] = $val['address'];
            $excel_data[$key]['remark'] = !empty($val['remark']) ? $val['remark'] : '未填写备注';
            $excel_data[$key]['number'] = $val['number'];
            $excel_data[$key]['con_schedule_time'] = $this->model->getConSchedule($val['con_schedule'], $val['make_time']);
            $excel_data[$key]['schedule_type'] = $val['schedule_type'] == 1 ? '普通排版' : '特殊排版';
            $excel_data[$key]['status'] = $this->model->getStatusName($val['status']);
            $excel_data[$key]['reason'] = $val['reason'];
            $excel_data[$key]['sign_time'] = !empty($val['sign_time']) ? $val['sign_time'] : '未签到';
            $excel_data[$key]['sign_end_time'] = !empty($val['sign_end_time']) ? $val['sign_end_time'] : '';
            $excel_data[$key]['is_violate'] = $val['is_violate'] == 1 ? '否' : '已违规';
            $excel_data[$key]['violate_reason'] = $val['violate_reason'];
            $excel_data[$key]['create_time'] = $val['create_time'];
        }
        return $excel_data;
    }
}
