<?php

namespace App\Http\Controllers\Export;

use App\Http\Controllers\Admin\CommonController;
use App\Models\TurnActivity;
use App\Models\TurnActivityUserGift;
use App\Models\UserDrawAddress;;

use App\Models\TurnActivityUserUnit;
use App\Models\UserInfo;
use App\Validate\TurnActivityUserGiftValidate;
use Maatwebsite\Excel\Facades\Excel;

/**
 * 在线抽奖-用户活动获取礼物导出
 */
class TurnActivityUserGiftExport extends CommonController
{

    public $model = null;
    public $activityModel = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new TurnActivityUserGift();
        $this->activityModel = new TurnActivity();
        $this->validate = new TurnActivityUserGiftValidate();
    }

    /**
     * 获取用户礼物列表
     * @param act_id int 活动id
     * @param state string 礼物状态 1未发放 2已发放 3未发货 4邮递中 5已到货
     * @param type int 1文化红包 2精美礼品
     * @param keywords string 搜索关键词
     * @param start_time datetime 开始时间    数据格式  年月日
     * @param end_time datetime 结束时间
     */
    public function index()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $act_info = $this->activityModel->detail($this->request->act_id, null, null);

        if (empty($act_info)) {
            return $this->returnApi(201, '参数错误');
        }
        if ($act_info['prize_form'] == 1) {
            return $this->returnApi(201, '此活动为排名类活动');
        }
        $res = $this->model->lists($this->request->act_id, null, $this->request->keywords, $this->request->type, $this->request->state, $this->request->start_time, $this->request->end_time, 1, 99999);

        if (empty($res['data'])) {
            return $this->returnApi(203, '暂无数据');
        }

        // $userDrawAddressModel = new UserDrawAddress();
        $userDrawAddressModel = new UserDrawAddress();
        $activityUserUnitModel = new TurnActivityUserUnit();
        $userInfoModel = new UserInfo();
        foreach ($res['data'] as $key => $val) {
            //  $res['data'][$key]['user_info'] = $userDrawAddressModel->detail($val['user_guid'], 2, $this->request->act_id);
            $res['data'][$key]['user_wechat_info'] = $userInfoModel->getWechatInfo($val['user_guid']);
            $res['data'][$key]['user_draw_address_info'] = $userDrawAddressModel->detail($val['user_guid'], 2, $this->request->act_id);
            $res['data'][$key]['user_unit_info'] =  $activityUserUnitModel->getUserUnitInfo($val['user_guid'], $this->request->act_id);
        }
        $real_info = $act_info['real_info'];
        $real_info = explode('|', $real_info);

        $row[0]["id"] = '序号';
        $row[0]["order_id"] = '订单号';
        $row[0]["nickname"] = '微信昵称';
        if (in_array(1, $real_info)) $row[0]["username"] = '姓名';
        if (in_array(2, $real_info)) $row[0]["tel"] = '电话';
        if (in_array(3, $real_info)) $row[0]["id_card"] = '身份证号码';
        if (in_array(4, $real_info)) $row[0]["tel"] = '读者证号';
        //  $row[0]["address"] = '邮递地址';
        $row[0]["name"] = '礼物名称';
        $row[0]["type"] = '礼物类型';
        $row[0]["way"] = '领取方式';
        $row[0]["state"] = '状态';
        $row[0]["user_draw_address_info_username"] = '收货姓名';
        $row[0]["user_draw_address_info_tel"] = '收货电话';
        $row[0]["user_draw_address_info_address"] = '收货地址';
        $row[0]["pick_time"] = '自提时间';
        $row[0]["create_time"] = '获奖时间';

        $title = '获奖名单列表-' . date('YmdHis');

        $data = $this->setData($res['data'], $real_info); //要导入的数据

        return $this->exportData($data, $title, $row);
    }

    /**
     * 导出数据
     *
     * @param [type] $data
     * @return void
     */
    public function exportData($data, $title, $row)
    {
        $header = $row; //导出表头
        $excel = new Export($data, $header, $title);
        $excel->setColumnWidth(['A' => 10, 'B' => 35, 'C' => 20, 'D' => 20, 'E' => 20, 'F' => 20, 'G' => 20, 'H' => 25, 'I' => 25, 'J' => 20, 'K' => 20, 'L' => 20, 'M' => 20, 'N' => 20, 'O' => 30]);
        $excel->setRowHeight(30, count($data) + 1);
        // $imgPosition = $node != 1 ? 'D' : 'C';
        // $excel->setExcelImg($img , $imgPosition , 40,40);
        // $excel->setFreezePane('A2');
        // $excel->setFont(['A1:Z1265' => '宋体']);
        // $excel->setFontSize(['A1:I1' => 14,'A2:Z1265' => 10]);
        // $excel->setBold(['A1:Z2' => true]);
        // $excel->setBackground(['A1:A1' => '808080','C1:C1' => '708080']);
        // $excel->setMergeCells(['A1:I1']);
        // $excel->setBorders(['A2:D5' => '#000000']);
        return Excel::download($excel, $title . '.xlsx');
    }


    /**
     * 处理 终端书籍列表 数据
     */
    public function setData($data, $real_info)
    {
        $excel_data = [];
        /*设置excel内容*/
        foreach ($data as $key => $val) {
            $excel_data[$key]['id'] = $key + 1;
            $excel_data[$key]["order_id"] = "\t" . $val["order_id"];
            $excel_data[$key]["nickname"] = $val["user_wechat_info"]['nickname'];
            // $excel_data[$key]["order_id"] = number_format($val["order_id"], 0, '', '');

            if (in_array(1, $real_info)) $excel_data[$key]["username"] = !empty($val['user_unit_info']) ? $val['user_unit_info']["username"] : '';
            if (in_array(2, $real_info)) $excel_data[$key]["tel"] =  !empty($val['user_unit_info']) ? "\t" . $val['user_unit_info']["tel"] : '';
            if (in_array(3, $real_info)) $excel_data[$key]["id_card"] =  !empty($val['user_unit_info']) ? "\t" . $val['user_unit_info']["id_card"] : '';
            if (in_array(4, $real_info)) $excel_data[$key]["reader_id"] =  !empty($val['user_unit_info']) ? "\t" . $val['user_unit_info']["reader_id"] : '';

            //  $excel_data[$key]["address"] =  !empty($val['user_info']["province"]) ? ($val['user_info']["province"] . $val['user_info']["city"] . $val['user_info']["district"] . $val['user_info']["address"]) : '';
            $excel_data[$key]["name"] = $val['name'];
            $excel_data[$key]["type"] = $val['type'] == 1 ? '文化红包' : '精美礼品';
            $excel_data[$key]["way"] = $val['way'] == 1 ? '自提' : '邮递';
            $excel_data[$key]["state"] = $this->getState($val['state']);
            $excel_data[$key]["user_draw_address_info_username"] = !empty($val["user_draw_address_info"]) ? $val["user_draw_address_info"]['username'] : '';
            $excel_data[$key]["user_draw_address_info_tel"] = !empty($val["user_draw_address_info"]) ? $val["user_draw_address_info"]['tel'] : '';
            $excel_data[$key]["user_draw_address_info_address"] = !empty($val["user_draw_address_info"]) ? $val["user_draw_address_info"]['province'] . $val["user_draw_address_info"]['city'] . $val["user_draw_address_info"]['district'] . $val["user_draw_address_info"]['address'] : '';
            $excel_data[$key]["pick_time"] = !empty($val["start_time"]) ? $val["start_time"] . '~' . $val["end_time"] : '';
            $excel_data[$key]["create_time"] = $val["create_time"];

        }
        return [$excel_data];
    }
    /**
     * Undocumented function
     *
     * @param $state 礼物状态 1未发放 2已发放 3未发货 4邮递中 5已到货
     */
    public function getState($state)
    {
        switch ($state) {
            case 1:
                return '未发放';
                break;
            case 2:
                return '已发放';
                break;
            case 3:
                return '未发货';
                break;
            case 4:
                return '邮递中';
                break;
            case 5:
                return '已到货';
                break;
            case 6:
                return '未领取';
                break;
            case 7:
                return '已领取';
                break;
            case 8:
                return '发放中';
                break;
        }
    }
}
