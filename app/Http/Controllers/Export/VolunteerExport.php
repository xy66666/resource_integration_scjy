<?php

namespace App\Http\Controllers\Export;

use App\Http\Controllers\Controller;
use App\Models\Volunteer;
use App\Models\VolunteerApply;
use App\Models\VolunteerPosition;
use Maatwebsite\Excel\Facades\Excel;

/**
 * excel导出导入功能 
 */
class VolunteerExport extends Controller
{
    public $limit = 99999;
    public $model = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new VolunteerApply();
    }


    /**
     * 志愿者申请列表导出
     * @param limit int 分页大小
     * @param keywords string 搜索关键词
     * @param degree string 教育程度 空表示全部
     * @param intention_id int  服务意向id  多个逗号拼接
     * @param times_id int  服务时间id  多个逗号拼接
     * @param sex int 性别  0 或空 1 男  2 女 
     * @param max_age int 最大年龄 
     * @param mim_age int 最小年龄 
     * @param status tinyint 状态  0 或空 全部 1 通过 2 拒绝 3 待审核
     * 
     */
    public function volunteerApplyList()
    {
        $keywords = $this->request->input('keywords', '');
        $status = $this->request->input('status', 0);
        $degree = $this->request->input('degree', 0);
        $intention_id = $this->request->input('intention_id', 0);
        $times_id = $this->request->input('times_id', 0);
        $sex = $this->request->input('sex', 0);
        $max_age = $this->request->input('max_age', '');
        $mim_age = $this->request->input('mim_age', '');

        $res = $this->model->lists($keywords, $status, $degree, $intention_id, $times_id, $sex, $mim_age, $max_age, 'id desc', $this->limit);

        if (empty($res)) {
            $res['data'] = [];
        }
        $volunteerPositionModel = new VolunteerPosition();
        $real_info = $volunteerPositionModel->where('id', 1)->value('real_info');
        $real_info = explode('|', $real_info);

        //设置表头
        $row[0]["id"] = 'ID';

        if (in_array(1, $real_info)) $row[0]["username"] = '姓名';
        if (in_array(2, $real_info)) $row[0]["sex"] = '性别';

        $row[0]["age"] = '年龄';

        if (in_array(3, $real_info)) $row[0]["birth"] = '生日';
        if (in_array(8, $real_info)) $row[0]["tel"] = '电话号码';
        if (in_array(4, $real_info)) $row[0]["id_card"] = '身份证号码';
        if (in_array(5, $real_info)) $row[0]["nation"] = '民族';
        if (in_array(6, $real_info)) $row[0]["politics"] = '政治面貌';
        if (in_array(7, $real_info)) $row[0]["health"] = '健康状态';
        if (in_array(9, $real_info)) $row[0]["school"] = '毕业院校';
        if (in_array(10, $real_info)) $row[0]["major"] = '专业';
        if (in_array(11, $real_info)) $row[0]["unit"] = '工作单位';
        if (in_array(12, $real_info)) $row[0]["degree"] = '教育程度';
        if (in_array(13, $real_info)) $row[0]["intro"] = '个人简介';
        if (in_array(14, $real_info)) $row[0]["specialty"] = '特长';
        if (in_array(15, $real_info)) $row[0]["address"] = '住址';

        $row[0]["intention"] = '服务志向';
        $row[0]["times"] = '服务时间';
        $row[0]["status"] = '处理状态';
        $row[0]["reason"] = '拒接原因';
        $row[0]["create_time"] = '第一次申请时间';
        $row[0]["change_time"] = '修改时间';

        $title = '志愿者申请列表-' . date('YmdHis');
        //执行导出
        $data = $this->setData1($res['data'], $real_info); //要导入的数据

        return $this->exportData($data, $title, $row);
    }


    /**
     * 志愿者列表导出
     * @param keywords string 搜索关键词
     * @param intention_id int 意向id
     * @param times_id int 时间id
     * @param status int 志愿者状态   1.已通过  
     * @param sex int 性别  1 男  2 女
     * @param birth_create_time date 出生年月   格式  2021-12-08
     * @param birth_end_time date 
     * 
     */
    public function volunteerList()
    {

        $keywords = $this->request->input('keywords', '');
        //   $status = $this->request->input('status', 0);
        $status = 1;
        $degree = $this->request->input('degree', 0);
        $intention_id = $this->request->input('intention_id', 0);
        $times_id = $this->request->input('times_id', 0);
        $sex = $this->request->input('sex', 0);
        $max_age = $this->request->input('max_age', '');
        $mim_age = $this->request->input('mim_age', '');

        $res = $this->model->lists($keywords, $status, $degree, $intention_id, $times_id, $sex, $mim_age, $max_age, 'id desc', $this->limit);

        if (empty($res)) {
            $res['data'] = [];
        }

        foreach ($res['data'] as $key => $val) {
            $res['data'][$key]['age'] = get_user_age_by_id_card($val['id_card']);
        }

        $volunteerPositionModel = new VolunteerPosition();
        $real_info = $volunteerPositionModel->where('id', 1)->value('real_info');
        $real_info = explode('|', $real_info);

        //设置表头
        $row[0]["id"] = 'ID';

        if (in_array(1, $real_info)) $row[0]["username"] = '姓名';
        if (in_array(2, $real_info)) $row[0]["sex"] = '性别';

        $row[0]["age"] = '年龄';

        if (in_array(3, $real_info)) $row[0]["birth"] = '生日';
        if (in_array(8, $real_info)) $row[0]["tel"] = '电话号码';
        if (in_array(4, $real_info)) $row[0]["id_card"] = '身份证号码';
        if (in_array(5, $real_info)) $row[0]["nation"] = '民族';
        if (in_array(6, $real_info)) $row[0]["politics"] = '政治面貌';
        if (in_array(7, $real_info)) $row[0]["health"] = '健康状态';
        if (in_array(9, $real_info)) $row[0]["school"] = '毕业院校';
        if (in_array(10, $real_info)) $row[0]["major"] = '专业';
        if (in_array(11, $real_info)) $row[0]["unit"] = '工作单位';
        if (in_array(12, $real_info)) $row[0]["degree"] = '教育程度';
        if (in_array(13, $real_info)) $row[0]["intro"] = '个人简介';
        if (in_array(14, $real_info)) $row[0]["specialty"] = '特长';
        if (in_array(15, $real_info)) $row[0]["address"] = '住址';

        $row[0]["intention"] = '服务志向';
        $row[0]["times"] = '服务时间';
        $row[0]["volunteer_service_time"] = '总服务时长';

        $title = '志愿者列表-' . date('YmdHis');
        //执行导出
        $data = $this->setData2($res['data'], $real_info); //要导入的数据
        return $this->exportData($data, $title, $row);
    }


    /**
     * 导出数据
     *
     * @param [type] $data
     * @return void
     */
    public function exportData($data, $title, $row)
    {
        $header = $row; //导出表头
        $excel = new Export($data, $header, $title);
        $excel->setColumnWidth(['A' => 10, 'B' => 10, 'C' => 10, 'D' => 10, 'E' => 15, 'F' => 15, 'G' => 20, 'H' => 10, 'I' => 10, 'J' => 20, 'K' => 20, 'L' => 20, 'M' => 20, 'N' => 20, 'O' => 20, 'P' => 20, 'Q' => 20, 'R' => 20, 'S' => 20]);
        $excel->setRowHeight([1 => 30]);
        // $excel->setFreezePane('A2');
        // $excel->setFont(['A1:Z1265' => '宋体']);
        // $excel->setFontSize(['A1:I1' => 14,'A2:Z1265' => 10]);
        // $excel->setBold(['A1:Z2' => true]);
        // $excel->setBackground(['A1:A1' => '808080','C1:C1' => '708080']);
        // $excel->setMergeCells(['A1:I1']);
        // $excel->setBorders(['A2:D5' => '#000000']);
        return Excel::download($excel, $title . '.xlsx');
    }


    /**
     * 处理 数据
     */
    public function setData1($data, $real_info)
    {
        $excel_data = [];
        /*设置excel内容*/
        foreach ($data as $key => $val) {
            $excel_data[$key]['id'] = $key + 1;

            if (in_array(1, $real_info)) $excel_data[$key]['username'] = $val['username'];
            if (in_array(2, $real_info)) $excel_data[$key]['sex'] = $val['sex'] == 1 ? '男' : '女';

            $excel_data[$key]['age'] = get_user_age_by_id_card($val['id_card']);

            if (in_array(3, $real_info)) $excel_data[$key]['birth'] = $val['birth'];
            if (in_array(8, $real_info)) $excel_data[$key]['tel'] = "\t" . $val['tel'];
            if (in_array(4, $real_info)) $excel_data[$key]['id_card'] = "\t" . $val['id_card'];
            if (in_array(5, $real_info)) $excel_data[$key]['nation'] = $val['nation'];
            if (in_array(6, $real_info)) $excel_data[$key]['politics'] = $val['politics'];
            if (in_array(7, $real_info)) $excel_data[$key]['health'] = $val['health_name'];
            if (in_array(9, $real_info)) $excel_data[$key]['school'] = $val['school'];
            if (in_array(10, $real_info)) $excel_data[$key]['major'] = $val['major'];
            if (in_array(11, $real_info)) $excel_data[$key]['unit'] = $val['unit'];
            if (in_array(12, $real_info)) $excel_data[$key]['degree'] = $val['health_name'];
            if (in_array(13, $real_info)) $excel_data[$key]['intro'] = $val['intro'];
            if (in_array(14, $real_info)) $excel_data[$key]['specialty'] = $val['specialty'];
            if (in_array(15, $real_info)) $excel_data[$key]['address'] = $val['address'];

            $excel_data[$key]['intention'] = join(';', array_column($val['intention'], 'name'));
            $excel_data[$key]['times'] = join(';', array_column($val['times'], 'times'));
            $excel_data[$key]['status'] = $val['status'] == 1 ? '已通过' : ($val['status'] == 2 ? '已拒绝' : '审核中');
            $excel_data[$key]['reason'] = $val['status'] == 2 ? strip_tags($val['reason']) : '';
            $excel_data[$key]['create_time'] = $val['create_time'];
            $excel_data[$key]['change_time'] = $val['change_time'];
        }
        return $excel_data;
    }
    /**
     * 处理  数据
     */
    public function setData2($data, $real_info)
    {
        $excel_data = [];
        /*设置excel内容*/
        foreach ($data as $key => $val) {
            $excel_data[$key]['id'] = $key + 1;

            if (in_array(1, $real_info)) $excel_data[$key]['username'] = $val['username'];
            if (in_array(2, $real_info)) $excel_data[$key]['sex'] = $val['sex'] == 1 ? '男' : '女';

            $excel_data[$key]['age'] = get_user_age_by_id_card($val['id_card']);

            if (in_array(3, $real_info)) $excel_data[$key]['birth'] = $val['birth'];
            if (in_array(8, $real_info)) $excel_data[$key]['tel'] = "\t" . $val['tel'];
            if (in_array(4, $real_info)) $excel_data[$key]['id_card'] = "\t" . $val['id_card'];
            if (in_array(5, $real_info)) $excel_data[$key]['nation'] = $val['nation'];
            if (in_array(6, $real_info)) $excel_data[$key]['politics'] = $val['politics'];
            if (in_array(7, $real_info)) $excel_data[$key]['health'] = $val['health_name'];
            if (in_array(9, $real_info)) $excel_data[$key]['school'] = $val['school'];
            if (in_array(10, $real_info)) $excel_data[$key]['major'] = $val['major'];
            if (in_array(11, $real_info)) $excel_data[$key]['unit'] = $val['unit'];
            if (in_array(12, $real_info)) $excel_data[$key]['degree'] = $val['health_name'];
            if (in_array(13, $real_info)) $excel_data[$key]['intro'] = $val['intro'];
            if (in_array(14, $real_info)) $excel_data[$key]['specialty'] = $val['specialty'];
            if (in_array(15, $real_info)) $excel_data[$key]['address'] = $val['address'];

            $excel_data[$key]['intention'] = join(';', array_column($val['intention'], 'name'));
            $excel_data[$key]['times'] = join(';', array_column($val['times'], 'times'));
            $excel_data[$key]['volunteer_service_time'] = $val['volunteer_service_time'] . '分';
        }

        return $excel_data;
    }
}
