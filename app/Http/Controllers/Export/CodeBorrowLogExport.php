<?php

namespace App\Http\Controllers\Export;

use App\Http\Controllers\Controller;
use App\Models\CodeBorrowLog;
use App\Models\CodeReturnLog;
use Maatwebsite\Excel\Facades\Excel;

/**
 * excel导出导入功能 
 */
class CodeBorrowLogExport extends Controller
{
    public $limit = 99999;

    public function __construct()
    {
        parent::__construct();
    }


    /**
     * 扫码借-借阅列表 导出
     * @param keywords  检索条件
     * @param place_code_id  场所码id
     * @param start_time 开始时间   数据格式    2020-05-12 12:00:00
     * @param end_time 结束时间     数据格式    2020-05-12 12:00:00
     * @param limit  条数，默认显示 10条
     */
    public function borrowList()
    {
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        $keywords = $this->request->keywords;
        $place_code_id = $this->request->place_code_id;
        $limit = $this->limit;

        $codeBorrowLogModel = new CodeBorrowLog();
        $res = $codeBorrowLogModel->lists(null,$place_code_id, $keywords, $start_time, $end_time, $limit);

        if (empty($res['data'])) {
            $res['data'] = [];
        }
        //设置表头
        $row = [[
            "id" => 'ID',
            "place_code_name" => '场所名称',
            "account" => '读者证号',
            "book_name" => '书名',
            "author" => '作者',
            "isbn" => 'ISBN',
            "barcode" => '条形码',
            "callno" => '索书号',
            "press" => '出版社',
            "pre_time" => '出版时间',
            "create_time" => '借阅时间',
        ]];
        $title = '扫码借阅记录列表-' . date('YmdHis');
        //执行导出
        $data = $this->setData($res['data']); //要导入的数据

        return $this->exportData($data, $title, $row);
    }

    /**
     * 扫码借-归还列表 导出
     * @param keywords  检索条件
     * @param place_code_id  场所码id
     * @param start_time 开始时间   数据格式    2020-05-12 12:00:00
     * @param end_time 结束时间     数据格式    2020-05-12 12:00:00
     * @param limit  条数，默认显示 10条
     */
    public function returnList()
    {
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        $keywords = $this->request->keywords;
        $place_code_id = $this->request->place_code_id;
        $limit = $this->limit;

        $codeReturnLogModel = new CodeReturnLog();
        $res = $codeReturnLogModel->lists(null,$place_code_id, $keywords, $start_time, $end_time, $limit);

        if (empty($res)) {
            $res['data'] = [];
        }
        //设置表头
        $row = [[
            "id" => 'ID',
            "place_code_name" => '场所名称',
            "account" => '读者证号',
            "book_name" => '书名',
            "author" => '作者',
            "isbn" => 'ISBN',
            "barcode" => '条形码',
            "callno" => '索书号',
            "press" => '出版社',
            "pre_time" => '出版时间',
            "create_time" => '归还时间',
        ]];
        $title = '扫码归还记录列表-' . date('YmdHis');
        //执行导出
        $data = $this->setData($res['data']); //要导入的数据

        return $this->exportData($data, $title, $row);
    }


    /**
     * 导出数据
     *
     * @param [type] $data
     * @return void
     */
    public function exportData($data, $title, $row)
    {
        $header = $row; //导出表头
        $excel = new Export($data, $header, $title);
        $excel->setColumnWidth(['A' => 10, 'B' => 20, 'C' => 20, 'D' => 20, 'E' => 20, 'F' => 20, 'G' => 20, 'H' => 20, 'I' => 20, 'J' => 20, 'K' => 20, 'L' => 20, 'M' => 20, 'N' => 20, 'O' => 20, 'P' => 20, 'Q' => 20, 'R' => 20, 'S' => 20]);
        $excel->setRowHeight([1 => 30]);
        // $excel->setFreezePane('A2');
        // $excel->setFont(['A1:Z1265' => '宋体']);
        // $excel->setFontSize(['A1:I1' => 14,'A2:Z1265' => 10]);
        // $excel->setBold(['A1:Z2' => true]);
        // $excel->setBackground(['A1:A1' => '808080','C1:C1' => '708080']);
        // $excel->setMergeCells(['A1:I1']);
        // $excel->setBorders(['A2:D5' => '#000000']);
        return Excel::download($excel, $title . '.xlsx');
    }


    /**
     * 处理 数据
     */
    public function setData($data)
    {
        $excel_data = [];
        /*设置excel内容*/
        foreach ($data as $key => $val) {
            $excel_data[$key]['id'] = $key + 1;
            $excel_data[$key]['place_code_name'] = $val['con_code_place']['name'];
            $excel_data[$key]['account'] = "\t" . $val['account'];
            $excel_data[$key]['book_name'] = $val['book_name'];
            $excel_data[$key]['author'] = $val['author'];
            $excel_data[$key]['isbn'] = "\t" . $val['isbn'];
            $excel_data[$key]['barcode'] = "\t" . $val['barcode'];
            $excel_data[$key]['callno'] = $val['callno'];
            $excel_data[$key]['press'] = $val['press'];
            $excel_data[$key]['pre_time'] = $val['pre_time'];
            $excel_data[$key]['create_time'] = !empty($val['borrow_time']) ? $val['borrow_time'] : $val['return_time'];
        }
        return $excel_data;
    }
}
