<?php

namespace App\Http\Controllers\Export;

use App\Http\Controllers\Admin\CommonController;
use App\Http\Controllers\Controller;
use App\Models\BookHomeOrder;
use App\Models\BookHomeOrderBook;
use App\Models\BookHomePurchase;
use App\Models\LibBook;
use App\Models\ShopBook;
use App\Validate\BookHomeNewBookValidate;
use Maatwebsite\Excel\Facades\Excel;

/**
 * 图书到家，新书、馆藏书籍导出
 * 新书采购清单（书店端）
 * Class NewBookRecom
 * @package app\api\controller
 */
class BookHomeExport extends Controller
{
    private $page = 1;
    private $limit = 999999;
    private $model = null;
    private $shopBookModel = null;
    private $bookHomeOrderBookModel = null;
    private $bookHomePurchaseModel = null;
    private $validate = null;
    private $bookHomeExportExcelObj = null;


    public function __construct()
    {
        parent::__construct();
        $this->shopBookModel = new ShopBook();
        $this->model = new BookHomeOrder();
        $this->bookHomePurchaseModel = new BookHomePurchase();
        $this->bookHomeOrderBookModel = new BookHomeOrderBook();
        $this->validate = new BookHomeNewBookValidate();
        $this->bookHomeExportExcelObj = new BookHomeExportExcel();
    }


    /**
     * 申请列表、发货列表、所有订单列表 导出(新书)  导出
     * @param is_pay  类型  2 已支付  5 .已退款  6.已同意（后台单纯同意）
     *              7 已拒绝（后台单纯同意）  8已发货（数据就增加到用户采购列表）
     *              9 无法发货   固定参数 申请列表 2   发货列表 6  所有订单列表 5789
     * @param start_time 申请开始时间   数据格式    2020-05-12 12:00:00
     * @param end_time 申请结束时间     数据格式    2020-05-12 12:00:00
     * @param keywords_type 检索key  0 全部 1 订单号 2 读者证号 3 收件人姓名  4 收件人电话
     * @param keywords 检索值
     * @param shop_id 书店id   不传  或转空  或 0，表示全部
     */
    public function orderList()
    {
        //处理已失效的订单
        BookHomeOrder::checkNoPayOrder();

        //增加验证场景进行验证
        if (!$this->validate->scene('order_list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $is_pay = $this->request->is_pay;
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        $keywords_type = $this->request->keywords_type;
        $keywords = $this->request->keywords;
        $shop_id = $this->request->shop_id;

        $commonObj = new CommonController();
        $shop_all_id = $commonObj->getAdminShopIdAll();
        if (empty($shop_all_id)) {
            $res['data'] = []; //无权限访问
        } else {

            $res = $this->model->lists(null, 1, $is_pay, $shop_id, $keywords, $keywords_type, $start_time, $end_time, $shop_all_id, $this->limit);

            if (empty($res['data'])) {
                $res['data'] = [];
            }

            //获取书籍信息
            foreach ($res['data'] as $key => $val) {
                $book_id = $this->bookHomeOrderBookModel->where('order_id', $val['id'])->pluck('book_id');
                $book_info = $this->shopBookModel
                    ->select(/* 'id', */'book_name', 'author', 'author', 'isbn', 'price', 'press', 'pre_time', 'book_selector', 'rack_type', 'rack_describe', 'rack_code')
                    ->whereIn('id', $book_id)
                    ->get()
                    ->toArray();

                // foreach ($book_info as $k => $v) {
                //     $book_info[$k]['barcode'] = $this->bookHomePurchaseModel->getBarcode($v['id'], $val['id'], $val['account_id']);
                //     unset($book_info[$k]['id']);
                // }

                $res['data'][$key]['book_info'] = $book_info;

                $stateName = $this->shopBookModel::getStateNameAndTime($val); //获取时间状态
                $refund_remark = $val['is_pay'] == 5 || $val['is_pay'] == 7 || $val['is_pay'] == 9 ? '(理由：' . $val['refund_remark'] . ')' : '';
                $res['data'][$key]['state_name'] = $stateName['state_name'] . $refund_remark;
                $res['data'][$key]['time'] = $stateName['time'];
            }
        }
        return $this->bookHomeExportExcelObj->export($res['data'], 1, $is_pay);
    }

    /**
     * 申请列表、发货列表、所有订单列表 （馆藏书）导出
     * @param is_pay  类型  2 已支付  5 .已退款  6.已同意（后台单纯同意）
     *              7 已拒绝（后台单纯同意）  8已发货（数据就增加到用户采购列表）
     *              9 无法发货    固定参数  申请列表 2   发货列表 6  所有订单列表 5789
     * @param start_time 申请开始时间   数据格式    2020-05-12 12:00:00
     * @param end_time 申请结束时间     数据格式    2020-05-12 12:00:00
     * @param keywords_type 检索key  0 全部 1 订单号 2 读者证号 3 收件人姓名  4 收件人电话
     * @param keywords 检索值
     */
    public function orderLibraryList()
    {
        //处理已失效的订单
        BookHomeOrder::checkNoPayOrder();

        //增加验证场景进行验证
        if (!$this->validate->scene('order_list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $is_pay = $this->request->is_pay;
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        $keywords_type = $this->request->keywords_type;
        $keywords = $this->request->keywords;

        $res = $this->model->lists(null, 2, $is_pay, null, $keywords, $keywords_type, $start_time, $end_time, null,  $this->limit);
        if (empty($res['data'])) {
            $res['data'] = [];
        }
        //获取书籍信息
        $libBookModel = new LibBook();
        foreach ($res['data'] as $key => $val) {
            $book_info = $libBookModel->from($libBookModel->getTable() . ' as b')
                ->select('book_name', 'author', 'isbn', 'l.barcode', 'press', 'pre_time', 'price', 'l.book_num', 'l.cur_local_note')
                ->rightJoin('lib_book_barcode as l', 'l.book_id', '=', 'b.id')
                ->rightJoin('book_home_order_book as o', 'o.barcode_id', '=', 'l.id')
                ->where('o.order_id', $val['id'])
                ->get()
                ->toArray();

            $res['data'][$key]['book_info'] = $book_info;

            $stateName = $this->shopBookModel::getStateNameAndTime($val); //获取时间状态
            $refund_remark = $val['is_pay'] == 5 || $val['is_pay'] == 7 || $val['is_pay'] == 9 ? '(理由：' . $val['refund_remark'] . ')' : '';
            $res['data'][$key]['state_name'] = $stateName['state_name'] . $refund_remark;
            $res['data'][$key]['time'] = $stateName['time'];
        }

        return $this->bookHomeExportExcelObj->export($res['data'], 2, $is_pay);
    }

    /**
     * 线下取书记录 书店端 导出
     * @param shop_id  书店id   0、全部
     * @param keywords_type  检索条件   0、全部  1、书名  2、作者 3 isbn 4 读者证号 5 条形码 默认 0
     * @param keywords  检索条件
     * @param start_time 取书开始时间   数据格式    2020-05-12 12:00:00
     * @param end_time 取书结束时间     数据格式    2020-05-12 12:00:00
     * @param book_selector  是否套数   不传  或转空  或 0，表示全部  1 是 2 否
     * @param pur_manage_id  书店采购管理员id   不传  或转空  或 0，表示全部
     */
    public function offlineListShop()
    {
        $commonObj = new CommonController();
        $shop_all_id = $commonObj->getAdminShopIdAll();
        if (empty($shop_all_id)) {
            $res['data'] = []; //无权限访问
        } else {
            $shop_id = $this->request->shop_id;
            $keywords_type = $this->request->keywords_type;
            $keywords = $this->request->keywords;
            $start_time = $this->request->start_time;
            $end_time = $this->request->end_time;
            $book_selector = $this->request->book_selector;
            $pur_manage_id = $this->request->pur_manage_id;

            $param = [
                'shop_id' => $shop_id,
                'keywords_type' => $keywords_type,
                'keywords' => $keywords,
                'start_time' => $start_time,
                'end_time' => $end_time,
                'book_selector' => $book_selector,
                'way' => 2, //线下
                'type' => 1, //新书
                'pur_manage_id' => $pur_manage_id,
                'shop_all_id' => $shop_all_id,
                'page' =>  $this->page,
                'limit' =>  $this->limit,
                'select_way' => 1,
            ];
            //列表
            $res = $this->bookHomePurchaseModel->lists($param);
            if (empty($res['data'])) {
                $res['data'] = [];
            }
        }

        $file_name = '线下取书记录列表-' . date('YmdHis');

        $offlineRecomExportObj = new OfflineRecomExportExcel();
        return $offlineRecomExportObj->export($res['data'], $file_name);
    }

    /**
     * 线下取书记录(图书馆端) 查看   导出
     * @param shop_id  书店id   0、全部
     * @param keywords_type  检索条件   0、全部  1、书名  2、作者 3 isbn 4 读者证号 5 条形码 默认 0
     * @param keywords  检索条件
     * @param start_time 取书开始时间   数据格式    2020-05-12 12:00:00
     * @param end_time 取书结束时间     数据格式    2020-05-12 12:00:00
     * @param book_selector  是否套数   不传  或转空  或 0，表示全部  1 是 2 否
     * @param pur_manage_id  书店采购管理员id   不传  或转空  或 0，表示全部
     * @param return_manage_id  归还管理员id   不传  或转空  或 0，表示全部
     * @param is_return 是否归还   不传  或转空  或 0，表示全部  1 借阅中  2 已归还   3 归还中
     * @param is_overdue 是否逾期   不传  或转空  或 0，表示全部  1 未逾期  2 已逾期
     */
    public function offlineListLib()
    {
        $shop_id = $this->request->shop_id;
        $commonObj = new CommonController();
        $shop_all_id = $commonObj->getAdminShopIdAll();
        if (empty($shop_all_id) || ($shop_id && !in_array($shop_id, $shop_all_id))) {
            $res['data'] = []; //无权限访问
        } else {
            $keywords_type = $this->request->keywords_type;
            $keywords = $this->request->keywords;
            $start_time = $this->request->start_time;
            $end_time = $this->request->end_time;
            $is_return = $this->request->is_return;
            $is_overdue = $this->request->is_overdue;
            $book_selector = $this->request->book_selector;
            $pur_manage_id = $this->request->pur_manage_id;
            $return_manage_id = $this->request->return_manage_id;

            $param = [
                'shop_id' => $shop_id,
                'keywords_type' => $keywords_type,
                'keywords' => $keywords,
                'start_time' => $start_time,
                'end_time' => $end_time,
                'is_return' => $is_return,
                'is_overdue' => $is_overdue,
                'book_selector' => $book_selector,
                'way' => 2, //线下
                'type' => 1, //新书
                'pur_manage_id' => $pur_manage_id,
                'return_manage_id' => $return_manage_id,
                'shop_all_id' => $shop_all_id,
                'page' =>  $this->page,
                'limit' =>  $this->limit,
                'select_way' => 1,
            ];
            //列表
            $res = $this->bookHomePurchaseModel->lists($param);
            if (empty($res['data'])) {
                $res['data'] = [];
            }
        }
        $file_name = '线下取书记录列表-' . date('YmdHis');

        $offlineRecomExportObj = new OfflineRecomExportExcel();
        return $offlineRecomExportObj->export($res['data'], $file_name);
    }


    /**
     * 新书线上取书记录列表 (图书馆端查看)(只查看线上的)   导出
     * @param shop_id  书店id   0、全部
     * @param keywords_type  检索条件   0、全部  1、书名  2、作者 3 ISBN 4 读者证号 5 条形码 默认 0
     * @param keywords  检索条件
     * @param start_time 申请开始时间   数据格式    2020-05-12 12:00:00
     * @param end_time 申请结束时间     数据格式    2020-05-12 12:00:00
     * @param is_return 是否归还   不传  或转空  或 0，表示全部  1 借阅中  2 已归还   3 归还中
     * @param is_overdue 是否逾期   不传  或转空  或 0，表示全部  1 未逾期  2 已逾期
     * @param pur_manage_id  采购管理员id  不传  或转空  或 0，表示全部
     * @param return_manage_id  归还书籍管理员id   不传  或转空  或 0，表示全部
     * @param book_selector  是否套书   不传  或转空  或 0，表示全部  1 是 2 否
     * @param settle_state		结算状态  0、全部 1 已结算  2 结算中  3 未结算
     * @param node   是否借阅成功  0全部 1成功  2 失败
     */
    public function libPurchaseShopList()
    {
        $shop_id = $this->request->shop_id;
        $commonObj = new CommonController();
        $shop_all_id = $commonObj->getAdminShopIdAll();
        if (empty($shop_all_id) || ($shop_id && !in_array($shop_id, $shop_all_id))) {
            $res['data'] = []; //无权限访问
        } else {
            $keywords_type = $this->request->keywords_type;
            $keywords = $this->request->keywords;
            $start_time = $this->request->start_time;
            $end_time = $this->request->end_time;
            $is_return = $this->request->is_return;
            $is_overdue = $this->request->is_overdue;
            $book_selector = $this->request->book_selector;
            $node = $this->request->node;
            $settle_state = $this->request->settle_state;
            $pur_manage_id = $this->request->pur_manage_id;
            $return_manage_id = $this->request->return_manage_id;

            $param = [
                'shop_id' => $shop_id,
                'keywords_type' => $keywords_type,
                'keywords' => $keywords,
                'start_time' => $start_time,
                'end_time' => $end_time,
                'is_return' => $is_return,
                'is_overdue' => $is_overdue,
                'book_selector' => $book_selector,
                'way' => 1,
                'type' => 1, //新书
                'node' => $node,
                'settle_state' => $settle_state,
                'pur_manage_id' => $pur_manage_id,
                'return_manage_id' => $return_manage_id,
                'shop_all_id' => $shop_all_id,
                'page' =>  $this->page,
                'limit' =>  $this->limit,
                'select_way' => 1,
            ];
            //列表
            $res = $this->bookHomePurchaseModel->lists($param);

            if (empty($res['data'])) {
                $res['data'] = [];
            }
        }

        $file_name = '新书线上取书记录列表-' . date('YmdHis');
        $purchaseAllListExportObj = new PurchaseAllListExportExcel();
        return $purchaseAllListExportObj->export($res['data'], $file_name, 1);
    }

    /**
     * 所有(馆藏书)采购清单 (图书馆端查看)(只查看线上的)   导出
     * @param keywords_type  检索条件   0、全部  1、书名  2、作者 3 ISBN 4 读者证号 5 条形码 默认 0
     * @param keywords  检索条件
     * @param start_time 申请开始时间   数据格式    2020-05-12 12:00:00
     * @param end_time 申请结束时间     数据格式    2020-05-12 12:00:00
     * @param is_return 是否归还   不传  或转空  或 0，表示全部  1 借阅中  2 已归还   3 归还中
     * @param is_overdue 是否逾期   不传  或转空  或 0，表示全部  1 未逾期  2 已逾期
     * @param return_manage_id  归还书籍管理员id   不传  或转空  或 0，表示全部
     * @param node   是否借阅成功  0全部 1成功  2 失败
     */
    public function libPurchaseLibList()
    {
        $keywords_type = $this->request->keywords_type;
        $keywords = $this->request->keywords;
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        $is_return = $this->request->is_return;
        $is_overdue = $this->request->is_overdue;
        $node = $this->request->node;
        $return_manage_id = $this->request->return_manage_id;

        $param = [
            'keywords_type' => $keywords_type,
            'keywords' => $keywords,
            'start_time' => $start_time,
            'end_time' => $end_time,
            'is_return' => $is_return,
            'is_overdue' => $is_overdue,
            'node' => $node,
            'return_manage_id' => $return_manage_id,
            'page' =>  $this->page,
            'limit' =>  $this->limit,
            'select_way' => 1,
        ];
        //列表
        $res = $this->bookHomePurchaseModel->libLists($param);

        if (empty($res['data'])) {
            $res['data'] = [];
        }

        $file_name = '馆藏书线上取书记录列表-' . date('YmdHis');
        $purchaseAllListExportObj = new PurchaseAllListExportExcel();
        return $purchaseAllListExportObj->export($res['data'], $file_name, 2);
    }

    /**
     * 新书线上取书记录列表 (书店端查看)(只查看线上的)   导出
     * @param shop_id  书店id   0、全部
     * @param keywords_type  检索条件   0、全部  1、书名  2、作者 3 ISBN 4 读者证号 5 条形码 默认 0
     * @param keywords  检索条件
     * @param start_time 申请开始时间   数据格式    2020-05-12 12:00:00
     * @param end_time 申请结束时间     数据格式    2020-05-12 12:00:00
     * @param is_return 是否归还   不传  或转空  或 0，表示全部  1 借阅中  2 已归还   3 归还中
     * @param is_overdue 是否逾期   不传  或转空  或 0，表示全部  1 未逾期  2 已逾期
     * @param pur_manage_id  采购管理员id  不传  或转空  或 0，表示全部
     * @param return_manage_id  归还书籍管理员id   不传  或转空  或 0，表示全部
     * @param book_selector  是否套书   不传  或转空  或 0，表示全部  1 是 2 否
     * @param node   是否借阅成功  0全部 1成功  2 失败
     */
    public function shopPurchaseAllList()
    {
        $commonObj = new CommonController();
        $shop_all_id = $commonObj->getAdminShopIdAll();
        if (empty($shop_all_id)) {
            $res['data'] = []; //无权限访问
        } else {
            $shop_id = $this->request->shop_id;
            $keywords_type = $this->request->keywords_type;
            $keywords = $this->request->keywords;
            $start_time = $this->request->start_time;
            $end_time = $this->request->end_time;
            $is_return = $this->request->is_return;
            $is_overdue = $this->request->is_overdue;
            $settle_state = $this->request->settle_state;
            $pur_manage_id = $this->request->pur_manage_id;
            $return_manage_id = $this->request->return_manage_id;
            $node = $this->request->node;


            $param = [
                'shop_id' => $shop_id,
                'keywords_type' => $keywords_type,
                'keywords' => $keywords,
                'start_time' => $start_time,
                'end_time' => $end_time,
                'settle_state' => $settle_state,
                'is_return' => $is_return,
                'is_overdue' => $is_overdue,
                'node' => $node,
                'pur_manage_id' => $pur_manage_id,
                'return_manage_id' => $return_manage_id,
                'shop_all_id' => $shop_all_id,
                'page' =>  $this->page,
                'limit' =>  $this->limit,
                'select_way' => 1,
                'way' => 1, //1为线上
            ];
            //列表
            $res = $this->bookHomePurchaseModel->lists($param);
            if (empty($res['data'])) {
                $res['data'] = [];
            }
        }
        $file_name = '线上取书记录列表-' . date('YmdHis');
        $purchaseAllListExportObj = new PurchaseAllListExportExcel();
        return $purchaseAllListExportObj->export($res['data'], $file_name, 1);
    }


    /**
     * 新书结算清单列表（图书馆端查看)(只查看线上的)   导出
     * @param shop_id  书店id   0、全部
     * @param keywords_type  检索条件   0、全部  1、书名  2、作者 3 ISBN 4 读者证号 5 条形码 默认 0
     * @param keywords  检索条件
     * @param start_time 申请开始时间   数据格式    2020-05-12 12:00:00
     * @param end_time 申请结束时间     数据格式    2020-05-12 12:00:00
     * @param is_return 是否归还   不传  或转空  或 0，表示全部  1 借阅中  2 已归还   3 归还中
     * @param is_overdue 是否逾期   不传  或转空  或 0，表示全部  1 未逾期  2 已逾期
     * @param pur_manage_id  采购管理员id  不传  或转空  或 0，表示全部
     * @param return_manage_id  归还书籍管理员id   不传  或转空  或 0，表示全部
     * @param book_selector  是否套书   不传  或转空  或 0，表示全部  1 是 2 否
     * @param settle_state		结算状态  0、全部 1 已结算  2 结算中  3 未结算
     * @param node   是否借阅成功  0全部 1成功  2 失败
     */
    public function purchaseLibList()
    {
        $shop_id = $this->request->shop_id;
        $commonObj = new CommonController();
        $shop_all_id = $commonObj->getAdminShopIdAll();
        if (empty($shop_all_id) || ($shop_id && !in_array($shop_id, $shop_all_id))) {
            $res['data'] = []; //无权限访问
        } else {
            $keywords_type = $this->request->keywords_type;
            $keywords = $this->request->keywords;
            $start_time = $this->request->start_time;
            $end_time = $this->request->end_time;
            $is_return = $this->request->is_return;
            $is_overdue = $this->request->is_overdue;
            $book_selector = $this->request->book_selector;
            $node = $this->request->node;
            $settle_state = $this->request->settle_state;
            $pur_manage_id = $this->request->pur_manage_id;
            $return_manage_id = $this->request->return_manage_id;
            $param = [
                'shop_id' => $shop_id,
                'keywords_type' => $keywords_type,
                'keywords' => $keywords,
                'start_time' => $start_time,
                'end_time' => $end_time,
                'is_return' => $is_return,
                'is_overdue' => $is_overdue,
                'book_selector' => $book_selector,
                'way' => 1,
                'type' => 1, //新书
                'node' => $node,
                'settle_state' => $settle_state,
                'pur_manage_id' => $pur_manage_id,
                'return_manage_id' => $return_manage_id,
                'shop_all_id' => $shop_all_id,
                'page' =>  $this->page,
                'limit' =>  $this->limit,
                'select_way' => 1,
            ];
            //列表
            $res = $this->bookHomePurchaseModel->lists($param);
            if (empty($res['data'])) {
                $res['data'] = [];
            }
        }

        $file_name = '新书结算清单列表（图书馆端）-' . date('YmdHis');
        $purchaseLibListExportObj = new PurchaseLibListExportExcel();
        return $purchaseLibListExportObj->export($res['data'], $file_name, 2);
    }

    /**
     * 书店采购清单(书店、图书馆端查看(根据结算状态区分书店还是图书馆查看))   导出
     * @param shop_id  书店id   0、全部
     * @param keywords_type  检索条件   0、全部  1、书名  2、作者 3 ISBN 4 读者证号 5 条形码 默认 0
     * @param keywords  检索条件
     * @param start_time 发货开始时间   数据格式    2020-05-12 12:00:00
     * @param end_time 发货结束时间     数据格式    2020-05-12 12:00:00
     * @param settle_state		结算状态  0、全部 1 已结算  2 结算中  3 未结算
     * @param way		采购方式   1 为 线上荐购 ， 2 为线下荐购
     * @param is_return 是否归还   不传  或转空  或 0，表示全部  1 借阅中  2 已归还   3 归还中
     * @param is_overdue 是否逾期   不传  或转空  或 0，表示全部  1 未逾期  2 已逾期
     * @param book_selector  是否套数   不传  或转空  或 0，表示全部  1 是 2 否
     * @param pur_manage_id  采购管理员id  不传  或转空  或 0，表示全部
     * @param settle_sponsor_manage_id  发起结算的管理员id   不传  或转空  或 0，表示全部
     * @param node   是否借阅成功  0全部 1成功  2 失败
     */
    public function purchaseShopList()
    {
        $shop_id = $this->request->shop_id;
        $commonObj = new CommonController();
        $shop_all_id = $commonObj->getAdminShopIdAll();
        if (empty($shop_all_id) || ($shop_id && !in_array($shop_id, $shop_all_id))) {
            $res['data'] = []; //无权限访问
        } else {
            $shop_id = $this->request->shop_id;
            $keywords_type = $this->request->keywords_type;
            $keywords = $this->request->keywords;
            $start_time = $this->request->start_time;
            $end_time = $this->request->end_time;
            $is_return = $this->request->is_return;
            $is_overdue = $this->request->is_overdue;
            $settle_state = $this->request->settle_state;
            $book_selector = $this->request->book_selector;
            $way = $this->request->way;
            $pur_manage_id = $this->request->pur_manage_id;
            $settle_sponsor_manage_id = $this->request->settle_sponsor_manage_id;
            $node = $this->request->node;

            $param = [
                'shop_id' => $shop_id,
                'keywords_type' => $keywords_type,
                'keywords' => $keywords,
                'start_time' => $start_time,
                'end_time' => $end_time,
                'is_return' => $is_return,
                'is_overdue' => $is_overdue,
                'book_selector' => $book_selector,
                'way' => $way,
                'type' => 1, //新书
                'node' => $node,
                'settle_state' => $settle_state,
                'pur_manage_id' => $pur_manage_id,
                'settle_sponsor_manage_id' => $settle_sponsor_manage_id,
                'shop_all_id' => $shop_all_id,
                'page' =>  $this->page,
                'limit' =>  $this->limit,
                'select_way' => 1,
            ];
            //列表
            $res = $this->bookHomePurchaseModel->lists($param);
            if (empty($res['data'])) {
                $res['data'] = [];
            }
        }
        $file_name = '新书结算清单列表（书店端）-' . date('YmdHis');
        $purchaseLibListExportObj = new PurchaseLibListExportExcel();
        return $purchaseLibListExportObj->export($res['data'], $file_name, 1);
    }
}
