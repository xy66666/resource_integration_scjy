<?php

namespace App\Http\Controllers\Export;

use App\Http\Controllers\Controller;
use App\Models\RecommendBook;
use Maatwebsite\Excel\Facades\Excel;

/**
 * excel导出导入功能 
 */
class RecommendBookExport extends Controller
{

    public $model = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new RecommendBook();
    }

    /** 
     * 用户荐购列表
     * @param start_time string 搜索开始时间
     * @param end_time string 搜索结束时间
     * @param keywords string 搜索关键词
     * @param keywords_type string 选择搜索的字段
     * @param type  int 审核状态  0 或 不传 为全部 1 已审核   2 未审核
     * @param status 状态 1待审核 2审核通过（购买中） 3审核不通过  4 已购买 5 购买失败 
     */
    public function index()
    {
        $start_time = $this->request->start_time ? date("Y-m-d H:i:s", strtotime($this->request->start_time)) : null;
        $end_time = $this->request->end_time ? date("Y-m-d H:i:s", strtotime($this->request->end_time)) : null;
        $keywords_type = $this->request->keywords_type;
        $keywords = $this->request->keywords;
        $type = $this->request->type ? $this->request->type : 0;
        $status = $this->request->status;

        $res = $this->model->lists(null, $keywords, $keywords_type, $type, $status, $start_time, $end_time, 999999);

        //设置表头
        $row = [[
            "id" => '序号',
            "book_name" => '书名',
            "author" => '作者',
            "isbn" => 'ISBN号',
            "price" => '价格',
            "press" => '出版社',
            "pre_time" => '出版时间',
            "number" => '荐购次数',
            "create_time" => '申请时间',
            "change_time" => '审核时间',
            "status" => '处理状态',
            "reason" => '拒绝原因',
        ]];

        $title = '用户荐购书籍列表导出-' . date('YmdHis');
        //执行导出

        $data = $this->setData($res['data']); //要导入的数据

        return $this->exportData($data, $title, $row);
    }


    /**
     * 导出数据
     *
     * @param [type] $data
     * @return void
     */
    public function exportData($data, $title, $row)
    {
        $header = $row; //导出表头
        $excel = new Export($data, $header, $title);
        $excel->setColumnWidth(['A' => 10, 'B' => 30, 'C' => 30, 'D' => 20, 'E' => 20, 'F' => 20, 'G' => 20, 'H' => 20, 'I' => 20, 'J' => 20, 'K' => 20,'L' => 30]);
        $excel->setRowHeight([1 => 30]);
        // $excel->setFreezePane('A2');
        // $excel->setFont(['A1:Z1265' => '宋体']);
        // $excel->setFontSize(['A1:I1' => 14,'A2:Z1265' => 10]);
        // $excel->setBold(['A1:Z2' => true]);
        // $excel->setBackground(['A1:A1' => '808080','C1:C1' => '708080']);
        // $excel->setMergeCells(['A1:I1']);
        // $excel->setBorders(['A2:D5' => '#000000']);
        return Excel::download($excel, $title . '.xlsx');
    }


    /**
     * 处理 终端书籍列表 数据
     */
    public function setData($data)
    {
        $excel_data = [];
        /*设置excel内容*/
        foreach ($data as $key => $val) {
            $excel_data[$key]['id'] = $key + 1;
            $excel_data[$key]['book_name'] = $val['book_name'];
            $excel_data[$key]['author'] = $val['author'];
            $excel_data[$key]['isbn'] = "\t".$val['isbn'];
            $excel_data[$key]['price'] = $val['price'];
            $excel_data[$key]['press'] = $val['press'];
            $excel_data[$key]['pre_time'] = $val['pre_time'];
            $excel_data[$key]['number'] = $val['number'];
            $excel_data[$key]['create_time'] = $val['create_time'];
            $excel_data[$key]['change_time'] = $val['status'] != 1 ? $val['change_time'] : '';
            $excel_data[$key]['status'] = $this->getStatus($val['status']);
            $excel_data[$key]['reason'] = $val['reason'];
        }
        return $excel_data;
    }


    /**
     * 获取状态
     * @param status
     */
    public function getStatus($status)
    {
        switch ($status) {
            case 1:
                $ststus_msg = '待审核';
                break;
            case 2:
                $ststus_msg = '审核通过';
                break;
            case 3:
                $ststus_msg = '审核未通过';
                break;
            case 4:
                $ststus_msg = '已购买';
                break;
            case 5:
                $ststus_msg = '购买失败';
                break;
            default:
                $ststus_msg = '审核未通过';
        }
        return $ststus_msg;
    }
}
