<?php

namespace App\Http\Controllers\Export;

use App\Http\Controllers\Admin\CommonController;
use App\Models\BookHomePostalReturn;
use Maatwebsite\Excel\Facades\Excel;

/**
 * 答题活动-用户活动获取礼物导出
 */
class PostalReturnExport extends CommonController
{
    private $page = 1;
    private $limit = 999999;


    /**
     * 邮递还书列表 导出
     * @param keywords_type  检索条件   0、全部  1、读者证号姓名  2、读者证号 3 条形码 4 快递单号  默认 0
     * @param keywords  检索条件
     * @param start_time 申请开始时间   数据格式    2020-05-12 12:00:00
     * @param end_time 申请结束时间     数据格式    2020-05-12 12:00:00
     */
    public function index()
    {
        $keywords_type = $this->request->keywords_type;
        $keywords = $this->request->keywords;
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;

        $param = [
            'keywords_type' => $keywords_type,
            'keywords' => $keywords,
            'start_time' => $start_time,
            'end_time' => $end_time,
            'page' => $this->page,
            'limit' => $this->limit,
        ];
        $bookHomePostalReturnModel = new BookHomePostalReturn();
        $res = $bookHomePostalReturnModel->lists($param);
        if (empty($res['data'])) {
            $res['data'] = [];
        }

        foreach ($res['data'] as $key => &$val) {
            if ($val['status'] == 3) {
                //归还中
                $postal_day = time_diff($val['create_time'], date('Y-m-d H:i:s'), false)['day'];
            } else {
                $change_time = !empty($val['change_time']) ? $val['change_time'] : date('Y-m-d H:i:s');
                $postal_day = time_diff($val['create_time'], $change_time, false)['day'];
            }
            $res['data'][$key]['postal_day'] = $postal_day;
            // $res['data'][$key][$this->list_index_key] = $this->addSerialNumberOne($key, $page, $limit);
        }

        return $this->export($res['data']);
    }


    /*excel导出*/
    public function export($data)
    {

        //设置表头
        $row = [[
            "index" => '序号',
            "account" => '读者证号',
            "username" => '读者姓名',
            "book_name" => '书名',
            "author" => '作者',
            "isbn" => 'ISBN号',
            "barcode" => '条形码',
            "status_name" => '归还状态',
            "borrow_time" => '借阅时间',
            "expire_time" => '应归还时间',
            "tracking_number" => '快递单号',
            "create_time" => '邮递时间',
            "postal_day" => '邮寄天数',
        ]];

        $title = '新书推荐导出-' . date('YmdHis');
        //执行导出
        $data = $this->setData($data); //要导入的数据
        return $this->exportData($data, $title, $row);
    }


    /**
     * 导出数据
     *
     * @param [type] $data
     * @return void
     */
    public function exportData($data, $title, $row)
    {

        $header = $row; //导出表头

        $excel = new Export($data, $header, $title);
        $excel->setColumnWidth(['A' => 10, 'B' => 30, 'C' => 30, 'D' => 20, 'E' => 20, 'F' => 20, 'G' => 20, 'H' => 20, 'I' => 20, 'J' => 20, 'K' => 30, 'L' => 20]);
        $excel->setRowHeight([1 => 30]);
        // $excel->setFreezePane('A2');
        // $excel->setFont(['A1:Z1265' => '宋体']);
        // $excel->setFontSize(['A1:I1' => 14,'A2:Z1265' => 10]);
        // $excel->setBold(['A1:Z2' => true]);
        // $excel->setBackground(['A1:A1' => '808080','C1:C1' => '708080']);
        // $excel->setMergeCells(['A1:I1']);
        // $excel->setBorders(['A2:D5' => '#000000']);

        return Excel::download($excel, $title . '.xlsx');
    }


    /**
     * 处理 终端书籍列表 数据
     */
    public function setData($data)
    {
        $excel_data = [];
        /*设置excel内容*/
        foreach ($data as $key => $val) {
            $excel_data[$key]['index'] = $key + 1;
            $excel_data[$key]['account'] = $val['account'];
            $excel_data[$key]['username'] = $val['username'];
            $excel_data[$key]['book_name'] = $val['book_name'];
            $excel_data[$key]['author'] = $val['author'];
            $excel_data[$key]['isbn'] = "\t" . $val['isbn'];
            $excel_data[$key]['barcode'] = "\t" . $val['barcode'];
            $excel_data[$key]['status_name'] = $this->getStatusName($val['status']);
            $excel_data[$key]['borrow_time'] = $val['borrow_time'];
            $excel_data[$key]['expire_time'] = $val['expire_time'];
            $excel_data[$key]['tracking_number'] = $val['tracking_number'];
            $excel_data[$key]['create_time'] = $val['create_time'];
            $excel_data[$key]['postal_day'] = $val['postal_day'];
        }
        return $excel_data;
    }


    /**
     * 状态
     * $status
     */
    public function getStatusName($status)
    {
        switch ($status) {
            case 1:
                $state = '已确认归还';
                break;
            case 2:
                $state = '已取消归还(管理员取消)';
                break;
            case 4:
                $state = '已撤销（用户自己操作）';
                break;
            default:
                $state = '归还中';
                break;
        }
        return $state;
    }
}
