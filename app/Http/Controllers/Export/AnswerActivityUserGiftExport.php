<?php

namespace App\Http\Controllers\Export;

use App\Http\Controllers\Admin\CommonController;
use App\Models\AnswerActivity;
use App\Models\AnswerActivityUnit;
use App\Models\AnswerActivityUserGift;
use App\Models\AnswerActivityUserUnit;
use App\Models\UserDrawAddress;;

use App\Validate\AnswerActivityUserGiftValidate;
use Maatwebsite\Excel\Facades\Excel;

/**
 * 答题活动-用户活动获取礼物导出
 */
class AnswerActivityUserGiftExport extends CommonController
{

    public $model = null;
    public $activityModel = null;
    public $validate = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new AnswerActivityUserGift();
        $this->activityModel = new AnswerActivity();
        $this->validate = new AnswerActivityUserGiftValidate();
    }




    /**
     * 获取用户礼物列表
     * @param act_id int 活动id
     * @param state string 礼物状态 1未发放 2已发放 3未发货 4邮递中 5已到货  8 红包发放中
     * @param type int 1文化红包 2精美礼品
     * @param unit_id int  单位id  空或null 代表  全部  0代表系统
     * @param keywords string 搜索关键词
     * @param start_time datetime 开始时间    数据格式  年月日
     * @param end_time datetime 结束时间
     */
    public function index()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $act_info = $this->activityModel->detail($this->request->act_id, null, null);

        if (empty($act_info)) {
            return $this->returnApi(201, '参数错误');
        }
        if ($act_info['prize_form'] == 1) {
            return $this->returnApi(201, '此活动为排名类活动');
        }
        $res = $this->model->lists($this->request->act_id, null, $this->request->keywords, $this->request->type, $this->request->state, $this->request->unit_id, $this->request->start_time, $this->request->end_time, 1, 99999);

        if (empty($res['data'])) {
            return $this->returnApi(203, '暂无数据');
        }

        $userDrawAddressModel = new UserDrawAddress();
        $activityUserUnitModel = new AnswerActivityUserUnit();
        foreach ($res['data'] as $key => $val) {
            if ($act_info['node'] != 1) {
                if ($val['unit_id']) {
                    $res['data'][$key]['unit_name'] = AnswerActivityUnit::where('act_id', $this->request->act_id)->where('id', $val['unit_id'])->value('name');
                } else {
                    $res['data'][$key]['unit_name'] = null;
                }
            } else {
                $res['data'][$key]['unit_name'] = null;
            }
            // $res['data'][$key]['user_info'] = $userDrawAddressModel->detail($val['user_guid'], 1, $this->request->act_id);

            $res['data'][$key]['user_info'] =  $activityUserUnitModel->getUserUnitInfo($val['user_guid'], $this->request->act_id);

            $res['data'][$key]['user_draw_address_info'] = $userDrawAddressModel->detail($val['user_guid'], 1, $this->request->act_id);
        }
        $real_info = $act_info['real_info'];
        $real_info = explode('|', $real_info);

        $row[0]["id"] = '序号';
        $row[0]["order_id"] = '订单号';

        if (in_array(1, $real_info)) $row[0]["username"] = '姓名';
        if (in_array(2, $real_info)) $row[0]["tel"] = '电话';
        if (in_array(3, $real_info)) $row[0]["id_card"] = '身份证号码';
        if (in_array(4, $real_info)) $row[0]["tel"] = '读者证号';

        $row[0]["address"] = '邮递地址';
        $row[0]["name"] = '礼物名称';
        $row[0]["state"] = '状态';
        $row[0]["intro"] = '礼物描述';
        $row[0]["way"] = '领取方式';
        $row[0]["type"] = '礼物类型';
        $row[0]["unit_name"] = '礼物单位';
        $row[0]["create_time"] = '获奖时间';

        $title = '获奖名单列表-' . date('YmdHis');

        $data = $this->setData($res['data'], $real_info); //要导入的数据

        return $this->exportData($data, $title, $row);
    }

    /**
     * 导出数据
     *
     * @param [type] $data
     * @return void
     */
    public function exportData($data, $title, $row)
    {
        $header = $row; //导出表头
        $excel = new Export($data, $header, $title);
        $excel->setColumnWidth(['A' => 10, 'B' => 35, 'C' => 20, 'D' => 20, 'E' => 20, 'F' => 20, 'G' => 20, 'H' => 25, 'I' => 25, 'J' => 20, 'K' => 20, 'L' => 20, 'M' => 20, 'N' => 20, 'O' => 30]);
        $excel->setRowHeight(30, count($data) + 1);
        // $imgPosition = $node != 1 ? 'D' : 'C';
        // $excel->setExcelImg($img , $imgPosition , 40,40);
        // $excel->setFreezePane('A2');
        // $excel->setFont(['A1:Z1265' => '宋体']);
        // $excel->setFontSize(['A1:I1' => 14,'A2:Z1265' => 10]);
        // $excel->setBold(['A1:Z2' => true]);
        // $excel->setBackground(['A1:A1' => '808080','C1:C1' => '708080']);
        // $excel->setMergeCells(['A1:I1']);
        // $excel->setBorders(['A2:D5' => '#000000']);
        return Excel::download($excel, $title . '.xlsx');
    }


    /**
     * 处理 终端书籍列表 数据
     */
    public function setData($data, $real_info)
    {
        $excel_data = [];
        /*设置excel内容*/
        foreach ($data as $key => $val) {
            $excel_data[$key]['id'] = $key + 1;
            //  $excel_data[$key]["order_id"] = $val["order_id"] . "\t";
            // $order_id = number_format($val["order_id"], 0, '', '');
            $excel_data[$key]["order_id"] = "\t" . $val["order_id"];

            if (in_array(1, $real_info)) $excel_data[$key]["username"] = !empty($val['user_info']) ? $val['user_info']["username"] : '';
            if (in_array(2, $real_info)) $excel_data[$key]["tel"] =  !empty($val['user_info']) ? "\t" . $val['user_info']["tel"] : '';
            if (in_array(3, $real_info)) $excel_data[$key]["id_card"] =  !empty($val['user_info']) ? "\t" . $val['user_info']["id_card"] : '';
            if (in_array(4, $real_info)) $excel_data[$key]["reader_id"] =  !empty($val['user_info']) ? "\t" . $val['user_info']["reader_id"] : '';

            $excel_data[$key]["address"] =  !empty($val['user_draw_address_info']["province"]) ? ($val['user_draw_address_info']["province"] . $val['user_draw_address_info']["city"] . $val['user_draw_address_info']["district"] . $val['user_draw_address_info']["address"]) : '';
            $excel_data[$key]["name"] = $val['name'];
            $excel_data[$key]["state"] = $this->getState($val['state']);
            $excel_data[$key]["intro"] = $val['type'] == 1 ? ('红包金额' . $val['price'] . '元') : $val["intro"];
            $excel_data[$key]["way"] = $val['type'] == 1 ? '' : ($val['way'] == 1 ? '自提' : '邮递');
            $excel_data[$key]["type"] = $val['type'] == 1 ? '文化红包' : '精美礼品';
            $excel_data[$key]["unit_name"] = $val["unit_name"];
            $excel_data[$key]["create_time"] = $val["create_time"];
        }
        return [$excel_data];
    }
    /**
     * Undocumented function
     *
     * @param $state 礼物状态 1未发放 2已发放 3未发货 4邮递中 5已到货 6 未领取  7 已领取
     */
    public function getState($state)
    {
        switch ($state) {
            case 1:
                return '未发放';
                break;
            case 2:
                return '已发放';
                break;
            case 3:
                return '未发货';
                break;
            case 4:
                return '邮递中';
                break;
            case 5:
                return '已到货';
                break;
            case 6:
                return '未领取';
                break;
            case 7:
                return '已领取';
                break;
            case 8:
                return '发放中';
                break;
        }
    }
}
