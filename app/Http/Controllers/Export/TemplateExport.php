<?php

namespace App\Http\Controllers\Export;

use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

/**
 * excel模板导出功能 
 */
class TemplateExport extends Controller
{

    /** 
     * 新书推荐模板导出
     */
    public function newBookRecommend()
    {
        //设置表头
        $row = [[
            "id" => '序号',
            "book_name" => '书名',
            "author" => '作者',
            "isbn" => 'ISBN号',
            "price" => '价格',
            "book_num" => '索书号',
            "press" => '出版社',
            "pre_time" => '出版时间',
            "type_name" => '书籍类型（自定义）',
            "is_recom" => '是否推荐（是或否）',
            "intro" => '简介',
        ]];
        $data = [[
            "id" => '1',
            "book_name" => '彼岸',
            "author" => '鲁娃著',
            "isbn" => '9787521203646',
            "price" => 'CNY45.00',
            "book_num" => 'I247.57/12670',
            "press" => '作家出版社',
            "pre_time" => '2019',
            "type_name" => '综合性图书',
            "is_recom" => '是',
            "intro" => '简介',
        ]];

        $title = '新书推荐模板下载';
        return $this->exportData($data, $title, $row);
    }

    /** 
     * 书店新书模板导出
     */
    public function shopNewBookRecommend()
    {
        //设置表头
        $row = [[
            "id" => '序号',
            "book_name" => '书名',
            "author" => '作者',
            "isbn" => 'ISBN号',
            "price" => '价格',
            "book_num" => '索书号',
            "press" => '出版社',
            "pre_time" => '出版时间',
            "number" => '库存',
            "type_name" => '书籍类型（自定义）',
            "is_recom" => '是否推荐（是或否）',
            "intro" => '简介',
        ]];
        $data = [[
            "id" => '1',
            "book_name" => '彼岸',
            "author" => '鲁娃著',
            "isbn" => '9787521203646',
            "price" => 'CNY45.00',
            "book_num" => 'I247.57/12670',
            "press" => '作家出版社',
            "pre_time" => '2019',
            "number" => '1',
            "type_name" => '综合性图书',
            "is_recom" => '是',
            "intro" => '简介',
        ]];

        $title = '书店新书模板下载';
        return $this->exportData($data, $title, $row);
    }

    /**
     * 馆藏书模板下载
     */
    public function libBookRecommend()
    {
        // $title = ['书名','作者','ISBN','条形码','价格','出版社','出版时间','索书号','馆藏地址（馆藏地址必须先在系统预设）','简介'];
        $row = [[
            "id" => '序号',
            "isbn" => 'ISBN号',
            "barcode" => '条形码',
            "type_name" => '类型名称',
        ]];
        $data = [[
            "id" => '1',
            "isbn" => "\t" . '9787530216194',
            "barcode" => "\t" . '01100001',
            "type_name" => '综合性图书',
        ]];
        $title = '馆藏书模板下载';
        return $this->exportData($data, $title, $row);
    }


    /**
     * 导出数据
     *
     * @param [type] $data
     * @return void
     */
    public function exportData($data, $title, $row)
    {
        $header = $row; //导出表头
        $excel = new Export($data, $header, $title);
        $excel->setColumnWidth(['A' => 10, 'B' => 20, 'C' => 20, 'D' => 20, 'E' => 50, 'F' => 20, 'G' => 20, 'H' => 20, 'I' => 20, 'J' => 20, 'K' => 20, 'L' => 20]);
        $excel->setRowHeight([1 => 30]);
        // $excel->setFreezePane('A2');
        // $excel->setFont(['A1:Z1265' => '宋体']);
        // $excel->setFontSize(['A1:I1' => 14,'A2:Z1265' => 10]);
        // $excel->setBold(['A1:Z2' => true]);
        // $excel->setBackground(['A1:A1' => '808080','C1:C1' => '708080']);
        // $excel->setMergeCells(['A1:I1']);
        // $excel->setBorders(['A2:D5' => '#000000']);
        return Excel::download($excel, $title . '.xlsx');
    }
}
