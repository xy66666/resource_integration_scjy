<?php

namespace App\Http\Controllers\Export;

use App\Http\Controllers\Admin\CommonController;
use App\Models\GoodsOrder;
use Maatwebsite\Excel\Facades\Excel;

/**
 *商品兑换导出
 */
class GoodsExchangeExport extends CommonController
{

    private $limit = 999999;
    private $model = null;

    public function __construct()
    {
        parent::__construct();
        $this->model = new GoodsOrder();
    }


    /**
     * 商品兑换记录 导出
     * @param send_way int 兑换方式(1 自提 2 邮递  3、2者都可以) 
     * @param start_time datetime 创建时间范围搜索(开始) 
     * @param end_time datetime 创建时间范围搜索(结束) 
     * @param type_id int 商品类型id
     * @param status int 订单状态 1 待领取  2已取走（自提） 3待发货  4已发货 （邮递）  5 已过期   6 已取消 
     * @param keywords string 搜索关键词
     * @param keywords_type int 搜索关键词类型 order_number 订单号  name 商品名   username 姓名  reader_id 读者证号  redeem_code 兑换码
     */
    public function index()
    {
        $this->model->checkOrderStatus(); //处理已过期的订单
        $this->model->checkNoPayOrder(); //获取未支付的订单，判断是否过期，修改状态

        $send_way = $this->request->send_way;
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        $type_id = $this->request->type_id;
        $status = $this->request->status;
        $keywords = $this->request->keywords;
        $keywords_type = $this->request->keywords_type;

        $res = $this->model->orderList($keywords, $keywords_type, $type_id, $status, $send_way, $start_time, $end_time, $this->limit);
        if (empty($res['data'])) {
            $res['data'] = [];
        }

        return $this->export($res['data']);
    }


    /*excel导出*/
    public function export($data)
    {
        //设置表头
        $row = [[
            "index" => '序号',
            "username" => '用户姓名',
            "order_number" => '订单号',
            "status_name" => '订单状态',
            "tel" => '电话号码',
            "account" => '读者证号',
            "create_time" => '兑换时间',
            "tracking_number" => '快递单号',
            "address" => '收件人地址',
            "name" => '商品',
            "score" => '所需积分',
            "send_way_name" => '兑换方式',
            "grounding_time" => '上架时间',
            "type_name" => '商品类别',
            "send_address" => '发货地址',
        ]];

        $title = '商品兑换记录列表导出-' . date('YmdHis');
        //执行导出
        $data = $this->setData($data); //要导入的数据
        return $this->exportData($data, $title, $row);
    }

    /**
     * 导出数据
     *
     * @param [type] $data
     * @return void
     */
    public function exportData($data, $title, $row)
    {

        $header = $row; //导出表头

        $excel = new Export($data, $header, $title);
        $excel->setColumnWidth(['A' => 10, 'B' => 20, 'C' => 20, 'D' => 20, 'E' => 20, 'F' => 20, 'G' => 20, 'H' => 20, 'I' => 20, 'J' => 20, 'K' => 30, 'L' => 20, 'M' => 20, 'N' => 20, 'O' => 20, 'P' => 20, 'Q' => 20]);
        $excel->setRowHeight([1 => 30]);
        // $excel->setFreezePane('A2');
        // $excel->setFont(['A1:Z1265' => '宋体']);
        // $excel->setFontSize(['A1:I1' => 14,'A2:Z1265' => 10]);
        // $excel->setBold(['A1:Z2' => true]);
        // $excel->setBackground(['A1:A1' => '808080','C1:C1' => '708080']);
        // $excel->setMergeCells(['A1:I1']);
        // $excel->setBorders(['A2:D5' => '#000000']);

        return Excel::download($excel, $title . '.xlsx');
    }


    /**
     * 处理 终端书籍列表 数据
     */
    public function setData($data)
    {
        $excel_data = [];
        /*设置excel内容*/
        foreach ($data as $key => $val) {
            $excel_data[$key]['index'] = $key + 1;
            $excel_data[$key]['username'] = $val['username'];
            $excel_data[$key]['order_number'] = "\t".$val['order_number'];
            $excel_data[$key]['status_name'] =  $this->model->getStatusName($val['status']);
            $excel_data[$key]['tel'] = "\t".$val['tel'];
            $excel_data[$key]['account'] = "\t".$val['account'];
            $excel_data[$key]['create_time'] = $val['create_time'];
            $excel_data[$key]['tracking_number'] = $val['tracking_number'];
            $excel_data[$key]['address'] = $val['province'] . $val['city'] . $val['district'] . $val['street'] . $val['address'];
            $excel_data[$key]['name'] = $val['name'];
            $excel_data[$key]['score'] = $val['score'];
            $excel_data[$key]['send_way_name'] = $this->model->getSendWayName($val['send_way']);
            $excel_data[$key]['grounding_time'] = $val['grounding_time'];
            $excel_data[$key]['type_name'] = $val['type_name'];
            $excel_data[$key]['send_address'] = $val['send_province'] . $val['send_city'] . $val['send_district'] . $val['send_street'] . $val['send_address'];
        }
        return $excel_data;
    }


 
}
