<?php

namespace App\Http\Controllers\Export;

use App\Http\Controllers\Controller;
use App\Models\NewBookRecommend;
use Maatwebsite\Excel\Facades\Excel;

/**
 * excel导出导入功能 
 */
class NewBookRecommendExport extends Controller
{

    public $model = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new NewBookRecommend();
    }

    /**
     * 新书推荐列表导出
     * @param page int 当前页
     * @param limit int 分页大小
     * @param type_id int 类型id  0或空表示全部
     * @param is_recom int 是否推荐  1 推荐  2 未推荐 
     * @param keywords string 搜索关键词(书名、作者、isbn号)
     */
    public function index()
    {
        $keywords = $this->request->keywords;
        $type_id = $this->request->type_id;
        $is_recom = $this->request->is_recom;

        $condition[] = ['is_del', '=', 1];
        if ($type_id) {
            $condition[] = ['type_id', '=', $type_id];
        }
        if ($is_recom) {
            $condition[] = ['is_recom', '=', $is_recom];
        }

        $res = $this->model->with(['conType' => function ($query) {
            $query->select('type_name');
        }])
            ->where($condition)
            ->where(function ($query) use ($keywords) {
                if ($keywords) {
                    $query->where('book_name', 'like', "%$keywords%")
                        ->orWhere('author', 'like', "%$keywords%")
                        ->orWhere('isbn', 'like', "%$keywords%");
                }
            })
            ->orderByDesc('id')
            ->get()
            ->toArray();


        //设置表头
        $row = [[
            "id" => '序号',
            "book_name" => '书名',
            "author" => '作者',
            "isbn" => 'ISBN号',
            "price" => '价格',
            "book_num" => '索书号',
            "press" => '出版社',
            "pre_time" => '出版时间',
            "type_name" => '书籍类型',
            "is_recom" => '是否推荐',
            "intro" => '简介',
            "create_time" => '添加时间',
        ]];

        $title = '新书推荐导出-' . date('YmdHis');
        //执行导出
        $data = $this->setData($res); //要导入的数据
        return $this->exportData($data, $title, $row);
    }

    /**
     * 新书推荐模板导出
     * @param page int 当前页
     * @param limit int 分页大小
     * @param type_id int 类型id  0或空表示全部
     * @param is_recom int 是否推荐  1 推荐  2 未推荐 
     * @param keywords string 搜索关键词(书名、作者、isbn号)
     */
    public function template()
    {
        //设置表头
        $row = [[
            "id" => '序号',
            "book_name" => '书名',
            "author" => '作者',
            "isbn" => 'ISBN号',
            "price" => '价格',
            "book_num" => '索书号',
            "press" => '出版社',
            "pre_time" => '出版时间',
            "type_name" => '书籍类型（自定义）',
            "is_recom" => '是否推荐（是或否）',
            "intro" => '简介',
        ]];

        $data = [[
            "id" => '1',
            "book_name" => '彼岸',
            "author" => '鲁娃著',
            "isbn" => '9787521203646',
            "price" => 'CNY45.00',
            "book_num" => 'I247.57/12670',
            "press" => '作家出版社',
            "pre_time" => '2019',
            "type_name" => '综合性图书',
            "is_recom" => '是',
            "intro" => '简介',
        ]];

        $title = '新书推荐导入模板';
        return $this->exportData($data, $title, $row);
    }




    /**
     * 导出数据
     *
     * @param [type] $data
     * @return void
     */
    public function exportData($data, $title, $row)
    {
     
        $header = $row; //导出表头

        $excel = new Export($data, $header, $title);
        $excel->setColumnWidth(['A' => 10, 'B' => 30, 'C' => 30, 'D' => 20, 'E' => 20, 'F' => 20, 'G' => 20, 'H' => 20, 'I' => 20, 'J' => 20, 'K' => 30, 'L' => 20]);
        $excel->setRowHeight([1 => 30]);
        // $excel->setFreezePane('A2');
        // $excel->setFont(['A1:Z1265' => '宋体']);
        // $excel->setFontSize(['A1:I1' => 14,'A2:Z1265' => 10]);
        // $excel->setBold(['A1:Z2' => true]);
        // $excel->setBackground(['A1:A1' => '808080','C1:C1' => '708080']);
        // $excel->setMergeCells(['A1:I1']);
        // $excel->setBorders(['A2:D5' => '#000000']);

        return Excel::download($excel, $title . '.xlsx');
    }


    /**
     * 处理 终端书籍列表 数据
     */
    public function setData($data)
    {
        $excel_data = [];
        /*设置excel内容*/
        foreach ($data as $key => $val) {
            $excel_data[$key]['id'] = $key + 1;
            $excel_data[$key]['book_name'] = $val['book_name'];
            $excel_data[$key]['author'] = $val['author'];
            $excel_data[$key]['isbn'] = "\t".$val['isbn'];
            $excel_data[$key]['price'] = $val['price'];
            $excel_data[$key]['book_num'] = $val['book_num'];
            $excel_data[$key]['press'] = $val['press'];
            $excel_data[$key]['pre_time'] = $val['pre_time'];
            $excel_data[$key]['type_name'] = !empty($val['con_type']['title']) ? $val['con_type']['title'] : '';
            $excel_data[$key]['is_recom'] = $val['is_recom'] == 1 ? '推荐' : '未推荐';
            $excel_data[$key]['intro'] = strip_tags($val['intro']);
            $excel_data[$key]['create_time'] = !empty($val['create_time']) ? $val['create_time'] : '';
        }
        return $excel_data;
    }
}
