<?php

namespace App\Http\Controllers\Export;

use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

/**
 * 书店新书采购   申请列表、发货列表、所有订单列表
 * Class NewBookRecomOrderExport
 * @package app\lib\Export
 */
class BookHomeExportExcel  extends Controller
{

    /*合并单元格参数*/
    protected $merge_cells = [];


    /*excel表标题的高度*/
    protected $header_length = 1;

    //合并行数
    protected $merge_num = 5;

    /**
     * excel导出
     *  $type 1新书  2 馆藏书 
     **/
    public function export($data, $type, $is_pay)
    {

        if ($type == 1) {
            if ($is_pay == 2) {
                //申请列表
                $row = [[
                    "id" => '序号',
                    "create_time" => '申请时间',
                    "order_number" => '订单号',
                    "username" => '借阅人姓名',
                    "account" => '读者证号',
                    "shop_name" => '所属书店',
                    "book_name" => '书名',
                    "author" => '作者',
                    "isbn" => 'ISBN号',
                    "price" => '价格',
                    "press" => '出版社',
                    "pre_time" => '出版时间',
                    "book_selector" => '是否套书',
                    "rack_type" => '架类型',
                    "rack_describe" => '架描述',
                    "rack_code" => '架代码'
                ]];

                $this->merge_num = 6;

                $file_name = '新书申请列表-' . date('YmdHis');
            } else if ($is_pay == 6) {
                //发货列表
                $row = [[
                    "id" => '序号',
                    "create_time" => '申请时间',
                    "order_number" => '订单号',
                    "username" => '借阅人姓名',
                    "account" => '读者证号',
                    "shop_name" => '所属书店',
                    "book_name" => '书名',
                    "author" => '作者',
                    "isbn" => 'ISBN号',
                    "price" => '价格',
                    "press" => '出版社',
                    "pre_time" => '出版时间',
                    "book_selector" => '是否套书',
                    "rack_type" => '架类型',
                    "rack_describe" => '架描述',
                    "rack_code" => '架代码',
                    "address_username" => '收货人姓名',
                    "tel" => '收货人电话',
                    "address" => '收货人地址',
                ]];

                $this->merge_num = 6;

                $file_name = '新书发货列表-' . date('YmdHis');
            } else {
                //所有订单               
                $row = [[
                    "id" => '序号',
                    "create_time" => '申请时间',
                    "order_number" => '订单号',
                    "username" => '借阅人姓名',
                    "account" => '读者证号',
                    "shop_name" => '所属书店',
                    "book_name" => '书名',
                    "author" => '作者',
                    "isbn" => 'ISBN号',
                    "price" => '价格',
                    "press" => '出版社',
                    "pre_time" => '出版时间',
                    "book_selector" => '是否套书',
                    "rack_type" => '架类型',
                    "rack_describe" => '架描述',
                    "rack_code" => '架代码',
                    "address_username" => '收货人姓名',
                    "tel" => '收货人电话',
                    "address" => '收货人地址',
                    "state_name" => '状态',
                    "time" => '状态变化时间',
                ]];

                $this->merge_num = 6;

                $file_name = '新书所有订单-' . date('YmdHis');
            }
        } else {
            if ($is_pay == 2) {
                //申请列表
                $row = [[
                    "id" => '序号',
                    "create_time" => '申请时间',
                    "order_number" => '订单号',
                    "username" => '借阅人姓名',
                    "account" => '读者证号',
                    "book_name" => '书名',
                    "author" => '作者',
                    "isbn" => 'ISBN号',
                    "barcode" => '条形码',
                    "press" => '出版社',
                    "pre_time" => '出版时间',
                    "price" => '价格',
                    "book_num" => '索书号',
                    "cur_local_note" => '馆藏地'
                ]];

                $this->merge_num = 5;
                $file_name = '馆藏书申请列表-' . date('YmdHis');
            } else if ($is_pay == 6) {
                //发货列表
                $row = [[
                    "id" => '序号',
                    "create_time" => '申请时间',
                    "order_number" => '订单号',
                    "username" => '借阅人姓名',
                    "account" => '读者证号',
                    "book_name" => '书名',
                    "author" => '作者',
                    "isbn" => 'ISBN号',
                    "barcode" => '条形码',
                    "press" => '出版社',
                    "pre_time" => '出版时间',
                    "price" => '价格',
                    "book_num" => '索书号',
                    "cur_local_note" => '馆藏地',
                    "address_username" => '收货人姓名',
                    "tel" => '收货人电话',
                    "address" => '收货人地址',
                ]];
                $this->merge_num = 5;
                $file_name = '馆藏书发货列表-' . date('YmdHis');
            } else {
                //所有订单               
                $row = [[
                    "id" => '序号',
                    "create_time" => '申请时间',
                    "order_number" => '订单号',
                    "username" => '借阅人姓名',
                    "account" => '读者证号',
                    "book_name" => '书名',
                    "author" => '作者',
                    "isbn" => 'ISBN号',
                    "barcode" => '条形码',
                    "press" => '出版社',
                    "pre_time" => '出版时间',
                    "price" => '价格',
                    "book_num" => '索书号',
                    "cur_local_note" => '馆藏地',
                    "address_username" => '收货人姓名',
                    "tel" => '收货人电话',
                    "address" => '收货人地址',
                    "state_name" => '状态',
                    "time" => '状态变化时间',
                ]];
                $this->merge_num = 5;
                $file_name = '馆藏书所有订单-' . date('YmdHis');
            }
        }
        //执行导出
        $data = $this->setData($data, $is_pay); //要导入的数据
        return $this->exportData($data, $file_name, $row);
    }


    /**
     * 导出数据
     *
     * @param [type] $data
     * @return void
     */
    public function exportData($data, $title, $row)
    {

        $header = $row; //导出表头

        $excel = new Export($data, $header, $title);
        $excel->setColumnWidth(['A' => 10, 'B' => 20, 'C' => 20, 'D' => 15, 'E' => 20, 'F' => 15, 'G' => 15, 'H' => 15, 'I' => 15, 'J' => 15, 'K' => 15, 'L' => 15, 'M' => 10, 'N' => 15, 'O' => 15, 'P' => 20, 'Q' => 20, 'R' => 20, 'S' => 20, 'T' => 20, 'U' => 20, 'V' => 20, 'W' => 20, 'X' => 20, 'Y' => 20, 'Z' => 20]);
        $excel->setRowHeight([1 => 30]);
        // $excel->setFreezePane('A2');
        // $excel->setFont(['A1:Z1265' => '宋体']);
        // $excel->setFontSize(['A1:I1' => 14,'A2:Z1265' => 10]);
        // $excel->setBold(['A1:Z2' => true]);
        // $excel->setBackground(['A1:A1' => '808080','C1:C1' => '708080']);
        //  $excel->setMergeCells(['A1:I1']);//合并数据

        $merge_cells_data = $this->getMergeCells($this->merge_cells); //获取合并数据
        $excel->setMergeCells($merge_cells_data); //合并数据
        // $excel->setBorders(['A2:D5' => '#000000']);
        return Excel::download($excel, $title . '.xlsx');
    }

    /**
     * 处理 终端书籍列表 数据
     */
    public function setData($data, $type)
    {
        $excel_data = [];

        /*设置excel内容*/
        $line = 0;
        foreach ($data as $key => $val) {
            $merge_cells = [];
            $tempArray = $val['book_info'];
            $merge_cells['start_index'] = $line + $this->header_length + 1;

            //$tempArray 为 需要合并的对象数组，为$data 里面的一个数组元素
            foreach ($tempArray as $k => $v) {
                $temp = [];
                $temp[$k]['id'] = $line + 1;
                $temp[$k]['create_time'] = $val['create_time'];
                $temp[$k]['order_number'] = "\t" .  $val['order_number'];
                $temp[$k]['username'] = $val['username'];
                $temp[$k]['account'] = "\t" . $val['account'];
                if (isset($val['shop_name'])) $temp[$k]['shop_name'] = $val['shop_name'];
                foreach ($v as $book_key => $book_val) {
                    if ($book_key == 'book_selector') {
                        $temp[$k][$book_key] = $v['book_selector'] == 1 ? '是' : '否';
                    } else {
                        $temp[$k][$book_key] = is_numeric($book_val) && mb_strlen($book_val) > 11 ? "\t" . $book_val : $book_val;
                    }
                }
                if ($type == 6) {
                    $temp[$k]['address_username'] = $val['address_username'];
                    $temp[$k]['tel'] = "\t".$val['tel'];
                    $temp[$k]['address'] = $val['province'] . $val['city'] . $val['district']. $val['street'] . $val['address'];
                }
                if ($type == 5 || $type == 7 || $type == 8 || $type == 9 || $type == 5789) {
                    $temp[$k]['address_username'] = $val['address_username'];
                    $temp[$k]['tel'] = "\t".$val['tel'];
                    $temp[$k]['address'] = $val['province'] . $val['city'] . $val['district']. $val['street'] . $val['address'];
                    $temp[$k]['state_name'] = $val['state_name'];
                    $temp[$k]['time'] = $val['time'];
                }
                $excel_data[] = $temp;
                $line++;
            }

            $merge_cells['end_index'] = $line + $this->header_length; //合并结束   5标识合并到哪里
            $this->merge_cells[] = $merge_cells; //需要合并的数据
        }

        return $excel_data;
    }

    /**
     * 获取合并数据
     * @param merge array 合并参数
     * @param excel object excel表实例对象
     */
    public function getMergeCells($merge)
    {
        $data = [];
        foreach ($merge as $key => $val) {
            //5为合并行数
            for ($i = 1; $i <= $this->merge_num; $i++) {
                $data[] = decimal_to_letter($i) . $val['start_index'] . ':' . decimal_to_letter($i) . $val['end_index'];
            }
        }
        return $data;
    }
}
