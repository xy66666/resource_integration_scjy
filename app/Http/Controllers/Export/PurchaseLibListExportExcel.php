<?php
/**
 * Created by PhpStorm.
 * User: cdbottle
 * Date: 2020-05-23
 * Time: 11:55
 */

namespace App\Http\Controllers\Export;

use Maatwebsite\Excel\Facades\Excel;

/**
 * 新书结算清单  导出
 */
class PurchaseLibListExportExcel
{

     /**
      * excel导出
      * @param   type  类型 1 新书  2 馆藏书
      */
     public function export($data, $title, $type = 1)
     {
         //申请列表
         if ($type == 1) {
             //设置表头
             $row = [[
                "index" => '序号',
                "shop_name" => '书店名称',
                "username" => '读者姓名',
                "account" => '读者证号',
                "book_name" => '书名',
                "author" => '作者',
                "ISBN" => 'ISBN号',
                "barcode" => '条形码',
                "price" => '价格',
                "press" => '出版社',
                "pre_time" => '出版时间',
                "book_selector" => '是否套书',
                "way" => '采购方式',
                "settle_state" => '结算状态',
                "pur_manage_name" => '确认借阅人',
                "create_time" => '确认借阅时间',
                "selttle_sponsor_manage_name" => '书店结算人',
                "settle_sponsor_time" => '书店结算时间',
             ]];
         } else {
             //设置表头
             $row = [[
                "index" => '序号',
                "shop_name" => '书店名称',
                "username" => '读者姓名',
                "account" => '读者证号',
                "book_name" => '书名',
                "author" => '作者',
                "ISBN" => 'ISBN号',
                "barcode" => '条形码',
                "price" => '价格',
                "press" => '出版社',
                "pre_time" => '出版时间',
                "book_selector" => '是否套书',
                "way" => '采购方式',
                "settle_state" => '结算状态',
                "pur_manage_name" => '确认借阅人',
                "create_time" => '确认借阅时间',
                "settle_affirm_manage_name" => '图书馆结算人',
                "settle_affirm_time" => '图书馆结算时间',
             ]];
         }
         //执行导出
         $data = $this->setData($data, $type); //要导入的数据
         return $this->exportData($data, $title, $row);
     }
 


       /**
      * 导出数据
      *
      * @param [type] $data
      * @return void
      */
      public function exportData($data, $title, $row)
      {
          $header = $row; //导出表头
          $excel = new Export($data, $header, $title);
          $excel->setColumnWidth(['A' => 10, 'B' => 20, 'C' => 20, 'D' => 20, 'E' => 20, 'F' => 20, 'G' => 20, 'H' => 20, 'I' => 20, 'J' => 20, 'K' => 20, 'L' => 15, 'M' => 20, 'N' => 20, 'O' => 20, 'P' => 20, 'Q' => 20, 'R' => 20]);
          $excel->setRowHeight([1 => 30]);
          // $excel->setFreezePane('A2');
          // $excel->setFont(['A1:Z1265' => '宋体']);
          // $excel->setFontSize(['A1:I1' => 14,'A2:Z1265' => 10]);
          // $excel->setBold(['A1:Z2' => true]);
          // $excel->setBackground(['A1:A1' => '808080','C1:C1' => '708080']);
          // $excel->setMergeCells(['A1:I1']);
          // $excel->setBorders(['A2:D5' => '#000000']);
          return Excel::download($excel, $title . '.xlsx');
      }
  
  
      /**
       * 处理 终端书籍列表 数据
       */
      public function setData($data, $type)
      {
          $excel_data = [];
          /*设置excel内容*/
          foreach ($data as $key => $val) {
            $excel_data[$key]['index'] = $key + 1;
            $excel_data[$key]['shop_name'] = $val['shop_name'];
            $excel_data[$key]['username'] = $val['username'];
            $excel_data[$key]['account'] = $val['account'];
           // $excel_data[$key]['book_type'] = !empty($val['shop_id']) ? '新书' : '馆藏书';
            $excel_data[$key]['book_name'] = $val['book_name'];
            $excel_data[$key]['author'] = $val['author'];
            $excel_data[$key]['isbn'] = "\t".$val['isbn'];
            $excel_data[$key]['barcode'] = "\t".$val['barcode'];
            $excel_data[$key]['price'] = $val['price'];
            $excel_data[$key]['press'] = $val['press'];
            $excel_data[$key]['pre_time'] = $val['pre_time'];
            $excel_data[$key]['book_selector'] = $val['book_selector'] == 1 ? '是' : '否';
            $excel_data[$key]['way'] = $val['way'] == 1 ? '线上荐购' : '线下荐购';
            $excel_data[$key]['settle_state'] = $val['settle_state'] == 1 ? "已结算" : ($val['settle_state'] == 2 ? '结算中' : "未结算");
            $excel_data[$key]['pur_manage_name'] = $val['pur_manage_name'];
            $excel_data[$key]['create_time'] = $val['create_time'];
            if ($type == 1) {
                $excel_data[$key]['selttle_sponsor_manage_name'] = $val['settle_sponsor_manage_name'];
                $excel_data[$key]['settle_sponsor_time'] = $val['settle_sponsor_time'];
            }else{
                $excel_data[$key]['settle_affirm_manage_name'] = $val['settle_affirm_manage_name'];
                $excel_data[$key]['settle_affirm_time'] = $val['settle_affirm_time'];
            }
          }
          return $excel_data;
      }

}