<?php

namespace App\Http\Controllers\Export;

use App\Http\Controllers\Controller;
use App\Models\AnswerActivity;
use App\Models\AnswerActivityBaseUnit;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

/**
 * excel导出导入功能 
 */
class AnswerActivityBaseUnitExport extends Controller
{

    public $model = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new AnswerActivityBaseUnit();
    }

    /**
     * 活动单位导出
     * @param nature int 单位性质   1 图书馆  2 文化馆  3 博物馆
     * @param start_time datetime 创建时间范围搜索(开始) 
     * @param end_time datetime 创建时间范围搜索(结束) 
     * @param keywords string 搜索关键词(类型名称)
     **/
    public function getUnitList()
    {
        $start_time = $this->request->start_time;
        $end_time = $this->request->end_time;
        $nature = $this->request->nature;
        $keywords = $this->request->keywords;
        $res = $this->model->lists([], $keywords, $nature, $start_time, $end_time, 999999);

        if (empty($res['data'])) {
            $res['data'] = [];
        }

        $row[0]["id"] = 'ID';
        $row[0]["name"] = '单位名称';
        $row[0]["nature_name"] = '单位性质';
        $row[0]["intro"] = '单位简介';
        $row[0]["words"] = '单位赠言';
        $row[0]["province"] = '省';
        $row[0]["city"] = '市';
        $row[0]["district"] = '区/县';
        $row[0]["address"] = '详细地址';
        //  $row[0]["order"] = '排序';
        $row[0]["create_time"] = '创建时间';


        $title = '基础单位列表导出-' . date('YmdHis');
        //执行导出

        $data = $this->setData($res['data']); //要导入的数据
        return $this->exportData($data, $title, $row);
    }


    /**
     * 导出数据
     *
     * @param [type] $data
     * @return void
     */
    public function exportData($data, $title, $row)
    {
        $header = $row; //导出表头
        $excel = new Export($data, $header, $title);
        $excel->setColumnWidth(['A' => 10, 'B' => 25, 'C' => 30, 'D' => 30, 'E' => 30, 'F' => 20, 'G' => 20, 'H' => 20, 'I' => 20, 'J' => 20, 'K' => 30, 'L' => 20]);
        $excel->setRowHeight([1 => 30]);
        // $excel->setFreezePane('A2');
        // $excel->setFont(['A1:Z1265' => '宋体']);
        // $excel->setFontSize(['A1:I1' => 14,'A2:Z1265' => 10]);
        // $excel->setBold(['A1:Z2' => true]);
        // $excel->setBackground(['A1:A1' => '808080','C1:C1' => '708080']);
        // $excel->setMergeCells(['A1:I1']);
        // $excel->setBorders(['A2:D5' => '#000000']);
        return Excel::download($excel, $title . '.xlsx');
    }


    /**
     * 处理 终端书籍列表 数据
     */
    public function setData($data)
    {
        $excel_data = [];
        /*设置excel内容*/
        foreach ($data as $key => $val) {
            $excel_data[$key]['id'] = $key + 1;
            $excel_data[$key]['name'] = $val['name'];
            $excel_data[$key]['nature_name'] = $this->model->getNatureName($val['nature']);
            $excel_data[$key]['intro'] = strip_tags($val['intro']);
            $excel_data[$key]['words'] = $val['words'];
            $excel_data[$key]["province"] = $val['province'];
            $excel_data[$key]["city"] = $val['city'];
            $excel_data[$key]["district"] = $val['district'];
            $excel_data[$key]["address"] = $val['address'];
            //    $excel_data[$key]['order'] = $val['order'];
            $excel_data[$key]['create_time'] = $val['create_time'];
        }
        return $excel_data;
    }
}
