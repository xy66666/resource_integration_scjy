<?php

namespace App\Http\Controllers;

use TCPDF;

/**
 * 生成PDF文件
 * composer require tecnickcom/tcpdf
 */
class PdfController extends Controller
{
    /**
     * //示例官网：https://tcpdf.org/examples/
     * 文字生成pdf文件  
     * @param $content 二维码内容
     * @param $border_line 是否显示边界线  默认显示
     * @param del_past_file_times 删除过期压缩包时间，单位秒，若传空，则不删除
     */
    public function setPdf($title, $content, $pdf_path, $pdf_name, $del_past_file_times = 60, $border_line = true)
    {

        $file_addr = public_path('uploads'); //压缩包文件夹路径
        if (empty($pdf_path)) {
            $pdf_path =  '/pdf_dir/'; //压缩包路径
        }

        if (file_exists($file_addr . $pdf_path . $pdf_name . '.pdf')) {
            return $pdf_path . $pdf_name . '.pdf'; //如果pdf存在，直接返回
        }
        if (!file_exists($file_addr . $pdf_path)) {
            mkdir($file_addr . $pdf_path, 0777);
        }
        //先删除过期文件,有效期1个小时
        if ($del_past_file_times) del_past_file($file_addr . $pdf_path, $del_past_file_times);

        //  require_once("../app/Extend/tcpdf/tcpdf.php");
        $pdf = new TCPDF();
        // 设置文档信息
        // $pdf->SetCreator('懒人开发网');
        // $pdf->SetAuthor('懒人开发网');
        // $pdf->SetTitle('TCPDF示例');
        // $pdf->SetSubject('TCPDF示例');
        // $pdf->SetKeywords('TCPDF, PDF, PHP');

        // 设置页眉和页脚信息
        $pdf->SetHeaderData('', 0, $title, '', [000, 000, 000], [0, 64, 128]);
        $pdf->setFooterData([0, 64, 0], [0, 64, 128]);

        //实现取消pdf页面顶端和底端的一条横线，不设置默认存在
        if (!$border_line) {
            $pdf->setPrintHeader(false);
            $pdf->setPrintFooter(false);
        }

        // 设置页眉和页脚字体
        $pdf->setHeaderFont(['stsongstdlight', '', '10']);
        $pdf->setFooterFont(['helvetica', '', '8']);

        // 设置默认等宽字体
        $pdf->SetDefaultMonospacedFont('courier');

        // 设置间距
        $pdf->SetMargins(15, 15, 15); //页面间隔
        $pdf->SetHeaderMargin(5); //页眉top间隔
        $pdf->SetFooterMargin(10); //页脚bottom间隔

        // 设置分页
        $pdf->SetAutoPageBreak(true, 25);

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        //设置字体 stsongstdlight支持中文
        $pdf->SetFont('stsongstdlight', '', 14);


        foreach ($content as $key => $val) {
            $pdf->AddPage(); //添加一页
            $pdf->Ln(5); //换行符
            //    $pdf->Image(public_path() . '/uploads/img/2023-11-20/2CS1CzRCyI4E1QEJeERV7PLYuvxeds1jZqnG7RK9.jpg');//图片放在正文内了
            $pdf->writeHTML($val);
        }
        // $pdf->Image(public_path() . '/uploads/img/2023-11-20/2CS1CzRCyI4E1QEJeERV7PLYuvxeds1jZqnG7RK9.jpg');
        //保存PDF
        $pdf->Output($file_addr . $pdf_path . $pdf_name . '.pdf', 'F'); //I输出、D下载 F 保存（但是前面地址就要改为绝对路径，并且路径必须存在）
        return $pdf_path . $pdf_name . '.pdf';
    }


    /**
     * //示例官网：https://tcpdf.org/examples/
     * 文字生成pdf文件  
     * @param $content 二维码内容
     * @param del_past_file_times 删除过期压缩包时间，单位秒，若传空，则不删除
     */
    public function setPdf1($title, $content, $pdf_path, $pdf_name, $del_past_file_times = 60)
    {

        $file_addr = public_path('uploads'); //压缩包文件夹路径
        if (empty($pdf_path)) {
            $pdf_path =  '/pdf_dir/'; //压缩包路径
        }

        if (file_exists($file_addr . $pdf_path . $pdf_name . '.pdf')) {
            return $pdf_path . $pdf_name . '.pdf'; //如果pdf存在，直接返回
        }
        if (!file_exists($file_addr . $pdf_path)) {
            mkdir($file_addr . $pdf_path, 0777);
        }
        //先删除过期文件,有效期1个小时
        if ($del_past_file_times) del_past_file($file_addr . $pdf_path, $del_past_file_times);

        //  require_once("../app/Extend/tcpdf/tcpdf.php");
        $pdf = new TCPDF();
        // 设置文档信息
        // $pdf->SetCreator('懒人开发网');
        // $pdf->SetAuthor('懒人开发网');
        // $pdf->SetTitle('TCPDF示例');
        // $pdf->SetSubject('TCPDF示例');
        // $pdf->SetKeywords('TCPDF, PDF, PHP');

        // 设置页眉和页脚信息
        $pdf->SetHeaderData('', 0, $title, '', [000, 000, 000], [0, 64, 128]);
        $pdf->setFooterData([0, 64, 0], [0, 64, 128]);

        // 设置页眉和页脚字体
        $pdf->setHeaderFont(['stsongstdlight', '', '10']);
        $pdf->setFooterFont(['helvetica', '', '8']);

        // 设置默认等宽字体
        $pdf->SetDefaultMonospacedFont('courier');

        // 设置间距
        $pdf->SetMargins(15, 15, 15); //页面间隔
        $pdf->SetHeaderMargin(5); //页眉top间隔
        $pdf->SetFooterMargin(10); //页脚bottom间隔

        // 设置分页
        $pdf->SetAutoPageBreak(true, 25);

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        //设置字体 stsongstdlight支持中文
        $pdf->SetFont('stsongstdlight', '', 14);


        foreach ($content as $key => $val) {
            $pdf->AddPage(); //添加一页
            $pdf->Ln(5); //换行符
            //    $pdf->Image(public_path() . '/uploads/img/2023-11-20/2CS1CzRCyI4E1QEJeERV7PLYuvxeds1jZqnG7RK9.jpg');//图片放在正文内了
            $pdf->writeHTML($val);
        }
        // $pdf->Image(public_path() . '/uploads/img/2023-11-20/2CS1CzRCyI4E1QEJeERV7PLYuvxeds1jZqnG7RK9.jpg');
        //保存PDF
        $pdf->Output($file_addr . $pdf_path . $pdf_name . '.pdf', 'F'); //I输出、D下载 F 保存（但是前面地址就要改为绝对路径，并且路径必须存在）
        return $pdf_path . $pdf_name . '.pdf';
    }


    /**
     * 文字生成pdf文件  
     * @param $content 二维码内容
     */
    public function _setPdf($content)
    {
        //  require_once("../app/Extend/tcpdf/tcpdf.php");
        $pdf = new TCPDF();
        // 设置文档信息
        $pdf->SetCreator('懒人开发网');
        $pdf->SetAuthor('懒人开发网');
        $pdf->SetTitle('TCPDF示例');
        $pdf->SetSubject('TCPDF示例');
        $pdf->SetKeywords('TCPDF, PDF, PHP');

        // 设置页眉和页脚信息
        $pdf->SetHeaderData('tcpdf_logo.jpg', 30, 'LanRenKaiFA.com', '学会偷懒，并懒出效率！', [0, 64, 255], [0, 64, 128]);
        $pdf->setFooterData([0, 64, 0], [0, 64, 128]);

        // 设置页眉和页脚字体
        $pdf->setHeaderFont(['stsongstdlight', '', '10']);
        $pdf->setFooterFont(['helvetica', '', '8']);

        // 设置默认等宽字体
        $pdf->SetDefaultMonospacedFont('courier');

        // 设置间距
        $pdf->SetMargins(15, 15, 15); //页面间隔
        $pdf->SetHeaderMargin(5); //页眉top间隔
        $pdf->SetFooterMargin(10); //页脚bottom间隔

        // 设置分页
        $pdf->SetAutoPageBreak(true, 25);

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        //设置字体 stsongstdlight支持中文
        $pdf->SetFont('stsongstdlight', '', 14);

        //第一页
        $pdf->AddPage();
        $pdf->writeHTML('<p style="text-align: center"><h1>第一页内容</h1></p>');
        $pdf->writeHTML('<p>我是第一行内容</p>');
        $pdf->writeHTML('<p style="color: red">我是第二行内容</p>');
        $pdf->writeHTML('<p>我是第三行内容</p>');
        $pdf->Ln(5); //换行符
        $pdf->writeHTML('<p><a href="http://www.lanrenkaifa.com/" rel="external nofollow"  title="">懒人开发网</a></p>');

        //第二页
        $pdf->AddPage();
        $pdf->writeHTML('<h1>第二页内容</h1>');

        //输出PDF

        $pdf->Output('t.pdf', 'D'); //I输出、D下载 F 保存（但是前面地址就要改为绝对路径，并且路径必须存在）
    }





    /**
     * 将pdf文件转化为多张png图片
     * @param string $pdf  pdf所在路径 （相对路径）
     * @param string $path 新生成图片所在路径 (/www/pngs/)
     *
     * @return array|bool  返回多个文件名
     */
    public function pdf2png($pdf, $pdf_img = '/pdf_img/')
    {
        $path = public_path('uploads');

        if (!extension_loaded('imagick')) {
            return false;
        }
        $pdf_address = $path . '/' . $pdf;
        if (!file_exists($pdf_address)) {
            return false;
        }
        $im = new \Imagick();
        $im->setResolution(120, 120); //设置分辨率 值越大分辨率越高
        $im->setCompressionQuality(100);
        $im->readImage($pdf_address);
        foreach ($im as $k => $v) {
            $v->setImageFormat('png');
            $file_name = uniqid() . '.png';

            create_dirs($path . $pdf_img); //创建目录

            if ($v->writeImage($path . $pdf_img . $file_name) == true) {
                $return[] = substr($pdf_img . $file_name, 1);
            }
        }
        return $return;
    }

    /**
     * 将pdf转化为单一png图片
     * @param string $pdf  pdf所在路径 （/www/pdf/abc.pdf pdf所在的绝对路径）
     * @param string $path 新生成图片所在路径 (/www/pngs/)
     *
     * @throws Exception  返回单个文件名
     */
    public function pdf2png2($pdf, $pdf_img = '/pdf_img/')
    {
        $path = public_path('uploads');
        $pdf_address = $path . $pdf;
        try {
            $im = new \Imagick();
            $im->setCompressionQuality(100);
            $im->setResolution(120, 120); //设置分辨率 值越大分辨率越高
            $im->readImage($pdf_address);

            $canvas = new \Imagick();
            $imgNum = $im->getNumberImages();
            //$canvas->setResolution(120, 120);
            foreach ($im as $k => $sub) {
                $sub->setImageFormat('png');
                //$sub->setResolution(120, 120);
                $sub->stripImage();
                $sub->trimImage(0);
                $width  = $sub->getImageWidth() + 20;
                $height = $sub->getImageHeight() + 20;
                if ($k + 1 == $imgNum) {
                    $height += 10;
                } //最后添加10的height
                $canvas->newImage($width, $height, new \ImagickPixel('white'));
                $canvas->compositeImage($sub, \Imagick::COMPOSITE_DEFAULT, 10, 10);
            }

            $file_name = uniqid() . '.png';
            $canvas->resetIterator();
            $canvas->appendImages(true)->writeImage($path . $pdf_img . $file_name);
            return substr($pdf_img . $file_name, 1);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
