<?php

namespace App\Http\Controllers\Port;

use App\Models\UserProtocol;

/**
 * 用户协议
 */
class UserProtocolController extends CommonController
{
    /**
     * 获取用户协议
     * @param type  1 用户协议    2  大赛原创声明  3 景点打卡  4 在线办证  5、扫码借借阅规则
     */
    public function getUserProtocol(){
        $type = request()->input('type' , 1);
        $res = UserProtocol::select('content')->where('type' , $type)->first();
        $res['content'] = $res ? $res['content']  : '';
        return $this->returnApi(200 , '' , true , $res);
    }

}
