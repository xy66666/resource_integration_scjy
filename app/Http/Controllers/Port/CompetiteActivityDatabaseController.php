<?php

namespace App\Http\Controllers\Port;


use App\Models\CompetiteActivityDatabase;
use App\Models\CompetiteActivityEbook;
use App\Models\CompetiteActivityWorksDatabase;
use App\Models\CompetiteActivityWorksEbook;
use App\Validate\CompetiteActivityDatabaseValidate;


/**
 * 线上大赛活动数据库
 */
class CompetiteActivityDatabaseController extends CommonController
{
    public $model = null;
    public $validate = null;
    public $worksDatabaseModel = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new CompetiteActivityDatabase();
        $this->validate = new CompetiteActivityDatabaseValidate();
        $this->worksDatabaseModel = new CompetiteActivityWorksDatabase();
    }

    /**
     * 列表
     * @param page int 当前页
     * @param limit int 分页大小
     * @param keywords string 搜索关键词(作品数据库名称)
     */
    public function lists()
    {
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;
        $res = $this->model->lists(['id', 'name', 'img', 'browse_num'], $keywords, 1, null, null, $limit);

        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }
        $res = $this->disPageData($res);
        return $this->returnApi(200, "查询成功", "YES", $res);
    }

    /**
     * 详情
     * @param database_id int 数据库id
     */
    public function detail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('wx_detail')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $res = $this->model->detail($this->request->database_id, ['id', 'name', 'img', 'intro', 'create_time', 'browse_num']);

        if (empty($res)) {
            return $this->returnApi(203, "暂无数据");
        }
        //添加浏览量
        $res->browse_num = ++$res->browse_num;
        $res->save();

        return $this->returnApi(200, "获取成功", true, $res->toArray());
    }


    /**
     * 数据库作品列表
     * @param database_id int 数据库id
     * @param page int 当前页
     * @param limit int 分页大小
     * @param keywords string 搜索关键词(数据库作品名称)
     * @param type datetime 类型  1 作品  2 电子书
     */
    public function worksList()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('wx_works_database_list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $database_id = $this->request->database_id;
        $type = $this->request->type;
        $page = $this->request->page ? intval($this->request->page) : 1;
        $limit = $this->request->limit ? intval($this->request->limit) : 10;
        $keywords = $this->request->keywords;
        $database_info = $this->model->where('id', $database_id)->where('is_del', 1)->where('is_play', 1)->first();
        if (empty($database_info)) {
            return $this->returnApi(203, "当前数据库信息不存在");
        }

        $res = $this->worksDatabaseModel->lists($database_id, $keywords, $type, null, null, $limit);

        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }
        $res = $this->disPageData($res);
        return $this->returnApi(200, "查询成功", "YES", $res);
    }


    /**
     * 电子书详情+电子书作品列表
     * @param ebook_id int 电子书id
     */
    public function ebookDetail()
    {
        //增加验证场景进行验证
        if (!$this->validate->scene('wx_works_ebook_detail_list')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }

        $ebook_id = $this->request->ebook_id;
        $competiteActivityEbookModel = new CompetiteActivityEbook();
        //添加数据库浏览量
        $ebook_info = $competiteActivityEbookModel->select('id', 'name', 'img', 'intro', 'author', 'browse_num')
            ->where('id', $ebook_id)->where('is_del', 1)->where('is_play', 1)->first();
        if (empty($ebook_info)) {
            return $this->returnApi(203, "当前电子书信息不存在");
        }
        //添加浏览量
        $ebook_info->browse_num = ++$ebook_info->browse_num;
        $ebook_info->save();

        $ebook_info = $ebook_info->toArray();

        $competiteActivityWorksEbookModel = new CompetiteActivityWorksEbook();
        $res = $competiteActivityWorksEbookModel->lists($ebook_id, null, null, null, 999);

        // if (empty($res['data'])) {
        //     return $this->returnApi(203, "暂无数据");
        // }
        $ebook_info['work_list'] = $res['data'];

        //  $res = $this->disPageData($res);
        return $this->returnApi(200, "查询成功", "YES", $ebook_info);
    }
}
