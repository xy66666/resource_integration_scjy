<?php

namespace App\Http\Controllers\Port;

use App\Http\Controllers\Controller;
use App\Models\SystemInfo;
use Illuminate\Support\Facades\DB;

/**
 * 系统消息
 */
class SystemInfoController extends Controller
{
    // 自动验证
    public $model = null;

    //审批列表
    public $type1 = ['5', '17', '20', '25', '32', '35', '39', '40', '41', '42', '52', '56', '58', '59', '60'];
    //支付
    public $type2 = ['22', '26', '27', '28', '44', '45'];
    //系统消息
    public $type3 = [
        '1', '2', '3', '4', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '18', '19', '21', '23', '24', '29', '30', '31',
        '33', '34', '36', '37', '38', '43', '46', '47', '48', '49', '50', '51', '53', '54', '55', '57', '61', '62', '63', '64', '65', '66', '67'
    ];

    public function __construct()
    {
        parent::__construct();

        $this->model = new SystemInfo();
    }

    /**
     * 用户消息列表
     */
    public function systemList()
    {
        $user_id = $this->request->user_info['id'];
        $condition[] = ['user_id', '=', $user_id];

        //获取消息第一条
        $examine = $this->model->select('title', 'create_time')->where($condition)->whereIn('type', $this->type1)->orderByDesc('create_time')->where('is_del', 1)->first(); //审批列表
        $examine_num = $this->model->where($condition)->whereIn('type', $this->type1)->where('is_look', 2)->where('is_del', 1)->orderByDesc('create_time')->count(); //审批列表
        $pay = $this->model->select('title', 'create_time')->where($condition)->whereIn('type', $this->type2)->where('is_del', 1)->orderByDesc('create_time')->first(); //支付
        $pay_num = $this->model->where($condition)->whereIn('type', $this->type2)->where('is_look', 2)->where('is_del', 1)->orderByDesc('create_time')->count(); //支付
        $system = $this->model->select('title', 'create_time')->where($condition)->whereIn('type', $this->type3)->where('is_del', 1)->orderByDesc('create_time')->first(); //系统消息
        $system_num = $this->model->where($condition)->whereIn('type', $this->type3)->where('is_look', 2)->where('is_del', 1)->orderByDesc('create_time')->count(); //系统消息

        return $this->returnApi(200, "查询成功", true, [
            'examine' => $examine,
            'examine_num' => $examine_num,
            'pay' => $pay,
            'pay_num' => $pay_num,
            'system' => $system,
            'system_num' => $system_num,
        ]);
    }

    /**
     * 获取用户分类消息列表
     * @param page 页数 默认为1
     * @param limit 限制条数 默认20
     * @param type 类型  1、用户审批信息列表  2、用户支付信息列表  3、用户系统消息
     */
    public function systemMessage()
    {
        $user_id = $this->request->user_info['id'];
        $type = $this->request->type;
        if (empty($type)) {
            return $this->returnApi(201, '参数错误');
        }
        if ($type == 1) {
            $type = $this->type1;
        } elseif ($type == 2) {
            $type = $this->type2;
        } else {
            $type = $this->type3;
        }

        $page = $this->request->input('page', 1);
        $limit = $this->request->input('limit', 10);

        $res = $this->model->select('id', 'title', 'user_id', 'account_id', 'type', 'con_id', 'intro', 'is_look', 'create_time')
            ->where(function ($query) use ($type, $user_id) {
                if ($type) {
                    $query->whereIn('type', $type);
                }
                if ($user_id) {
                    $query->where('user_id', $user_id);
                }
            })->where('is_del', 1)
            ->orderByDesc('create_time')
            ->paginate($limit)
            ->toArray();

        if (empty($res['data'])) {
            return $this->returnApi(203, "暂无数据");
        }

        $id = array_column($res['data'], 'id');
        $this->model->whereIn('id', $id)->where('is_look', 2)->update(['is_look' => 1, 'change_time' => date('Y-m-d H:i:s')]);

        $res = $this->disPageData($res);
        return $this->returnApi(200, "查询成功", true, $res);
    }
}
