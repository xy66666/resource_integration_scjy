<?php

namespace App\Http\Controllers\Port;

use App\Models\AccessNum;
use App\Models\AppViewGray;
use App\Models\BranchInfo;



/**
 * 本馆简介
 */
class BranchInfoController extends CommonController
{

    public $model = null;
    public function __construct()
    {
        parent::__construct();

        $this->model = new BranchInfo();
    }




    /**
     * 场馆简介
     * @param id int id   固定传  1 或为空
     */
    public function detail()
    {
        //增加验证场景进行验证
        // if (!$this->validate->scene('detail')->check($this->request->all())) {
        //     return $this->returnApi(201,  $this->validate->getError());
        // }

        $id = $this->request->input('id', 1);
        $id = $id ? $id : 1;

        $res = $this->model->select([
            'id'/* , 'intro', 'img', 'dispark_time', 'transport_line' */, 'province', 'city', 'district',
            'address', 'tel', 'contacts', 'lon', 'lat'
        ])->find($id);

        if (!$res) {
            return $this->returnApi(201, "参数传递错误");
        }

        $res = $res->toArray();

        $accessNumObj = new AccessNum();
        //获取浏览量
        $res['now_day'] =  $accessNumObj->where('yyy', date('Y'))->where('mmm', date('m'))->where('ddd', date('d'))->value('web_num');
        $res['now_day'] = $res['now_day'] ? $res['now_day'] : 0;
        $res['month_day'] =  $accessNumObj->where('yyy', date('Y'))->where('mmm', date('m'))->sum('web_num');
        $res['total_num'] =  $accessNumObj->sum('web_num');

        $appViewGrayModel = new AppViewGray();
        $res['color']['is_gray'] = $appViewGrayModel->isViewGray(); //是否显示灰色

        $res['qr_code'] = 'default/qr_img.png';
      //  $res['friendly_link'] = [['name' => '渝中区图书馆', 'link' => 'https://www.yzqlib.cn/yzqlib/index']];
        $res['friendly_link'] = [];
        return $this->returnApi(200, "查询成功", true, $res);
    }
}
