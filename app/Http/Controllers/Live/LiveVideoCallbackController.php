<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/6/19
 * Time: 13:17
 */

namespace App\Http\Controllers\Live;

use App\Models\VideoLive;
use App\Models\VideoLiveResource;
use Illuminate\Support\Facades\Log;
use think\Db;

//视频回调地址 https://demo.usharego.com/resource_integration/public/live/liveVideoCallback/getVideo
/**
 * 获取网易云视频直播回调地址，然后把视频存储在我们服务器
 * Class VideoCallback
 * @package app\live\controller
 */
class LiveVideoCallbackController
{
    //处理回调视频   此接口必须是  post 方式 ，不支持get
    public function getVideo()
    {
        $callbcak_video_info_original = file_get_contents("php://input"); //获取视频回调信息

        Log::error('收到视频回调：');
        Log::error($callbcak_video_info_original);
        // $callbcak_video_info_original = '{"orig_video_key":"da327d124de74642bcdd0d24b7474a8d_1693560014572_1693560314572_6563178737-00003.mp4","vid":"8554245817","uid":"102325023","video_name":"测试直播视频回调-20230901164039_20230901-172014_20230901-172514","recordFileFlag":"0","nId":"nId7395113614","beginTime":"1693560014572","endTime":"1693560314572","type":"live_record","origUrl":"http://jdvoduidkekrr.vod.126.net/jdvoduidkekrr/da327d124de74642bcdd0d24b7474a8d_1693560014572_1693560314572_6563178737-00003.mp4","cid":"da327d124de74642bcdd0d24b7474a8d"}';
        $callbcak_video_info = json_decode($callbcak_video_info_original, true);
        $cid = $callbcak_video_info['cid'];

        //获取是否是直播的cid
        $videoLiveModel = new VideoLive();
        $video_live_info = $videoLiveModel->where('cid', $cid)->where('is_del', 1)->first();

        if (empty($video_live_info)) return false;
        //开始处理视频
        $start_time = $callbcak_video_info['beginTime'];
        $end_time = $callbcak_video_info['endTime'];
        $link = $callbcak_video_info['origUrl'];
        $video_name = $callbcak_video_info['video_name'];
        $orig_video_key = explode(".", $callbcak_video_info['orig_video_key']);
        $format = end($orig_video_key); //视频格式
        $duration_time = $end_time - $start_time; //分段视频时长(毫秒)
        $total_time = $video_live_info['total_time'] + $duration_time; //总视频时长(毫秒)

        $video_size = get_resource_size($link); //获取视频大小
        $total_size =  $video_live_info['size'] + $video_size; //总视频大小

        //判断是否重复回调
        $videoLiveResourceModel = new VideoLiveResource();
        $link_res = $videoLiveResourceModel->where('link', $link)->first();
        if (!empty($link_res)) {
            return false; //不继续让下执行
        }

        //修改直播回看与当前直播表
        $video_live_info->size = $total_size;
        $video_live_info->total_time = $total_time;
        $video_live_info->save();

        //获取视频地址
        $video_address = 'video_live/' . date('Y-m-d') . '/' . $callbcak_video_info['orig_video_key'];
        //添加直播视频分段链接
        $videoLiveResourceModel->add([
            'name' => $video_name,
            'act_id' => $video_live_info['id'],
            'video_address' => $video_address,
            'link' => $link,
            'format' => $format,
            'size' => $video_size,
            'duration_time' => $duration_time,
            'start_time' => $start_time,
            'end_time' => $end_time,
            'callbcak_video_info' => $callbcak_video_info_original,
        ]);
        //最后处理视频
        $this->getVideoReserve($callbcak_video_info['orig_video_key'], $link);
    }

    /**
     * 频道变化数据处理
     * @retrun status 频道状态（0：空闲； 1：直播； 2：禁用； 3：直播录制）
     */
    public  function channelStatus()
    {
        $callbcak_video_status_change = file_get_contents("php://input"); //获取视频回调信息

        Log::error('收到频道视频状态变化回调：');
        Log::error($callbcak_video_status_change);
       // $callbcak_video_status_change = '{"nId":"nId7396527361","time":"1693795815421","type":"channel_status","status":"0","cid":"982045d5c2544d52b3e08560f1db18ec"}';

        $callbcak_video_status = json_decode($callbcak_video_status_change, true);
        $cid = $callbcak_video_status['cid'];
        $wy_status = $callbcak_video_status['status'];
        $videoLiveModel = new VideoLive();
        $res = $videoLiveModel->where('cid', $cid)->first();
        if (empty($res) || $res->wy_status == $wy_status) {
            return false;
        }
        if($wy_status == 2){
            $res->status = 3;
        }elseif($wy_status == 1 || $wy_status == 3){
            $res->status = 2;
        }
        $res->wy_status = $wy_status;
        $res->save();
        return true;
    }

    /**
     * 获取远程视频到本地服务器的指定目录
     * 增对大文件
     * @param  $name 视频名称
     * @param  $url 视频链接
     */
    public function getVideoReserve($name, $url)
    {
        set_time_limit(60); // 脚本执行没有时间限
        // 设置浏览器下载的文件名，这里还以原文件名一样
        //$filename = basename($url);
        $video_address = public_path('uploads') . '/video_live/' . date('Y-m-d'); //文件最后保存位置的文件夹
        //判断文件夹是否存在，不存在则递归创建
        if (!is_dir($video_address)) {
            mkdir($video_address, 0777, true);
        }
        $filename = $video_address . '/' . $name;

        // 获取远程文件大小
        // 注意filesize()无法获取远程文件大小
        $headers = get_headers($url, 1);
        $fileSize = $headers['Content-Length']; //获取文件大小

        $read_buffer = 4096;
        $handle = fopen($url, 'rb');
        //总的缓冲的字节数（也是总读取数）
        $sum_buffer = 0;

        //先判断本地是否有此文件
        if (file_exists($filename)) {
            @unlink($filename);
        }

        //只要没到文件尾，就一直读取
        while (!feof($handle) && $sum_buffer < $fileSize) {
            $a =  fread($handle, $read_buffer); //读取文件内容
            file_put_contents($filename, $a, FILE_APPEND); //将获取到的内容追加在文件中
            $sum_buffer += $read_buffer; //读取的总数量
        }
        fclose($handle);
        echo "下载成功";
    }


    /**
     * 获取远程视频到本地服务器的指定目录（增对一次性直接下载完毕）
     */
    public function getVideoAddr()
    {
        echo date('Y-m-d H:i:s');
        set_time_limit(0); // 脚本执行没有时间限
        ini_set("memory_limit", "-1"); //不限制内存
        //http://vfx.mtime.cn/Video/2019/02/04/mp4/190204084208765161.mp4
        $buffer = file_get_contents('http://jdvodoyucpnxi.vod.126.net/jdvodoyucpnxi/cf7ba6f233504b1e9418687f06540d2e_1560994776603_1560996058821_2150082205-00000.mp4');
        file_put_contents(__DIR__ . '/test1.mp4', $buffer);
        echo date('Y-m-d H:i:s');
    }
    /**
     * 获取远程视频到本地服务器的指定目录（增对一次性直接下载完毕）
     */
    public function getVideoAddr3()
    {
        set_time_limit(90); // 脚本执行时间限制90秒
        ini_set("memory_limit", "300M"); //不限制内存
        $remoteFileUrl = "http://jdvodoyucpnxi.vod.126.net/jdvodoyucpnxi/cf7ba6f233504b1e9418687f06540d2e_1560994776603_1560996058821_2150082205-00000.mp4";
        // $remoteFileUrl = "http://vfx.mtime.cn/Video/2019/02/04/mp4/190204084208765161.mp4";
        $filename = __DIR__ . '/live_' . uniqid() . '.mp4';
        if (!empty($remoteFileUrl)) {
            $headers = get_headers($remoteFileUrl);
            if ($headers[0] == 'HTTP/1.1 200 OK') {
                $ch = curl_init();
                $timeout = 10;
                curl_setopt($ch, CURLOPT_URL, $remoteFileUrl);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                $fileDta = curl_exec($ch);
                //检查是否有错误发生
                $error = curl_errno($ch);
                curl_close($ch);

                if ($error) {
                    var_dump($error);
                } else {
                    if (empty($fileDta)) {
                        echo "没有内容！";
                    }
                    $result = file_put_contents($filename, $fileDta);
                    var_dump($result);
                    echo "下载成功！！！！";
                }
            } else {
                echo 1321231;
                die;
            }
        }
    }

    /**
     * 把远程视频下载到本地，一次性下载
     */
    public function getVideoAddr4()
    {
        // 远程文件链接
        $url = 'http://vfx.mtime.cn/Video/2019/02/04/mp4/190204084208765161.mp4';
        // 设置浏览器下载的文件名，这里还以原文件名一样
        $filename = basename($url);
        // 获取远程文件大小
        // 注意filesize()无法获取远程文件大小
        $headers = get_headers($url, 1);
        $fileSize = $headers['Content-Length'];
        // 设置header头
        // 因为不知道文件是什么类型的，告诉浏览器输出的是字节流
        header('Content-Type: application/octet-stream');
        // 告诉浏览器返回的文件大小类型是字节
        header('Accept-Ranges:bytes');
        // 告诉浏览器返回的文件大小
        header('Content-Length: ' . $fileSize);
        // 告诉浏览器文件作为附件处理并且设定最终下载完成的文件名称
        header('Content-Disposition: attachment; filename="' . $filename . '"');

        readfile($url);
        exit;
    }

    /**
     *  把远程视频下载到本地   (测试只下载了一半的视频)
     * 增对大文件
     */
    public function getVideoAddr5()
    {
        set_time_limit(60); // 脚本执行没有时间限
        // 远程文件链接
        // $url = 'http://jdvodoyucpnxi.vod.126.net/jdvodoyucpnxi/cf7ba6f233504b1e9418687f06540d2e_1560994776603_1560996058821_2150082205-00000.mp4';
        $url = 'http://jdvodoyucpnxi.vod.126.net/jdvodoyucpnxi/889b18de62634902be3b0ceb71ab0d53_1560929683593_1560933495787_2148105954-00000.mp4';
        //  $url = 'http://jdvodoyucpnxi.vod.126.net/jdvodoyucpnxi/e54a14addc064b4b8045da3ade892798_1561001848913_1561002033373_2150381168-00000.mp4';
        // 设置浏览器下载的文件名，这里还以原文件名一样
        $filename = basename($url);
        //  $filename = __DIR__.'/live_' . uniqid() . '.mp4';
        // 获取远程文件大小
        // 注意filesize()无法获取远程文件大小
        $headers = get_headers($url, 1);
        $fileSize = $headers['Content-Length'];
        // 设置header头
        // 因为不知道文件是什么类型的，告诉浏览器输出的是字节流
        header('Content-Type: application/octet-stream');
        // 告诉浏览器返回的文件大小类型是字节
        header('Accept-Ranges:bytes');
        // 告诉浏览器返回的文件大小
        header('Content-Length: ' . $fileSize);
        // 告诉浏览器文件作为附件处理并且设定最终下载完成的文件名称
        header('Content-Disposition: attachment; filename="' . $filename . '"');

        //针对大文件，规定每次读取文件的字节数为4096字节，直接输出数据
        $read_buffer = 4096;
        $handle = fopen($url, 'rb');
        //总的缓冲的字节数（也是总读取数）
        $sum_buffer = 0;
        //只要没到文件尾，就一直读取
        while (!feof($handle) && $sum_buffer < $fileSize) {
            echo fread($handle, $read_buffer); //读取文件内容
            $sum_buffer += $read_buffer; //读取的总数量
        }
        echo "下载成功";
    }
}
