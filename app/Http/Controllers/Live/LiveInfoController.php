<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2019/6/18
 * Time: 14:32
 */

namespace app\live\controller;

use App\Http\Controllers\Live\ServerAPI;

/**
 * 网易云直播服务，原生调用 （未修改成本项目框架使用）
 */
class LiveInfoController
{
    private $AppKey = '';
    private $AppSecret = '';
    private $WyObj; //网易接口对象
    private $BaseObj; //返回数据的对象
    private $type = 0; //频道类型   所有频道类型均使用  rtmp(0)


    //开始前需检查 是否设置   视频录制回调地址   不然视频回看不会自动下载到本地
    public function __construct()
    {
        $this->WyObj = new ServerAPI($this->AppKey, $this->AppSecret);
        $this->BaseObj = new \app\port\controller\Base();
    }

    /**
     * 创建直播频道
     * @return bool|false|string
     * @param  name  直播频道名称
     */
    public function createChannel()
    {
        $name = input('name');
        if (empty($name)) return $this->BaseObj->returnApi(201, '参数错误');
        $result = $this->WyObj->channelCreate($name, $this->type);
        if ($result['code'] === 200) {
            $data = $result['ret'];
            // $data['requestId'] = $result['requestId'];
            return $this->BaseObj->returnApi(200, '创建成功', 'YES', $data);
        } else {
            return $this->BaseObj->returnApi(202, $result['msg']);
        }
    }
    /**
     * 修改直播频道信息
     * @param name 直播频道名称
     * @param cid 直播频道ID，32位字符串
     */
    public function modifyChannel()
    {
        $name = input('name');
        $cid = input('cid');
        if (empty($name) || empty($cid)) return $this->BaseObj->returnApi(201, '参数错误');
        $result = $this->WyObj->channelUpdate($name, $cid, $this->type);
        if ($result['code'] === 200) {
            return $this->BaseObj->returnApi(200, '修改成功', 'YES');
        }
        return $this->BaseObj->returnApi(202, '修改失败');
    }
    /**
     * 删除直播频道信息
     * @param cid 直播频道ID，32位字符串
     */
    public function delChannel()
    {
        $cid = input('cid');
        if (empty($cid)) return $this->BaseObj->returnApi(201, '参数错误');
        $result = $this->WyObj->channelDelete($cid);
        if ($result['code'] === 200) {
            return $this->BaseObj->returnApi(200, '删除成功', 'YES');
        }
        return $this->BaseObj->returnApi(202, $result['msg']);
    }

    /**
     * 禁止直播频道信息
     * @param cid 直播频道ID，32位字符串
     */
    public function forbidChannel()
    {
        $cid = input('cid');
        if (empty($cid)) return $this->BaseObj->returnApi(201, '参数错误');
        $result = $this->WyObj->channelForbid($cid);
        if ($result['code'] === 200) {
            return $this->BaseObj->returnApi(200, '禁用成功', 'YES');
        }
        return $this->BaseObj->returnApi(202, $result['msg']);
    }

    /**
     * 获取用户直播频道列表
     * @param  $records       [单页记录数，默认值为10]
     * @param  $page = 1       [要取第几页，默认值为1]
     */
    public function getChannelList()
    {
        $records = intval(input('records', 10));
        $page = intval(input('page', 1));

        $result = $this->WyObj->channelList($records, $page);
        //$result['ret']['totalRecords'] 为0，表示没有数据
        if ($result['code'] === 200 && !empty($result['ret']['totalRecords'])) {
            $data = $result['ret'];
            return $this->BaseObj->returnApi(200, '获取成功', 'YES', $data);
        }
        return $this->BaseObj->returnApi(202, '暂无数据');
    }
    /**
     * 获取一个直播频道的信息
     * @param  cid       [频道ID，32位字符串]
     */
    public function channelInfo()
    {
        $cid = input('cid');
        if (empty($cid)) return $this->BaseObj->returnApi(201, '参数错误');
        $result = $this->WyObj->channelStats($cid);
        if ($result['code'] === 200) {
            $data = $result['ret'];
            return $this->BaseObj->returnApi(200, '获取成功', 'YES', $data);
        }
        return $this->BaseObj->returnApi(202, $result['msg']);
    }

    /**
     * 重新获取推流地址
     * @param  cid       [频道ID，32位字符串]
     */
    public function channelRefreshAddr()
    {
        $cid = input('cid');
        if (empty($cid)) return $this->BaseObj->returnApi(201, '参数错误');
        $result = $this->WyObj->channelRefreshAddr($cid);
        if ($result['code'] === 200) {
            $data = $result['ret'];
            return $this->BaseObj->returnApi(200, '获取成功', 'YES', $data);
        }
        return $this->BaseObj->returnApi(202, $result['msg']);
    }


    /**
     *  设置频道为录制状态(设置录制信息)
     *  设置直播录制信息。直播录制信息包括录制文件的格式、切片时长、文件名等录制配置，
     *      以及频道的自动录制标志位。直播录制信息的变更在下次推流生效。
     *  注意：由于录制信息的变更不会在当前推流录制中启用，建议针对使用自动录制功能的频道，
     *      在创建完毕后即进行设置，不建议频繁调用
     * @param cid [频道ID，32位字符串]
     * @param needRecord [1-自动录制； 0-不录制]
     * @param format [0-mp4, 1-flv, 2-mp3]
     * @param duration [录制切片时长(分钟)，5~120分钟]
     * @param filename [录制后文件名（只支持中文、字母和数字），格式为filename_YYYYMMDD-HHmmssYYYYMMDD-HHmmss,
     */
    public function channelTranscribeState()
    {
        $cid        = input('cid');
        $needRecord = input('needRecord');
        $format     = input('format', 0);
        $duration   = input('duration', 20);
        if (empty($cid) || $needRecord === '' || $needRecord === null) return $this->BaseObj->returnApi(201, '参数错误');
        $result = $this->WyObj->setChannelTranscribeInfo($cid, $needRecord, $format, $duration);
        if ($result['code'] === 200) {
            return $this->BaseObj->returnApi(200, '设置成功', 'YES');
        }
        return $this->BaseObj->returnApi(202, $result['msg']);
    }
    /**
     * 获取录制视频文件列表
     * @param  $cid  [频道ID，32位字符串]
     * @param  $records  [单页记录数，默认值为10]
     * @param  $page  [要取第几页，默认值为1]
     */
    public function getChannelTranscribeList()
    {
        $cid        = input('cid');
        $records    = intval(input('$records'), 10);
        $page       = intval(input('$page', 1));
        if (empty($cid)) return $this->BaseObj->returnApi(201, '参数错误');
        $result = $this->WyObj->channelTranscribeList($cid, $records, $page);
        if ($result['code'] === 200 && !empty($result['ret']['totalRecords'])) {
            $data = $result['ret'];
            return $this->BaseObj->returnApi(200, '获取成功', 'YES', $data);
        }
        return $this->BaseObj->returnApi(202, '暂无数据');
    }
    /**
     * 获取某一时间范围的录制视频文件列表(通过开始和结束的时间点，获取某频道录制视频文件列表。(时间跨度不能超过1周))
     * @param  $cid  [频道ID，32位字符串]
     * @param  $beginTime  [查询的起始时间戳(毫秒)]
     * @param  $endTime  [查询的结束时间戳(毫秒)]
     */
    public function channelTranscribeListByTime()
    {
        $cid        = input('cid');
        $beginTime  = input('beginTime');
        $endTime    = input('endTime');
        if (empty($cid) || empty($beginTime) || empty($endTime)) return $this->BaseObj->returnApi(201, '参数错误');
        $beginTime = strtotime($beginTime);
        $endTime = strtotime($endTime);

        $result = $this->WyObj->channelTranscribeListByTime($cid, $beginTime * 1000, $endTime * 1000);
        if ($result['code'] === 200 && !empty($result['ret']['totalRecords'])) {
            $data = $result['ret'];
            return $this->BaseObj->returnApi(200, '获取成功', 'YES', $data);
        }
        return $this->BaseObj->returnApi(202, '暂无数据');
    }
    /**
     * 设置视频录制回调地址
     * @param  $recordClk  [录制文件生成回调地址(http开头或https开头)]
     * http://www.cgqwhg.com/public/live/getVideo
     */
    public function setChannelTranscribeAddr()
    {
        $recordClk        = input('recordClk');
        if (empty($recordClk)) return $this->BaseObj->returnApi(201, '参数错误');

        $result = $this->WyObj->setChannelTranscribeAddr($recordClk);
        if ($result['code'] === 200) {
            return $this->BaseObj->returnApi(200, '设置成功', 'YES');
        }
        return $this->BaseObj->returnApi(202, '设置失败');
    }

    /**
     * 设置频道状态变化回调地址
     * @param  $recordClk  [录制文件生成回调地址(http开头或https开头)]
     */
    public function setChannelTranscribeAddrByState()
    {
        $recordClk        = input('recordClk');
        if (empty($recordClk)) return $this->BaseObj->returnApi(201, '参数错误');

        $result = $this->WyObj->setChannelTranscribeAddrByState($recordClk);
        if ($result['code'] === 200) {
            return $this->BaseObj->returnApi(200, '设置成功', 'YES');
        }
        return $this->BaseObj->returnApi(202, '设置失败');
    }
    /**
     * 录制重置（在直播录制过程中，结束正在进行的录制，开启一个新的录制任务。可用来对录制进行主动分片）
     * @param  cid  [频道ID，32位字符串]
     */
    public function channelTranscribeRestart()
    {
        $cid        = input('cid');
        if (empty($cid)) return $this->BaseObj->returnApi(201, '参数错误');

        $result = $this->WyObj->channelTranscribeRestart($cid);
        if ($result['code'] === 200) {
            return $this->BaseObj->returnApi(200, '重置成功', 'YES');
        }
        return $this->BaseObj->returnApi(202, $result['msg']);
    }

    /**
     * 视频录制回调地址查询
     */
    public function getchannelTranscribeAddr()
    {
        $result = $this->WyObj->getchannelTranscribeAddr();
        if ($result['code'] === 200 && !empty($result['ret'])) {
            $data['addr'] = $result['ret'];
            return $this->BaseObj->returnApi(200, '获取成功', 'YES', $data);
        }
        return $this->BaseObj->returnApi(202, '获取失败或暂无回调地址');
    }
    /**
     * 设置录制视频存活时间(用户可设置录制视频的存活时间，过期后视频将被自动删除)
     * @param cid [频道ID，32位字符串]  (选填)
     * @param lifespan [录制视频存活时间（单位：秒），范围：[1天-1年]
     *                   如果cid为NULL，则是用户级别的存活时间
     *                  如果cid不为NULL，则是频道级别的存活时间]   默认 1年
     */
    public function setChannelTranscribeTime()
    {
        $cid            = input('cid');
        $lifespan       = input('lifespan', 31536000);
        if (empty($cid) || empty($lifespan)) return $this->BaseObj->returnApi(201, '参数错误');

        $result = $this->WyObj->setChannelTranscribeTime($cid, $lifespan);

        if ($result['code'] === 200) {
            return $this->BaseObj->returnApi(200, '设置成功', 'YES');
        }
        return $this->BaseObj->returnApi(202, $result['msg']);
    }

    /**
     * 查询录制视频存活时间
     * @param records [单页记录数，不超过200，默认值为10]
     * @param page [要取第几页，默认值为1]
     */
    public function getChannelTranscribeTime()
    {
        $records           = intval(input('records', 10));
        $page              = intval(input('page', 1));

        $result = $this->WyObj->getChannelTranscribeTime($records, $page);
        if ($result['code'] === 200 && !empty($result['ret']['channelLevel']['totalRecords'])) {
            $data = $result['ret'];
            return $this->BaseObj->returnApi(200, '获取成功', 'YES', $data);
        }
        return $this->BaseObj->returnApi(202, '暂无数据');
    }

    /**
     * 删除回调地址
     * @param type [回调地址类型：1:录制回调  2:频道状态变化回调]
     */
    public function delChannelTranscribeAddr()
    {
        $type           = input('type', 10);
        if (empty($type)) return $this->BaseObj->returnApi(201, '参数错误');
        $result = $this->WyObj->delChannelTranscribeAddr($type);
        if ($result['code'] === 200) {
            return $this->BaseObj->returnApi(200, '删除成功', 'YES');
        }
        return $this->BaseObj->returnApi(202, '删除失败');
    }
}
