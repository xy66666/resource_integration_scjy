<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * 小程序支付
 * @author admin
 *
 */

class SmallPay
{
    protected $appid = '';       //小程序id
    protected $mch_id = '';    //商户id
    protected $key = '';          //商户key
    private   $appSecret = ''; //小程序后台appSecret


    public function __construct()
    {

        //获取支付配置文件参数
        $config = config('smallpay');
        $this->appid      = $config['appid'];
        $this->mch_id     = $config['mch_id'];
        $this->key        = $config['key'];
        $this->appSecret  = $config['appSecret'];
    }


    public function pay()
    {
        //统一下单接口
        $return = $this->weixinapp();

        return $return;
    }


    //统一下单接口
    private function unifiedorder()
    {
        $url = 'https://api.mch.weixin.qq.com/pay/unifiedorder';

        $openId = $this->getOpendId();


        // dump($openId);
        $body = $this->body;
        $body_num = strlen($body);
        if ($body_num > 128) {
            $body = mb_substr($body, 0, 25); //body字符不能超过128个字符，这里截取一部分
        }

        //配置回调地址
        if ($this->type == 'online_registration') {
            $notify_url = $this->online_registration_notify_url;
        } else {
            $notify_url = $this->notify_url;
        }

        $parameters = array(
            'appid'         => $this->appid, //小程序ID
            'mch_id'        => $this->mch_id, //商户号
            'nonce_str'     => $this->createNoncestr(), //随机字符串
            //            'body' => 'test', //商品描述
            'body'          => $body,
            //            'out_trade_no' => '2015450806125348', //商户订单号
            'out_trade_no'  => $this->out_trade_no,
            //            'total_fee' => floatval(0.01 * 100), //总金额 单位 分
            'total_fee'     => $this->total_fee,
            //            'spbill_create_ip' => $_SERVER['REMOTE_ADDR'], //终端IP
            'spbill_create_ip' => '127.0.0.1', //终端IP
            'notify_url'    => $notify_url, //通知地址  确保外网能正常访问
            'openid'        => $openId, //用户id
            'trade_type'    => 'JSAPI' //交易类型
        );

        //统一下单签名
        $parameters['sign'] = $this->getSign($parameters);
        $xmlData = $this->arrayToXml($parameters);
        $return = $this->xmlToArray($this->postXmlCurl($xmlData, $url, 60));

        return $return;
    }

    /**
     * 获取 用户 openid，用户支付
     */
    private  function getOpendId()
    {
        $url = 'https://api.weixin.qq.com/sns/jscode2session?appid=' . $this->appid . '&secret=' . $this->appSecret . '&js_code=' . $this->code . '&grant_type=authorization_code';
        $data = $this->postXmlCurl(' ', $url);
        $data = json_decode($data, true);
        return $data['openid'];
    }

    private static function postXmlCurl($xml, $url, $second = 30)
    {
        $ch = curl_init();
        //设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE); //严格校验
        //设置header
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //post提交方式
        if (!empty($xml)) {
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        }

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_TIMEOUT, 40);
        set_time_limit(0);

        //运行curl
        $data = curl_exec($ch);
        //返回结果
        if ($data) {
            curl_close($ch);
            return $data;
        } else {
            $error = curl_errno($ch);
            curl_close($ch);
            Log::error("curl出错，错误码:$error");
        }
    }

    //post https请求，CURLOPT_POSTFIELDS xml格式
    function postXmlCurlByTransfer($xml, $url, $second = 30)
    {
        //初始化curl       
        $ch = curl_init();
        //超时时间
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);
        //这里设置代理，如果有的话
        //curl_setopt($ch,CURLOPT_PROXY, '8.8.8.8');
        //curl_setopt($ch,CURLOPT_PROXYPORT, 8080);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        //设置header
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //post提交方式
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);

        //默认格式为PEM，可以注释
        curl_setopt($ch, CURLOPT_SSLCERTTYPE, 'PEM');
        curl_setopt($ch, CURLOPT_SSLCERT, public_path('uploads') . '/cert/apiclient_cert.pem');
        //默认格式为PEM，可以注释
        curl_setopt($ch, CURLOPT_SSLKEYTYPE, 'PEM');
        curl_setopt($ch, CURLOPT_SSLKEY, public_path('uploads') . '/cert/apiclient_key.pem');

        //运行curl
        $data = curl_exec($ch);
        //返回结果
        if ($data) {
            curl_close($ch);
            return $data;
        } else {
            $error = curl_errno($ch);

            Log::error("curl出错，错误码:" . $error);
            //    echo "<a href='http://curl.haxx.se/libcurl/c/libcurl-errors.html'>错误原因查询</a></br>";
            curl_close($ch);
            return false;
        }
    }



    //数组转换成xml
    private function arrayToXml($arr)
    {
        $xml = "<root>";
        foreach ($arr as $key => $val) {
            if (is_array($val)) {
                $xml .= "<" . $key . ">" . $this->arrayToXml($val) . "</" . $key . ">";
            } else {
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            }
        }
        $xml .= "</root>";
        return $xml;
    }


    //xml转换成数组
    private function xmlToArray($xml)
    {
        //禁止引用外部xml实体
        libxml_disable_entity_loader(true);
        $xmlstring = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
        $val = json_decode(json_encode($xmlstring), true);
        return $val;
    }


    //微信小程序接口
    private function weixinapp()
    {
        //统一下单接口
        $unifiedorder = $this->unifiedorder();
        //  print_r($unifiedorder);die;
        $parameters = array(
            'appId' => $this->appid, //小程序ID
            'timeStamp' => '' . time() . '', //时间戳
            'nonceStr' => $this->createNoncestr(), //随机串
            'package' => 'prepay_id=' . $unifiedorder['prepay_id'], //数据包
            'signType' => 'MD5' //签名方式
        );
        //签名
        $parameters['paySign'] = $this->getSign($parameters);
        return $parameters;
    }

    //作用：产生随机字符串，不长于32位
    private function createNoncestr($length = 32)
    {
        $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }


    //作用：生成签名
    private function getSign($Obj)
    {
        foreach ($Obj as $k => $v) {
            $Parameters[$k] = $v;
        }
        //签名步骤一：按字典序排序参数
        ksort($Parameters);
        $String = $this->formatBizQueryParaMap($Parameters, false);
        //签名步骤二：在string后加入KEY
        $String = $String . "&key=" . $this->key;
        //签名步骤三：MD5加密
        $String = md5($String);
        //签名步骤四：所有字符转为大写
        $result_ = strtoupper($String);
        return $result_;
    }


    ///作用：格式化参数，签名过程需要使用
    private function formatBizQueryParaMap($paraMap, $urlencode)
    {
        $buff = "";
        ksort($paraMap);
        foreach ($paraMap as $k => $v) {
            if ($urlencode) {
                $v = urlencode($v);
            }
            $buff .= $k . "=" . $v . "&";
        }
        $reqPar = '';
        if (strlen($buff) > 0) {
            $reqPar = substr($buff, 0, strlen($buff) - 1);
        }
        return $reqPar;
    }


    /**
     * 自主查询微信接口，订单是否支付成功 (通用)
     * @param order_id   订单id
     */
    public function checkWxOrderPay($order_id)
    {
        header("Content-type: text/html; charset=utf-8");
        $url = "https://api.mch.weixin.qq.com/pay/orderquery";

        $onoce_str = $this->createNoncestr(32);

        $data["appid"] = $this->appid;
        $data["mch_id"] = $this->mch_id;
        $data["out_trade_no"] = $order_id;
        $data["nonce_str"] = $onoce_str;

        $s = $this->getSign($data, false);
        $data["sign"] = $s;

        $xml = $this->arrayToXml($data);
        $response = $this->postXmlCurl($xml, $url);

        $ruturnOut = json_encode(simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA));
        $outType = json_decode($ruturnOut, true); //将微信返回的结果xml转成数组
        //$aString = '$a = '.var_export($outType, true).';';


        //验证正确性
        if ($outType['return_code'] == 'SUCCESS' && $outType['result_code'] == 'SUCCESS' && $outType['trade_state'] == 'SUCCESS') {
            //成功
            return $outType; //直接把订单信息返回
        } else {
            //失败
            //获取错误代码
            /* $error = $outType['result_code'];//FAIL
             * $error_msg = $outType['err_code_des'];//错误消息*/
            return false;
        }
    }



    /**
     * 红包转账
     *
     * @param [type] $order_id
     * @param [type] $price
     * @param [type] $open_id 
     * @param [type] $packet_msg
     * @param [type] $log_model
     * @return void
     */
    public function transfer($order_id, $price, $open_id, $packet_msg)
    {
        //   return ['code' => 202];//先默认不转账

        if ($price > 50000) {
            Log::error('转账金额过大，不允许转账：'.$price);
            return false; //金额太大不允许转账  最大500，这里是分为单位
        }

        header("Content-type: text/html; charset=utf-8");
        $url = 'https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers';

        $onoce_str = $this->getRandChar(32);
        $data["mch_appid"] = $this->appid;
        $data["mchid"] = $this->mch_id;
        $data["nonce_str"] = $onoce_str;
        $data["partner_trade_no"] = $order_id;
        $data['openid'] = $open_id; //'ovzql5BBiTnCV4EQBCj_EIHc-poc'
        $data['check_name'] = 'NO_CHECK';
        $data['amount'] = $price;
        $data['desc'] = $packet_msg;

        $s = $this->getSign($data, false);
        $data["sign"] = $s;

        $xml = $this->arrayToXml($data);
        $response = $this->postXmlCurlByTransfer($xml, $url);

        $ruturnOut = json_encode(simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA));

        $outType = json_decode($ruturnOut, true); //将微信返回的结果xml转成数组

        Log::error(json_encode($ruturnOut));

        if (!$outType) {
            return ['code' => 202];
        }

        $data = [
            'return_code' => $outType['return_code'],
            'return_msg' => !empty($outType['return_msg']) ? $outType['return_msg'] : '',
            'result_code' => $outType['result_code'],
            'create_time' => date("Y-m-d H:i:s")
        ];

        if ($outType['result_code'] == 'SUCCESS') {
            // $data['device_info'] = $outType['device_info'];
            // $data['nonce_str'] = $outType['nonce_str'];
            $data['mch_appid'] = $outType['mch_appid'];
            $data['mchid'] = $outType['mchid'];
        }

        if ($outType['result_code'] == 'FAIL') {
            $data['err_code'] = $outType['err_code'];
            $data['err_code_des'] = $outType['err_code_des'];
        }

        if ($outType['return_code'] == 'SUCCESS' && $outType['result_code'] == 'SUCCESS') {
            $data['partner_trade_no'] = $outType['partner_trade_no'];
            $data['payment_no'] = $outType['payment_no'];
            $data['payment_time'] = $outType['payment_time'];
        }

        $data["partner_trade_no"] = $order_id;

        if ($outType['return_code'] == 'SUCCESS' && $outType['result_code'] == 'SUCCESS') {
            return ['code' => 200, 'pay_time' => $outType['payment_time'], 'data' => $data];
        } else {
            return ['code' => 202, 'msg' => $outType['return_msg'], 'data' => $data];
        }
    }

    //获取指定长度的随机字符串
    function getRandChar($length)
    {
        $str = null;
        $strPol = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
        $max = strlen($strPol) - 1;

        for ($i = 0; $i < $length; $i++) {
            $str .= $strPol[rand(0, $max)]; //rand($min,$max)生成介于min和max两个数之间的一个随机整数
        }

        return $str;
    }
}
