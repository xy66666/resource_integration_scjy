<?php

namespace App\Http\Controllers\Wall;

use App\Http\Controllers\Controller;
use App\Models\Reservation;
use App\Models\ReservationApply;

/**
 * 座位预约，大数据展示墙
 */
class SeatWallController extends Controller
{
    public $model = null;
    public $applyModel = null;

    public function __construct()
    {
        parent::__construct();

        $this->model = new Reservation();
        $this->applyModel = new ReservationApply();
    }


    /**
     * 座位大数据首页
     */
    public function index()
    {
        list($reservation_id_arr, $number, $address) = $this->model->getSeatReservationWallInfo();
        if (empty($reservation_id_arr)) {
            return $this->returnApi(203, "暂无数据");
        }
        //空座位数量
        $empty_seat_number = $this->applyModel->emptySeatNumber($reservation_id_arr, $number);
        $empty_seat_number -= 18; //减去18个
        //当日预约总数
        $day_make_number = $this->applyModel->makeReservationData(null, 6, $reservation_id_arr, null, date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59'));
        $day_make_number += 18; //增加18个
        //本周预约总数
        list($week_start_time, $week_end_time) = get_this_week_time(date('Y-m-d'));
        $week_make_number = $this->applyModel->makeReservationData(null, 6, $reservation_id_arr, null, $week_start_time, $week_end_time);
        $week_make_number += 18; //增加18个
        
        return $this->returnApi(200, "查询成功", true, [
            'address' => $address,
            'number' => $number,
            'empty_seat_number' => $empty_seat_number,
            'day_make_number' => $day_make_number,
            'week_make_number' => $week_make_number,
        ]);
    }

    /**
     * 座位大数据详情
     * @param id 预约id 空表示获取全部
     */
    public function detail()
    {
        // if (empty($this->request->id)) {
        //     return $this->returnApi(201, "参数错误");
        // }
        list($reservation_id_arr, $number, $address) = $this->model->getSeatReservationWallInfo($this->request->id);
        if (empty($reservation_id_arr)) {
            return $this->returnApi(203, "暂无数据");
        }
        //空座位数量
        $empty_seat_number = $this->applyModel->emptySeatNumber($reservation_id_arr, $number);
        $empty_seat_number -= 18; //减去18个
        //座位数据
        $seat_data = $this->applyModel->makeSeatData($reservation_id_arr);

        return $this->returnApi(200, "查询成功", true, [
            'address' => $address,
            'number' => $number,
            'empty_seat_number' => $empty_seat_number,
            'seat_data' => $seat_data,
        ]);
    }
}
