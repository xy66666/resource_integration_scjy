<?php

namespace App\Http\Controllers\PayInfo;
/**
 * 关于微信退款的说明
 * 1.微信退款要求必传证书，需要到https://pay.weixin.qq.com 账户中心->账户设置->API安全->下载证书，证书路径在第119行和122行修改
 * 2.错误码参照 ：https://pay.weixin.qq.com/wiki/doc/api/jsapi.php?chapter=9_4
 */

class WxReturnMoneyController
{	
    protected $mchid = '1497771792';//https://pay.weixin.qq.com 产品中心-开发配置-商户号
    protected $appid= '';//微信支付申请对应的公众号的APPID
    protected $apiKey= 'db91fc73a9bc131922527c3d86f7f499';//https://pay.weixin.qq.com 帐户设置-安全设置-API安全-API密钥-设置API密钥
    
 //   protected $notifyUrl ="http://game.huilanyun.com/answer/public/index/Notify/return_index";//回调域名,退款的回调域名
    public $data = null;

    public function __construct()
    {
        $this->appid = config('other.appid');//微信公众号appid

    }

    /**
     * 退款
     * @param float $totalFee 订单金额 单位元
     * @param float $refundFee 退款金额 单位元
     * @param string $refundNo 退款单号
     * @param string $wxOrderNo 微信订单号
     * @param string $orderNo 商户订单号
     * @param string $refundDesc 退款原因
     * @return string
     */
    public function doRefund($totalFee, $refundFee, $refundNo, $wxOrderNo='',$orderNo='' ,$refundDesc ='')
    {   
      //  $notifyUrl = $this->notifyUrl;
        
        $config = array(
            'mch_id' => $this->mchid,
            'appid' => $this->appid,
            'key' => $this->apiKey,
        );
        $unified = array(
            'appid' => $config['appid'],
            'mch_id' => $config['mch_id'],
            'nonce_str' => self::createNonceStr(),
            'total_fee' => intval($totalFee * 100),       //订单金额	 单位 转为分
            'refund_fee' => intval($refundFee * 100),       //退款金额 单位 转为分
            'sign_type' => 'MD5',               //签名类型 支持HMAC-SHA256和MD5，默认为MD5
            'transaction_id'=>$wxOrderNo,       //微信订单号
            'out_trade_no'=>$orderNo,           //商户订单号
            'out_refund_no'=>$refundNo,         //商户退款单号
            'refund_desc'=>$refundDesc,     //退款原因（选填）
        );
     //   'notify_url' => $notifyUrl, //退款回调域名
        $unified['sign'] = self::getSign($unified, $config['key']);

        $responseXml = $this->curlPost('https://api.mch.weixin.qq.com/secapi/pay/refund', self::arrayToXml($unified));     

        $unifiedOrder = simplexml_load_string($responseXml, 'SimpleXMLElement', LIBXML_NOCDATA);

        if ($unifiedOrder === false) {
            return  ['code'=>203 , 'msg'=>'parse xml error'];
        }
        
        //提交成功
        if ($unifiedOrder->return_code != 'SUCCESS') {
            //提交失败了
            $msg = $unifiedOrder->return_msg;
            $msg = object_array($msg);
            $msg = isset($msg[0]) ? $msg[0] : $msg;
            return  ['code'=>205 , 'msg'=>$msg];
        }
        //表示提交申请成功了
        if ($unifiedOrder->result_code != 'SUCCESS') {
            $err_code = $unifiedOrder->err_code;//错误码
            $err_code_des = $unifiedOrder->err_code_des;//消息内容

            $err_code = object_array($err_code);
            $err_code = isset($err_code[0]) ? $err_code[0] : $err_code;

            $err_code_des = object_array($err_code_des);
            $msg = isset($err_code_des[0]) ? $err_code_des[0] : $err_code_des;

            return  ['code'=>206 , 'err_code'=>$err_code , 'msg'=>$msg];
        }
        return  ['code'=>200 , 'refund_id'=>$unifiedOrder->refund_id];//返回微信退款单号
    }

    /**
     * 对象转数组
     * @param string $url
     * @param unknown $options
     * @return unknown
     */
    public  function object_array($array){
	if(is_object($array)){
		$array = (array)$array;
	}
	if(is_array($array)){
		foreach($array as $key=>$value){
			$array[$key] = object_array($value);
		}
	}
	return $array;
}
    public static function curlGet($url = '', $options = array())
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        if (!empty($options)) {
            curl_setopt_array($ch, $options);
        }
        //https请求 不验证证书和host
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    public function curlPost($url = '', $postData = '', $options = array())
    {	
        if (is_array($postData)) {
            $postData = http_build_query($postData);
        }
        $ch = \curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //设置cURL允许执行的最长秒数
        if (!empty($options)) {
            curl_setopt_array($ch, $options);
        }
        //https请求 不验证证书和host
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        //第一种方法，cert 与 key 分别属于两个.pem文件
        //默认格式为PEM，可以注释
        $root_addr = config('other.root_addr');

        curl_setopt($ch,CURLOPT_SSLCERTTYPE,'PEM');
        curl_setopt($ch,CURLOPT_SSLCERT , $root_addr.'/cert/apiclient_cert.pem');
        //默认格式为PEM，可以注释
        curl_setopt($ch,CURLOPT_SSLKEYTYPE,'PEM');
        curl_setopt($ch,CURLOPT_SSLKEY , $root_addr.'/cert/apiclient_key.pem');
        //第二种方式，两个文件合成一个.pem文件
//        curl_setopt($ch,CURLOPT_SSLCERT,getcwd().'/all.pem');
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    public static function createNonceStr($length = 16)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }
    public static function arrayToXml($arr)
    {
        $xml = "<xml>";
        foreach ($arr as $key => $val) {
            if (is_numeric($val)) {
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            } else
                $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
        }
        $xml .= "</xml>";
        return $xml;
    }

    public static function getSign($params, $key)
    {
        ksort($params, SORT_STRING);
        $unSignParaString = self::formatQueryParaMap($params, false);
        $signStr = strtoupper(md5($unSignParaString . "&key=" . $key));
        return $signStr;
    }
    protected static function formatQueryParaMap($paraMap, $urlEncode = false)
    {
        $buff = "";
        ksort($paraMap);
        foreach ($paraMap as $k => $v) {
            if (null != $v && "null" != $v) {
                if ($urlEncode) {
                    $v = urlencode($v);
                }
                $buff .= $k . "=" . $v . "&";
            }
        }
        $reqPar = '';
        if (strlen($buff) > 0) {
            $reqPar = substr($buff, 0, strlen($buff) - 1);
        }
        return $reqPar;
    }
}

