<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2020/5/22
 * Time: 14:47
 */

namespace App\Http\Controllers\PayInfo;

use App\Http\Controllers\PayInfo\WechatPayController;
use App\Models\OweOrder;
use App\Models\OweOrderPay;
use App\Models\OweOrderRecord;
use App\Models\UserLibraryInfo;
use App\Models\UserWechatInfo;
use App\Validate\OwePayInfoValidate;
use Illuminate\Support\Facades\DB;

/**
 * 商品支付模块
 * Class PayInfo
 * @package app\port\controller
 */
class OwePayInfoController extends CommonController
{
    private $validate = null;
    private $model = null;

    public function __construct()
    {
        parent::__construct();
        $this->validate = new OwePayInfoValidate();
        $this->model = new OweOrder();
    }

    /**
     * 统一下单，获取支付所需的参数 (欠费))
     * @param order_id  订单id
     */
    public function getPayInfo()
    {
        return $this->returnApi(202, '演示项目不支持在线支付！');
        // 自动验证
        if (!$this->validate->scene('get_pay_param')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $order_id = $this->request->order_id;
        $wechat_id = $this->request->user_info['wechat_id'];
        $order_info = $this->model->find($order_id);

        if (empty($order_info)) {
            return $this->returnApi(203, '网络错误');
        }
        if ($order_info['is_pay'] != 1) {
            return $this->returnApi(203, '网络错误');
        }

        $open_id = UserWechatInfo::where('id', $wechat_id)->value('open_id');
        $price = $order_info->price_fen; //支付金额转换为分
        $wechatPayObj = new WechatPayController($order_info->order_number, '欠费缴纳', $price, $open_id, 'owe');
        $res = $wechatPayObj->pay();

        //  return $this->returnApi(202 , json_encode($res));

        if ($res) {
            return $this->returnApi(200, '获取成功', true, $res);
        }
        return $this->returnApi(202, '获取失败');
    }


    /**
     * 支付成功,验证订单状态，验证成功后修改订单状态
     * @param order_id  订单id
     */
    public function paymentSuccess()
    {
        // 自动验证
        if (!$this->validate->scene('payment_success')->check($this->request->all())) {
            return $this->returnApi(201,  $this->validate->getError());
        }
        $order_id = $this->request->order_id;

        //直接修改状态
        $order_info = $this->model->find($order_id);
        if (empty($order_info)) {
            return $this->returnApi(203, '网络错误');
        }

        if ($order_info['is_pay'] == 1) {
            //自助验证状态
            $result = $this->checkWeChatPay($order_id);

            if (!$result) {
                return $this->returnApi(202, '支付失败');
            }
            return $this->returnApi(200, '支付成功', true);
        } elseif ($order_info['is_pay'] == 2) {
            return $this->returnApi(200, '支付成功', true);
        }

        return $this->returnApi(202, '支付失败');
    }

    /**
     * 推送
     */
    public function pushMsg($order_info)
    {
        //添加系统消息
        $system_id = $this->systemAdd('欠款缴纳支付成功', $order_info['user_id'], $order_info['account_id'], 22, 0, '缴纳欠款【' . $order_info['price'] . '元】成功');
    }

    /**
     * 查询微信支付系统
     */
    public function checkWeChatPay($order_id)
    {
        $order_info = $this->model->find($order_id);
        if ($order_info->is_pay == 2) {
            return true; //已支付成功
        }

        //调用微信查询接口
        $wechatPayObj = new WechatPayController($order_info->order_number);
        $result = $wechatPayObj->checkWxOrderPay();

        if (!$result) {
            return false;
            //return $this->returnData(203 , '数据异常');//实际为未查询到支付交易情况
        }
        //否则提交信息，修改数据
        //返回的$result为数组

        //微信支付订单号
        $trade_no = $result['transaction_id'];
        //订单支付时间
        $time_end = $result['time_end'];
        //转换为时间戳
        $gtime = strtotime($time_end);
        $gmt_payment = date("Y-m-d H:i:s", $gtime); //转换为日期时间格式
        //买家微信支付分配的终端设备号，
        //   $buyer_id  = $result['device_info'];
        //买家微信账号
        //	$buyer_login_id  = $_POST['buyer_login_id'];
        //订单总金额,订单总金额，单位为分
        $total_amount = $result['total_fee'];
        //实收金额,商家在交易中实际收到的款项
        $receipt_amount = $result['total_fee']; //微信的实收与总金额一致
        //付款金额,现金支付金额订单现金支付金额，详见支付金额
        $buyer_pay_amount = $result['cash_fee'];

        //此处编写回调处理逻辑
        $where = array(
            'order_number' => $order_info->order_number,
            'pay_time' => $gmt_payment,
            'payment' => 1,
            'trade_no' => $trade_no,
            //    'buyer_id'		=> $buyer_id,
            //	'buyer_login_id'=>$buyer_login_id,
            'total_amount' => $total_amount,
            'receipt_amount' => $receipt_amount,
            'buyer_pay_amount' => $buyer_pay_amount,
        );

        //开启事务
        DB::beginTransaction();
        try {
            $this->model->where('id', $order_id)->update(array('is_pay' => 2, 'change_time' => $gmt_payment));
            $oweOrderPayModel = new OweOrderPay();
            $oweOrderPayModel->add($where);

            //添加推送信息
            $this->pushMsg($order_info);

            $fin_account_id_arr = OweOrderRecord::where('order_id', $order_id)->pluck('fin_account_id');
            //处理真实数据
            $libApi = $this->getLibApiObj();
            $account = UserLibraryInfo::where('id', $order_info['account_id'])->value('account');
            $res = $libApi->payArrears($account, $fin_account_id_arr, 1); //修改真实数据
            if ($res['code'] !== 200) {
                throw new \Exception('处理失败');
            }

            DB::commit();
            return true;    //支付成功
        } catch (\Exception $e) {
            DB::rollBack();
            return false;
        }
    }
}
