<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;

/**
 * 文件上传类
 */
class UploadsFileController extends Controller
{

    //允许文件上传的key
    protected $allow_file_name_arr = [
        'img',
        'news',
        'production',
        'author_img',
        'foreword',
        'production_cover',
        'production_video',
        'production_voice',
        'digital_img',
        'book_img',
        'branch_map',
        'banner',
        'goods_img',
        'share_img',
        'watermark_img',
        'scenic',
        'scenic_type_icon',
        'temp_video', //临时视频
        'temp_audio', //临时音频
        'code_guide_exhibition',
        'code_guide_production',
        'navigation_svg',
        'ebook_img', //电子书图片
        'ebook_pdf', //电子书pdf
        'competite_activity_cover', //大赛作品封面
        'competite_activity_img', //大赛作品图片
        'competite_activity_video', //大赛作品视频
        'competite_activity_voice', //大赛作品音频
        'competite_activity_pdf', //pdf类型作品
        'competite_activity_authorizations', //授权书标识
        'competite_activity_promise', //授权书标识
    ];

    protected $need_thumb_file_name_arr = [
        'news',
        'production_cover',
        'author_img',
        'foreword',
        'goods_img',
        'share_img',
        'scenic',
        'code_guide_exhibition',
        'code_guide_production',
        'competite_activity_cover',
        'competite_activity_img'
    ]; //需要生成缩略图的key

    /**
     * 定义图片上传公共方法           (单文件与多文件都可以(多文件上传不生成缩略图)  暂不支持多文件上传)
     * @param file 文件名   根据每个方法要求不同，传不同名称   默认file
     *                     固定参数   img  前台活动报名，上传图片   不需生成缩略图   最大1M
     *                     固定参数   news 新闻   需生成缩略图   最大1M
     *                     固定参数   production  展览图片   需生成缩略图   最大10M
     *                     固定参数   author_img 作者   需生成缩略图   最大2M
     *                     固定参数   foreword   前言   需生成缩略图   最大2M
     *                     固定参数   production_video 展览视频     最大200M
     *                     固定参数   production_voice 展览音频    最大20M
     *                     固定参数   digital_img 数字阅读图片   不需生成缩略图
     *                     固定参数   book_img 书籍图片   不需生成缩略图
     *                     固定参数   branch_img 场馆图片   不需生成缩略图
     *                     固定参数   branch_map 文旅地图   不需生成缩略图
     *                     固定参数   banner BANNER图片   不需生成缩略图
     *                     固定参数   goods_img 商品图片   需生成缩略图
     *                     固定参数   share_img 分享图片   需生成缩略图
     *                     固定参数   scenic 景点打卡   需生成缩略图
     *                     固定参数   watermark_img 分享图片   水印图片  会自动按比例生成 3张图片
     *                     固定参数   scenic_type_icon 景点打卡类型
     *
     *
     *
     *                     固定参数   terminal_video  屏幕视频     无需生成缩略图   最大2M
     *                     固定参数   bookcase        终端        无需生成缩略图   最大2M
     *                     固定参数   book_img       书籍封面      需要生成缩略图的key   最大2M
     *                     固定参数   installation_screen       屏幕安装包（命名必须Screen-Stand-Alone-A开头）      不需要生成缩略图的key   最大30M
     *                     固定参数   screen_log    终端屏幕终端日志  最大 3M  文件命名规范  002800005~1995-06-15.txt
     *
     *                     固定参数   competite_activity_cover  升级版大赛活动封面图片   需生成缩略图   最大1M
     *                     固定参数   competite_activity_img   升级版大赛活动展览图片   需生成缩略图   最大3M
     *                     固定参数   competite_activity_video 升级版大赛活动展展览视频     最大200M
     *                     固定参数   competite_activity_voice 升级版大赛活动展展览音频    最大20M
     *                     固定参数   competite_activity_pdf pdf类型作品    最大5M
     *                     固定参数   competite_activity_authorizations 授权书标识    最大1M
     *                     固定参数   competite_activity_promise 承诺书标识    最大1M
     */
    public function commonUpload(Request $request)
    {
        //删除所有临时文件
        $this->delAllTempFile();

        $file_name = array_keys($_FILES);
        //   dd([request()->file($file_name[0])->getMimeType(),request()->file($file_name[0])->getClientMimeType(),request()->file($file_name[0])->getClientOriginalExtension()]);

        if (empty($file_name))  return response()->json(['code' => 201, 'msg' => '上传文件不能为空']);

        if (!in_array($file_name[0], $this->allow_file_name_arr)) return response()->json(['code' => 201, 'msg' => '上传文件名错误']);


        //验证文件类型和大小
        $file_validate = $this->fileValidate($request, $file_name[0]);
        if ($file_validate !== true) {
            return response()->json(['code' => 201, 'msg' => $file_validate]);
        }

        $foldername = $file_name[0] . '/' . date('Y-m-d'); //$file_name.'/'.date('Y-m-d')  存放路径，按日志存储
        $file = $request->file($file_name)->store($foldername);
        $file_addr = public_path('uploads') . '/' . $file; //图片地址

        if ($file) {
            $data['img'] = substr($file, 0, 1) == '/' ? substr($file, 1, strlen($file)) : $file; //去掉第一个 斜线

            //是否需要生成缩略图
            if ($file_name[0] == 'watermark_img') {
                $thumb_img1 = $this->getThumbImg($file_addr, $file, '1.', true, 0.67, 90); //只压缩质量 20，前面已经添加了水印
                $thumb_img2 = $this->getThumbImg($file_addr, $file, '2.', true, 0.33, 90); //按比例裁剪图片大小，压缩质量 60

                if ($thumb_img1 && $thumb_img2) {
                    $data['img1'] = $thumb_img1['thumb_img_name']; //中间的图
                    $data['img2'] = $thumb_img2['thumb_img_name']; //最小的图
                    $data['width'] = $thumb_img2['small_img_width'];
                    $data['height'] = $thumb_img2['small_img_height'];
                }
            } elseif (in_array($file_name[0], $this->need_thumb_file_name_arr)) {
                $thumb_img = $this->getThumbImg($file_addr, $file); //获取缩略图
                if ($thumb_img) {
                    $data['thumb_img'] = $thumb_img['thumb_img_name'];
                    $data['width'] = $thumb_img['small_img_width'];
                    $data['height'] = $thumb_img['small_img_height'];
                } else {
                    $data['thumb_img'] = substr($file, 0, 1) == '/' ? substr($file, 1, strlen($file)) : $file; //去掉第一个 斜线
                    $data['width'] = null;
                    $data['height'] = null;
                }
            }

            return (['code' => 200, 'msg' => '上传成功', 'content' => $data]);
        } else {
            return (['code' => 404, 'msg' => '未找到资源', 'data' => '']);
        }
    }


    /**
     * 获取图片封面（缩略图）
     * @param $file_addr 图片路径  ,数据库存放路径
     * @param $file 原图片返回到前端的地址
     * @param $is_ratio 是否按比例压缩   true 是  false 不压缩
     * @param $ratio 压缩比例   0 默认压缩比例  其他 就是按比例压缩，比如传 0.5 就在原来基础上压缩一半
     * @param $quality 压缩质量   90为和原图一致   1 ~ 100 中间  100图片变大了
     * @param $watermark 是否添加水印  true false
     * @param $watermark_img 水印图片，默认采用统一，可单独上传
     * @param $new_file_name 新图片名，必须包含点 ·  最后会在原基础上，进行替换
     */
    public function getThumbImg($file_addr, $file, $new_file_name = null, $is_ratio = true, $ratio = 0, $quality = 90, $watermark = false, $watermark_img = null)
    {

        ini_set('memory_limit', '1024M'); //临时加大内存，否则在生成缩略图时可能没反应 Image::make($file_addr) 时；

        $img_info = @getimagesize($file_addr);
        // if(!$img_info) return 'default/default_production_cover.png';//图片不存在直接返回false
        if (!$img_info) return ''; //图片不存在直接返回'' ; 使用默认图片

        $width = $img_info[0];
        $height = $img_info[1];
        $small_img_width = $width;
        $small_img_height = $height;

        $quality = $quality ? $quality : 90; //压缩质量,90
        $thumb_img = Image::make($file_addr);
        if ($is_ratio) {
            if ($ratio) {
                $small_img_width = round($width * $ratio, 2); //压缩宽度
                $small_img_height = round($height * $ratio, 2); //压缩高度

                $thumb_img = $thumb_img->resize($small_img_width, $small_img_height);
            } else {
                $small_img_width = $width < 500 ? (int)$width : 500; //缩略图宽
                $ratio = round($small_img_width / $width, 2); //计算压缩比
                $small_img_height = round($height * $ratio, 2); //高度按宽度比例压缩

                $thumb_img = $thumb_img->resize($small_img_width, $small_img_height);
            }
        }

        //是否添加水印
        if ($watermark) {
            //根据原图片宽度，调整水印大小
            if ($width < 1000) {
                //插入水印, 水印位置在原图片的右下角, 距离下边距 10 像素, 距离右边距 15 像素
                $watermark_img = $watermark_img ? str_replace('.', '2.', $watermark_img) : 'default/default_watermark_img2.png';
                $watermark_x = 15;
                $watermark_y = 10;
            } elseif ($width > 1000 && $width < 2000) {
                //插入水印, 水印位置在原图片的右下角, 距离下边距 10 像素, 距离右边距 15 像素
                $watermark_img = $watermark_img ? str_replace('.', '1.', $watermark_img) : 'default/default_watermark_img1.png';
                $watermark_x = 30;
                $watermark_y = 20;
            } elseif ($width > 2000) {
                //插入水印, 水印位置在原图片的右下角, 距离下边距 10 像素, 距离右边距 15 像素
                $watermark_img = $watermark_img ? $watermark_img : 'default/default_watermark_img.png'; //原图最大，2最小
                $watermark_x = 50;
                $watermark_y = 35;
            }
            $watermark_img = public_path('uploads') . '/' . $watermark_img;
            $thumb_img = $thumb_img->insert($watermark_img, 'bottom-right', $watermark_x, $watermark_y);
            // $thumb_img = $thumb_img->text('我就是来看看水印加字好看不.', 120, 100); //添加文字水印
        }


        //将处理后的图片重新保存到其他路径
        $new_file_name = $new_file_name ? $new_file_name : '_thumb.';
        if (stripos($new_file_name, '.') === false) {
            return ''; //生成失败，返回默认图
        }
        $thumb_img_addr = str_replace('.', $new_file_name, $file_addr);
        $thumb_img = $thumb_img->save($thumb_img_addr, $quality); // 保存图片，并设置质量压缩为60   save 第二个参数为压缩质量

        if (is_object($thumb_img)) {
            $thumb_img_name = str_replace('.', $new_file_name, $file);
            return ['thumb_img_name' => $thumb_img_name, 'small_img_width' => $small_img_width, 'small_img_height' => $small_img_height];
        }
        return ''; //生成失败，返回默认图
    }




    /**
     * 文件验证
     */
    public function fileValidate($request, $file = 'file')
    {
        $file_type = $this->getFileTypeAndSize($file);

        if (!empty($file_type['min_width']) && !empty($file_type['min_height']) && !empty($file_type['max_width']) && !empty($file_type['max_height'])) {
            if ($file_type['min_width'] == $file_type['max_width']) {
                $width_msg = $file_type['min_width'];
            } else {
                $width_msg = $file_type['min_width'] . '~' . $file_type['max_width'];
            }
            if ($file_type['min_height'] == $file_type['max_height']) {
                $height_msg = $file_type['min_height'];
            } else {
                $height_msg = $file_type['min_height'] . '~' . $file_type['max_height'];
            }

            $validator = Validator::make($request->file(), [
                $file => 'required|file|max:' . $file_type['size'] . '|mimes:' . $file_type['ext'] . '|dimensions:min_width=' . $file_type['min_width'] . ',min_height=' . $file_type['min_height'] . ',max_width=' . $file_type['max_width'] . ',max_height=' . $file_type['max_height'],
            ], [
                //验证是否存在文件
                $file . '.required' => '文件不能为空',
                //验证是否为文件
                $file . '.file' => '请确认你的文件格式',
                //验证文件上传大小
                $file . '.max' => '文件大小不符合要求',
                //验证上传文件格式
                $file . '.mimes' => '请确认上传文件后缀' . $file_type['ext'],
                //验证上传文件尺寸
                $file . '.dimensions' => '请确认上传文件尺寸，宽度像素：' . $width_msg . '，宽度像素：' . $height_msg,
            ]);
        } else {
            $validator = Validator::make($request->file(), [
                $file => 'required|file|max:' . $file_type['size'] . '|mimes:' . $file_type['ext'],
            ], [
                //验证是否存在文件
                $file . '.required' => '文件不能为空',
                //验证是否为文件
                $file . '.file' => '请确认你的文件格式',
                //验证文件上传大小
                $file . '.max' => '文件大小不符合要求',
                //验证上传文件格式
                $file . '.mimes' => '请确认上传文件后缀' . $file_type['ext'],
            ]);
        }

        if ($validator->fails()) { //如果有错误
            // return $validator->errors(); //返回得到错误
            return join('|', $validator->errors()->toArray()[$file]); //获取所有的错误，用 | 拼接
        }

        return true;
    }

    /**
     * 根据文件名，定义上传允许的后缀 和 文件大小
     * https://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types   mimes mimetypes  对照表
     */
    public function getFileTypeAndSize($file_name)
    {
        switch ($file_name) {
            case 'code_guide_exhibition':   //扫码导游
                $file_type = ['size' => 2 * 1024, 'ext' => 'jpg,png,jpeg']; //10M   size 单位 kb  tp框架单位为 b
                break;
            case 'code_guide_production':   //扫码导游
                $file_type = ['size' => 1 * 1024, 'ext' => 'jpg,png,jpeg']; //10M   size 单位 kb  tp框架单位为 b
                break;
            case 'production':
                $file_type = ['size' => 10 * 1024, 'ext' => 'jpg,png,jpeg']; //10M   size 单位 kb  tp框架单位为 b
                break;
            case 'book_img':
                $file_type = ['size' => 2 * 1024, 'ext' => 'jpg,png,jpeg'];
                break;
            case 'share_img':
                $file_type = ['size' => 0.1 * 1024, 'ext' => 'jpg,png,jpeg'];
                break;
            case 'ebook_img':
                $file_type = ['size' => 0.5 * 1024, 'ext' => 'jpg,png,jpeg']; //0.5M 电子书图片
                break;
            case 'ebook_pdf':
                $file_type = ['size' => 3 * 1024, 'ext' => 'pdf']; //3M 电子书PDF
                break;
            case 'production_cover':
                $file_type = ['size' => 0.5 * 1024, 'ext' => 'jpg,png,jpeg']; //0.5M   size 是以 kb为单位
                break;
            case 'scenic':
                $file_type = ['size' => 0.5 * 1024, 'ext' => 'jpg,png,jpeg']; //0.5M   size 是以 kb为单位
                break;
            case 'temp_video':
                $file_type = ['size' => 10 * 1024, 'ext' => 'mp4,mov']; //10M
                break;
            case 'temp_audio':
                $file_type = ['size' => 3 * 1024, 'ext' => 'mp3,m4a']; //3M
                break;
            case 'production_video':
                $file_type = ['size' => 100 * 1024, 'ext' => 'mp4,mov']; //100M
                break;
            case 'production_voice':
                $file_type = ['size' => 10 * 1024, 'ext' => 'mp3']; //10M
                break;
            case 'competite_activity_authorizations':
                $file_type = ['size' => 1 * 1024, 'ext' => 'jpg,png,jpeg']; //1M   size 是以 kb为单位
                break;
            case 'competite_activity_promise':
                $file_type = ['size' => 1 * 1024, 'ext' => 'jpg,png,jpeg']; //1M   size 是以 kb为单位
                break;
            case 'competite_activity_cover':
                $file_type = ['size' => 0.5 * 1024, 'ext' => 'jpg,png,jpeg']; //0.5M   size 是以 kb为单位
                break;
            case 'competite_activity_img':
                $file_type = ['size' => 0.5 * 1024, 'ext' => 'jpg,png,jpeg']; //0.5M   size 是以 kb为单位
                break;
            case 'competite_activity_video':
                $file_type = ['size' => 100 * 1024, 'ext' => 'mp4,mov']; //100M
                break;
            case 'competite_activity_voice':
                $file_type = ['size' => 10 * 1024, 'ext' => 'mp3']; //10M
                break;
            case 'competite_activity_pdf':
                $file_type = ['size' => 5 * 1024, 'ext' => 'pdf']; //5M 电子书PDF
                break;
            case 'installation_screen':
                $file_type = ['size' => 30 * 1024, 'ext' => 'apk,zip']; //30M    //apk文件被识别成了 zip文件
                break;
            case 'scenic_type_icon':
                $file_type = ['size' => 0.1 * 1024, 'ext' => 'jpg,png,jpeg']; //100kb    //100 kb以内
                break;
            case 'watermark_img':
                $file_type = ['size' => 0.5 * 1024, 'ext' => 'jpg,png,jpeg', 'min_width' => 580, 'min_height' => 300, 'max_width' => 680, 'max_height' => 600]; //0.5M   size 是以 kb为单位
                break;
            case 'screen_log':
                $file_type = ['size' => 30 * 1024, 'ext' => 'txt']; //3M
                break;
            case 'doc_file':
                $file_type = ['size' => 3 * 1024, 'ext' => 'xlx,xlsx,doc,docx,zip']; //3M  //xlsx文件有时会被识别成了 zip文件，有时正常， docx文件有时会被识别成了 zip文件，有时正常，   doc，xls 则还是  xls格式
                break;
            case 'ui_resource':
                $file_type = ['size' => 5 * 1024, 'ext' => 'zip']; //500KB  //xlsx文件有时会被识别成了 zip文件，有时正常， docx文件有时会被识别成了 zip文件，有时正常，   doc，xls 则还是  xls格式
                break;
            case 'activity_apply_accessory':
                $file_type = ['size' => 3 * 1024, 'ext' => 'zip']; //3M  
                break;
            case 'navigation_svg':
                $file_type = ['size' => 5 * 1024, 'ext' => 'svg']; //5M  
                break;
            default:
                $file_type = ['size' => 1 * 1024, 'ext' => 'jpg,png,jpeg'];
                break;
        }
        return $file_type;
    }
}
