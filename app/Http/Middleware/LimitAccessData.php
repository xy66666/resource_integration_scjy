<?php

namespace App\Http\Middleware;

use App\Http\Controllers\RedisServiceController;
use App\Models\AnswerActivityBrowseCount;
use App\Models\Manage;
use App\Models\TurnActivityBrowseCount;
use App\Models\UserInfo;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * 限制访问人数
 */
class LimitAccessData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // Log::channel('draw_activity')->info('URL：' . $request->fullUrl());
        // Log::channel('draw_activity')->info($request->all());
        // Log::channel('draw_activity')->info('-------------------------------------------------------------------------------------');

        //放入redis，限制访问人数
        $RedisServiceObj = RedisServiceController::getInstance()->getRedis();
        $RedisServiceObj->lpush('resource_integration', time());

        $path = $request->path();
        $act_id = $request->act_id;
        $id = $request->id;
        if ($path == 'wechat/answerActivity/detail' || $path == 'wechat/turnActivity/detail') {
            $act_id = $id;
        }

        //添加点击量
        if ($act_id && $path) {
            if (stripos($path, 'answerActivity')) {
                $browseCountModel = new AnswerActivityBrowseCount();
                $browseCountModel->updateBrowseNumber($act_id, 0);
            } elseif (stripos($path, 'turnActivity')) {
                $browseCountModel = new TurnActivityBrowseCount();
                $browseCountModel->updateBrowseNumber($act_id);
            }
        }

        return $next($request);
    }
}
