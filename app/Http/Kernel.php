<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        // \App\Http\Middleware\TrustHosts::class,
        \App\Http\Middleware\CrossHttp::class,  //解决接口跨域   // 命令：php artisan make:middleware CrossHttp
        \App\Http\Middleware\TrustProxies::class,
        \Fruitcake\Cors\HandleCors::class,
        \App\Http\Middleware\PreventRequestsDuringMaintenance::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,


        /**解决 mews/captcha 验证码一直验证不通过  因为所有的session 都是不能跨控制器、方法的，如果跨的话，session会重新生成，默认session的传递需要经过中间件 */
        \Illuminate\Session\Middleware\StartSession::class,  //laravel 8 只需要开启session 即可，其他不需要
        /** 解决 mews/captcha 验证码一直验证不通过 end */
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            // \Laravel\Sanctum\Http\Middleware\EnsureFrontendRequestsAreStateful::class,
            'throttle:api',
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],


        //port  名字与 路由名字一样，会在 请求路由时，自动调用
        'jwt' => [
            \App\Http\Middleware\CheckJwtHeaderToken::class, //验证前台接口权限   在  RouteServiceProvider  里面使用他
        ],

        //port  名字与 路由名字一样，会在 请求路由时，自动调用
        'invite' => [
            \App\Http\Middleware\CheckAppInviteCode::class, //验证前台接口权限   在  RouteServiceProvider  里面使用他
        ],

        //port  名字与 路由名字一样，会在 请求路由时，自动调用
        'wechat_auth' => [
            \App\Http\Middleware\CheckWechatAuth::class, //验证前台接口权限  在  RouteServiceProvider  里面使用他
        ],

        //port  名字与 路由名字一样，会在 请求路由时，自动调用
        'web_auth' => [
            \App\Http\Middleware\CheckWebAuth::class, //验证前台接口权限  在  RouteServiceProvider  里面使用他
        ],

        'limit_black_list_access' => [
            \App\Http\Middleware\LimitBlackListAccess::class, //验证前台接口权限  在  RouteServiceProvider  里面使用他
        ],

    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'password.confirm' => \Illuminate\Auth\Middleware\RequirePassword::class,
        'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,


        'check.admin.header.token' => \App\Http\Middleware\CheckAdminHeaderToken::class, //验证后台管理端登录token，只能从header头获取
        // 'check.admin.token' => \App\Http\Middleware\CheckAdminToken::class,//验证后台管理端登录token,从参数获取
        'check.admin.permission' => \App\Http\Middleware\CheckAdminPermission::class, //验证后台管理端登录权限

        'check.admin.mobile.header.token' => \App\Http\Middleware\CheckAdminMobileHeaderToken::class, //验证后台管理端登录token，只能从header头获取
        // 'check.admin.token' => \App\Http\Middleware\CheckAdminToken::class,//验证后台管理端登录token,从参数获取
        'check.admin.mobile.permission' => \App\Http\Middleware\CheckAdminMobilePermission::class, //验证后台管理端登录权限


        // 'check.web.token' => \App\Http\Middleware\CheckWebToken::class,//验证前台登录权限
        // 'check.may.web.token' => \App\Http\Middleware\CheckMayWebToken::class,//验证前台可选登录权限

        'check.wechat.token' => \App\Http\Middleware\CheckWechatToken::class, //验证微信端登录权限
        'check.may.wechat.token' => \App\Http\Middleware\CheckMayWechatToken::class, //验证微信端可选登录权限

        'check.jwt.header.token' => \App\Http\Middleware\CheckJwtHeaderToken::class, //验证前台接口权限
        'check.wechat.auth' => \App\Http\Middleware\CheckWechatAuth::class, //验证前台接口权限

        'limit.access.data' => \App\Http\Middleware\LimitAccessData::class, //活动前台访问人数 限制接口权限

        'check.bind.reader.token' => \App\Http\Middleware\CheckBindReaderToken::class, //验证前台绑定读者证和微信授权接口权限
        'check.app.invite.code' => \App\Http\Middleware\CheckAppInviteCode::class, //验证前台邀请码访问

    ];
}
