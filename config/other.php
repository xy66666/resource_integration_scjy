<?php


/**
 * 自定义其他信息
 */


return [

    //域名地址 laravel 代码在用命令行缓存时，获取不到下列数据
    // 'host_url' => isset($_SERVER['REQUEST_SCHEME']) ? ($_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['HTTP_HOST'] . str_replace('/index.php', '', $_SERVER['PHP_SELF']) . '/') : '',

    //项目名，用于拼接二维码链接
    "project_name" => 'resource_integration_scjy/',

    //图书馆名称
    'lib_name' => '简阳市图书馆',

    //独立活动显示名称
    'unit_name' => '主办方',

    //后台管理员token过期时间
    'admin_token_refresh_time' => 7200, //单位s

    //后台管理员token过期时间
    'admin_mobile_token_refresh_time' => 7 * 60 * 60 * 24, //单位s


    //程序有效期，超过这个时间段，将不能在使用
    'app_start_time' => '2023-01-01', //开始时间，记录作用，无意义
    'app_expire_time' => '2100-01-01', //如果没填就是永久
    'app_expire_msg' => '试用已过期，如需继续使用，请联系管理员处理！', //如果没填就是永久


    'md5_key' => 'c48fdec18317a7ec7e192e73a6420f85',

    'gaode_key' => '6c8bb9226fbcef660db6c3f1b36267af', //高德地图key


    // 'time_filtrate_interval_day' => 31,//时间筛选最大的天数

    // 'invite_user' => true, //是否显示邀请二维码按钮 不在这里配置，采用积分消息配置

    //是否验证图书馆接口 true 需要， false 不需要 (用于接口可有可无的情况，必须验证的接口，无需此判断)
    'is_validate_lib_api' => true, //图书到家登录接口不实用此配置，因为图书到家必须要读者证号登录接口



    //【图书到家】图书馆书籍前缀
    'lib_book_prefix' => '',

    //图书到家新书写入馆藏，固定条形码,结束判断（非一卡通）
    'new_book_fixed_barcode' => '100000001',

    //图书到家新书固定条形码前缀  01060080000001
    'new_book_barcode_prefix' => '100',


    //在线办证是否需要选择书店id
    'online_registration_is_need_shop_id' => true, //false不需要

    //邮政正式接口切换
    'postal' => 'postal_formal', //postal_test 测试    postal_formal 正式
    'is_write_postal' => false, //是否写入邮政系统，false 不写入 （测试阶段使用）


    //视频加密key
    'video_key' => 'cf30da25e9391587ed3578db254266d4',

    //是否需要积分系统
    'is_need_score' => true, //true 需要，false不需要
    'score_table' => 'user_account_lib', //积分关联表 user_account_lib 读者证表   user_info 用户信息表（用于不绑定读者证时使用） 暂时不能修改，修改要改相应代码逻辑


    'tel_key' => 'resource_integration_scjy', //手机验证码发送的key

    //跳转的二维码链接
    'qr_jump_link' => 'http://www.usharego.com', //跳转的二维码链接

    //列表返回的序号名
    'list_index_key' => 'key',

    //扫码借有效范围 单位米
    'code_borrow_valid_range' => 5000,
    //扫码签到有效范围 单位米
    'scan_sign_valid_range' => 5000,


    //排除地区限制的用户id
    'exclude_address_limit_guid' => [],


    //年度账单开启月份
    'year_bill_open_month' => ['01', '02'],

    //当前项目是小程序还是公众号
    'project_type' => 'wechat', //小程序 applet   微信公众号 wechat


    //网易云直播
    'wy_app_key' => 'ecf1a93642d3dcd799f96449d2035a1f',
    'wy_app_secket' => '58ee9fa4fa24',


    //是否发送微信模板消息
    'is_send_wechat_temp_info' => true,


    //景点打卡距离判断
    'scenic_range' => '5000', //

    //看书蛙微信公众号信息
    // 'appid' => 'wx014c953b56395cb1',
    // 'appsecret' => 'cf30da25e9391587ed3578db254266d4',

    //成都贝图科技有限公司微信公众号信息
    'appid' => 'wxb06b34c761e7ec7b',
    'appsecret' => '7afe11efc369d69a2d670ee29321adac',

    //小程序测试信息
    // 'applet_appid' => 'wx538c847e96938425',
    // 'applet_appsecret' => '05f2973513d90fdf2fc2f6374241584e',

    //测试号
    // 'appid' => 'wxde5a65d4281ac58c',
    // 'appsecret' => 'cc3f83eb3f56f241fb49b7f4ea1eec02',


    //是否微信、web端同时登录授权
    'is_web_auth' => false, //默认false


    //空间预约系统
    'access_system_secret_key' => '6e30ad9186578fad3c5d4d65ec1c8a46',
    'access_system_qr_secret_key' => 'CB13697456921',

    //推送地址
    'borrow_success' => '', //图书馆借书通知
    'return_success' => '', //图书馆还书成功通知
    'apply_success' => '', //申请成功通知
    'apply_reject' => '', //申请驳回通知
    'apply_reject1' => '', //申请驳回通知,归还失败的，用这个链接
    'renew_success' => '', //图书馆续借成功通知
    'make_inform' => '', //预约的图书到馆通知
    'activity_new_issue' => '', //活动新发布通知
    'my_activity_list' => '', //我的活动列表


    //违规配置
    'violate_config' => [
        ['field' => 'activity', 'name' => '活动报名'],
        //   ['field' => 'reservation', 'name' => '场馆预约'],
        //  ['field' => 'contest', 'name' => '在线投票'],
        //  ['field' => 'scenic', 'name' => '景点打卡'],
        //  ['field' => 'study_room_reservation', 'name' => '空间预约'],
        //   ['field' => 'competite', 'name' => '作品征集活动'],
    ], //activity  活动违规（手动违规） reservation 预约违规（手动+自动）contest 大赛违规（手动违规）scenic 景点打卡（手动违规） study_room_reservation 空间预约违规（手动+自动）

    /**
     * 全局条形码
   * 200:成功       筛选列表   无数据也是返回的  200，但是数据是空数组
   * 201:参数错误
   * 202:添加错误，修改错误等错误
   * 203:暂无数据         筛选列表   无数据也是返回的  200，但是数据是空数组
   * 204:需绑定读者证号，跳到绑定读者证号界面（读者证号）（特殊除外，会特殊说明）
   * 205:前台请求接口JWT错误 ，需重新获取Jwt      后台为登录过期，从新登录
   * 206:授权已过期，请重新授权（微信授权）    后台一般为无权限使用模块（后台一般不做处理）
   * 207:开始自定义
     *
     * 301:试用已过期！
     * 302:邀请码输入错误
     */

    //视频号直播链接，存在则显示
    'wechat_channel_link' => '',


    #############################################答题活动配置start#################################################

    //签名key
    'sign_key' => '77090290762d9d32189824e0b25520a3',

    //是否是阅享用户
    'is_ushare' => 2,  //是否阅享用户  1  是  2 否

    'answer_activity_node' => ['1', '2', '3'], // 获取活动类型  活动类型    1 独立活动  2 单位联盟   3 区域联盟

    'answer_activity_pattern' => ['1', '2', '3'], // 获取答题模式  1  馆内答题形式  2 轮次排名形式 （按轮次排名）  3 爬楼梯形式

    'answer_activity_prize_form' => ['1', '2'], // 获奖情况  1 排名  2 抽奖

    'answer_activity_gift_type' => ['2'], // 获奖礼物类型（现在只要礼物）  1 红包  2 实物

    'turn_activity_gift_type' => ['2'], // 获奖礼物类型（现在只要礼物）  1 红包  2 实物

    //活动单位性质
    'activity_unit_nature' => ['1', '2', '3'], // 单位性质   1 图书馆  2 文化馆  3 博物馆
    //大赛单位性质
    'contest_unit_nature' => ['1', '2', '3'], // 单位性质   1 图书馆  2 文化馆  3 博物馆


    #############################################答题活动配置end#################################################

    //后台存在的权限id  空表示所有
    'admin_show_auth_id' => [

        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
        27,
        28,
        29,
        30,
        31,
        32,
        33,

        200,
        201,
        202,
        203,
        204,
        205,
        206,
        207,
        208,
        209,
        210,
        211,
        212,
        213,
        214,
        215,
        216,
        217,
        218,
        219,
        220,


        //以下是每个项目必须
        589,
        590,
        591,
        592,
        593,
        596,
        598,
        601,
        602,
        // 603,//banner设置
        // 604,
        // 605,
        // 606,
        // 607,
        610,
        611,
        612,
        613,
        614,
        615,
        616,
        617,
        618,
        619,
        620,
        621,
        622,
        623,
        624,
        625,
        626,
        627,
        628
    ],
    //前台接口权限  空表示所有
    // 'wechat_port_auth' => [],
    'wechat_port_auth' => [

        'activity', //活动管理

        // 'answerActivity', //答题活动管理
        // 'drawActivity', //答题活动管理
        // 'answerActivityAnswer', //答题活动管理
        // 'answerActivityArea', //答题活动管理
        // 'answerActivityDraw', //答题活动管理
        // 'answerActivityGiftPublic', //答题活动管理
        // 'answerActivityRanking', //答题活动管理
        // 'answerActivityUnit', //答题活动管理
        // 'answerActivityUserGift', //答题活动管理
        // 'userDrawShare', //答题活动管理
        // 'userDrawAddress', //答题活动管理
        // 'userDrawInvite', //答题活动管理
        // 'userDrawUnit', //答题活动管理

        // 'bookHomeBorrow', //图书到家（新书、馆藏书记录管理）

        // 'branchInfo', //场馆管理

        // 'codeGuideProduction', //扫码导游

        // 'codeOnBorrow', //码上借

        // 'contestActivity', //线上大赛

        // 'digital', //数字资源管理

        // 'goods', //商品管理

        // 'libBookRecom', //图书到家（馆藏书籍）

        // 'navigation', //室内导航管理

        // 'newBookRecom', //图书到家（新书书籍）

        // 'news', //新闻

        // 'onlineRegistration', //线上办证

        // 'oweProcess', //欠费缴纳

        'pictureLive', //图片直播作品管理
        'pictureLiveWorks', //图片直播作品管理
        'pictureLivePersonal', //图片直播作品管理

        // 'recommendBook', //新书荐购

        // 'reservation', //预约管理（除空间预约外的其他预约）

        // 'scenic', //景点打卡
        // 'scenicWorks', //景点打卡

        // 'serviceDirectory', //服务指南

        // 'studyRoomReservation', //空间预约管理

        // 'survey', //问卷调查

        // 'turnActivity', //在线抽奖活动管理
        // 'turnActivityDraw', //在线抽奖活动管理
        // 'turnActivityUserGift', //在线抽奖活动管理

        // 'videoLive', //视频直播管理

        // 'bookRecom', //好书推荐管理

        // 'branchMap', //文旅地图

        // 'volunteer', //志愿者服务

        // 'newBookRecommend', //新书推荐（独立功能）

        // 'signScore', //积分签到
        // 'scorerule', //积分规则

        // 'competiteActivity',//征集活动
        // 'competiteActivityDatabase',//征集活动电子书
        // 'readingTask',//阅读任务

    ],
];
