<?php
/*接入邮政系统参数*/
return [
    /**图书馆地址配置废弃 */
    // 'library_address' => [
    //     'name' => '王晓明',
    //     'mobile' => '15328022656',
    //     'prov' => '四川省',
    //     'city' => '重庆市',
    //     'county' => '九龙坡区',
    //     'address' => '西郊支路19号',
    // ],
    /**下单接口 */
    'order_url' => 'https://211.156.197.233/iwaybillno-web/a/iwaybill/receive',//接口地址
    'sender_type' => 1,//客户类型  0 散户 1协议客户（默认为1）
    'logistics_provider' => 'A',//A：邮务 B：速递
    'base_product_no' => 2,//1：标准快递  2：快递包裹 3：代收/到付（标准快递）
    'biz_product_no' => null,// 参数A 加 参数2 时，这个参数必传，可以填任意内容，获取单号为97开头。 参数B 加 参数1 时，这个参数不传，获取单号为1开头
    //业务产品分类（可售卖产品代码）必须要传不传会报错，可以为null(去开头为1的单号这个字段不要)
    'ecommerce_no' => 'YZQCDBOTTLE', //渠道来源标识
    'sender_no' => '1100095581459', //协议客户代码(大客户账号),
    'sign_str' => 'key123xydJDPT',//下单接口签名秘钥
    'inner_channel' => 0, //内部订单来源标识	必填默认0：直接对接

    /**分拣码接口 */
    'sheet_url' => 'http://211.156.197.239:8086/ceshi_csb_broker',//分拣码接口地址
    'sheet_name' => 'routInfoQueryForPDD',//csb接口代码(固定)
    'wp_code' => 'YZQCDBOTTLE-YZXB', //对接系统标识,如果是快递包裹就传：XXX-YZXB,如果是标快传：XXX-EMS
    'sheet_digest' => '123', //无签名方法,但是本参数必须有且不能为空，不然会报错
    'version' => '1.0.0',//版本(固定)
    'ak' => 'dd979a77b2a44e54b65f01dcbc0bae04',//秘钥
    'sk' => 'kCZfusS4Zn300MTnYtIlWzhdoE8=',//秘钥

    /**轨迹查询接口 */
    'travel_url' => 'http://211.156.197.242:8080/querypush-gjcx/mailTrackForGjcx/queryMailTrackForGjcx/plus',//轨迹查询接口地址
    'sign_str2' => '796363326874324E85C43E8533989787',//轨迹查询接口秘钥
    'send_id' => 'YZQCDBOTTLE',//发送方标识
    'provice_no' => 99,//数据生产的省公司代码 对不能确定的省份取99(固定)
    'recieve_id' => 'JDPT', //代表接收方标识(固定)
    'bath_no' => 999, //批次号(固定)
    'data_type' => 1, //数据类型  1-JSON 2-XML 3-压缩后的Byte[](固定)
    'msg_kind' => 'YZQCDBOTTLE_JDPT_TRACE', //消息类别 主动查询(标识_JDPT_TRACE)

    /**轨迹推送接口 */
    'msg_receive_kind' => 'JDPT_YZQCDBOTTLE_TRACE' //消息类别 被动接受(JDPT_标识_TRACE)

];