/*
Navicat MySQL Data Transfer

Source Server         : 贝图项目整合系统-德阳市图书馆
Source Server Version : 50729
Source Host           : 47.110.241.53:3306
Source Database       : resource_integration_scjy_scdy

Target Server Type    : MYSQL
Target Server Version : 50729
File Encoding         : 65001

Date: 2023-11-09 13:48:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `re_access_num`
-- ----------------------------
DROP TABLE IF EXISTS `re_access_num`;
CREATE TABLE `re_access_num` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `web_num` int(10) NOT NULL DEFAULT '0' COMMENT 'web端浏览量',
  `wx_num` int(10) DEFAULT '0' COMMENT '微信端浏览量',
  `total_num` int(11) DEFAULT '0' COMMENT '总浏览量',
  `yyy` int(4) DEFAULT NULL COMMENT '年',
  `mmm` tinyint(2) DEFAULT NULL COMMENT '月',
  `ddd` tinyint(2) DEFAULT NULL COMMENT '日',
  `create_time` datetime DEFAULT NULL COMMENT '数据创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='访问统计';

-- ----------------------------
-- Records of re_access_num
-- ----------------------------
INSERT INTO `re_access_num` VALUES ('1', '0', '6', '6', '2023', '11', '9', '2023-11-09 13:34:01');

-- ----------------------------
-- Table structure for `re_activity`
-- ----------------------------
DROP TABLE IF EXISTS `re_activity`;
CREATE TABLE `re_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '活动名称',
  `img` varchar(255) DEFAULT 'default/default_activity.png' COMMENT '活动封面',
  `type_id` int(11) DEFAULT NULL COMMENT '类型id',
  `tag_id` varchar(100) DEFAULT NULL COMMENT '标签id   多个用逗号拼接',
  `apply_start_time` datetime DEFAULT NULL COMMENT '活动报名开始时间',
  `apply_end_time` datetime DEFAULT NULL COMMENT '活动报名结束时间',
  `start_time` datetime DEFAULT NULL COMMENT '活动开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '活动结束时间',
  `cancel_end_time` int(11) DEFAULT NULL COMMENT '用户可取消活动预约的最后期限(以活动开始时间往前计算）  单位分钟  0表示不限制',
  `is_long` tinyint(1) DEFAULT '2' COMMENT '是否是长期活动   1 是   2 否 （默认）',
  `everyday_start_time` time DEFAULT NULL COMMENT '是长期活动的   每日开始时间 段',
  `everyday_end_time` time DEFAULT NULL COMMENT '是长期活动的   每日结束时间 段',
  `start_age` mediumint(3) DEFAULT '0' COMMENT '开始年龄  默认0   两个都是 0 则表示不需要年龄限制',
  `end_age` mediumint(3) DEFAULT '0' COMMENT '结束年龄  默认0',
  `astrict_sex` tinyint(1) DEFAULT '3' COMMENT '性别限制   1.限男性  2.限女性  3.不限(默认)',
  `is_reader` tinyint(1) DEFAULT '2' COMMENT '报名是否需要绑定读者证  1 是  2 否',
  `is_qr` tinyint(1) DEFAULT '2' COMMENT '是否需要二维码进场   1.是   2.否(默认）',
  `is_check` tinyint(1) DEFAULT '2' COMMENT '报名是否需要审核   1.是   2.否(默认）',
  `is_continue` tinyint(1) DEFAULT '2' COMMENT '取消报名后是否可以继续报名   1.是   2.否(默认）',
  `is_real` tinyint(1) DEFAULT '2' COMMENT '是否需要用户真实信息 1需要 2不需要 默认2',
  `real_info` varchar(255) NOT NULL COMMENT '需要验证的真实信息id用|连接起来    1、姓名   2 、身份证号码  3、电话号码   4、读者证号码    5、真实人物头像   6、备注  7 单位名称 8 性别 9 年龄  10、附件 ',
  `province` varchar(20) DEFAULT NULL COMMENT '省',
  `city` varchar(20) DEFAULT NULL COMMENT '市',
  `district` varchar(20) DEFAULT NULL COMMENT '区',
  `street` varchar(30) DEFAULT NULL COMMENT '街道',
  `address` varchar(255) DEFAULT NULL COMMENT '活动地址',
  `tel` varchar(30) DEFAULT NULL COMMENT '活动联系方式',
  `contacts` varchar(30) DEFAULT NULL COMMENT '联系人',
  `number` int(11) DEFAULT '0' COMMENT '活动名额 0为不限制报名',
  `pattern` varchar(255) DEFAULT NULL COMMENT '报名方式',
  `apply_number` int(11) DEFAULT '0' COMMENT '已报名人数',
  `is_apply` tinyint(1) DEFAULT '1' COMMENT '是否需要报名 1.是  2.否  默认1  否：参与人数可不写',
  `objects` tinyint(1) DEFAULT '1' COMMENT '参赛对象  1个人，2团队  默认1',
  `intro` text COMMENT '活动内容',
  `stadium_id` int(11) DEFAULT NULL COMMENT '场馆id',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员',
  `browse_num` int(11) DEFAULT '0' COMMENT '浏览量  默认0',
  `lon` varchar(20) DEFAULT NULL COMMENT '经度',
  `lat` varchar(20) DEFAULT NULL COMMENT '纬度',
  `is_play` tinyint(1) DEFAULT '1' COMMENT '是否发布 1.发布 2.未发布    默认1',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1.正常  2.已删除  默认1',
  `create_time` datetime DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime DEFAULT NULL COMMENT '新增时间',
  `qr_url` varchar(255) DEFAULT NULL COMMENT '活动二维码引用地址',
  `qr_code` varchar(10) DEFAULT NULL COMMENT '二维码 code',
  `is_recom` tinyint(1) DEFAULT '2' COMMENT '是否推荐为 热门活动  1 是  2 否 （默认）',
  `push_num` int(1) DEFAULT '0' COMMENT '发布次数',
  `link` varchar(255) DEFAULT NULL COMMENT '跳转外链',
  `sign_way` tinyint(1) DEFAULT '2' COMMENT '扫码方式  1 、2者都可以  2、用户自主扫码  3、管理员设备扫码  ',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='活动表';

-- ----------------------------
-- Records of re_activity
-- ----------------------------

-- ----------------------------
-- Table structure for `re_activity_apply`
-- ----------------------------
DROP TABLE IF EXISTS `re_activity_apply`;
CREATE TABLE `re_activity_apply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证号id',
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `status` tinyint(2) DEFAULT '0' COMMENT '报名状态    1.已通过  2.已取消  3.已拒绝  4.审核中',
  `reason` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '拒绝理由',
  `sign_num` int(11) DEFAULT '0' COMMENT '签到次数  默认0    如果是短期活动  签到一次就表示已签到',
  `username` varchar(30) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '姓名',
  `id_card` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '身份证',
  `tel` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '电话',
  `reader_id` varchar(30) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '读者证',
  `img` varchar(120) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '头像',
  `is_violate` tinyint(1) DEFAULT '1' COMMENT '是否违规  1正常  2 违规',
  `violate_reason` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '违规原因',
  `violate_time` datetime DEFAULT NULL COMMENT '违规时间',
  `score` int(10) DEFAULT '0' COMMENT '积分信息',
  `accessory` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '附件  ',
  `accessory_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '附件名',
  `remark` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注，最多100字',
  `unit` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  `sex` tinyint(1) DEFAULT '3' COMMENT '性别  1男  2女',
  `manage_id` int(11) DEFAULT NULL COMMENT '操作管理员id',
  `create_time` datetime DEFAULT NULL COMMENT '报名时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT COMMENT='活动报名表';

-- ----------------------------
-- Records of re_activity_apply
-- ----------------------------

-- ----------------------------
-- Table structure for `re_activity_collect`
-- ----------------------------
DROP TABLE IF EXISTS `re_activity_collect`;
CREATE TABLE `re_activity_collect` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='活动收藏表';

-- ----------------------------
-- Records of re_activity_collect
-- ----------------------------

-- ----------------------------
-- Table structure for `re_activity_signs`
-- ----------------------------
DROP TABLE IF EXISTS `re_activity_signs`;
CREATE TABLE `re_activity_signs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `apply_id` int(11) DEFAULT NULL COMMENT '申请id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `date` date DEFAULT NULL COMMENT '报名时期 年月日',
  `yyy` smallint(4) DEFAULT NULL COMMENT '年',
  `mmm` tinyint(2) DEFAULT NULL COMMENT '月',
  `ddd` tinyint(2) DEFAULT NULL COMMENT '日',
  `create_time` datetime DEFAULT NULL COMMENT '数据插入时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT COMMENT='活动用户签到表';

-- ----------------------------
-- Records of re_activity_signs
-- ----------------------------

-- ----------------------------
-- Table structure for `re_activity_tag`
-- ----------------------------
DROP TABLE IF EXISTS `re_activity_tag`;
CREATE TABLE `re_activity_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(30) DEFAULT NULL COMMENT '标签名称',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除 1.正常 2.删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='活动标签表';

-- ----------------------------
-- Records of re_activity_tag
-- ----------------------------

-- ----------------------------
-- Table structure for `re_activity_type`
-- ----------------------------
DROP TABLE IF EXISTS `re_activity_type`;
CREATE TABLE `re_activity_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(30) DEFAULT NULL COMMENT '活动类型名称',
  `is_del` tinyint(255) DEFAULT '1' COMMENT '是否删除 1.正常 2.删除',
  `create_time` datetime DEFAULT NULL COMMENT '数据插入时间',
  `change_time` datetime DEFAULT NULL COMMENT '数据上次更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='活动类型表';

-- ----------------------------
-- Records of re_activity_type
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity`;
CREATE TABLE `re_answer_activity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL COMMENT '标题',
  `node` tinyint(2) DEFAULT '1' COMMENT '活动类型    1 独立活动  2 单位联盟   3 区域联盟   ',
  `pattern` tinyint(2) DEFAULT NULL COMMENT '答题模式  1  馆内答题形式  2 轮次排名形式 （按轮次排名）  3 爬楼梯形式',
  `prize_form` tinyint(1) DEFAULT '1' COMMENT '获奖情况  1 排名  2 抽奖',
  `is_ushare` tinyint(1) DEFAULT '1' COMMENT '是否阅享用户  1  是  2 否',
  `real_info` varchar(100) DEFAULT NULL COMMENT '填写信息  如果是阅享用户，则无效    1、姓名  2、电话   3、身份证号码  4、读者证   多个  | 连接',
  `is_need_unit` tinyint(1) DEFAULT '2' COMMENT '是否需要选择图书馆   1 需要  2 不需要 （单活动模式，默认不需要）',
  `img` varchar(120) DEFAULT 'default/default_readshare_activity.png',
  `total_floor` int(11) DEFAULT NULL COMMENT '爬楼梯梯模式，总楼层',
  `number` int(11) DEFAULT '1' COMMENT '每日答题次数    除轮次外 （一轮为一次，不管有多少题），其余一道题就是一次',
  `answer_number` int(11) DEFAULT '1' COMMENT '除轮次排名外（每次多少题）,单纯排名不需要此参数，其余都是多少题获得一次抽奖机会，馆内答题就是馆内答题数量',
  `is_loop` tinyint(1) DEFAULT '0' COMMENT '是否可以循环，只对 场馆答题 有效    1可以   2 不可以',
  `is_show_list` tinyint(1) DEFAULT '1' COMMENT '是否显示列表 1显示 2 不显示  单位联盟 是否显示单位列表  区域联盟 是否显示区域和单位列表',
  `share_number` int(11) DEFAULT NULL COMMENT '分享获取答题次数，每日几次',
  `answer_time` int(11) DEFAULT '0' COMMENT '每题答题时间   单位 秒',
  `answer_rule` tinyint(1) DEFAULT '1' COMMENT '答题规则   1、专属题和公共题 混合  2、优先回答专属题',
  `rule_type` tinyint(1) DEFAULT NULL COMMENT '规则方式   1 内容   2外链',
  `rule` text COMMENT '规则内容     rule_type  为1 这里是编辑器内容   2 这里是链接',
  `start_time` datetime DEFAULT NULL COMMENT '活动开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '活动结束时间',
  `answer_start_time` datetime DEFAULT NULL COMMENT '开始答题时间',
  `answer_end_time` datetime DEFAULT NULL COMMENT '结束答题时间',
  `lottery_start_time` datetime DEFAULT NULL COMMENT '抽奖开始时间',
  `lottery_end_time` datetime DEFAULT NULL COMMENT '抽奖结束时间',
  `invite_code` varchar(8) DEFAULT NULL COMMENT '邀请码',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除   1 正常 2 已删除',
  `is_play` tinyint(1) DEFAULT '2' COMMENT '是否发布   1 已发布  2 未发布',
  `manage_id` int(11) DEFAULT NULL,
  `browse_num` int(10) DEFAULT '0' COMMENT '浏览量',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL,
  `qr_code` varchar(10) DEFAULT NULL,
  `qr_url` varchar(120) DEFAULT NULL,
  `is_show_small_rank` tinyint(1) DEFAULT '2' COMMENT '排名活动，是否显示分榜排名   1 显示  2 不显示',
  `share_title` varchar(20) DEFAULT NULL COMMENT '分享标题',
  `share_img` varchar(120) DEFAULT NULL COMMENT '分享图片',
  `is_show_address` tinyint(1) DEFAULT '1' COMMENT '是否需要填写  1 需要  2 不需要',
  `technical_support` varchar(30) DEFAULT '成都贝图科技有限公司' COMMENT '技术支持,如果不填，则前端不显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='阅享竞答活动';

-- ----------------------------
-- Records of re_answer_activity
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_area`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_area`;
CREATE TABLE `re_answer_activity_area` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL,
  `name` varchar(10) DEFAULT NULL COMMENT '区域名称',
  `img` varchar(120) DEFAULT NULL COMMENT '区域名称',
  `intro` text COMMENT '区域简介',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除   1 正常 2 已删除',
  `order` int(11) DEFAULT '0' COMMENT '排序  数字越小越靠前    1在最前面',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='活动区域表';

-- ----------------------------
-- Records of re_answer_activity_area
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_browse_count`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_browse_count`;
CREATE TABLE `re_answer_activity_browse_count` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL,
  `unit_id` int(11) DEFAULT '0' COMMENT '单位id   0 代表全部',
  `date` date DEFAULT NULL,
  `hour` varchar(2) DEFAULT NULL COMMENT '小时',
  `number` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='活动浏览量统计表';

-- ----------------------------
-- Records of re_answer_activity_browse_count
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_gift`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_gift`;
CREATE TABLE `re_answer_activity_gift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `unit_id` int(11) DEFAULT NULL COMMENT '单位id   无单位表示公共奖品',
  `name` varchar(50) DEFAULT NULL COMMENT '礼品名称',
  `img` varchar(150) DEFAULT NULL COMMENT '礼品图片',
  `intro` varchar(100) DEFAULT NULL COMMENT '礼品简介',
  `total_number` int(11) DEFAULT NULL COMMENT '礼品总数量',
  `use_number` int(11) DEFAULT '0' COMMENT '礼品已被抽中数量',
  `percent` decimal(6,2) DEFAULT NULL COMMENT '中奖率(百分比)',
  `price` decimal(6,2) DEFAULT NULL COMMENT '红包大小',
  `type` tinyint(1) DEFAULT NULL COMMENT '1文化红包 2精美礼品',
  `way` tinyint(1) DEFAULT '0' COMMENT '领取方式   1 自提  2邮递   红包无此选项',
  `start_time` datetime DEFAULT NULL COMMENT '自提开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '自提结束时间',
  `tel` varchar(15) DEFAULT NULL COMMENT '联系电话',
  `contacts` varchar(30) DEFAULT NULL COMMENT '联系人',
  `province` varchar(20) DEFAULT NULL COMMENT '省',
  `city` varchar(20) DEFAULT NULL COMMENT '市',
  `district` varchar(20) DEFAULT NULL COMMENT '区',
  `address` varchar(255) DEFAULT NULL COMMENT '详细地址',
  `remark` text COMMENT '自提备注',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否被删除 1正常 2已删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `unit_index` (`unit_id`) USING BTREE,
  KEY `activity_index` (`act_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='礼物表';

-- ----------------------------
-- Records of re_answer_activity_gift
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_gift_config`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_gift_config`;
CREATE TABLE `re_answer_activity_gift_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `percent_way` tinyint(1) DEFAULT '1' COMMENT '概率方式   1 单个奖品独立概率方式    2 所以奖品总概率形式',
  `gift_max_number_day` int(11) DEFAULT NULL COMMENT '礼物每日发送奖品最大数量',
  `red_max_number_day` int(11) DEFAULT NULL COMMENT '礼物每日发送奖品最大数量',
  `user_gift_number_day` int(11) DEFAULT '1' COMMENT '用户当日可获得多少实物礼品',
  `user_red_number_day` int(11) DEFAULT '1' COMMENT '用户当日可获得多少红包礼品',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='礼品领取总配置表';

-- ----------------------------
-- Records of re_answer_activity_gift_config
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_gift_public`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_gift_public`;
CREATE TABLE `re_answer_activity_gift_public` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `unit_id` int(11) DEFAULT NULL COMMENT '单位id   无单位表示公共奖品',
  `type` tinyint(1) DEFAULT NULL COMMENT '礼物类型  1文化红包 2精美礼品',
  `name` varchar(50) DEFAULT NULL COMMENT '礼品名称',
  `way` tinyint(1) DEFAULT NULL COMMENT '操作方式  1 投放  2追加',
  `number` int(11) DEFAULT NULL COMMENT '礼品数量',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否被删除 1正常 2已删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `unit_index` (`unit_id`) USING BTREE,
  KEY `activity_index` (`act_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='礼物公示表';

-- ----------------------------
-- Records of re_answer_activity_gift_public
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_gift_record`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_gift_record`;
CREATE TABLE `re_answer_activity_gift_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `unit_id` int(11) DEFAULT NULL COMMENT '单位id',
  `gift_id` int(11) DEFAULT NULL COMMENT '礼物id',
  `before_number` int(11) DEFAULT '0' COMMENT '最加之前数量    ',
  `later_number` int(11) DEFAULT '0' COMMENT '追加之后数量',
  `gift_number` int(11) DEFAULT '0' COMMENT '礼物数量',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态 1 增加 2减少',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员id',
  `create_time` datetime DEFAULT NULL COMMENT '记录时间',
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `activity_index` (`act_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='礼物变动记录表';

-- ----------------------------
-- Records of re_answer_activity_gift_record
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_gift_time`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_gift_time`;
CREATE TABLE `re_answer_activity_gift_time` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `start_time` datetime DEFAULT NULL COMMENT '奖品开始发放时间   ',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间   和开始时间对应不允许跨天',
  `type` tinyint(4) DEFAULT NULL COMMENT '奖品类型 1文化红包 2精美礼品',
  `number` int(11) DEFAULT NULL COMMENT '当前时间段发放数量',
  `user_gift_number` int(11) DEFAULT '1' COMMENT '用户当前时间段可获得多少礼品',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除 1正常 2删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='礼品领取排班表';

-- ----------------------------
-- Records of re_answer_activity_gift_time
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_invite`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_invite`;
CREATE TABLE `re_answer_activity_invite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `user_guid` varchar(32) DEFAULT NULL,
  `invite_code` varchar(10) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='活动邀请码';

-- ----------------------------
-- Records of re_answer_activity_invite
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_limit_address`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_limit_address`;
CREATE TABLE `re_answer_activity_limit_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '每个活动独立',
  `province` varchar(50) DEFAULT NULL COMMENT '省',
  `city` varchar(50) DEFAULT NULL COMMENT '市',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1 正常  2删除',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime DEFAULT NULL COMMENT '删除、修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='答题活动限制参与地址';

-- ----------------------------
-- Records of re_answer_activity_limit_address
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_problem`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_problem`;
CREATE TABLE `re_answer_activity_problem` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `unit_id` int(11) DEFAULT NULL COMMENT '题目所属单位   没有代表是公共题库',
  `title` varchar(300) DEFAULT NULL COMMENT '标题',
  `img` varchar(150) DEFAULT NULL COMMENT '题目图片',
  `answer_id` int(10) DEFAULT NULL COMMENT '正确答案id',
  `type` tinyint(1) DEFAULT '1' COMMENT '题目类型  1 单选 （默认）2 填空题',
  `state` tinyint(1) DEFAULT '1' COMMENT '1正常 2被禁用',
  `analysis` varchar(300) DEFAULT NULL COMMENT '解析',
  `is_show_analysis` tinyint(1) DEFAULT '2' COMMENT '是否显示解析  1、显示 2、不显示',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否被删除 1正常 2已删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `unit_index` (`unit_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='活动题目表\r\n';

-- ----------------------------
-- Records of re_answer_activity_problem
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_problem_answer`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_problem_answer`;
CREATE TABLE `re_answer_activity_problem_answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pro_id` int(10) DEFAULT NULL COMMENT '标题id',
  `content` varchar(1000) DEFAULT NULL COMMENT '答案内容',
  `status` tinyint(2) DEFAULT '2' COMMENT '答题状态  1代表正确  2代表错误  默认2',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `title_index` (`pro_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='活动答案表\r\n';

-- ----------------------------
-- Records of re_answer_activity_problem_answer
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_resource`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_resource`;
CREATE TABLE `re_answer_activity_resource` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '标题',
  `theme_color` varchar(10) DEFAULT NULL COMMENT '主题色',
  `progress_color` varchar(10) DEFAULT NULL COMMENT '进度条颜色',
  `invite_color` varchar(10) DEFAULT NULL COMMENT '邀请码颜色',
  `theme_text_color` varchar(10) DEFAULT NULL COMMENT '主题文字色',
  `warning_color` varchar(10) DEFAULT NULL COMMENT '警告颜色',
  `error_color` varchar(10) DEFAULT NULL COMMENT '错误颜色',
  `resource_path` varchar(100) DEFAULT NULL COMMENT '资源路径，中间半截',
  `is_change` tinyint(1) DEFAULT '2' COMMENT '资源是否变化，包括图片资源是否改变  1 未变化  2 有变化',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='活动资源表';

-- ----------------------------
-- Records of re_answer_activity_resource
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_share`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_share`;
CREATE TABLE `re_answer_activity_share` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL,
  `user_guid` varchar(32) DEFAULT NULL COMMENT '区域名称',
  `date` date DEFAULT NULL COMMENT '日期',
  `number` int(11) DEFAULT '0' COMMENT '分享次数',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='活动分享次数表';

-- ----------------------------
-- Records of re_answer_activity_share
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_unit`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_unit`;
CREATE TABLE `re_answer_activity_unit` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL COMMENT '区域id  若区域id存在，则每个区域下的单位唯一，若区域不存在，则整个活动下的单位唯一',
  `nature` tinyint(2) DEFAULT NULL COMMENT '单位性质   1 图书馆  2 文化馆  3 博物馆',
  `name` varchar(50) DEFAULT NULL COMMENT '单位名称',
  `img` varchar(120) DEFAULT 'default/default_readshare_unit.png',
  `intro` text COMMENT '单位简介',
  `tel` varchar(15) DEFAULT NULL COMMENT '联系电话',
  `contacts` varchar(50) DEFAULT NULL COMMENT '联系人姓名',
  `words` varchar(500) DEFAULT NULL COMMENT '单位赠言，最多50字',
  `letter` varchar(1) DEFAULT NULL COMMENT '首字母',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除   1 正常 2 已删除',
  `order` int(11) DEFAULT '0' COMMENT '排序  数字越小越靠前    1在最前面',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL,
  `qr_code` varchar(10) DEFAULT NULL,
  `qr_url` varchar(200) DEFAULT NULL COMMENT '二维码链接',
  `is_default` tinyint(1) DEFAULT '2' COMMENT '是否默认 1 默认 2 不默认',
  `province` varchar(20) DEFAULT NULL COMMENT '省',
  `city` varchar(20) DEFAULT NULL COMMENT '市',
  `district` varchar(20) DEFAULT NULL COMMENT '区',
  `address` varchar(255) DEFAULT NULL COMMENT '活动地址',
  `lon` varchar(20) DEFAULT NULL COMMENT '经度',
  `lat` varchar(20) DEFAULT NULL COMMENT '纬度',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='活动单位表';

-- ----------------------------
-- Records of re_answer_activity_unit
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_unit_user_number`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_unit_user_number`;
CREATE TABLE `re_answer_activity_unit_user_number` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `act_id` int(10) DEFAULT NULL COMMENT '活动id',
  `user_guid` char(32) DEFAULT NULL,
  `unit_id` int(11) DEFAULT '0' COMMENT '单位id',
  `date` date DEFAULT NULL,
  `number` int(11) DEFAULT '0' COMMENT '次数',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `act_id` (`act_id`) USING BTREE,
  KEY `user_guid` (`user_guid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户回答单位题目 次数表（3种模式都在里面）\r\n';

-- ----------------------------
-- Records of re_answer_activity_unit_user_number
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_user_answer_record`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_user_answer_record`;
CREATE TABLE `re_answer_activity_user_answer_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `guid` char(32) DEFAULT NULL COMMENT '每次抽区问题时随机id',
  `user_guid` char(32) DEFAULT NULL,
  `act_id` int(10) DEFAULT NULL COMMENT '标题id',
  `unit_id` int(11) DEFAULT '0' COMMENT '单位id',
  `problem_id` int(11) DEFAULT NULL COMMENT '问题id',
  `answer_id` int(11) DEFAULT NULL COMMENT '答案id',
  `answer` varchar(100) DEFAULT NULL COMMENT '填空题答案',
  `status` tinyint(2) DEFAULT '0' COMMENT '答题状态  1代表正确  2代表错误   3 超时回答（算错误）默认2',
  `times` int(11) DEFAULT '0' COMMENT '答题所需时长 单位秒',
  `create_time` datetime DEFAULT NULL,
  `expire_time` datetime DEFAULT NULL COMMENT '回答问题超时时间  后台处理问题，允许 2s的延时',
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `act_id` (`act_id`) USING BTREE,
  KEY `user_guid` (`user_guid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户答题记录表（馆内形式答题）\r\n';

-- ----------------------------
-- Records of re_answer_activity_user_answer_record
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_user_answer_total_number`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_user_answer_total_number`;
CREATE TABLE `re_answer_activity_user_answer_total_number` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_guid` char(32) DEFAULT NULL COMMENT '用户id',
  `act_id` int(10) DEFAULT NULL COMMENT '活动id',
  `unit_id` int(10) DEFAULT '0' COMMENT '单位id  如果 是 0则代表总排名  不是则是馆排名',
  `answer_number` int(10) DEFAULT '0' COMMENT '应该答题总数量 默认 0',
  `correct_number` int(10) DEFAULT '0' COMMENT '答题正确数量  默认 0',
  `accuracy` decimal(5,2) DEFAULT '0.00' COMMENT '答题正确率   百分数',
  `times` int(11) DEFAULT '0' COMMENT '答题所需时长 单位秒',
  `create_time` datetime DEFAULT NULL COMMENT '选题时间',
  `change_time` datetime DEFAULT NULL COMMENT '最后一次答题时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `act_id` (`act_id`) USING BTREE,
  KEY `user_guid` (`user_guid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户答题排名表（馆内形式答题）\r\n';

-- ----------------------------
-- Records of re_answer_activity_user_answer_total_number
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_user_correct_answer_record`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_user_correct_answer_record`;
CREATE TABLE `re_answer_activity_user_correct_answer_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `correct_number_id` int(11) DEFAULT NULL COMMENT '竞答次数id',
  `user_guid` char(32) DEFAULT NULL,
  `guid` char(32) DEFAULT NULL COMMENT '每次抽区问题时随机id',
  `act_id` int(10) DEFAULT NULL COMMENT '标题id',
  `unit_id` int(11) DEFAULT '0',
  `level` int(11) DEFAULT '1' COMMENT '第几题',
  `problem_id` int(11) DEFAULT NULL COMMENT '问题id',
  `answer_id` int(11) DEFAULT NULL COMMENT '答案id',
  `answer` varchar(100) DEFAULT NULL COMMENT '填空题答案',
  `status` tinyint(2) DEFAULT '0' COMMENT '答题状态  1代表正确  2代表错误   3 超时回答（算错误）默认2',
  `times` int(11) DEFAULT '0' COMMENT '答题所需时长 单位秒',
  `create_time` datetime DEFAULT NULL,
  `expire_time` datetime DEFAULT NULL COMMENT '回答问题超时时间  后台处理问题，允许 2s的延时',
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `act_id` (`act_id`) USING BTREE,
  KEY `user_guid` (`user_guid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户答题记录表（竞答（轮次记录）形式答题）\r\n';

-- ----------------------------
-- Records of re_answer_activity_user_correct_answer_record
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_user_correct_number`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_user_correct_number`;
CREATE TABLE `re_answer_activity_user_correct_number` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_guid` char(32) DEFAULT NULL COMMENT '用户id',
  `act_id` int(10) DEFAULT NULL COMMENT '活动id',
  `unit_id` int(11) DEFAULT '0',
  `date` date DEFAULT NULL COMMENT '年',
  `answer_number` int(10) DEFAULT '0' COMMENT '应该答题总数量 默认 0',
  `have_answer_number` int(10) DEFAULT '0' COMMENT '总共已答题目数量  默认 0',
  `correct_number` int(10) DEFAULT '0' COMMENT '答题正确数量  默认 0',
  `accuracy` decimal(5,2) DEFAULT '0.00' COMMENT '答题正确率   百分数',
  `status` tinyint(1) DEFAULT '0' COMMENT '是否回复 0 未回复  1 已回复',
  `times` int(11) DEFAULT '0' COMMENT '答题所需时长 单位秒',
  `create_time` datetime DEFAULT NULL COMMENT '选题时间',
  `expire_time` datetime DEFAULT NULL COMMENT '总过期时间，针对用户中途放弃答题的情况   在所有时间上增加  30秒，用户延时消耗',
  `change_time` datetime DEFAULT NULL COMMENT '最后一次答题时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_guid` (`user_guid`) USING BTREE,
  KEY `act_id` (`act_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户竞答轮次表（一轮一条记录）\r\n';

-- ----------------------------
-- Records of re_answer_activity_user_correct_number
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_user_correct_total_number`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_user_correct_total_number`;
CREATE TABLE `re_answer_activity_user_correct_total_number` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_guid` char(32) DEFAULT NULL COMMENT '用户id',
  `act_id` int(10) DEFAULT NULL COMMENT '活动id',
  `unit_id` int(10) DEFAULT '0' COMMENT '单位id  如果 是 0则代表总排名  不是则是馆排名',
  `answer_number` int(10) DEFAULT '0' COMMENT '应该答题总数量 默认 0',
  `correct_number` int(10) DEFAULT '0' COMMENT '答题正确数量  默认 0   取一天中的最高的一次来计算',
  `accuracy` decimal(5,2) DEFAULT '0.00' COMMENT '答题正确率   百分数',
  `times` int(11) DEFAULT '0' COMMENT '答题所需时长 单位秒',
  `create_time` datetime DEFAULT NULL COMMENT '选题时间',
  `change_time` datetime DEFAULT NULL COMMENT '最后一次答题时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `act_id` (`act_id`) USING BTREE,
  KEY `user_guid` (`user_guid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户竞答总数据（取一天中的最高的一轮来计算）表\r\n';

-- ----------------------------
-- Records of re_answer_activity_user_correct_total_number
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_user_gift`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_user_gift`;
CREATE TABLE `re_answer_activity_user_gift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `user_guid` varchar(32) DEFAULT '' COMMENT '用户guid',
  `gift_id` int(11) DEFAULT NULL COMMENT '礼物id',
  `order_id` varchar(50) DEFAULT '' COMMENT '订单号',
  `use_prize_record_id` int(11) DEFAULT NULL COMMENT '用户使用抽奖机会id',
  `price` decimal(6,2) DEFAULT NULL COMMENT '红包大小',
  `state` tinyint(1) DEFAULT '1' COMMENT '礼物状态 1未发放 2已发放 3未发货 4邮递中 5已到货 6 未领取  7 已领取',
  `date` date DEFAULT NULL COMMENT '获奖日期',
  `hour` varchar(2) DEFAULT NULL COMMENT '小时',
  `type` tinyint(1) DEFAULT NULL COMMENT '1文化红包 2精美礼品',
  `create_time` datetime DEFAULT NULL COMMENT '红包创建时间',
  `pay_time` datetime DEFAULT NULL COMMENT '红包到账时间',
  `change_time` datetime DEFAULT NULL COMMENT '到货（领取、发货）时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `act_id` (`act_id`) USING BTREE,
  KEY `user_guid` (`user_guid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='用户获取礼物表';

-- ----------------------------
-- Records of re_answer_activity_user_gift
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_user_prize`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_user_prize`;
CREATE TABLE `re_answer_activity_user_prize` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `user_guid` varchar(32) DEFAULT '' COMMENT '用户guid',
  `number` int(11) DEFAULT '0' COMMENT '总次数',
  `use_number` int(11) DEFAULT '0' COMMENT '剩余次数',
  `create_time` datetime DEFAULT NULL COMMENT '红包创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '到货时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `act_id` (`act_id`) USING BTREE,
  KEY `user_guid` (`user_guid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='用户抽奖机会总表';

-- ----------------------------
-- Records of re_answer_activity_user_prize
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_user_prize_record`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_user_prize_record`;
CREATE TABLE `re_answer_activity_user_prize_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `pattern` tinyint(1) DEFAULT NULL COMMENT '答题模式（获奖方式）  1  馆内答题形式    3 爬楼梯形式',
  `answer_record_id` int(11) DEFAULT NULL COMMENT '对应答题记录id',
  `user_guid` varchar(32) DEFAULT '' COMMENT '用户guid',
  `create_time` datetime DEFAULT NULL COMMENT '红包创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '到货时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `act_id` (`act_id`) USING BTREE,
  KEY `user_guid` (`user_guid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='用户获取抽奖机会记录表';

-- ----------------------------
-- Records of re_answer_activity_user_prize_record
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_user_stairs_answer_record`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_user_stairs_answer_record`;
CREATE TABLE `re_answer_activity_user_stairs_answer_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `guid` char(32) DEFAULT NULL COMMENT '每次抽区问题时随机id',
  `user_guid` char(32) DEFAULT NULL,
  `act_id` int(10) DEFAULT NULL COMMENT '标题id',
  `unit_id` int(11) DEFAULT '0',
  `floor` int(11) DEFAULT '1' COMMENT '楼层  ',
  `problem_id` int(11) DEFAULT NULL COMMENT '问题id',
  `answer_id` int(11) DEFAULT NULL COMMENT '答案id',
  `answer` varchar(100) DEFAULT NULL COMMENT '填空题答案',
  `status` tinyint(2) DEFAULT '0' COMMENT '答题状态  1代表正确  2代表错误   3 超时回答（算错误）默认2',
  `times` int(11) DEFAULT '0' COMMENT '答题所需时长 单位秒',
  `create_time` datetime DEFAULT NULL,
  `expire_time` datetime DEFAULT NULL COMMENT '回答问题超时时间  后台处理问题，允许 2s的延时',
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `act_id` (`act_id`) USING BTREE,
  KEY `user_guid` (`user_guid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户答题记录表（楼梯形式答题）\r\n';

-- ----------------------------
-- Records of re_answer_activity_user_stairs_answer_record
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_user_stairs_total_number`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_user_stairs_total_number`;
CREATE TABLE `re_answer_activity_user_stairs_total_number` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_guid` char(32) DEFAULT NULL COMMENT '用户id',
  `act_id` int(10) DEFAULT NULL COMMENT '活动id',
  `unit_id` int(10) DEFAULT '0' COMMENT '单位id  如果 是 0则代表总排名  不是则是馆排名',
  `answer_number` int(10) DEFAULT '0' COMMENT '应该答题总数量 默认 0',
  `correct_number` int(10) DEFAULT '0' COMMENT '答题正确数量（对一次就是一层）  默认 0',
  `accuracy` decimal(5,2) DEFAULT '0.00' COMMENT '答题正确率   百分数',
  `times` int(11) DEFAULT '0' COMMENT '答题所需时长 单位秒',
  `create_time` datetime DEFAULT NULL COMMENT '选题时间',
  `change_time` datetime DEFAULT NULL COMMENT '最后一次答题时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `act_id` (`act_id`) USING BTREE,
  KEY `user_guid` (`user_guid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户答题总楼层表（楼梯形式答题）\r\n';

-- ----------------------------
-- Records of re_answer_activity_user_stairs_total_number
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_user_unit`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_user_unit`;
CREATE TABLE `re_answer_activity_user_unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_guid` char(32) DEFAULT NULL COMMENT '用户id',
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `username` varchar(50) DEFAULT NULL COMMENT '姓名',
  `tel` varchar(20) DEFAULT NULL COMMENT '电话号码',
  `id_card` varchar(20) DEFAULT NULL COMMENT '身份证',
  `reader_id` varchar(20) DEFAULT NULL COMMENT '读者证',
  `unit_id` int(11) DEFAULT NULL COMMENT '单位id',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime DEFAULT NULL COMMENT '删除、修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户所属单位表';

-- ----------------------------
-- Records of re_answer_activity_user_unit
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_user_use_prize_record`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_user_use_prize_record`;
CREATE TABLE `re_answer_activity_user_use_prize_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(18) DEFAULT NULL COMMENT '订单号',
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `user_guid` varchar(32) DEFAULT '' COMMENT '用户guid',
  `status` tinyint(1) DEFAULT NULL COMMENT '是否中奖  1 中奖  2 未中奖',
  `date` date DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '红包创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '到货时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `act_id` (`act_id`) USING BTREE,
  KEY `user_guid` (`user_guid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='用户使用抽奖机会表';

-- ----------------------------
-- Records of re_answer_activity_user_use_prize_record
-- ----------------------------

-- ----------------------------
-- Table structure for `re_answer_activity_word_resource`
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_word_resource`;
CREATE TABLE `re_answer_activity_word_resource` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '标题',
  `invitation_prompt` varchar(50) DEFAULT NULL COMMENT '邀请码提示',
  `relevant_information` varchar(100) DEFAULT NULL COMMENT '所属馆填写信息',
  `unit_detail_answer` varchar(50) DEFAULT NULL COMMENT '单位详情答题数提示',
  `stair_display` varchar(50) DEFAULT NULL COMMENT '楼梯显示数据',
  `answer_success1` varchar(50) DEFAULT NULL COMMENT '答题成功2',
  `answer_success2` varchar(50) DEFAULT NULL COMMENT '答题成功3',
  `answer_success3` varchar(50) DEFAULT NULL COMMENT '答题成功4',
  `answer_error1` varchar(50) DEFAULT NULL COMMENT '答题错误2',
  `answer_error2` varchar(50) DEFAULT NULL COMMENT '答题错误3',
  `answer_error3` varchar(50) DEFAULT NULL COMMENT '答题错误4',
  `answer_error4` tinyint(1) DEFAULT '2' COMMENT '是否显示解析  1、显示 2、不显示',
  `grade1` varchar(50) DEFAULT NULL COMMENT '我的成绩1',
  `grade2` varchar(50) DEFAULT NULL COMMENT '我的成绩2',
  `grade3` varchar(50) DEFAULT NULL COMMENT '我的成绩3',
  `ranking1` varchar(50) DEFAULT NULL COMMENT '排名1',
  `ranking2` varchar(50) DEFAULT NULL COMMENT '排名2',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='活动资源表';

-- ----------------------------
-- Records of re_answer_activity_word_resource
-- ----------------------------

-- ----------------------------
-- Table structure for `re_app_invite_code`
-- ----------------------------
DROP TABLE IF EXISTS `re_app_invite_code`;
CREATE TABLE `re_app_invite_code` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT '备注信息',
  `content` varchar(6) DEFAULT NULL COMMENT '邀请码',
  `intro` text COMMENT '备注',
  `start_time` datetime DEFAULT NULL COMMENT '有效期开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '有效期结束时间',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除 1 正常  2删除',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='应用邀请码';

-- ----------------------------
-- Records of re_app_invite_code
-- ----------------------------

-- ----------------------------
-- Table structure for `re_app_invite_code_behavior`
-- ----------------------------
DROP TABLE IF EXISTS `re_app_invite_code_behavior`;
CREATE TABLE `re_app_invite_code_behavior` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` tinyint(3) DEFAULT NULL COMMENT '类型id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `invite_code_id` int(11) DEFAULT NULL COMMENT '邀请码id',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='应用邀请码用户行为分析';

-- ----------------------------
-- Records of re_app_invite_code_behavior
-- ----------------------------

-- ----------------------------
-- Table structure for `re_app_view_gray`
-- ----------------------------
DROP TABLE IF EXISTS `re_app_view_gray`;
CREATE TABLE `re_app_view_gray` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `manage_id` int(11) DEFAULT NULL COMMENT '配置的管理员id',
  `start_time` datetime DEFAULT NULL COMMENT '清除违规时间，在此之前的违规都不算，为空，则都计算在内',
  `end_time` datetime DEFAULT NULL COMMENT '清除违规时间，在此之前的违规都不算，为空，则都计算在内',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1 正常 2已删除',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='应用显示灰色表';

-- ----------------------------
-- Records of re_app_view_gray
-- ----------------------------

-- ----------------------------
-- Table structure for `re_banner`
-- ----------------------------
DROP TABLE IF EXISTS `re_banner`;
CREATE TABLE `re_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL COMMENT 'banner名称',
  `img` varchar(255) DEFAULT NULL COMMENT 'Bannner图片引用地址',
  `link` varchar(255) DEFAULT NULL COMMENT 'banner的跳转链接',
  `type` tinyint(2) DEFAULT '1' COMMENT '显示位置   1 首页     2 商城',
  `is_play` tinyint(2) DEFAULT '1' COMMENT '是否发布   1 发布  2 撤销',
  `sort` int(10) DEFAULT '0' COMMENT '排序  数字越大，排在越前面',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '1 正常  2 删除',
  `create_time` datetime DEFAULT NULL COMMENT '数据插入时间',
  `change_time` datetime DEFAULT NULL COMMENT '数据上一次更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='banner显示控制表';

-- ----------------------------
-- Records of re_banner
-- ----------------------------

-- ----------------------------
-- Table structure for `re_bind_log`
-- ----------------------------
DROP TABLE IF EXISTS `re_bind_log`;
CREATE TABLE `re_bind_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(32) DEFAULT NULL COMMENT '用户微信open_id',
  `account` varchar(50) DEFAULT NULL COMMENT '账号',
  `password` varchar(32) DEFAULT NULL COMMENT '密码',
  `state` tinyint(1) DEFAULT '1' COMMENT '状态 1绑定  2 解绑 ',
  `create_time` datetime DEFAULT NULL COMMENT '绑定时间',
  `change_time` datetime DEFAULT NULL COMMENT '解绑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='微信公众号绑定读者证日志表';

-- ----------------------------
-- Records of re_bind_log
-- ----------------------------

-- ----------------------------
-- Table structure for `re_book_home_budget_log`
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_budget_log`;
CREATE TABLE `re_book_home_budget_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(2) DEFAULT NULL COMMENT '类型  增加还是减少金额   1增加   2减少',
  `kind` tinyint(1) DEFAULT '1' COMMENT '种类   1   经费   2 邮费',
  `money` decimal(8,2) DEFAULT NULL COMMENT '增加减少的金额',
  `total_money` decimal(8,2) DEFAULT '0.00' COMMENT '更改之前总金额',
  `after_total_money` decimal(8,2) DEFAULT NULL COMMENT '更改之前总金额',
  `change_before_money` decimal(8,2) DEFAULT NULL COMMENT '更改之前剩余金额',
  `change_after_money` decimal(8,2) DEFAULT NULL COMMENT '更改之后剩余金额',
  `way` tinyint(1) DEFAULT '1' COMMENT '邮费设置的模式（只对类型14有效）  1 独立模式（只生效一次）  2 循环模式 （每月循环）',
  `start_time` varchar(30) DEFAULT NULL,
  `end_time` varchar(30) DEFAULT NULL,
  `times` int(10) DEFAULT '0' COMMENT '限制次数    在此时间段内限制次数',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员id',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `start_month` varchar(10) DEFAULT NULL COMMENT '如果 type为14 必选   其余不需要此参数  循环模式 （开始的月份）',
  `month_number` tinyint(2) DEFAULT NULL COMMENT '如果 type为14 必选   其余不需要此参数  循环模式 几个月循环一次（默认1）范围 1~12',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='采购预算增加的历史表';

-- ----------------------------
-- Records of re_book_home_budget_log
-- ----------------------------

-- ----------------------------
-- Table structure for `re_book_home_collect`
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_collect`;
CREATE TABLE `re_book_home_collect` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `book_id` int(11) DEFAULT NULL COMMENT '书籍id',
  `type` tinyint(1) DEFAULT NULL COMMENT '类型 1 新书   2 馆藏书',
  `create_time` datetime DEFAULT NULL COMMENT '收藏时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='书籍收藏表';

-- ----------------------------
-- Records of re_book_home_collect
-- ----------------------------

-- ----------------------------
-- Table structure for `re_book_home_manage_shop`
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_manage_shop`;
CREATE TABLE `re_book_home_manage_shop` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员id',
  `shop_id` int(11) DEFAULT NULL COMMENT '书店id',
  `create_time` datetime DEFAULT NULL COMMENT '收藏时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='图书到家管理员管辖书店表';

-- ----------------------------
-- Records of re_book_home_manage_shop
-- ----------------------------

-- ----------------------------
-- Table structure for `re_book_home_order`
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_order`;
CREATE TABLE `re_book_home_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_number` varchar(20) DEFAULT NULL COMMENT '订单号',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证号id 2020 05 09 新增 ',
  `shop_id` int(11) DEFAULT NULL COMMENT '书店 id',
  `price` decimal(5,2) DEFAULT NULL COMMENT '邮费  价格',
  `dis_price` decimal(5,2) DEFAULT NULL COMMENT '折扣后的价格  （默认与实际价格一致）   用户实际支付金额',
  `discount` float(5,2) DEFAULT '100.00' COMMENT '折扣   默认 100  没有折扣  百分比',
  `is_pay` tinyint(1) DEFAULT '2' COMMENT '1 未支付 2 已支付    3支付异常， 4 订单已失效     5 .已退款    6.已同意（后台单纯同意） 7 已拒绝（后台单纯同意）  8已发货（数据就增加到用户采购列表） 9 无法发货  10已取消（用户自己取消）',
  `create_time` datetime DEFAULT NULL COMMENT '加入时间（订单时间）',
  `expire_time` datetime DEFAULT NULL COMMENT '支付过期时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间  （支付时间，支付异常时间，订单失效后的时间）',
  `payment` tinyint(1) DEFAULT '1' COMMENT '支付方式  1 微信支付   2 系统预算扣除',
  `agree_time` datetime DEFAULT NULL COMMENT '同意时间、拒绝时间',
  `deliver_time` datetime DEFAULT NULL COMMENT '发货时间、无法发货时间',
  `refund_number` varchar(20) DEFAULT NULL COMMENT '退款单号',
  `refund_time` datetime DEFAULT NULL COMMENT '退款时间',
  `refund_remark` varchar(500) DEFAULT NULL COMMENT '退款备注',
  `refund_manage_id` int(11) DEFAULT NULL COMMENT '退款管理员id',
  `tracking_number` varchar(30) DEFAULT NULL COMMENT '快递单号',
  `deliver_manage_id` int(11) DEFAULT NULL COMMENT '确认发货的管理员id',
  `cancel_time` datetime DEFAULT NULL COMMENT '取消时间，用户自己取消订单，只能在未审核前取消',
  `cancel_remark` varchar(300) DEFAULT NULL COMMENT '取消备注',
  `settle_state` tinyint(1) DEFAULT '3' COMMENT '邮费结算状态（这里只标记就行）  1 已结算   3 未结算',
  `settle_sponsor_time` datetime DEFAULT NULL COMMENT '结算时间（结算方结算）',
  `settle_affirm_time` datetime DEFAULT NULL COMMENT '确认结算时间',
  `settle_sponsor_manage_id` int(11) DEFAULT NULL COMMENT '书店结算的管理员id',
  `settle_affirm_manage_id` int(11) DEFAULT NULL COMMENT '图书馆结算的管理员id',
  `type` tinyint(1) DEFAULT '1' COMMENT '类型 1 新书（默认）   2 馆藏书',
  `is_sign` tinyint(1) DEFAULT '1' COMMENT '1 未发货 2 已发货 3已派送 4已签收 5拒绝签收',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='新书订单表';

-- ----------------------------
-- Records of re_book_home_order
-- ----------------------------

-- ----------------------------
-- Table structure for `re_book_home_order_address`
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_order_address`;
CREATE TABLE `re_book_home_order_address` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL COMMENT '订单id',
  `username` varchar(30) DEFAULT NULL COMMENT '用户姓名',
  `tel` varchar(20) DEFAULT NULL COMMENT '电话号码',
  `province` varchar(30) DEFAULT NULL COMMENT '省',
  `city` varchar(30) DEFAULT NULL,
  `district` varchar(30) DEFAULT NULL COMMENT '区',
  `street` varchar(30) DEFAULT NULL COMMENT '街道',
  `address` varchar(200) DEFAULT NULL COMMENT '详细地址',
  `send_username` varchar(30) DEFAULT NULL COMMENT '发货人名称',
  `send_tel` varchar(20) DEFAULT NULL COMMENT '发货人电话',
  `send_province` varchar(30) DEFAULT NULL,
  `send_city` varchar(30) DEFAULT NULL,
  `send_district` varchar(30) DEFAULT NULL,
  `send_street` varchar(30) DEFAULT NULL,
  `send_address` varchar(200) DEFAULT NULL,
  `manage_id` int(11) DEFAULT NULL COMMENT '修改管理员id',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime DEFAULT NULL COMMENT '删除、修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户订单地址表';

-- ----------------------------
-- Records of re_book_home_order_address
-- ----------------------------

-- ----------------------------
-- Table structure for `re_book_home_order_book`
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_order_book`;
CREATE TABLE `re_book_home_order_book` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL COMMENT '订单id',
  `book_id` int(11) DEFAULT NULL,
  `barcode_id` int(11) DEFAULT NULL COMMENT '条形码id (馆藏书才有)',
  `type` tinyint(1) DEFAULT '1' COMMENT '类型 1 新书（默认）   2 馆藏书',
  `node` tinyint(1) DEFAULT '3' COMMENT '是否借阅成功   1成功  2 失败  3未处理（默认）',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime DEFAULT NULL COMMENT '成功或失败的时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='新书订单书籍表';

-- ----------------------------
-- Records of re_book_home_order_book
-- ----------------------------

-- ----------------------------
-- Table structure for `re_book_home_order_pay`
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_order_pay`;
CREATE TABLE `re_book_home_order_pay` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_number` varchar(20) DEFAULT NULL COMMENT '订单号',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `payment` tinyint(1) DEFAULT '1' COMMENT '支付方式  1 微信支付',
  `trade_no` varchar(50) DEFAULT NULL COMMENT '微信支付订单号',
  `buyer_id` varchar(50) DEFAULT NULL COMMENT '买家微信支付分配的终端设备号',
  `total_amount` decimal(8,2) DEFAULT NULL COMMENT '订单总金额,订单总金额，单位为分',
  `receipt_amount` decimal(8,2) DEFAULT NULL COMMENT '实收金额,商家在交易中实际收到的款项',
  `buyer_pay_amount` decimal(8,2) DEFAULT NULL COMMENT '付款金额,现金支付金额订单现金支付金额，详见支付金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='订单支付表';

-- ----------------------------
-- Records of re_book_home_order_pay
-- ----------------------------

-- ----------------------------
-- Table structure for `re_book_home_order_refund`
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_order_refund`;
CREATE TABLE `re_book_home_order_refund` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `refund_number` varchar(20) DEFAULT NULL COMMENT '退款单号',
  `refund_id` varchar(255) DEFAULT NULL COMMENT '微信退款单号',
  `refund_time` datetime DEFAULT NULL COMMENT '退款时间',
  `price` decimal(5,2) DEFAULT NULL COMMENT '退款金额',
  `payment` tinyint(1) DEFAULT NULL COMMENT '退款路径  1 微信',
  `status` tinyint(1) DEFAULT NULL COMMENT '退款结果  1 成功  2失败',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员id',
  `error_code` varchar(20) DEFAULT NULL COMMENT '退款失败的code',
  `error_msg` varchar(150) DEFAULT NULL COMMENT '退款失败的消息提示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='订单退款日志表';

-- ----------------------------
-- Records of re_book_home_order_refund
-- ----------------------------

-- ----------------------------
-- Table structure for `re_book_home_postage_set`
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_postage_set`;
CREATE TABLE `re_book_home_postage_set` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) DEFAULT NULL COMMENT '类型  1 新书  2 馆藏书',
  `number` mediumint(5) DEFAULT NULL COMMENT '本数',
  `price` decimal(5,2) DEFAULT NULL COMMENT '邮费    根据本数来设置      本数必须从小到大依次排',
  `create_time` datetime DEFAULT NULL COMMENT '设置时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='邮费设置表';

-- ----------------------------
-- Records of re_book_home_postage_set
-- ----------------------------

-- ----------------------------
-- Table structure for `re_book_home_postal_return`
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_postal_return`;
CREATE TABLE `re_book_home_postal_return` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tracking_number` varchar(20) DEFAULT NULL COMMENT '订单号 （一次性归还多本，订单号一致）',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证号id',
  `purchase_id` int(11) DEFAULT NULL COMMENT '采购记录 id',
  `courier_id` int(11) DEFAULT NULL COMMENT '快递名称',
  `barcode` varchar(25) DEFAULT NULL COMMENT '条形码',
  `create_time` datetime DEFAULT NULL COMMENT '加入时间（归还时间）',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间  （确认归还时间）',
  `status` tinyint(1) DEFAULT '3' COMMENT '归还状态   1 已确认归还  2 已取消归还(管理员取消)  3 归还中  4已撤销（用户自己操作）',
  `return_manage_id` int(11) DEFAULT NULL COMMENT '确认归还的管理员id',
  `book_name` varchar(100) DEFAULT NULL COMMENT '书名',
  `author` varchar(100) DEFAULT NULL COMMENT '作者',
  `price` varchar(15) DEFAULT NULL COMMENT '金额',
  `book_num` varchar(50) DEFAULT NULL COMMENT '索书号',
  `isbn` varchar(25) DEFAULT NULL COMMENT 'ISBN号',
  `press` varchar(50) DEFAULT NULL COMMENT '出版社',
  `pre_time` varchar(30) DEFAULT NULL COMMENT '出版时间',
  `img` varchar(120) DEFAULT '/default/default_book.png',
  `borrow_time` datetime DEFAULT NULL COMMENT '到期时间 ',
  `expire_time` datetime DEFAULT NULL COMMENT '到期时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='新书邮递归还表';

-- ----------------------------
-- Records of re_book_home_postal_return
-- ----------------------------

-- ----------------------------
-- Table structure for `re_book_home_postal_rev`
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_postal_rev`;
CREATE TABLE `re_book_home_postal_rev` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trace_no` varchar(255) DEFAULT NULL COMMENT '邮政运单号',
  `send_id` varchar(255) DEFAULT '' COMMENT '推送方标识，JDPT：寄递平台',
  `provice_no` int(11) DEFAULT NULL COMMENT '数据生产的省公司代码，对不能确定的省份取99',
  `msg_kind` varchar(255) DEFAULT NULL COMMENT '消息类别，接口编码（JDPT_BOTTLE_TRACE）',
  `serial_no` varchar(50) DEFAULT NULL COMMENT '消息唯一序列号，相对于消息类别的唯一流水号，对于本类消息，是一个不重复的ID值，不同类别的消息，该值会重复',
  `receive_id` varchar(255) DEFAULT NULL COMMENT '接收方标识，BOTTLE：贝图科技',
  `batch_no` int(11) DEFAULT NULL COMMENT '批次号',
  `data_type` varchar(255) DEFAULT NULL COMMENT '数据类型1-JSON   2-XML   3-压缩后的Byte[]',
  `data_digest` varchar(255) DEFAULT NULL COMMENT '签名结果',
  `trace_content` text COMMENT '轨迹信息',
  `receive_content` text COMMENT '推送信息原本',
  `create_time` datetime DEFAULT NULL COMMENT '接收时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='邮政轨迹推送信息表';

-- ----------------------------
-- Records of re_book_home_postal_rev
-- ----------------------------

-- ----------------------------
-- Table structure for `re_book_home_purchase`
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_purchase`;
CREATE TABLE `re_book_home_purchase` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `book_id` int(11) DEFAULT NULL COMMENT '书籍id',
  `barcode_id` int(11) DEFAULT NULL COMMENT '条形码id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证号id',
  `shop_id` int(11) DEFAULT NULL COMMENT '书店id',
  `order_id` int(11) DEFAULT NULL COMMENT '订单id',
  `price` decimal(5,2) DEFAULT NULL COMMENT '金额   采购时的金额',
  `isbn` varchar(25) DEFAULT NULL COMMENT 'isbn号',
  `barcode` varchar(25) DEFAULT NULL COMMENT '条形码',
  `type` tinyint(1) DEFAULT '1' COMMENT '类型 1 新书  2 馆藏书',
  `create_time` datetime DEFAULT NULL COMMENT '采购时间',
  `expire_time` datetime DEFAULT NULL COMMENT '到期时间   默认是采购时间后面一个月',
  `return_time` datetime DEFAULT NULL COMMENT '归还时间  (预归还时间)',
  `return_state` tinyint(1) DEFAULT '1' COMMENT '归还状态  1 借阅中  2 已归还   3 归还中',
  `settle_state` tinyint(1) DEFAULT NULL COMMENT '结算状态  1 已结算  2 结算中  3 未结算',
  `settle_sponsor_time` datetime DEFAULT NULL COMMENT '结算发起时间',
  `settle_affirm_time` datetime DEFAULT NULL COMMENT '结算确认时间',
  `settle_sponsor_manage_id` int(11) DEFAULT NULL COMMENT '结算发起的管理员id',
  `settle_affirm_manage_id` int(11) DEFAULT NULL COMMENT '结算确认的管理员id',
  `overdue_money` decimal(5,2) DEFAULT NULL COMMENT '逾期金额     超期已交金额，归还后才有的金额，没归还，列表显示的时候，自己算',
  `is_drop` tinyint(1) DEFAULT '1' COMMENT '丢失状态  1 正常  2 已丢失',
  `drop_money` decimal(5,2) DEFAULT NULL COMMENT '丢失后补偿的金额',
  `total_money` decimal(5,2) DEFAULT NULL COMMENT '总赔付金额',
  `is_pay` tinyint(1) DEFAULT '4' COMMENT '逾期金额和丢失赔付金额 缴纳状态 1 已缴纳   2 免除违约金   3 未缴纳   4 无违约金',
  `pay_time` datetime DEFAULT NULL COMMENT '缴纳逾期金额时间',
  `pur_manage_id` int(11) DEFAULT NULL COMMENT '采购管理员id',
  `return_manage_id` int(11) DEFAULT NULL COMMENT '归还书籍管理员id',
  `money_manage_id` int(11) DEFAULT NULL COMMENT '收取滞纳金的管理员id   如果未逾期也未丢失  就没有此数据',
  `node` tinyint(1) DEFAULT NULL COMMENT '是否借阅成功   1成功  2 失败',
  `is_dispose` tinyint(1) DEFAULT '2' COMMENT '是否处理  1 已处理  2 未处理（默认）',
  `dispose_time` datetime DEFAULT NULL COMMENT '处理时间',
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `barcode` (`barcode`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='书店书籍采购表';

-- ----------------------------
-- Records of re_book_home_purchase
-- ----------------------------

-- ----------------------------
-- Table structure for `re_book_home_purchase_set`
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_purchase_set`;
CREATE TABLE `re_book_home_purchase_set` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(2) DEFAULT NULL COMMENT '类型 1 图书采购经费预算（总预算） 2.逾期滞纳金（多少元每天）3.每人每次可采购图书本数4.每人每次可采购图书金额 5.每人每月可采购图书本数6.每人每月可采购图书金额7.每人每年可采购图书本数8.每人每年可采购图书金额 9.单本金额上限10. 复本数不允许超过 几本11.多少年之前的书不允许采购（0 表示全部 都可以采购）12.读者一次性借阅的天数13.书籍损坏赔偿原价的 多少（百分比） 14 图书采购邮费预算（总预算） 15.积分扣除比例  （欠费缴纳） 16.复本数不允许超过 几本（本系统）17.每人同种书不允许超过 几本（增对新书有效）18.有效复本书计算排除的馆藏地点代码（多个逗号拼接） 19.馆藏书可借阅的馆藏地点代码（多个逗号拼接）',
  `number` float(10,2) DEFAULT NULL COMMENT '数量（金额）  存放数字',
  `content` varchar(1000) DEFAULT NULL COMMENT '存放文本内容',
  `create_time` datetime DEFAULT NULL COMMENT '设置时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  `way` tinyint(1) DEFAULT NULL COMMENT '邮费设置的模式（只对类型14有效）  1 独立模式（只生效一次）  2 循环模式 （每月循环）',
  `start_time` varchar(30) DEFAULT NULL COMMENT '开始时间  只对活动邮费预算有效',
  `end_time` varchar(30) DEFAULT NULL COMMENT '结束时间  只对活动邮费预算有效',
  `times` int(10) DEFAULT NULL COMMENT '限制次数    在此时间段内限制次数',
  `start_month` varchar(10) DEFAULT NULL COMMENT '如果 type为14 必选   其余不需要此参数  循环模式 （开始的月份）',
  `month_number` tinyint(2) DEFAULT NULL COMMENT '如果 type为14 必选   其余不需要此参数  循环模式 几个月循环一次（默认1）范围 1~12',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='采购设置表';

-- ----------------------------
-- Records of re_book_home_purchase_set
-- ----------------------------

-- ----------------------------
-- Table structure for `re_book_home_return_address`
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_return_address`;
CREATE TABLE `re_book_home_return_address` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) DEFAULT NULL COMMENT '书店id，只增对  way为 3有效',
  `way` tinyint(1) DEFAULT NULL COMMENT '方式  1 现场归还   2 邮递归还   3 发货地址（只能有一个）',
  `username` varchar(30) DEFAULT NULL COMMENT '发货人名称',
  `tel` varchar(20) DEFAULT NULL COMMENT '发货人联系电话号码',
  `province` varchar(20) DEFAULT NULL COMMENT '省',
  `city` varchar(30) DEFAULT NULL COMMENT '市',
  `district` varchar(30) DEFAULT NULL COMMENT '区',
  `street` varchar(30) DEFAULT NULL COMMENT '街道',
  `address` varchar(200) DEFAULT NULL COMMENT '详细地址',
  `intro` text COMMENT '简介',
  `type` tinyint(1) DEFAULT NULL COMMENT '类型 1 新书（默认）   2 馆藏书',
  `dispark_time` varchar(300) DEFAULT NULL COMMENT '开发时间（一段话）',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1正常  2删除',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL COMMENT '删除、修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='图书到家邮递地址、归还地址设置表';

-- ----------------------------
-- Records of re_book_home_return_address
-- ----------------------------

-- ----------------------------
-- Table structure for `re_book_home_return_address_explain`
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_return_address_explain`;
CREATE TABLE `re_book_home_return_address_explain` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `way` tinyint(1) DEFAULT NULL COMMENT '方式   1 现场归还   2 邮递归还',
  `content` text COMMENT '说明内容',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL COMMENT '删除、修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='图书到家归还地址说明表';

-- ----------------------------
-- Records of re_book_home_return_address_explain
-- ----------------------------

-- ----------------------------
-- Table structure for `re_book_home_return_record`
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_return_record`;
CREATE TABLE `re_book_home_return_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account` varchar(20) DEFAULT NULL COMMENT '读者证号',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证号id',
  `book_name` varchar(300) DEFAULT NULL COMMENT '书名',
  `author` varchar(100) DEFAULT NULL COMMENT '作者',
  `price` varchar(15) DEFAULT NULL COMMENT '价格',
  `book_num` varchar(15) DEFAULT NULL COMMENT '索书号',
  `isbn` varchar(25) DEFAULT NULL COMMENT 'ISBN号',
  `press` varchar(50) DEFAULT NULL COMMENT '出版社',
  `pre_time` varchar(50) DEFAULT NULL,
  `barcode` varchar(50) DEFAULT NULL COMMENT '条形码',
  `borrow_time` datetime DEFAULT NULL COMMENT '借阅时间',
  `expire_time` datetime DEFAULT NULL COMMENT '到期时间',
  `return_time` datetime DEFAULT NULL COMMENT '归还时间 ',
  `return_manage_id` int(11) DEFAULT NULL COMMENT '归还书籍管理员id',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户归还书籍记录表';

-- ----------------------------
-- Records of re_book_home_return_record
-- ----------------------------

-- ----------------------------
-- Table structure for `re_book_home_schoolbag`
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_schoolbag`;
CREATE TABLE `re_book_home_schoolbag` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `book_id` int(11) DEFAULT NULL COMMENT '书籍id',
  `barcode_id` int(11) DEFAULT NULL COMMENT '条形码id（只有馆藏书才有此字段）',
  `type` tinyint(1) DEFAULT '1' COMMENT '类型 1 新书   2 馆藏书',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `create_time` datetime DEFAULT NULL COMMENT '加入时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间(购买时间)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='新书书包表';

-- ----------------------------
-- Records of re_book_home_schoolbag
-- ----------------------------

-- ----------------------------
-- Table structure for `re_book_types`
-- ----------------------------
DROP TABLE IF EXISTS `re_book_types`;
CREATE TABLE `re_book_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(100) DEFAULT NULL COMMENT '名称',
  `classify` varchar(30) DEFAULT NULL COMMENT '编码',
  `pid` int(10) unsigned DEFAULT NULL COMMENT 'pid',
  `path` varchar(255) DEFAULT NULL COMMENT '路径',
  `level` int(10) unsigned DEFAULT NULL COMMENT '等级',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='中图分类法';

-- ----------------------------
-- Records of re_book_types
-- ----------------------------

-- ----------------------------
-- Table structure for `re_branch_access_num`
-- ----------------------------
DROP TABLE IF EXISTS `re_branch_access_num`;
CREATE TABLE `re_branch_access_num` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT '0' COMMENT '分馆id',
  `type` tinyint(1) DEFAULT '1' COMMENT '类型   1.点击量   2. 下载量  3.浏览人次   4.评估年度 ',
  `number` int(11) DEFAULT '0' COMMENT '访问量',
  `yyy` int(4) DEFAULT NULL COMMENT '年',
  `mmm` tinyint(2) DEFAULT NULL COMMENT '月',
  `ddd` tinyint(2) DEFAULT NULL COMMENT '日',
  `create_time` datetime DEFAULT NULL COMMENT '数据创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='数字阅读资源访问统计';

-- ----------------------------
-- Records of re_branch_access_num
-- ----------------------------

-- ----------------------------
-- Table structure for `re_branch_account`
-- ----------------------------
DROP TABLE IF EXISTS `re_branch_account`;
CREATE TABLE `re_branch_account` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT '0' COMMENT '分馆id',
  `username` varchar(30) DEFAULT NULL COMMENT '名称',
  `account` varchar(20) DEFAULT '' COMMENT '账号',
  `password` varchar(32) DEFAULT '' COMMENT '密码',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除 1 正常   2 删除',
  `manage_id` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '数据创建时间',
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='分馆用户登录账号和密码';

-- ----------------------------
-- Records of re_branch_account
-- ----------------------------

-- ----------------------------
-- Table structure for `re_branch_info`
-- ----------------------------
DROP TABLE IF EXISTS `re_branch_info`;
CREATE TABLE `re_branch_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '单页面id',
  `branch_name` varchar(30) DEFAULT NULL COMMENT '场馆名称',
  `is_main` tinyint(1) DEFAULT '1' COMMENT '是否总馆  1 总馆 2 分馆',
  `img` varchar(120) DEFAULT NULL COMMENT '封面图片',
  `intro` text COMMENT '场馆简介',
  `dispark_time` varchar(200) DEFAULT NULL COMMENT '开放时间（一段文字）',
  `transport_line` varchar(500) DEFAULT NULL COMMENT '交通线路 ',
  `province` varchar(30) DEFAULT NULL COMMENT '省',
  `city` varchar(50) DEFAULT NULL COMMENT '市',
  `district` varchar(50) DEFAULT NULL COMMENT '区县',
  `street` varchar(30) DEFAULT NULL COMMENT '街道',
  `address` varchar(200) DEFAULT NULL COMMENT '详细地址',
  `tel` varchar(50) DEFAULT NULL COMMENT '联系方式',
  `contacts` varchar(30) DEFAULT NULL COMMENT '联系人',
  `lon` varchar(20) DEFAULT NULL COMMENT '经度',
  `lat` varchar(20) DEFAULT NULL COMMENT '纬度',
  `borrow_notice` varchar(1000) DEFAULT NULL COMMENT '借阅须知',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1 正常  2 删除',
  `browse_num` int(11) DEFAULT '0' COMMENT '浏览量',
  `create_time` datetime DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='总分馆简介表';

-- ----------------------------
-- Records of re_branch_info
-- ----------------------------

-- ----------------------------
-- Table structure for `re_code_borrow_log`
-- ----------------------------
DROP TABLE IF EXISTS `re_code_borrow_log`;
CREATE TABLE `re_code_borrow_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `place_code_id` int(11) DEFAULT NULL COMMENT '场所码id',
  `user_id` char(32) DEFAULT NULL COMMENT '用户id',
  `book_name` varchar(80) DEFAULT NULL COMMENT '书名',
  `type` varchar(20) DEFAULT NULL COMMENT '书的类别   22大类',
  `small_type` int(11) DEFAULT NULL COMMENT '小类型',
  `author` varchar(80) DEFAULT '' COMMENT '作者',
  `price` float(5,2) DEFAULT NULL COMMENT '价格',
  `press` varchar(50) DEFAULT '' COMMENT '出版社',
  `isbn` varchar(20) DEFAULT '' COMMENT 'Isbn号',
  `barcode` varchar(20) DEFAULT '' COMMENT '条码号	主要是处理图书馆的书籍，图书馆的系统都有统一的条码',
  `img` varchar(120) DEFAULT 'default/default_book.png' COMMENT '图片',
  `thumb_img` varchar(120) DEFAULT NULL COMMENT '封面缩略图',
  `pre_time` varchar(20) DEFAULT '' COMMENT '出版时间',
  `callno` varchar(50) DEFAULT '' COMMENT '索书号',
  `classno` varchar(50) DEFAULT NULL,
  `intro` text COMMENT '简介',
  `handler` varchar(10) DEFAULT NULL COMMENT '是否是管理员操作	YES_APP(手机管理员)，YES_WEB（web管理员）,NO，YES_APP，YES_WEB的话，user_id就为管理员的id',
  `expire_time` datetime DEFAULT NULL COMMENT '预计归还时间    时间戳',
  `return_time` datetime DEFAULT NULL COMMENT '归还时间   日期时间格式',
  `create_time` datetime DEFAULT NULL COMMENT '借阅时间',
  `change_time` datetime DEFAULT NULL,
  `rdrrecno` int(10) DEFAULT NULL COMMENT '读者记录号',
  `lib_id` varchar(30) DEFAULT NULL COMMENT '所属馆',
  `lib_name` varchar(30) DEFAULT NULL COMMENT '所属馆',
  `cur_local` varchar(30) DEFAULT NULL COMMENT '当前所在地',
  `cur_local_name` varchar(30) DEFAULT NULL COMMENT '当前所在地',
  `cur_lib` varchar(30) DEFAULT NULL COMMENT '当前所在馆',
  `cur_lib_name` varchar(30) DEFAULT NULL COMMENT '当前所在馆',
  `owner_local` varchar(30) DEFAULT NULL COMMENT '所属地',
  `owner_local_name` varchar(30) DEFAULT NULL COMMENT '所属地',
  `recno` int(10) DEFAULT NULL COMMENT '馆藏记录号',
  `bibrecno` int(10) DEFAULT NULL COMMENT '书目记录号',
  `account` varchar(20) DEFAULT NULL COMMENT '读者证号',
  `status` tinyint(1) DEFAULT '2' COMMENT '状态  1  已归还  2借阅中',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='码上借 书籍借阅记录';

-- ----------------------------
-- Records of re_code_borrow_log
-- ----------------------------

-- ----------------------------
-- Table structure for `re_code_guide_exhibition`
-- ----------------------------
DROP TABLE IF EXISTS `re_code_guide_exhibition`;
CREATE TABLE `re_code_guide_exhibition` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '展览名',
  `img` varchar(120) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL COMMENT '展览开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '展览结束时间',
  `intro` text COMMENT '简介详情',
  `qr_url` varchar(255) DEFAULT NULL COMMENT '活动二维码引用地址',
  `qr_code` varchar(10) DEFAULT NULL COMMENT '二维码 code',
  `browse_num` int(10) unsigned DEFAULT '0' COMMENT '浏览量',
  `manage_id` int(11) DEFAULT NULL,
  `is_play` tinyint(1) DEFAULT '2' COMMENT '是否发布  1 发布  2 未发布',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1 正常  2 已删除',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='作品展览表';

-- ----------------------------
-- Records of re_code_guide_exhibition
-- ----------------------------

-- ----------------------------
-- Table structure for `re_code_guide_production`
-- ----------------------------
DROP TABLE IF EXISTS `re_code_guide_production`;
CREATE TABLE `re_code_guide_production` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '作品名',
  `exhibition_id` int(11) DEFAULT NULL COMMENT '展览id',
  `type_id` int(11) DEFAULT NULL COMMENT '类型id',
  `intro` text COMMENT '简介',
  `img` varchar(1200) DEFAULT NULL COMMENT '图片,多张用 | 拼接',
  `thumb_img` varchar(1200) DEFAULT NULL,
  `content` longtext COMMENT '作品正文内容',
  `voice` varchar(120) DEFAULT NULL COMMENT '作品音频地址，可以用内容直接转换成语音',
  `voice_name` varchar(80) DEFAULT NULL COMMENT '音频名称',
  `video` varchar(120) DEFAULT NULL COMMENT '视频资源',
  `video_name` varchar(80) DEFAULT NULL COMMENT '视频名称',
  `browse_num` int(10) unsigned DEFAULT '0' COMMENT '浏览量',
  `qr_url` varchar(255) DEFAULT NULL COMMENT '活动二维码引用地址',
  `qr_code` varchar(10) DEFAULT NULL COMMENT '二维码 code',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1 正常  2 已删除',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL,
  `manage_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='作品表';

-- ----------------------------
-- Records of re_code_guide_production
-- ----------------------------

-- ----------------------------
-- Table structure for `re_code_guide_production_type`
-- ----------------------------
DROP TABLE IF EXISTS `re_code_guide_production_type`;
CREATE TABLE `re_code_guide_production_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(50) DEFAULT NULL COMMENT '类型名',
  `intro` text,
  `is_del` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否删除  1 正常   2 删除',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='作品类型表';

-- ----------------------------
-- Records of re_code_guide_production_type
-- ----------------------------

-- ----------------------------
-- Table structure for `re_code_place`
-- ----------------------------
DROP TABLE IF EXISTS `re_code_place`;
CREATE TABLE `re_code_place` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '志愿者id',
  `name` varchar(20) DEFAULT NULL COMMENT '姓名',
  `way` tinyint(1) DEFAULT '1' COMMENT '定位方式  1 经纬度   2 地址',
  `lon` varchar(30) DEFAULT NULL COMMENT '经度',
  `lat` varchar(30) DEFAULT NULL COMMENT '纬度',
  `province` varchar(50) DEFAULT NULL COMMENT '身份证号码',
  `city` varchar(50) DEFAULT NULL COMMENT '出生日期',
  `district` varchar(50) DEFAULT NULL COMMENT '学校',
  `address` varchar(100) DEFAULT NULL COMMENT '地址|住址',
  `qr_code` varchar(10) DEFAULT NULL,
  `qr_url` varchar(120) DEFAULT NULL,
  `intro` text COMMENT '简介',
  `distance` int(11) DEFAULT '0' COMMENT '扫码有效距离，单位米  只有admin才能配置，若未配置，则使用配置环境数据',
  `is_del` tinyint(1) unsigned DEFAULT '1' COMMENT '是否删除   1.正常   2.已删除',
  `is_play` tinyint(1) DEFAULT '1' COMMENT '是否发布  1 已发布  2 未发布',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='码上借场所码';

-- ----------------------------
-- Records of re_code_place
-- ----------------------------

-- ----------------------------
-- Table structure for `re_code_return_log`
-- ----------------------------
DROP TABLE IF EXISTS `re_code_return_log`;
CREATE TABLE `re_code_return_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `place_code_id` int(1) DEFAULT NULL COMMENT '场所码id',
  `book_name` varchar(100) DEFAULT '' COMMENT '书名',
  `user_id` varchar(32) DEFAULT '' COMMENT '用户id',
  `type` varchar(30) DEFAULT '' COMMENT '书的类别	参照book_types表    22大类',
  `small_type` int(11) DEFAULT NULL COMMENT '小类型',
  `author` varchar(100) DEFAULT '',
  `price` varchar(15) DEFAULT '' COMMENT '价格',
  `press` varchar(100) DEFAULT '' COMMENT '出版社',
  `isbn` varchar(30) DEFAULT '',
  `barcode` varchar(20) DEFAULT NULL COMMENT '增对后台添加图书馆的数据',
  `pre_time` varchar(32) DEFAULT '' COMMENT '出版时间',
  `callno` varchar(50) DEFAULT '' COMMENT '索书号',
  `classno` varchar(50) DEFAULT NULL,
  `intro` varchar(3000) DEFAULT '' COMMENT '简介',
  `img` varchar(150) DEFAULT 'default/default_book.png',
  `thumb_img` varchar(150) DEFAULT NULL COMMENT '封面缩略图',
  `borrow_time` datetime DEFAULT NULL COMMENT '添加时间',
  `is_del` varchar(5) DEFAULT 'NO' COMMENT '是否删除，用户自己删除',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL,
  `rdrrecno` int(10) DEFAULT NULL COMMENT '读者记录号',
  `lib_id` varchar(30) DEFAULT NULL COMMENT '所属馆',
  `lib_name` varchar(30) DEFAULT NULL COMMENT '所属馆',
  `cur_local` varchar(30) DEFAULT NULL COMMENT '当前所在地',
  `cur_local_name` varchar(30) DEFAULT NULL COMMENT '当前所在地',
  `cur_lib` varchar(30) DEFAULT NULL COMMENT '当前所在馆',
  `cur_lib_name` varchar(30) DEFAULT NULL COMMENT '当前所在馆',
  `owner_local` varchar(30) DEFAULT NULL COMMENT '所属地',
  `owner_local_name` varchar(30) DEFAULT NULL COMMENT '所属地',
  `recno` int(10) DEFAULT NULL COMMENT '馆藏记录号',
  `bibrecno` int(10) DEFAULT NULL COMMENT '书目记录号',
  `account` varchar(20) DEFAULT NULL COMMENT '读者证号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='码上借 书籍归还记录';

-- ----------------------------
-- Records of re_code_return_log
-- ----------------------------

-- ----------------------------
-- Table structure for `re_contest_activity`
-- ----------------------------
DROP TABLE IF EXISTS `re_contest_activity`;
CREATE TABLE `re_contest_activity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '线上大赛活动id',
  `node` tinyint(1) DEFAULT '1' COMMENT '举办方式   1 独立活动  2 多馆联合   ',
  `title` varchar(100) DEFAULT NULL COMMENT '活动名称',
  `web_img` varchar(120) DEFAULT NULL COMMENT 'web封面图片',
  `app_img` varchar(120) DEFAULT 'default/default_contest_activity.png' COMMENT 'app封面图片',
  `host_handle` varchar(255) DEFAULT '' COMMENT '主办单位',
  `bear_handle` varchar(255) DEFAULT NULL COMMENT '承办单位   弃用',
  `tel` varchar(30) DEFAULT NULL COMMENT '咨询方式',
  `con_start_time` datetime DEFAULT NULL COMMENT '投稿开始时间',
  `con_end_time` datetime DEFAULT NULL COMMENT '投稿结束时间',
  `vote_start_time` datetime DEFAULT NULL COMMENT '投票开始时间',
  `vote_end_time` datetime DEFAULT NULL COMMENT '投票结束时间',
  `is_reader` tinyint(1) DEFAULT '2' COMMENT '报名是否需要绑定读者证   1.是   2.否(默认）',
  `vote_way` tinyint(1) DEFAULT NULL COMMENT '投票方式   1  总票数方式   2 每日投票方式',
  `number` int(10) DEFAULT '0' COMMENT '票数',
  `originals` text COMMENT '原创申明 配置  在每个馆也可单独设置  ',
  `promise` text COMMENT '作者承诺',
  `intro` text COMMENT '简介',
  `rule_content` longtext COMMENT '活动规则内容',
  `manage_id` int(10) unsigned DEFAULT NULL COMMENT '管理员id',
  `is_play` tinyint(1) DEFAULT '2' COMMENT '是否发布 1.发布 2.未发布    默认1',
  `is_del` tinyint(1) unsigned DEFAULT '1' COMMENT '是否删除   1.正常   2.已删除',
  `browse_num` int(11) DEFAULT '0' COMMENT '浏览量',
  `create_time` datetime DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime DEFAULT NULL COMMENT '编辑时间',
  `real_info` varchar(255) CHARACTER SET utf8mb4 NOT NULL COMMENT '需要验证的真实信息id用|连接起来    1、姓名   2 、身份证号码  3、电话号码   4、读者证号码    5、微信号 6、单位名称 7、指导老师  8、指导老师联系方式',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='线上大赛活动表';

-- ----------------------------
-- Records of re_contest_activity
-- ----------------------------

-- ----------------------------
-- Table structure for `re_contest_activity_declare`
-- ----------------------------
DROP TABLE IF EXISTS `re_contest_activity_declare`;
CREATE TABLE `re_contest_activity_declare` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '线上大赛活动id',
  `con_id` int(11) DEFAULT NULL COMMENT '大赛id',
  `lib_id` int(11) DEFAULT NULL,
  `content` text COMMENT '内容名称',
  `create_time` datetime DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='线上大赛申明表';

-- ----------------------------
-- Records of re_contest_activity_declare
-- ----------------------------

-- ----------------------------
-- Table structure for `re_contest_activity_type`
-- ----------------------------
DROP TABLE IF EXISTS `re_contest_activity_type`;
CREATE TABLE `re_contest_activity_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '线上作品大赛活动相关类型',
  `con_id` int(10) unsigned DEFAULT NULL COMMENT '线上大赛活动id',
  `type_name` varchar(50) DEFAULT NULL COMMENT '类型名称',
  `limit_num` int(10) unsigned DEFAULT NULL COMMENT '限制该分类作品上传最大数量  0为不限',
  `letter` char(1) DEFAULT NULL COMMENT '编号首字母',
  `node` varchar(255) DEFAULT '1' COMMENT '上传文件类型，连接  1.图片(默认)  2.文字  3.音频  4.视频  多个逗号链接',
  `is_del` tinyint(1) unsigned DEFAULT '1' COMMENT '是否删除   1.正常   2.已删除',
  `create_time` datetime DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='线上大赛活动相关类型';

-- ----------------------------
-- Records of re_contest_activity_type
-- ----------------------------

-- ----------------------------
-- Table structure for `re_contest_activity_unit`
-- ----------------------------
DROP TABLE IF EXISTS `re_contest_activity_unit`;
CREATE TABLE `re_contest_activity_unit` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `con_id` int(11) DEFAULT NULL,
  `nature` tinyint(2) DEFAULT NULL COMMENT '单位性质   1 图书馆  2 文化馆  3 博物馆',
  `name` varchar(10) DEFAULT NULL COMMENT '单位名称',
  `img` varchar(120) DEFAULT 'default/default_readshare_unit.png',
  `intro` text COMMENT '单位简介',
  `tel` varchar(15) DEFAULT NULL COMMENT '联系电话',
  `contacts` varchar(50) DEFAULT NULL COMMENT '联系人姓名',
  `originals` text COMMENT '原创申明  可为空，空则用活动设置的申明',
  `letter` varchar(1) DEFAULT NULL COMMENT '首字母',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除   1 正常 2 已删除',
  `order` int(11) DEFAULT '0' COMMENT '排序  数字越小越靠前    1在最前面',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='大赛单位表';

-- ----------------------------
-- Records of re_contest_activity_unit
-- ----------------------------

-- ----------------------------
-- Table structure for `re_contest_activity_vote`
-- ----------------------------
DROP TABLE IF EXISTS `re_contest_activity_vote`;
CREATE TABLE `re_contest_activity_vote` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户投票',
  `con_id` int(10) unsigned DEFAULT NULL COMMENT '线上大赛活动id',
  `works_id` int(10) unsigned DEFAULT NULL COMMENT '作品id',
  `yyy` smallint(4) unsigned DEFAULT NULL COMMENT '年',
  `mmm` varchar(2) DEFAULT NULL,
  `ddd` varchar(2) DEFAULT NULL,
  `date` date DEFAULT NULL COMMENT '时间 2019-08-07',
  `user_id` int(10) unsigned DEFAULT NULL COMMENT '用户id',
  `terminal` varchar(10) DEFAULT NULL COMMENT '投票终端    web   android   ios  wx',
  `create_time` datetime DEFAULT NULL COMMENT '投票时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户投票';

-- ----------------------------
-- Records of re_contest_activity_vote
-- ----------------------------

-- ----------------------------
-- Table structure for `re_contest_activity_works`
-- ----------------------------
DROP TABLE IF EXISTS `re_contest_activity_works`;
CREATE TABLE `re_contest_activity_works` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '线上大赛作品表',
  `serial_number` varchar(10) DEFAULT NULL COMMENT '作品编号  大写字母开头   在加8位数字',
  `username` varchar(20) DEFAULT NULL COMMENT '作者姓名',
  `tel` varchar(20) DEFAULT NULL COMMENT '联系电话',
  `id_card` varchar(18) DEFAULT NULL COMMENT '身份证号码',
  `unit_id` int(11) DEFAULT NULL COMMENT '单位',
  `unit` varchar(100) DEFAULT NULL,
  `wechat_number` varchar(30) DEFAULT NULL COMMENT '微信号',
  `reader_id` varchar(20) DEFAULT NULL COMMENT '读者证号',
  `adviser` varchar(20) DEFAULT NULL COMMENT '指导老师',
  `adviser_tel` varchar(20) DEFAULT NULL COMMENT '指导老师联系方式',
  `user_id` int(10) unsigned DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '用户读者证号id',
  `title` varchar(50) DEFAULT NULL COMMENT '名称',
  `con_id` int(10) unsigned DEFAULT NULL COMMENT '线上大赛活动id',
  `type_id` int(10) DEFAULT NULL COMMENT '线上大赛活动类型id',
  `img` varchar(120) DEFAULT NULL COMMENT '作品图片地址 node=1',
  `content` longtext COMMENT '作品正文内容 node=2',
  `voice` varchar(120) DEFAULT NULL COMMENT '作品音频地址 node=3',
  `voice_name` varchar(80) DEFAULT NULL COMMENT '音频名称',
  `video` varchar(120) DEFAULT NULL COMMENT '视频资源 node=4',
  `video_name` varchar(80) DEFAULT NULL COMMENT '视频名称',
  `intro` varchar(255) DEFAULT NULL COMMENT '作品简介',
  `cover` varchar(120) DEFAULT NULL COMMENT '作品封面',
  `width` int(11) DEFAULT '0' COMMENT '宽度',
  `height` int(11) unsigned DEFAULT '0' COMMENT '高度',
  `terminal` varchar(10) DEFAULT NULL COMMENT '作品上传终端    web   android   ios  wx',
  `browse_num` int(10) unsigned DEFAULT '0' COMMENT '浏览量',
  `vote_num` int(10) unsigned DEFAULT '0' COMMENT '投票量',
  `status` tinyint(1) unsigned DEFAULT '3' COMMENT '状态    1.已通过   2.未通过   3.未审核(默认)  4.已撤销， 5已删除 ',
  `reason` varchar(300) DEFAULT NULL COMMENT '拒绝理由',
  `is_look` tinyint(1) DEFAULT '2' COMMENT '是否被查看 1已查看  2未查看',
  `create_time` datetime DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  `score` int(10) DEFAULT '0' COMMENT '积分',
  `is_violate` tinyint(1) DEFAULT '1' COMMENT '是否违规  1正常 2违规 默认1',
  `violate_reason` varchar(255) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '违规原因',
  `violate_time` datetime DEFAULT NULL COMMENT '违规时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `serial_number` (`serial_number`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='线上大赛作品表';

-- ----------------------------
-- Records of re_contest_activity_works
-- ----------------------------

-- ----------------------------
-- Table structure for `re_courier_name`
-- ----------------------------
DROP TABLE IF EXISTS `re_courier_name`;
CREATE TABLE `re_courier_name` (
  `id` int(1) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='快递公司名称';

-- ----------------------------
-- Records of re_courier_name
-- ----------------------------

-- ----------------------------
-- Table structure for `re_digital_access_num`
-- ----------------------------
DROP TABLE IF EXISTS `re_digital_access_num`;
CREATE TABLE `re_digital_access_num` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `digital_id` int(11) DEFAULT '0' COMMENT '数字资源id ',
  `type` tinyint(1) DEFAULT '1' COMMENT '类型   1.点击量   2. 下载量  3.浏览人次   4.评估年度 ',
  `number` int(11) DEFAULT '1' COMMENT '访问量',
  `yyy` int(4) DEFAULT NULL COMMENT '年',
  `mmm` tinyint(2) DEFAULT NULL COMMENT '月',
  `ddd` tinyint(2) DEFAULT NULL COMMENT '日',
  `create_time` datetime DEFAULT NULL COMMENT '数据创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='数字阅读资源访问统计（单个访问量）';

-- ----------------------------
-- Records of re_digital_access_num
-- ----------------------------

-- ----------------------------
-- Table structure for `re_digital_read`
-- ----------------------------
DROP TABLE IF EXISTS `re_digital_read`;
CREATE TABLE `re_digital_read` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '名称',
  `img` varchar(255) DEFAULT NULL COMMENT '图片',
  `url` varchar(500) DEFAULT NULL COMMENT '链接',
  `type_id` int(11) DEFAULT NULL COMMENT '类型id',
  `sort` int(11) DEFAULT '0' COMMENT '排序 数字越大越靠前',
  `browse_num` int(11) DEFAULT '0' COMMENT '浏览量',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1 正常  2 已删除',
  `create_time` datetime DEFAULT NULL COMMENT '数据插入时间',
  `change_time` datetime DEFAULT NULL COMMENT '数据上一次更新时间',
  `is_play` tinyint(1) DEFAULT '1' COMMENT '是否发布 1.发布 2.未发布    默认1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='数字阅读表';

-- ----------------------------
-- Records of re_digital_read
-- ----------------------------

-- ----------------------------
-- Table structure for `re_digital_type`
-- ----------------------------
DROP TABLE IF EXISTS `re_digital_type`;
CREATE TABLE `re_digital_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(20) DEFAULT NULL COMMENT '类型名称',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否被删除 1否 2是',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='数字资源类型表';

-- ----------------------------
-- Records of re_digital_type
-- ----------------------------

-- ----------------------------
-- Table structure for `re_district`
-- ----------------------------
DROP TABLE IF EXISTS `re_district`;
CREATE TABLE `re_district` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL COMMENT '多少年公布的数据',
  `name` varchar(100) DEFAULT NULL COMMENT '地区名称',
  `code` varchar(100) DEFAULT NULL COMMENT '地区代码',
  `level` tinyint(1) DEFAULT NULL COMMENT '等级 1省 2市 3区 4街道',
  `type` varchar(20) DEFAULT NULL COMMENT '城乡分类代码',
  `url` varchar(150) DEFAULT NULL,
  `is_spider` tinyint(1) DEFAULT '0' COMMENT '是否全部爬取 1是 0否',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `level_index` (`level`) USING BTREE,
  KEY `parent_index` (`parent_id`) USING BTREE,
  KEY `year_index` (`year`) USING BTREE,
  KEY `code_index` (`code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='省市区街道地址表';

-- ----------------------------
-- Records of re_district
-- ----------------------------

-- ----------------------------
-- Table structure for `re_feedback`
-- ----------------------------
DROP TABLE IF EXISTS `re_feedback`;
CREATE TABLE `re_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `tel` varchar(30) DEFAULT NULL COMMENT '电话号码',
  `feedback` text,
  `create_time` datetime DEFAULT NULL,
  `is_look` tinyint(1) DEFAULT '2' COMMENT '是否查看  1 已查看  2 未查看',
  `manage_feedback` text COMMENT '管理员回复',
  `manage_id` int(11) DEFAULT NULL COMMENT '回复管理id',
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='用户反馈表';

-- ----------------------------
-- Records of re_feedback
-- ----------------------------

-- ----------------------------
-- Table structure for `re_goods`
-- ----------------------------
DROP TABLE IF EXISTS `re_goods`;
CREATE TABLE `re_goods` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '商品名称',
  `type_id` int(11) DEFAULT NULL COMMENT '类型id',
  `img` varchar(120) DEFAULT NULL COMMENT '商品封面图片',
  `score` int(11) DEFAULT NULL COMMENT '所需积分',
  `price` decimal(8,2) DEFAULT NULL COMMENT '除了积分外，还需要多少金额 ',
  `number` int(11) DEFAULT NULL COMMENT '数量',
  `has_number` int(11) DEFAULT '0' COMMENT '已兑换数量',
  `times` int(11) DEFAULT '0' COMMENT '每个用户可兑换次数 默认0次(不限制兑换)',
  `tel` varchar(30) DEFAULT NULL COMMENT '联系方式',
  `contacts` varchar(30) DEFAULT NULL COMMENT '联系人',
  `send_way` tinyint(1) DEFAULT NULL COMMENT '1 自提 2 邮递  3、 2者都可以',
  `start_time` datetime DEFAULT NULL COMMENT '开始兑换时间',
  `end_time` datetime DEFAULT NULL COMMENT '截止兑换时间',
  `pick_end_time` datetime DEFAULT NULL COMMENT '自提截止时间',
  `cancel_end_time` int(11) unsigned DEFAULT '0' COMMENT '支付成功后，可取消的最后期限  单位 分钟',
  `is_play` tinyint(1) DEFAULT '1' COMMENT '1 上架  2下架  默认2',
  `intro` text COMMENT '简介',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员id',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1 正常  2 删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='商品表';

-- ----------------------------
-- Records of re_goods
-- ----------------------------

-- ----------------------------
-- Table structure for `re_goods_img`
-- ----------------------------
DROP TABLE IF EXISTS `re_goods_img`;
CREATE TABLE `re_goods_img` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) DEFAULT NULL COMMENT '商品id',
  `img` varchar(120) DEFAULT NULL COMMENT '商品图片',
  `thumb_img` varchar(120) DEFAULT NULL COMMENT '商品缩略图',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='商品图片表';

-- ----------------------------
-- Records of re_goods_img
-- ----------------------------

-- ----------------------------
-- Table structure for `re_goods_order`
-- ----------------------------
DROP TABLE IF EXISTS `re_goods_order`;
CREATE TABLE `re_goods_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_number` varchar(20) DEFAULT NULL COMMENT '订单号',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证号id 2020 05 09 新增 ',
  `goods_id` int(11) DEFAULT NULL COMMENT '商品id',
  `score` int(11) DEFAULT '0' COMMENT '兑换商品的积分',
  `price` decimal(8,2) DEFAULT NULL COMMENT '价格 null  表示 不需要支付金额',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态 1 待领取  2已取走（自提） 3待发货  4已发货 （邮递）  5 已过期   6 已取消',
  `is_pay` tinyint(1) DEFAULT '1' COMMENT '1 未支付 2 已支付    3支付异常， 4 订单已失效 5 .已退款',
  `payment` tinyint(1) DEFAULT '1' COMMENT '支付方式  1 微信支付   2 其他',
  `refund_number` varchar(20) DEFAULT NULL COMMENT '退款单号',
  `refund_time` datetime DEFAULT NULL COMMENT '退款时间',
  `refund_remark` varchar(500) DEFAULT NULL COMMENT '退款备注',
  `refund_manage_id` int(11) DEFAULT NULL COMMENT '退款管理员id 为空表示  自己主动退款',
  `redeem_code` varchar(10) DEFAULT NULL COMMENT '兑换码（8位）',
  `send_way` tinyint(1) DEFAULT NULL COMMENT '1 自提 2 邮递',
  `tracking_number` varchar(20) DEFAULT NULL COMMENT '运单号',
  `take_time` datetime DEFAULT NULL COMMENT '确认发货、领取时间',
  `take_manage_id` int(11) DEFAULT NULL COMMENT '确认发货、领取 的管理员id',
  `cancel_end_time` datetime DEFAULT NULL COMMENT '支付后可取消的最后阶段',
  `settle_state` tinyint(1) DEFAULT '3' COMMENT '结算状态 只有涉及金额的才有结算（这里只标记就行）  1 已结算  2 结算中 3 未结算',
  `settle_shop_time` datetime DEFAULT NULL COMMENT '结算时间（书店结算）',
  `settle_lib_time` datetime DEFAULT NULL,
  `settle_shop_manage_id` int(11) DEFAULT NULL COMMENT '书店结算的管理员id',
  `settle_lib_manage_id` int(11) DEFAULT NULL COMMENT '图书馆结算的管理员id',
  `create_time` datetime DEFAULT NULL COMMENT '加入时间（订单时间）',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间  （支付时间，支付异常时间，订单失效后的时间）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='商品订单表';

-- ----------------------------
-- Records of re_goods_order
-- ----------------------------

-- ----------------------------
-- Table structure for `re_goods_order_address`
-- ----------------------------
DROP TABLE IF EXISTS `re_goods_order_address`;
CREATE TABLE `re_goods_order_address` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL COMMENT '订单id',
  `username` varchar(30) DEFAULT NULL COMMENT '用户姓名',
  `tel` varchar(20) DEFAULT NULL COMMENT '电话号码',
  `province` varchar(30) DEFAULT NULL COMMENT '省',
  `city` varchar(30) DEFAULT NULL,
  `district` varchar(30) DEFAULT NULL COMMENT '区',
  `street` varchar(30) DEFAULT NULL COMMENT '街道',
  `address` varchar(200) DEFAULT NULL COMMENT '详细地址',
  `send_username` varchar(30) DEFAULT NULL COMMENT '发货人名称',
  `send_tel` varchar(20) DEFAULT NULL COMMENT '发货人电话',
  `send_province` varchar(30) DEFAULT '' COMMENT '省(发货）',
  `send_city` varchar(30) DEFAULT '' COMMENT '市(发货）',
  `send_district` varchar(30) DEFAULT '' COMMENT '区(发货）',
  `send_street` varchar(30) DEFAULT '' COMMENT '街道(发货）',
  `send_address` varchar(200) DEFAULT '' COMMENT '详细地址(发货）',
  `manage_id` int(11) DEFAULT NULL COMMENT '修改管理员id',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime DEFAULT NULL COMMENT '删除、修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户订单地址表';

-- ----------------------------
-- Records of re_goods_order_address
-- ----------------------------

-- ----------------------------
-- Table structure for `re_goods_order_pay`
-- ----------------------------
DROP TABLE IF EXISTS `re_goods_order_pay`;
CREATE TABLE `re_goods_order_pay` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_number` varchar(20) DEFAULT NULL COMMENT '订单号',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `payment` tinyint(1) DEFAULT '1' COMMENT '支付方式  1 微信支付',
  `trade_no` varchar(50) DEFAULT NULL COMMENT '微信支付订单号',
  `buyer_id` varchar(50) DEFAULT NULL COMMENT '买家微信支付分配的终端设备号',
  `total_amount` decimal(8,2) DEFAULT NULL COMMENT '订单总金额,订单总金额，单位为分',
  `receipt_amount` decimal(8,2) DEFAULT NULL COMMENT '实收金额,商家在交易中实际收到的款项',
  `buyer_pay_amount` decimal(8,2) DEFAULT NULL COMMENT '付款金额,现金支付金额订单现金支付金额，详见支付金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='商品订单支付表';

-- ----------------------------
-- Records of re_goods_order_pay
-- ----------------------------

-- ----------------------------
-- Table structure for `re_goods_order_refund`
-- ----------------------------
DROP TABLE IF EXISTS `re_goods_order_refund`;
CREATE TABLE `re_goods_order_refund` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `refund_number` varchar(20) DEFAULT NULL COMMENT '退款单号',
  `refund_id` varchar(50) DEFAULT NULL COMMENT '微信退款单号',
  `refund_time` datetime DEFAULT NULL COMMENT '退款时间',
  `price` decimal(5,2) DEFAULT NULL COMMENT '退款金额',
  `payment` tinyint(1) DEFAULT NULL COMMENT '退款路径  1 微信',
  `status` tinyint(1) DEFAULT NULL COMMENT '退款结果  1 成功  2失败',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员id',
  `error_code` varchar(20) DEFAULT NULL COMMENT '退款失败的code',
  `error_msg` varchar(150) DEFAULT NULL COMMENT '退款失败的消息提示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='订单退款日志表';

-- ----------------------------
-- Records of re_goods_order_refund
-- ----------------------------

-- ----------------------------
-- Table structure for `re_goods_pick_address`
-- ----------------------------
DROP TABLE IF EXISTS `re_goods_pick_address`;
CREATE TABLE `re_goods_pick_address` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `province` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `district` varchar(50) DEFAULT NULL,
  `street` varchar(30) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `tel` varchar(15) DEFAULT NULL COMMENT '联系电话',
  `contacts` varchar(20) DEFAULT NULL COMMENT '联系人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='商品 自提地址';

-- ----------------------------
-- Records of re_goods_pick_address
-- ----------------------------

-- ----------------------------
-- Table structure for `re_goods_type`
-- ----------------------------
DROP TABLE IF EXISTS `re_goods_type`;
CREATE TABLE `re_goods_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(30) DEFAULT NULL COMMENT '类型',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1 成功  2 删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='商品类型表';

-- ----------------------------
-- Records of re_goods_type
-- ----------------------------

-- ----------------------------
-- Table structure for `re_hot_search`
-- ----------------------------
DROP TABLE IF EXISTS `re_hot_search`;
CREATE TABLE `re_hot_search` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(30) DEFAULT NULL COMMENT '检索热词',
  `number` int(11) DEFAULT NULL COMMENT '次数',
  `type` tinyint(1) DEFAULT NULL COMMENT '类型 1 图书到家新书   2 图书到家新书馆藏书  3 单纯馆藏书检索',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='热门检索表';

-- ----------------------------
-- Records of re_hot_search
-- ----------------------------

-- ----------------------------
-- Table structure for `re_lib_book`
-- ----------------------------
DROP TABLE IF EXISTS `re_lib_book`;
CREATE TABLE `re_lib_book` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `metaid` int(11) DEFAULT NULL COMMENT '馆藏书籍id',
  `metatable` varchar(20) DEFAULT NULL COMMENT '书目库',
  `book_name` varchar(300) DEFAULT NULL COMMENT '书名',
  `author` varchar(100) DEFAULT NULL COMMENT '作者',
  `press` varchar(50) DEFAULT NULL COMMENT '出版社',
  `pre_time` varchar(50) DEFAULT NULL COMMENT '出版时间',
  `isbn` varchar(20) DEFAULT NULL COMMENT 'ISBN号',
  `old_isbn` varchar(20) DEFAULT NULL COMMENT '原本的isbn号',
  `price` varchar(20) DEFAULT NULL COMMENT '金额',
  `img` varchar(120) DEFAULT 'default/default_lib_book.png' COMMENT '封面图片',
  `intro` text COMMENT '简介',
  `book_num` varchar(50) DEFAULT NULL COMMENT '索书号',
  `type_id` int(11) DEFAULT '0' COMMENT '类型id  （自己手动分类）',
  `type_big_id` int(11) DEFAULT NULL COMMENT '对应的中图分类法id  只需要一级大类',
  `holding_num` int(11) DEFAULT NULL COMMENT '馆藏数量',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  `access_number` int(5) DEFAULT '0' COMMENT '获取图片次数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='(馆藏书籍表)（有实际操作才放入数据库）';

-- ----------------------------
-- Records of re_lib_book
-- ----------------------------

-- ----------------------------
-- Table structure for `re_lib_book_barcode`
-- ----------------------------
DROP TABLE IF EXISTS `re_lib_book_barcode`;
CREATE TABLE `re_lib_book_barcode` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `book_id` int(11) DEFAULT NULL COMMENT '对应馆藏书籍id',
  `barcode` varchar(20) DEFAULT NULL COMMENT '条形码',
  `book_num` varchar(20) DEFAULT NULL COMMENT '索书号',
  `init_sub_lib` varchar(50) DEFAULT NULL COMMENT '馆藏所属馆备注',
  `init_local` varchar(50) DEFAULT NULL COMMENT '馆藏所属地备注',
  `cur_sub_lib` varchar(50) DEFAULT NULL COMMENT '馆藏当前所在馆备注',
  `cur_local` varchar(50) DEFAULT NULL COMMENT '馆藏当前所在地备注',
  `init_sub_lib_note` varchar(30) DEFAULT NULL,
  `init_local_note` varchar(30) DEFAULT NULL,
  `cur_sub_lib_note` varchar(30) DEFAULT NULL,
  `cur_local_note` varchar(30) DEFAULT NULL,
  `reg_date` date DEFAULT NULL,
  `ecard_flag` varchar(10) DEFAULT NULL,
  `status` varchar(2) DEFAULT NULL COMMENT '馆藏状态',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='书店书籍对应条形码表';

-- ----------------------------
-- Records of re_lib_book_barcode
-- ----------------------------

-- ----------------------------
-- Table structure for `re_lib_book_recom`
-- ----------------------------
DROP TABLE IF EXISTS `re_lib_book_recom`;
CREATE TABLE `re_lib_book_recom` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `metaid` int(11) DEFAULT NULL COMMENT '馆藏书籍id',
  `metatable` varchar(20) DEFAULT NULL COMMENT '书目库',
  `book_name` varchar(200) DEFAULT NULL COMMENT '书名',
  `author` varchar(150) DEFAULT NULL COMMENT '作者',
  `press` varchar(60) DEFAULT NULL,
  `pre_time` varchar(20) DEFAULT NULL COMMENT '出版时间',
  `isbn` varchar(20) DEFAULT NULL COMMENT 'ISBN号',
  `old_isbn` varchar(35) DEFAULT NULL COMMENT '原本的isbn号',
  `barcode` varchar(20) DEFAULT NULL COMMENT '条形码',
  `price` varchar(30) DEFAULT NULL COMMENT '金额  ',
  `img` varchar(120) DEFAULT 'default/default_lib_book.png',
  `book_num` varchar(50) DEFAULT NULL COMMENT '索书号',
  `intro` text COMMENT '简介',
  `type_id` int(11) DEFAULT NULL COMMENT '类型id',
  `is_recom` tinyint(1) DEFAULT '1' COMMENT '是否推荐  1 推荐  2 未推荐 ',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1 正常  2 删除 ',
  `access_number` int(5) DEFAULT '0' COMMENT '获取图片次数',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='馆藏推荐书籍表';

-- ----------------------------
-- Records of re_lib_book_recom
-- ----------------------------

-- ----------------------------
-- Table structure for `re_lib_book_type`
-- ----------------------------
DROP TABLE IF EXISTS `re_lib_book_type`;
CREATE TABLE `re_lib_book_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(100) DEFAULT NULL COMMENT '类型名称',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1 正常  2 删除 ',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='书馆藏推荐书籍类型表';

-- ----------------------------
-- Records of re_lib_book_type
-- ----------------------------

-- ----------------------------
-- Table structure for `re_lib_borrow_log`
-- ----------------------------
DROP TABLE IF EXISTS `re_lib_borrow_log`;
CREATE TABLE `re_lib_borrow_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` char(32) DEFAULT NULL COMMENT '用户id',
  `book_name` varchar(80) DEFAULT NULL COMMENT '书名',
  `type` varchar(20) DEFAULT NULL COMMENT '书的类别   22大类',
  `small_type` int(11) DEFAULT NULL COMMENT '小类型',
  `author` varchar(80) DEFAULT '' COMMENT '作者',
  `price` float(5,2) DEFAULT NULL COMMENT '价格',
  `press` varchar(50) DEFAULT '' COMMENT '出版社',
  `isbn` varchar(20) DEFAULT '' COMMENT 'Isbn号',
  `barcode` varchar(20) DEFAULT '' COMMENT '条码号	主要是处理图书馆的书籍，图书馆的系统都有统一的条码',
  `img` varchar(120) DEFAULT 'default/default_book.png' COMMENT '图片',
  `thumb_img` varchar(120) DEFAULT NULL COMMENT '封面缩略图',
  `pre_time` varchar(20) DEFAULT '' COMMENT '出版时间',
  `callno` varchar(50) DEFAULT '' COMMENT '索书号',
  `classno` varchar(50) DEFAULT NULL,
  `intro` text COMMENT '简介',
  `handler` varchar(10) DEFAULT NULL COMMENT '是否是管理员操作	YES_APP(手机管理员)，YES_WEB（web管理员）,NO，YES_APP，YES_WEB的话，user_id就为管理员的id',
  `borrow_time` datetime DEFAULT NULL COMMENT '借阅时间',
  `expire_time` datetime DEFAULT NULL COMMENT '预计归还时间    时间戳',
  `return_time` datetime DEFAULT NULL COMMENT '归还时间   日期时间格式',
  `status` tinyint(1) DEFAULT '2' COMMENT '状态  1 已归还  2 借阅中',
  `create_time` datetime DEFAULT NULL COMMENT '借阅时间',
  `rdrrecno` int(10) DEFAULT NULL COMMENT '读者记录号',
  `lib_id` varchar(30) DEFAULT NULL COMMENT '所属馆',
  `lib_name` varchar(30) DEFAULT NULL COMMENT '所属馆',
  `account` varchar(20) DEFAULT NULL COMMENT '读者证号',
  `cur_local` varchar(100) DEFAULT NULL,
  `cur_local_name` varchar(100) DEFAULT NULL,
  `cur_lib` varchar(100) DEFAULT NULL,
  `cur_lib_name` varchar(100) DEFAULT NULL,
  `owner_local` varchar(100) DEFAULT NULL,
  `owner_local_name` varchar(100) DEFAULT NULL,
  `renew_num` int(11) DEFAULT '0' COMMENT '续借次数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='作为图书馆借阅记录表，实际可不用';

-- ----------------------------
-- Records of re_lib_borrow_log
-- ----------------------------

-- ----------------------------
-- Table structure for `re_manage`
-- ----------------------------
DROP TABLE IF EXISTS `re_manage`;
CREATE TABLE `re_manage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(30) DEFAULT NULL COMMENT '账号',
  `username` varchar(20) DEFAULT NULL COMMENT '用户名',
  `password` varchar(32) DEFAULT NULL COMMENT '密码(md5)',
  `tel` varchar(15) NOT NULL DEFAULT '' COMMENT '用户电话',
  `create_time` datetime DEFAULT NULL COMMENT '数据插入时间',
  `change_time` datetime DEFAULT NULL COMMENT '数据更新时间',
  `token` varchar(32) NOT NULL COMMENT '用户令牌，每次登录刷新一次token（单点登录）',
  `expire_time` datetime DEFAULT NULL COMMENT '令牌过期时间',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除   1.正常(默认)  2.已删除',
  `manage_id` int(10) DEFAULT NULL,
  `way` tinyint(1) DEFAULT '1' COMMENT '管辖位置   1 全部  2 图书馆  3 书店',
  `end_login_time` datetime DEFAULT NULL,
  `end_login_ip` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`,`tel`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='后台管理员表';

-- ----------------------------
-- Records of re_manage
-- ----------------------------
INSERT INTO `re_manage` VALUES ('1', 'admin', 'admin', 'a5fbf270764293be79c88838f33f17e7', '', '2023-01-03 13:57:24', '2023-11-09 13:47:22', 'c4072931b3cd9044d8e0a7615c9db44f', '2023-11-09 15:47:22', '1', null, '1', '2023-11-09 13:43:46', '182.139.67.36');
INSERT INTO `re_manage` VALUES ('2', 'dylib', '图书馆管理员', '895a5e98af111fd6a4cc37075d422c33', '15200000000', '2023-11-09 13:40:43', '2023-11-09 13:48:55', 'fa0a1d5271f1ee025fce2dbde8a8e486', '2023-11-09 15:48:55', '1', '1', '1', '2023-11-09 13:48:03', '182.139.67.36');

-- ----------------------------
-- Table structure for `re_manage_login_log`
-- ----------------------------
DROP TABLE IF EXISTS `re_manage_login_log`;
CREATE TABLE `re_manage_login_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `manage_id` int(11) DEFAULT NULL,
  `login_time` datetime DEFAULT NULL COMMENT '登陆时间',
  `login_addr` varchar(50) DEFAULT NULL COMMENT '登陆地址',
  `login_ip` varchar(20) DEFAULT NULL COMMENT '登陆ip',
  `is_success` tinyint(1) DEFAULT '2' COMMENT '是否成功 1 成功  2 失败',
  `is_lock` tinyint(1) DEFAULT '1' COMMENT '是否锁定  1 正常  2 锁定',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='管理员登陆日志表';

-- ----------------------------
-- Records of re_manage_login_log
-- ----------------------------
INSERT INTO `re_manage_login_log` VALUES ('1', '2', '2023-11-09 13:40:57', null, '182.139.67.36', '2', '1');
INSERT INTO `re_manage_login_log` VALUES ('2', '2', '2023-11-09 13:41:11', null, '182.139.67.36', '1', '1');
INSERT INTO `re_manage_login_log` VALUES ('3', '1', '2023-11-09 13:43:46', null, '182.139.67.36', '1', '1');
INSERT INTO `re_manage_login_log` VALUES ('4', '2', '2023-11-09 13:47:51', null, '182.139.67.36', '2', '1');
INSERT INTO `re_manage_login_log` VALUES ('5', '2', '2023-11-09 13:48:03', null, '182.139.67.36', '1', '1');

-- ----------------------------
-- Table structure for `re_manage_role`
-- ----------------------------
DROP TABLE IF EXISTS `re_manage_role`;
CREATE TABLE `re_manage_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manage_id` int(11) DEFAULT NULL COMMENT '人员id',
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='管理员 角色关联表';

-- ----------------------------
-- Records of re_manage_role
-- ----------------------------
INSERT INTO `re_manage_role` VALUES ('2', '2', '1', '2023-11-09 13:46:09');

-- ----------------------------
-- Table structure for `re_migrations`
-- ----------------------------
DROP TABLE IF EXISTS `re_migrations`;
CREATE TABLE `re_migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of re_migrations
-- ----------------------------

-- ----------------------------
-- Table structure for `re_navigation_area`
-- ----------------------------
DROP TABLE IF EXISTS `re_navigation_area`;
CREATE TABLE `re_navigation_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `build_id` int(11) DEFAULT NULL COMMENT '所属建筑id',
  `name` varchar(255) DEFAULT NULL COMMENT '区域名称',
  `alias_name` varchar(255) DEFAULT NULL COMMENT '区域别名',
  `img` varchar(255) DEFAULT NULL COMMENT '楼层svg图',
  `rotation_angle` varchar(11) DEFAULT '0' COMMENT '指北针偏移角度 0~360',
  `is_start` tinyint(1) DEFAULT '2' COMMENT '是否建筑起点 1起点 2非起点,默认2',
  `level` tinyint(1) DEFAULT '1' COMMENT '级别 默认1级 2级为房间内部区域',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `is_play` tinyint(1) DEFAULT '1' COMMENT '是否展示1正常 2停用',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除 1正常 2删除',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  `sort` int(11) DEFAULT '0' COMMENT '排序 数字越大越靠前',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='导航线路图';

-- ----------------------------
-- Records of re_navigation_area
-- ----------------------------

-- ----------------------------
-- Table structure for `re_navigation_build`
-- ----------------------------
DROP TABLE IF EXISTS `re_navigation_build`;
CREATE TABLE `re_navigation_build` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) DEFAULT NULL COMMENT '建筑名称',
  `province` varchar(30) DEFAULT NULL COMMENT '省',
  `city` varchar(20) DEFAULT NULL COMMENT '市',
  `district` varchar(20) DEFAULT NULL COMMENT '区县',
  `address` varchar(255) DEFAULT NULL COMMENT '具体地址',
  `is_play` tinyint(1) DEFAULT '1' COMMENT '是否展示1正常 2停用',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `is_del` tinyint(1) DEFAULT '1' COMMENT ' 是否删除 1正常 2删除',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='导航线路图';

-- ----------------------------
-- Records of re_navigation_build
-- ----------------------------

-- ----------------------------
-- Table structure for `re_navigation_point`
-- ----------------------------
DROP TABLE IF EXISTS `re_navigation_point`;
CREATE TABLE `re_navigation_point` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) DEFAULT NULL COMMENT '点位名称',
  `area_id` int(11) DEFAULT NULL COMMENT '所属区域id',
  `transtion_area_id` varchar(255) DEFAULT NULL COMMENT '过渡区域，多个用,链接',
  `line` text COMMENT '路线代码',
  `type` tinyint(1) DEFAULT '2' COMMENT '类型 1起点 2目的地 3过渡点 4起点/过渡点',
  `is_play` tinyint(1) DEFAULT '1' COMMENT '是否禁用 1 正常 2禁用',
  `level` tinyint(1) DEFAULT '1' COMMENT '级别 默认1级',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  `sort` int(11) DEFAULT '0' COMMENT '排序 数字越大越靠前',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除 1 正常 2 删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='室内分布表';

-- ----------------------------
-- Records of re_navigation_point
-- ----------------------------

-- ----------------------------
-- Table structure for `re_new_book_recommend`
-- ----------------------------
DROP TABLE IF EXISTS `re_new_book_recommend`;
CREATE TABLE `re_new_book_recommend` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `book_name` varchar(100) DEFAULT NULL COMMENT '书名',
  `author` varchar(100) DEFAULT NULL COMMENT '作者',
  `press` varchar(60) DEFAULT NULL COMMENT '出版社',
  `pre_time` varchar(20) DEFAULT NULL COMMENT '出版时间',
  `isbn` varchar(20) DEFAULT NULL COMMENT 'ISBN号',
  `price` decimal(8,2) DEFAULT NULL COMMENT '金额',
  `img` varchar(120) DEFAULT 'default/default_new_book.png' COMMENT '封面图片',
  `book_num` varchar(30) DEFAULT NULL,
  `intro` text COMMENT '简介',
  `type_id` int(11) DEFAULT '0' COMMENT '类型id   0 代表没有分类',
  `is_recom` tinyint(1) DEFAULT '1' COMMENT '是否为推荐图书  1 是 2 否',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1 正常  2 删除',
  `qr_url` varchar(100) DEFAULT NULL COMMENT '书籍二维码链接',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `type_id` (`type_id`) USING BTREE,
  KEY `isbn` (`isbn`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='新书推荐书籍表';

-- ----------------------------
-- Records of re_new_book_recommend
-- ----------------------------

-- ----------------------------
-- Table structure for `re_new_book_recommend_collect`
-- ----------------------------
DROP TABLE IF EXISTS `re_new_book_recommend_collect`;
CREATE TABLE `re_new_book_recommend_collect` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `book_id` int(11) DEFAULT NULL COMMENT '书籍id',
  `create_time` datetime DEFAULT NULL COMMENT '收藏时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='书籍收藏表';

-- ----------------------------
-- Records of re_new_book_recommend_collect
-- ----------------------------

-- ----------------------------
-- Table structure for `re_new_book_recommend_type`
-- ----------------------------
DROP TABLE IF EXISTS `re_new_book_recommend_type`;
CREATE TABLE `re_new_book_recommend_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) DEFAULT NULL COMMENT '类型名称',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否被删除 1否 2是',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of re_new_book_recommend_type
-- ----------------------------

-- ----------------------------
-- Table structure for `re_news`
-- ----------------------------
DROP TABLE IF EXISTS `re_news`;
CREATE TABLE `re_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL COMMENT '新闻标题',
  `img` varchar(200) DEFAULT NULL COMMENT '封面',
  `type_id` int(11) DEFAULT NULL COMMENT '新闻类型id',
  `content` text COMMENT '新闻正文',
  `browse_num` int(11) DEFAULT '0' COMMENT '浏览量',
  `is_top` tinyint(1) DEFAULT '0' COMMENT '是否置顶   只能置顶3条  越大越在前  0表示未置顶',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除 1否 2是',
  `add_time` datetime DEFAULT NULL COMMENT '用户手动选择的创建时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='阅享精选';

-- ----------------------------
-- Records of re_news
-- ----------------------------

-- ----------------------------
-- Table structure for `re_news_collect`
-- ----------------------------
DROP TABLE IF EXISTS `re_news_collect`;
CREATE TABLE `re_news_collect` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_id` int(11) DEFAULT NULL COMMENT '新闻id',
  `user_id` int(11) DEFAULT '1' COMMENT '用户id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='阅享精选类型表';

-- ----------------------------
-- Records of re_news_collect
-- ----------------------------

-- ----------------------------
-- Table structure for `re_news_type`
-- ----------------------------
DROP TABLE IF EXISTS `re_news_type`;
CREATE TABLE `re_news_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(20) DEFAULT NULL COMMENT '类型名称',
  `type` tinyint(1) DEFAULT '1' COMMENT '列表数据展示方式   1 列表   2 图文展示',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否被删除 1否 2是',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='阅享精选类型表';

-- ----------------------------
-- Records of re_news_type
-- ----------------------------

-- ----------------------------
-- Table structure for `re_online_registration`
-- ----------------------------
DROP TABLE IF EXISTS `re_online_registration`;
CREATE TABLE `re_online_registration` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(32) DEFAULT NULL,
  `shop_id` int(11) DEFAULT NULL COMMENT '书店id',
  `order_id` int(11) DEFAULT NULL COMMENT '订单id 为null表示不需要支付',
  `reader_id` varchar(20) DEFAULT NULL COMMENT '读者证号',
  `certificate_type` tinyint(1) DEFAULT '1' COMMENT '证件类型  1 为身份证  2 为其他证',
  `certificate_num` varchar(20) DEFAULT NULL COMMENT '证件号码',
  `username` varchar(30) DEFAULT NULL COMMENT '姓名',
  `id_card` varchar(20) DEFAULT NULL COMMENT '身份证号码',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `sex` tinyint(1) DEFAULT NULL COMMENT '性别  1 男  2 女',
  `card_type` varchar(10) DEFAULT NULL COMMENT '读者证类型',
  `workunit` varchar(100) DEFAULT NULL COMMENT '工作单位',
  `province` varchar(20) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `district` varchar(30) DEFAULT NULL,
  `street` varchar(50) DEFAULT NULL COMMENT '街道',
  `address` varchar(100) DEFAULT NULL COMMENT '联系地址',
  `card_front` varchar(120) DEFAULT NULL COMMENT '身份证正面 ',
  `card_reverse` varchar(120) DEFAULT NULL COMMENT '身份证反面',
  `zip` varchar(50) DEFAULT NULL COMMENT '邮编',
  `tel` varchar(20) DEFAULT NULL COMMENT '手机号码',
  `phone` varchar(20) DEFAULT NULL COMMENT '电话号码',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `culture_id` int(11) DEFAULT NULL,
  `culture` varchar(20) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `position` varchar(20) DEFAULT NULL,
  `pay_way` tinyint(2) DEFAULT '1' COMMENT '1  现金  2 微信',
  `cash` decimal(6,2) DEFAULT '0.00' COMMENT '押金 单位元',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `is_pay` tinyint(1) DEFAULT '2' COMMENT '支付状态 1 未支付 2 已支付    3支付异常， 4 订单已失效 5 .已退款',
  `source` tinyint(1) DEFAULT '1' COMMENT '办证来源 1前端 2后台',
  `status` tinyint(1) DEFAULT '2' COMMENT '是否领取 1 已领取  2 待领取  3 无需领取   4、已注销',
  `create_time` datetime DEFAULT NULL,
  `certificate_manage_id` int(11) DEFAULT NULL,
  `settle_state` tinyint(1) DEFAULT '3' COMMENT '结算状态（这里只标记就行）  1 已结算  2 结算中 3 未结算',
  `settle_sponsor_time` datetime DEFAULT NULL COMMENT '结算时间（书店结算）',
  `settle_sponsor_manage_id` int(11) DEFAULT NULL COMMENT '书店结算的管理员id',
  `settle_affirm_time` datetime DEFAULT NULL,
  `settle_affirm_manage_id` int(11) DEFAULT NULL COMMENT '图书馆结算时间',
  `change_time` datetime DEFAULT NULL COMMENT '领取时间',
  `node` tinyint(1) DEFAULT '2' COMMENT '是否写入成功  1 成功  2 失败',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='在线办证表';

-- ----------------------------
-- Records of re_online_registration
-- ----------------------------

-- ----------------------------
-- Table structure for `re_online_registration_config`
-- ----------------------------
DROP TABLE IF EXISTS `re_online_registration_config`;
CREATE TABLE `re_online_registration_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `intro` text COMMENT '简介',
  `real_info` varchar(1000) DEFAULT NULL COMMENT '报名所需信息 多个 | 链接    1、姓名 2、性别 3、生日  4、身份证号码  5、民族 6、政治面貌  7、健康状态  8、联系电话  9、毕业院校  10、专业  11、工作单位 12、教育程度  13、个人简介  14、特长  15、地址  ',
  `real_info_must` varchar(1000) DEFAULT NULL COMMENT '信息必填参数 多个 | 链接     1、姓名 2、性别 3、生日  4、身份证号码  5、民族 6、政治面貌  7、健康状态  8、联系电话  9、毕业院校  10、专业  11、工作单位 12、教育程度  13、个人简介  14、特长  15、用户地址 ',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '删除  1 正常 2删除',
  `is_play` tinyint(1) DEFAULT '2' COMMENT '是否发布 1.发布 2.未发布    默认2',
  `create_time` datetime DEFAULT NULL COMMENT '开始时间',
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='志愿者岗位';

-- ----------------------------
-- Records of re_online_registration_config
-- ----------------------------

-- ----------------------------
-- Table structure for `re_online_registration_order`
-- ----------------------------
DROP TABLE IF EXISTS `re_online_registration_order`;
CREATE TABLE `re_online_registration_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_number` varchar(20) DEFAULT NULL COMMENT '订单号',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证号id 2020 05 09 新增 ',
  `shop_id` int(11) DEFAULT NULL COMMENT '书店 id',
  `price` decimal(5,2) DEFAULT NULL COMMENT '押金 价格',
  `dis_price` decimal(5,2) DEFAULT NULL COMMENT '折扣后的价格  （默认与实际价格一致）   用户实际支付金额',
  `discount` float(5,2) DEFAULT '100.00' COMMENT '折扣   默认 100  没有折扣  百分比',
  `is_pay` tinyint(1) DEFAULT '2' COMMENT '1 未支付 2 已支付    3支付异常， 4 订单已失效     5 .已退款',
  `create_time` datetime DEFAULT NULL COMMENT '加入时间（订单时间）',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间  （支付时间，支付异常时间，订单失效后的时间）',
  `payment` tinyint(1) DEFAULT '1' COMMENT '支付方式  1 微信支付 ',
  `refund_number` varchar(20) DEFAULT NULL COMMENT '退款单号',
  `refund_time` datetime DEFAULT NULL COMMENT '退款时间',
  `refund_remark` varchar(500) DEFAULT NULL COMMENT '退款备注',
  `refund_manage_id` int(11) DEFAULT NULL COMMENT '退款管理员id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='在线办证订单表';

-- ----------------------------
-- Records of re_online_registration_order
-- ----------------------------

-- ----------------------------
-- Table structure for `re_online_registration_order_pay`
-- ----------------------------
DROP TABLE IF EXISTS `re_online_registration_order_pay`;
CREATE TABLE `re_online_registration_order_pay` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_number` varchar(20) DEFAULT NULL COMMENT '订单号',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `payment` tinyint(1) DEFAULT '1' COMMENT '支付方式  1 微信支付',
  `trade_no` varchar(50) DEFAULT NULL COMMENT '微信支付订单号',
  `buyer_id` varchar(50) DEFAULT NULL COMMENT '买家微信支付分配的终端设备号',
  `total_amount` decimal(8,2) DEFAULT NULL COMMENT '订单总金额,订单总金额，单位为分',
  `receipt_amount` decimal(8,2) DEFAULT NULL COMMENT '实收金额,商家在交易中实际收到的款项',
  `buyer_pay_amount` decimal(8,2) DEFAULT NULL COMMENT '付款金额,现金支付金额订单现金支付金额，详见支付金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='在线办证订单支付表';

-- ----------------------------
-- Records of re_online_registration_order_pay
-- ----------------------------

-- ----------------------------
-- Table structure for `re_online_registration_order_refund`
-- ----------------------------
DROP TABLE IF EXISTS `re_online_registration_order_refund`;
CREATE TABLE `re_online_registration_order_refund` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `refund_number` varchar(20) DEFAULT NULL COMMENT '退款单号',
  `refund_id` varchar(255) DEFAULT NULL COMMENT '微信退款单号',
  `refund_time` datetime DEFAULT NULL COMMENT '退款时间',
  `price` decimal(5,2) DEFAULT NULL COMMENT '退款金额',
  `payment` tinyint(1) DEFAULT NULL COMMENT '退款路径  1 微信',
  `status` tinyint(1) DEFAULT NULL COMMENT '退款结果  1 成功  2失败',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员id',
  `error_code` varchar(20) DEFAULT NULL COMMENT '退款失败的code',
  `error_msg` varchar(150) DEFAULT NULL COMMENT '退款失败的消息提示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='订单退款日志表';

-- ----------------------------
-- Records of re_online_registration_order_refund
-- ----------------------------

-- ----------------------------
-- Table structure for `re_other_access_num`
-- ----------------------------
DROP TABLE IF EXISTS `re_other_access_num`;
CREATE TABLE `re_other_access_num` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `total_num` int(11) DEFAULT '0' COMMENT '总浏览量',
  `type` tinyint(4) DEFAULT '1' COMMENT '    1 数字阅读  2 图书到家 3 活动 4 场馆预约 5 空间预约 6阅读互动',
  `yyy` int(4) DEFAULT NULL COMMENT '年',
  `mmm` tinyint(2) DEFAULT NULL COMMENT '月',
  `ddd` tinyint(2) DEFAULT NULL COMMENT '日',
  `create_time` datetime DEFAULT NULL COMMENT '数据创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='其他界面访问统计';

-- ----------------------------
-- Records of re_other_access_num
-- ----------------------------

-- ----------------------------
-- Table structure for `re_owe_order`
-- ----------------------------
DROP TABLE IF EXISTS `re_owe_order`;
CREATE TABLE `re_owe_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_number` varchar(20) DEFAULT NULL COMMENT '订单号',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证号id  ',
  `score` int(11) DEFAULT '0' COMMENT '抵扣的积分',
  `price` decimal(8,2) DEFAULT '0.00' COMMENT '价格 null  表示 不需要支付金额  单位元',
  `price_fen` int(11) DEFAULT NULL,
  `is_pay` tinyint(1) DEFAULT '1' COMMENT '1 未支付 2 已支付    3支付异常， 4 订单已失效 5 .已退款',
  `payment` tinyint(1) DEFAULT '1' COMMENT '支付方式  1 微信支付   2 积分抵扣  3其他',
  `refund_number` varchar(20) DEFAULT NULL COMMENT '退款单号',
  `refund_time` datetime DEFAULT NULL COMMENT '退款时间',
  `refund_remark` varchar(500) DEFAULT NULL COMMENT '退款备注',
  `create_time` datetime DEFAULT NULL COMMENT '加入时间（订单时间）',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间  （支付时间，支付异常时间，订单失效后的时间）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='欠费订单表';

-- ----------------------------
-- Records of re_owe_order
-- ----------------------------

-- ----------------------------
-- Table structure for `re_owe_order_pay`
-- ----------------------------
DROP TABLE IF EXISTS `re_owe_order_pay`;
CREATE TABLE `re_owe_order_pay` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_number` varchar(20) DEFAULT NULL COMMENT '订单号',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `payment` tinyint(1) DEFAULT '1' COMMENT '支付方式  1 微信支付',
  `trade_no` varchar(50) DEFAULT NULL COMMENT '微信支付订单号',
  `buyer_id` varchar(50) DEFAULT NULL COMMENT '买家微信支付分配的终端设备号',
  `total_amount` decimal(8,2) DEFAULT NULL COMMENT '订单总金额,订单总金额，单位为分',
  `receipt_amount` decimal(8,2) DEFAULT NULL COMMENT '实收金额,商家在交易中实际收到的款项',
  `buyer_pay_amount` decimal(8,2) DEFAULT NULL COMMENT '付款金额,现金支付金额订单现金支付金额，详见支付金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='欠费订单支付表';

-- ----------------------------
-- Records of re_owe_order_pay
-- ----------------------------

-- ----------------------------
-- Table structure for `re_owe_order_record`
-- ----------------------------
DROP TABLE IF EXISTS `re_owe_order_record`;
CREATE TABLE `re_owe_order_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` varchar(20) DEFAULT NULL COMMENT '欠费订单id',
  `fin_account_id` int(11) DEFAULT NULL COMMENT '欠款记录号',
  `price` decimal(8,2) DEFAULT '0.00' COMMENT '欠费金额',
  `price_fen` int(10) DEFAULT '0' COMMENT '单位元',
  `cost_type` varchar(20) DEFAULT NULL,
  `cost_type_note` varchar(50) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `intro` varchar(1000) DEFAULT NULL COMMENT '备注',
  `fin_event_id` int(11) DEFAULT NULL COMMENT '财经事务记录号',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='欠费订单记录表';

-- ----------------------------
-- Records of re_owe_order_record
-- ----------------------------

-- ----------------------------
-- Table structure for `re_permission`
-- ----------------------------
DROP TABLE IF EXISTS `re_permission`;
CREATE TABLE `re_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_name` varchar(255) DEFAULT NULL COMMENT '权限名称',
  `sidebar_name` varchar(255) DEFAULT '' COMMENT '侧边栏名称',
  `meta_title` varchar(30) DEFAULT NULL,
  `icon` varchar(255) DEFAULT '' COMMENT '图标',
  `route_name` varchar(255) DEFAULT '' COMMENT '如果为页面级权限，页面组件名称',
  `route_path` varchar(255) DEFAULT '' COMMENT '访问路径，菜单级权限必填',
  `component_path` varchar(255) DEFAULT '' COMMENT '组件路径,菜单与页面级权限必填',
  `type` tinyint(1) DEFAULT '1' COMMENT '权限类型 1目录 2菜单 3页面 4按钮',
  `path` varchar(1000) DEFAULT '' COMMENT '权限树，补齐4位显示',
  `view_name` varchar(20) DEFAULT NULL,
  `path_name` varchar(1000) DEFAULT '' COMMENT '权限树(名称)',
  `redirect` varchar(50) DEFAULT NULL,
  `api_path` varchar(1000) DEFAULT '' COMMENT '权限对应的接口路由',
  `pid` int(11) DEFAULT NULL COMMENT '父级id',
  `level` int(11) DEFAULT NULL COMMENT '深度',
  `hl_routes` varchar(1000) DEFAULT '' COMMENT '高亮路由定义',
  `order` int(10) DEFAULT NULL COMMENT '排序',
  `is_admin` tinyint(1) DEFAULT '2' COMMENT '是否为admin管理员独有权限  1是 2不是 默认2',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除   1.正常(默认)  2.已删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=746 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='权限表';

-- ----------------------------
-- Records of re_permission
-- ----------------------------
INSERT INTO `re_permission` VALUES ('1', '活动报名', '', '活动报名', 'active', null, null, null, '1', '0001', null, '活动报名', null, '', '0', '1', '', '1', '2', '1', '2023-08-01 11:22:41', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('2', '活动列表', '', '活动列表', 'active_list', 'activityList', '/managementActivities/activityList', null, '1', '0001-0002', null, '活动报名-活动列表', null, 'admin/activity/list,admin/activityType/filterList', '1', '2', '', '2', '2', '1', '2023-08-01 11:28:54', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('3', '活动报名-活动列表-撤回', '', null, null, null, null, null, '3', '0001-0002-0003', '撤回', '活动报名-活动列表-活动报名-活动列表-撤回', null, 'admin/activity/cancel', '2', '3', '', '24', '2', '1', '2023-08-01 11:33:31', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('4', '活动报名-活动列表-发布', '', null, null, null, null, null, '3', '0001-0002-0004', '发布', '活动报名-活动列表-活动报名-活动列表-发布', null, 'admin/activity/cancel', '2', '3', '', '3', '2', '1', '2023-08-01 11:33:58', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('5', '活动报名-活动列表-详情', '', null, null, null, null, null, '3', '0001-0002-0005', '详情', '活动报名-活动列表-活动报名-活动列表-详情', null, 'admin/activity/detail', '2', '3', '', '4', '2', '1', '2023-08-01 11:49:12', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('6', '活动报名-活动列表-修改', '', null, null, null, null, null, '3', '0001-0002-0006', '修改', '活动报名-活动列表-活动报名-活动列表-修改', null, 'admin/activity/detail,admin/activity/change', '2', '3', '', '5', '2', '1', '2023-08-01 11:49:44', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('7', '活动报名-活动列表-创建活动', '', null, null, null, null, null, '3', '0001-0002-0007', '创建活动', '活动报名-活动列表-活动报名-活动列表-创建活动', null, 'admin/activity/add', '2', '3', '', '6', '2', '1', '2023-08-01 11:55:48', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('8', '活动报名-活动列表-申请列表', '', null, null, null, null, null, '3', '0001-0002-0008', '申请列表', '活动报名-活动列表-活动报名-活动列表-申请列表', null, 'admin/activityApply/activityApplyList', '2', '3', '', '7', '2', '1', '2023-08-01 13:10:28', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('9', '活动报名-活动列表-人员签到', '', null, null, null, null, null, '3', '0001-0002-0009', '人员签到', '活动报名-活动列表-活动报名-活动列表-人员签到', null, 'admin/activityApply/activityApplyUser', '2', '3', '', '25', '2', '1', '2023-08-01 13:22:10', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('10', '活动报名-活动列表-人员管理', '', null, null, null, null, null, '3', '0001-0002-0010', '人员管理', '活动报名-活动列表-活动报名-活动列表-人员管理', null, 'admin/activityApply/activityApplyUser', '2', '3', '', '8', '2', '1', '2023-08-01 13:23:34', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('11', '活动报名-活动列表-删除', '', null, null, null, null, null, '3', '0001-0002-0011', '删除', '活动报名-活动列表-活动报名-活动列表-删除', null, 'admin/activity/del', '2', '3', '', '9', '2', '1', '2023-08-01 13:24:01', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('12', '活动报名-活动列表-按钮集合', '', null, null, null, null, null, '3', '0001-0002-0012', '按钮集合', '活动报名活动列表-活动报名-活动列表-按钮集合', null, 'admin/activity/list', '2', '3', '', '10', '2', '2', '2023-08-01 13:25:32', '2023-08-15 17:30:05');
INSERT INTO `re_permission` VALUES ('13', '活动报名-活动列表-添加活动', '', '添加活动', null, 'activityEstablish', '/managementActivities/activityList/Establish', null, '2', '0001-0002-0013', null, '活动报名-活动列表-添加活动', null, 'admin/activity/add,admin/activityType/filterList,admin/activityTag/filterList', '2', '3', '', '10', '2', '1', '2023-08-01 13:31:42', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('14', '修改活动', '', '修改活动', null, 'activitYamend', '/managementActivities/activityList/activitYamend', null, '2', '0001-0002-0014', null, '活动报名-活动列表-修改活动', null, 'admin/activity/change,admin/activity/detail,admin/activityType/filterList,admin/activityTag/filterList', '2', '3', '', '11', '2', '1', '2023-08-01 13:32:33', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('15', '申请列表', '', '申请列表', null, 'applicationList', '/managementActivities/activityList/applicationList', null, '2', '0001-0002-0015', '申请列表', '活动报名活动列表-申请列表', null, 'admin/activityApply/activityApplyList,admin/activity/detail', '2', '3', '', '12', '2', '1', '2023-08-01 13:42:48', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('16', '人员签到', '', '人员签到', null, 'personnelCheckIn', '/managementActivities/activityList/personnelCheckIn', null, '2', '0001-0002-0016', '人员签到', '活动报名-活动列表-人员签到', null, 'admin/activityApply/activityApplyUser,admin/activity/detail', '2', '3', '', '16', '2', '1', '2023-08-01 13:44:13', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('17', '人员管理', '', '人员管理', null, 'eventPersonnel', '/managementActivities/activityList/eventPersonnel', null, '2', '0001-0002-0017', '人员管理', '活动报名-活动列表-人员管理', null, 'admin/activityApply/activityApplyUser,admin/activity/detail', '2', '3', '', '18', '2', '1', '2023-08-01 13:45:04', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('18', '活动列表-申请列表-导出', '', null, null, null, null, null, '3', '0001-0002-0015-0018', '导出列表', '活动报名-活动列表-申请列表-活动列表-申请列表-导出', null, 'admin/ActivityApplyExport/activityApplyList', '15', '4', '', '13', '2', '1', '2023-08-01 13:47:43', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('19', '活动列表-人员管理-导出列表', '', null, null, null, null, null, '3', '0001-0002-0017-0019', '导出列表', '活动报名-活动列表-人员管理-活动列表-人员管理-导出列表', null, 'admin/ActivityApplyExport/activityApplyUser', '17', '4', '', '19', '2', '1', '2023-08-01 13:48:33', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('20', '活动列表-申请列表-同意', '', null, null, null, null, null, '3', '0001-0002-0015-0020', '同意', '活动报名-活动列表-申请列表-活动列表-申请列表-同意', null, 'admin/activityApply/agreeAndRefused', '15', '4', '', '14', '2', '1', '2023-08-01 13:55:26', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('21', '活动列表-申请列表-拒绝', '', null, null, null, null, null, '3', '0001-0002-0015-0021', '拒绝', '活动报名-活动列表-申请列表-活动列表-申请列表-拒绝', null, 'admin/activityApply/agreeAndRefused', '15', '4', '', '15', '2', '1', '2023-08-01 13:55:57', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('22', '活动列表-人员签到-签到', '', null, null, null, null, null, '3', '0001-0002-0016-0022', '签到', '活动报名-活动列表-人员签到-活动列表-人员签到-签到', null, 'admin/activity/sign', '16', '4', '', '17', '2', '1', '2023-08-01 13:56:50', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('23', '活动列表-人员管理-用户违规', '', null, null, null, null, null, '3', '0001-0002-0017-0023', '用户违规', '活动报名-活动列表-人员管理-活动列表-人员管理-用户违规', null, 'admin/activityApply/violateAndCancel', '17', '4', '', '20', '2', '1', '2023-08-01 13:57:36', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('24', '活动列表-人员管理-取消违规', '', null, null, null, null, null, '3', '0001-0002-0017-0024', '取消违规', '活动报名-活动列表-人员管理-活动列表-人员管理-取消违规', null, 'admin/activityApply/violateAndCancel', '17', '4', '', '21', '2', '1', '2023-08-01 13:58:01', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('25', '活动类型', '', '活动类型', 'class', 'Activitytype', '/managementActivities/activitytype', null, '1', '0001-0025', null, '活动报名-活动类型', null, 'admin/activityType/list', '1', '2', '', '26', '2', '1', '2023-08-01 14:40:10', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('26', '活动类型-添加类型', '', null, null, null, null, null, '3', '0001-0025-0026', '添加类型', '活动报名-活动类型-活动类型-添加类型', null, 'admin/activityType/add', '25', '3', '', '27', '2', '1', '2023-08-01 14:41:52', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('27', '活动类型-修改', '', null, null, null, null, null, '3', '0001-0025-0027', '修改', '活动报名-活动类型-活动类型-修改', null, 'admin/activityType/change', '25', '3', '', '28', '2', '1', '2023-08-01 14:42:17', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('28', '活动类型-删除', '', null, null, null, null, null, '3', '0001-0025-0028', '删除', '活动报名-活动类型-活动类型-删除', null, 'admin/activityType/del', '25', '3', '', '29', '2', '1', '2023-08-01 14:42:37', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('29', '活动标签', '', '活动标签', 'type', 'Activetags', '/managementActivities/Activetags', null, '1', '0001-0029', null, '活动报名-活动标签', null, 'admin/activityTag/list', '1', '2', '', '30', '2', '1', '2023-08-01 14:47:43', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('30', '活动标签-添加标签', '', null, null, null, null, null, '3', '0001-0029-0030', '添加标签', '活动报名-活动标签-活动标签-添加标签', null, 'admin/activityTag/add', '29', '3', '', '31', '2', '1', '2023-08-01 14:48:11', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('31', '活动标签-修改', '', null, null, null, null, null, '3', '0001-0029-0031', '修改', '活动报名-活动标签-活动标签-修改', null, 'admin/activityTag/change', '29', '3', '', '32', '2', '1', '2023-08-01 14:48:30', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('32', '活动标签-删除', '', null, null, null, null, null, '3', '0001-0029-0032', '删除', '活动报名-活动标签-活动标签-删除', null, 'admin/activityTag/del', '29', '3', '', '33', '2', '1', '2023-08-01 14:48:50', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('33', '悦读互动', '', '悦读互动', 'game', null, null, null, '1', '0033', null, '悦读互动', null, '', '0', '1', '', '34', '2', '1', '2023-08-01 14:51:05', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('34', '在线答题', '', '在线答题', 'answer', 'answeractivityList', '/answerActivities/activityList', null, '1', '0033-0034', null, '悦读互动-在线答题', null, 'admin/answerActivity/list', '33', '2', '', '35', '2', '1', '2023-08-01 14:52:37', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('35', '在线答题-创建活动', '', null, null, null, null, null, '3', '0033-0034-0035', '创建活动', '悦读互动-在线答题-在线答题-创建活动', null, 'admin/answerActivity/add', '34', '3', '', '36', '2', '1', '2023-08-01 14:53:46', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('36', '在线答题-撤回', '', null, null, null, null, null, '3', '0033-0034-0036', '撤回', '悦读互动-在线答题-在线答题-撤回', null, 'admin/answerActivity/cancelAndRelease', '34', '3', '', '113', '2', '1', '2023-08-01 14:55:10', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('37', '在线答题-发布', '', null, null, null, null, null, '3', '0033-0034-0037', '发布', '悦读互动-在线答题-在线答题-发布', null, 'admin/answerActivity/cancelAndRelease', '34', '3', '', '37', '2', '1', '2023-08-01 14:55:37', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('38', '在线答题-详情', '', null, null, null, null, null, '3', '0033-0034-0038', '详情', '悦读互动-在线答题-在线答题-详情', null, 'admin/answerActivity/detail', '34', '3', '', '38', '2', '1', '2023-08-01 14:56:12', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('39', '在线答题-修改', '', null, null, null, null, null, '3', '0033-0034-0039', '修改', '悦读互动-在线答题-在线答题-修改', null, 'admin/answerActivity/detail,admin/answerActivity/change', '34', '3', '', '114', '2', '1', '2023-08-01 14:56:41', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('40', '在线答题-区域管理', '', null, null, null, null, null, '3', '0033-0034-0040', '区域管理', '悦读互动-在线答题-在线答题-区域管理', null, 'admin/answerActivityArea/list', '34', '3', '', '39', '2', '1', '2023-08-01 15:08:03', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('41', '在线答题-单位管理', '', null, null, null, null, null, '3', '0033-0034-0041', '单位管理', '悦读互动-在线答题-在线答题-单位管理', null, 'admin/answerActivityUnit/list,admin/answerActivityUnit/getNatureName', '34', '3', '', '40', '2', '1', '2023-08-01 15:09:03', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('42', '在线答题-礼品管理', '', null, null, null, null, null, '3', '0033-0034-0042', '礼品管理', '悦读互动-在线答题-在线答题-礼品管理', null, 'admin/answerActivityGift/list,admin/answerActivityGiftTime/getSendTotalConfig', '34', '3', '', '41', '2', '1', '2023-08-01 15:20:00', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('43', '在线答题-获奖列表', '', null, null, null, null, null, '3', '0033-0034-0043', '获奖列表', '悦读互动-在线答题-在线答题-获奖列表', null, 'admin/answerActivityUserGift/list', '34', '3', '', '42', '2', '1', '2023-08-01 15:23:38', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('44', '在线答题-排名列表', '', null, null, null, null, null, '3', '0033-0034-0044', '排名列表', '悦读互动-在线答题-在线答题-排名列表', null, 'admin/answerActivityRanking/ranking', '34', '3', '', '43', '2', '1', '2023-08-01 15:24:11', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('45', '在线答题-图片配置', '', null, null, null, null, null, '3', '0033-0034-0045', '图片配置', '悦读互动-在线答题-在线答题-图片配置', null, 'admin/answerActivityResource/resourceUpload', '34', '3', '', '44', '2', '1', '2023-08-01 15:25:17', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('46', '在线答题-文字颜色配置', '', null, null, null, null, null, '3', '0033-0034-0046', '文字颜色配置', '悦读互动-在线答题-在线答题-文字颜色配置', null, 'admin/answerActivityResource/setColorResource', '34', '3', '', '45', '2', '1', '2023-08-01 15:25:44', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('47', '在线答题-题库管理', '', null, null, null, null, null, '3', '0033-0034-0047', '题库管理', '悦读互动-在线答题-在线答题-题库管理', null, 'admin/answerActivityProblem/list', '34', '3', '', '46', '2', '1', '2023-08-01 15:38:00', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('48', '在线答题-数据统计', '', null, null, null, null, null, '3', '0033-0034-0048', '数据统计', '悦读互动-在线答题-在线答题-数据统计', null, 'admin/answerActivityDataAnalysis/dataStatistics,admin/answerActivityDataAnalysis/systemGiftGrantStatistics,admin/answerActivityDataAnalysis/giftGrantStatistics,admin/answerActivityDataAnalysis/answerStatistics', '34', '3', '', '47', '2', '1', '2023-08-01 15:38:59', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('49', '在线答题-修改邀请码', '', null, null, null, null, null, '3', '0033-0034-0049', '修改邀请码', '悦读互动-在线答题-在线答题-修改邀请码', null, 'admin/answerActivity/invitCodeChange', '34', '3', '', '48', '2', '1', '2023-08-01 15:51:10', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('50', '在线答题-文案设置', '', null, null, null, null, null, '3', '0033-0034-0050', '文案设置', '悦读互动-在线答题-在线答题-文案设置', null, 'admin/answerActivityResource/getWordResource', '34', '3', '', '49', '2', '1', '2023-08-01 15:51:35', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('51', '在线答题-限制参与地区', '', null, null, null, null, null, '3', '0033-0034-0051', '限制参与地区', '悦读互动-在线答题-在线答题-限制参与地区', null, 'admin/answerActivityProblem/list', '34', '3', '', '50', '2', '1', '2023-08-01 15:51:59', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('52', '在线答题-复制活动', '', null, null, null, null, null, '3', '0033-0034-0052', '复制活动', '悦读互动-在线答题-在线答题-复制活动', null, 'admin/answerActivity/copyData', '34', '3', '', '51', '2', '1', '2023-08-01 15:53:24', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('53', '在线答题-重置数据', '', null, null, null, null, null, '3', '0033-0034-0053', '重置数据', '悦读互动-在线答题-在线答题-重置数据', null, 'admin/answerActivity/resetData', '34', '3', '', '52', '2', '1', '2023-08-01 15:54:17', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('54', '在线答题-删除', '', null, null, null, null, null, '3', '0033-0034-0054', '删除', '悦读互动-在线答题-在线答题-删除', null, 'admin/answerActivity/del', '34', '3', '', '115', '2', '1', '2023-08-01 15:54:50', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('55', '创建答题活动', '', '创建答题活动', null, 'answeractivityEstablish', '/answerActivities/activityList/Establish', null, '2', '0033-0034-0055', null, '悦读互动-在线答题-创建答题活动', null, 'admin/answerActivity/add', '34', '3', '', '53', '2', '1', '2023-08-01 16:03:02', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('56', '在线答题详情', '', '详情', null, 'activitInfo', '/answerActivities/activityList/activitInfo', null, '2', '0033-0034-0056', null, '悦读互动-在线答题-详情', null, 'admin/answerActivity/detail', '34', '3', '', '54', '2', '1', '2023-08-01 16:36:57', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('57', '修改答题活动', '', '修改答题活动', null, 'answeractivitYamend', '/answerActivities/activityList/activitYamend', null, '2', '0033-0034-0057', null, '悦读互动-在线答题-修改答题活动', null, 'admin/answerActivity/detail,admin/answerActivity/change', '34', '3', '', '55', '2', '1', '2023-08-01 16:42:17', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('58', '答题排名列表', '', '排名列表', null, 'UnitReaderRanking', '/answerActivities/activityList/areaList/UnitReaderRanking', null, '2', '0033-0034-0058', null, '悦读互动-在线答题-排名列表', null, 'admin/answerActivityRanking/ranking', '34', '3', '', '56', '2', '1', '2023-08-01 16:50:29', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('59', '答题排名列表-导出列表', '', '排名列表', null, 'UnitReaderRanking', '/answerActivities/activityList/areaList/UnitReaderRanking', null, '3', '0033-0034-0058-0059', '导出列表', '悦读互动-在线答题-排名列表-排名列表', null, 'admin/answerActivityRankingExport/ranking', '58', '4', '', '57', '2', '1', '2023-08-01 16:51:12', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('60', '答题限制参与地区', '', '限制参与地区', null, 'resrictionsList', '/answerActivities/activityList/resrictionsList', null, '2', '0033-0034-0060', null, '悦读互动-在线答题-限制参与地区', null, 'admin/answerActivityLimitAddres/list', '34', '3', '', '58', '2', '1', '2023-08-01 17:22:18', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('61', '答题限制参与地区-添加地址', '', '限制参与地区', null, 'resrictionsList', '/answerActivities/activityList/resrictionsList', null, '3', '0033-0034-0061', '添加地址', '悦读互动-在线答题-限制参与地区', null, 'admin/answerActivityLimitAddres/add', '34', '3', '', '59', '2', '1', '2023-08-01 17:30:49', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('62', '答题限制参与地区-修改', '', '限制参与地区', null, 'resrictionsList', '/answerActivities/activityList/resrictionsList', null, '3', '0033-0034-0062', '修改', '悦读互动-在线答题-限制参与地区', null, 'admin/answerActivityLimitAddres/change', '34', '3', '', '60', '2', '1', '2023-08-01 17:31:19', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('63', '答题限制参与地区-删除', '', '限制参与地区', null, 'resrictionsList', '/answerActivities/activityList/resrictionsList', null, '3', '0033-0034-0063', '删除', '悦读互动-在线答题-限制参与地区', null, 'admin/answerActivityLimitAddres/del', '34', '3', '', '61', '2', '1', '2023-08-01 17:31:37', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('64', '答题图片配置', '', '图片配置', null, 'UIimgList', '/answerActivities/activityList/UIimg', null, '2', '0033-0034-0064', null, '悦读互动-在线答题-图片配置', null, 'admin/answerActivityResource/getColorResource,admin/answerActivityResource/getDefaultResourceAddr,admin/answerActivityResource/resourceDownload,admin/answerActivityResource/resourceUpload', '34', '3', '', '62', '2', '1', '2023-08-01 17:59:48', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('65', '文字颜色配置', '', '文字颜色配置', null, 'UItextList', '/answerActivities/activityList/UItext', null, '2', '0033-0034-0065', null, '悦读互动-在线答题-文字颜色配置', null, 'admin/answerActivityResource/getColorResource,admin/answerActivityResource/setColorResource', '34', '3', '', '63', '2', '1', '2023-08-01 18:04:20', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('66', '答题文案设置', '', '文案设置', null, 'Copywriting', '/answerActivities/activityList/copywriting', null, '2', '0033-0034-0066', null, '悦读互动-在线答题-文案设置', null, 'admin/answerActivityResource/getWordResource,admin/answerActivityResource/setWordResource', '34', '3', '', '133', '2', '1', '2023-08-02 09:09:12', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('67', '答题数据统计', '', '数据统计', null, 'answerdataStatistics', '/answerActivities/activityList/dataStatistics', null, '2', '0033-0034-0067', null, '悦读互动-在线答题-数据统计', null, 'admin/answerActivityDataAnalysis/systemGiftGrantStatistics,admin/answerActivityDataAnalysis/giftGrantStatistics,admin/answerActivityDataAnalysis/dataStatistics,admin/answerActivityDataAnalysis/answerStatistics', '34', '3', '', '64', '2', '1', '2023-08-02 09:49:44', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('68', '答题题库管理', '', '题库管理', null, 'QuestionList', '/answerActivities/activityList/QuestionList', null, '2', '0033-0034-0068', null, '悦读互动-在线答题-题库管理', null, 'admin/answerActivityProblem/list', '34', '3', '', '65', '2', '1', '2023-08-02 09:50:34', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('69', '答题题库管理-添加题库', '', null, null, null, null, null, '3', '0033-0034-0068-0069', '添加题库', '悦读互动-在线答题-题库管理-答题题库管理-添加题库', null, 'admin/answerActivityProblem/add', '68', '4', '', '66', '2', '1', '2023-08-02 10:01:05', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('70', '答题题库管理-导入题库', '', null, null, null, null, null, '3', '0033-0034-0068-0070', '导入题库', '悦读互动-在线答题-题库管理-答题题库管理-导入题库', null, 'admin/answerActivityProblemImport/index', '68', '4', '', '67', '2', '1', '2023-08-02 10:01:29', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('71', '答题题库管理-下载模版', '', null, null, null, null, null, '3', '0033-0034-0068-0071', '下载模版', '悦读互动-在线答题-题库管理-答题题库管理-下载模版', null, 'admin/answerActivityProblemExport/template', '68', '4', '', '68', '2', '1', '2023-08-02 10:04:01', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('72', '答题题库管理-导出题库', '', null, null, null, null, null, '3', '0033-0034-0068-0072', '导出题库', '悦读互动-在线答题-题库管理-答题题库管理-导出题库', null, 'admin/answerActivityProblemExport/index', '68', '4', '', '69', '2', '1', '2023-08-02 10:04:44', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('73', '答题题库管理-显示解析', '', null, null, null, null, null, '3', '0033-0034-0068-0073', '显示解析', '悦读互动-在线答题-题库管理-答题题库管理-显示解析', null, 'admin/answerActivityProblem/analysisShowAndHidden', '68', '4', '', '70', '2', '1', '2023-08-02 10:05:28', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('74', '答题题库管理-不显示解析', '', null, null, null, null, null, '3', '0033-0034-0068-0074', '不显示解析', '悦读互动-在线答题-题库管理-答题题库管理-不显示解析', null, 'admin/answerActivityProblem/analysisShowAndHidden', '68', '4', '', '71', '2', '1', '2023-08-02 10:05:48', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('75', '答题题库管理-清空题库', '', null, null, null, null, null, '3', '0033-0034-0068-0075', '清空题库', '悦读互动-在线答题-题库管理-答题题库管理-清空题库', null, 'admin/answerActivityProblem/delMany', '68', '4', '', '72', '2', '1', '2023-08-02 10:06:20', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('76', '答题题库管理-启用', '', null, null, null, null, null, '3', '0033-0034-0068-0076', '启用', '悦读互动-在线答题-题库管理-答题题库管理-启用', null, 'admin/answerActivityProblem/disablingAndEnabling', '68', '4', '', '73', '2', '1', '2023-08-02 10:07:07', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('77', '答题题库管理-禁用', '', null, null, null, null, null, '3', '0033-0034-0068-0077', '禁用', '悦读互动-在线答题-题库管理-答题题库管理-禁用', null, 'admin/answerActivityProblem/disablingAndEnabling', '68', '4', '', '74', '2', '1', '2023-08-02 10:07:22', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('78', '答题题库管理-详情', '', null, null, null, null, null, '3', '0033-0034-0068-0078', '详情', '悦读互动-在线答题-题库管理-答题题库管理-详情', null, 'admin/answerActivity/detail', '68', '4', '', '75', '2', '1', '2023-08-02 10:11:49', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('79', '答题题库管理-修改', '', null, null, null, null, null, '3', '0033-0034-0068-0079', '修改', '悦读互动-在线答题-题库管理-答题题库管理-修改', null, 'admin/answerActivity/detail,admin/answerActivityProblem/change', '68', '4', '', '79', '2', '1', '2023-08-02 10:12:17', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('80', '答题题库管理-删除', '', null, null, null, null, null, '3', '0033-0034-0068-0080', '删除', '悦读互动-在线答题-题库管理-答题题库管理-删除', null, 'admin/answerActivityProblem/del', '68', '4', '', '76', '2', '1', '2023-08-02 10:12:48', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('81', '答题题库管理-添加题目', '', '添加题目', null, 'addQuestionList', '/answerActivities/activityList/QuestionList/add', null, '2', '0033-0034-0068-0081', null, '悦读互动-在线答题-题库管理-添加题目', null, 'admin/answerActivityProblem/add', '68', '4', '', '80', '2', '1', '2023-08-02 10:14:09', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('82', '答题题库管理-修改题目', '', '修改题目', null, 'changQuestionList', '/answerActivities/activityList/QuestionList/chang', null, '2', '0033-0034-0068-0082', null, '悦读互动-在线答题-题库管理-修改题目', null, 'admin/answerActivity/detail,admin/answerActivityProblem/change', '68', '4', '', '77', '2', '1', '2023-08-02 10:14:59', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('83', '答题题库管理-题目详情', '', '详情', null, 'QuestionListinfo', '/answerActivities/activityList/QuestionList/info', null, '2', '0033-0034-0068-0083', null, '悦读互动-在线答题-题库管理-详情', null, 'admin/answerActivity/detail', '68', '4', '', '78', '2', '1', '2023-08-02 10:15:37', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('84', '答题区域单位管理', '', '区域单位管理', null, 'areaList', '/answerActivities/activityList/areaList', null, '2', '0033-0034-0084', null, '悦读互动-在线答题-区域单位管理', null, 'admin/answerActivityUnit/list,admin/answerActivityUnit/getNatureName', '34', '3', '', '116', '2', '1', '2023-08-02 10:20:49', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('85', '答题区域单位管理-添加单位', '', null, null, null, null, null, '3', '0033-0034-0084-0085', '添加单位', '悦读互动-在线答题-区域单位管理-答题区域单位管理-添加单位', null, 'admin/answerActivityUnit/getNatureName,admin/answerActivityUnit/add', '84', '4', '', '117', '2', '1', '2023-08-02 10:57:44', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('86', '答题区域单位管理-下载模版', '', null, null, null, null, null, '3', '0033-0034-0084-0086', '下载模版', '悦读互动-在线答题-区域单位管理-答题区域单位管理-下载模版', null, 'admin/answerActivityUnitExport/template', '84', '4', '', '118', '2', '1', '2023-08-02 11:00:46', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('87', '答题区域单位管理-导入列表', '', null, null, null, null, null, '3', '0033-0034-0084-0087', '导入列表', '悦读互动-在线答题-区域单位管理-答题区域单位管理-导入列表', null, 'admin/answerActivityUnitImport/index', '84', '4', '', '119', '2', '1', '2023-08-02 11:01:06', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('88', '答题区域单位管理-导出列表', '', null, null, null, null, null, '3', '0033-0034-0084-0088', '导出列表', '悦读互动-在线答题-区域单位管理-答题区域单位管理-导出列表', null, 'admin/answerActivityUnitExport/getUnitList', '84', '4', '', '120', '2', '1', '2023-08-02 11:01:25', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('89', '答题区域单位管理-单位排序', '', null, null, null, null, null, '3', '0033-0034-0084-0089', '单位排序', '悦读互动-在线答题-区域单位管理-答题区域单位管理-单位排序', null, 'admin/answerActivityUnit/orderChange', '84', '4', '', '121', '2', '1', '2023-08-02 11:03:19', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('90', '答题区域单位管理-区域管理', '', null, null, null, null, null, '3', '0033-0034-0084-0090', '区域管理', '悦读互动-在线答题-区域单位管理-答题区域单位管理-区域管理', null, 'admin/answerActivityArea/list', '84', '4', '', '132', '2', '1', '2023-08-02 11:16:18', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('91', '答题区域单位管理-二维码导出', '', null, null, null, null, null, '3', '0033-0034-0084-0091', '二维码导出', '悦读互动-在线答题-区域单位管理-答题区域单位管理-二维码导出', null, 'admin/answerActivityUnit/downloadUnitQr', '84', '4', '', '122', '2', '1', '2023-08-02 11:17:16', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('92', '答题区域单位管理-设置默认', '', null, null, null, null, null, '3', '0033-0034-0084-0092', '设置默认', '悦读互动-在线答题-区域单位管理-答题区域单位管理-设置默认', null, 'admin/answerActivityUnit/cancelAndDefault', '84', '4', '', '123', '2', '1', '2023-08-02 11:22:23', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('93', '答题区域单位管理-取消默认', '', null, null, null, null, null, '3', '0033-0034-0084-0093', '取消默认', '悦读互动-在线答题-区域单位管理-答题区域单位管理-取消默认', null, 'admin/answerActivityUnit/cancelAndDefault', '84', '4', '', '124', '2', '1', '2023-08-02 11:22:33', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('94', '答题区域单位管理-详情', '', null, null, null, null, null, '3', '0033-0034-0084-0094', '详情', '悦读互动-在线答题-区域单位管理-答题区域单位管理-详情', null, 'admin/answerActivityUnit/detail', '84', '4', '', '125', '2', '1', '2023-08-02 11:23:11', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('95', '答题区域单位管理-修改', '', null, null, null, null, null, '3', '0033-0034-0084-0095', '修改', '悦读互动-在线答题-区域单位管理-答题区域单位管理-修改', null, 'admin/answerActivityUnit/detail,admin/answerActivityUnit/change', '84', '4', '', '126', '2', '1', '2023-08-02 11:26:52', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('96', '答题区域单位管理-排名', '', null, null, null, null, null, '3', '0033-0034-0084-0096', '排名', '悦读互动-在线答题-区域单位管理-答题区域单位管理-排名', null, 'admin/answerActivityUnit/filterList,admin/answerActivityRanking/ranking', '84', '4', '', '127', '2', '1', '2023-08-02 11:27:59', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('97', '答题区域单位管理-删除', '', null, null, null, null, null, '3', '0033-0034-0084-0097', '删除', '悦读互动-在线答题-区域单位管理-答题区域单位管理-删除', null, 'admin/answerActivityUnit/del', '84', '4', '', '128', '2', '1', '2023-08-02 11:43:32', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('98', '答题区域单位管理-新增单位', '', '添加单位', null, 'addarea', '/answerActivities/activityList/areaList/add', null, '2', '0033-0034-0084-0098', null, '悦读互动-在线答题-区域单位管理-添加单位', null, 'admin/answerActivityUnit/getNatureName,admin/answerActivityUnit/add', '84', '4', '', '129', '2', '1', '2023-08-02 13:42:43', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('99', '答题区域单位管理-单位详情', '', '单位详情', null, 'infoarea', '/answerActivities/activityList/areaList/info', null, '2', '0033-0034-0084-0099', null, '悦读互动-在线答题-区域单位管理-单位详情', null, 'admin/answerActivityUnit/detail', '84', '4', '', '130', '2', '1', '2023-08-02 13:44:00', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('100', '答题区域单位管理-修改单位', '', '修改单位', null, 'changarea', '/answerActivities/activityList/areaList/chang', null, '2', '0033-0034-0084-0100', null, '悦读互动-在线答题-区域单位管理-修改单位', null, 'admin/answerActivityUnit/detail,admin/answerActivityUnit/change,admin/answerActivityUnit/getNatureName', '84', '4', '', '131', '2', '1', '2023-08-02 13:44:32', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('101', '答题区域管理-修改单位', '', '修改单位', null, 'changarea', '/answerActivities/activityList/areaList/chang', null, '2', '0033-0034-0084-0101', null, '悦读互动-在线答题-区域单位管理-修改单位', null, 'admin/answerActivityUnit/detail,admin/answerActivityUnit/change,admin/answerActivityUnit/getNatureName', '84', '4', '', '602', '2', '2', '2023-08-02 13:57:12', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('102', '答题区域管理', '', '区域管理', null, 'districtManagement', '/answerActivities/activityList/areaList/districtManagement', null, '2', '0033-0034-0102', null, '悦读互动-在线答题-区域管理', null, 'admin/answerActivityArea/list', '34', '3', '', '81', '2', '1', '2023-08-02 14:04:28', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('103', '答题区域管理-添加区域', '', null, null, null, null, null, '3', '0033-0034-0102-0103', '添加区域', '悦读互动-在线答题-区域管理-答题区域管理-添加区域', null, 'admin/answerActivityArea/add', '102', '4', '', '82', '2', '1', '2023-08-02 14:08:21', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('104', '答题区域管理-详情', '', null, null, null, null, null, '3', '0033-0034-0102-0104', '详情', '悦读互动-在线答题-区域管理-答题区域管理-详情', null, 'admin/answerActivityArea/list', '102', '4', '', '83', '2', '1', '2023-08-02 14:09:24', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('105', '答题区域管理-修改', '', null, null, null, null, null, '3', '0033-0034-0102-0105', '修改', '悦读互动-在线答题-区域管理-答题区域管理-修改', null, 'admin/answerActivityArea/change', '102', '4', '', '84', '2', '1', '2023-08-02 14:09:39', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('106', '答题区域管理-删除', '', null, null, null, null, null, '3', '0033-0034-0102-0106', '删除', '悦读互动-在线答题-区域管理-答题区域管理-删除', null, 'admin/answerActivityArea/del', '102', '4', '', '85', '2', '1', '2023-08-02 14:09:55', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('107', '答题单位排序', '', '单位排序', null, 'districtUnitSort', '/answerActivities/activityList/areaList/unitSort', null, '2', '0033-0034-0107', null, '悦读互动-在线答题-单位排序', null, 'admin/answerActivityUnit/orderChange,admin/answerActivityUnit/list', '34', '3', '', '86', '2', '1', '2023-08-02 14:20:04', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('108', '答题后台操作记录', '', '后台操作记录', null, 'record', '/answerActivities/activityList/record', null, '2', '0033-0034-0108', null, '悦读互动-在线答题-后台操作记录', null, 'admin/activity/list', '34', '3', '', '87', '2', '1', '2023-08-02 14:30:49', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('109', '答题获奖列表', '', '获奖列表', null, 'awardList', '/answerActivities/activityList/awardList', null, '2', '0033-0034-0109', null, '悦读互动-在线答题-获奖列表', null, 'admin/answerActivityUserGift/getUnitList,admin/answerActivityUserGift/list', '34', '3', '', '88', '2', '1', '2023-08-02 14:32:30', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('110', '答题获奖列表-导出列表', '', null, null, null, null, null, '3', '0033-0034-0109-0110', '导出列表', '悦读互动-在线答题-获奖列表-答题获奖列表-导出列表', null, 'admin/answerActivityUserGiftExport/index', '109', '4', '', '89', '2', '1', '2023-08-02 14:33:49', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('111', '答题获奖列表-操作', '', null, null, null, null, null, '3', '0033-0034-0109-0111', '操作', '悦读互动-在线答题-获奖列表-答题获奖列表-操作', null, 'admin/answerActivityUserGift/userGiftGrant', '109', '4', '', '90', '2', '1', '2023-08-02 14:34:24', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('112', '答题获奖列表-转账', '', null, null, null, null, null, '3', '0033-0034-0109-0112', '转账', '悦读互动-在线答题-获奖列表-答题获奖列表-转账', null, 'admin/answerActivityUserGift/userPacketTransfer', '109', '4', '', '91', '2', '1', '2023-08-02 14:34:51', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('113', '答题礼品管理', '', '礼品管理', null, 'GiftList', '/answerActivities/activityList/GiftList', null, '2', '0033-0034-0113', null, '悦读互动-在线答题-礼品管理', null, 'admin/answerActivityGift/list', '34', '3', '', '92', '2', '1', '2023-08-02 14:52:51', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('114', '答题礼品管理-投放礼品', '', null, null, null, null, null, '3', '0033-0034-0113-0114', '投放礼品', '悦读互动-在线答题-礼品管理-答题礼品管理-投放礼品', null, 'admin/answerActivityGift/add', '113', '4', '', '93', '2', '1', '2023-08-02 15:35:03', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('115', '答题礼品管理-礼品发放管理', '', '礼品发放管理', null, 'GiftSet', '/answerActivities/activityList/GiftList/GiftSet', null, '2', '0033-0034-0113-0115', '礼品发放管理', '悦读互动-在线答题-礼品管理-礼品发放管理', null, 'admin/answerActivityGiftTime/list', '113', '4', '', '94', '2', '1', '2023-08-02 15:43:17', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('116', '答题礼品管理-概率设置', '', null, null, null, null, null, '3', '0033-0034-0113-0116', '概率设置', '悦读互动-在线答题-礼品管理-答题礼品管理-概率设置', null, 'admin/answerActivityGiftTime/setSendTotalConfig', '113', '4', '', '99', '2', '1', '2023-08-02 15:43:48', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('117', '答题礼品管理-礼品公示', '', null, null, null, null, null, '3', '0033-0034-0113-0117', '礼品公示', '悦读互动-在线答题-礼品管理-答题礼品管理-礼品公示', null, 'admin/answerActivityUnit/filterList', '113', '4', '', '100', '2', '1', '2023-08-02 15:50:40', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('118', '答题礼品管理-详情', '', null, null, null, null, null, '3', '0033-0034-0113-0118', '详情', '悦读互动-在线答题-礼品管理-答题礼品管理-详情', null, 'admin/answerActivityGift/detail', '113', '4', '', '101', '2', '1', '2023-08-02 15:52:18', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('119', '答题礼品管理-修改', '', null, null, null, null, null, '3', '0033-0034-0113-0119', '修改', '悦读互动-在线答题-礼品管理-答题礼品管理-修改', null, 'admin/answerActivityGift/detail,admin/answerActivityGift/change', '113', '4', '', '102', '2', '1', '2023-08-02 15:52:44', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('120', '答题礼品管理-删除', '', null, null, null, null, null, '3', '0033-0034-0113-0120', '删除', '悦读互动-在线答题-礼品管理-答题礼品管理-删除', null, 'admin/answerActivityGift/del', '113', '4', '', '103', '2', '1', '2023-08-02 15:53:06', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('121', '答题礼品管理-投放', '', '投放礼品', null, 'addGift', '/answerActivities/activityList/GiftList/add', null, '2', '0033-0034-0113-0121', '投放礼品', '悦读互动-在线答题-礼品管理-投放礼品', null, 'admin/answerActivityGift/add', '113', '4', '', '104', '2', '1', '2023-08-02 16:03:50', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('122', '答题礼品管理-详情页面', '', '详情', null, 'Giftinfo', '/answerActivities/activityList/GiftList/info', null, '2', '0033-0034-0113-0122', null, '悦读互动-在线答题-礼品管理-详情', null, 'admin/answerActivityGift/detail', '113', '4', '', '105', '2', '1', '2023-08-02 16:07:36', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('123', '答题礼品管理-修改礼品', '', '修改礼品', null, 'changGift', '/answerActivities/activityList/GiftList/chang', null, '2', '0033-0034-0113-0123', null, '悦读互动-在线答题-礼品管理-修改礼品', null, 'admin/answerActivityGift/detail,admin/answerActivityGift/change', '113', '4', '', '106', '2', '1', '2023-08-02 16:11:53', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('124', '答题礼品管理-礼品发放', '', '礼品发放管理', null, 'GiftSet', '/answerActivities/activityList/GiftList/GiftSet', null, '2', '0033-0034-0113-0124', null, '悦读互动-在线答题-礼品管理-礼品发放管理', null, 'admin/answerActivityGiftTime/list', '113', '4', '', '107', '2', '1', '2023-08-02 16:16:15', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('125', '答题礼品管理-礼品发放-新增发放限制', '', null, null, null, null, null, '3', '0033-0034-0113-0115-0125', '新增发放限制', '悦读互动-在线答题-礼品管理-答题礼品管理-礼品发放管理-答题礼品管理-礼品发放-新增发放限制', null, 'admin/answerActivityGiftTime/add', '115', '5', '', '95', '2', '1', '2023-08-02 16:17:01', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('126', '答题礼品管理-礼品发放-详情', '', null, null, null, null, null, '3', '0033-0034-0113-0115-0126', '详情', '悦读互动-在线答题-礼品管理-答题礼品管理-礼品发放管理-答题礼品管理-礼品发放-详情', null, 'admin/answerActivityGiftTime/list', '115', '5', '', '96', '2', '1', '2023-08-02 16:18:28', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('127', '答题礼品管理-礼品发放-修改', '', null, null, null, null, null, '3', '0033-0034-0113-0115-0127', '修改', '悦读互动-在线答题-礼品管理-答题礼品管理-礼品发放管理-答题礼品管理-礼品发放-修改', null, 'admin/answerActivityGiftTime/change', '115', '5', '', '97', '2', '1', '2023-08-02 16:18:44', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('128', '答题礼品管理-礼品发放- 删除', '', null, null, null, null, null, '3', '0033-0034-0113-0115-0128', '删除', '悦读互动-在线答题-礼品管理-答题礼品管理-礼品发放管理-答题礼品管理-礼品发放- 删除', null, 'admin/answerActivityGiftTime/del', '115', '5', '', '98', '2', '1', '2023-08-02 16:18:57', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('129', '答题礼品管理-礼品公示列表', '', '礼品公示', null, 'Publicity', '/answerActivities/activityList/GiftList/publicity', null, '2', '0033-0034-0113-0129', '礼品公示', '悦读互动-在线答题-礼品管理-礼品公示', null, 'admin/answerActivityUnit/filterList,admin/answerActivityGiftPublic/list', '113', '4', '', '108', '2', '1', '2023-08-02 16:24:45', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('130', '礼品公示-添加礼品', '', null, null, null, null, null, '3', '0033-0034-0113-0129-0130', '添加礼品', '悦读互动-在线答题-礼品管理-礼品公示-礼品公示-添加礼品', null, 'admin/answerActivityGiftPublic/add', '129', '5', '', '109', '2', '1', '2023-08-02 16:25:29', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('131', '礼品公示-详情', '', null, null, null, null, null, '3', '0033-0034-0113-0129-0131', '详情', '悦读互动-在线答题-礼品管理-礼品公示-礼品公示-详情', null, 'admin/answerActivityGiftPublic/list', '129', '5', '', '110', '2', '1', '2023-08-02 16:25:50', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('132', '礼品公示-修改', '', null, null, null, null, null, '3', '0033-0034-0113-0129-0132', '修改', '悦读互动-在线答题-礼品管理-礼品公示-礼品公示-修改', null, 'admin/answerActivityGiftPublic/change', '129', '5', '', '111', '2', '1', '2023-08-02 16:26:07', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('133', '礼品公示-删除', '', null, null, null, null, null, '3', '0033-0034-0113-0129-0133', '删除', '悦读互动-在线答题-礼品管理-礼品公示-礼品公示-删除', null, 'admin/answerActivityGiftPublic/del', '129', '5', '', '112', '2', '1', '2023-08-02 16:26:25', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('134', '在线抽奖', '', '在线抽奖', 'zxcj', 'DoubleList', '/Double/activityList', null, '1', '0033-0134', null, '悦读互动-在线抽奖', null, 'admin/turnActivity/list', '33', '2', '', '134', '2', '1', '2023-08-02 16:45:36', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('135', '在线抽奖-添加活动', '', null, null, null, null, null, '3', '0033-0134-0135', '添加活动', '悦读互动-在线抽奖-在线抽奖-添加活动', null, 'admin/turnActivity/add', '134', '3', '', '135', '2', '1', '2023-08-02 16:49:19', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('136', '在线抽奖-撤回', '', null, null, null, null, null, '3', '0033-0134-0136', '撤回', '悦读互动-在线抽奖-在线抽奖-撤回', null, 'admin/turnActivity/cancelAndRelease', '134', '3', '', '136', '2', '1', '2023-08-02 16:50:13', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('137', '在线抽奖-发布', '', null, null, null, null, null, '3', '0033-0134-0137', '发布', '悦读互动-在线抽奖-在线抽奖-发布', null, 'admin/turnActivity/cancelAndRelease', '134', '3', '', '137', '2', '1', '2023-08-02 16:50:43', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('138', '在线抽奖-详情', '', null, null, null, null, null, '3', '0033-0134-0138', '详情', '悦读互动-在线抽奖-在线抽奖-详情', null, 'admin/turnActivity/detail', '134', '3', '', '138', '2', '1', '2023-08-02 16:51:48', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('139', '在线抽奖-获奖列表', '', null, null, null, null, null, '3', '0033-0134-0139', '获奖列表', '悦读互动-在线抽奖-在线抽奖-获奖列表', null, 'admin/turnActivityUserGift/list', '134', '3', '', '139', '2', '1', '2023-08-02 16:55:17', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('140', '在线抽奖-礼品管理', '', null, null, null, null, null, '3', '0033-0134-0140', '礼品管理', '悦读互动-在线抽奖-在线抽奖-礼品管理', null, 'admin/turnActivityGift/list,admin/turnActivityGiftTime/getSendTotalConfig', '134', '3', '', '140', '2', '1', '2023-08-02 16:56:12', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('141', '在线抽奖-图片配置', '', null, null, null, null, null, '3', '0033-0134-0141', '图片配置', '悦读互动-在线抽奖-在线抽奖-图片配置', null, 'admin/turnActivityResource/getColorResource', '134', '3', '', '141', '2', '1', '2023-08-02 16:56:48', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('142', '在线抽奖-文字颜色配置', '', null, null, null, null, null, '3', '0033-0134-0142', '文字颜色配置', '悦读互动-在线抽奖-在线抽奖-文字颜色配置', null, 'admin/turnActivityResource/setColorResource', '134', '3', '', '142', '2', '1', '2023-08-02 16:57:57', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('143', '在线抽奖-修改邀请码', '', null, null, null, null, null, '3', '0033-0134-0143', '修改邀请码', '悦读互动-在线抽奖-在线抽奖-修改邀请码', null, 'admin/turnActivity/invitCodeChange', '134', '3', '', '143', '2', '1', '2023-08-02 16:58:28', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('144', '在线抽奖-数据统计', '', null, null, null, null, null, '3', '0033-0134-0144', '数据统计', '悦读互动-在线抽奖-在线抽奖-数据统计', null, 'admin/turnActivityDataAnalysis/giftGrantStatistics,admin/turnActivityDataAnalysis/drawStatistics,admin/turnActivityDataAnalysis/dataStatistics', '134', '3', '', '144', '2', '1', '2023-08-02 17:22:51', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('145', '在线抽奖-限制参与地区', '', null, null, null, null, null, '3', '0033-0134-0145', '限制参与地区', '悦读互动-在线抽奖-在线抽奖-限制参与地区', null, 'admin/turnActivityLimitAddress/list', '134', '3', '', '145', '2', '1', '2023-08-02 17:32:13', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('146', '在线抽奖-复制活动', '', null, null, null, null, null, '3', '0033-0134-0146', '复制活动', '悦读互动-在线抽奖-在线抽奖-复制活动', null, 'admin/turnActivity/copyData', '134', '3', '', '146', '2', '1', '2023-08-02 17:37:11', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('147', '在线抽奖-删除', '', null, null, null, null, null, '3', '0033-0134-0147', '删除', '悦读互动-在线抽奖-在线抽奖-删除', null, 'admin/turnActivity/del', '134', '3', '', '147', '2', '1', '2023-08-02 17:37:40', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('148', '抽奖添加活动', '', '添加活动', null, 'Doubleadd', '/Double/activityList/add', null, '2', '0033-0134-0148', null, '悦读互动-在线抽奖-添加活动', null, 'admin/turnActivity/add', '134', '3', '', '148', '2', '1', '2023-08-02 17:41:06', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('149', '抽奖活动详情', '', '详情', null, 'Doubleinfo', '/Double/activityList/info', null, '2', '0033-0134-0149', null, '悦读互动-在线抽奖-详情', null, 'admin/turnActivity/detail', '134', '3', '', '149', '2', '1', '2023-08-02 17:43:31', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('150', '抽奖活动修改', '', '修改', null, 'Doublechang', '/Double/activityList/chang', null, '2', '0033-0134-0150', null, '悦读互动-在线抽奖-修改', null, 'admin/turnActivity/detail,admin/turnActivity/change', '134', '3', '', '150', '2', '1', '2023-08-02 17:44:17', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('151', '抽奖限制参与地区', '', '限制参与地区', null, 'doubleResrictionsList', '/Double/activityList/resrictionsList', null, '2', '0033-0134-0151', null, '悦读互动-在线抽奖-限制参与地区', null, 'admin/turnActivityLimitAddress/list', '134', '3', '', '151', '2', '1', '2023-08-02 17:46:20', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('152', '抽奖限制参与地区-添加地址', '', '限制参与地区', null, 'doubleResrictionsList', '/Double/activityList/resrictionsList', null, '3', '0033-0134-0151-0152', '添加地址', '悦读互动-在线抽奖-限制参与地区-限制参与地区', null, 'admin/turnActivityLimitAddress/add', '151', '4', '', '152', '2', '1', '2023-08-02 17:47:02', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('153', '抽奖限制参与地区-修改地址', '', '限制参与地区', null, 'doubleResrictionsList', '/Double/activityList/resrictionsList', null, '3', '0033-0134-0151-0153', '修改', '悦读互动-在线抽奖-限制参与地区-限制参与地区', null, 'admin/turnActivityLimitAddress/change', '151', '4', '', '153', '2', '1', '2023-08-02 17:47:28', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('154', '抽奖限制参与地区-删除', '', '限制参与地区', null, 'doubleResrictionsList', '/Double/activityList/resrictionsList', null, '3', '0033-0134-0151-0154', '删除', '悦读互动-在线抽奖-限制参与地区-限制参与地区', null, 'admin/turnActivityLimitAddress/del', '151', '4', '', '154', '2', '1', '2023-08-02 17:47:47', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('155', '抽奖礼品管理', '', '礼品管理', null, 'DoubleGiftList', '/Double/activityList/GiftList', null, '2', '0033-0134-0155', null, '悦读互动-在线抽奖-礼品管理', null, 'admin/turnActivityGift/list', '134', '3', '', '155', '2', '1', '2023-08-02 18:04:26', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('156', '抽奖礼品管理-投放礼品', '', null, null, null, null, null, '3', '0033-0134-0155-0156', '投放礼品', '悦读互动-在线抽奖-礼品管理-抽奖礼品管理-投放礼品', null, 'admin/turnActivityGift/add', '155', '4', '', '156', '2', '1', '2023-08-03 11:53:31', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('157', '抽奖礼品管理-礼品发放管理', '', '礼品发放管理', null, 'DoubleGiftSet', '/Double/activityList/GiftList/GiftSet', null, '2', '0033-0134-0155-0157', '礼品发放管理', '悦读互动-在线抽奖-礼品管理-礼品发放管理', null, 'admin/turnActivityGiftTime/list', '155', '4', '', '157', '2', '1', '2023-08-03 11:54:00', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('158', '抽奖礼品管理-概率设置', '', null, null, null, null, null, '3', '0033-0134-0155-0158', '概率设置', '悦读互动-在线抽奖-礼品管理-抽奖礼品管理-概率设置', null, 'admin/turnActivityGiftTime/setSendTotalConfig', '155', '4', '', '162', '2', '1', '2023-08-03 11:54:23', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('159', '抽奖礼品管理-详情', '', null, null, null, null, null, '3', '0033-0134-0155-0159', '详情', '悦读互动-在线抽奖-礼品管理-抽奖礼品管理-详情', null, 'admin/turnActivityGift/detail', '155', '4', '', '163', '2', '1', '2023-08-03 11:54:51', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('160', '抽奖礼品管理-修改', '', null, null, null, null, null, '3', '0033-0134-0155-0160', '修改', '悦读互动-在线抽奖-礼品管理-抽奖礼品管理-修改', null, 'admin/turnActivityGift/detail,admin/turnActivityGift/change', '155', '4', '', '164', '2', '1', '2023-08-03 11:55:18', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('161', '抽奖礼品管理-删除', '', null, null, null, null, null, '3', '0033-0134-0155-0161', '删除', '悦读互动-在线抽奖-礼品管理-抽奖礼品管理-删除', null, 'admin/turnActivityGift/del', '155', '4', '', '165', '2', '1', '2023-08-03 11:55:45', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('162', '抽奖礼品管理-添加礼品', '', '投放礼品', null, 'addDoubleGift', '/Double/activityList/GiftList/add', null, '2', '0033-0134-0155-0162', null, '悦读互动-在线抽奖-礼品管理-投放礼品', null, 'admin/turnActivityGift/add', '155', '4', '', '166', '2', '1', '2023-08-03 11:59:39', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('163', '抽奖礼品管理-修改礼品', '', '修改礼品', null, 'changDoubleGift', '/Double/activityList/GiftList/chang', null, '2', '0033-0134-0155-0163', null, '悦读互动-在线抽奖-礼品管理-修改礼品', null, 'admin/turnActivityGift/detail,admin/turnActivityGift/change', '155', '4', '', '167', '2', '1', '2023-08-03 13:04:14', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('164', '抽奖礼品管理-礼品详情', '', '礼品详情', null, 'DoubleGiftinfo', '/Double/activityList/GiftList/info', null, '2', '0033-0134-0155-0164', null, '悦读互动-在线抽奖-礼品管理-礼品详情', null, 'admin/turnActivityGift/detail', '155', '4', '', '168', '2', '1', '2023-08-03 13:05:58', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('165', '抽奖礼品管理-礼品发放列表', '', '礼品发放管理', null, 'DoubleGiftSet', '/Double/activityList/GiftList/GiftSet', null, '2', '0033-0134-0155-0165', null, '悦读互动-在线抽奖-礼品管理-礼品发放管理', null, 'admin/turnActivityGiftTime/list', '155', '4', '', '169', '2', '1', '2023-08-03 13:09:53', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('166', '抽奖礼品管理-礼品发放列表-新增发放限制', '', null, null, null, null, null, '3', '0033-0134-0155-0157-0166', '新增发放限制', '悦读互动-在线抽奖-礼品管理-抽奖礼品管理-礼品发放管理-抽奖礼品管理-礼品发放列表-新增发放限制', null, 'admin/turnActivityGiftTime/add', '157', '5', '', '158', '2', '1', '2023-08-03 13:11:00', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('167', '抽奖礼品管理-礼品发放列表-详情', '', null, null, null, null, null, '3', '0033-0134-0155-0157-0167', '详情', '悦读互动-在线抽奖-礼品管理-抽奖礼品管理-礼品发放管理-抽奖礼品管理-礼品发放列表-详情', null, 'admin/turnActivityGiftTime/list', '157', '5', '', '159', '2', '1', '2023-08-03 13:11:24', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('168', '抽奖礼品管理-礼品发放列表-修改', '', null, null, null, null, null, '3', '0033-0134-0155-0157-0168', '修改', '悦读互动-在线抽奖-礼品管理-抽奖礼品管理-礼品发放管理-抽奖礼品管理-礼品发放列表-修改', null, 'admin/turnActivityGiftTime/change', '157', '5', '', '160', '2', '1', '2023-08-03 13:11:43', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('169', '抽奖礼品管理-礼品发放列表-删除', '', null, null, null, null, null, '3', '0033-0134-0155-0157-0169', '删除', '悦读互动-在线抽奖-礼品管理-抽奖礼品管理-礼品发放管理-抽奖礼品管理-礼品发放列表-删除', null, 'admin/turnActivityGiftTime/del', '157', '5', '', '161', '2', '1', '2023-08-03 13:12:00', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('170', '抽奖获奖列表', '', '获奖列表', null, 'DoubleawardList', '/Double/activityList/awardList', null, '2', '0033-0134-0170', '获奖列表', '悦读互动-在线抽奖-获奖列表', null, 'admin/turnActivityUserGift/list', '134', '3', '', '170', '2', '1', '2023-08-03 13:17:33', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('171', '抽奖获奖列表-导出列表', '', null, null, null, null, null, '3', '0033-0134-0170-0171', '导出列表', '悦读互动-在线抽奖-获奖列表-抽奖获奖列表-导出列表', null, 'admin/turnActivityUserGiftExport/index', '170', '4', '', '171', '2', '1', '2023-08-03 13:19:18', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('172', '抽奖获奖列表-操作', '', null, null, null, null, null, '3', '0033-0134-0170-0172', '操作', '悦读互动-在线抽奖-获奖列表-抽奖获奖列表-操作', null, 'admin/turnActivityUserGift/userGiftGrant', '170', '4', '', '172', '2', '1', '2023-08-03 13:20:07', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('173', '抽奖数据统计', '', '后台操作记录', null, 'dataStatistics', '/Double/activityList/dataStatistics', null, '2', '0033-0134-0173', '数据统计', '悦读互动-在线抽奖-后台操作记录', null, 'admin/turnActivityDataAnalysis/dataStatistics,admin/turnActivityDataAnalysis/giftGrantStatistics,admin/turnActivityDataAnalysis/drawStatistics', '134', '3', '', '173', '2', '1', '2023-08-03 13:24:06', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('174', '抽奖图片配置', '', '图片配置', null, 'DoubleUIimgList', '/Double/activityList/UIimg', null, '2', '0033-0134-0174', null, '悦读互动-在线抽奖-图片配置', null, 'admin/turnActivityResource/getColorResource,admin/turnActivityResource/resourceUpload', '134', '3', '', '174', '2', '1', '2023-08-03 13:25:48', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('175', '抽奖文字配置', '', '文字配置', null, 'DoubleUItextList', '/Double/activityList/UItext', null, '2', '0033-0134-0175', null, '悦读互动-在线抽奖-文字配置', null, 'admin/turnActivityResource/getColorResource,admin/turnActivityResource/setColorResource', '134', '3', '', '175', '2', '1', '2023-08-03 13:27:07', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('176', '在线投票', '', '在线投票', 'game_list', 'megagameList', '/CompetitionManagement/megagameList', null, '1', '0033-0176', null, '悦读互动-在线投票', null, 'admin/contestActivity/list', '33', '2', '', '176', '2', '1', '2023-08-03 13:27:35', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('177', '在线投票-创建活动', '', null, null, null, null, null, '3', '0033-0176-0177', '创建活动', '悦读互动-在线投票-在线投票-创建活动', null, 'admin/contestActivity/add', '176', '3', '', '177', '2', '1', '2023-08-03 13:29:06', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('178', '在线投票-撤回', '', null, null, null, null, null, '3', '0033-0176-0178', '撤回', '悦读互动-在线投票-在线投票-撤回', null, 'admin/contestActivity/playAndCancel', '176', '3', '', '178', '2', '1', '2023-08-03 13:29:45', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('179', '在线投票-发布', '', null, null, null, null, null, '3', '0033-0176-0179', '发布', '悦读互动-在线投票-在线投票-发布', null, 'admin/contestActivity/playAndCancel', '176', '3', '', '179', '2', '1', '2023-08-03 13:31:57', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('180', '在线投票-详情', '', null, null, null, null, null, '3', '0033-0176-0180', '详情', '悦读互动-在线投票-在线投票-详情', null, 'admin/contestActivity/detail', '176', '3', '', '180', '2', '1', '2023-08-03 13:48:14', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('181', '在线投票-修改', '', null, null, null, null, null, '3', '0033-0176-0181', '修改', '悦读互动-在线投票-在线投票-修改', null, 'admin/contestActivity/change', '176', '3', '', '181', '2', '1', '2023-08-03 13:48:37', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('182', '在线投票-作品管理', '', null, null, null, null, null, '3', '0033-0176-0182', '作品管理', '悦读互动-在线投票-在线投票-作品管理', null, 'admin/contestActivityWorks/list', '176', '3', '', '182', '2', '1', '2023-08-03 13:49:19', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('183', '在线投票-类型管理', '', null, null, null, null, null, '3', '0033-0176-0183', '类型管理', '悦读互动-在线投票-在线投票-类型管理', null, 'admin/contestActivityType/list', '176', '3', '', '183', '2', '1', '2023-08-03 13:49:43', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('184', '在线投票-单位管理', '', null, null, null, null, null, '3', '0033-0176-0184', '单位管理', '悦读互动-在线投票-在线投票-单位管理', null, 'admin/contestActivityUnit/list', '176', '3', '', '184', '2', '1', '2023-08-03 13:50:37', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('185', '在线投票-删除', '', null, null, null, null, null, '3', '0033-0176-0185', '删除', '悦读互动-在线投票-在线投票-删除', null, 'admin/contestActivity/del', '176', '3', '', '185', '2', '1', '2023-08-03 13:50:54', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('186', '投票创建活动', '', '创建活动', null, 'addmegagame', '/CompetitionManagement/megagameList/add', null, '2', '0033-0176-0186', '创建活动', '悦读互动-在线投票-创建活动', null, 'admin/contestActivity/add', '176', '3', '', '186', '2', '1', '2023-08-03 13:51:48', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('187', '投票修改活动', '', '修改活动', null, 'changmegagame', '/CompetitionManagement/megagameList/chang', null, '2', '0033-0176-0187', '修改', '悦读互动-在线投票-修改活动', null, 'admin/contestActivity/detail,admin/contestActivity/change', '176', '3', '', '187', '2', '1', '2023-08-03 13:53:04', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('188', '投票作品管理', '', '作品管理', null, 'entry', '/CompetitionManagement/megagameList/management/entry', null, '2', '0033-0176-0188', '作品管理', '悦读互动-在线投票-作品管理', null, 'admin/contestActivityWorks/list,admin/contestActivityType/filterList,admin/contestActivity/detail,admin/contestActivityWorks/violateAndCancel,admin/contestActivityWorks/check', '176', '3', '', '188', '2', '1', '2023-08-03 13:55:17', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('189', '投票作品管理-作品查看', '', '作品查看', null, 'entryInfo', '/CompetitionManagement/megagameList/management/entry/info', null, '2', '0033-0176-0189', null, '悦读互动-在线投票-作品查看', null, 'admin/contestActivityWorks/detail,admin/contestActivityWorks/check', '176', '3', '', '189', '2', '1', '2023-08-03 13:59:34', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('190', '投票类型管理', '', '类型管理', null, 'typeWorks', '/CompetitionManagement/megagameList/management/typeWorks', null, '2', '0033-0176-0190', '类型管理', '悦读互动-在线投票-类型管理', null, 'admin/contestActivityType/list', '176', '3', '', '190', '2', '1', '2023-08-03 14:01:12', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('191', '投票类型管理-添加类型', '', null, null, null, null, null, '3', '0033-0176-0190-0191', '添加类型', '悦读互动-在线投票-类型管理-投票类型管理-添加类型', null, 'admin/contestActivityType/add', '190', '4', '', '191', '2', '1', '2023-08-03 14:02:13', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('192', '投票类型管理-修改', '', null, null, null, null, null, '3', '0033-0176-0190-0192', '修改', '悦读互动-在线投票-类型管理-投票类型管理-修改', null, 'admin/contestActivityType/change', '190', '4', '', '192', '2', '1', '2023-08-03 14:02:36', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('193', '投票类型管理-删除', '', null, null, null, null, null, '3', '0033-0176-0190-0193', '删除', '悦读互动-在线投票-类型管理-投票类型管理-删除', null, 'admin/contestActivityType/del', '190', '4', '', '193', '2', '1', '2023-08-03 14:02:53', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('194', '投票单位管理', '', '单位管理', null, 'megagameListareaList', '/CompetitionManagement/megagameList/areaList', null, '2', '0033-0176-0194', null, '悦读互动-在线投票-单位管理', null, 'admin/contestActivityUnit/list,admin/contestActivityUnit/getNatureName', '176', '3', '', '194', '2', '1', '2023-08-03 14:23:11', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('195', '投票单位管理-添加单位', '', '添加单位', null, 'megagameListaddarea', '/CompetitionManagement/megagameList/areaList/add', null, '2', '0033-0176-0194-0195', '添加单位', '悦读互动-在线投票-单位管理-添加单位', null, 'admin/contestActivityUnit/add', '194', '4', '', '195', '2', '1', '2023-08-03 14:24:08', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('196', '投票单位管理-单位排序', '', '添加单位', null, 'megagameListaddarea', '/CompetitionManagement/megagameList/areaList/add', null, '3', '0033-0176-0194-0196', '单位排序', '悦读互动-在线投票-单位管理-添加单位', null, 'admin/contestActivityUnit/orderChange', '194', '4', '', '196', '2', '1', '2023-08-03 14:27:47', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('197', '投票单位管理-详情', '', '详情', null, 'megagameListinfoarea', '/CompetitionManagement/megagameList/areaList/info', null, '2', '0033-0176-0194-0197', '详情', '悦读互动-在线投票-单位管理-详情', null, 'admin/contestActivityUnit/detail', '194', '4', '', '197', '2', '1', '2023-08-03 14:29:49', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('198', '投票单位管理-修改', '', '修改', null, 'megagameListchangarea', '/CompetitionManagement/megagameList/areaList/chang', null, '2', '0033-0176-0194-0198', '修改', '悦读互动-在线投票-单位管理-修改', null, 'admin/contestActivityUnit/detail,admin/contestActivityUnit/change', '194', '4', '', '198', '2', '1', '2023-08-03 14:31:25', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('199', '投票单位管理-删除', '', '修改', null, 'megagameListchangarea', '/CompetitionManagement/megagameList/areaList/chang', null, '3', '0033-0176-0194-0199', '删除', '悦读互动-在线投票-单位管理-修改', null, 'admin/contestActivityUnit/del', '194', '4', '', '199', '2', '1', '2023-08-03 14:31:42', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('200', '图片直播', '', '图片直播', 'tupianzhibo', 'megagameListPicture', '/PictureStreaming/megagameList', null, '1', '0033-0200', null, '悦读互动-图片直播', null, 'admin/pictureLive/list', '33', '2', '', '200', '2', '1', '2023-08-03 14:35:02', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('201', '图片直播-创建直播', '', null, null, null, null, null, '3', '0033-0200-0201', '创建直播', '悦读互动-图片直播-图片直播-创建直播', null, 'admin/pictureLive/add', '200', '3', '', '201', '2', '1', '2023-08-03 14:47:24', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('202', '图片直播-黑名单列表', '', null, null, null, null, null, '3', '0033-0200-0202', '黑名单列表', '悦读互动-图片直播-图片直播-黑名单列表', null, 'admin/pictureLiveBlack/list', '200', '3', '', '202', '2', '1', '2023-08-03 14:50:50', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('203', '图片直播-撤销', '', null, null, null, null, null, '3', '0033-0200-0203', '撤销', '悦读互动-图片直播-图片直播-撤销', null, 'admin/pictureLive/cancelAndRelease', '200', '3', '', '203', '2', '1', '2023-08-03 14:51:31', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('204', '图片直播-发布', '', null, null, null, null, null, '3', '0033-0200-0204', '发布', '悦读互动-图片直播-图片直播-发布', null, 'admin/pictureLive/cancelAndRelease', '200', '3', '', '204', '2', '1', '2023-08-03 14:51:44', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('205', '图片直播-详情', '', null, null, null, null, null, '3', '0033-0200-0205', '详情', '悦读互动-图片直播-图片直播-详情', null, 'admin/pictureLive/detail', '200', '3', '', '205', '2', '1', '2023-08-03 14:52:11', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('206', '图片直播-修改', '', null, null, null, null, null, '3', '0033-0200-0206', '修改', '悦读互动-图片直播-图片直播-修改', null, 'admin/pictureLive/change', '200', '3', '', '206', '2', '1', '2023-08-03 14:53:26', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('207', '图片直播-删除', '', null, null, null, null, null, '3', '0033-0200-0207', '删除', '悦读互动-图片直播-图片直播-删除', null, 'admin/pictureLive/del', '200', '3', '', '207', '2', '1', '2023-08-03 14:55:05', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('208', '直播创建直播', '', '创建直播', null, 'addmegagamePicture', '/PictureStreaming/megagameList/add', null, '2', '0033-0200-0208', '创建直播', '悦读互动-图片直播-创建直播', null, 'admin/pictureLive/add', '200', '3', '', '208', '2', '1', '2023-08-03 14:56:10', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('209', '直播修改直播', '', '修改直播', null, 'changmegagamePicture', '/PictureStreaming/megagameList/chang', null, '2', '0033-0200-0209', '修改', '悦读互动-图片直播-修改直播', null, 'admin/pictureLive/change,admin/pictureLive/detail', '200', '3', '', '209', '2', '1', '2023-08-03 15:00:37', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('210', '直播图片管理', '', '图片管理', null, 'entryPicture', '/PictureStreaming/megagameList/management/entry', null, '2', '0033-0200-0210', '图片管理', '悦读互动-图片直播-图片管理', null, 'admin/pictureLiveWorks/list', '200', '3', '', '210', '2', '1', '2023-08-03 15:05:31', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('211', '直播图片管理-批量上传', '', null, null, null, null, null, '3', '0033-0200-0210-0211', '批量上传', '悦读互动-图片直播-图片管理-直播图片管理-批量上传', null, 'admin/pictureLiveWorks/batchAdd', '210', '4', '', '211', '2', '1', '2023-08-03 15:11:29', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('212', '直播图片管理-单个上传', '', null, null, null, null, null, '3', '0033-0200-0210-0212', '单个上传', '悦读互动-图片直播-图片管理-直播图片管理-单个上传', null, 'admin/pictureLiveWorks/add', '210', '4', '', '212', '2', '1', '2023-08-03 15:11:54', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('213', '直播图片管理-批量下载', '', null, null, null, null, null, '3', '0033-0200-0210-0213', '批量下载', '悦读互动-图片直播-图片管理-直播图片管理-批量下载', null, 'admin/pictureLiveWorks/downloadWorksImg', '210', '4', '', '213', '2', '1', '2023-08-03 15:12:20', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('214', '直播图片管理-同意', '', null, null, null, null, null, '3', '0033-0200-0210-0214', '同意', '悦读互动-图片直播-图片管理-直播图片管理-同意', null, 'admin/pictureLiveWorks/worksCheck', '210', '4', '', '214', '2', '1', '2023-08-03 15:13:29', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('215', '直播图片管理-拒绝', '', null, null, null, null, null, '3', '0033-0200-0210-0215', '拒绝', '悦读互动-图片直播-图片管理-直播图片管理-拒绝', null, 'admin/pictureLiveWorks/worksCheck', '210', '4', '', '215', '2', '1', '2023-08-03 15:14:08', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('216', '直播图片管理-删除', '', null, null, null, null, null, '3', '0033-0200-0210-0216', '删除', '悦读互动-图片直播-图片管理-直播图片管理-删除', null, 'admin/pictureLiveWorks/del', '210', '4', '', '216', '2', '1', '2023-08-03 15:14:26', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('217', '图片管理-批量上传', '', '批量上传', null, 'batchUpload', '/PictureStreaming/megagameList/management/batchUpload', null, '2', '0033-0200-0210-0217', '批量上传', '悦读互动-图片直播-图片管理-批量上传', null, 'admin/pictureLiveWorks/batchAdd,admin/pictureLiveAppoint/list,admin/userInfo/list', '210', '4', '', '217', '2', '1', '2023-08-03 15:15:41', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('218', '图片管理-黑名单用户', '', '黑名单用户', null, 'specifyUser', '/PictureStreaming/megagameList/specifyUser', null, '2', '0033-0200-0218', '黑名单用户', '悦读互动-图片直播-黑名单用户', null, 'admin/pictureLiveBlack/list,admin/userInfo/list', '200', '3', '', '218', '2', '1', '2023-08-03 15:16:41', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('219', '图片管理-黑名单用户-添加', '', null, null, null, null, null, '3', '0033-0200-0218-0219', '添加', '悦读互动-图片直播-黑名单用户-图片管理-黑名单用户-添加', null, 'admin/pictureLiveBlack/add', '218', '4', '', '219', '2', '1', '2023-08-03 15:17:36', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('220', '图片管理-黑名单用户-删除', '', null, null, null, null, null, '3', '0033-0200-0218-0220', '删除', '悦读互动-图片直播-黑名单用户-图片管理-黑名单用户-删除', null, 'admin/pictureLiveBlack/del', '218', '4', '', '220', '2', '1', '2023-08-03 15:17:48', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('221', '问卷列表', '', '问卷列表', 'questionnaire', 'QuestionnaireList', '/questionnaireSurvey/questionnaireList', null, '1', '0033-0221', null, '悦读互动-问卷列表', null, 'admin/survey/list', '33', '2', '', '225', '2', '1', '2023-08-03 15:28:03', '2023-08-31 13:21:13');
INSERT INTO `re_permission` VALUES ('222', '问卷调查-创建问卷', '', '创建问卷', null, 'AddQuestionnaire', '/questionnaireSurvey/addQuestionnaire', null, '2', '0033-0221-0222', '创建问卷', '悦读互动-问卷列表-创建问卷', null, 'admin/survey/add', '221', '3', '', '226', '2', '1', '2023-08-03 15:29:33', '2023-08-31 13:21:13');
INSERT INTO `re_permission` VALUES ('223', '问卷调查-修改', '', '修改', null, 'ChangQuestionnaire', '/questionnaireSurvey/changQuestionnaire', null, '2', '0033-0221-0223', '修改', '悦读互动-问卷列表-修改', null, 'admin/survey/detail,admin/survey/change', '221', '3', '', '227', '2', '1', '2023-08-03 15:30:19', '2023-08-31 13:21:13');
INSERT INTO `re_permission` VALUES ('224', '问卷调查-发布', '', '修改', null, 'ChangQuestionnaire', '/questionnaireSurvey/changQuestionnaire', null, '3', '0033-0221-0224', '发布', '悦读互动-问卷列表-修改', null, 'admin/survey/cancelAndRelease', '221', '3', '', '228', '2', '1', '2023-08-03 15:30:48', '2023-08-31 13:21:13');
INSERT INTO `re_permission` VALUES ('225', '问卷调查-撤销', '', '修改', null, 'ChangQuestionnaire', '/questionnaireSurvey/changQuestionnaire', null, '3', '0033-0221-0225', '撤销', '悦读互动-问卷列表-修改', null, 'admin/survey/cancelAndRelease', '221', '3', '', '229', '2', '1', '2023-08-03 15:31:07', '2023-08-31 13:21:13');
INSERT INTO `re_permission` VALUES ('226', '问卷调查-问卷详情', '', '问卷详情', null, 'QuestionnaireInfo', '/questionnaireSurvey/questionnaireInfo', null, '2', '0033-0221-0226', '详情', '悦读互动-问卷列表-问卷详情', null, 'admin/survey/list,admin/survey/detail', '221', '3', '', '230', '2', '1', '2023-08-03 15:37:47', '2023-08-31 13:21:13');
INSERT INTO `re_permission` VALUES ('227', '问卷调查-问卷详情-用户回复统计导出', '', null, null, null, null, null, '3', '0033-0221-0226-0227', '用户回复统计导出', '悦读互动-问卷列表-问卷详情-问卷调查-问卷详情-用户回复统计导出', null, 'admin/surveyExport/index', '226', '4', '', '231', '2', '1', '2023-08-03 15:38:43', '2023-08-31 13:21:13');
INSERT INTO `re_permission` VALUES ('228', '问卷调查-删除', '', null, null, null, null, null, '3', '0033-0221-0228', '删除', '悦读互动-问卷列表-问卷调查-删除', null, 'admin/survey/del', '221', '3', '', '232', '2', '1', '2023-08-03 15:39:24', '2023-08-31 13:21:13');
INSERT INTO `re_permission` VALUES ('229', '读者荐购', '', '读者荐购', 'duzhejiangou', null, null, null, '1', '0229', null, '读者荐购', null, '', '0', '1', '', '233', '2', '1', '2023-08-03 15:41:29', '2023-08-31 13:21:13');
INSERT INTO `re_permission` VALUES ('230', '荐购列表', '', '荐购列表', 'recommend', 'RecommendationList', '/ReaderLnteraction/RecommendationList', null, '1', '0229-0230', null, '读者荐购-荐购列表', null, 'admin/recommendBook/list', '229', '2', '', '234', '2', '1', '2023-08-03 15:43:26', '2023-08-31 13:21:13');
INSERT INTO `re_permission` VALUES ('231', '荐购列表-审核通过', '', null, null, null, null, null, '3', '0229-0230-0231', '审核通过', '读者荐购-荐购列表-荐购列表-审核通过', null, 'admin/recommendBook/agreeAndRefused', '230', '3', '', '235', '2', '1', '2023-08-03 15:58:00', '2023-08-31 13:21:13');
INSERT INTO `re_permission` VALUES ('232', '荐购列表-审核不通过', '', null, null, null, null, null, '3', '0229-0230-0232', '审核不通过', '读者荐购-荐购列表-荐购列表-审核不通过', null, 'admin/recommendBook/agreeAndRefused', '230', '3', '', '236', '2', '1', '2023-08-03 16:06:05', '2023-08-31 13:21:13');
INSERT INTO `re_permission` VALUES ('233', '荐购列表-确定购买', '', null, null, null, null, null, '3', '0229-0230-0233', '确定购买', '读者荐购-荐购列表-荐购列表-确定购买', null, 'admin/recommendBook/agreeAndRefused', '230', '3', '', '237', '2', '1', '2023-08-03 16:11:38', '2023-08-31 13:21:13');
INSERT INTO `re_permission` VALUES ('234', '荐购列表-拒绝购买', '', null, null, null, null, null, '3', '0229-0230-0234', '拒绝购买', '读者荐购-荐购列表-荐购列表-拒绝购买', null, 'admin/recommendBook/agreeAndRefused', '230', '3', '', '238', '2', '1', '2023-08-03 16:12:15', '2023-08-31 13:21:13');
INSERT INTO `re_permission` VALUES ('235', '荐购列表-导出列表', '', null, null, null, null, null, '3', '0229-0230-0235', '导出列表', '读者荐购-荐购列表-荐购列表-导出列表', null, 'admin/recommendBookExport/index', '230', '3', '', '239', '2', '1', '2023-08-03 16:12:48', '2023-08-31 13:21:13');
INSERT INTO `re_permission` VALUES ('236', '服务指南', '', '服务指南', 'fuwuzhinan1', null, null, null, '1', '0236', null, '服务指南', null, '', '0', '1', '', '240', '2', '1', '2023-08-03 16:14:09', '2023-08-31 13:21:13');
INSERT INTO `re_permission` VALUES ('237', '服务指南', '', '服务指南', 'fuwuzhinan2', 'ServiceList', '/servicedirectory/serviceList', null, '1', '0236-0237', null, '服务指南-服务指南', null, 'admin/serviceDirectory/list', '236', '2', '', '241', '2', '1', '2023-08-03 16:15:01', '2023-08-31 13:21:13');
INSERT INTO `re_permission` VALUES ('238', '服务指南-修改', '', null, null, null, null, null, '3', '0236-0237-0238', '修改', '服务指南-服务指南-服务指南-修改', null, 'admin/serviceDirectory/detail,admin/serviceDirectory/change', '237', '3', '', '242', '2', '1', '2023-08-03 16:55:32', '2023-08-31 13:21:13');
INSERT INTO `re_permission` VALUES ('239', '新闻动态', '', '新闻动态', 'xinwennewguanli', null, null, null, '1', '0239', null, '新闻动态', null, '', '0', '1', '', '244', '2', '1', '2023-08-03 16:58:41', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('240', '新闻列表', '', '新闻列表', 'xinwennewliebiao', 'NewsManagementNewsList', '/NewsManagement/NewsList', null, '1', '0239-0240', null, '新闻管理-新闻列表', null, 'admin/news/list,admin/newsType/filterList', '239', '2', '', '245', '2', '1', '2023-08-03 16:59:35', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('241', '新闻列表-添加新闻', '', null, null, null, null, null, '3', '0239-0240-0241', '添加新闻', '新闻管理-新闻列表-新闻列表-添加新闻', null, 'admin/news/add', '240', '3', '', '246', '2', '1', '2023-08-03 17:04:13', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('242', '新闻列表-置顶', '', null, null, null, null, null, '3', '0239-0240-0242', '置顶', '新闻管理-新闻列表-新闻列表-置顶', null, 'admin/news/topAndCancel', '240', '3', '', '247', '2', '1', '2023-08-03 17:04:41', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('243', '新闻列表-取消置顶', '', null, null, null, null, null, '3', '0239-0240-0243', '取消置顶', '新闻管理-新闻列表-新闻列表-取消置顶', null, 'admin/news/topAndCancel', '240', '3', '', '248', '2', '1', '2023-08-03 17:04:57', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('244', '新闻列表-修改', '', null, null, null, null, null, '3', '0239-0240-0244', '修改', '新闻管理-新闻列表-新闻列表-修改', null, 'admin/news/change', '240', '3', '', '249', '2', '1', '2023-08-03 17:07:33', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('245', '新闻列表-删除', '', null, null, null, null, null, '3', '0239-0240-0245', '删除', '新闻管理-新闻列表-新闻列表-删除', null, 'admin/news/del', '240', '3', '', '251', '2', '1', '2023-08-03 17:08:03', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('246', '新闻列表-添加新闻', '', '添加新闻', null, 'NewsManagementAddNews', '/NewsManagement/addNews', null, '2', '0239-0240-0246', '添加新闻', '新闻管理-新闻列表-添加新闻', null, 'admin/news/add', '240', '3', '', '252', '2', '1', '2023-08-03 17:12:26', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('247', '新闻列表-修改新闻', '', '修改新闻', null, 'NewsManagementChangNews', '/NewsManagement/changNews', null, '2', '0239-0240-0247', '修改', '新闻管理-新闻列表-修改新闻', null, 'admin/news/detail,admin/newsType/filterList,admin/news/change', '240', '3', '', '250', '2', '1', '2023-08-03 17:16:15', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('248', '新闻类型', '', '新闻类型', 'xinwenewnleixing', 'NewsManagementNewsType', '/NewsManagement/NewsType', null, '1', '0239-0248', null, '新闻管理-新闻类型', null, 'admin/newsType/list', '239', '2', '', '253', '2', '1', '2023-08-03 17:17:21', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('249', '新闻类型-添加类型', '', null, null, null, null, null, '3', '0239-0248-0249', '添加类型', '新闻管理-新闻类型-新闻类型-添加类型', null, 'admin/newsType/add', '248', '3', '', '254', '2', '1', '2023-08-03 17:26:52', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('250', '新闻类型-修改', '', null, null, null, null, null, '3', '0239-0248-0250', '修改', '新闻管理-新闻类型-新闻类型-修改', null, 'admin/newsType/change', '248', '3', '', '255', '2', '1', '2023-08-03 17:30:28', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('251', '新闻类型-删除', '', null, null, null, null, null, '3', '0239-0248-0251', '删除', '新闻管理-新闻类型-新闻类型-删除', null, 'admin/newsType/del', '248', '3', '', '256', '2', '1', '2023-08-03 17:30:42', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('252', '指尖办证', '', '指尖办证', 'banzheng', null, null, null, '1', '0252', null, '指尖办证', null, '', '0', '1', '', '258', '2', '1', '2023-08-03 17:34:36', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('253', '办证操作', '', '办证操作', 'banzheng1', null, null, null, '1', '0252-0253', null, '办证管理-办证操作', null, '', '252', '2', '', '259', '2', '1', '2023-08-03 17:39:52', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('254', '办证操作台', '', '办证操作台', null, 'operationConsole', '/certificateHandling/operationConsole', null, '1', '0252-0253-0254', null, '办证管理-办证操作-办证操作台', null, 'admin/onlineRegistration/registration,admin/onlineRegistration/getDynamicParam,admin/shop/filterList,admin/onlineRegistration/getCertificateType,admin/onlineRegistration/getFrontCardType,admin/onlineRegistration/sendVerify', '253', '3', '', '260', '2', '1', '2023-08-03 17:42:32', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('255', '线下办证历史(图书馆端)', '', '线下办证历史(图书馆端)', null, 'LibCertificateHistory', '/certificateHandling/LibCertificateHistory', null, '1', '0252-0253-0255', null, '办证管理-办证操作-线下办证历史(图书馆端)', null, 'admin/onlineRegistration/sponsorRegistrationStatistics,admin/onlineRegistration/sponsorRegistrationList,admin/manage/filterList,admin/shop/filterList,admin/onlineRegistration/getCertificateType,admin/onlineRegistration/getFrontCardType', '253', '3', '', '261', '2', '1', '2023-08-03 17:49:25', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('256', '线下办证历史(图书馆端)-全部结算', '', null, null, null, null, null, '3', '0252-0253-0255-0256', '全部结算', '办证管理-办证操作-线下办证历史(图书馆端)-线下办证历史(图书馆端)-全部结算', null, 'admin/onlineRegistration/confirmSettleState', '255', '4', '', '262', '2', '1', '2023-08-03 17:52:00', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('257', '线下办证历史(图书馆端)-确认结算', '', null, null, null, null, null, '3', '0252-0253-0255-0257', '确认结算', '办证管理-办证操作-线下办证历史(图书馆端)-线下办证历史(图书馆端)-确认结算', null, 'admin/onlineRegistration/confirmSettleState', '255', '4', '', '263', '2', '1', '2023-08-03 17:52:22', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('258', '线下办证历史(图书馆端)-详情', '', null, null, null, null, null, '3', '0252-0253-0255-0258', '详情', '办证管理-办证操作-线下办证历史(图书馆端)-线下办证历史(图书馆端)-详情', null, 'admin/onlineRegistration/sponsorRegistrationList', '255', '4', '', '264', '2', '1', '2023-08-03 17:52:59', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('259', '线下办证历史(书店端)', '', '线下办证历史(书店端)', null, 'shopCertificateHistory', '/certificateHandling/shopCertificateHistory', null, '1', '0252-0253-0259', null, '办证管理-办证操作-线下办证历史(书店端)', null, 'admin/onlineRegistration/confirmPostageStatistics,admin/onlineRegistration/confirmPostageList,admin/manage/filterList,admin/shop/filterList,admin/onlineRegistration/getCertificateType,admin/onlineRegistration/getFrontCardType', '253', '3', '', '265', '2', '1', '2023-08-03 17:57:16', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('260', '线下办证历史(书店端)-全部结算', '', null, null, null, null, null, '3', '0252-0253-0259-0260', '全部结算', '办证管理-办证操作-线下办证历史(书店端)-线下办证历史(书店端)-全部结算', null, 'admin/onlineRegistration/sponsorSettleState', '259', '4', '', '266', '2', '1', '2023-08-03 18:03:09', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('261', '线下办证历史(书店端)-确认结算', '', null, null, null, null, null, '3', '0252-0253-0259-0261', '确认结算', '办证管理-办证操作-线下办证历史(书店端)-线下办证历史(书店端)-确认结算', null, 'admin/onlineRegistration/sponsorSettleState', '259', '4', '', '267', '2', '1', '2023-08-03 18:03:20', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('262', '线下办证历史(书店端)-详情', '', null, null, null, null, null, '3', '0252-0253-0259-0262', '详情', '办证管理-办证操作-线下办证历史(书店端)-线下办证历史(书店端)-详情', null, 'admin/onlineRegistration/confirmPostageList', '259', '4', '', '268', '2', '1', '2023-08-03 18:03:41', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('263', '线上办证历史', '', '线上办证历史', null, 'onlineHistory', '/certificateHandling/onlineHistory', null, '1', '0252-0253-0263', null, '办证管理-办证操作-线上办证历史', null, 'admin/onlineRegistration/sponsorRegistrationList,admin/manage/filterList,admin/onlineRegistration/getCertificateType,admin/onlineRegistration/getFrontCardType', '253', '3', '', '269', '2', '1', '2023-08-04 09:20:35', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('264', '线上办证历史-详情', '', null, null, null, null, null, '3', '0252-0253-0263-0264', '详情', '办证管理-办证操作-线上办证历史-线上办证历史-详情', null, 'admin/onlineRegistration/sponsorRegistrationList', '263', '4', '', '270', '2', '1', '2023-08-04 09:21:58', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('265', '线上办证历史-领取', '', null, null, null, null, null, '3', '0252-0253-0263-0265', '领取', '办证管理-办证操作-线上办证历史-线上办证历史-领取', null, 'admin/onlineRegistration/sponsorRegistrationList,admin/onlineRegistration/toReceive', '263', '4', '', '271', '2', '1', '2023-08-04 09:22:21', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('266', '退款列表', '', '退款列表', null, 'refundList', '/certificateHandling/refundList', null, '1', '0252-0253-0266', null, '办证管理-办证操作-退款列表', null, 'admin/onlineRegistration/sponsorRegistrationList,admin/manage/filterList,admin/shop/filterList,admin/onlineRegistration/getCertificateType,admin/onlineRegistration/getFrontCardType', '253', '3', '', '272', '2', '1', '2023-08-04 09:25:35', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('267', '退款列表-详情', '', null, null, null, null, null, '3', '0252-0253-0266-0267', '详情', '办证管理-办证操作-退款列表-退款列表-详情', null, 'admin/onlineRegistration/sponsorRegistrationList', '266', '4', '', '273', '2', '1', '2023-08-04 09:26:19', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('268', '退款列表-退款', '', null, null, null, null, null, '3', '0252-0253-0266-0268', '退款', '办证管理-办证操作-退款列表-退款列表-退款', null, 'admin/onlineRegistration/refund', '266', '4', '', '274', '2', '1', '2023-08-04 09:26:48', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('269', '办证协议', '', '办证协议', null, 'agreeOn', '/certificateHandling/agreeOn', null, '1', '0252-0253-0269', null, '办证管理-办证操作-办证协议', null, 'admin/userProtocol/getProtocol,admin/userProtocol/setProtocol', '253', '3', '', '275', '2', '1', '2023-08-04 09:28:32', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('270', '费用缴纳', '', '费用缴纳', 'paymentRecords', 'Paymentofarrears', '/Libraryconsole/Paymentofarrears', null, '1', '0701-0270', null, '费用缴纳-费用缴纳', null, 'admin/oweProcess/list', '701', '2', '', '278', '2', '1', '2023-08-04 09:30:24', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('271', '欠费缴纳记录-积分抵扣设置', '', null, null, null, null, null, '3', '0252-0253-0270-0271', '积分抵扣设置', '办证管理-办证操作-欠费缴纳记录-欠费缴纳记录-积分抵扣设置', null, 'admin/oweProcess/subscriptionRatioSet', '270', '4', '', '279', '2', '1', '2023-08-04 09:32:42', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('272', '办证参数配置', '', '办证参数配置', null, 'configurationParameter', '/certificateHandling/configurationParameter', null, '1', '0252-0253-0272', null, '办证管理-办证操作-办证参数配置', null, 'admin/onlineRegistrationConfig/getOnlineRegistrationConfigParam,admin/onlineRegistrationConfig/setOnlineRegistrationConfigParam', '253', '3', '', '276', '2', '1', '2023-08-04 09:53:22', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('273', '积分商城', '', '积分商城', 'mall', null, null, null, '1', '0273', null, '积分商城', null, '', '0', '1', '', '280', '2', '1', '2023-08-04 10:03:11', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('274', '商品列表', '', '商品列表', 'product', 'ProductList', '/IntegralShopping/productList', null, '1', '0273-0274', null, '积分商城-商品列表', null, 'admin/goods/list,admin/goodsType/filterList,admin/goods/getGoodsPickAddress', '273', '2', '', '281', '2', '1', '2023-08-04 10:04:36', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('275', '商品列表-添加商品', '', '添加商品', null, 'AddCommodity', '/IntegralShopping/productList/add', null, '2', '0273-0274-0275', '添加商品', '积分商城-商品列表-添加商品', null, 'admin/goods/add', '274', '3', '', '282', '2', '1', '2023-08-04 10:20:23', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('276', '商品列表-设置自提地址', '', null, null, null, null, null, '3', '0273-0274-0276', '设置自提地址', '积分商城-商品列表-商品列表-设置自提地址', null, 'admin/goods/setGoodsPickAddress', '274', '3', '', '283', '2', '1', '2023-08-04 10:20:54', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('277', '商品列表-全部上架', '', null, null, null, null, null, '3', '0273-0274-0277', '全部上架', '积分商城-商品列表-商品列表-全部上架', null, 'admin/goods/goodsOnAndOff', '274', '3', '', '284', '2', '1', '2023-08-04 10:21:34', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('278', '商品列表-全部下架', '', null, null, null, null, null, '3', '0273-0274-0278', '全部下架', '积分商城-商品列表-商品列表-全部下架', null, 'admin/goods/goodsOnAndOff', '274', '3', '', '285', '2', '1', '2023-08-04 10:22:11', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('279', '商品列表-批量上架', '', null, null, null, null, null, '3', '0273-0274-0279', '批量上架', '积分商城-商品列表-商品列表-批量上架', null, 'admin/goods/goodsOnAndOff', '274', '3', '', '286', '2', '1', '2023-08-04 10:22:40', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('280', '商品列表-批量下架', '', null, null, null, null, null, '3', '0273-0274-0280', '批量下架', '积分商城-商品列表-商品列表-批量下架', null, 'admin/goods/goodsOnAndOff', '274', '3', '', '287', '2', '1', '2023-08-04 10:23:02', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('281', '商品列表-详情', '', null, null, null, null, null, '3', '0273-0274-0281', '详情', '积分商城-商品列表-商品列表-详情', null, 'admin/goods/detail', '274', '3', '', '288', '2', '1', '2023-08-04 10:23:54', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('282', '商品列表-修改商品', '', '修改商品', null, 'ChangCommodity', '/IntegralShopping/productList/chang', null, '2', '0273-0274-0282', '修改', '积分商城-商品列表-修改商品', null, 'admin/goods/detail,admin/goods/change', '274', '3', '', '289', '2', '1', '2023-08-04 10:25:31', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('283', '商品列表-删除', '', null, null, null, null, null, '3', '0273-0274-0283', '删除', '积分商城-商品列表-商品列表-删除', null, 'admin/goods/del', '274', '3', '', '290', '2', '1', '2023-08-04 10:26:07', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('284', '商品类型', '', '商品类型', 'type_1', 'CommodityType', '/IntegralShopping/commodityType', null, '1', '0273-0284', null, '积分商城-商品类型', null, 'admin/goodsType/list', '273', '2', '', '291', '2', '1', '2023-08-04 10:29:29', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('285', '商品类型-添加类别', '', null, null, null, null, null, '3', '0273-0284-0285', '添加类别', '积分商城-商品类型-商品类型-添加类别', null, 'admin/goodsType/add', '284', '3', '', '292', '2', '1', '2023-08-04 10:41:04', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('286', '商品类型-修改类别', '', null, null, null, null, null, '3', '0273-0284-0286', '修改', '积分商城-商品类型-商品类型-修改类别', null, 'admin/goodsType/change', '284', '3', '', '293', '2', '1', '2023-08-04 10:41:40', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('287', '商品类型-删除', '', null, null, null, null, null, '3', '0273-0284-0287', '删除', '积分商城-商品类型-商品类型-删除', null, 'admin/goodsType/del', '284', '3', '', '294', '2', '1', '2023-08-04 10:42:00', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('288', '兑换记录', '', '兑换记录', 'change', 'ForRecord', '/IntegralShopping/forRecord', null, '1', '0273-0288', null, '积分商城-兑换记录', null, 'admin/goodsType/filterList,admin/goodsRecord/list', '273', '2', '', '295', '2', '1', '2023-08-04 10:42:56', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('289', '兑换记录-发货', '', null, null, null, null, null, '3', '0273-0288-0289', '发货', '积分商城-兑换记录-兑换记录-发货', null, 'admin/goodsRecord/send', '288', '3', '', '296', '2', '1', '2023-08-04 14:20:53', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('290', '兑换记录-待领取', '', null, null, null, null, null, '3', '0273-0288-0290', '待领取', '积分商城-兑换记录-兑换记录-待领取', null, 'admin/goodsRecord/fetch', '288', '3', '', '297', '2', '1', '2023-08-04 14:21:15', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('291', '数字悦读', '', '数字悦读', 'resource_2', null, null, null, '1', '0291', null, '数字悦读', null, '', '0', '1', '', '298', '2', '1', '2023-08-04 14:24:05', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('292', '悦读列表', '', '悦读列表', 'yueduliebiao', 'FigureList', '/digitalResource/figureList', null, '1', '0291-0292', null, '数字悦读-悦读列表', null, 'admin/digital/list,admin/branchInfo/filterList', '291', '2', '', '299', '2', '1', '2023-08-04 14:34:31', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('293', '悦读列表-添加资源', '', null, null, null, null, null, '3', '0291-0292-0293', '添加资源', '数字悦读-悦读列表-悦读列表-添加资源', null, 'admin/digital/add', '292', '3', '', '300', '2', '1', '2023-08-04 14:35:39', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('294', '悦读列表-资源排序', '', null, null, null, null, null, '3', '0291-0292-0294', '资源排序', '数字悦读-悦读列表-悦读列表-资源排序', null, 'admin/digital/sortChange', '292', '3', '', '301', '2', '1', '2023-08-04 14:36:38', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('295', '悦读列表-撤回', '', null, null, null, null, null, '3', '0291-0292-0295', '撤回', '数字悦读-悦读列表-悦读列表-撤回', null, 'admin/digital/cancelAndRelease', '292', '3', '', '302', '2', '1', '2023-08-04 14:37:10', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('296', '悦读列表-发布', '', null, null, null, null, null, '3', '0291-0292-0296', '发布', '数字悦读-悦读列表-悦读列表-发布', null, 'admin/digital/cancelAndRelease', '292', '3', '', '303', '2', '1', '2023-08-04 14:37:19', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('297', '悦读列表-修改', '', null, null, null, null, null, '3', '0291-0292-0297', '修改', '数字悦读-悦读列表-悦读列表-修改', null, 'admin/digital/change', '292', '3', '', '304', '2', '1', '2023-08-04 14:37:40', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('298', '悦读列表-删除', '', null, null, null, null, null, '3', '0291-0292-0298', '删除', '数字悦读-悦读列表-悦读列表-删除', null, 'admin/digital/del', '292', '3', '', '305', '2', '1', '2023-08-04 14:37:59', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('299', '悦读类型', '', '悦读类型', 'yueduneixin', 'FigureType', '/digitalResource/figureType', null, '1', '0291-0299', null, '数字悦读-悦读类型', null, 'admin/digitalType/list', '291', '2', '', '306', '2', '1', '2023-08-04 14:39:32', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('300', '悦读类型-添加类型', '', null, null, null, null, null, '3', '0291-0299-0300', '添加类型', '数字悦读-悦读类型-悦读类型-添加类型', null, 'admin/digitalType/add', '299', '3', '', '307', '2', '1', '2023-08-04 14:40:39', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('301', '悦读类型-修改', '', null, null, null, null, null, '3', '0291-0299-0301', '修改', '数字悦读-悦读类型-悦读类型-修改', null, 'admin/digitalType/change', '299', '3', '', '308', '2', '1', '2023-08-04 14:40:55', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('302', '悦读类型-删除', '', null, null, null, null, null, '3', '0291-0299-0302', '删除', '数字悦读-悦读类型-悦读类型-删除', null, 'admin/digitalType/del', '299', '3', '', '309', '2', '1', '2023-08-04 14:41:11', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('303', '场馆预约', '', '场馆预约', 'date', null, null, null, '1', '0303', null, '场馆预约', null, '', '0', '1', '', '310', '2', '1', '2023-08-04 14:42:31', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('304', '预约列表', '', '预约列表', 'appointment', null, null, null, '1', '0303-0304', null, '读者预约-预约列表', null, '', '303', '2', '', '311', '2', '1', '2023-08-04 14:42:53', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('305', '到馆预约', '', '预约列表', null, 'pavilionList', '/ReaderLnteraction/pavilionList', null, '1', '0303-0304-0305', null, '读者预约-预约列表-预约列表', null, 'admin/reservation/list', '304', '3', '', '312', '2', '1', '2023-08-04 14:43:32', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('306', '到馆预约-创建预约', '', '创建预约', null, 'addpavilion', '/ReaderLnteraction/addpavilion', null, '2', '0303-0304-0305-0306', '创建预约', '读者预约-预约列表-预约列表-创建预约', null, 'admin/reservation/add,admin/reservation/filterTagList,admin/reservation/getReservationApplyParam', '305', '4', '', '313', '2', '1', '2023-08-04 14:45:43', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('307', '到馆预约-发布', '', null, null, null, null, null, '3', '0303-0304-0305-0307', '发布', '读者预约-预约列表-预约列表-到馆预约-发布', null, 'admin/reservation/playAndCancel', '305', '4', '', '314', '2', '1', '2023-08-04 14:46:27', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('308', '到馆预约-撤销', '', null, null, null, null, null, '3', '0303-0304-0305-0308', '撤销', '读者预约-预约列表-预约列表-到馆预约-撤销', null, 'admin/reservation/playAndCancel', '305', '4', '', '315', '2', '1', '2023-08-04 14:46:42', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('309', '到馆预约-详情', '', null, null, null, null, null, '3', '0303-0304-0305-0309', '详情', '读者预约-预约列表-预约列表-到馆预约-详情', null, 'admin/reservation/detail', '305', '4', '', '316', '2', '1', '2023-08-04 14:47:04', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('310', '到馆预约-修改', '', '修改', null, 'changpavilion', '/ReaderLnteraction/changpavilion', null, '2', '0303-0304-0305-0310', '修改', '读者预约-预约列表-预约列表-修改', null, 'admin/reservation/detail,admin/reservation/getReservationApplyParam,admin/reservation/filterTagList,admin/reservation/change', '305', '4', '', '317', '2', '1', '2023-08-04 14:48:03', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('311', '到馆预约-特殊日期排班表', '', '特殊日期排班表', null, 'pavilionSpecialview', '/ReaderLnteraction/pavilionSpecialview', null, '2', '0303-0304-0305-0311', '特殊日期排班表', '读者预约-预约列表-预约列表-特殊日期排班表', null, 'admin/reservationSpecialSchedule/list', '305', '4', '', '318', '2', '1', '2023-08-04 14:49:13', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('312', '到馆预约-预约人员', '', '预约人员', null, 'pavilionpersonnel', '/ReaderLnteraction/pavilionpersonnel', null, '2', '0303-0304-0305-0312', '预约人员', '读者预约-预约列表-预约列表-预约人员', null, 'admin/reservationApply/list,admin/reservation/detail', '305', '4', '', '322', '2', '1', '2023-08-04 14:50:19', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('313', '到馆预约-删除', '', '预约人员', null, 'pavilionpersonnel', '/ReaderLnteraction/pavilionpersonnel', null, '3', '0303-0304-0305-0313', '删除', '读者预约-预约列表-预约列表-预约人员', null, 'admin/reservation/del', '305', '4', '', '335', '2', '1', '2023-08-04 14:50:48', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('314', '特殊日期排班表-新增时间段', '', null, null, null, null, null, '3', '0303-0304-0305-0311-0314', '新增时间段', '读者预约-预约列表-预约列表-特殊日期排班表-特殊日期排班表-新增时间段', null, 'admin/reservationSpecialSchedule/add', '311', '5', '', '319', '2', '1', '2023-08-04 14:51:58', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('315', '特殊日期排班表-修改', '', null, null, null, null, null, '3', '0303-0304-0305-0311-0315', '修改', '读者预约-预约列表-预约列表-特殊日期排班表-特殊日期排班表-修改', null, 'admin/reservationSpecialSchedule/detail,admin/reservationSpecialSchedule/change', '311', '5', '', '320', '2', '1', '2023-08-04 14:52:54', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('316', '特殊日期排班表-删除', '', null, null, null, null, null, '3', '0303-0304-0305-0311-0316', '删除', '读者预约-预约列表-预约列表-特殊日期排班表-特殊日期排班表-删除', null, 'admin/reservationSpecialSchedule/del', '311', '5', '', '321', '2', '1', '2023-08-04 14:53:11', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('317', '预约人员-取消预约', '', null, null, null, null, null, '3', '0303-0304-0305-0312-0317', '取消预约', '读者预约-预约列表-预约列表-预约人员-预约人员-取消预约', null, 'admin/reservationApply/applyCancel', '312', '5', '', '323', '2', '1', '2023-08-04 14:56:24', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('318', '预约人员-导出列表', '', null, null, null, null, null, '3', '0303-0304-0305-0312-0318', '导出列表', '读者预约-预约列表-预约列表-预约人员-预约人员-导出列表', null, 'admin/reservationApplyExport/index', '312', '5', '', '324', '2', '1', '2023-08-04 14:56:43', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('319', '预约人员-同意', '', null, null, null, null, null, '3', '0303-0304-0305-0312-0319', '同意', '读者预约-预约列表-预约列表-预约人员-预约人员-同意', null, 'admin/reservationApply/agreeAndRefused', '312', '5', '', '325', '2', '1', '2023-08-04 14:57:11', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('320', '预约人员-拒绝', '', null, null, null, null, null, '3', '0303-0304-0305-0312-0320', '拒绝', '读者预约-预约列表-预约列表-预约人员-预约人员-拒绝', null, 'admin/reservationApply/agreeAndRefused', '312', '5', '', '326', '2', '1', '2023-08-04 14:57:54', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('321', '预约人员-预约信息', '', null, null, null, null, null, '3', '0303-0304-0305-0312-0321', '预约信息', '读者预约-预约列表-预约列表-预约人员-预约人员-预约信息', null, 'admin/reservationApply/list', '312', '5', '', '327', '2', '1', '2023-08-04 14:58:57', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('322', '预约人员-用户违规', '', null, null, null, null, null, '3', '0303-0304-0305-0312-0322', '用户违规', '读者预约-预约列表-预约列表-预约人员-预约人员-用户违规', null, 'admin/reservationApply/violateAndCancel', '312', '5', '', '328', '2', '1', '2023-08-04 14:59:16', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('323', '预约人员-取消违规', '', null, null, null, null, null, '3', '0303-0304-0305-0312-0323', '取消违规', '读者预约-预约列表-预约列表-预约人员-预约人员-取消违规', null, 'admin/reservationApply/violateAndCancel', '312', '5', '', '329', '2', '1', '2023-08-04 14:59:34', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('324', '座位预约', '', '座位预约', null, 'seatList', '/ReaderLnteraction/seatList', null, '1', '0303-0304-0324', null, '读者预约-预约列表-座位预约', null, 'admin/reservation/list', '304', '3', '', '336', '2', '1', '2023-08-04 15:03:11', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('325', '座位预约-创建预约', '', '创建预约', null, 'addseat', '/ReaderLnteraction/seatList/addseat', null, '2', '0303-0304-0324-0325', '创建预约', '读者预约-预约列表-座位预约-创建预约', null, 'admin/reservation/filterTagList,admin/reservation/getReservationApplyParam,admin/reservation/add', '324', '4', '', '337', '2', '1', '2023-08-04 15:05:22', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('326', '座位预约-发布', '', '创建预约', null, 'addseat', '/ReaderLnteraction/seatList/addseat', null, '3', '0303-0304-0324-0326', '发布', '读者预约-预约列表-座位预约-创建预约', null, 'admin/reservation/playAndCancel', '324', '4', '', '338', '2', '1', '2023-08-04 15:09:15', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('327', '座位预约-撤销', '', '创建预约', null, 'addseat', '/ReaderLnteraction/seatList/addseat', null, '3', '0303-0304-0324-0327', '撤销', '读者预约-预约列表-座位预约-创建预约', null, 'admin/reservation/playAndCancel', '324', '4', '', '339', '2', '1', '2023-08-04 15:09:24', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('328', '座位预约-详情', '', '创建预约', null, 'addseat', '/ReaderLnteraction/seatList/addseat', null, '3', '0303-0304-0324-0328', '详情', '读者预约-预约列表-座位预约-创建预约', null, 'admin/reservation/detail', '324', '4', '', '340', '2', '1', '2023-08-04 15:09:47', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('329', '座位预约-修改', '', '修改', null, 'changseat', '/ReaderLnteraction/seatList/changseat', null, '2', '0303-0304-0324-0329', '修改', '读者预约-预约列表-座位预约-修改', null, 'admin/reservation/detail,admin/reservation/filterTagList,admin/reservation/getReservationApplyParam,admin/reservation/change', '324', '4', '', '341', '2', '1', '2023-08-04 15:11:03', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('330', '座位预约-特殊日期排班表', '', '特殊日期排班表', null, 'seatListSpecialview', '/ReaderLnteraction/seatListSpecialview', null, '2', '0303-0304-0324-0330', '特殊日期排班表', '读者预约-预约列表-座位预约-特殊日期排班表', null, 'admin/reservationSpecialSchedule/list', '324', '4', '', '342', '2', '1', '2023-08-04 15:13:04', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('331', '座位预约-预约人员', '', '预约人员', null, 'seatListpersonnel', '/ReaderLnteraction/seatList/personnel', null, '2', '0303-0304-0324-0331', '预约人员', '读者预约-预约列表-座位预约-预约人员', null, 'admin/reservationApply/list,admin/reservation/detail', '324', '4', '', '346', '2', '1', '2023-08-04 15:13:53', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('332', '座位预约-座位总览', '', '座位总览', null, 'seatListseat', '/ReaderLnteraction/seatList/seat', null, '2', '0303-0304-0324-0332', '座位总览', '读者预约-预约列表-座位预约-座位总览', null, 'admin/reservationSeat/scheduleList,admin/reservationSeat/list,admin/reservation/detail', '324', '4', '', '358', '2', '1', '2023-08-04 15:15:08', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('333', '座位预约-删除', '', '座位总览', null, 'seatListseat', '/ReaderLnteraction/seatList/seat', null, '3', '0303-0304-0324-0333', '删除', '读者预约-预约列表-座位预约-座位总览', null, 'admin/reservation/del', '324', '4', '', '363', '2', '1', '2023-08-04 15:15:28', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('334', '座位特殊日期排班表-新增时间段', '', null, null, null, null, null, '3', '0303-0304-0324-0330-0334', '新增时间段', '读者预约-预约列表-座位预约-特殊日期排班表-座位特殊日期排班表-新增时间段', null, 'admin/reservationSpecialSchedule/add', '330', '5', '', '343', '2', '1', '2023-08-04 15:16:33', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('335', '座位特殊日期排班表-修改', '', null, null, null, null, null, '3', '0303-0304-0324-0330-0335', '修改', '读者预约-预约列表-座位预约-特殊日期排班表-座位特殊日期排班表-修改', null, 'admin/reservationSpecialSchedule/detail,admin/reservationSpecialSchedule/change', '330', '5', '', '344', '2', '1', '2023-08-04 15:17:23', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('336', '座位特殊日期排班表-删除', '', null, null, null, null, null, '3', '0303-0304-0324-0330-0336', '删除', '读者预约-预约列表-座位预约-特殊日期排班表-座位特殊日期排班表-删除', null, 'admin/reservationSpecialSchedule/del', '330', '5', '', '345', '2', '1', '2023-08-04 15:17:44', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('337', '座位预约人员-取消预约', '', null, null, null, null, null, '3', '0303-0304-0324-0331-0337', '取消预约', '读者预约-预约列表-座位预约-预约人员-座位预约人员-取消预约', null, 'admin/reservationApply/applyCancel', '331', '5', '', '347', '2', '1', '2023-08-04 15:29:33', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('338', '座位预约人员-导出列表', '', null, null, null, null, null, '3', '0303-0304-0324-0331-0338', '导出列表', '读者预约-预约列表-座位预约-预约人员-座位预约人员-导出列表', null, 'admin/reservationApplyExport/index', '331', '5', '', '348', '2', '1', '2023-08-04 15:29:48', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('339', '座位预约人员-同意', '', null, null, null, null, null, '3', '0303-0304-0324-0331-0339', '同意', '读者预约-预约列表-座位预约-预约人员-座位预约人员-同意', null, 'admin/reservationApply/agreeAndRefused', '331', '5', '', '349', '2', '1', '2023-08-04 15:33:15', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('340', '座位预约人员-拒绝', '', null, null, null, null, null, '3', '0303-0304-0324-0331-0340', '拒绝', '读者预约-预约列表-座位预约-预约人员-座位预约人员-拒绝', null, 'admin/reservationApply/agreeAndRefused', '331', '5', '', '350', '2', '1', '2023-08-04 15:33:35', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('341', '座位预约人员-预约信息', '', null, null, null, null, null, '3', '0303-0304-0324-0331-0341', '预约信息', '读者预约-预约列表-座位预约-预约人员-座位预约人员-预约信息', null, 'admin/reservationApply/list', '331', '5', '', '351', '2', '1', '2023-08-04 15:34:09', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('342', '座位预约人员-签到打卡', '', null, null, null, null, null, '3', '0303-0304-0324-0331-0342', '签到打卡', '读者预约-预约列表-座位预约-预约人员-座位预约人员-签到打卡', null, 'admin/reservationApply/applySignIn', '331', '5', '', '352', '2', '1', '2023-08-04 15:34:33', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('343', '座位预约人员-签退打卡', '', null, null, null, null, null, '3', '0303-0304-0324-0331-0343', '签退打卡', '读者预约-预约列表-座位预约-预约人员-座位预约人员-签退打卡', null, 'admin/reservationApply/applySignOut', '331', '5', '', '353', '2', '1', '2023-08-04 15:35:02', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('344', '座位预约人员-用户违规', '', null, null, null, null, null, '3', '0303-0304-0324-0331-0344', '用户违规', '读者预约-预约列表-座位预约-预约人员-座位预约人员-用户违规', null, 'admin/reservationApply/violateAndCancel', '331', '5', '', '354', '2', '1', '2023-08-04 15:35:30', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('345', '座位预约人员-取消违规', '', null, null, null, null, null, '3', '0303-0304-0324-0331-0345', '取消违规', '读者预约-预约列表-座位预约-预约人员-座位预约人员-取消违规', null, 'admin/reservationApply/violateAndCancel', '331', '5', '', '355', '2', '1', '2023-08-04 15:35:58', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('346', '座位总览-新增座位', '', null, null, null, null, null, '3', '0303-0304-0324-0332-0346', '新增座位', '读者预约-预约列表-座位预约-座位总览-座位总览-新增座位', null, 'admin/reservationSeat/add', '332', '5', '', '359', '2', '1', '2023-08-04 15:39:51', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('347', '座位总览-撤销座位', '', null, null, null, null, null, '3', '0303-0304-0324-0332-0347', '撤销座位', '读者预约-预约列表-座位预约-座位总览-座位总览-撤销座位', null, 'admin/reservationSeat/playAndCancel', '332', '5', '', '360', '2', '1', '2023-08-04 15:40:18', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('348', '座位总览-启用座位', '', null, null, null, null, null, '3', '0303-0304-0324-0332-0348', '启用座位', '读者预约-预约列表-座位预约-座位总览-座位总览-启用座位', null, 'admin/reservationSeat/playAndCancel', '332', '5', '', '361', '2', '1', '2023-08-04 15:40:30', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('349', '座位总览-删除座位', '', null, null, null, null, null, '3', '0303-0304-0324-0332-0349', '删除座位', '读者预约-预约列表-座位预约-座位总览-座位总览-删除座位', null, 'admin/reservationSeat/del', '332', '5', '', '362', '2', '1', '2023-08-04 15:40:52', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('350', '空间预约', '', '空间预约', 'zixishiyuyue', null, null, null, '1', '0350', null, '空间预约', null, '', '0', '1', '', '448', '2', '1', '2023-08-04 15:43:55', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('351', '预约列表', '', '预约列表', 'zixishiliebiao', 'selfstudyList', '/SelfstudyRoom/selfstudyList', null, '1', '0350-0351', null, '空间预约-预约列表', null, 'admin/studyRoomReservation/list', '350', '2', '', '449', '2', '1', '2023-08-04 15:45:10', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('352', '空间预约列表-创建预约', '', '创建预约', null, 'addselfstudy', '/SelfstudyRoom/selfstudyList/addselfstudy', null, '2', '0350-0351-0352', '创建预约', '空间预约-预约列表-创建预约', null, 'admin/studyRoomReservation/getReservationApplyParam,admin/studyRoomReservation/filterTagList,admin/studyRoomReservation/add', '351', '3', '', '450', '2', '1', '2023-08-04 15:47:56', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('353', '空间预约列表-发布', '', '创建预约', null, 'addselfstudy', '/SelfstudyRoom/selfstudyList/addselfstudy', null, '3', '0350-0351-0353', '发布', '自习室预约-预约列表-创建预约', null, 'admin/studyRoomReservation/playAndCancel', '351', '3', '', '451', '2', '1', '2023-08-04 15:48:19', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('354', '空间预约列表-撤销', '', '创建预约', null, 'addselfstudy', '/SelfstudyRoom/selfstudyList/addselfstudy', null, '3', '0350-0351-0354', '撤销', '自习室预约-预约列表-创建预约', null, 'admin/studyRoomReservation/playAndCancel', '351', '3', '', '452', '2', '1', '2023-08-04 15:48:28', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('355', '自习室预约列表-详情', '', '创建预约', null, 'addselfstudy', '/SelfstudyRoom/selfstudyList/addselfstudy', null, '3', '0350-0351-0355', '详情', '自习室预约-预约列表-创建预约', null, 'admin/studyRoomReservation/detail', '351', '3', '', '453', '2', '1', '2023-08-04 15:48:49', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('356', '空间预约列表-修改', '', '修改', null, 'changselfstudy', '/SelfstudyRoom/selfstudyList/changselfstudy', null, '2', '0350-0351-0356', '修改', '自习室预约-预约列表-修改', null, 'admin/studyRoomReservation/detail,admin/reservation/getReservationApplyParam,admin/reservation/filterTagList,admin/studyRoomReservation/change', '351', '3', '', '454', '2', '1', '2023-08-04 15:51:43', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('357', '自习室预约列表-远程开门', '', '修改', null, 'changselfstudy', '/SelfstudyRoom/selfstudyList/changselfstudy', null, '3', '0350-0351-0357', '远程开门', '自习室预约-预约列表-修改', null, 'admin/studyRoomReservationRemoteOperation/remoteOperation', '351', '3', '', '455', '2', '1', '2023-08-04 15:52:18', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('358', '空间预约列表-预约人员', '', '预约人员', null, 'selfstudypersonnel', '/SelfstudyRoom/selfstudyList/personnel', null, '2', '0350-0351-0358', '预约人员', '自习室预约-预约列表-预约人员', null, 'admin/studyRoomReservationApply/list,admin/studyRoomReservation/detail', '351', '3', '', '456', '2', '1', '2023-08-04 15:53:13', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('359', '自习室预约列表-删除', '', '预约人员', null, 'selfstudypersonnel', '/SelfstudyRoom/selfstudyList/personnel', null, '3', '0350-0351-0359', '删除', '自习室预约-预约列表-预约人员', null, 'admin/studyRoomReservation/del', '351', '3', '', '466', '2', '1', '2023-08-04 15:54:17', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('360', '自习室预约人员-取消预约', '', null, null, null, null, null, '3', '0350-0351-0358-0360', '取消预约', '自习室预约-预约列表-预约人员-自习室预约人员-取消预约', null, 'admin/studyRoomReservationApply/applyCancel', '358', '4', '', '457', '2', '1', '2023-08-04 15:56:05', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('361', '自习室预约人员-导出列表', '', null, null, null, null, null, '3', '0350-0351-0358-0361', '导出列表', '自习室预约-预约列表-预约人员-自习室预约人员-导出列表', null, 'admin/studyRoomReservationApplyExport/index', '358', '4', '', '458', '2', '1', '2023-08-04 15:56:22', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('362', '自习室预约人员-同意', '', null, null, null, null, null, '3', '0350-0351-0358-0362', '同意', '自习室预约-预约列表-预约人员-自习室预约人员-同意', null, 'admin/studyRoomReservationApply/agreeAndRefused', '358', '4', '', '459', '2', '1', '2023-08-04 15:56:44', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('363', '自习室预约人员-拒绝', '', null, null, null, null, null, '3', '0350-0351-0358-0363', '拒绝', '自习室预约-预约列表-预约人员-自习室预约人员-拒绝', null, 'admin/studyRoomReservationApply/agreeAndRefused', '358', '4', '', '460', '2', '1', '2023-08-04 15:57:02', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('364', '自习室预约人员-预约信息', '', null, null, null, null, null, '3', '0350-0351-0358-0364', '预约信息', '自习室预约-预约列表-预约人员-自习室预约人员-预约信息', null, 'admin/studyRoomReservationApply/list', '358', '4', '', '461', '2', '1', '2023-08-04 15:57:27', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('365', '自习室预约人员-用户违规', '', null, null, null, null, null, '3', '0350-0351-0358-0365', '用户违规', '自习室预约-预约列表-预约人员-自习室预约人员-用户违规', null, 'admin/studyRoomReservationApply/violateAndCancel', '358', '4', '', '462', '2', '1', '2023-08-04 15:57:49', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('366', '自习室预约人员-取消违规', '', null, null, null, null, null, '3', '0350-0351-0358-0366', '取消违规', '自习室预约-预约列表-预约人员-自习室预约人员-取消违规', null, 'admin/studyRoomReservationApply/violateAndCancel', '358', '4', '', '463', '2', '1', '2023-08-04 15:58:07', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('367', '白名单列表', '', '白名单列表', 'baimingdan', 'whiteList', '/SelfstudyRoom/whiteList', null, '1', '0350-0367', null, '自习室预约-白名单列表', null, 'admin/studyRoomReservationWhite/list,admin/studyRoomReservation/getFilterList', '350', '2', '', '467', '2', '1', '2023-08-04 15:59:37', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('368', '白名单列表-添加白名单', '', null, null, null, null, null, '3', '0350-0367-0368', '添加白名单', '自习室预约-白名单列表-白名单列表-添加白名单', null, 'admin/studyRoomReservationWhite/add', '367', '3', '', '468', '2', '1', '2023-08-04 16:00:41', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('369', '白名单列表-下载白名单', '', null, null, null, null, null, '3', '0350-0367-0369', '下载白名单', '自习室预约-白名单列表-白名单列表-下载白名单', null, 'admin/studyRoomReservationRemoteOperation/remoteOperation', '367', '3', '', '469', '2', '1', '2023-08-04 16:01:25', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('370', '白名单列表-清空白名单', '', null, null, null, null, null, '3', '0350-0367-0370', '清空白名单', '自习室预约-白名单列表-白名单列表-清空白名单', null, 'admin/studyRoomReservationRemoteOperation/remoteOperation', '367', '3', '', '470', '2', '1', '2023-08-04 16:01:35', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('371', '白名单列表-出示二维码', '', null, null, null, null, null, '3', '0350-0367-0371', '出示二维码', '自习室预约-白名单列表-白名单列表-出示二维码', null, 'admin/studyRoomReservationWhite/getApplyQr', '367', '3', '', '471', '2', '1', '2023-08-04 16:02:30', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('372', '白名单列表-修改', '', null, null, null, null, null, '3', '0350-0367-0372', '修改', '自习室预约-白名单列表-白名单列表-修改', null, 'admin/studyRoomReservationWhite/change', '367', '3', '', '472', '2', '1', '2023-08-04 16:02:49', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('373', '白名单列表-删除', '', null, null, null, null, null, '3', '0350-0367-0373', '删除', '自习室预约-白名单列表-白名单列表-删除', null, 'admin/studyRoomReservationWhite/del', '367', '3', '', '473', '2', '1', '2023-08-04 16:03:10', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('374', '远程操作列表', '', '远程操作列表', 'yuanchengcaozuo', 'remoteList', '/SelfstudyRoom/remoteList', null, '1', '0350-0374', null, '自习室预约-远程操作列表', null, 'admin/studyRoomReservationRemoteOperation/lis', '350', '2', '', '474', '2', '1', '2023-08-04 16:04:17', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('375', '图书到家', '', '图书到家', 'tushudaojia', null, null, null, '1', '0375', null, '图书到家', null, '', '0', '1', '', '475', '2', '1', '2023-08-04 16:21:22', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('376', '图书馆操作台', '', '图书馆操作台', 'tushucaozuotai', null, null, null, '1', '0375-0376', null, '图书到家-图书馆操作台', null, '', '375', '2', '', '476', '2', '1', '2023-08-04 16:21:41', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('377', '馆藏书业务清单', '', '馆藏书业务清单', null, 'Librarycollection', '/Libraryconsole/Librarycollection', null, '1', '0375-0376-0377', null, '图书到家-图书馆操作台-馆藏书业务清单', null, 'admin/bookHomeLibBook/getOrderNumber', '376', '3', '', '477', '2', '1', '2023-08-04 16:28:15', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('378', '馆藏书业务清单-申请列表', '', '申请列表', null, 'Listviewapply', 'Librarycollection/Listview/apply', null, '2', '0375-0376-0377-0378', null, '图书到家-图书馆操作台-馆藏书业务清单-申请列表', null, 'admin/bookHomeLibBook/orderList', '377', '4', '', '478', '2', '1', '2023-08-04 16:30:02', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('379', '馆藏书业务清单-申请列表-一键同意', '', null, null, null, null, null, '3', '0375-0376-0377-0378-0379', '一键同意', '图书到家-图书馆操作台-馆藏书业务清单-申请列表-馆藏书业务清单-申请列表-一键同意', null, 'admin/bookHomeLibBook/disposeOrderApply', '378', '5', '', '479', '2', '1', '2023-08-04 16:33:14', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('380', '馆藏书业务清单-申请列表-一键拒绝', '', null, null, null, null, null, '3', '0375-0376-0377-0378-0380', '一键拒绝', '图书到家-图书馆操作台-馆藏书业务清单-申请列表-馆藏书业务清单-申请列表-一键拒绝', null, 'admin/bookHomeLibBook/disposeOrderApply', '378', '5', '', '480', '2', '1', '2023-08-04 16:38:28', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('381', '馆藏书业务清单-申请列表-导出书单', '', null, null, null, null, null, '3', '0375-0376-0377-0378-0381', '导出书单', '图书到家-图书馆操作台-馆藏书业务清单-申请列表-馆藏书业务清单-申请列表-导出书单', null, 'admin/bookHomeExport/orderLibraryList', '378', '5', '', '481', '2', '1', '2023-08-04 16:38:45', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('382', '馆藏书业务清单-申请列表-同意', '', null, null, null, null, null, '3', '0375-0376-0377-0378-0382', '同意', '图书到家-图书馆操作台-馆藏书业务清单-申请列表-馆藏书业务清单-申请列表-同意', null, 'admin/bookHomeLibBook/disposeOrderApply', '378', '5', '', '482', '2', '1', '2023-08-04 16:45:42', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('383', '馆藏书业务清单-申请列表-拒绝', '', null, null, null, null, null, '3', '0375-0376-0377-0378-0383', '拒绝', '图书到家-图书馆操作台-馆藏书业务清单-申请列表-馆藏书业务清单-申请列表-拒绝', null, 'admin/bookHomeLibBook/disposeOrderApply', '378', '5', '', '483', '2', '1', '2023-08-04 16:46:09', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('384', '馆藏书业务清单-申请列表-查看详情', '', '详情', null, 'libraryDetails', '/Librarycollection/libraryDetails', null, '2', '0375-0376-0377-0378-0384', '查看详情', '图书到家-图书馆操作台-馆藏书业务清单-申请列表-详情', null, 'admin/bookHomeLibBook/orderDetail', '378', '5', '', '484', '2', '1', '2023-08-04 16:47:33', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('385', '馆藏书业务清单-发货列表', '', '发货列表', null, 'Listviewsend', '/Librarycollection/Listview/send', null, '2', '0375-0376-0377-0385', null, '图书到家-图书馆操作台-馆藏书业务清单-发货列表', null, 'admin/bookHomeLibBook/orderList', '377', '4', '', '485', '2', '1', '2023-08-04 16:51:32', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('386', '馆藏书业务清单-发货列表-查看详情', '', '详情', null, 'libraryDetails', '/Librarycollection/libraryDetails', null, '2', '0375-0376-0377-0385-0386', null, '图书到家-图书馆操作台-馆藏书业务清单-发货列表-详情', null, 'admin/bookHomeLibBook/orderList', '385', '5', '', '486', '2', '1', '2023-08-04 16:52:24', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('387', '馆藏书业务清单-发货列表-导出书单', '', '导出书单', null, null, null, null, '3', '0375-0376-0377-0385-0387', '导出书单', '图书到家-图书馆操作台-馆藏书业务清单-发货列表-导出书单', null, 'admin/bookHomeExport/orderLibraryList', '385', '5', '', '487', '2', '1', '2023-08-04 16:52:54', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('388', '馆藏书业务清单-发货列表-发货', '', '导出书单', null, null, null, null, '3', '0375-0376-0377-0385-0388', '发货', '图书到家-图书馆操作台-馆藏书业务清单-发货列表-导出书单', null, 'admin/bookHomeLibBook/disposeOrderDeliver', '385', '5', '', '488', '2', '1', '2023-08-04 16:54:26', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('389', '馆藏书业务清单-发货列表-无法发货', '', '导出书单', null, null, null, null, '3', '0375-0376-0377-0385-0389', '无法发货', '图书到家-图书馆操作台-馆藏书业务清单-发货列表-导出书单', null, 'admin/bookHomeLibBook/disposeOrderDeliver', '385', '5', '', '489', '2', '1', '2023-08-04 16:54:59', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('390', '馆藏书业务清单-所有订单', '', '所有订单', null, 'Listviewall', '/Librarycollection/Listview/all', null, '2', '0375-0376-0377-0390', null, '图书到家-图书馆操作台-馆藏书业务清单-所有订单', null, 'admin/bookHomeLibBook/orderList', '377', '4', '', '490', '2', '1', '2023-08-04 16:55:49', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('391', '馆藏书业务清单-所有列表-查看详情', '', '查看详情', null, 'libraryDetails', '/Librarycollection/libraryDetails', null, '2', '0375-0376-0377-0390-0391', '查看详情', '图书到家-图书馆操作台-馆藏书业务清单-所有订单-查看详情', null, 'admin/bookHomeLibBook/orderList', '390', '5', '', '491', '2', '1', '2023-08-04 16:57:45', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('392', '馆藏书业务清单-所有列表-导出书单', '', null, null, null, null, null, '3', '0375-0376-0377-0390-0392', '导出书单', '图书到家-图书馆操作台-馆藏书业务清单-所有订单-馆藏书业务清单-所有列表-导出书单', null, 'admin/bookHomeExport/orderLibraryList', '390', '5', '', '492', '2', '1', '2023-08-04 17:00:31', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('393', '线上借阅历史', '', '线上借阅历史', null, 'Onlinehistory', '/Libraryconsole/Onlinehistory', null, '1', '0375-0376-0393', null, '图书到家-图书馆操作台-线上借阅历史', null, 'admin/bookHomeNewBookLib/purchaseLibList,admin/bookHomeNewBookLib/purchaseShopList', '376', '3', '', '493', '2', '1', '2023-08-04 17:02:58', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('394', '线上借阅历史-新书导出书单', '', null, null, null, null, null, '3', '0375-0376-0393-0394', '导出书单', '图书到家-图书馆操作台-线上借阅历史-线上借阅历史-新书导出书单', null, 'admin/bookHomeExport/libPurchaseLibList', '393', '4', '', '494', '2', '1', '2023-08-04 17:03:46', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('395', '线上借阅历史-馆藏书导出书单', '', null, null, null, null, null, '3', '0375-0376-0393-0395', '导出书单', '图书到家-图书馆操作台-线上借阅历史-线上借阅历史-馆藏书导出书单', null, 'admin/bookHomeExport/purchaseLibList', '393', '4', '', '495', '2', '1', '2023-08-04 17:04:12', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('396', '线下借阅历史', '', '线下借阅历史', null, 'Offlinehistory', '/Libraryconsole/Offlinehistory', null, '1', '0375-0376-0396', null, '图书到家-图书馆操作台-线下借阅历史', null, 'admin/shop/filterList,admin/manage/filterList,admin/bookHomeOfflineRecom/offlineListLib', '376', '3', '', '496', '2', '1', '2023-08-04 17:06:07', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('397', '线下借阅历史-导出书单', '', null, null, null, null, null, '3', '0375-0376-0396-0397', '导出书单', '图书到家-图书馆操作台-线下借阅历史-线下借阅历史-导出书单', null, 'admin/bookHomeExport/offlineListShop', '396', '4', '', '497', '2', '1', '2023-08-04 17:12:48', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('398', '新书结算清单', '', '新书结算清单', null, 'Settlementlist', '/Libraryconsole/Settlementlist', null, '1', '0375-0376-0398', null, '图书到家-图书馆操作台-新书结算清单', null, 'admin/shop/filterList3,admin/bookHomeNewBookLib/purchaseShopList,admin/bookHomeNewBookLib/getPurchaseNumber', '376', '3', '', '498', '2', '1', '2023-08-04 17:14:24', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('399', '图书馆操作台-新书结算清单-全部结算', '', null, null, null, null, null, '3', '0375-0376-0398-0399', '全部结算', '图书到家-图书馆操作台-新书结算清单-图书馆操作台-新书结算清单-全部结算', null, 'admin/bookHomeNewBookLib/purchaseStateModify', '398', '4', '', '499', '2', '1', '2023-08-04 17:15:16', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('400', '图书馆操作台-新书结算清单-导出书单', '', null, null, null, null, null, '3', '0375-0376-0398-0400', '导出书单', '图书到家-图书馆操作台-新书结算清单-图书馆操作台-新书结算清单-导出书单', null, 'admin/bookHomeExport/purchaseShopList', '398', '4', '', '500', '2', '1', '2023-08-04 17:15:34', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('401', '图书馆操作台-新书结算清单-确认结算', '', null, null, null, null, null, '3', '0375-0376-0398-0401', '确认结算', '图书到家-图书馆操作台-新书结算清单-图书馆操作台-新书结算清单-确认结算', null, 'admin/bookHomeNewBookLib/purchaseStateModify', '398', '4', '', '501', '2', '1', '2023-08-04 17:16:18', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('402', '书目信息管理', '', '书目信息管理', null, 'Bibliography', '/Libraryconsole/Bibliography', null, '1', '0375-0376-0402', null, '图书到家-图书馆操作台-书目信息管理', null, 'admin/libBookRecomType/filterList,admin/libBookRecom/list', '376', '3', '', '502', '2', '1', '2023-08-04 17:17:30', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('403', '图书馆操作台-书目信息管理-全部上架', '', null, null, null, null, null, '3', '0375-0376-0402-0403', '全部上架', '图书到家-图书馆操作台-书目信息管理-图书馆操作台-书目信息管理-全部上架', null, 'admin/libBookRecom/cancelAndRelease', '402', '4', '', '503', '2', '1', '2023-08-04 17:19:08', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('404', '图书馆操作台-书目信息管理-全部不推荐', '', null, null, null, null, null, '3', '0375-0376-0402-0404', '全部不推荐', '图书到家-图书馆操作台-书目信息管理-图书馆操作台-书目信息管理-全部不推荐', null, 'admin/libBookRecom/cancelAndRelease', '402', '4', '', '504', '2', '1', '2023-08-04 17:19:19', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('405', '图书馆操作台-书目信息管理-类型列表', '', null, null, null, null, null, '3', '0375-0376-0402-0405', '类型列表', '图书到家-图书馆操作台-书目信息管理-图书馆操作台-书目信息管理-类型列表', null, 'admin/libBookRecomType/list', '402', '4', '', '505', '2', '1', '2023-08-04 17:19:42', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('406', '图书馆操作台-书目信息管理-模板导出', '', null, null, null, null, null, '3', '0375-0376-0402-0406', '模板导出', '图书到家-图书馆操作台-书目信息管理-图书馆操作台-书目信息管理-模板导出', null, 'admin/templateExport/libBookRecommend', '402', '4', '', '506', '2', '1', '2023-08-04 17:20:09', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('407', '图书馆操作台-书目信息管理-导入书架', '', null, null, null, null, null, '3', '0375-0376-0402-0407', '导入书架', '图书到家-图书馆操作台-书目信息管理-图书馆操作台-书目信息管理-导入书架', null, 'admin/libBookRecomImport/index', '402', '4', '', '507', '2', '1', '2023-08-04 17:20:34', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('408', '图书馆操作台-书目信息管理-详情', '', null, null, null, null, null, '3', '0375-0376-0402-0408', '详情', '图书到家-图书馆操作台-书目信息管理-图书馆操作台-书目信息管理-详情', null, 'admin/libBookRecom/detail', '402', '4', '', '508', '2', '1', '2023-08-04 17:21:01', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('409', '图书馆操作台-书目信息管理-修改', '', null, null, null, null, null, '3', '0375-0376-0402-0409', '修改', '图书到家-图书馆操作台-书目信息管理-图书馆操作台-书目信息管理-修改', null, 'admin/libBookRecom/change', '402', '4', '', '509', '2', '1', '2023-08-04 17:21:27', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('410', '图书馆操作台-书目信息管理-删除', '', null, null, null, null, null, '3', '0375-0376-0402-0410', '删除', '图书到家-图书馆操作台-书目信息管理-图书馆操作台-书目信息管理-删除', null, 'admin/libBookRecom/del', '402', '4', '', '510', '2', '1', '2023-08-04 17:21:49', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('411', '流通设置', '', '流通设置', null, 'Circulationsettings', '/Libraryconsole/Circulationsettings', null, '1', '0375-0376-0411', null, '图书到家-图书馆操作台-流通设置', null, 'admin/bookHomePurchaseSet/purchaseSetRecord,admin/bookHomePurchaseSet/purchaseBudgetList,admin/bookHomePurchaseSet/postageSet,admin/bookHomePurchaseSet/libDeliverAddress,admin/bookHomePurchaseSet/purchaseSet,admin/oweProcess/subscriptionRatioSet', '376', '3', '', '511', '2', '1', '2023-08-04 17:28:53', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('412', '邮寄还书', '', '邮寄还书', null, 'Mailreturn', '/Libraryconsole/Mailreturn', null, '1', '0375-0376-0412', null, '图书到家-图书馆操作台-邮寄还书', null, 'admin/bookHomeLibBook/postalReturnList', '376', '3', '', '512', '2', '1', '2023-08-04 17:29:31', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('413', '图书馆操作台-邮寄还书-确认还书', '', null, null, null, null, null, '3', '0375-0376-0412-0413', '确认还书', '图书到家-图书馆操作台-邮寄还书-图书馆操作台-邮寄还书-确认还书', null, 'admin/bookHomeLibBook/onlineReturn', '412', '4', '', '513', '2', '1', '2023-08-04 17:30:20', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('414', '图书馆操作台-邮寄还书-还书失败', '', null, null, null, null, null, '3', '0375-0376-0412-0414', '还书失败', '图书到家-图书馆操作台-邮寄还书-图书馆操作台-邮寄还书-还书失败', null, 'admin/bookHomeLibBook/onlineReturn', '412', '4', '', '514', '2', '1', '2023-08-04 17:30:30', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('415', '线下还书', '', '线下还书', null, 'Offlinereturn', '/Libraryconsole/Offlinereturn', null, '1', '0375-0376-0415', null, '图书到家-图书馆操作台-线下还书', null, 'admin/bookHomeLibBook/getBookBorrow,admin/bookHomeLibBook/offlineReturnDirect,admin/bookHomeLibBook/getReaderBorrowList', '376', '3', '', '515', '2', '1', '2023-08-04 17:36:45', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('416', '归还地址管理', '', '归还地址管理', null, 'ReturnAddress', '/Libraryconsole/ReturnAddress', null, '1', '0375-0376-0416', null, '图书到家-图书馆操作台-归还地址管理', null, 'admin/bookHomeReturnAddress/list', '376', '3', '', '516', '2', '1', '2023-08-04 17:37:36', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('417', '图书馆操作台-归还地址管理-添加', '', null, null, null, null, null, '3', '0375-0376-0416-0417', '添加', '图书到家-图书馆操作台-归还地址管理-图书馆操作台-归还地址管理-添加', null, 'admin/bookHomeReturnAddress/add', '416', '4', '', '517', '2', '1', '2023-08-04 17:38:51', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('418', '图书馆操作台-归还地址管理-现场归还说明', '', null, null, null, null, null, '3', '0375-0376-0416-0418', '现场归还说明', '图书到家-图书馆操作台-归还地址管理-图书馆操作台-归还地址管理-现场归还说明', null, 'admin/bookHomeReturnAddress/getAddressExplain,admin/bookHomeReturnAddress/setAddressExplain', '416', '4', '', '518', '2', '1', '2023-08-04 17:39:49', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('419', '图书馆操作台-归还地址管理-邮递归还说明', '', null, null, null, null, null, '3', '0375-0376-0416-0419', '邮递归还说明', '图书到家-图书馆操作台-归还地址管理-图书馆操作台-归还地址管理-邮递归还说明', null, 'admin/bookHomeReturnAddress/getAddressExplain,admin/bookHomeReturnAddress/setAddressExplain', '416', '4', '', '519', '2', '1', '2023-08-04 17:39:59', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('420', '图书馆操作台-归还地址管理-修改', '', null, null, null, null, null, '3', '0375-0376-0416-0420', '修改', '图书到家-图书馆操作台-归还地址管理-图书馆操作台-归还地址管理-修改', null, 'admin/bookHomeReturnAddress/change,admin/bookHomeReturnAddress/detail', '416', '4', '', '520', '2', '1', '2023-08-04 17:40:24', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('421', '图书馆操作台-归还地址管理-删除', '', null, null, null, null, null, '3', '0375-0376-0416-0421', '删除', '图书到家-图书馆操作台-归还地址管理-图书馆操作台-归还地址管理-删除', null, 'admin/bookHomeReturnAddress/del', '416', '4', '', '521', '2', '1', '2023-08-04 17:40:42', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('422', '书店操作台', '', '书店操作台', 'shudiancaozuotai', null, null, null, '1', '0375-0422', null, '图书到家-书店操作台', null, '', '375', '2', '', '522', '2', '1', '2023-08-04 17:41:15', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('423', '线上业务清单', '', '线上业务清单', null, 'OnlineBusinesslist', '/Bookstoreconsole/OnlineBusinesslist', null, '1', '0375-0422-0423', null, '图书到家-书店操作台-线上业务清单', null, 'admin/bookHomeNewBookShop/getOrderNumber', '422', '3', '', '523', '2', '1', '2023-08-04 17:41:54', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('424', '书店操作台-线上业务清单-申请列表', '', '申请列表', null, 'Onlineviewapply', '/OnlineBusinesslist/Listview/apply', null, '2', '0375-0422-0423-0424', null, '图书到家-书店操作台-线上业务清单-申请列表', null, 'admin/bookHomeNewBookShop/orderList', '423', '4', '', '524', '2', '1', '2023-08-04 17:42:42', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('425', '书店操作台-线上业务清单-申请列表-一键同意', '', null, null, null, null, null, '3', '0375-0422-0423-0424-0425', '一键同意', '图书到家-书店操作台-线上业务清单-申请列表-书店操作台-线上业务清单-申请列表-一键同意', null, 'admin/bookHomeNewBookShop/disposeOrderApply', '424', '5', '', '525', '2', '1', '2023-08-04 17:43:35', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('426', '书店操作台-线上业务清单-申请列表-一键拒绝', '', null, null, null, null, null, '3', '0375-0422-0423-0424-0426', '一键拒绝', '图书到家-书店操作台-线上业务清单-申请列表-书店操作台-线上业务清单-申请列表-一键拒绝', null, 'admin/bookHomeNewBookShop/disposeOrderApply', '424', '5', '', '526', '2', '1', '2023-08-04 17:44:03', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('427', '书店操作台-线上业务清单-申请列表-导出书单', '', null, null, null, null, null, '3', '0375-0422-0423-0424-0427', '导出书单', '图书到家-书店操作台-线上业务清单-申请列表-书店操作台-线上业务清单-申请列表-导出书单', null, 'admin/bookHomeExport/orderList', '424', '5', '', '527', '2', '1', '2023-08-04 17:45:08', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('428', '书店操作台-线上业务清单-申请列表-同意', '', null, null, null, null, null, '3', '0375-0422-0423-0424-0428', '同意', '图书到家-书店操作台-线上业务清单-申请列表-书店操作台-线上业务清单-申请列表-同意', null, 'admin/bookHomeNewBookShop/disposeOrderApply', '424', '5', '', '528', '2', '1', '2023-08-04 17:45:52', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('429', '书店操作台-线上业务清单-申请列表-拒绝', '', null, null, null, null, null, '3', '0375-0422-0423-0424-0429', '拒绝', '图书到家-书店操作台-线上业务清单-申请列表-书店操作台-线上业务清单-申请列表-拒绝', null, 'admin/bookHomeNewBookShop/disposeOrderApply', '424', '5', '', '529', '2', '1', '2023-08-04 17:46:13', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('430', '书店操作台-线上业务清单-详情', '', '详情', null, 'onlineDetails', '/OnlineBusinesslist/onlineDetails', null, '2', '0375-0422-0423-0430', '查看详情', '图书到家-书店操作台-线上业务清单-详情', null, 'admin/bookHomeNewBookShop/orderDetail', '423', '4', '', '531', '2', '1', '2023-08-04 17:47:40', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('431', '书店操作台-线上业务清单-申请列表-查看详情', '', null, null, null, null, null, '3', '0375-0422-0423-0424-0431', '查看详情', '图书到家-书店操作台-线上业务清单-申请列表-书店操作台-线上业务清单-申请列表-查看详情', null, 'admin/bookHomeNewBookShop/orderDetail', '424', '5', '', '530', '2', '1', '2023-08-04 17:48:12', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('432', '书店操作台-线上业务清单-发货列表', '', '发货列表', null, 'Onlineviewsend', '/OnlineBusinesslist/Listview/send', null, '2', '0375-0422-0423-0432', null, '图书到家-书店操作台-线上业务清单-发货列表', null, 'admin/bookHomeNewBookShop/orderList', '423', '4', '', '532', '2', '1', '2023-08-04 17:48:53', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('433', '书店操作台-线上业务清单-发货列表-导出书单', '', null, null, null, null, null, '3', '0375-0422-0423-0432-0433', '导出书单', '图书到家-书店操作台-线上业务清单-发货列表-书店操作台-线上业务清单-发货列表-导出书单', null, 'admin/bookHomeExport/orderList', '432', '5', '', '533', '2', '1', '2023-08-04 17:49:29', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('434', '书店操作台-线上业务清单-发货列表-发货', '', null, null, null, null, null, '3', '0375-0422-0423-0432-0434', '发货', '图书到家-书店操作台-线上业务清单-发货列表-书店操作台-线上业务清单-发货列表-发货', null, 'admin/bookHomeNewBookShop/disposeOrderDeliver', '432', '5', '', '534', '2', '1', '2023-08-04 17:50:14', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('435', '书店操作台-线上业务清单-发货列表-无法发货', '', null, null, null, null, null, '3', '0375-0422-0423-0432-0435', '无法发货', '图书到家-书店操作台-线上业务清单-发货列表-书店操作台-线上业务清单-发货列表-无法发货', null, 'admin/bookHomeNewBookShop/disposeOrderDeliver', '432', '5', '', '535', '2', '1', '2023-08-04 17:50:39', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('436', '书店操作台-线上业务清单-发货列表-查看详情', '', null, null, null, null, null, '3', '0375-0422-0423-0432-0436', '查看详情', '图书到家-书店操作台-线上业务清单-发货列表-书店操作台-线上业务清单-发货列表-查看详情', null, 'admin/bookHomeNewBookShop/orderDetail', '432', '5', '', '536', '2', '1', '2023-08-04 17:50:59', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('437', '书店操作台-线上业务清单-所有订单', '', '所有订单', null, 'Onlineviewall', '/OnlineBusinesslist/Listview/all', null, '2', '0375-0422-0423-0437', null, '图书到家-书店操作台-线上业务清单-所有订单', null, 'admin/bookHomeNewBookShop/orderList', '423', '4', '', '537', '2', '1', '2023-08-04 17:51:51', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('438', '书店操作台-线上业务清单-所有列表-导出书单', '', null, null, null, null, null, '3', '0375-0422-0423-0438', '导出书单', '图书到家-书店操作台-线上业务清单-书店操作台-线上业务清单-所有列表-导出书单', null, 'admin/bookHomeExport/orderList', '423', '4', '', '538', '2', '1', '2023-08-04 17:52:38', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('439', '书店操作台-线上业务清单-所有列表-查看详情', '', null, null, null, null, null, '3', '0375-0422-0423-0439', '查看详情', '图书到家-书店操作台-线上业务清单-书店操作台-线上业务清单-所有列表-查看详情', null, 'admin/bookHomeNewBookShop/orderDetail', '423', '4', '', '539', '2', '1', '2023-08-04 17:53:09', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('440', '线下借阅操作', '', '线下借阅操作', null, 'OfflineBorrowing', '/Bookstoreconsole/OfflineBorrowing', null, '1', '0375-0422-0440', null, '图书到家-书店操作台-线下借阅操作', null, 'admin/bookHomeOfflineRecom/getReaderInfo,admin/shop/filterList,admin/bookHomeOfflineRecom/getBookInfo,admin/bookHomeOfflineRecom/affirmBorrow', '422', '3', '', '540', '2', '1', '2023-08-04 17:54:38', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('441', '线上借阅历史', '', '线上借阅历史', null, 'OnlineBorrowinghistory', '/Bookstoreconsole/OnlineBorrowinghistory', null, '1', '0375-0422-0441', null, '图书到家-书店操作台-线上借阅历史', null, 'admin/shop/filterList,admin/manage/filterList,admin/bookHomeNewBookShop/purchaseAllList', '422', '3', '', '541', '2', '1', '2023-08-04 17:55:35', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('442', '书店操作台-线上借阅历史-导出书单', '', null, null, null, null, null, '3', '0375-0422-0441-0442', '导出书单', '图书到家-书店操作台-线上借阅历史-书店操作台-线上借阅历史-导出书单', null, 'admin/bookHomeExport/shopPurchaseAllList', '441', '4', '', '542', '2', '1', '2023-08-04 17:56:37', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('443', '线下借阅历史', '', '线下借阅历史', null, 'OfflineBorrowinghistory', '/Bookstoreconsole/OfflineBorrowinghistory', null, '1', '0375-0422-0443', null, '图书到家-书店操作台-线下借阅历史', null, 'admin/shop/filterList,admin/manage/filterList,admin/bookHomeOfflineRecom/offlineListShop', '422', '3', '', '543', '2', '1', '2023-08-04 17:57:40', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('444', '书店操作台-线下借阅历史-导出书单', '', null, null, null, null, null, '3', '0375-0422-0443-0444', '导出书单', '图书到家-书店操作台-线下借阅历史-书店操作台-线下借阅历史-导出书单', null, 'admin/bookHomeExport/offlineListShop', '443', '4', '', '544', '2', '1', '2023-08-04 17:58:12', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('445', '新书结算清单', '', '新书结算清单', null, 'BooksSettlement', '/Bookstoreconsole/BooksSettlement', null, '1', '0375-0422-0445', null, '图书到家-书店操作台-新书结算清单', null, 'admin/shop/filterList,admin/manage/filterList,admin/bookHomeNewBookShop/getPurchaseNumber,admin/bookHomeNewBookShop/purchaseList', '422', '3', '', '545', '2', '1', '2023-08-04 17:59:07', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('446', '书店操作台-新书结算清单-全部结算', '', null, null, null, null, null, '3', '0375-0422-0445-0446', '全部结算', '图书到家-书店操作台-新书结算清单-书店操作台-新书结算清单-全部结算', null, 'admin/bookHomeNewBookShop/purchaseStateModify', '445', '4', '', '546', '2', '1', '2023-08-07 09:41:45', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('447', '书店操作台-新书结算清单-导出书单', '', null, null, null, null, null, '3', '0375-0422-0445-0447', '导出书单', '图书到家-书店操作台-新书结算清单-书店操作台-新书结算清单-导出书单', null, 'admin/bookHomeExport/purchaseShopList', '445', '4', '', '547', '2', '1', '2023-08-07 09:42:07', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('448', '书店操作台-新书结算清单-确认结算', '', null, null, null, null, null, '3', '0375-0422-0445-0448', '确认结算', '图书到家-书店操作台-新书结算清单-书店操作台-新书结算清单-确认结算', null, 'admin/bookHomeNewBookShop/purchaseStateModify', '445', '4', '', '548', '2', '1', '2023-08-07 09:42:38', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('449', '书籍信息管理', '', '书籍信息管理', null, 'Bookmanagement', '/Bookstoreconsole/Bookmanagement', null, '1', '0375-0422-0449', null, '图书到家-书店操作台-书籍信息管理', null, 'admin/newBookRecom/list,admin/shop/filterList,admin/newBookRecomType/filterList', '422', '3', '', '549', '2', '1', '2023-08-07 09:44:20', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('450', '书店操作台-书籍信息管理-添加新书', '', null, null, null, null, null, '3', '0375-0422-0449-0450', '添加新书', '图书到家-书店操作台-书籍信息管理-书店操作台-书籍信息管理-添加新书', null, 'admin/newBookRecom/add', '449', '4', '', '550', '2', '1', '2023-08-07 09:50:47', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('451', '书店操作台-书籍信息管理-全部上架', '', null, null, null, null, null, '3', '0375-0422-0449-0451', '全部上架', '图书到家-书店操作台-书籍信息管理-书店操作台-书籍信息管理-全部上架', null, 'admin/newBookRecom/upAndDown', '449', '4', '', '551', '2', '1', '2023-08-07 09:51:18', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('452', '书店操作台-书籍信息管理-全部不推荐', '', null, null, null, null, null, '3', '0375-0422-0449-0452', '全部不推荐', '图书到家-书店操作台-书籍信息管理-书店操作台-书籍信息管理-全部不推荐', null, 'admin/newBookRecom/upAndDown', '449', '4', '', '552', '2', '1', '2023-08-07 09:51:34', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('453', '书店操作台-书籍信息管理-类型列表', '', null, null, null, null, null, '3', '0375-0422-0449-0453', '类型列表', '图书到家-书店操作台-书籍信息管理-书店操作台-书籍信息管理-类型列表', null, 'admin/newBookRecomType/list,admin/shop/filterList,admin/newBookRecomType/change,admin/newBookRecomType/add,admin/newBookRecomType/del', '449', '4', '', '553', '2', '1', '2023-08-07 10:01:16', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('454', '书店操作台-书籍信息管理-模板导出', '', null, null, null, null, null, '3', '0375-0422-0449-0454', '模板导出', '图书到家-书店操作台-书籍信息管理-书店操作台-书籍信息管理-模板导出', null, 'admin/templateExport/shopNewBookRecommend', '449', '4', '', '554', '2', '1', '2023-08-07 10:02:59', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('455', '书店操作台-书籍信息管理-导入书架', '', null, null, null, null, null, '3', '0375-0422-0449-0455', '导入书架', '图书到家-书店操作台-书籍信息管理-书店操作台-书籍信息管理-导入书架', null, 'admin/shopBookImport/index', '449', '4', '', '555', '2', '1', '2023-08-07 10:04:05', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('456', '书店操作台-书籍信息管理-设置推荐', '', null, null, null, null, null, '3', '0375-0422-0449-0456', '设置推荐', '图书到家-书店操作台-书籍信息管理-书店操作台-书籍信息管理-设置推荐', null, 'admin/newBookRecom/cancelAndRelease', '449', '4', '', '556', '2', '1', '2023-08-07 10:04:36', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('457', '书店操作台-书籍信息管理-撤销推荐', '', null, null, null, null, null, '3', '0375-0422-0449-0457', '撤销推荐', '图书到家-书店操作台-书籍信息管理-书店操作台-书籍信息管理-撤销推荐', null, 'admin/newBookRecom/cancelAndRelease', '449', '4', '', '557', '2', '1', '2023-08-07 10:05:00', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('458', '书店操作台-书籍信息管理-详情', '', null, null, null, null, null, '3', '0375-0422-0449-0458', '详情', '图书到家-书店操作台-书籍信息管理-书店操作台-书籍信息管理-详情', null, 'admin/newBookRecom/detail', '449', '4', '', '558', '2', '1', '2023-08-07 10:05:23', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('459', '书店操作台-书籍信息管理-修改', '', null, null, null, null, null, '3', '0375-0422-0449-0459', '修改', '图书到家-书店操作台-书籍信息管理-书店操作台-书籍信息管理-修改', null, 'admin/newBookRecom/detail,admin/newBookRecom/change', '449', '4', '', '559', '2', '1', '2023-08-07 10:05:59', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('460', '书店操作台-书籍信息管理-删除', '', null, null, null, null, null, '3', '0375-0422-0449-0460', '删除', '图书到家-书店操作台-书籍信息管理-书店操作台-书籍信息管理-删除', null, 'admin/newBookRecom/del', '449', '4', '', '560', '2', '1', '2023-08-07 10:07:35', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('461', '邮费结算', '', '邮费结算', 'youfeijiesuan', null, null, null, '1', '0375-0461', null, '图书到家-邮费结算', null, '', '375', '2', '', '561', '2', '1', '2023-08-07 10:10:00', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('462', '邮费退款-书店', '', '邮费退款-书店', null, 'Bookstore', '/Postagesettlement/Bookstore', null, '1', '0375-0461-0462', null, '图书到家-邮费结算-邮费退款-书店', null, 'admin/shop/filterList,admin/bookHomeNewBookLib/refundList', '461', '3', '', '562', '2', '1', '2023-08-07 10:11:28', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('463', '邮费退款-书店-退款', '', null, null, null, null, null, '3', '0375-0461-0462-0463', '退款', '图书到家-邮费结算-邮费退款-书店-邮费退款-书店-退款', null, 'admin/bookHomeLibBook/refund', '462', '4', '', '563', '2', '1', '2023-08-07 10:27:55', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('464', '邮费退款-图书馆', '', '邮费退款-图书馆', null, 'librarys', '/Postagesettlement/library', null, '1', '0375-0461-0464', null, '图书到家-邮费结算-邮费退款-图书馆', null, 'admin/shop/filterList,admin/bookHomeNewBookLib/refundList', '461', '3', '', '564', '2', '1', '2023-08-07 10:29:49', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('465', '邮费退款-图书馆-退款', '', null, null, null, null, null, '3', '0375-0461-0464-0465', '退款', '图书到家-邮费结算-邮费退款-图书馆-邮费退款-图书馆-退款', null, 'admin/bookHomeLibBook/refund', '464', '4', '', '565', '2', '1', '2023-08-07 10:47:12', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('466', '邮费结算-发起方', '', '邮费结算-发起方', null, 'Originator', '/Postagesettlement/Originator', null, '1', '0375-0461-0466', null, '图书到家-邮费结算-邮费结算-发起方', null, 'admin/shop/filterList,admin/bookHomePostage/list,admin/bookHomePostage/statistics', '461', '3', '', '566', '2', '1', '2023-08-07 10:48:17', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('467', '邮费结算-发起方-全部结算', '', null, null, null, null, null, '3', '0375-0461-0466-0467', '全部结算', '图书到家-邮费结算-邮费结算-发起方-邮费结算-发起方-全部结算', null, 'admin/bookHomePostage/sponsorSettleState', '466', '4', '', '567', '2', '1', '2023-08-07 10:49:50', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('468', '邮费结算-发起方-结算', '', null, null, null, null, null, '3', '0375-0461-0466-0468', '结算', '图书到家-邮费结算-邮费结算-发起方-邮费结算-发起方-结算', null, 'admin/bookHomePostage/sponsorSettleState', '466', '4', '', '568', '2', '1', '2023-08-07 10:50:24', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('469', '邮费结算-结算方', '', '邮费结算-结算方', null, 'Settlementparty', '/Postagesettlement/Settlementparty', null, '1', '0375-0461-0469', null, '图书到家-邮费结算-邮费结算-结算方', null, 'admin/shop/filterList,admin/bookHomePostage/list,admin/bookHomePostage/statistics', '461', '3', '', '569', '2', '1', '2023-08-07 11:04:09', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('470', '邮费结算-结算方-全部结算', '', null, null, null, null, null, '3', '0375-0461-0469-0470', '全部结算', '图书到家-邮费结算-邮费结算-结算方-邮费结算-结算方-全部结算', null, 'admin/bookHomePostage/affirmSettleState', '469', '4', '', '570', '2', '1', '2023-08-07 11:04:57', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('471', '邮费结算-结算方-结算', '', null, null, null, null, null, '3', '0375-0461-0469-0471', '结算', '图书到家-邮费结算-邮费结算-结算方-邮费结算-结算方-结算', null, 'admin/bookHomePostage/affirmSettleState', '469', '4', '', '571', '2', '1', '2023-08-07 11:05:21', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('472', '书店管理', '', '书店管理', 'shudianguanli', 'BooksComehome', '/BooksComehome/BookShop', null, '1', '0375-0472', null, '图书到家-书店管理', null, 'admin/shop/list', '375', '2', '', '572', '2', '1', '2023-08-08 09:24:48', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('473', '添加书店', '', null, null, null, null, null, '3', '0375-0472-0473', '添加书店', '图书到家-书店管理-添加书店', null, 'admin/shop/add', '472', '3', '', '573', '2', '1', '2023-08-08 09:27:52', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('474', '书店管理-修改', '', null, null, null, null, null, '3', '0375-0472-0474', '修改', '图书到家-书店管理-书店管理-修改', null, 'admin/shop/change,admin/shop/detail', '472', '3', '', '574', '2', '1', '2023-08-08 09:28:40', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('475', '书店管理-详情', '', '详情', null, 'BookshopDetail', '/BooksComehome/BookShop/shopDetail', null, '2', '0375-0472-0475', '详情', '图书到家-书店管理-详情', null, 'admin/shop/detail', '472', '3', '', '575', '2', '1', '2023-08-08 09:29:12', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('476', '书店管理-编辑邮寄地址', '', null, null, null, null, null, '3', '0375-0472-0476', '编辑邮寄地址', '图书到家-书店管理-书店管理-编辑邮寄地址', null, 'admin/bookHomePurchaseSet/shopDeliverAddress', '472', '3', '', '576', '2', '1', '2023-08-08 09:32:07', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('477', '书店管理-删除', '', null, null, null, null, null, '3', '0375-0472-0477', '删除', '图书到家-书店管理-书店管理-删除', null, 'admin/shop/del', '472', '3', '', '577', '2', '1', '2023-08-08 09:32:44', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('478', '书店管理-操作', '', '操作', null, 'BookshopAdd', '/BooksComehome/BookShop/shopAdd', null, '2', '0375-0472-0478', null, '图书到家-书店管理-操作', null, 'admin/shop/detail,admin/shop/add,admin/shop/change', '472', '3', '', '578', '2', '1', '2023-08-08 09:33:56', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('479', '文旅地图', '', '文旅地图', 'jindiandaka', null, null, null, '1', '0479', null, '文旅地图', null, '', '0', '1', '', '580', '2', '1', '2023-08-08 09:34:24', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('480', '景点列表', '', '景点列表', 'active_list', 'AttractionsList', '/Tickspots/AttractionsList', null, '1', '0479-0480', null, '文旅地图-景点列表', null, 'admin/scenic/list,admin/scenicType/filterList', '479', '2', '', '581', '2', '1', '2023-08-08 09:39:28', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('481', '景点列表-添加', '', null, null, null, null, null, '3', '0479-0480-0481', '添加', '文旅地图-景点列表-景点列表-添加', null, 'admin/scenic/add', '480', '3', '', '582', '2', '1', '2023-08-08 10:17:16', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('482', '景点列表-发布', '', null, null, null, null, null, '3', '0479-0480-0482', '发布', '文旅地图-景点列表-景点列表-发布', null, 'admin/scenic/cancelAndRelease', '480', '3', '', '583', '2', '1', '2023-08-08 10:18:45', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('483', '景点列表-撤销', '', null, null, null, null, null, '3', '0479-0480-0483', '撤销', '文旅地图-景点列表-景点列表-撤销', null, 'admin/scenic/cancelAndRelease', '480', '3', '', '584', '2', '1', '2023-08-08 10:18:58', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('484', '景点列表-详情', '', null, null, null, null, null, '3', '0479-0480-0484', '详情', '文旅地图-景点列表-景点列表-详情', null, 'admin/scenic/detail', '480', '3', '', '585', '2', '1', '2023-08-08 10:19:21', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('485', '景点列表-详情', '', '详情', null, 'Attrdetails', '/Tickspots/AttractionsList/Attrdetails', null, '2', '0479-0480-0485', '详情', '文旅地图-景点列表-详情', null, 'admin/scenic/detail', '480', '3', '', '586', '2', '1', '2023-08-08 10:23:32', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('486', '景点列表-修改', '', null, null, null, null, null, '3', '0479-0480-0486', '修改', '文旅地图-景点列表-景点列表-修改', null, 'admin/scenic/detail,admin/scenic/change', '480', '3', '', '587', '2', '1', '2023-08-08 10:24:29', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('487', '景点列表-打卡作品', '', '打卡作品', null, 'Punchworks', '/Tickspots/AttractionsList/Punchworks', null, '2', '0479-0480-0487', '打卡作品', '文旅地图-景点列表-打卡作品', null, 'admin/scenicWorks/list', '480', '3', '', '588', '2', '1', '2023-08-08 10:26:28', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('488', '景点列表-打卡作品-详情', '', null, null, null, null, null, '3', '0479-0480-0487-0488', '详情', '文旅地图-景点列表-打卡作品-景点列表-打卡作品-详情', null, 'admin/scenicWorks/detail', '487', '4', '', '589', '2', '1', '2023-08-08 10:29:41', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('489', '景点列表-打卡作品-通过', '', null, null, null, null, null, '3', '0479-0480-0487-0489', '通过', '文旅地图-景点列表-打卡作品-景点列表-打卡作品-通过', null, 'admin/scenicWorks/worksCheck', '487', '4', '', '590', '2', '1', '2023-08-08 10:30:06', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('490', '景点列表-打卡作品-拒绝', '', null, null, null, null, null, '3', '0479-0480-0487-0490', '拒绝', '文旅地图-景点列表-打卡作品-景点列表-打卡作品-拒绝', null, 'admin/scenicWorks/worksCheck', '487', '4', '', '591', '2', '1', '2023-08-08 10:30:24', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('491', '景点列表-打卡作品-用户违规', '', null, null, null, null, null, '3', '0479-0480-0487-0491', '用户违规', '文旅地图-景点列表-打卡作品-景点列表-打卡作品-用户违规', null, 'admin/scenicWorks/violateAndCancel', '487', '4', '', '592', '2', '1', '2023-08-08 10:30:43', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('492', '景点列表-打卡作品-取消违规', '', null, null, null, null, null, '3', '0479-0480-0487-0492', '取消违规', '文旅地图-景点列表-打卡作品-景点列表-打卡作品-取消违规', null, 'admin/scenicWorks/violateAndCancel', '487', '4', '', '593', '2', '1', '2023-08-08 10:31:08', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('493', '景点列表-删除', '', null, null, null, null, null, '3', '0479-0480-0493', '删除', '文旅地图-景点列表-景点列表-删除', null, 'admin/scenic/del', '480', '3', '', '594', '2', '1', '2023-08-08 10:32:06', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('494', '类型管理', '', '类型管理', 'class', 'attractionsTypes', '/Tickspots/attractionsTypes', null, '1', '0479-0494', null, '文旅地图-类型管理', null, 'admin/scenicType/list', '479', '2', '', '596', '2', '1', '2023-08-08 10:57:03', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('495', '类型管理-添加类型', '', null, null, null, null, null, '3', '0479-0494-0495', '添加类型', '文旅地图-类型管理-类型管理-添加类型', null, 'admin/scenicType/add', '494', '3', '', '597', '2', '1', '2023-08-08 10:58:56', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('496', '类型管理-修改', '', null, null, null, null, null, '3', '0479-0494-0496', '修改', '文旅地图-类型管理-类型管理-修改', null, 'admin/scenicType/change', '494', '3', '', '598', '2', '1', '2023-08-08 10:59:15', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('497', '类型管理-删除', '', null, null, null, null, null, '3', '0479-0494-0497', '删除', '文旅地图-类型管理-类型管理-删除', null, 'admin/scenicType/del', '494', '3', '', '599', '2', '1', '2023-08-08 10:59:32', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('498', '码上游', '', '码上游', 'icon_ydpt', null, null, null, '1', '0498', null, '码上游', null, '', '0', '1', '', '600', '2', '1', '2023-08-08 11:26:59', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('499', '景区列表', '', '景区列表', 'icon_hdlb', 'scenicSpotList', '/ScanNavigation/scenicSpotList', null, '1', '0498-0499', null, '扫码导游-景区列表', null, 'admin/codeGuideExhibition/list', '498', '2', '', '601', '2', '1', '2023-08-08 11:27:41', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('500', '创建景区', '', '创建景区', null, 'addScenic', '/ScanNavigation/scenicSpotList/add', null, '2', '0498-0499-0500', '创建景区', '景区管理-景区列表-创建景区', null, 'admin/codeGuideExhibition/add', '499', '3', '', '602', '2', '1', '2023-08-08 11:53:58', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('501', '修改景区', '', '修改景区', null, 'changScenic', '/ScanNavigation/scenicSpotList/chang', null, '2', '0498-0499-0501', '修改', '景区管理-景区列表-修改景区', null, 'admin/codeGuideExhibition/detail,admin/codeGuideExhibition/change', '499', '3', '', '603', '2', '1', '2023-08-08 11:57:27', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('502', '景区列表-景点类型', '', '景点类型', null, 'scenicWorksType', '/ScanNavigation/scenicSpotList/management/worksType', null, '2', '0498-0499-0502', '景点类型', '景区管理-景区列表-景点类型', null, 'admin/codeGuideProductionType/list', '499', '3', '', '604', '2', '1', '2023-08-08 11:58:55', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('503', '活动列表-人员管理-签到', '', null, null, null, null, null, '3', '0001-0002-0017-0503', '签到', '活动日历-活动列表-人员管理-活动列表-人员管理-签到', null, 'admin/activity/sign', '17', '4', '', '22', '2', '1', '2023-08-08 16:55:02', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('504', '景区列表-撤回', '', null, null, null, null, null, '3', '0498-0499-0504', '撤回', '景区管理-景区列表-景区列表-撤回', null, 'admin/codeGuideExhibition/cancelAndRelease', '499', '3', '', '608', '2', '1', '2023-08-09 09:28:24', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('505', '景区列表-发布', '', null, null, null, null, null, '3', '0498-0499-0505', '发布', '景区管理-景区列表-景区列表-发布', null, 'admin/codeGuideExhibition/cancelAndRelease', '499', '3', '', '609', '2', '1', '2023-08-09 09:28:40', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('506', '景区列表-详情', '', null, null, null, null, null, '3', '0498-0499-0506', '详情', '景区管理-景区列表-景区列表-详情', null, 'admin/codeGuideExhibition/detail', '499', '3', '', '610', '2', '1', '2023-08-09 09:29:53', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('507', '参赛景点', '', '景点管理', null, 'scenicEntry', '/ScanNavigation/scenicSpotList/management/entry\'', null, '2', '0498-0499-0507', '景点管理', '扫码导游-景区列表-景点管理', null, 'admin/codeGuideProductionType/filterList,admin/codeGuideProduction/list', '499', '3', '', '611', '2', '1', '2023-08-09 09:32:01', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('508', '景区列表-删除', '', null, null, null, null, null, '3', '0498-0499-0508', '删除', '景区管理-景区列表-景区列表-删除', null, 'admin/codeGuideExhibition/del', '499', '3', '', '616', '2', '1', '2023-08-09 09:45:24', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('509', '景点类型-添加类型', '', null, null, null, null, null, '3', '0498-0499-0502-0509', '添加类型', '景区管理-景区列表-景点类型-景点类型-添加类型', null, 'admin/codeGuideProductionType/add', '502', '4', '', '605', '2', '1', '2023-08-09 09:46:44', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('510', '景点类型-修改', '', null, null, null, null, null, '3', '0498-0499-0502-0510', '修改', '景区管理-景区列表-景点类型-景点类型-修改', null, 'admin/codeGuideProductionType/change', '502', '4', '', '606', '2', '1', '2023-08-09 09:47:04', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('511', '景点类型-删除', '', null, null, null, null, null, '3', '0498-0499-0502-0511', '删除', '景区管理-景区列表-景点类型-景点类型-删除', null, 'admin/codeGuideProductionType/del', '502', '4', '', '607', '2', '1', '2023-08-09 09:47:21', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('512', '新增景点', '', '新增景点', null, 'scenicWorksAdd', '/ScanNavigation/scenicSpotList/management/worksAdd', null, '2', '0498-0499-0507-0512', '新增景点', '景区管理-景区列表-景区管理-新增景点', null, 'admin/codeGuideProduction/add', '507', '4', '', '612', '2', '1', '2023-08-09 09:50:44', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('513', '修改景点', '', '修改景点', null, 'scenicWorksChange', '/ScanNavigation/scenicSpotList/management/worksChange', null, '2', '0498-0499-0507-0513', '修改', '景区管理-景区列表-景区管理-修改景点', null, 'admin/codeGuideProduction/change,admin/codeGuideProduction/detail,admin/codeGuideProductionType/filterList', '507', '4', '', '613', '2', '1', '2023-08-09 09:52:13', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('514', '参赛景点-详情', '', null, null, null, null, null, '3', '0498-0499-0507-0514', '详情', '景区管理-景区列表-景区管理-参赛景点-详情', null, 'admin/codeGuideProduction/detail', '507', '4', '', '614', '2', '1', '2023-08-09 09:52:48', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('515', '参赛景点-删除', '', null, null, null, null, null, '3', '0498-0499-0507-0515', '删除', '景区管理-景区列表-景区管理-参赛景点-删除', null, 'admin/codeGuideProduction/del', '507', '4', '', '615', '2', '1', '2023-08-09 09:53:04', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('516', '新书推荐', '', '新书推荐', 'xinshutuijian', null, null, null, '1', '0516', null, '新书推荐', null, '', '0', '1', '', '617', '2', '1', '2023-08-09 10:03:39', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('517', '新书列表', '', '新书列表', 'xinshuliebiao', 'NewsList', '/newBookmanagement/NewsList', null, '1', '0516-0517', null, '新书推荐-新书列表', null, 'admin/newBookRecommendType/filterList,admin/newBookRecommend/list', '516', '2', '', '618', '2', '1', '2023-08-09 10:05:01', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('518', '添加新书', '', '添加新书', null, 'AddNews', '/newBookmanagement/addNews', null, '2', '0516-0517-0518', '添加新书', '新书推荐-新书列表-添加新书', null, 'admin/newBookRecommend/add', '517', '3', '', '26', '2', '2', '2023-08-09 10:15:09', '2023-08-15 17:30:05');
INSERT INTO `re_permission` VALUES ('519', '新书操作', '', '新书操作', null, 'AddNews', '/newBookmanagement/addNews', null, '2', '0516-0517-0519', null, '新书推荐-新书列表-新书操作', null, 'admin/newBookRecommend/change,admin/newBookRecommend/detail,admin/newBookRecommend/add', '517', '3', '', '619', '2', '1', '2023-08-09 10:16:08', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('520', '添加新书', '', null, null, null, null, null, '3', '0516-0517-0520', '添加新书', '新书推荐-新书列表-添加新书', null, 'admin/newBookRecommend/add', '517', '3', '', '620', '2', '1', '2023-08-09 10:17:17', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('521', '新书列表-推荐', '', null, null, null, null, null, '3', '0516-0517-0521', '推荐', '新书推荐-新书列表-新书列表-推荐', null, 'admin/newBookRecommend/cancelAndRelease', '517', '3', '', '621', '2', '1', '2023-08-09 10:18:05', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('522', '新书列表-撤销', '', null, null, null, null, null, '3', '0516-0517-0522', '撤销推荐', '新书推荐-新书列表-新书列表-撤销', null, 'admin/newBookRecommend/cancelAndRelease', '517', '3', '', '623', '2', '1', '2023-08-09 10:18:15', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('523', '新书详情', '', '详情', null, 'detailNews', '/newBookmanagement/detailNews', null, '2', '0516-0517-0523', '详情', '新书推荐-新书列表-详情', null, 'admin/newBookRecommend/detail', '517', '3', '', '624', '2', '1', '2023-08-09 10:19:05', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('524', '新书列表', '', null, null, null, null, null, '3', '0516-0517-0524', '修改', '新书推荐-新书列表-新书列表', null, 'admin/newBookRecommend/change', '517', '3', '', '625', '2', '1', '2023-08-09 10:38:25', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('525', '新书列表-删除', '', null, null, null, null, null, '3', '0516-0517-0525', '删除', '新书推荐-新书列表-新书列表-删除', null, 'admin/newBookRecommend/del', '517', '3', '', '626', '2', '1', '2023-08-09 10:39:13', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('526', '新书类型', '', '新书类型', 'xinshuneixin', 'NewsType', '/newBookmanagement/NewsType', null, '1', '0516-0526', null, '新书推荐-新书类型', null, 'admin/newBookRecommendType/list', '516', '2', '', '627', '2', '1', '2023-08-09 11:28:50', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('527', '新书类型-添加类型', '', null, null, null, null, null, '3', '0516-0526-0527', '添加类型', '新书推荐-新书类型-新书类型-添加类型', null, 'admin/newBookRecommendType/add', '526', '3', '', '628', '2', '1', '2023-08-09 11:31:13', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('528', '新书类型-修改', '', null, null, null, null, null, '3', '0516-0526-0528', '修改', '新书推荐-新书类型-新书类型-修改', null, 'admin/newBookRecommendType/change', '526', '3', '', '629', '2', '1', '2023-08-09 11:31:29', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('529', '新书类型-删除', '', null, null, null, null, null, '3', '0516-0526-0529', '删除', '新书推荐-新书类型-新书类型-删除', null, 'admin/newBookRecommendType/del', '526', '3', '', '630', '2', '1', '2023-08-09 11:31:44', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('530', '室内导航', '', '室内导航', 'jianzhu', null, null, null, '1', '0530', null, '室内导航', null, '', '0', '1', '', '631', '2', '1', '2023-08-09 11:38:48', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('531', '室内导航', '', '室内导航', 'jianzhuliebiao', 'BuildingList', '/BuildingManagement/megagameList', null, '1', '0530-0531', null, '室内导航-室内导航', null, 'admin/navigationBuild/list', '530', '2', '', '632', '2', '1', '2023-08-09 11:39:31', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('532', '创建作品', '', '创建作品', null, 'Buildingadd', '/BuildingManagement/megagameList/add', null, '2', '0530-0531-0532', '创建建筑', '建筑管理-建筑管理-创建作品', null, 'admin/navigationBuild/add', '531', '3', '', '633', '2', '1', '2023-08-09 11:43:12', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('533', '修改作品', '', '修改作品', null, 'Buildingchang', '/BuildingManagement/megagameList/chang', null, '2', '0530-0531-0533', '修改', '建筑管理-建筑管理-修改作品', null, 'admin/navigationBuild/detail,admin/navigationBuild/change', '531', '3', '', '634', '2', '1', '2023-08-09 11:48:08', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('534', '建筑管理-停用', '', null, null, null, null, null, '3', '0530-0531-0534', '停用', '建筑管理-建筑管理-建筑管理-停用', null, 'admin/navigationBuild/playAndCancel', '531', '3', '', '635', '2', '1', '2023-08-09 11:49:33', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('535', '建筑管理-启用', '', null, null, null, null, null, '3', '0530-0531-0535', '启用', '建筑管理-建筑管理-建筑管理-启用', null, 'admin/navigationBuild/playAndCancel', '531', '3', '', '636', '2', '1', '2023-08-09 11:49:40', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('536', '资源管理', '', '资源管理', null, 'Buildingentry', '/BuildingManagement/megagameList/management/Buildingentry', null, '2', '0530-0531-0536', '资源管理', '建筑管理-建筑管理-资源管理', null, 'admin/navigationArea/list', '531', '3', '', '637', '2', '1', '2023-08-09 11:57:54', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('537', '建筑管理-删除', '', null, null, null, null, null, '3', '0530-0531-0537', '删除', '建筑管理-建筑管理-建筑管理-删除', null, 'admin/navigationBuild/del', '531', '3', '', '647', '2', '1', '2023-08-09 11:58:47', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('538', '新增区域', '', '新增区域', null, 'addRegion', '/BuildingManagement/megagameList/management/addRegion', null, '2', '0530-0531-0536-0538', '新增区域', '建筑管理-建筑管理-资源管理-新增区域', null, 'admin/navigationArea/add', '536', '4', '', '638', '2', '1', '2023-08-09 13:19:07', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('539', '区域排序', '', null, null, null, null, null, '3', '0530-0531-0536-0539', '区域排序', '建筑管理-建筑管理-资源管理-区域排序', null, 'admin/navigationArea/filterList,admin/navigationArea/sortChange', '536', '4', '', '639', '2', '1', '2023-08-09 13:21:54', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('540', '资源管理-停用', '', null, null, null, null, null, '3', '0530-0531-0536-0540', '停用', '建筑管理-建筑管理-资源管理-资源管理-停用', null, 'admin/navigationArea/playAndCancel', '536', '4', '', '640', '2', '1', '2023-08-09 13:27:14', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('541', '资源管理-启用', '', null, null, null, null, null, '3', '0530-0531-0536-0541', '启用', '建筑管理-建筑管理-资源管理-资源管理-启用', null, 'admin/navigationArea/playAndCancel', '536', '4', '', '641', '2', '1', '2023-08-09 13:27:27', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('542', '修改区域', '', '修改区域', null, 'editRegion', '/BuildingManagement/megagameList/management/editRegion', null, '2', '0530-0531-0536-0542', '修改', '建筑管理-建筑管理-资源管理-修改区域', null, 'admin/navigationArea/detail,admin/navigationArea/change', '536', '4', '', '642', '2', '1', '2023-08-09 13:28:32', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('543', '新增点位', '', '新增点位', null, 'addPoint', '/BuildingManagement/megagameList/management/addPoint', null, '2', '0530-0531-0536-0543', '增加点位', '建筑管理-建筑管理-资源管理-新增点位', null, 'admin/navigationPoint/add', '536', '4', '', '643', '2', '1', '2023-08-09 13:30:29', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('544', '资源管理-删除', '', null, null, null, null, null, '3', '0530-0531-0536-0544', '删除', '建筑管理-建筑管理-资源管理-资源管理-删除', null, 'admin/navigationArea/del', '536', '4', '', '644', '2', '1', '2023-08-09 13:31:14', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('545', '修改点位', '', '修改点位', null, 'editPoint', '/BuildingManagement/megagameList/management/editPoint', null, '2', '0530-0531-0536-0545', '修改', '建筑管理-建筑管理-资源管理-修改点位', null, 'admin/navigationPoint/detail,admin/navigationPoint/change', '536', '4', '', '645', '2', '1', '2023-08-09 13:32:57', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('546', '资源管理-点位删除', '', null, null, null, null, null, '3', '0530-0531-0536-0546', '删除', '建筑管理-建筑管理-资源管理-资源管理-点位删除', null, 'admin/navigationPoint/del', '536', '4', '', '646', '2', '1', '2023-08-09 13:34:02', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('547', '志愿者管理', '', '志愿者管理', 'recommend_1', null, null, null, '1', '0547', null, '志愿者管理', null, '', '0', '1', '', '648', '2', '1', '2023-08-09 13:45:27', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('548', '景点列表-操作', '', '景点操作', null, 'Attradd', '/Tickspots/AttractionsList/Attradd', null, '2', '0479-0480-0548', null, '文旅地图-景点列表-景点操作', null, 'admin/scenic/detail,admin/scenic/add,admin/scenic/change', '480', '3', '', '595', '2', '1', '2023-08-09 14:13:18', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('549', '申请列表', '', '申请列表', 'icon_volunteer', 'volunteerApplyList', '/volunteer/applyList', null, '1', '0547-0549', null, '志愿者管理-申请列表', null, 'admin/volunteerApply/list,admin/volunteerServiceTime/filterList,admin/volunteerServiceIntention/filterList', '547', '2', '', '649', '2', '1', '2023-08-09 16:22:07', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('550', '申请列表-导出列表', '', null, null, null, null, null, '3', '0547-0549-0550', '导出列表', '志愿者管理-申请列表-申请列表-导出列表', null, 'admin/volunteerExport/volunteerApplyList', '549', '3', '', '650', '2', '1', '2023-08-09 16:36:35', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('551', '申请列表-详情', '', null, null, null, null, null, '3', '0547-0549-0551', '详情', '志愿者管理-申请列表-申请列表-详情', null, 'admin/volunteerApply/list', '549', '3', '', '651', '2', '1', '2023-08-09 16:37:30', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('552', '申请列表-同意', '', null, null, null, null, null, '3', '0547-0549-0552', '同意', '志愿者管理-申请列表-申请列表-同意', null, 'admin/volunteerApply/check', '549', '3', '', '652', '2', '1', '2023-08-09 16:37:55', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('553', '申请列表-拒绝', '', null, null, null, null, null, '3', '0547-0549-0553', '拒绝', '志愿者管理-申请列表-申请列表-拒绝', null, 'admin/volunteerApply/check', '549', '3', '', '653', '2', '1', '2023-08-09 16:38:21', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('554', '服务人员', '', '服务人员', 'fuwurenyuan', 'volunteerVolunteerList', '/volunteer/volunteerList', null, '1', '0547-0554', null, '志愿者管理-服务人员', null, 'admin/volunteerApply/list,admin/volunteerServiceIntention/filterList,admin/volunteerServiceTime/filterList', '547', '2', '', '654', '2', '1', '2023-08-09 16:50:34', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('555', '服务人员-导出列表', '', null, null, null, null, null, '3', '0547-0554-0555', '导出列表', '志愿者管理-服务人员-服务人员-导出列表', null, 'admin/volunteerExport/volunteerList', '554', '3', '', '655', '2', '1', '2023-08-09 16:51:13', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('556', '服务人员-详情', '', null, null, null, null, null, '3', '0547-0554-0556', '详情', '志愿者管理-服务人员-服务人员-详情', null, 'admin/volunteerApply/list', '554', '3', '', '656', '2', '1', '2023-08-09 16:59:29', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('557', '服务人员-服务时长设置', '', null, null, null, null, null, '3', '0547-0554-0557', '服务时长设置', '志愿者管理-服务人员-服务人员-服务时长设置', null, 'admin/volunteerApply/serviceTimeChange', '554', '3', '', '657', '2', '1', '2023-08-09 16:59:57', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('558', '服务人员-服务时长明细', '', null, null, null, null, null, '3', '0547-0554-0558', '服务时长明细', '志愿者管理-服务人员-服务人员-服务时长明细', null, 'admin/volunteerApply/serviceTimeList', '554', '3', '', '658', '2', '1', '2023-08-09 17:00:16', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('559', '服务意向', '', '服务意向', 'fuwuyixiang', 'ServiceIntention', '/VolunteerManagement/serviceIntention', null, '1', '0547-0559', null, '志愿者管理-服务意向', null, 'admin/volunteerServiceIntention/list', '547', '2', '', '659', '2', '1', '2023-08-09 17:02:41', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('560', '服务意向-添加意向', '', null, null, null, null, null, '3', '0547-0559-0560', '添加意向', '志愿者管理-服务意向-服务意向-添加意向', null, 'admin/volunteerServiceIntention/add', '559', '3', '', '660', '2', '1', '2023-08-09 17:15:54', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('561', '服务意向-修改', '', null, null, null, null, null, '3', '0547-0559-0561', '修改', '志愿者管理-服务意向-服务意向-修改', null, 'admin/volunteerServiceIntention/change', '559', '3', '', '661', '2', '1', '2023-08-09 17:16:13', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('562', '服务意向-删除', '', null, null, null, null, null, '3', '0547-0559-0562', '删除', '志愿者管理-服务意向-服务意向-删除', null, 'admin/volunteerServiceIntention/del', '559', '3', '', '662', '2', '1', '2023-08-09 17:16:30', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('563', '服务时间', '', '服务时间', 'fuwushijian', 'ServicingTime', '/VolunteerManagement/servicingtime', null, '1', '0547-0563', null, '志愿者管理-服务时间', null, 'admin/volunteerServiceTime/list', '547', '2', '', '663', '2', '1', '2023-08-09 17:18:41', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('564', '服务时间-添加时间', '', null, null, null, null, null, '3', '0547-0563-0564', '添加时间', '志愿者管理-服务时间-服务时间-添加时间', null, 'admin/volunteerServiceTime/add', '563', '3', '', '664', '2', '1', '2023-08-09 17:21:22', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('565', '服务时间-修改', '', null, null, null, null, null, '3', '0547-0563-0565', '修改', '志愿者管理-服务时间-服务时间-修改', null, 'admin/volunteerServiceTime/change', '563', '3', '', '665', '2', '1', '2023-08-09 17:25:48', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('566', '服务时间-删除', '', null, null, null, null, null, '3', '0547-0563-0566', '删除', '志愿者管理-服务时间-服务时间-删除', null, 'admin/volunteerServiceIntention/del', '563', '3', '', '666', '2', '1', '2023-08-09 17:26:03', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('567', '规则设置', '', '规则设置', 'guizeshezhi', 'rulesettings', '/VolunteerManagement/rulesettings', null, '1', '0547-0567', null, '志愿者管理-规则设置', null, 'admin/volunteer/detail,admin/volunteer/getVolunteerNotice,admin/volunteer/change,admin/volunteer/setVolunteerNotice', '547', '2', '', '667', '2', '1', '2023-08-09 17:28:43', '2023-09-21 11:32:00');
INSERT INTO `re_permission` VALUES ('568', '阅读网点', '', '阅读网点管理', 'zongfenguan', null, null, null, '1', '0568', null, '阅读网点管理', null, '', '0', '1', '', '669', '2', '1', '2023-08-09 17:32:30', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('569', '总分馆列表', '', '总分馆列表', 'zongfenguan1', 'assembleBranch', '/assembleBranch/branchList', null, '1', '0568-0569', null, '总分馆管理-总分馆列表', null, 'admin/branchInfo/list', '568', '2', '', '670', '2', '1', '2023-08-09 17:33:11', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('570', '新增总分馆', '', '新增总分馆', null, 'AddBranch', '/assembleBranch/addBranch', null, '2', '0568-0569-0570', '添加', '总分馆管理-总分馆列表-新增总分馆', null, 'admin/branchInfo/add', '569', '3', '', '671', '2', '1', '2023-08-09 17:44:15', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('571', '总分馆详情', '', '总分馆详情', null, 'DetailBranch', '/answerActivities/activityList/detailBranch', null, '2', '0568-0569-0571', '详情', '总分馆管理-总分馆列表-总分馆详情', null, 'admin/branchInfo/detail', '569', '3', '', '672', '2', '1', '2023-08-09 17:45:24', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('572', '修改总分馆', '', '修改总分馆', null, 'EditBranch', '/assembleBranch/editBranch', null, '2', '0568-0569-0572', '修改', '总分馆管理-总分馆列表-修改总分馆', null, 'admin/branchInfo/detail,admin/branchInfo/change', '569', '3', '', '673', '2', '1', '2023-08-09 17:46:03', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('573', '总分馆列表-删除', '', null, null, null, null, null, '3', '0568-0569-0573', '删除', '总分馆管理-总分馆列表-总分馆列表-删除', null, 'admin/branchInfo/del', '569', '3', '', '674', '2', '1', '2023-08-09 17:46:33', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('574', '码上借', '', '码上借', 'daomajiejilv', null, null, null, '1', '0574', null, '码上借', null, '', '0', '1', '', '675', '2', '1', '2023-08-09 17:50:13', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('575', '借阅记录', '', '借阅记录', 'jieyuejilv', 'scanBorrowingRecords', '/scanCodeBorrowing/borrowingRecords', null, '1', '0574-0575', null, '扫码借记录-借阅记录', null, 'admin/codeOnBorrowLog/borrowList,admin/codePlace/filterList', '574', '2', '', '683', '2', '1', '2023-08-09 17:50:52', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('576', '借阅记录-导出列表', '', null, null, null, null, null, '3', '0574-0575-0576', '导出列表', '扫码借记录-借阅记录-借阅记录-导出列表', null, 'admin/codeBorrowLogExport/borrowList', '575', '3', '', '684', '2', '1', '2023-08-09 17:51:49', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('577', '归还记录', '', '归还记录', 'guihuanjilv', 'scanReturnRecord', '/scanCodeBorrowing/returnRecord', null, '1', '0574-0577', null, '扫码借记录-归还记录', null, 'admin/codePlace/filterList,admin/codeOnBorrowLog/returnList', '574', '2', '', '685', '2', '1', '2023-08-09 17:56:22', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('578', '归还记录-导出列表', '', null, null, null, null, null, '3', '0574-0577-0578', '导出列表', '扫码借记录-归还记录-归还记录-导出列表', null, 'admin/codeBorrowLogExport/returnList', '577', '3', '', '686', '2', '1', '2023-08-09 17:57:12', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('579', '码上借配置', '', '码上借配置', 'qrcode', 'qrcodeConfiguration', '/scanCodeBorrowing/qrcodeConfiguration', null, '1', '0574-0579', null, '码上借记录-码上借配置', null, 'admin/codePlace/list', '574', '2', '', '676', '2', '1', '2023-08-09 18:00:16', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('580', '码上借配置-添加', '', null, null, null, null, null, '3', '0574-0579-0580', '添加', '码上借记录-码上借配置-码上借配置-添加', null, 'admin/codePlace/add', '579', '3', '', '677', '2', '1', '2023-08-09 18:01:08', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('581', '码上借配置-发布', '', null, null, null, null, null, '3', '0574-0579-0581', '发布', '码上借记录-码上借配置-码上借配置-发布', null, 'admin/codePlace/playAndCancel', '579', '3', '', '678', '2', '1', '2023-08-09 18:01:36', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('582', '码上借配置-撤销', '', null, null, null, null, null, '3', '0574-0579-0582', '撤销', '码上借记录-码上借配置-码上借配置-撤销', null, 'admin/codePlace/playAndCancel', '579', '3', '', '679', '2', '1', '2023-08-09 18:01:48', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('583', '码上借配置-修改', '', null, null, null, null, null, '3', '0574-0579-0583', '修改', '码上借记录-码上借配置-码上借配置-修改', null, 'admin/codePlace/detail,admin/codePlace/change', '579', '3', '', '680', '2', '1', '2023-08-09 18:02:22', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('584', '码上借配置-删除', '', null, null, null, null, null, '3', '0574-0579-0584', '删除', '码上借记录-码上借配置-码上借配置-删除', null, 'admin/codePlace/del', '579', '3', '', '681', '2', '1', '2023-08-09 18:02:47', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('585', '借阅规则', '', '借阅规则', 'jieyueguize', 'borrowingRules', '/scanCodeBorrowing/borrowingRules', null, '1', '0574-0585', null, '扫码借记录-借阅规则', null, 'admin/userProtocol/getProtocol,admin/userProtocol/setProtocol', '574', '2', '', '682', '2', '1', '2023-08-09 18:04:00', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('586', '数据分析', '', '数据分析', 'bigdata', null, null, null, '1', '0586', null, '数据分析', null, '', '0', '1', '', '687', '2', '1', '2023-08-09 18:05:25', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('587', '数据分析', '', '数据分析', 'icon_sjfx', 'DataAnalysis', '/bigdata/dataAnalysis', null, '1', '0586-0587', null, '数据分析-数据分析', null, 'admin/dataAnalysis/accessStatistics3,admin/dataAnalysis/appletStatistics,admin/dataAnalysis/activityStatistics,admin/dataAnalysis/recomAndSurveyStatistics', '586', '2', '', '688', '2', '1', '2023-08-09 18:06:27', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('588', '活动统计', '', '活动统计', 'icon_hdtj', 'ActiveStatistics', '/bigdata/activeStatistics', null, '1', '0586-0588', null, '数据分析-活动统计', null, 'admin/answerActivityToatlDataAnalysis/answerDataStatistics,admin/answerActivityToatlDataAnalysis/answerDataRanking,admin/answerActivityToatlDataAnalysis/answerTimeStatistics.,admin/turnActivityToatlDataAnalysis/turnDataStatistics,admin/turnActivityToatlDataAnalysis/turnDataRanking,admin/turnActivityToatlDataAnalysis/turnTimeStatistics,admin/contestToatlDataAnalysis/contestDataStatistics,admin/contestToatlDataAnalysis/contestDataRanking', '586', '2', '', '689', '2', '1', '2023-08-09 18:08:07', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('589', '系统设置', '', '系统设置', 'set', null, null, null, '1', '0589', null, '系统设置', null, '', '0', '1', '', '690', '2', '1', '2023-08-10 09:10:29', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('590', '用户管理', '', '用户管理', 'user', 'UserList', '/UserManagement/userList', null, '1', '0589-0590', null, '设置-用户管理', null, 'admin/userInfo/list,admin/violateConfig/detail', '589', '2', '', '691', '2', '1', '2023-08-10 09:16:12', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('591', '用户管理-违规规则设置', '', null, null, null, null, null, '3', '0589-0590-0591', '违规规则设置', '设置-用户管理-用户管理-违规规则设置', null, 'admin/violateConfig/change', '590', '3', '', '692', '2', '1', '2023-08-10 09:24:01', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('592', '用户管理-一键清空违规', '', null, null, null, null, null, '3', '0589-0590-0592', '一键清空违规', '设置-用户管理-用户管理-一键清空违规', null, 'admin/userInfo/clearViolate', '590', '3', '', '693', '2', '1', '2023-08-10 09:24:47', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('593', '用户管理-清空违规次数', '', null, null, null, null, null, '3', '0589-0590-0593', '清空违规次数', '设置-用户管理-用户管理-清空违规次数', null, 'admin/userInfo/clearViolate', '590', '3', '', '694', '2', '1', '2023-08-10 09:25:29', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('594', '用户管理-设置积分', '', null, null, null, null, null, '3', '0589-0590-0594', '设置积分', '设置-用户管理-用户管理-设置积分', null, 'admin/scoreInfo/scoreChange', '590', '3', '', '695', '2', '1', '2023-08-10 09:26:43', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('595', '用户管理-积分明细', '', null, null, null, null, null, '3', '0589-0590-0595', '积分明细', '设置-用户管理-用户管理-积分明细', null, 'admin/scoreInfo/scoreLog', '590', '3', '', '696', '2', '1', '2023-08-10 09:28:33', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('596', '用户管理-活动报名违规详情', '', null, null, null, null, null, '3', '0589-0590-0596', '活动报名违规详情', '设置-用户管理-用户管理-活动报名违规详情', null, 'admin/userInfo/getViolateList', '590', '3', '', '697', '2', '1', '2023-08-10 09:34:47', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('597', '用户管理-场馆预约违规详情', '', null, null, null, null, null, '3', '0589-0590-0597', '场馆预约违规详情', '设置-用户管理-用户管理-场馆预约违规详情', null, 'admin/userInfo/getViolateList', '590', '3', '', '698', '2', '1', '2023-08-10 09:34:58', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('598', '用户管理-在线投票违规详情', '', null, null, null, null, null, '3', '0589-0590-0598', '在线投票违规详情', '设置-用户管理-用户管理-在线投票违规详情', null, 'admin/userInfo/getViolateList', '590', '3', '', '699', '2', '1', '2023-08-10 09:35:07', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('599', '用户管理-景点打卡违规详情', '', null, null, null, null, null, '3', '0589-0590-0599', '景点打卡违规详情', '设置-用户管理-用户管理-景点打卡违规详情', null, 'admin/userInfo/getViolateList', '590', '3', '', '700', '2', '1', '2023-08-10 09:35:19', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('600', '用户管理-空间预约违规详情', '', null, null, null, null, null, '3', '0589-0590-0600', '空间预约违规详情', '设置-用户管理-用户管理-空间预约违规详情', null, 'admin/userInfo/getViolateList', '590', '3', '', '701', '2', '1', '2023-08-10 09:36:55', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('601', '意见反馈', '', '意见反馈', 'voice_message', 'ReaderList', '/ReaderComments/ReaderList', null, '1', '0589-0601', null, '设置-意见反馈', null, 'admin/feedback/list', '589', '2', '', '712', '2', '1', '2023-08-10 09:46:21', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('602', '意见反馈-回复', '', null, null, null, null, null, '3', '0589-0601-0602', '回复', '设置-意见反馈-意见反馈-回复', null, 'admin/feedback/reply', '601', '3', '', '713', '2', '1', '2023-08-10 09:47:05', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('603', 'banner管理', '', 'banner管理', 'resource2', 'PublicBanner', '/ImageManagement/publicBanner', null, '1', '0589-0603', null, '设置-banner管理', null, 'admin/banner/list', '589', '2', '', '714', '2', '1', '2023-08-10 10:07:43', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('604', '添加banner', '', '添加banner', null, 'AddBanner', '/ImageManagement/addBanner', null, '2', '0589-0603-0604', '添加banner', '设置-banner管理-添加banner', null, 'admin/banner/add', '603', '3', '', '715', '2', '1', '2023-08-10 10:28:51', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('605', '修改banner', '', '修改banner', null, 'ChangBanner', '/ImageManagement/changBanner', null, '2', '0589-0603-0605', '修改', '设置-banner管理-修改banner', null, 'admin/banner/detail,admin/banner/change', '603', '3', '', '716', '2', '1', '2023-08-10 10:38:49', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('606', 'banner管理-保存', '', null, null, null, null, null, '3', '0589-0603-0606', '保存', '设置-banner管理-banner管理-保存', null, 'admin/banner/sortChange', '603', '3', '', '717', '2', '1', '2023-08-10 10:40:08', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('607', 'banner管理-删除', '', null, null, null, null, null, '3', '0589-0603-0607', '删除', '设置-banner管理-banner管理-删除', null, 'admin/banner/del', '603', '3', '', '718', '2', '1', '2023-08-10 10:41:44', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('608', '积分设置', '', '积分设置', 'rule', 'IntegralList', '/IntegralSet/integralList', null, '1', '0589-0608', null, '设置-积分设置', null, 'admin/scoreInfo/list', '589', '2', '', '719', '2', '1', '2023-08-10 10:47:07', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('609', '积分设置-调整', '', null, null, null, null, null, '3', '0589-0608-0609', '调整', '设置-积分设置-积分设置-调整', null, 'admin/scoreInfo/change', '608', '3', '', '720', '2', '1', '2023-08-10 10:49:14', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('610', '管理员列表', '', '管理员列表', 'admin', 'adminList', '/systemSetting/adminList', null, '1', '0589-0610', null, '设置-管理员列表', null, 'admin/manage/list', '589', '2', '', '721', '2', '1', '2023-08-10 10:56:53', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('611', '添加管理员', '', '添加管理员', null, 'addAdmin', '/systemSetting/adminList/add', null, '2', '0589-0610-0611', '添加', '设置-管理员列表-添加管理员', null, 'admin/manage/add', '610', '3', '', '722', '2', '1', '2023-08-10 11:02:01', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('612', '修改管理员', '', '修改管理员', null, 'reviseAdmin', '/systemSetting/adminList/revise', null, '2', '0589-0610-0612', '修改', '设置-管理员列表-修改管理员', null, 'admin/manage/change,admin/manage/detail', '610', '3', '', '723', '2', '1', '2023-08-10 11:02:18', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('613', '管理员列表-删除', '', null, null, null, null, null, '3', '0589-0610-0613', '删除', '设置-管理员列表-管理员列表-删除', null, 'admin/manage/del', '610', '3', '', '724', '2', '1', '2023-08-10 11:10:02', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('614', '管理员列表-修改密码', '', null, null, null, null, null, '3', '0589-0610-0614', '修改密码', '设置-管理员列表-管理员列表-修改密码', null, 'admin/manage/changePwd', '610', '3', '', '725', '2', '1', '2023-08-10 11:24:34', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('615', '角色列表', '', '角色列表', 'role', 'roleList', '/systemSetting/roleList', null, '1', '0589-0615', null, '设置-角色列表', null, 'admin/role/list', '589', '2', '', '726', '2', '1', '2023-08-10 11:43:54', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('616', '角色列表-添加角色', '', null, null, null, null, null, '3', '0589-0615-0616', '添加角色', '设置-角色列表-角色列表-添加角色', null, 'admin/role/add', '615', '3', '', '727', '2', '1', '2023-08-10 13:03:16', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('617', '角色列表-修改', '', null, null, null, null, null, '3', '0589-0615-0617', '修改', '设置-角色列表-角色列表-修改', null, 'admin/role/change,admin/role/detail', '615', '3', '', '728', '2', '1', '2023-08-10 13:03:39', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('618', '角色列表-删除', '', null, null, null, null, null, '3', '0589-0615-0618', '删除', '设置-角色列表-角色列表-删除', null, 'admin/role/del', '615', '3', '', '729', '2', '1', '2023-08-10 13:04:01', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('619', '权限管理', '', '权限管理', 'permission', 'AuthorityList', '/systemSetting/authorityList', null, '1', '0589-0619', null, '设置-权限管理', null, 'admin/permission/list', '589', '2', '', '730', '2', '1', '2023-08-10 13:07:30', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('620', '添加权限', '', '添加权限', null, 'AddauthorityList', '/Authority/authorityList/add', null, '2', '0589-0619-0620', '添加权限', '设置-权限管理-添加权限', null, 'admin/permission/add', '619', '3', '', '731', '2', '1', '2023-08-10 13:17:07', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('621', '修改权限', '', '修改权限', null, 'reviseAuthority', '/systemSetting/authorityList/revise', null, '2', '0589-0619-0621', '修改', '设置-权限管理-修改权限', null, 'admin/permission/change,admin/permission/detail', '619', '3', '', '732', '2', '1', '2023-08-10 13:17:49', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('622', '权限管理-删除', '', null, null, null, null, null, '3', '0589-0619-0622', '删除', '设置-权限管理-权限管理-删除', null, 'admin/permission/del', '619', '3', '', '733', '2', '1', '2023-08-10 13:32:58', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('623', '权限管理-系统菜单排序', '', null, null, null, null, null, '3', '0589-0619-0623', '系统菜单排序', '设置-权限管理-权限管理-系统菜单排序', null, 'admin/permission/orderChange', '619', '3', '', '734', '2', '1', '2023-08-10 13:33:34', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('624', '空间预约人员-签到打卡', '', null, null, null, null, null, '3', '0350-0351-0358-0624', '签到打卡', '自习室预约-预约列表-预约人员-空间预约人员-签到打卡', null, 'admin/studyRoomReservationApply/applySignIn', '358', '4', '', '464', '2', '1', '2023-08-18 14:04:00', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('625', '事件配置', '', '事件配置', 'event', 'eventConfiguration', '/SystemSettings/eventConfiguration', null, '1', '0589-0625', null, '系统设置-事件配置', null, 'admin/appViewGray/list', '589', '2', '', '708', '2', '1', '2023-08-22 15:39:28', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('626', '添加', '', null, null, null, null, null, '3', '0589-0625-0626', '添加', '系统设置-事件配置-添加', null, 'admin/appViewGray/add', '625', '3', '', '709', '2', '1', '2023-08-22 15:41:53', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('627', '修改', '', null, null, null, null, null, '3', '0589-0625-0627', '修改', '系统设置-事件配置-修改', null, 'admin/appViewGray/change', '625', '3', '', '710', '2', '1', '2023-08-22 15:42:10', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('628', '删除', '', null, null, null, null, null, '3', '0589-0625-0628', '删除', '系统设置-事件配置-删除', null, 'admin/appViewGray/del', '625', '3', '', '711', '2', '1', '2023-08-22 15:42:32', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('629', '师资预约', '', '师资预约', null, 'teachersList', '/ReaderLnteraction/teachersList', null, '1', '0303-0304-0629', null, '读者预约-预约列表-师资预约', null, 'admin/reservation/list', '304', '3', '', '364', '2', '1', '2023-08-22 17:06:10', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('630', '创建师资预约', '', '创建预约', null, 'addteachers', '/ReaderLnteraction/addteachers', null, '2', '0303-0304-0629-0630', '创建预约', '读者预约-预约列表-师资预约-创建预约', null, 'admin/reservation/add,admin/reservation/filterTagList,admin/reservation/getReservationApplyParam', '629', '4', '', '365', '2', '1', '2023-08-22 17:06:58', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('631', '师资预约-发布', '', null, null, null, null, null, '3', '0303-0304-0629-0631', '发布', '读者预约-预约列表-师资预约-师资预约-发布', null, 'admin/reservation/playAndCancel', '629', '4', '', '366', '2', '1', '2023-08-22 17:56:56', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('632', '师资预约-撤销', '', null, null, null, null, null, '3', '0303-0304-0629-0632', '撤销', '读者预约-预约列表-师资预约-师资预约-撤销', null, 'admin/reservation/playAndCancel', '629', '4', '', '367', '2', '1', '2023-08-22 17:57:40', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('633', '师资预约-详情', '', null, null, null, null, null, '3', '0303-0304-0629-0633', '详情', '读者预约-预约列表-师资预约-师资预约-详情', null, 'admin/reservation/detail', '629', '4', '', '368', '2', '1', '2023-08-22 18:03:09', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('634', '师资预约修改', '', '修改', null, 'changteachers', '/ReaderLnteraction/changteachers', null, '2', '0303-0304-0629-0634', '修改', '读者预约-预约列表-师资预约-修改', null, 'admin/reservation/detail,admin/reservation/getReservationApplyParam,admin/reservation/filterTagList,admin/reservation/change', '629', '4', '', '369', '2', '1', '2023-08-22 18:03:57', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('635', '师资预约特殊日期排班表', '', '特殊日期排班表', null, 'teachersSpecialview', '/ReaderLnteraction/teachersSpecialview', null, '2', '0303-0304-0629-0635', '特殊日期排班表', '读者预约-预约列表-师资预约-特殊日期排班表', null, 'admin/reservationSpecialSchedule/list', '629', '4', '', '370', '2', '1', '2023-08-23 09:03:32', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('636', '师资预约人员', '', '预约人员', null, 'teachersPersonnel', '/ReaderLnteraction/teachersPersonnel', null, '2', '0303-0304-0629-0636', '预约人员', '读者预约-预约列表-师资预约-预约人员', null, 'admin/reservationApply/list,admin/reservation/detail', '629', '4', '', '375', '2', '1', '2023-08-23 09:04:37', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('637', '师资预约-删除', '', null, null, null, null, null, '3', '0303-0304-0629-0637', '删除', '读者预约-预约列表-师资预约-师资预约-删除', null, 'admin/reservation/del', '629', '4', '', '386', '2', '1', '2023-08-23 09:05:59', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('638', '师资预约-特殊日期排班表-新增时间段', '', null, null, null, null, null, '3', '0303-0304-0629-0635-0638', '新增时间段', '读者预约-预约列表-师资预约-特殊日期排班表-师资预约-特殊日期排班表-新增时间段', null, 'admin/reservationSpecialSchedule/add', '635', '5', '', '371', '2', '1', '2023-08-23 09:08:22', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('639', '师资预约-特殊日期排班表-修改', '', null, null, null, null, null, '3', '0303-0304-0629-0635-0639', '修改', '读者预约-预约列表-师资预约-特殊日期排班表-师资预约-特殊日期排班表-修改', null, 'admin/reservationSpecialSchedule/detail,admin/reservationSpecialSchedule/change', '635', '5', '', '372', '2', '1', '2023-08-23 09:08:48', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('640', '师资预约-特殊日期排班表-删除', '', null, null, null, null, null, '3', '0303-0304-0629-0635-0640', '删除', '读者预约-预约列表-师资预约-特殊日期排班表-师资预约-特殊日期排班表-删除', null, 'admin/reservationSpecialSchedule/del', '635', '5', '', '373', '2', '1', '2023-08-23 09:09:19', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('641', '师资预约-预约人员-取消预约', '', null, null, null, null, null, '3', '0303-0304-0629-0636-0641', '取消预约', '读者预约-预约列表-师资预约-预约人员-师资预约-预约人员-取消预约', null, 'admin/reservationApply/applyCancel', '636', '5', '', '376', '2', '1', '2023-08-23 09:10:26', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('642', '师资预约-预约人员-导出列表', '', null, null, null, null, null, '3', '0303-0304-0305-0312-0642', '导出列表', '读者预约-预约列表-预约列表-预约人员-师资预约-预约人员-导出列表', null, 'admin/reservationApplyExport/index', '312', '5', '', '330', '2', '1', '2023-08-23 09:11:08', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('643', '师资预约-预约人员-同意', '', null, null, null, null, null, '3', '0303-0304-0629-0636-0643', '同意', '读者预约-预约列表-师资预约-预约人员-师资预约-预约人员-同意', null, 'admin/reservationApply/agreeAndRefused', '636', '5', '', '377', '2', '1', '2023-08-23 09:11:42', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('644', '师资预约-预约人员-拒绝', '', null, null, null, null, null, '3', '0303-0304-0629-0636-0644', '拒绝', '读者预约-预约列表-师资预约-预约人员-师资预约-预约人员-拒绝', null, 'admin/reservationApply/agreeAndRefused', '636', '5', '', '378', '2', '1', '2023-08-23 09:22:27', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('645', '师资预约-预约人员-预约信息', '', null, null, null, null, null, '3', '0303-0304-0629-0636-0645', '预约信息', '读者预约-预约列表-师资预约-预约人员-师资预约-预约人员-预约信息', null, 'admin/reservationApply/list', '636', '5', '', '379', '2', '1', '2023-08-23 09:22:48', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('646', '师资预约-预约人员-用户违规', '', null, null, null, null, null, '3', '0303-0304-0629-0636-0646', '用户违规', '读者预约-预约列表-师资预约-预约人员-师资预约-预约人员-用户违规', null, 'admin/reservationApply/violateAndCancel', '636', '5', '', '380', '2', '1', '2023-08-23 09:30:30', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('647', '师资预约-预约人员-取消违规', '', null, null, null, null, null, '3', '0303-0304-0629-0636-0647', '取消违规', '读者预约-预约列表-师资预约-预约人员-师资预约-预约人员-取消违规', null, 'admin/reservationApply/violateAndCancel', '636', '5', '', '381', '2', '1', '2023-08-23 09:30:54', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('648', '设备预约', '', '设备预约', null, 'articleList', '/ReaderLnteraction/articleList', null, '1', '0303-0304-0648', null, '读者预约-预约列表-设备预约', null, 'admin/reservation/list', '304', '3', '', '387', '2', '1', '2023-08-23 10:05:59', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('649', '教室预约', '', '教室预约', null, 'classroomList', '/ReaderLnteraction/classroomList', null, '1', '0303-0304-0649', null, '读者预约-预约列表-教室预约', null, 'admin/reservation/list', '304', '3', '', '408', '2', '1', '2023-08-23 10:06:57', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('650', '展览预约', '', '展览预约', null, 'exhibitList', '/ReaderLnteraction/exhibitList', null, '1', '0303-0304-0650', null, '读者预约-预约列表-展览预约', null, 'admin/reservation/list', '304', '3', '', '428', '2', '1', '2023-08-23 10:07:26', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('651', '创建设备预约', '', '创建预约', null, 'addteachers', '/ReaderLnteraction/addteachers', null, '2', '0303-0304-0648-0651', '创建预约', '读者预约-预约列表-设备预约-创建预约', null, 'admin/reservation/add,admin/reservation/filterTagList,admin/reservation/getReservationApplyParam', '648', '4', '', '388', '2', '1', '2023-08-23 10:35:30', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('652', '设备预约-发布', '', null, null, null, null, null, '3', '0303-0304-0648-0652', '发布', '读者预约-预约列表-设备预约-设备预约-发布', null, 'admin/reservation/playAndCancel', '648', '4', '', '389', '2', '1', '2023-08-23 10:35:59', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('653', '设备预约-撤销', '', null, null, null, null, null, '3', '0303-0304-0648-0653', '撤销', '读者预约-预约列表-设备预约-设备预约-撤销', null, 'admin/reservation/playAndCancel', '648', '4', '', '390', '2', '1', '2023-08-23 10:41:08', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('654', '设备预约-详情', '', null, null, null, null, null, '3', '0303-0304-0648-0654', '详情', '读者预约-预约列表-设备预约-设备预约-详情', null, 'admin/reservation/detail', '648', '4', '', '391', '2', '1', '2023-08-23 10:41:34', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('655', '设备预约修改', '', '修改', null, 'changteachers', '/ReaderLnteraction/changteachers', null, '2', '0303-0304-0648-0655', '修改', '读者预约-预约列表-设备预约-修改', null, 'admin/reservation/detail,admin/reservation/getReservationApplyParam,admin/reservation/filterTagList,admin/reservation/change', '648', '4', '', '392', '2', '1', '2023-08-23 10:42:33', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('656', '设备预约删除', '', null, null, null, null, null, '3', '0303-0304-0648-0656', '删除', '读者预约-预约列表-设备预约-设备预约删除', null, 'admin/reservation/del', '648', '4', '', '393', '2', '1', '2023-08-23 10:43:05', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('657', '设备预约特殊日期排班表', '', '特殊日期排班表', null, 'teachersSpecialview', '/ReaderLnteraction/teachersSpecialview', null, '2', '0303-0304-0648-0657', '特殊日期排班表', '读者预约-预约列表-设备预约-特殊日期排班表', null, 'admin/reservationSpecialSchedule/list', '648', '4', '', '394', '2', '1', '2023-08-23 10:44:36', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('658', '设备预约人员', '', '预约人员', null, 'articlePersonnel', '/ReaderLnteraction/articlePersonnel', null, '2', '0303-0304-0648-0658', '预约人员', '读者预约-预约列表-设备预约-预约人员', null, 'admin/reservationApply/list,admin/reservation/detail', '648', '4', '', '397', '2', '1', '2023-08-23 10:50:34', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('659', '设备预约-特殊日期排班表-新增时间段', '', null, null, null, null, null, '3', '0303-0304-0648-0657-0659', '新增时间段', '读者预约-预约列表-设备预约-特殊日期排班表-设备预约-特殊日期排班表-新增时间段', null, 'admin/reservationSpecialSchedule/add', '657', '5', '', '395', '2', '1', '2023-08-23 10:54:18', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('660', '设备预约-特殊日期排班表-修改', '', null, null, null, null, null, '3', '0303-0304-0629-0635-0660', '修改', '读者预约-预约列表-师资预约-特殊日期排班表-设备预约-特殊日期排班表-修改', null, 'admin/reservationSpecialSchedule/detail,admin/reservationSpecialSchedule/change', '635', '5', '', '374', '2', '1', '2023-08-23 10:55:01', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('661', '设备预约-特殊日期排班表-删除', '', null, null, null, null, null, '3', '0303-0304-0648-0657-0661', '删除', '读者预约-预约列表-设备预约-特殊日期排班表-设备预约-特殊日期排班表-删除', null, 'admin/reservationSpecialSchedule/del', '657', '5', '', '396', '2', '1', '2023-08-23 10:55:48', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('662', '设备预约-预约人员-取消预约', '', null, null, null, null, null, '3', '0303-0304-0648-0658-0662', '取消预约', '读者预约-预约列表-设备预约-预约人员-设备预约-预约人员-取消预约', null, 'admin/reservationApply/applyCancel', '658', '5', '', '398', '2', '1', '2023-08-23 10:56:17', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('663', '设备预约-预约人员-同意', '', null, null, null, null, null, '3', '0303-0304-0648-0658-0663', '同意', '读者预约-预约列表-设备预约-预约人员-设备预约-预约人员-同意', null, 'admin/reservationApply/agreeAndRefused', '658', '5', '', '399', '2', '1', '2023-08-23 10:57:28', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('664', '设备预约-预约人员-拒绝', '', null, null, null, null, null, '3', '0303-0304-0648-0658-0664', '拒绝', '读者预约-预约列表-设备预约-预约人员-设备预约-预约人员-拒绝', null, 'admin/reservationApply/agreeAndRefused', '658', '5', '', '400', '2', '1', '2023-08-23 10:57:55', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('665', '设备预约-预约人员-预约信息', '', null, null, null, null, null, '3', '0303-0304-0648-0658-0665', '预约信息', '读者预约-预约列表-设备预约-预约人员-设备预约-预约人员-预约信息', null, 'admin/reservationApply/list', '658', '5', '', '401', '2', '1', '2023-08-23 10:58:23', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('666', '设备预约-预约人员-用户违规', '', null, null, null, null, null, '3', '0303-0304-0648-0658-0666', '用户违规', '读者预约-预约列表-设备预约-预约人员-设备预约-预约人员-用户违规', null, 'admin/reservationApply/violateAndCancel', '658', '5', '', '402', '2', '1', '2023-08-23 10:58:58', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('667', '设备预约-预约人员-取消违规', '', null, null, null, null, null, '3', '0303-0304-0648-0658-0667', '取消违规', '读者预约-预约列表-设备预约-预约人员-设备预约-预约人员-取消违规', null, 'admin/reservationApply/violateAndCancel', '658', '5', '', '403', '2', '1', '2023-08-23 11:01:11', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('668', '物品预约-删除', '', null, null, null, null, null, '3', '0303-0304-0648-0668', '删除', '读者预约-预约列表-物品预约-物品预约-删除', null, 'admin/reservation/del', '648', '4', '', '413', '2', '2', '2023-08-23 11:02:09', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('669', '创建教室预约', '', '创建预约', null, 'addteachers', '/ReaderLnteraction/addteachers', null, '2', '0303-0304-0649-0669', '创建预约', '读者预约-预约列表-教室预约-创建预约', null, 'admin/reservation/add,admin/reservation/filterTagList,admin/reservation/getReservationApplyParam', '649', '4', '', '409', '2', '1', '2023-08-23 11:09:27', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('670', '教室预约-发布', '', null, null, null, null, null, '3', '0303-0304-0649-0670', '发布', '读者预约-预约列表-教室预约-教室预约-发布', null, 'admin/reservation/playAndCancel', '649', '4', '', '410', '2', '1', '2023-08-23 11:10:21', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('671', '教室预约-撤销', '', null, null, null, null, null, '3', '0303-0304-0649-0671', '撤销', '读者预约-预约列表-教室预约-教室预约-撤销', null, 'admin/reservation/playAndCancel', '649', '4', '', '411', '2', '1', '2023-08-23 11:10:49', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('672', '教室预约-详情', '', null, null, null, null, null, '3', '0303-0304-0649-0672', '详情', '读者预约-预约列表-教室预约-教室预约-详情', null, 'admin/reservation/detail', '649', '4', '', '412', '2', '1', '2023-08-23 11:14:31', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('673', '教室预约修改', '', '修改', null, 'changteachers', '/ReaderLnteraction/changteachers', null, '2', '0303-0304-0649-0673', '修改', '读者预约-预约列表-教室预约-修改', null, 'admin/reservation/detail,admin/reservation/getReservationApplyParam,admin/reservation/filterTagList,admin/reservation/change', '649', '4', '', '413', '2', '1', '2023-08-23 11:15:11', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('674', '教室预约特殊日期排班表', '', '特殊日期排班表', null, 'teachersSpecialview', '/ReaderLnteraction/teachersSpecialview', null, '2', '0303-0304-0649-0674', '特殊日期排班表', '读者预约-预约列表-教室预约-特殊日期排班表', null, 'admin/reservationSpecialSchedule/list', '649', '4', '', '414', '2', '1', '2023-08-23 11:19:57', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('675', '教室预约人员', '', '预约人员', null, 'classroomPersonnel', '/ReaderLnteraction/classroomPersonnel', null, '2', '0303-0304-0649-0675', '预约人员', '读者预约-预约列表-教室预约-预约人员', null, 'admin/reservationApply/list,admin/reservation/detail', '649', '4', '', '418', '2', '1', '2023-08-23 11:20:40', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('676', '教室预约-特殊日期排班表-新增时间段', '', null, null, null, null, null, '3', '0303-0304-0649-0674-0676', '新增时间段', '读者预约-预约列表-教室预约-特殊日期排班表-教室预约-特殊日期排班表-新增时间段', null, 'admin/reservationSpecialSchedule/add', '674', '5', '', '415', '2', '1', '2023-08-23 11:21:07', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('677', '教室预约-特殊日期排班表-修改', '', null, null, null, null, null, '3', '0303-0304-0649-0674-0677', '修改', '读者预约-预约列表-教室预约-特殊日期排班表-教室预约-特殊日期排班表-修改', null, 'admin/reservationSpecialSchedule/detail,admin/reservationSpecialSchedule/change', '674', '5', '', '416', '2', '1', '2023-08-23 11:21:39', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('678', '教室预约-特殊日期排班表-删除', '', null, null, null, null, null, '3', '0303-0304-0649-0674-0678', '删除', '读者预约-预约列表-教室预约-特殊日期排班表-教室预约-特殊日期排班表-删除', null, 'admin/reservationSpecialSchedule/del', '674', '5', '', '417', '2', '1', '2023-08-23 11:22:07', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('679', '教室预约-预约人员-取消预约', '', null, null, null, null, null, '3', '0303-0304-0649-0675-0679', '取消预约', '读者预约-预约列表-教室预约-预约人员-教室预约-预约人员-取消预约', null, 'admin/reservationApply/applyCancel', '675', '5', '', '419', '2', '1', '2023-08-23 11:22:41', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('680', '教室预约-删除', '', null, null, null, null, null, '3', '0303-0304-0649-0680', '删除', '读者预约-预约列表-教室预约-教室预约-删除', null, 'admin/reservation/del', '649', '4', '', '427', '2', '1', '2023-08-23 11:27:32', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('681', '教室预约-预约人员-同意', '', null, null, null, null, null, '3', '0303-0304-0649-0675-0681', '同意', '读者预约-预约列表-教室预约-预约人员-教室预约-预约人员-同意', null, 'admin/reservationApply/agreeAndRefused', '675', '5', '', '420', '2', '1', '2023-08-23 11:28:06', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('682', '教室预约-预约人员-拒绝', '', null, null, null, null, null, '3', '0303-0304-0649-0675-0682', '拒绝', '读者预约-预约列表-教室预约-预约人员-教室预约-预约人员-拒绝', null, 'admin/reservationApply/agreeAndRefused', '675', '5', '', '421', '2', '1', '2023-08-23 11:28:32', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('683', '教室预约-预约人员-预约信息', '', null, null, null, null, null, '3', '0303-0304-0649-0675-0683', '预约信息', '读者预约-预约列表-教室预约-预约人员-教室预约-预约人员-预约信息', null, 'admin/reservationApply/list', '675', '5', '', '422', '2', '1', '2023-08-23 11:31:53', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('684', '教室预约-预约人员-用户违规', '', null, null, null, null, null, '3', '0303-0304-0649-0675-0684', '用户违规', '读者预约-预约列表-教室预约-预约人员-教室预约-预约人员-用户违规', null, 'admin/reservationApply/violateAndCancel', '675', '5', '', '423', '2', '1', '2023-08-23 11:32:21', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('685', '创建展览预约', '', '创建预约', null, 'addteachers', '/ReaderLnteraction/addteachers', null, '2', '0303-0304-0650-0685', '创建预约', '读者预约-预约列表-展览预约-创建预约', null, 'admin/reservation/add,admin/reservation/filterTagList,admin/reservation/getReservationApplyParam', '650', '4', '', '429', '2', '1', '2023-08-23 11:39:25', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('686', '展览预约-发布', '', null, null, null, null, null, '3', '0303-0304-0650-0686', '发布', '读者预约-预约列表-展览预约-展览预约-发布', null, 'admin/reservation/playAndCancel', '650', '4', '', '430', '2', '1', '2023-08-23 11:40:16', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('687', '展览预约-撤销', '', null, null, null, null, null, '3', '0303-0304-0650-0687', '撤销', '读者预约-预约列表-展览预约-展览预约-撤销', null, 'admin/reservation/playAndCancel', '650', '4', '', '431', '2', '1', '2023-08-23 11:40:37', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('688', '展览预约-详情', '', null, null, null, null, null, '3', '0303-0304-0650-0688', '详情', '读者预约-预约列表-展览预约-展览预约-详情', null, 'admin/reservation/detail', '650', '4', '', '432', '2', '1', '2023-08-23 11:40:58', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('689', '展览预约修改', '', '修改', null, 'changteachers', '/ReaderLnteraction/changteachers', null, '2', '0303-0304-0650-0689', '修改', '读者预约-预约列表-展览预约-修改', null, 'admin/reservation/detail,admin/reservation/getReservationApplyParam,admin/reservation/filterTagList,admin/reservation/change', '650', '4', '', '433', '2', '1', '2023-08-23 11:41:33', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('690', '展览预约删除', '', null, null, null, null, null, '3', '0303-0304-0650-0690', '删除', '读者预约-预约列表-展览预约-展览预约删除', null, 'admin/reservation/del', '650', '4', '', '434', '2', '1', '2023-08-23 11:42:17', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('691', '展览预约特殊日期排班表', '', '特殊日期排班表', null, 'teachersSpecialview', '/ReaderLnteraction/teachersSpecialview', null, '2', '0303-0304-0650-0691', '特殊日期排班表', '读者预约-预约列表-展览预约-特殊日期排班表', null, 'admin/reservationSpecialSchedule/list', '650', '4', '', '435', '2', '1', '2023-08-23 11:43:01', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('692', '展览预约人员', '', '预约人员', null, 'exhibitPersonnel', '/ReaderLnteraction/exhibitPersonnel', null, '2', '0303-0304-0650-0692', '预约人员', '读者预约-预约列表-展览预约-预约人员', null, 'admin/reservationApply/list,admin/reservation/detail', '650', '4', '', '439', '2', '1', '2023-08-23 11:44:10', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('693', '展览预约-特殊日期排班表-新增时间段', '', null, null, null, null, null, '3', '0303-0304-0650-0691-0693', '新增时间段', '读者预约-预约列表-展览预约-特殊日期排班表-展览预约-特殊日期排班表-新增时间段', null, 'admin/reservationSpecialSchedule/add', '691', '5', '', '436', '2', '1', '2023-08-23 11:44:42', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('694', '展览预约-特殊日期排班表-修改', '', null, null, null, null, null, '3', '0303-0304-0650-0691-0694', '修改', '读者预约-预约列表-展览预约-特殊日期排班表-展览预约-特殊日期排班表-修改', null, 'admin/reservationSpecialSchedule/detail,admin/reservationSpecialSchedule/change', '691', '5', '', '437', '2', '1', '2023-08-23 11:45:13', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('695', '展览预约-特殊日期排班表-删除', '', null, null, null, null, null, '3', '0303-0304-0650-0691-0695', '删除', '读者预约-预约列表-展览预约-特殊日期排班表-展览预约-特殊日期排班表-删除', null, 'admin/reservationSpecialSchedule/del', '691', '5', '', '438', '2', '1', '2023-08-23 11:47:37', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('696', '展览预约-预约人员-取消预约', '', null, null, null, null, null, '3', '0303-0304-0650-0692-0696', '取消预约', '读者预约-预约列表-展览预约-预约人员-展览预约-预约人员-取消预约', null, 'admin/reservationApply/applyCancel', '692', '5', '', '440', '2', '1', '2023-08-23 11:48:01', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('697', '展览预约-预约人员-同意', '', null, null, null, null, null, '3', '0303-0304-0324-0331-0697', '同意', '读者预约-预约列表-座位预约-预约人员-展览预约-预约人员-同意', null, 'admin/reservationApply/agreeAndRefused', '331', '5', '', '356', '2', '1', '2023-08-23 11:48:34', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('698', '展览预约-预约人员-拒绝', '', null, null, null, null, null, '3', '0303-0304-0650-0692-0698', '拒绝', '读者预约-预约列表-展览预约-预约人员-展览预约-预约人员-拒绝', null, 'admin/reservationApply/agreeAndRefused', '692', '5', '', '441', '2', '1', '2023-08-23 11:49:19', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('699', '展览预约-预约人员-预约信息', '', null, null, null, null, null, '3', '0303-0304-0650-0692-0699', '预约信息', '读者预约-预约列表-展览预约-预约人员-展览预约-预约人员-预约信息', null, 'admin/reservationApply/list', '692', '5', '', '442', '2', '1', '2023-08-23 11:49:46', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('700', '展览预约-预约人员-用户违规', '', null, null, null, null, null, '3', '0303-0304-0650-0692-0700', '用户违规', '读者预约-预约列表-展览预约-预约人员-展览预约-预约人员-用户违规', null, 'admin/reservationApply/violateAndCancel', '692', '5', '', '443', '2', '1', '2023-08-23 11:50:11', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('701', '费用缴纳', '', '费用缴纳', 'paymentFees', null, null, null, '1', '0701', null, '费用缴纳', null, '', '0', '1', '', '277', '2', '1', '2023-08-23 17:59:38', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('702', '到馆预约人员-签到打卡', '', null, null, null, null, null, '3', '0303-0304-0305-0312-0702', '签到打卡', '读者预约-预约列表-预约列表-预约人员-到馆预约人员-签到打卡', null, 'admin/reservationApply/applySignIn', '312', '5', '', '331', '2', '1', '2023-08-24 10:21:51', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('703', '到馆预约人员-签退打卡', '', null, null, null, null, null, '3', '0303-0304-0305-0312-0703', '签退打卡', '读者预约-预约列表-预约列表-预约人员-到馆预约人员-签退打卡', null, 'admin/reservationApply/applySignIn', '312', '5', '', '332', '2', '1', '2023-08-24 10:22:28', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('704', '到馆预约人员-签到/签退', '', null, null, null, null, null, '3', '0303-0304-0305-0312-0704', '签到/签退', '读者预约-预约列表-预约列表-预约人员-到馆预约人员-签到/签退', null, 'admin/reservationApply/applySignIn', '312', '5', '', '333', '2', '1', '2023-08-24 10:30:01', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('705', '师资预约人员-签到/签退', '', null, null, null, null, null, '3', '0303-0304-0629-0636-0705', '签到/签退', '读者预约-预约列表-师资预约-预约人员-师资预约人员-签到/签退', null, 'admin/reservationApply/applySignIn', '636', '5', '', '382', '2', '1', '2023-08-24 10:31:53', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('706', '师资预约人员-签到打卡', '', null, null, null, null, null, '3', '0303-0304-0629-0636-0706', '签到打卡', '读者预约-预约列表-师资预约-预约人员-师资预约人员-签到打卡', null, 'admin/reservationApply/applySignIn', '636', '5', '', '383', '2', '1', '2023-08-24 10:32:25', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('707', '设备预约人员-签到打卡', '', null, null, null, null, null, '3', '0303-0304-0648-0658-0707', '签到打卡', '读者预约-预约列表-设备预约-预约人员-设备预约人员-签到打卡', null, 'admin/reservationApply/applySignIn', '658', '5', '', '404', '2', '1', '2023-08-24 10:33:07', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('708', '设备预约人员-签到/签退', '', null, null, null, null, null, '3', '0303-0304-0648-0658-0708', '签到/签退', '读者预约-预约列表-设备预约-预约人员-设备预约人员-签到/签退', null, 'admin/reservationApply/applySignIn', '658', '5', '', '405', '2', '1', '2023-08-24 10:33:33', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('709', '教室预约人员-签到/签退', '', null, null, null, null, null, '3', '0303-0304-0305-0312-0709', '签到/签退', '读者预约-预约列表-预约列表-预约人员-教室预约人员-签到/签退', null, 'admin/reservationApply/applySignIn', '312', '5', '', '334', '2', '1', '2023-08-24 10:35:12', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('710', '教室预约人员-签到打卡', '', null, null, null, null, null, '3', '0303-0304-0649-0675-0710', '签到打卡', '读者预约-预约列表-教室预约-预约人员-教室预约人员-签到打卡', null, 'admin/reservationApply/applySignIn', '675', '5', '', '424', '2', '1', '2023-08-24 10:35:48', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('711', '展览预约人员-签到/签退', '', null, null, null, null, null, '3', '0303-0304-0650-0692-0711', '签到/签退', '读者预约-预约列表-展览预约-预约人员-展览预约人员-签到/签退', null, 'admin/reservationApply/applySignIn', '692', '5', '', '444', '2', '1', '2023-08-24 10:36:50', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('712', '展览预约人员-签到打卡', '', null, null, null, null, null, '3', '0303-0304-0650-0692-0712', '签到打卡', '读者预约-预约列表-展览预约-预约人员-展览预约人员-签到打卡', null, 'admin/reservationApply/applySignIn', '692', '5', '', '445', '2', '1', '2023-08-24 10:37:22', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('713', '座位预约人员-签到/签退', '', null, null, null, null, null, '3', '0303-0304-0324-0331-0713', '签到/签退', '读者预约-预约列表-座位预约-预约人员-座位预约人员-签到/签退', null, 'admin/reservationApply/applySignIn', '331', '5', '', '357', '2', '1', '2023-08-24 10:38:11', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('714', '活动列表-人员管理-签到/签退', '', null, null, null, null, null, '3', '0001-0002-0017-0714', '签到/签退', '活动报名-活动列表-人员管理-活动列表-人员管理-签到/签退', null, 'admin/activity/sign', '17', '4', '', '23', '2', '1', '2023-08-24 10:58:16', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('715', '空间预约人员-签到/签退', '', null, null, null, null, null, '3', '0350-0351-0358-0715', '签到/签退', '自习室预约-预约列表-预约人员-空间预约人员-签到/签退', null, 'admin/studyRoomReservationApply/applySignIn', '358', '4', '', '465', '2', '1', '2023-08-24 11:05:41', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('716', '服务提醒通知', '', '服务提醒通知', 'menu-note', 'serviceReminder', '/SystemSettings/serviceReminder', null, '1', '0589-0716', null, '系统设置-服务提醒通知', null, 'admin/serviceNotice/list', '589', '2', '', '702', '2', '1', '2023-08-24 14:34:41', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('717', '添加通知', '', null, null, null, null, null, '3', '0589-0716-0717', '添加通知', '系统设置-服务提醒通知-添加通知', null, 'admin/serviceNotice/add', '716', '3', '', '703', '2', '1', '2023-08-25 10:11:09', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('718', '修改通知', '', null, null, null, null, null, '3', '0589-0716-0718', '修改', '系统设置-服务提醒通知-修改通知', null, 'admin/serviceNotice/change,admin/serviceNotice/detail', '716', '3', '', '704', '2', '1', '2023-08-25 10:11:31', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('719', '立即发送', '', null, null, null, null, null, '3', '0589-0716-0719', '立即发送', '系统设置-服务提醒通知-立即发送', null, 'admin/serviceNotice/send', '716', '3', '', '705', '2', '1', '2023-08-25 10:13:16', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('720', '通知删除', '', null, null, null, null, null, '3', '0589-0716-0720', '删除', '系统设置-服务提醒通知-通知删除', null, 'admin/serviceNotice/del', '716', '3', '', '706', '2', '1', '2023-08-25 10:13:38', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('721', '通知操作', '', '通知操作', null, 'noticeFrom', '/SystemSettings/serviceReminder/noticeFrom', null, '2', '0589-0716-0721', null, '系统设置-服务提醒通知-通知操作', null, 'admin/serviceNotice/add,admin/serviceNotice/detail,admin/serviceNotice/change', '716', '3', '', '707', '2', '1', '2023-08-25 10:14:35', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('722', '师资预约人员-导出列表', '', null, null, null, null, null, '3', '0303-0304-0629-0636-0722', '导出列表', '读者预约-预约列表-师资预约-预约人员-师资预约人员-导出列表', null, 'admin/reservationApplyExport/index', '636', '5', '', '384', '2', '1', '2023-08-28 11:33:30', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('723', '设备预约人员-导出列表', '', null, null, null, null, null, '3', '0303-0304-0648-0658-0723', '导出列表', '读者预约-预约列表-设备预约-预约人员-设备预约人员-导出列表', null, 'admin/reservationApplyExport/index', '658', '5', '', '406', '2', '1', '2023-08-28 11:34:06', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('724', '教室预约人员-导出列表', '', null, null, null, null, null, '3', '0303-0304-0649-0675-0724', '导出列表', '读者预约-预约列表-教室预约-预约人员-教室预约人员-导出列表', null, 'admin/reservationApplyExport/index', '675', '5', '', '425', '2', '1', '2023-08-28 11:35:28', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('725', '展览预约人员-导出列表', '', null, null, null, null, null, '3', '0303-0304-0650-0692-0725', '导出列表', '读者预约-预约列表-展览预约-预约人员-展览预约人员-导出列表', null, 'admin/reservationApplyExport/index', '692', '5', '', '446', '2', '1', '2023-08-28 11:36:03', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('726', '师资预约人员-签退打卡', '', null, null, null, null, null, '3', '0303-0304-0629-0636-0726', '签退打卡', '读者预约-预约列表-师资预约-预约人员-师资预约人员-签退打卡', null, 'admin/reservationApply/applySignIn', '636', '5', '', '385', '2', '1', '2023-08-29 10:16:21', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('727', '设备预约人员-签退打卡', '', null, null, null, null, null, '3', '0303-0304-0648-0658-0727', '签退打卡', '读者预约-预约列表-设备预约-预约人员-设备预约人员-签退打卡', null, 'admin/reservationApply/applySignIn', '658', '5', '', '407', '2', '1', '2023-08-29 10:16:59', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('728', '教室预约人员-签退打卡', '', null, null, null, null, null, '3', '0303-0304-0649-0675-0728', '签退打卡', '读者预约-预约列表-教室预约-预约人员-教室预约人员-签退打卡', null, 'admin/reservationApply/applySignIn', '675', '5', '', '426', '2', '1', '2023-08-29 10:17:31', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('729', '展览预约人员-签退打卡', '', null, null, null, null, null, '3', '0303-0304-0650-0692-0729', '签退打卡', '读者预约-预约列表-展览预约-预约人员-展览预约人员-签退打卡', null, 'admin/reservationApply/applySignIn', '692', '5', '', '447', '2', '1', '2023-08-29 10:18:03', '2023-09-14 17:54:38');
INSERT INTO `re_permission` VALUES ('730', '视频直播', '', '视频直播', 'livevideo', 'megagameListVideo', '/liveVideo/megagameList', null, '1', '0033-0730', null, '悦读互动-视频直播', null, 'admin/videoLive/list', '33', '2', '', '221', '2', '1', '2023-08-30 09:53:38', '2023-08-30 15:21:22');
INSERT INTO `re_permission` VALUES ('731', '创建直播', '', '创建视频直播', null, 'addmegagameVideo', '/liveVideo/megagameList/add', null, '2', '0033-0730-0731', '创建直播', '悦读互动-视频直播-创建视频直播', null, 'admin/videoLive/add', '730', '3', '', '223', '2', '1', '2023-08-30 11:06:57', '2023-08-31 13:21:13');
INSERT INTO `re_permission` VALUES ('732', '修改直播', '', '修改视频直播', null, 'changmegagameVideo', '/liveVideo/megagameList/chang', null, '2', '0033-0730-0732', '修改', '悦读互动-视频直播-修改视频直播', null, 'admin/videoLive/change,admin/videoLive/detail', '730', '3', '', '224', '2', '1', '2023-08-30 11:17:36', '2023-08-31 13:21:13');
INSERT INTO `re_permission` VALUES ('733', '视频管理', '', '视频管理', null, 'entryVideo', '/liveVideo/megagameList/management/entry', null, '2', '0033-0730-0733', '视频管理', '悦读互动-视频直播-视频管理', null, 'admin/videoLive/detail', '730', '3', '', '691', '2', '1', '2023-08-30 16:09:44', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('734', '视频回调地址', '', null, null, null, null, null, '3', '0033-0730-0734', '视频回调地址', '悦读互动-视频直播-视频回调地址', null, 'admin/videoLive/getVideoCallbackAddress,admin/videoLive/setVideoCallbackAddress', '730', '3', '', '691', '2', '1', '2023-08-31 13:17:27', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('735', '视频直播删除', '', null, null, null, null, null, '3', '0033-0730-0735', '删除', '悦读互动-视频直播-视频直播删除', null, 'admin/videoLive/del', '730', '3', '', '691', '2', '1', '2023-08-31 13:18:36', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('736', '视频直播-发布', '', null, null, null, null, null, '3', '0033-0730-0736', '发布', '悦读互动-视频直播-视频直播-发布', null, 'admin/videoLive/cancelAndRelease', '730', '3', '', '691', '2', '1', '2023-08-31 13:19:29', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('737', '视频直播-撤销', '', null, null, null, null, null, '3', '0033-0730-0737', '撤销', '悦读互动-视频直播-视频直播-撤销', null, 'admin/videoLive/cancelAndRelease', '730', '3', '', '691', '2', '1', '2023-08-31 13:19:51', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('738', '特殊日期排班表', '', '特殊日期排班表', null, 'selfstudyPup', '/SelfstudyRoom/selfstudyList/addPup', null, '2', '0350-0351-0738', '特殊日期排班表', '空间预约-预约列表-特殊日期排班表', null, 'admin/reservationSpecialSchedule/detail,admin/reservationSpecialSchedule/del,admin/reservationSpecialSchedule/change,admin/reservationSpecialSchedule/detail,admin/reservationSpecialSchedule/list', '351', '3', '', '691', '2', '1', '2023-09-04 13:38:39', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('739', '新增时间段', '', null, null, null, null, null, '3', '0350-0351-0738-0739', '新增时间段', '空间预约-预约列表-特殊日期排班表-新增时间段', null, 'admin/reservationSpecialSchedule/detail,admin/reservationSpecialSchedule/del,admin/reservationSpecialSchedule/change,admin/reservationSpecialSchedule/add,admin/reservationSpecialSchedule/list', '738', '4', '', '691', '2', '1', '2023-09-04 13:52:15', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('740', '空间预约-特殊日期排班表-修改', '', null, null, null, null, null, '3', '0350-0351-0738-0740', '修改', '空间预约-预约列表-特殊日期排班表-空间预约-特殊日期排班表-修改', null, 'admin/reservationSpecialSchedule/detail,admin/reservationSpecialSchedule/del,admin/reservationSpecialSchedule/change,admin/reservationSpecialSchedule/add,admin/reservationSpecialSchedule/list', '738', '4', '', '691', '2', '1', '2023-09-04 13:54:29', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('741', '空间预约-排班表-删除', '', null, null, null, null, null, '3', '0350-0351-0738-0741', '删除', '空间预约-预约列表-特殊日期排班表-空间预约-排班表-删除', null, 'admin/reservationSpecialSchedule/detail,admin/reservationSpecialSchedule/del,admin/reservationSpecialSchedule/change,admin/reservationSpecialSchedule/add,admin/reservationSpecialSchedule/list', '738', '4', '', '691', '2', '1', '2023-09-04 13:55:16', '2023-09-21 11:32:28');
INSERT INTO `re_permission` VALUES ('742', '应用邀请码', '', '应用邀请码', 'none', null, null, null, '1', '0742', null, '应用邀请码', null, '', '0', '1', '', '691', '2', '1', '2023-10-13 16:59:39', '2023-10-13 16:59:39');
INSERT INTO `re_permission` VALUES ('743', '邀请码列表', '', '邀请码列表', 'none', 'inviteCode', '/inviteCode/codeList', null, '1', '0742-0743', null, '应用邀请码-邀请码列表', null, '', '742', '2', '', '692', '2', '1', '2023-10-13 17:02:20', '2023-10-13 17:16:54');
INSERT INTO `re_permission` VALUES ('744', '数据统计', '', '数据统计', 'none', 'inviteCodeEchart', '/inviteCode/echart', null, '1', '0742-0744', null, '应用邀请码-数据统计', null, '', '742', '2', '', '692', '2', '1', '2023-10-13 17:03:52', '2023-10-13 17:17:09');
INSERT INTO `re_permission` VALUES ('745', '编辑邀请码', '', '编辑邀请码', null, 'inviteCodeRedact', '/inviteCode/codeList/redactCode', null, '2', '0742-0743-0745', '修改,添加邀请码', '应用邀请码-邀请码列表-编辑邀请码', null, 'admin/appInviteCode/change,admin/appInviteCode/add,admin/appInviteCode/detail', '743', '3', '', '692', '2', '1', '2023-10-13 17:10:28', '2023-10-13 17:17:50');

-- ----------------------------
-- Table structure for `re_picture_live`
-- ----------------------------
DROP TABLE IF EXISTS `re_picture_live`;
CREATE TABLE `re_picture_live` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '线上大赛活动id',
  `title` varchar(100) DEFAULT NULL COMMENT '活动名称',
  `main_img` varchar(120) DEFAULT NULL COMMENT '进入主界面封面图片',
  `img` varchar(120) DEFAULT 'default/default_picture_live.png' COMMENT '封面图片',
  `host_handle` varchar(255) DEFAULT '' COMMENT '主办单位',
  `tel` varchar(30) DEFAULT NULL COMMENT '咨询方式',
  `address` varchar(200) DEFAULT NULL COMMENT '地址',
  `con_start_time` datetime DEFAULT NULL COMMENT '投稿开始时间',
  `con_end_time` datetime DEFAULT NULL COMMENT '投稿结束时间',
  `vote_start_time` datetime DEFAULT NULL COMMENT '投票开始时间',
  `vote_end_time` datetime DEFAULT NULL COMMENT '投票结束时间',
  `is_check` tinyint(1) DEFAULT '1' COMMENT '前台上传照片是否需要审核   1.是   2.否(默认）',
  `vote_way` tinyint(1) DEFAULT NULL COMMENT '投票方式   1  总票数方式   2 每日投票方式',
  `number` int(10) DEFAULT '0' COMMENT '票数',
  `uploads_number` int(11) DEFAULT '0' COMMENT '每个人上传限制张数，后台上传不影响   0 不限制',
  `uploads_way` tinyint(1) DEFAULT '1' COMMENT '上传方式  1 前台后台都可以上传   2  仅支持后台上传  3 仅支持前台上传  ',
  `is_appoint` tinyint(1) DEFAULT '2' COMMENT '是否指定用户上传  1 指定  2 不指定（都可以上传）',
  `intro` text COMMENT '简介',
  `manage_id` int(10) unsigned DEFAULT NULL COMMENT '管理员id',
  `is_play` tinyint(1) DEFAULT '2' COMMENT '是否发布 1.发布 2.未发布    默认1',
  `is_del` tinyint(1) unsigned DEFAULT '1' COMMENT '是否删除   1.正常   2.已删除',
  `browse_num` int(11) DEFAULT '0' COMMENT '浏览量',
  `create_time` datetime DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime DEFAULT NULL COMMENT '编辑时间',
  `theme_color` varchar(10) DEFAULT NULL COMMENT '主题色',
  `watermark_img` varchar(200) DEFAULT 'default/default_watermark_img.png' COMMENT '水印图片',
  `animation` varchar(20) DEFAULT NULL COMMENT '动画',
  `animation_time` int(11) DEFAULT '1' COMMENT '单位秒',
  `qr_code` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL,
  `qr_url` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '二维码链接',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='图片直播活动表';

-- ----------------------------
-- Records of re_picture_live
-- ----------------------------

-- ----------------------------
-- Table structure for `re_picture_live_appoint_user`
-- ----------------------------
DROP TABLE IF EXISTS `re_picture_live_appoint_user`;
CREATE TABLE `re_picture_live_appoint_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '线上大赛作品表',
  `act_id` int(11) DEFAULT NULL COMMENT '直播活动id  0 表示全部活动',
  `user_id` int(10) unsigned DEFAULT NULL COMMENT '用户id',
  `create_time` datetime DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='图片直播指定用户表';

-- ----------------------------
-- Records of re_picture_live_appoint_user
-- ----------------------------

-- ----------------------------
-- Table structure for `re_picture_live_black_user`
-- ----------------------------
DROP TABLE IF EXISTS `re_picture_live_black_user`;
CREATE TABLE `re_picture_live_black_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '线上大赛作品表',
  `act_id` int(11) DEFAULT NULL COMMENT '直播活动id  0 表示全部活动 ，先不判断此值，所有活动通用',
  `user_id` int(10) unsigned DEFAULT NULL COMMENT '用户id',
  `create_time` datetime DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='图片直播黑名单用户表';

-- ----------------------------
-- Records of re_picture_live_black_user
-- ----------------------------

-- ----------------------------
-- Table structure for `re_picture_live_browse`
-- ----------------------------
DROP TABLE IF EXISTS `re_picture_live_browse`;
CREATE TABLE `re_picture_live_browse` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '线上大赛作品表',
  `act_id` int(11) DEFAULT NULL COMMENT '单位',
  `user_id` int(10) unsigned DEFAULT NULL COMMENT '用户id',
  `create_time` datetime DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='图片直播活动浏览过的表';

-- ----------------------------
-- Records of re_picture_live_browse
-- ----------------------------

-- ----------------------------
-- Table structure for `re_picture_live_vote`
-- ----------------------------
DROP TABLE IF EXISTS `re_picture_live_vote`;
CREATE TABLE `re_picture_live_vote` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户投票',
  `act_id` int(10) unsigned DEFAULT NULL COMMENT '线上大赛活动id',
  `works_id` int(10) unsigned DEFAULT NULL COMMENT '作品id',
  `yyy` smallint(4) unsigned DEFAULT NULL COMMENT '年',
  `mmm` varchar(2) DEFAULT NULL,
  `ddd` varchar(2) DEFAULT NULL,
  `date` date DEFAULT NULL COMMENT '时间 2019-08-07',
  `user_id` int(10) unsigned DEFAULT NULL COMMENT '用户id',
  `terminal` varchar(10) DEFAULT NULL COMMENT '投票终端    web   android   ios  wx',
  `create_time` datetime DEFAULT NULL COMMENT '投票时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户投票';

-- ----------------------------
-- Records of re_picture_live_vote
-- ----------------------------

-- ----------------------------
-- Table structure for `re_picture_live_works`
-- ----------------------------
DROP TABLE IF EXISTS `re_picture_live_works`;
CREATE TABLE `re_picture_live_works` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '线上大赛作品表',
  `serial_number` varchar(10) DEFAULT NULL COMMENT '作品编号  大写字母开头   在加8位数字',
  `act_id` int(11) DEFAULT NULL COMMENT '单位',
  `user_id` int(10) unsigned DEFAULT NULL COMMENT '用户id',
  `title` varchar(50) DEFAULT NULL COMMENT '名称',
  `intro` varchar(255) DEFAULT NULL COMMENT '作品简介',
  `cover` varchar(120) DEFAULT NULL COMMENT '作品封面  默认是图片最小版本',
  `img` varchar(120) DEFAULT NULL COMMENT '作品图片地址 node=1',
  `thumb_img` varchar(120) DEFAULT NULL COMMENT '缩略图',
  `width` int(11) DEFAULT '0' COMMENT '宽度  裁剪后',
  `height` int(11) unsigned DEFAULT '0' COMMENT '高度  裁剪后',
  `size` int(11) DEFAULT NULL COMMENT '原图大小  单位 字节',
  `ratio` varchar(20) DEFAULT NULL COMMENT ' 原图尺寸  长x宽',
  `browse_num` int(10) unsigned DEFAULT '0' COMMENT '浏览量',
  `vote_num` int(10) unsigned DEFAULT '0' COMMENT '投票量',
  `status` tinyint(1) unsigned DEFAULT '3' COMMENT '状态    1.已通过   2.未通过   3.未审核(默认)  4.已撤销， 5已删除（用户自己删除） 6.已违规',
  `reason` varchar(300) DEFAULT NULL COMMENT '拒绝理由',
  `way` tinyint(1) DEFAULT '1' COMMENT '上传方式  1 前台上传  2 后台上传',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1正常  2 删除   后台删除',
  `create_time` datetime DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `serial_number` (`serial_number`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='图片直播图片表';

-- ----------------------------
-- Records of re_picture_live_works
-- ----------------------------

-- ----------------------------
-- Table structure for `re_problem_feedback`
-- ----------------------------
DROP TABLE IF EXISTS `re_problem_feedback`;
CREATE TABLE `re_problem_feedback` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `problem_id` int(10) DEFAULT NULL COMMENT '问题id',
  `content` varchar(1000) DEFAULT NULL COMMENT '返回其他内容',
  `case_guid` char(32) DEFAULT NULL COMMENT '书柜guid',
  `user_guid` char(32) DEFAULT NULL COMMENT '用户guid',
  `status` tinyint(1) DEFAULT '3' COMMENT '状态   1 已完成  2 处理中 3 未处理',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='问题反馈';

-- ----------------------------
-- Records of re_problem_feedback
-- ----------------------------

-- ----------------------------
-- Table structure for `re_recommend_book`
-- ----------------------------
DROP TABLE IF EXISTS `re_recommend_book`;
CREATE TABLE `re_recommend_book` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `book_name` varchar(100) DEFAULT NULL COMMENT '书名      书名+ISBN 确定唯一性',
  `author` varchar(100) DEFAULT NULL COMMENT '作者',
  `isbn` varchar(20) DEFAULT NULL COMMENT 'ISBN号',
  `number` int(11) DEFAULT '1' COMMENT '荐购次数 ',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1待审核 2审核通过 3审核不通过   4 已购买 5 购买失败默认1',
  `reason` varchar(300) DEFAULT NULL COMMENT '拒绝理由',
  `create_time` datetime DEFAULT NULL COMMENT '第一次创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改（购买）时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户荐购书籍表---推荐购买';

-- ----------------------------
-- Records of re_recommend_book
-- ----------------------------

-- ----------------------------
-- Table structure for `re_recommend_user`
-- ----------------------------
DROP TABLE IF EXISTS `re_recommend_user`;
CREATE TABLE `re_recommend_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `recom_id` int(11) DEFAULT NULL COMMENT '荐购id',
  `book_name` varchar(100) DEFAULT NULL COMMENT '书名      书名+ISBN 确定唯一性',
  `author` varchar(100) DEFAULT NULL COMMENT '作者',
  `press` varchar(60) DEFAULT NULL COMMENT '出版社',
  `pre_time` varchar(20) DEFAULT NULL COMMENT '出版时间',
  `isbn` varchar(20) DEFAULT NULL COMMENT 'ISBN号',
  `price` decimal(5,2) DEFAULT NULL COMMENT '金额',
  `img` varchar(120) DEFAULT NULL COMMENT '封面图片',
  `intro` text COMMENT '简介',
  `create_time` datetime DEFAULT NULL COMMENT '第一次创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改（购买）时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户荐购表---推荐购买';

-- ----------------------------
-- Records of re_recommend_user
-- ----------------------------

-- ----------------------------
-- Table structure for `re_register_verify`
-- ----------------------------
DROP TABLE IF EXISTS `re_register_verify`;
CREATE TABLE `re_register_verify` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(32) DEFAULT NULL COMMENT '验证码',
  `phone` varchar(20) DEFAULT NULL COMMENT '电话号码',
  `token` varchar(32) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `expir_time` datetime DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of re_register_verify
-- ----------------------------

-- ----------------------------
-- Table structure for `re_reservation`
-- ----------------------------
DROP TABLE IF EXISTS `re_reservation`;
CREATE TABLE `re_reservation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `node` tinyint(1) DEFAULT '0' COMMENT '分类  1  人物    2  物品    3 教室   4 展览预约    5 到馆预约 6 座位预约',
  `kind` tinyint(1) DEFAULT '1' COMMENT '物品种类   1 单一物品  2 多物品（物品默认一个可预约多个，其他都只能预约一个）',
  `img` varchar(255) DEFAULT NULL COMMENT '图片',
  `name` varchar(30) DEFAULT NULL COMMENT '名称',
  `unit` varchar(255) DEFAULT NULL COMMENT '物品所属单位（node为2才有）',
  `type_tag` varchar(200) DEFAULT NULL COMMENT '补充类型(多个|连接)',
  `intro` text COMMENT '简介',
  `province` varchar(50) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `district` varchar(30) DEFAULT NULL,
  `street` varchar(30) DEFAULT NULL COMMENT '街道',
  `address` varchar(255) DEFAULT NULL COMMENT '详细地址',
  `contacts` varchar(30) DEFAULT '' COMMENT '联系人姓名',
  `tel` varchar(20) DEFAULT '' COMMENT '联系方式',
  `number` int(11) DEFAULT '1' COMMENT '预约量 默认1   多物品预约才有  座位预约，这里为座位量',
  `apply_number` int(11) DEFAULT '0' COMMENT '总预约次数',
  `is_reader` tinyint(1) NOT NULL DEFAULT '2' COMMENT '报名是否需要绑定读者证  1 是  2 否',
  `education` varchar(20) DEFAULT NULL COMMENT '人物学历（node为1才有）',
  `clock_time` int(11) DEFAULT '30' COMMENT '预约开始后，多少分钟之内可打卡  单位分钟  打卡限制时间 用户预约时间段为9:00-12:00，当前设置为20分钟内如果用户在9:00之前预约，则用户打卡有效时间为9:00-9:20',
  `serial_prefix` varchar(1) DEFAULT NULL COMMENT '座位预约编号前缀',
  `day_make_time` time DEFAULT NULL COMMENT '每日开始预约时间，这个时间之前不能预约',
  `display_day` tinyint(2) DEFAULT '7' COMMENT '前台展示 天数  1 ~ 30',
  `limit_num` int(11) DEFAULT '1' COMMENT '当前借阅（未使用）次数   1 ~ 30',
  `is_sign` tinyint(1) DEFAULT '1' COMMENT '是否需要签到 1需要 2不需要',
  `qr_code` varchar(15) DEFAULT '' COMMENT '二维码code',
  `qr_url` varchar(150) DEFAULT NULL COMMENT '预约二维码引用链接',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除   默认 1 正常   2 已删除',
  `is_play` tinyint(1) DEFAULT '1' COMMENT '是否发布  1.发布  2.未发布  默认2',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员id',
  `lon` varchar(50) DEFAULT NULL COMMENT '经度',
  `lat` varchar(50) DEFAULT NULL COMMENT '纬度',
  `is_approval` tinyint(1) DEFAULT NULL COMMENT '预约是否需要审核 1需要 2不需要',
  `is_cancel` tinyint(1) DEFAULT NULL COMMENT '是否可以取消 1是 2否',
  `cancel_end_time` int(11) DEFAULT NULL COMMENT '用户可取消活动预约的最后期限(以活动开始时间往前计算）  单位分钟 0表示不限制',
  `start_age` mediumint(3) DEFAULT '0' COMMENT '开始年龄  默认0   两个都是 0 则表示不需要年龄限制',
  `end_age` mediumint(3) DEFAULT '0' COMMENT '结束年龄  默认0',
  `astrict_sex` tinyint(1) DEFAULT '3' COMMENT '性别限制   1.限男性  2.限女性  3.不限(默认)',
  `is_real` tinyint(1) DEFAULT '2' COMMENT '是否需要用户真实信息 1需要 2不需要 默认2',
  `real_info` varchar(255) DEFAULT NULL COMMENT '需要验证的真实信息id用|连接起来    1、姓名   2 、身份证号码  3、电话号码   4、读者证号码    5、真实人物头像   6、备注  7 单位名称 8 性别 9 年龄  10、附件 ',
  `make_max_num` int(11) DEFAULT '1' COMMENT '设备限制一次预约个数',
  `sign_way` tinyint(1) DEFAULT '2' COMMENT '扫码方式  1 、2者都可以  2、用户自主扫码  3、管理员设备扫码  ',
  PRIMARY KEY (`id`,`is_reader`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='文化配送物品预约表';

-- ----------------------------
-- Records of re_reservation
-- ----------------------------

-- ----------------------------
-- Table structure for `re_reservation_apply`
-- ----------------------------
DROP TABLE IF EXISTS `re_reservation_apply`;
CREATE TABLE `re_reservation_apply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证id',
  `reservation_id` int(11) DEFAULT NULL COMMENT '申请的文化配送预约表主键id',
  `schedule_id` int(11) DEFAULT NULL COMMENT '排班id ',
  `seat_id` int(11) DEFAULT NULL COMMENT '座位预约id',
  `make_time` date DEFAULT NULL COMMENT '预约日期',
  `remark` varchar(255) DEFAULT NULL COMMENT '预约备注',
  `status` tinyint(2) DEFAULT '0' COMMENT '预约状态   1.已通过  2.已取消，3待审核，4已拒绝 ,5 已过期 , 6 已签到  7 结束打卡（增对座位预约）',
  `reason` varchar(255) DEFAULT NULL COMMENT '拒绝理由 ',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员',
  `number` int(11) DEFAULT '0' COMMENT '针对多物品    预约的数量   默认0',
  `is_violate` tinyint(1) DEFAULT '1' COMMENT '是否违规  1正常 2违规 默认1',
  `violate_reason` varchar(255) DEFAULT NULL COMMENT '违规原因',
  `violate_time` datetime DEFAULT NULL COMMENT '违规时间',
  `tel` varchar(20) DEFAULT NULL COMMENT '用户电话号码',
  `unit` varchar(50) DEFAULT NULL COMMENT '用户填写单位信息',
  `id_card` varchar(50) DEFAULT NULL,
  `sex` tinyint(1) DEFAULT '0' COMMENT '性别   1 男  2 女',
  `age` int(100) DEFAULT NULL COMMENT '年龄',
  `create_time` datetime DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  `expire_time` datetime DEFAULT NULL COMMENT '签到过期时间 ',
  `sign_time` datetime DEFAULT NULL COMMENT '签到时间',
  `sign_end_time` datetime DEFAULT NULL COMMENT '结束签到时间，座位预约才有',
  `score` int(11) DEFAULT '0' COMMENT '积分 ',
  `reader_id` varchar(30) DEFAULT NULL COMMENT '读者证',
  `img` varchar(120) DEFAULT NULL COMMENT '头像',
  `username` varchar(30) DEFAULT NULL COMMENT '姓名',
  `schedule_type` tinyint(1) DEFAULT '1' COMMENT '排版类型 1正常排班 2特殊日期排',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='用户预约表';

-- ----------------------------
-- Records of re_reservation_apply
-- ----------------------------

-- ----------------------------
-- Table structure for `re_reservation_schedule`
-- ----------------------------
DROP TABLE IF EXISTS `re_reservation_schedule`;
CREATE TABLE `re_reservation_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reservation_id` int(11) DEFAULT NULL COMMENT '文化配送预约表主键id',
  `week` tinyint(1) DEFAULT '0' COMMENT '星期   1-7   星期日 为  7 ',
  `start_time` time DEFAULT NULL COMMENT '开始时间  时分秒',
  `end_time` time DEFAULT NULL COMMENT '结束时间   时分秒',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1 正常  2 已删除 ',
  `create_time` datetime DEFAULT NULL COMMENT '数据插入时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='文化配送排班表';

-- ----------------------------
-- Records of re_reservation_schedule
-- ----------------------------

-- ----------------------------
-- Table structure for `re_reservation_seat`
-- ----------------------------
DROP TABLE IF EXISTS `re_reservation_seat`;
CREATE TABLE `re_reservation_seat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reservation_id` int(11) DEFAULT NULL COMMENT '预约id',
  `number` int(11) DEFAULT '1' COMMENT '序号',
  `qr_code` varchar(30) DEFAULT NULL COMMENT '二维码code',
  `qr_url` varchar(150) DEFAULT NULL COMMENT '座位二维码引用地址',
  `is_play` tinyint(1) DEFAULT '1' COMMENT '是否发布 1.发布 2.未发布    默认1',
  `is_del` tinyint(1) DEFAULT '1',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='预约座位管理';

-- ----------------------------
-- Records of re_reservation_seat
-- ----------------------------

-- ----------------------------
-- Table structure for `re_reservation_special_schedule`
-- ----------------------------
DROP TABLE IF EXISTS `re_reservation_special_schedule`;
CREATE TABLE `re_reservation_special_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自动id',
  `reservation_id` int(11) DEFAULT NULL COMMENT '预约id',
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `date` date DEFAULT NULL COMMENT '日期',
  `start_time` time DEFAULT NULL COMMENT '开始时间',
  `end_time` time DEFAULT NULL COMMENT '结束时间',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1 正常  2 已删除  默认1 ',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='文化配送特殊日期排班表';

-- ----------------------------
-- Records of re_reservation_special_schedule
-- ----------------------------

-- ----------------------------
-- Table structure for `re_reservation_type`
-- ----------------------------
DROP TABLE IF EXISTS `re_reservation_type`;
CREATE TABLE `re_reservation_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reservation_id` int(11) DEFAULT NULL COMMENT '文化配送预约表主键id',
  `type_name` varchar(30) DEFAULT NULL COMMENT '标签名称',
  `type_id` int(11) DEFAULT '0' COMMENT '标签来源 1固定标签 2用户自己手动添加',
  `create_time` datetime DEFAULT NULL COMMENT '数据插入时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='文化配送类型表';

-- ----------------------------
-- Records of re_reservation_type
-- ----------------------------

-- ----------------------------
-- Table structure for `re_role`
-- ----------------------------
DROP TABLE IF EXISTS `re_role`;
CREATE TABLE `re_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `manage_id` int(11) DEFAULT NULL COMMENT '创建该角色人员的id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '编辑时间',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除   1.正常(默认)  2.已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='角色表';

-- ----------------------------
-- Records of re_role
-- ----------------------------
INSERT INTO `re_role` VALUES ('1', '管理员', '1', '2023-11-09 13:39:04', '2023-11-09 13:39:04', '1');

-- ----------------------------
-- Table structure for `re_role_permission`
-- ----------------------------
DROP TABLE IF EXISTS `re_role_permission`;
CREATE TABLE `re_role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) DEFAULT '1' COMMENT '1选中权限 2选中权限父级权限',
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  `permission_id` int(11) DEFAULT NULL COMMENT '权限id',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=274 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='角色权限关联表';

-- ----------------------------
-- Records of re_role_permission
-- ----------------------------
INSERT INTO `re_role_permission` VALUES ('1', '1', '1', '589', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('2', '1', '1', '590', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('3', '1', '1', '1', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('4', '1', '1', '2', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('5', '1', '1', '4', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('6', '1', '1', '5', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('7', '1', '1', '6', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('8', '1', '1', '7', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('9', '1', '1', '8', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('10', '1', '1', '10', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('11', '1', '1', '11', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('12', '1', '1', '13', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('13', '1', '1', '14', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('14', '1', '1', '15', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('15', '1', '1', '18', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('16', '1', '1', '20', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('17', '1', '1', '21', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('18', '1', '1', '16', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('19', '1', '1', '22', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('20', '1', '1', '17', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('21', '1', '1', '19', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('22', '1', '1', '23', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('23', '1', '1', '24', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('24', '1', '1', '3', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('25', '1', '1', '9', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('26', '1', '1', '25', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('27', '1', '1', '26', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('28', '1', '1', '27', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('29', '1', '1', '28', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('30', '1', '1', '29', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('31', '1', '1', '30', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('32', '1', '1', '31', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('33', '1', '1', '32', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('34', '1', '1', '33', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('35', '1', '1', '34', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('36', '1', '1', '35', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('37', '1', '1', '37', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('38', '1', '1', '38', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('39', '1', '1', '40', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('40', '1', '1', '41', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('41', '1', '1', '42', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('42', '1', '1', '43', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('43', '1', '1', '44', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('44', '1', '1', '45', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('45', '1', '1', '46', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('46', '1', '1', '47', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('47', '1', '1', '48', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('48', '1', '1', '49', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('49', '1', '1', '50', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('50', '1', '1', '51', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('51', '1', '1', '52', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('52', '1', '1', '53', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('53', '1', '1', '55', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('54', '1', '1', '56', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('55', '1', '1', '57', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('56', '1', '1', '58', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('57', '1', '1', '59', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('58', '1', '1', '60', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('59', '1', '1', '61', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('60', '1', '1', '62', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('61', '1', '1', '63', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('62', '1', '1', '64', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('63', '1', '1', '65', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('64', '1', '1', '67', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('65', '1', '1', '68', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('66', '1', '1', '69', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('67', '1', '1', '70', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('68', '1', '1', '71', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('69', '1', '1', '72', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('70', '1', '1', '73', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('71', '1', '1', '74', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('72', '1', '1', '75', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('73', '1', '1', '76', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('74', '1', '1', '77', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('75', '1', '1', '78', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('76', '1', '1', '80', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('77', '1', '1', '82', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('78', '1', '1', '83', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('79', '1', '1', '79', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('80', '1', '1', '81', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('81', '1', '1', '102', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('82', '1', '1', '103', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('83', '1', '1', '104', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('84', '1', '1', '105', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('85', '1', '1', '106', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('86', '1', '1', '107', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('87', '1', '1', '108', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('88', '1', '1', '109', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('89', '1', '1', '110', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('90', '1', '1', '111', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('91', '1', '1', '112', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('92', '1', '1', '113', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('93', '1', '1', '114', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('94', '1', '1', '115', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('95', '1', '1', '125', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('96', '1', '1', '126', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('97', '1', '1', '127', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('98', '1', '1', '128', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('99', '1', '1', '116', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('100', '1', '1', '117', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('101', '1', '1', '118', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('102', '1', '1', '119', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('103', '1', '1', '120', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('104', '1', '1', '121', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('105', '1', '1', '122', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('106', '1', '1', '123', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('107', '1', '1', '124', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('108', '1', '1', '129', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('109', '1', '1', '130', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('110', '1', '1', '131', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('111', '1', '1', '132', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('112', '1', '1', '133', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('113', '1', '1', '36', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('114', '1', '1', '39', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('115', '1', '1', '54', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('116', '1', '1', '84', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('117', '1', '1', '85', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('118', '1', '1', '86', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('119', '1', '1', '87', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('120', '1', '1', '88', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('121', '1', '1', '89', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('122', '1', '1', '91', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('123', '1', '1', '92', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('124', '1', '1', '93', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('125', '1', '1', '94', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('126', '1', '1', '95', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('127', '1', '1', '96', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('128', '1', '1', '97', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('129', '1', '1', '98', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('130', '1', '1', '99', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('131', '1', '1', '100', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('132', '1', '1', '90', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('133', '1', '1', '66', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('134', '1', '1', '134', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('135', '1', '1', '135', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('136', '1', '1', '136', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('137', '1', '1', '137', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('138', '1', '1', '138', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('139', '1', '1', '139', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('140', '1', '1', '140', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('141', '1', '1', '141', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('142', '1', '1', '142', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('143', '1', '1', '143', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('144', '1', '1', '144', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('145', '1', '1', '145', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('146', '1', '1', '146', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('147', '1', '1', '147', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('148', '1', '1', '148', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('149', '1', '1', '149', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('150', '1', '1', '150', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('151', '1', '1', '151', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('152', '1', '1', '152', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('153', '1', '1', '153', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('154', '1', '1', '154', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('155', '1', '1', '155', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('156', '1', '1', '156', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('157', '1', '1', '157', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('158', '1', '1', '166', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('159', '1', '1', '167', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('160', '1', '1', '168', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('161', '1', '1', '169', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('162', '1', '1', '158', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('163', '1', '1', '159', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('164', '1', '1', '160', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('165', '1', '1', '161', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('166', '1', '1', '162', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('167', '1', '1', '163', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('168', '1', '1', '164', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('169', '1', '1', '165', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('170', '1', '1', '170', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('171', '1', '1', '171', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('172', '1', '1', '172', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('173', '1', '1', '173', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('174', '1', '1', '174', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('175', '1', '1', '175', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('176', '1', '1', '176', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('177', '1', '1', '177', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('178', '1', '1', '178', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('179', '1', '1', '179', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('180', '1', '1', '180', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('181', '1', '1', '181', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('182', '1', '1', '182', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('183', '1', '1', '183', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('184', '1', '1', '184', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('185', '1', '1', '185', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('186', '1', '1', '186', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('187', '1', '1', '187', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('188', '1', '1', '188', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('189', '1', '1', '189', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('190', '1', '1', '190', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('191', '1', '1', '191', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('192', '1', '1', '192', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('193', '1', '1', '193', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('194', '1', '1', '194', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('195', '1', '1', '195', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('196', '1', '1', '196', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('197', '1', '1', '197', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('198', '1', '1', '198', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('199', '1', '1', '199', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('200', '1', '1', '200', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('201', '1', '1', '201', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('202', '1', '1', '202', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('203', '1', '1', '203', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('204', '1', '1', '204', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('205', '1', '1', '205', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('206', '1', '1', '206', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('207', '1', '1', '207', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('208', '1', '1', '208', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('209', '1', '1', '209', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('210', '1', '1', '210', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('211', '1', '1', '211', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('212', '1', '1', '212', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('213', '1', '1', '213', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('214', '1', '1', '214', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('215', '1', '1', '215', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('216', '1', '1', '216', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('217', '1', '1', '217', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('218', '1', '1', '218', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('219', '1', '1', '219', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('220', '1', '1', '220', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('221', '1', '1', '221', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('222', '1', '1', '222', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('223', '1', '1', '223', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('224', '1', '1', '224', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('225', '1', '1', '225', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('226', '1', '1', '226', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('227', '1', '1', '227', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('228', '1', '1', '228', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('229', '1', '1', '547', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('230', '1', '1', '549', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('231', '1', '1', '550', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('232', '1', '1', '551', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('233', '1', '1', '552', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('234', '1', '1', '553', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('235', '1', '1', '554', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('236', '1', '1', '555', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('237', '1', '1', '556', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('238', '1', '1', '557', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('239', '1', '1', '558', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('240', '1', '1', '559', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('241', '1', '1', '560', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('242', '1', '1', '561', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('243', '1', '1', '562', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('244', '1', '1', '563', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('245', '1', '1', '564', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('246', '1', '1', '565', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('247', '1', '1', '566', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('248', '1', '1', '567', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('249', '1', '1', '591', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('250', '1', '1', '592', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('251', '1', '1', '593', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('252', '1', '1', '596', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('253', '1', '1', '598', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('254', '1', '1', '625', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('255', '1', '1', '626', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('256', '1', '1', '627', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('257', '1', '1', '628', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('258', '1', '1', '601', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('259', '1', '1', '602', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('260', '1', '1', '603', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('261', '1', '1', '604', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('262', '1', '1', '605', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('263', '1', '1', '606', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('264', '1', '1', '607', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('265', '1', '1', '610', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('266', '1', '1', '611', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('267', '1', '1', '612', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('268', '1', '1', '613', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('269', '1', '1', '614', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('270', '1', '1', '615', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('271', '1', '1', '616', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('272', '1', '1', '617', '2023-11-09 13:45:31');
INSERT INTO `re_role_permission` VALUES ('273', '1', '1', '618', '2023-11-09 13:45:31');

-- ----------------------------
-- Table structure for `re_scenic`
-- ----------------------------
DROP TABLE IF EXISTS `re_scenic`;
CREATE TABLE `re_scenic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) DEFAULT NULL COMMENT '景点名称',
  `img` varchar(200) DEFAULT NULL COMMENT '景点封面',
  `intro` text COMMENT '景点简介',
  `province` varchar(30) DEFAULT NULL COMMENT '地址，省',
  `city` varchar(30) DEFAULT NULL COMMENT '地址，市',
  `district` varchar(30) DEFAULT NULL COMMENT '地址，区',
  `street` varchar(30) DEFAULT NULL COMMENT '街道',
  `address` varchar(30) DEFAULT NULL COMMENT '地址，详细地址',
  `start_time` datetime DEFAULT NULL COMMENT '作品可上传开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '作品可上传结束时间',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否被删除 1正常  2 已被删除',
  `is_reader` tinyint(1) DEFAULT '2' COMMENT '打卡是否需要绑定读者证  1 是  2 否',
  `is_play` tinyint(1) DEFAULT '2' COMMENT ' 是否发布 1是 2否',
  `lon` varchar(30) DEFAULT NULL COMMENT '经度',
  `lat` varchar(30) DEFAULT NULL COMMENT '纬度',
  `type_id` int(11) DEFAULT NULL COMMENT '类型id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of re_scenic
-- ----------------------------

-- ----------------------------
-- Table structure for `re_scenic_type`
-- ----------------------------
DROP TABLE IF EXISTS `re_scenic_type`;
CREATE TABLE `re_scenic_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(20) DEFAULT NULL COMMENT '类型名称',
  `color` varchar(120) DEFAULT NULL COMMENT '类型图标颜色',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL,
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除 1否 2是',
  `icon` varchar(120) DEFAULT NULL COMMENT '类型图标',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of re_scenic_type
-- ----------------------------

-- ----------------------------
-- Table structure for `re_scenic_works`
-- ----------------------------
DROP TABLE IF EXISTS `re_scenic_works`;
CREATE TABLE `re_scenic_works` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scenic_id` int(11) DEFAULT NULL COMMENT '景点id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证号id',
  `title` varchar(255) DEFAULT NULL COMMENT '作品标题',
  `cover` varchar(200) DEFAULT NULL COMMENT '作品封面',
  `intro` text COMMENT '作品简介',
  `status` tinyint(1) DEFAULT '3' COMMENT '状态    1.已通过   2.未通过   3.未审核(默认)  4.已撤销， 5已删除 ',
  `reason` varchar(500) DEFAULT NULL COMMENT '拒绝原因',
  `browse_num` int(11) DEFAULT '0' COMMENT '浏览量',
  `vote_num` int(11) DEFAULT '0' COMMENT '点赞量',
  `score` int(11) DEFAULT '0' COMMENT '积分',
  `is_look` tinyint(1) DEFAULT '2' COMMENT '是否被查看 1已查看  2未查看',
  `is_violate` tinyint(1) DEFAULT '1' COMMENT '是否违规  1 正常  2 违规',
  `violate_reason` varchar(500) DEFAULT NULL COMMENT '违规原因',
  `violate_time` datetime DEFAULT NULL COMMENT '违规时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of re_scenic_works
-- ----------------------------

-- ----------------------------
-- Table structure for `re_scenic_works_img`
-- ----------------------------
DROP TABLE IF EXISTS `re_scenic_works_img`;
CREATE TABLE `re_scenic_works_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scenic_works_id` int(11) DEFAULT NULL COMMENT '景点打卡作品id',
  `img` varchar(200) DEFAULT NULL COMMENT '原图',
  `thumb_img` varchar(200) DEFAULT NULL,
  `width` int(11) DEFAULT NULL COMMENT '宽',
  `height` int(11) DEFAULT NULL COMMENT '高',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of re_scenic_works_img
-- ----------------------------

-- ----------------------------
-- Table structure for `re_scenic_works_vote`
-- ----------------------------
DROP TABLE IF EXISTS `re_scenic_works_vote`;
CREATE TABLE `re_scenic_works_vote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scenic_id` int(11) DEFAULT NULL COMMENT '景点id',
  `user_id` int(11) DEFAULT NULL,
  `scenic_works_id` int(11) DEFAULT NULL COMMENT '作品id',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of re_scenic_works_vote
-- ----------------------------

-- ----------------------------
-- Table structure for `re_score_info`
-- ----------------------------
DROP TABLE IF EXISTS `re_score_info`;
CREATE TABLE `re_score_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '积分管理id',
  `manage_id` int(11) unsigned DEFAULT NULL COMMENT '管理员id',
  `type` int(10) DEFAULT NULL COMMENT '类型',
  `type_icon` varchar(10) DEFAULT NULL COMMENT '图标显示文字',
  `type_name` varchar(20) DEFAULT NULL COMMENT '类型名称',
  `score` int(10) DEFAULT NULL COMMENT '消耗或获得的积分   0 表示关闭',
  `number` int(11) DEFAULT '1' COMMENT '一天最多几次     0 表示不限',
  `intro` varchar(5000) DEFAULT NULL COMMENT '简介',
  `node` tinyint(1) DEFAULT '3' COMMENT '1 正数  2 负数  3 不限',
  `is_open` tinyint(1) DEFAULT '1' COMMENT '是否开启   1 开启  2 关闭',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除 1正常 2删除',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='积分管理表';

-- ----------------------------
-- Records of re_score_info
-- ----------------------------
INSERT INTO `re_score_info` VALUES ('1', '1', '1', '绑', '读者证首次被绑定', '50', '1', '读者证首次被绑定增加 50 积分', '3', '1', '1', '2018-12-19 16:15:42', '2023-08-28 16:20:43');
INSERT INTO `re_score_info` VALUES ('2', '1', '2', '注', '邀请用户授权注册', '20', '2', '邀请用户授权注册增加 20 积分（每日限制2次）', '3', '1', '1', '2018-12-06 11:24:33', '2023-08-28 15:50:44');
INSERT INTO `re_score_info` VALUES ('3', '1', '3', '邀', '邀请用户授权注册后第一次绑定读者证', '10', '2', '邀请用户授权注册后第一次绑定读者证增加 10 积分（每日限制2次）', '3', '1', '2', '2018-12-06 11:24:33', '2021-05-19 09:11:07');
INSERT INTO `re_score_info` VALUES ('4', '1', '4', '活', '参加线上活动', '20', '1', '参加线上活动增加 20 积分（每日限制1次）', '3', '1', '1', '2018-12-06 15:56:32', '2021-05-19 09:11:22');
INSERT INTO `re_score_info` VALUES ('5', '1', '5', '约', '参加预约', '10', '0', '参加预约增加 10 积分', '3', '1', '2', '2020-10-15 09:32:02', '2023-08-31 10:02:21');
INSERT INTO `re_score_info` VALUES ('6', '1', '6', '景', '景点打卡', '10', '3', '景点打卡增加 10 积分（每日限制3次）', '3', '1', '2', '2020-10-15 09:34:30', '2023-08-31 09:51:00');
INSERT INTO `re_score_info` VALUES ('7', '1', '7', '赛', '线上大赛投稿', '0', '0', '线上大赛投稿增加 0 积分', '3', '2', '2', '2020-11-27 15:10:33', '2023-08-31 10:03:58');
INSERT INTO `re_score_info` VALUES ('8', '1', '8', '借', '借阅书籍', '10', '10', '借阅书籍增加 10 积分（每日限制10次）', '3', '1', '1', '2020-11-27 15:10:33', '2021-05-19 09:15:23');
INSERT INTO `re_score_info` VALUES ('9', '1', '9', '还', '按时还回书籍', '10', '10', '按时还回书籍增加 10 积分（每日限制10次）', '3', '1', '1', '2020-11-27 15:10:33', '2021-05-19 09:15:33');
INSERT INTO `re_score_info` VALUES ('10', '1', '10', '还', '过期还回书籍', '-5', '1', '过期还回书籍减少 5 积分（每日限制1次）', '3', '1', '1', '2020-11-27 15:10:33', '2021-05-18 10:20:57');
INSERT INTO `re_score_info` VALUES ('11', '1', '11', '坏', '还回书籍已损坏', '-10', '1', '还回书籍已损坏减少 10 积分（每日限制1次）', '3', '1', '2', '2020-11-27 15:10:33', '2021-05-18 10:21:02');
INSERT INTO `re_score_info` VALUES ('12', '1', '12', '丢', '书籍已丢失', '-5', '1', '书籍已丢失减少 5 积分（每日限制1次）', '3', '1', '2', '2020-11-27 15:10:33', '2021-05-19 09:13:05');
INSERT INTO `re_score_info` VALUES ('13', '1', '13', '签', '每日签到', '5', '1', '每日签到增加 5 积分', '3', '1', '1', '2020-12-15 18:45:36', '2021-05-19 09:13:29');
INSERT INTO `re_score_info` VALUES ('14', '1', '14', '签', '连续签到7天', '10', '1', '连续签到7天将额外增加 10 积分', '3', '1', '1', '2020-12-21 11:23:22', '2021-05-19 09:13:34');
INSERT INTO `re_score_info` VALUES ('15', '1', '15', '卷', '参加问卷调查', '50', '1', '参加问卷调查增加 50 积分（每日限制1次）', '3', '1', '1', '2021-03-01 11:55:05', '2021-05-19 09:13:43');
INSERT INTO `re_score_info` VALUES ('16', '1', '16', '预', '门禁自习室预约', '20', '1', '门禁自习室预约增加 20 积分（每日限制1次）', '3', '1', '2', '2023-08-03 17:41:43', '2023-08-31 09:49:39');
INSERT INTO `re_score_info` VALUES ('17', '1', '17', '到', '图书到家进行图书借阅', '10', '1', '图书到家进行图书借阅增加10积分（每日限制1次）', '3', '1', '2', '2023-08-07 16:09:52', '2023-08-07 16:10:01');
INSERT INTO `re_score_info` VALUES ('18', '1', '18', '扫', '扫码借阅图书', '10', '1', '扫码借阅图书增加10积分（每日限制1次）', '3', '1', '2', '2023-08-07 16:09:54', '2023-08-07 16:10:04');
INSERT INTO `re_score_info` VALUES ('19', '1', '19', '志', '首次成为志愿者', '5', '1', '首次成为志愿者增加10积分', '3', '1', '1', '2023-09-07 10:34:26', null);
INSERT INTO `re_score_info` VALUES ('20', '1', '20', '签', '活动准时签到', '5', '1', '活动准时签到增加 5 积分（每日限制1次）', '3', '1', '1', '2023-09-07 13:25:52', '2023-09-08 15:21:40');
INSERT INTO `re_score_info` VALUES ('21', '1', '21', '签', '活动迟到签到', '5', '1', '活动迟到签到增加 5 积分（每日限制1次）', '3', '1', '1', '2023-09-07 13:27:13', '2023-09-08 15:22:15');
INSERT INTO `re_score_info` VALUES ('22', '1', '22', '缺', '活动结束未签到', '5', '1', '活动结束未签到增加 5 积分（每日限制1次）', '3', '1', '1', '2023-09-07 13:27:15', '2023-09-08 15:35:55');

-- ----------------------------
-- Table structure for `re_score_log`
-- ----------------------------
DROP TABLE IF EXISTS `re_score_log`;
CREATE TABLE `re_score_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '积分明细id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) unsigned DEFAULT NULL COMMENT '用户id/团长',
  `score` varchar(10) DEFAULT NULL COMMENT '消耗或增加的积分     1 ， -1',
  `total_score` varchar(10) DEFAULT '0' COMMENT '应该扣除的积分  与 socre一致',
  `system_id` int(11) DEFAULT NULL COMMENT '系统消息id',
  `type_id` int(11) DEFAULT '0' COMMENT '类型id    0为商品兑换类型id    0为商品兑换 1000为管理员调整积分',
  `type` varchar(20) DEFAULT NULL COMMENT '类型',
  `intro` varchar(500) DEFAULT NULL COMMENT '说明',
  `manage_id` int(11) DEFAULT NULL COMMENT '积分操作人',
  `create_time` datetime DEFAULT NULL COMMENT '新增时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='积分明细表';

-- ----------------------------
-- Records of re_score_log
-- ----------------------------

-- ----------------------------
-- Table structure for `re_score_rule`
-- ----------------------------
DROP TABLE IF EXISTS `re_score_rule`;
CREATE TABLE `re_score_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '积分管理id',
  `manage_id` int(11) unsigned DEFAULT NULL COMMENT '管理员id',
  `intro` text COMMENT '简介',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='积分规则表';

-- ----------------------------
-- Records of re_score_rule
-- ----------------------------

-- ----------------------------
-- Table structure for `re_service_directory`
-- ----------------------------
DROP TABLE IF EXISTS `re_service_directory`;
CREATE TABLE `re_service_directory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL COMMENT '新闻标题',
  `type` tinyint(1) DEFAULT '1' COMMENT '1.本馆概况 2.开馆时间 3.入馆须知 4.读者须知 5.办证指南 6.馆内导航 7.分馆服务 8.交通指南 9.联系图书馆',
  `img` varchar(200) DEFAULT NULL COMMENT '封面',
  `content` text COMMENT '新闻正文',
  `browse_num` int(11) DEFAULT '0' COMMENT '浏览量',
  `lon` varchar(20) DEFAULT NULL COMMENT '经度',
  `lat` varchar(20) DEFAULT NULL COMMENT '纬度',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除 1否 2是',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='服务指南';

-- ----------------------------
-- Records of re_service_directory
-- ----------------------------

-- ----------------------------
-- Table structure for `re_service_notice`
-- ----------------------------
DROP TABLE IF EXISTS `re_service_notice`;
CREATE TABLE `re_service_notice` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `node` tinyint(1) DEFAULT '1' COMMENT '是否批量推送  1 批量推送， 2、微信昵称  3 读者证号 ',
  `content` varchar(500) DEFAULT NULL COMMENT '内容',
  `type_name` varchar(100) DEFAULT NULL COMMENT '服务类别',
  `time` varchar(30) DEFAULT NULL,
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `link` varchar(200) DEFAULT NULL COMMENT '链接',
  `is_send` tinyint(1) DEFAULT '2' COMMENT '是否发送  1 立即发送   2 后续发送',
  `wechat_id` varchar(5000) DEFAULT NULL COMMENT '微信id  多个逗号拼接',
  `account_id` varchar(5000) DEFAULT NULL COMMENT '读者证号id  多个逗号拼接',
  `user_id` text COMMENT '最终发送的用户id',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime DEFAULT NULL,
  `manage_id` int(11) DEFAULT NULL COMMENT '推送管理员',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1 正常   2已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of re_service_notice
-- ----------------------------

-- ----------------------------
-- Table structure for `re_service_notice_send_log`
-- ----------------------------
DROP TABLE IF EXISTS `re_service_notice_send_log`;
CREATE TABLE `re_service_notice_send_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `notice_id` int(11) DEFAULT '1' COMMENT '服务提醒通知id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `status` tinyint(1) DEFAULT '3' COMMENT '状态  1 已成功 2 失败  3 待发送',
  `open_id` varchar(100) DEFAULT NULL COMMENT '用户微信open_id',
  `reason` varchar(1000) DEFAULT NULL COMMENT '失败理由',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='服务提醒通知发送日志';

-- ----------------------------
-- Records of re_service_notice_send_log
-- ----------------------------

-- ----------------------------
-- Table structure for `re_shop`
-- ----------------------------
DROP TABLE IF EXISTS `re_shop`;
CREATE TABLE `re_shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL COMMENT '书店名称',
  `img` varchar(120) DEFAULT NULL COMMENT '封面图片',
  `province` varchar(20) DEFAULT NULL COMMENT '省',
  `city` varchar(30) DEFAULT NULL COMMENT '市',
  `district` varchar(30) DEFAULT NULL COMMENT '区',
  `street` varchar(50) DEFAULT NULL COMMENT '街道',
  `address` varchar(50) DEFAULT NULL COMMENT '详细地址',
  `tel` varchar(25) DEFAULT NULL COMMENT '书店电话',
  `contacts` varchar(20) DEFAULT NULL COMMENT '书店联系人姓名',
  `intro` text COMMENT '书店简介',
  `lon` varchar(20) DEFAULT NULL COMMENT '经度',
  `lat` varchar(20) DEFAULT NULL COMMENT '纬度',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除   1正常  2删除',
  `create_time` datetime DEFAULT NULL COMMENT '数据插入时间',
  `change_time` datetime DEFAULT NULL COMMENT '数据上一次更新时间',
  `purchase_number` int(11) DEFAULT '0' COMMENT '采购本书',
  `purchase_money` decimal(10,2) DEFAULT '0.00' COMMENT '采购金额',
  `manage_id` int(10) DEFAULT NULL,
  `way` tinyint(1) DEFAULT '1' COMMENT '1开通荐购方式，线上线下等服务，2 只开通线下、3 只开通线上 ',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='书店表';

-- ----------------------------
-- Records of re_shop
-- ----------------------------

-- ----------------------------
-- Table structure for `re_shop_book`
-- ----------------------------
DROP TABLE IF EXISTS `re_shop_book`;
CREATE TABLE `re_shop_book` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) DEFAULT NULL COMMENT '书店id',
  `shop_identifier` varchar(50) DEFAULT NULL COMMENT '图书馆唯一标识  我们自己是  书名+ISBN+价格  在MD5',
  `book_name` varchar(100) DEFAULT NULL COMMENT '书名',
  `author` varchar(100) DEFAULT NULL COMMENT '作者',
  `press` varchar(60) DEFAULT NULL COMMENT '出版社',
  `pre_time` varchar(20) DEFAULT NULL COMMENT '出版时间',
  `isbn` varchar(20) DEFAULT NULL COMMENT 'ISBN号',
  `price` decimal(8,2) DEFAULT NULL COMMENT '金额',
  `img` varchar(120) DEFAULT 'default/default_new_book.png' COMMENT '封面图片',
  `book_num` varchar(30) DEFAULT NULL,
  `intro` text COMMENT '简介',
  `type_id` int(11) DEFAULT '0' COMMENT '类型id   0 代表没有分类',
  `is_recom` tinyint(1) DEFAULT '2' COMMENT '是否为推荐图书  1 是 2 否',
  `book_selector` tinyint(1) DEFAULT '2' COMMENT '是否套书  1 是  2 否   默认 2 ',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1 正常  2 删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  `number` int(11) unsigned DEFAULT '0' COMMENT '门市陈列数(库存数) 总数，每次导入覆盖',
  `borrow_number` int(11) DEFAULT '0' COMMENT '借阅本数量',
  `rack_type` varchar(50) DEFAULT NULL COMMENT '架类型',
  `rack_describe` varchar(200) DEFAULT NULL COMMENT '架描述',
  `rack_code` varchar(30) DEFAULT NULL COMMENT '架位代码',
  `is_plane` tinyint(1) DEFAULT '1' COMMENT '是否在架   1 是   2 已下架',
  `access_number` int(5) DEFAULT '0' COMMENT '获取图片次数，超过5次，不在获取',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `type_id` (`type_id`) USING BTREE,
  KEY `ISBN` (`isbn`) USING BTREE,
  KEY `shop_identifier` (`shop_identifier`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='书店书籍表';

-- ----------------------------
-- Records of re_shop_book
-- ----------------------------

-- ----------------------------
-- Table structure for `re_shop_book_type`
-- ----------------------------
DROP TABLE IF EXISTS `re_shop_book_type`;
CREATE TABLE `re_shop_book_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) DEFAULT NULL,
  `type_name` varchar(20) DEFAULT NULL,
  `order_num` int(11) DEFAULT '0' COMMENT '对应类型数据排序，数字越大，越靠前',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除 1否 2是 默认1',
  `create_time` datetime DEFAULT NULL COMMENT '数据创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '数据更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='新书类型表';

-- ----------------------------
-- Records of re_shop_book_type
-- ----------------------------

-- ----------------------------
-- Table structure for `re_sign_score`
-- ----------------------------
DROP TABLE IF EXISTS `re_sign_score`;
CREATE TABLE `re_sign_score` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证用户id',
  `count` int(11) DEFAULT '0' COMMENT '签到次数计数',
  `date` date DEFAULT NULL COMMENT '打卡签到日期',
  `create_time` datetime DEFAULT NULL COMMENT '录入时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of re_sign_score
-- ----------------------------

-- ----------------------------
-- Table structure for `re_study_room_reservation`
-- ----------------------------
DROP TABLE IF EXISTS `re_study_room_reservation`;
CREATE TABLE `re_study_room_reservation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img` varchar(255) DEFAULT NULL COMMENT '图片',
  `name` varchar(30) DEFAULT NULL COMMENT '名称',
  `intro` text COMMENT '简介',
  `province` varchar(50) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `district` varchar(30) DEFAULT NULL,
  `street` varchar(50) DEFAULT NULL COMMENT '街道',
  `address` varchar(255) DEFAULT NULL COMMENT '详细地址',
  `contacts` varchar(30) DEFAULT '' COMMENT '联系人姓名',
  `tel` varchar(20) DEFAULT '' COMMENT '联系方式',
  `number` int(11) DEFAULT '0' COMMENT '预约量 默认0   多物品预约才有',
  `apply_number` int(11) DEFAULT '0' COMMENT '总预约次数',
  `clock_time` int(11) DEFAULT '30' COMMENT '预约开始后，多少分钟之内可打卡  单位分钟',
  `qr_code` varchar(10) DEFAULT NULL,
  `qr_url` varchar(150) DEFAULT NULL COMMENT '预约二维码引用链接',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除   默认 1 正常   2 已删除',
  `is_play` tinyint(1) DEFAULT '2' COMMENT '是否发布  1.未发布  2.发布  默认2',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员id',
  `lon` varchar(50) DEFAULT NULL COMMENT '经度',
  `lat` varchar(50) DEFAULT NULL COMMENT '纬度',
  `is_approval` tinyint(1) DEFAULT NULL COMMENT '预约是否需要审核 1需要 2不需要',
  `is_cancel` tinyint(1) DEFAULT NULL COMMENT '是否可以取消 1是 2否',
  `cancel_end_time` int(11) DEFAULT NULL COMMENT '用户可取消活动预约的最后期限(以活动开始时间往前计算）  单位分钟 0表示不限制',
  `type_tag` varchar(200) DEFAULT NULL COMMENT '补充类型(多个|连接)',
  `cab_code` varchar(20) CHARACTER SET utf8 DEFAULT '' COMMENT '整套柜子的编号，柜子分为，主柜和副柜，和柜子对接的编号',
  `stack_number` varchar(15) CHARACTER SET utf8 DEFAULT NULL COMMENT '栈号，对外',
  `display_day` int(11) DEFAULT '7' COMMENT '预约展示天数',
  `day_make_time` time DEFAULT NULL COMMENT '每日开始预约时间，这个时间之前不能预约',
  `is_real` tinyint(1) DEFAULT '2' COMMENT '是否需要用户真实信息 1需要 2不需要 默认2',
  `real_info` varchar(255) DEFAULT NULL COMMENT '需要验证的真实信息id用|连接起来    1、姓名   2 、身份证号码  3、电话号码   4、读者证号码    5、真实人物头像   6、备注  7 单位名称 8 性别 9 年龄  10、附件 ',
  `is_reader` tinyint(1) NOT NULL DEFAULT '2' COMMENT '报名是否需要绑定读者证  1 是  2 否',
  `limit_num` int(11) DEFAULT '1' COMMENT '可预约次数   1 ~ 30',
  `astrict_sex` tinyint(1) DEFAULT '3' COMMENT '性别限制   1.限男性  2.限女性  3.不限(默认)',
  `start_age` mediumint(3) DEFAULT '0' COMMENT '开始年龄  默认0   两个都是 0 则表示不需要年龄限制',
  `end_age` mediumint(3) DEFAULT '0' COMMENT '结束年龄  默认0',
  `is_sign` tinyint(1) DEFAULT '1' COMMENT '是否需要签到 1需要 固定为1',
  `sign_way` tinyint(1) DEFAULT '2' COMMENT '扫码方式  1 、2者都可以  2、用户自主扫码  3、管理员设备扫码  ',
  PRIMARY KEY (`id`,`is_reader`) USING BTREE,
  UNIQUE KEY `cab_code` (`cab_code`) USING BTREE,
  UNIQUE KEY `stack_number` (`stack_number`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='书房预约表';

-- ----------------------------
-- Records of re_study_room_reservation
-- ----------------------------

-- ----------------------------
-- Table structure for `re_study_room_reservation_apply`
-- ----------------------------
DROP TABLE IF EXISTS `re_study_room_reservation_apply`;
CREATE TABLE `re_study_room_reservation_apply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证号id',
  `reservation_id` int(11) DEFAULT NULL COMMENT '申请的文化配送预约表主键id',
  `schedule_id` int(11) DEFAULT NULL COMMENT '排班id ',
  `make_time` date DEFAULT NULL COMMENT '预约日期',
  `remark` varchar(255) DEFAULT NULL COMMENT '预约备注',
  `status` tinyint(2) DEFAULT '0' COMMENT '预约状态   1.已通过  2.已取消，3待审核，4已拒绝 ,5 已过期 , 6 已签到 7 已签退',
  `reason` varchar(255) DEFAULT NULL COMMENT '拒绝理由 ',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员',
  `number` int(11) DEFAULT '0' COMMENT '针对多物品    预约的数量   默认0',
  `is_violate` tinyint(1) DEFAULT '1' COMMENT '是否违规  1正常 2违规 默认1',
  `violate_reason` varchar(255) DEFAULT NULL COMMENT '违规原因',
  `violate_time` datetime DEFAULT NULL COMMENT '违规时间',
  `tel` varchar(20) DEFAULT NULL COMMENT '用户电话号码',
  `unit` varchar(50) DEFAULT NULL COMMENT '用户填写单位信息',
  `id_card` varchar(50) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  `expire_time` datetime DEFAULT NULL COMMENT '签到过期时间 ',
  `sign_time` datetime DEFAULT NULL COMMENT '签到开始时间',
  `sign_end_time` datetime DEFAULT NULL COMMENT '签到结束时间',
  `sex` tinyint(1) DEFAULT '0' COMMENT '性别   1 男  2 女',
  `age` int(100) DEFAULT NULL COMMENT '年龄',
  `score` int(11) DEFAULT '0' COMMENT '积分 ',
  `reader_id` varchar(30) DEFAULT NULL COMMENT '读者证',
  `img` varchar(120) DEFAULT NULL COMMENT '头像',
  `username` varchar(30) DEFAULT NULL COMMENT '姓名',
  `schedule_type` tinyint(1) DEFAULT '1' COMMENT '排版类型 1正常排班 2特殊日期排',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='用户书房预约表';

-- ----------------------------
-- Records of re_study_room_reservation_apply
-- ----------------------------

-- ----------------------------
-- Table structure for `re_study_room_reservation_collect`
-- ----------------------------
DROP TABLE IF EXISTS `re_study_room_reservation_collect`;
CREATE TABLE `re_study_room_reservation_collect` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `reservation_id` int(11) DEFAULT NULL COMMENT '文化配送id',
  `create_time` datetime DEFAULT NULL COMMENT '收藏时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='文化配送收藏表';

-- ----------------------------
-- Records of re_study_room_reservation_collect
-- ----------------------------

-- ----------------------------
-- Table structure for `re_study_room_reservation_enter_log`
-- ----------------------------
DROP TABLE IF EXISTS `re_study_room_reservation_enter_log`;
CREATE TABLE `re_study_room_reservation_enter_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(20) DEFAULT NULL COMMENT '用户id',
  `create_time` datetime DEFAULT NULL COMMENT '数据插入时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='书房用户开门进入记录表';

-- ----------------------------
-- Records of re_study_room_reservation_enter_log
-- ----------------------------

-- ----------------------------
-- Table structure for `re_study_room_reservation_remote_operation`
-- ----------------------------
DROP TABLE IF EXISTS `re_study_room_reservation_remote_operation`;
CREATE TABLE `re_study_room_reservation_remote_operation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_number` varchar(20) DEFAULT NULL COMMENT '订单号',
  `type` tinyint(2) DEFAULT NULL COMMENT 'type 类型  1 远程开门   6 下载白名单； 7 清空本地所有白名单；',
  `reservation_id` int(11) DEFAULT NULL COMMENT '开始时间',
  `cab_code` varchar(10) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '状态   1 已成功 2 处理中 3待处理  4 执行失败',
  `account` varchar(500) DEFAULT NULL COMMENT '白名单账号  多个逗号拼接',
  `node` tinyint(1) DEFAULT '1' COMMENT '操作平台，1、前台用户  2 、后台管理员',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `create_time` datetime DEFAULT NULL COMMENT '数据插入时间',
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='书房开门白名单表';

-- ----------------------------
-- Records of re_study_room_reservation_remote_operation
-- ----------------------------

-- ----------------------------
-- Table structure for `re_study_room_reservation_schedule`
-- ----------------------------
DROP TABLE IF EXISTS `re_study_room_reservation_schedule`;
CREATE TABLE `re_study_room_reservation_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reservation_id` int(11) DEFAULT NULL COMMENT '文化配送预约表主键id',
  `week` tinyint(1) DEFAULT '0' COMMENT '星期   1-7   星期日 为  7 ',
  `start_time` time DEFAULT NULL COMMENT '开始时间  时分秒',
  `end_time` time DEFAULT NULL COMMENT '结束时间   时分秒',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1 正常  2 已删除 ',
  `create_time` datetime DEFAULT NULL COMMENT '数据插入时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='书房预约排班表';

-- ----------------------------
-- Records of re_study_room_reservation_schedule
-- ----------------------------

-- ----------------------------
-- Table structure for `re_study_room_reservation_special_schedule`
-- ----------------------------
DROP TABLE IF EXISTS `re_study_room_reservation_special_schedule`;
CREATE TABLE `re_study_room_reservation_special_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自动id',
  `reservation_id` int(11) DEFAULT NULL COMMENT '预约id',
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `date` date DEFAULT NULL COMMENT '日期',
  `start_time` time DEFAULT NULL COMMENT '开始时间',
  `end_time` time DEFAULT NULL COMMENT '结束时间',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1 正常  2 已删除  默认1 ',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='文化配送特殊日期排班表';

-- ----------------------------
-- Records of re_study_room_reservation_special_schedule
-- ----------------------------

-- ----------------------------
-- Table structure for `re_study_room_reservation_white`
-- ----------------------------
DROP TABLE IF EXISTS `re_study_room_reservation_white`;
CREATE TABLE `re_study_room_reservation_white` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL COMMENT '文化配送预约表主键id',
  `account` varchar(20) DEFAULT NULL COMMENT '账号',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL,
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1 正常  2删除',
  `create_time` datetime DEFAULT NULL COMMENT '数据插入时间',
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='书房开门白名单表';

-- ----------------------------
-- Records of re_study_room_reservation_white
-- ----------------------------

-- ----------------------------
-- Table structure for `re_study_room_reservation_white_enter_log`
-- ----------------------------
DROP TABLE IF EXISTS `re_study_room_reservation_white_enter_log`;
CREATE TABLE `re_study_room_reservation_white_enter_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL COMMENT '文化配送预约表主键id',
  `create_time` datetime DEFAULT NULL COMMENT '数据插入时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='书房白名单开门进入记录表';

-- ----------------------------
-- Records of re_study_room_reservation_white_enter_log
-- ----------------------------

-- ----------------------------
-- Table structure for `re_survey`
-- ----------------------------
DROP TABLE IF EXISTS `re_survey`;
CREATE TABLE `re_survey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_name` varchar(100) DEFAULT NULL COMMENT '问卷名称',
  `intro` varchar(3000) DEFAULT NULL COMMENT '问卷简介',
  `is_play` tinyint(255) DEFAULT '1' COMMENT '是否发布的状态   1 已发布   2 未发布',
  `start_time` datetime DEFAULT NULL COMMENT '问卷开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '问卷结束时间',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_del` tinyint(2) DEFAULT '1' COMMENT '是否删除 1未删除 2已删除',
  `reply_num` int(11) DEFAULT '0' COMMENT '回答数量 ',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='问卷调查表';

-- ----------------------------
-- Records of re_survey
-- ----------------------------

-- ----------------------------
-- Table structure for `re_survey_answer`
-- ----------------------------
DROP TABLE IF EXISTS `re_survey_answer`;
CREATE TABLE `re_survey_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sur_id` int(11) DEFAULT NULL COMMENT '问卷调查id',
  `pro_id` int(11) DEFAULT NULL COMMENT '问卷调查问题  id ',
  `answer` varchar(1000) DEFAULT NULL COMMENT '问卷答案   有 答案 ： 定向题  无答案：用户自定义回答题目',
  `reply_num` int(11) DEFAULT '0' COMMENT '回复量 默认0',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='问卷调查 答案表';

-- ----------------------------
-- Records of re_survey_answer
-- ----------------------------

-- ----------------------------
-- Table structure for `re_survey_problem`
-- ----------------------------
DROP TABLE IF EXISTS `re_survey_problem`;
CREATE TABLE `re_survey_problem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sur_id` int(11) DEFAULT NULL COMMENT '问卷调查  id  ',
  `problem` varchar(300) DEFAULT NULL COMMENT '问卷问题',
  `type` tinyint(1) DEFAULT '1' COMMENT '题目类型  1  多选  2 单选 （默认）3 填空题',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='问卷调查 问题表';

-- ----------------------------
-- Records of re_survey_problem
-- ----------------------------

-- ----------------------------
-- Table structure for `re_survey_reply`
-- ----------------------------
DROP TABLE IF EXISTS `re_survey_reply`;
CREATE TABLE `re_survey_reply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sur_id` int(11) DEFAULT NULL COMMENT '问卷调查信息  id ',
  `pro_id` int(11) DEFAULT NULL COMMENT '问卷调查问题  id  ',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `answer` varchar(20) DEFAULT '' COMMENT '问卷答案 id，多选用 | 拼接 , ',
  `content` text COMMENT '用户填空回答回答',
  `create_time` datetime DEFAULT NULL COMMENT '答题时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='问卷调查 答案表';

-- ----------------------------
-- Records of re_survey_reply
-- ----------------------------

-- ----------------------------
-- Table structure for `re_system_info`
-- ----------------------------
DROP TABLE IF EXISTS `re_system_info`;
CREATE TABLE `re_system_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL COMMENT '消息标题',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT '0' COMMENT '读者证号id',
  `type` varchar(20) DEFAULT NULL COMMENT '类型',
  `con_id` int(11) DEFAULT NULL COMMENT '内容id',
  `intro` varchar(500) DEFAULT NULL COMMENT '消息内容',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1正常   2 删除',
  `is_look` tinyint(1) unsigned DEFAULT '2' COMMENT '是否已读   1.是   2.否(默认)',
  `create_time` datetime DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统消息表\r\n';

-- ----------------------------
-- Records of re_system_info
-- ----------------------------

-- ----------------------------
-- Table structure for `re_turn_activity`
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity`;
CREATE TABLE `re_turn_activity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL COMMENT '标题',
  `node` tinyint(2) DEFAULT '1' COMMENT '执行时间类型    1 单次  2 每周   3 每月   ',
  `execute_day` varchar(150) DEFAULT NULL COMMENT '执行时间设置   如果是每周，1代表星期一，2代表星期2，多个逗号拼接，如果是每月，1代表 1号，2代表 2号，多个逗号拼接',
  `is_ushare` tinyint(1) DEFAULT '1' COMMENT '是否阅享用户  1  是  2 否',
  `real_info` varchar(100) DEFAULT NULL COMMENT '填写信息  如果是阅享用户，则无效    1、姓名  2、电话   3、身份证号码  4、读者证   多个  | 连接',
  `img` varchar(120) DEFAULT 'default/default_readshare_activity.png',
  `number` int(11) DEFAULT '1' COMMENT '次数，根据 vote_way执行方式， 总形式，则表示总执行次数，每日模式，则是每日次数    ',
  `round_number` int(11) DEFAULT '1' COMMENT '每轮几次    设置前端抽奖后（或抽几次后），重置界面',
  `share_number` int(11) DEFAULT NULL COMMENT '分享获取答题次数，每日几次  ',
  `rule_type` tinyint(1) DEFAULT NULL COMMENT '规则方式   1 内容   2外链',
  `rule` text COMMENT '规则内容     rule_type  为1 这里是编辑器内容   2 这里是链接',
  `start_time` datetime DEFAULT NULL COMMENT '活动开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '活动结束时间    不设置则表示结束时间',
  `turn_start_time` datetime DEFAULT NULL COMMENT '开始抽奖的时间 ',
  `turn_end_time` datetime DEFAULT NULL COMMENT '结束抽奖的时间',
  `invite_code` varchar(8) DEFAULT NULL COMMENT '邀请码',
  `vote_way` tinyint(1) DEFAULT '1' COMMENT '活动执行方式    1 总数量形式   2 每日执行形式',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除   1 正常 2 已删除',
  `is_play` tinyint(1) DEFAULT '2' COMMENT '是否发布   1 已发布  2 未发布',
  `manage_id` int(11) DEFAULT NULL,
  `browse_num` int(11) DEFAULT '0' COMMENT '浏览量',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL,
  `share_title` varchar(20) DEFAULT NULL COMMENT '分享标题',
  `share_img` varchar(120) DEFAULT NULL COMMENT '分享图片',
  `is_show_address` tinyint(1) DEFAULT '1' COMMENT '是否需要填写  1 需要  2 不需要',
  `technical_support` varchar(30) DEFAULT '成都贝图科技有限公司' COMMENT '技术支持,如果不填，则前端不显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='翻一翻活动';

-- ----------------------------
-- Records of re_turn_activity
-- ----------------------------

-- ----------------------------
-- Table structure for `re_turn_activity_browse_count`
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_browse_count`;
CREATE TABLE `re_turn_activity_browse_count` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL,
  `unit_id` int(11) DEFAULT '0' COMMENT '单位id   0 代表全部',
  `date` date DEFAULT NULL,
  `hour` varchar(2) DEFAULT NULL COMMENT '小时',
  `number` int(11) DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='翻一翻活动浏览量统计表';

-- ----------------------------
-- Records of re_turn_activity_browse_count
-- ----------------------------

-- ----------------------------
-- Table structure for `re_turn_activity_gift`
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_gift`;
CREATE TABLE `re_turn_activity_gift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `name` varchar(50) DEFAULT NULL COMMENT '礼品名称',
  `img` varchar(150) DEFAULT NULL COMMENT '礼品图片',
  `intro` varchar(100) DEFAULT NULL COMMENT '礼品简介',
  `total_number` int(11) DEFAULT NULL COMMENT '礼品总数量',
  `price` decimal(6,2) DEFAULT NULL COMMENT '红包金额',
  `percent` decimal(6,2) DEFAULT NULL COMMENT '中奖率(百分比)',
  `type` tinyint(1) DEFAULT NULL COMMENT '1文化红包 2精美礼品',
  `way` tinyint(1) DEFAULT '0' COMMENT '领取方式   1 自提  2邮递   红包无此选项',
  `start_time` datetime DEFAULT NULL COMMENT '自提开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '自提结束时间',
  `tel` varchar(15) DEFAULT NULL COMMENT '联系电话',
  `contacts` varchar(30) DEFAULT NULL COMMENT '联系人',
  `province` varchar(20) DEFAULT NULL COMMENT '省',
  `city` varchar(20) DEFAULT NULL COMMENT '市',
  `district` varchar(20) DEFAULT NULL COMMENT '区',
  `address` varchar(255) DEFAULT NULL COMMENT '详细地址',
  `remark` text COMMENT '自提备注',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否被删除 1正常 2已删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `activity_index` (`act_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='礼物表';

-- ----------------------------
-- Records of re_turn_activity_gift
-- ----------------------------

-- ----------------------------
-- Table structure for `re_turn_activity_gift_config`
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_gift_config`;
CREATE TABLE `re_turn_activity_gift_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `percent_way` tinyint(1) DEFAULT '1' COMMENT '概率方式   1 单个奖品独立概率方式    2 所以奖品总概率形式',
  `gift_max_number_day` int(11) DEFAULT NULL COMMENT '礼物每日发送奖品最大数量',
  `red_max_number_day` int(11) DEFAULT NULL COMMENT '礼物每日发送奖品最大数量',
  `user_gift_number_day` int(11) DEFAULT '1' COMMENT '用户当日可获得多少实物礼品',
  `user_red_number_day` int(11) DEFAULT '1' COMMENT '用户当日可获得多少红包礼品',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='礼品领取总配置表';

-- ----------------------------
-- Records of re_turn_activity_gift_config
-- ----------------------------

-- ----------------------------
-- Table structure for `re_turn_activity_gift_public`
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_gift_public`;
CREATE TABLE `re_turn_activity_gift_public` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `type` tinyint(1) DEFAULT NULL COMMENT '礼物类型  1文化红包 2精美礼品',
  `name` varchar(50) DEFAULT NULL COMMENT '礼品名称',
  `way` tinyint(1) DEFAULT NULL COMMENT '操作方式  1 投放  2追加',
  `number` int(11) DEFAULT NULL COMMENT '礼品数量',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否被删除 1正常 2已删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `activity_index` (`act_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='礼物公示表';

-- ----------------------------
-- Records of re_turn_activity_gift_public
-- ----------------------------

-- ----------------------------
-- Table structure for `re_turn_activity_gift_record`
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_gift_record`;
CREATE TABLE `re_turn_activity_gift_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `unit_id` int(11) DEFAULT NULL COMMENT '单位id',
  `gift_id` int(11) DEFAULT NULL COMMENT '礼物id',
  `before_number` int(11) DEFAULT '0' COMMENT '最加之前数量    ',
  `later_number` int(11) DEFAULT '0' COMMENT '追加之后数量',
  `gift_number` int(11) DEFAULT '0' COMMENT '礼物数量',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态 1 增加 2减少',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员id',
  `create_time` datetime DEFAULT NULL COMMENT '记录时间',
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `act_id` (`act_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='礼物变动记录表';

-- ----------------------------
-- Records of re_turn_activity_gift_record
-- ----------------------------

-- ----------------------------
-- Table structure for `re_turn_activity_gift_time`
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_gift_time`;
CREATE TABLE `re_turn_activity_gift_time` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `start_time` datetime DEFAULT NULL COMMENT '奖品开始发放时间   ',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间   和开始时间对应不允许跨天',
  `type` tinyint(4) DEFAULT NULL COMMENT '奖品类型 1文化红包 2精美礼品',
  `number` int(11) DEFAULT NULL COMMENT '发放数量',
  `user_gift_number` int(11) DEFAULT '1' COMMENT '用户当日可获得多少礼品',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除 1正常 2删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='礼品领取排班表';

-- ----------------------------
-- Records of re_turn_activity_gift_time
-- ----------------------------

-- ----------------------------
-- Table structure for `re_turn_activity_invite`
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_invite`;
CREATE TABLE `re_turn_activity_invite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `user_guid` varchar(32) DEFAULT NULL,
  `invite_code` varchar(10) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='活动邀请码';

-- ----------------------------
-- Records of re_turn_activity_invite
-- ----------------------------

-- ----------------------------
-- Table structure for `re_turn_activity_limit_address`
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_limit_address`;
CREATE TABLE `re_turn_activity_limit_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '每个活动独立',
  `province` varchar(20) DEFAULT NULL COMMENT '省',
  `city` varchar(30) DEFAULT NULL COMMENT '市',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1 正常  2删除',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime DEFAULT NULL COMMENT '删除、修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='答题活动限制参与地址';

-- ----------------------------
-- Records of re_turn_activity_limit_address
-- ----------------------------

-- ----------------------------
-- Table structure for `re_turn_activity_resource`
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_resource`;
CREATE TABLE `re_turn_activity_resource` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '标题',
  `theme_color` varchar(10) DEFAULT NULL COMMENT '主题色',
  `progress_color` varchar(10) DEFAULT NULL COMMENT '进度条颜色',
  `invite_color` varchar(10) DEFAULT NULL COMMENT '邀请码颜色',
  `theme_text_color` varchar(10) DEFAULT NULL COMMENT '主题文字色',
  `warning_color` varchar(10) DEFAULT NULL COMMENT '警告颜色',
  `error_color` varchar(10) DEFAULT NULL COMMENT '错误颜色',
  `resource_path` varchar(100) DEFAULT NULL COMMENT '资源路径，中间半截',
  `is_change` tinyint(1) DEFAULT '2' COMMENT '资源是否变化，包括图片资源是否改变  1 未变化  2 有变化',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='活动资源表';

-- ----------------------------
-- Records of re_turn_activity_resource
-- ----------------------------

-- ----------------------------
-- Table structure for `re_turn_activity_share`
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_share`;
CREATE TABLE `re_turn_activity_share` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL,
  `user_guid` varchar(32) DEFAULT NULL COMMENT '区域名称',
  `date` date DEFAULT NULL COMMENT '日期',
  `number` int(11) DEFAULT '0' COMMENT '分享次数',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='活动分享次数表';

-- ----------------------------
-- Records of re_turn_activity_share
-- ----------------------------

-- ----------------------------
-- Table structure for `re_turn_activity_user_gift`
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_user_gift`;
CREATE TABLE `re_turn_activity_user_gift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `user_guid` varchar(32) DEFAULT '' COMMENT '用户guid',
  `gift_id` int(11) DEFAULT NULL COMMENT '礼物id',
  `order_id` varchar(50) DEFAULT '' COMMENT '订单号',
  `use_prize_record_id` int(11) DEFAULT NULL COMMENT '用户使用抽奖机会id',
  `price` decimal(6,2) DEFAULT NULL COMMENT '红包大小',
  `state` tinyint(1) DEFAULT '1' COMMENT '礼物状态 1未发放 2已发放 3未发货 4邮递中 5已到货 6 未领取  7 已领取',
  `date` date DEFAULT NULL COMMENT '获奖日期',
  `hour` varchar(2) DEFAULT NULL COMMENT '小时',
  `type` tinyint(1) DEFAULT NULL COMMENT '1文化红包 2精美礼品',
  `create_time` datetime DEFAULT NULL COMMENT '红包创建时间',
  `pay_time` datetime DEFAULT NULL COMMENT '红包到账时间',
  `change_time` datetime DEFAULT NULL COMMENT '到货时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `activity_index` (`act_id`) USING BTREE,
  KEY `user_index` (`user_guid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='用户获取礼物表';

-- ----------------------------
-- Records of re_turn_activity_user_gift
-- ----------------------------

-- ----------------------------
-- Table structure for `re_turn_activity_user_unit`
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_user_unit`;
CREATE TABLE `re_turn_activity_user_unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_guid` char(32) DEFAULT NULL COMMENT '用户id',
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `username` varchar(50) DEFAULT NULL COMMENT '姓名',
  `tel` varchar(20) DEFAULT NULL COMMENT '电话号码',
  `id_card` varchar(20) DEFAULT NULL COMMENT '身份证',
  `reader_id` varchar(20) DEFAULT NULL COMMENT '读者证',
  `unit_id` int(11) DEFAULT NULL COMMENT '电话号码',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime DEFAULT NULL COMMENT '删除、修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户所属单位表';

-- ----------------------------
-- Records of re_turn_activity_user_unit
-- ----------------------------

-- ----------------------------
-- Table structure for `re_turn_activity_user_use_prize_record`
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_user_use_prize_record`;
CREATE TABLE `re_turn_activity_user_use_prize_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `user_guid` varchar(32) DEFAULT '' COMMENT '用户guid',
  `date` date DEFAULT NULL COMMENT '日期',
  `status` tinyint(1) DEFAULT NULL COMMENT '是否中奖  1 中奖  2 未中奖',
  `order_id` varchar(20) DEFAULT NULL COMMENT '订单号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `act_id` (`act_id`) USING BTREE,
  KEY `user_guid` (`user_guid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='用户抽奖记录表';

-- ----------------------------
-- Records of re_turn_activity_user_use_prize_record
-- ----------------------------

-- ----------------------------
-- Table structure for `re_user_account_borrow`
-- ----------------------------
DROP TABLE IF EXISTS `re_user_account_borrow`;
CREATE TABLE `re_user_account_borrow` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL COMMENT '读者证号id',
  `year` int(10) DEFAULT NULL COMMENT '年',
  `number` int(10) DEFAULT NULL COMMENT '数量',
  `create_time` datetime DEFAULT NULL COMMENT '开始时间',
  `change_time` datetime DEFAULT NULL COMMENT '最近一次修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='图书馆系统 读者证号对应的借阅数量';

-- ----------------------------
-- Records of re_user_account_borrow
-- ----------------------------

-- ----------------------------
-- Table structure for `re_user_account_borrow_log`
-- ----------------------------
DROP TABLE IF EXISTS `re_user_account_borrow_log`;
CREATE TABLE `re_user_account_borrow_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `api_id` int(11) unsigned DEFAULT NULL COMMENT '外部接口返回的id',
  `eventType` varchar(10) DEFAULT NULL COMMENT '状态 读者借出 Ea   读者续借Eb   读者过期Ec  文献损坏Ed   读者丢失文献Ef',
  `loginWay` varchar(20) DEFAULT NULL,
  `readerId` int(10) DEFAULT NULL COMMENT '读者id',
  `assetId` int(10) DEFAULT NULL,
  `servrequestId` int(10) DEFAULT NULL,
  `metatable` varchar(20) DEFAULT NULL,
  `metaid` int(10) DEFAULT NULL,
  `curSublib` varchar(20) DEFAULT NULL,
  `curLocal` varchar(20) DEFAULT NULL,
  `initSublib` varchar(20) DEFAULT NULL,
  `initLocal` varchar(20) DEFAULT NULL,
  `cardno` varchar(20) DEFAULT NULL,
  `readerLib` varchar(20) DEFAULT NULL,
  `realName` varchar(20) DEFAULT NULL,
  `barcode` varchar(20) DEFAULT NULL,
  `cirType` varchar(20) DEFAULT NULL,
  `volumeno` int(10) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `author` varchar(200) DEFAULT NULL,
  `isbn` varchar(50) DEFAULT NULL,
  `publish` varchar(50) DEFAULT NULL,
  `callno` varchar(50) DEFAULT NULL,
  `price` varchar(50) DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `adminName` varchar(50) DEFAULT NULL,
  `updateDateTime` datetime DEFAULT NULL,
  `eventTypeNote` varchar(50) DEFAULT NULL,
  `loginWayNote` varchar(50) DEFAULT NULL,
  `curSublibNote` varchar(50) DEFAULT NULL,
  `curLocalNote` varchar(50) DEFAULT NULL,
  `initSublibNote` varchar(50) DEFAULT NULL,
  `initLocalNote` varchar(50) DEFAULT NULL,
  `readerLibNote` varchar(50) DEFAULT NULL,
  `cirTypeNote` varchar(50) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='图书馆系统 读者证号对应的借阅数据日志表，在表的标识';

-- ----------------------------
-- Records of re_user_account_borrow_log
-- ----------------------------

-- ----------------------------
-- Table structure for `re_user_account_lib`
-- ----------------------------
DROP TABLE IF EXISTS `re_user_account_lib`;
CREATE TABLE `re_user_account_lib` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `account` varchar(30) DEFAULT NULL COMMENT '账户',
  `password` varchar(32) DEFAULT NULL COMMENT '密码',
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `cash` decimal(10,0) DEFAULT NULL COMMENT '押金',
  `id_card` varchar(20) DEFAULT NULL COMMENT '身份证号码',
  `sex` tinyint(1) DEFAULT '1' COMMENT '1男 2女',
  `tel` varchar(15) DEFAULT NULL COMMENT '电话',
  `type` varchar(30) DEFAULT '普通卡' COMMENT '借阅卡类型 ',
  `time` datetime DEFAULT NULL COMMENT '借阅证注册时间',
  `is_surpass` varchar(3) DEFAULT 'NO',
  `pavilion` varchar(20) DEFAULT NULL COMMENT '开户馆',
  `pavilion_note` varchar(30) DEFAULT NULL COMMENT '开户馆注释',
  `end_time` datetime DEFAULT NULL COMMENT '借阅证有效期',
  `diff_money` decimal(10,2) DEFAULT '0.00' COMMENT '欠费（图书馆欠费金额）',
  `recom_diff_money` decimal(10,2) DEFAULT '0.00' COMMENT '书店荐购欠费金额',
  `status_card` varchar(20) DEFAULT '有效' COMMENT '证件状态',
  `recomed_num` mediumint(5) DEFAULT '0' COMMENT '用户一年采购本数',
  `recomed_money` decimal(10,2) DEFAULT '0.00' COMMENT '用户一年采购金额',
  `s_time` datetime DEFAULT NULL COMMENT ' 一年的开始时间',
  `e_time` datetime DEFAULT NULL COMMENT '一年的结束时间',
  `last_time` datetime DEFAULT NULL COMMENT '上一次采购时间',
  `last_mmm` varchar(6) DEFAULT NULL COMMENT '上一次采购月份   Ym',
  `last_num` tinyint(3) DEFAULT '0' COMMENT '上一次采购的本数，如果还是本月就是本月采购的本数，如果不是本月，就是上一月采购的本数',
  `last_money` decimal(10,2) DEFAULT '0.00' COMMENT '上一次采购的金额，如果还是本月就是本月采购的金额，如果不是本月，就是上一月采购的金额',
  `purch_num_lib` mediumint(5) DEFAULT '0' COMMENT '读者在图书馆采购本数',
  `purch_money_lib` decimal(10,2) DEFAULT '0.00' COMMENT '读者在图书馆采购金额',
  `purch_num_shop` int(5) DEFAULT '0' COMMENT '书店采购总本书',
  `purch_money_shop` decimal(10,2) DEFAULT '0.00' COMMENT '书店采购总金额',
  `qr_url` varchar(100) DEFAULT NULL COMMENT '读者证二维码，与用户表对应',
  `score` int(10) DEFAULT '0' COMMENT '积分',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL COMMENT '数据更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `account` (`account`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='用户账号表（图书馆读者证号表）';

-- ----------------------------
-- Records of re_user_account_lib
-- ----------------------------

-- ----------------------------
-- Table structure for `re_user_address`
-- ----------------------------
DROP TABLE IF EXISTS `re_user_address`;
CREATE TABLE `re_user_address` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `username` varchar(30) DEFAULT NULL COMMENT '用户姓名',
  `tel` varchar(20) DEFAULT NULL COMMENT '电话号码',
  `province` varchar(20) DEFAULT NULL COMMENT '省',
  `city` varchar(30) DEFAULT NULL COMMENT '市',
  `district` varchar(30) DEFAULT NULL COMMENT '区',
  `street` varchar(30) DEFAULT NULL COMMENT '街道',
  `address` varchar(200) DEFAULT NULL COMMENT '详细地址',
  `area_code` varchar(100) DEFAULT NULL COMMENT ' 地区代码',
  `is_default` tinyint(1) DEFAULT '2' COMMENT '是否默认   1 默认  2 非默认',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1 正常  2删除',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime DEFAULT NULL COMMENT '删除、修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户收货地址表';

-- ----------------------------
-- Records of re_user_address
-- ----------------------------

-- ----------------------------
-- Table structure for `re_user_draw_address`
-- ----------------------------
DROP TABLE IF EXISTS `re_user_draw_address`;
CREATE TABLE `re_user_draw_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_guid` char(32) DEFAULT NULL COMMENT '用户id',
  `type` tinyint(1) DEFAULT NULL COMMENT '类型  1、答题活动   2、翻一翻',
  `act_id` int(11) DEFAULT NULL COMMENT '每个活动独立',
  `username` varchar(30) DEFAULT NULL COMMENT '用户姓名',
  `tel` varchar(20) DEFAULT NULL COMMENT '电话号码',
  `province` varchar(20) DEFAULT NULL COMMENT '省',
  `city` varchar(30) DEFAULT NULL COMMENT '市',
  `district` varchar(30) DEFAULT NULL COMMENT '区',
  `street` varchar(30) DEFAULT NULL COMMENT '街道',
  `address` varchar(200) DEFAULT NULL COMMENT '详细地址',
  `area_code` varchar(100) DEFAULT NULL COMMENT ' 地区代码',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除  1 正常  2删除',
  `create_time` datetime DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime DEFAULT NULL COMMENT '删除、修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户抽奖收货地址表';

-- ----------------------------
-- Records of re_user_draw_address
-- ----------------------------

-- ----------------------------
-- Table structure for `re_user_info`
-- ----------------------------
DROP TABLE IF EXISTS `re_user_info`;
CREATE TABLE `re_user_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL COMMENT '图书证表id',
  `wechat_id` int(11) DEFAULT NULL COMMENT '微信表id',
  `token` char(32) DEFAULT NULL COMMENT '用户token',
  `qr_url` varchar(100) DEFAULT NULL COMMENT '用户二维码',
  `qr_code` varchar(10) DEFAULT NULL COMMENT '二维码标识code   固定 前  2位 为  10',
  `invite_user_id` int(11) DEFAULT NULL,
  `invite_account_id` int(11) DEFAULT NULL,
  `bind_time` datetime DEFAULT NULL COMMENT '绑定时间',
  `is_volunteer` tinyint(1) DEFAULT '2' COMMENT '是否是志愿者   1 是  2 否',
  `volunteer_service_time` int(11) DEFAULT '0' COMMENT '志愿者服务时长 单位 分钟',
  `score` int(11) DEFAULT '0' COMMENT '用户积分，与图书馆账号表中的score 2选一',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `token` (`token`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户表';

-- ----------------------------
-- Records of re_user_info
-- ----------------------------
INSERT INTO `re_user_info` VALUES ('1', null, '1', 'a73f978dc877f4b199bd7252408cab05', 'qr/2023-11-09/654c73671b254231109015135.png', '10U63MVI58', null, null, null, '2', '0', '0', '2023-11-09 13:51:35');

-- ----------------------------
-- Table structure for `re_user_protocol`
-- ----------------------------
DROP TABLE IF EXISTS `re_user_protocol`;
CREATE TABLE `re_user_protocol` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) DEFAULT '1' COMMENT '1 用户协议    2  大赛原创声明  3 景点打卡  4 在线办证  5、扫码借借阅规则',
  `content` longtext COMMENT '内容',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户协议';

-- ----------------------------
-- Records of re_user_protocol
-- ----------------------------

-- ----------------------------
-- Table structure for `re_user_violate`
-- ----------------------------
DROP TABLE IF EXISTS `re_user_violate`;
CREATE TABLE `re_user_violate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `activity_violate_num` int(11) DEFAULT '0' COMMENT '用户活动违规次数',
  `reservation_violate_num` int(11) DEFAULT '0' COMMENT '用户文化预约违规次数',
  `contest_violate_num` int(11) DEFAULT '0' COMMENT '线上大赛违规次数',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL,
  `activity_violate_clear_time` datetime DEFAULT NULL COMMENT '清除违规时间，在此之前的违规都不算，为空，则都计算在内',
  `reservation_violate_clear_time` datetime DEFAULT NULL COMMENT '清除违规时间，在此之前的违规都不算，为空，则都计算在内',
  `contest_violate_clear_time` datetime DEFAULT NULL COMMENT '清除违规时间，在此之前的违规都不算，为空，则都计算在内',
  `scenic_violate_num` int(11) DEFAULT '0' COMMENT '用户景点打卡违规次数  ',
  `scenic_violate_clear_time` datetime DEFAULT NULL COMMENT '清除违规时间，在此之前的违规都不算，为空，则都计算在内',
  `study_room_reservation_violate_clear_time` datetime DEFAULT NULL COMMENT '清除违规时间，在此之前的违规都不算，为空，则都计算在内',
  `study_room_reservation_violate_num` int(11) DEFAULT '0' COMMENT '自习室门禁预约违规次数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户违规次数表';

-- ----------------------------
-- Records of re_user_violate
-- ----------------------------

-- ----------------------------
-- Table structure for `re_user_wechat_info`
-- ----------------------------
DROP TABLE IF EXISTS `re_user_wechat_info`;
CREATE TABLE `re_user_wechat_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `union_id` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `session_key` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_id` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '用户微信id',
  `nickname` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '微信昵称',
  `head_img` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '微信头像',
  `tel` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '微信获取的用户电话',
  `create_time` datetime DEFAULT NULL COMMENT '数据插入时间',
  `change_time` datetime DEFAULT NULL COMMENT '上一次绑定借阅证时间',
  `invite_user_id` int(11) DEFAULT NULL,
  `invite_account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `open_id` (`open_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT COMMENT='微信用户表';

-- ----------------------------
-- Records of re_user_wechat_info
-- ----------------------------
INSERT INTO `re_user_wechat_info` VALUES ('1', null, null, 'oR-rKs26pzrTrOzYSi3AQX8JdsJE', '  ', 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIgoKOeiatBlzoOQlXYvp3ygQ9JzLbHbe3ibbN1JRNf12JySFyJ5UMPZX7S988n1opIalyyQhKdfeDQ/132', null, '2023-11-09 13:51:35', '2023-11-09 13:51:35', null, null);

-- ----------------------------
-- Table structure for `re_video_live`
-- ----------------------------
DROP TABLE IF EXISTS `re_video_live`;
CREATE TABLE `re_video_live` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '线上大赛活动id',
  `cid` varchar(50) DEFAULT NULL COMMENT '网易直播cid',
  `title` varchar(100) DEFAULT NULL COMMENT '活动名称',
  `channel_name` varchar(200) DEFAULT NULL COMMENT '网易云信直播频道名称',
  `img` varchar(120) DEFAULT 'default/default_picture_live.png' COMMENT '封面图片',
  `tel` varchar(30) DEFAULT NULL COMMENT '咨询方式',
  `host_handle` varchar(255) DEFAULT '' COMMENT '主办单位',
  `address` varchar(200) DEFAULT NULL COMMENT '地址',
  `start_time` datetime DEFAULT NULL COMMENT '直播开始时间（对外展示时间，实际不以这个时间为准）',
  `end_time` datetime DEFAULT NULL COMMENT '投稿结束时间',
  `browse_num` int(11) DEFAULT '0' COMMENT '浏览量',
  `size` varchar(20) DEFAULT '0' COMMENT '文件大小',
  `total_time` varchar(20) DEFAULT '0' COMMENT '总时长(毫秒)  默认0',
  `status` tinyint(1) DEFAULT '1' COMMENT '直播状态   1 未开始直播 2 直播中 3 已结束',
  `wy_status` tinyint(1) DEFAULT '0' COMMENT '网易直播状态   0：空闲； 1：直播； 2：禁用； 3：直播录制',
  `intro` text COMMENT '简介',
  `http_pull_url` varchar(500) DEFAULT NULL COMMENT 'http拉流地址',
  `hls_pull_url` varchar(500) DEFAULT NULL COMMENT 'hls拉流地址',
  `push_url` varchar(500) DEFAULT NULL COMMENT '推流地址',
  `rtmp_pull_url` varchar(500) DEFAULT NULL COMMENT 'rtmp拉流地址',
  `manage_id` int(10) unsigned DEFAULT NULL COMMENT '管理员id',
  `is_play` tinyint(1) DEFAULT '1' COMMENT '是否发布 1.发布 2.未发布    默认1',
  `is_del` tinyint(1) unsigned DEFAULT '1' COMMENT '是否删除   1.正常   2.已删除',
  `create_time` datetime DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime DEFAULT NULL COMMENT '编辑时间',
  `video_num` int(11) DEFAULT '0' COMMENT '视频个数(主要是统计直播分段视频个数) 默认0',
  `qr_code` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL,
  `qr_url` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '二维码链接',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='视频直播活动表';

-- ----------------------------
-- Records of re_video_live
-- ----------------------------

-- ----------------------------
-- Table structure for `re_video_live_resource`
-- ----------------------------
DROP TABLE IF EXISTS `re_video_live_resource`;
CREATE TABLE `re_video_live_resource` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '线上大赛活动id',
  `name` varchar(200) DEFAULT NULL COMMENT '视频名称',
  `act_id` int(11) DEFAULT NULL COMMENT '视频id',
  `link` varchar(500) DEFAULT NULL COMMENT '分段链接原链接',
  `video_address` varchar(300) DEFAULT '' COMMENT '本地视频地址',
  `format` varchar(30) DEFAULT NULL COMMENT '格式',
  `size` varchar(200) DEFAULT NULL COMMENT '分段视频大小',
  `duration_time` int(11) DEFAULT NULL COMMENT '分段视频时长  毫秒',
  `start_time` varchar(20) DEFAULT NULL COMMENT '视频开始时间   (毫秒)',
  `end_time` varchar(20) DEFAULT NULL COMMENT '视频结束时间   (毫秒)',
  `is_del` tinyint(1) unsigned DEFAULT '1' COMMENT '是否删除   1.正常   2.已删除',
  `browse_num` int(11) DEFAULT '0' COMMENT '浏览量',
  `callbcak_video_info` varchar(5000) DEFAULT NULL COMMENT '获取原本的回调信息',
  `create_time` datetime DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='视频直播数据资源表';

-- ----------------------------
-- Records of re_video_live_resource
-- ----------------------------

-- ----------------------------
-- Table structure for `re_video_live_vote`
-- ----------------------------
DROP TABLE IF EXISTS `re_video_live_vote`;
CREATE TABLE `re_video_live_vote` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '线上大赛活动id',
  `act_id` int(11) DEFAULT NULL COMMENT '网易直播cid',
  `user_id` int(11) DEFAULT NULL COMMENT '活动名称',
  `create_time` datetime DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='视频直播活动表';

-- ----------------------------
-- Records of re_video_live_vote
-- ----------------------------

-- ----------------------------
-- Table structure for `re_violate_config`
-- ----------------------------
DROP TABLE IF EXISTS `re_violate_config`;
CREATE TABLE `re_violate_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `activity` int(11) DEFAULT '0' COMMENT '用户活动可违规次数',
  `reservation` int(11) DEFAULT '0' COMMENT '用户预约可违规次数',
  `contest` int(11) DEFAULT '0' COMMENT '用户线上大赛可违规次数',
  `activity_clear_day` int(11) DEFAULT '0' COMMENT '用户活动自动清空违规天数    0 为用不清空',
  `reservation_clear_day` int(11) DEFAULT '0' COMMENT '用户预约自动清空违规天数    0 为用不清空',
  `contest_clear_day` int(11) DEFAULT '0' COMMENT '用户线上大赛自动清空违规天数   0 为用不清空',
  `create_time` datetime DEFAULT NULL COMMENT '数据上次更新时间',
  `change_time` datetime DEFAULT NULL COMMENT '数据上次更新时间',
  `scenic` int(11) DEFAULT '0' COMMENT '用户景点打卡可违规次数',
  `scenic_clear_day` int(11) DEFAULT '0' COMMENT '用户景点打卡自动清空违规天数  0 为用不清空',
  `study_room_reservation` int(11) DEFAULT '0' COMMENT '用户预约可违规次数',
  `study_room_reservation_clear_day` int(11) DEFAULT '0' COMMENT '用户预约自动清空违规天数    0 为用不清空',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='违规设置';

-- ----------------------------
-- Records of re_violate_config
-- ----------------------------
INSERT INTO `re_violate_config` VALUES ('1', '3', '0', '3', '4', '0', '4', null, '2023-11-09 13:48:17', '0', '0', '0', '0');

-- ----------------------------
-- Table structure for `re_volunteer_apply`
-- ----------------------------
DROP TABLE IF EXISTS `re_volunteer_apply`;
CREATE TABLE `re_volunteer_apply` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL COMMENT '岗位名称',
  `username` varchar(50) DEFAULT NULL COMMENT '姓名',
  `sex` tinyint(1) DEFAULT NULL COMMENT '性别   1 男  2 女 ',
  `birth` date DEFAULT NULL COMMENT '信息必填参数   1、姓名 2、性别 3、生日  4、身份证号码  5、名族 6、政治面貌  7、健康状态  8、联系电话  9、毕业院校  10、专业  11、工作单位 12、教育程度  13、个人简介  14、特长  ',
  `id_card` varchar(18) DEFAULT NULL COMMENT '身份证号码',
  `nation` varchar(100) DEFAULT NULL COMMENT '民族',
  `politics` varchar(100) DEFAULT NULL COMMENT '政治面貌',
  `health` tinyint(1) unsigned zerofill DEFAULT '1' COMMENT '健康状态  1 良好  2 较好 3 较差',
  `tel` varchar(11) DEFAULT NULL COMMENT '电话号码',
  `school` varchar(100) DEFAULT NULL COMMENT '毕业院校',
  `major` varchar(100) DEFAULT NULL COMMENT '专业',
  `unit` varchar(100) DEFAULT NULL COMMENT '工作单位',
  `degree` varchar(30) DEFAULT NULL COMMENT '教育程度',
  `address` varchar(200) DEFAULT NULL COMMENT '地址',
  `intro` text COMMENT '个人简介',
  `specialty` varchar(1000) DEFAULT NULL COMMENT '特长',
  `status` tinyint(1) DEFAULT '4' COMMENT '报名状态    1. 已通过  2.已取消  3.已拒绝  4.审核中',
  `reason` varchar(1000) DEFAULT NULL COMMENT '拒绝原因',
  `manage_id` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '开始时间',
  `expire_time` datetime DEFAULT NULL COMMENT '过期时间',
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='志愿者申请';

-- ----------------------------
-- Records of re_volunteer_apply
-- ----------------------------

-- ----------------------------
-- Table structure for `re_volunteer_apply_intention`
-- ----------------------------
DROP TABLE IF EXISTS `re_volunteer_apply_intention`;
CREATE TABLE `re_volunteer_apply_intention` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '志愿者id',
  `intention_id` int(10) unsigned DEFAULT NULL COMMENT '意向id',
  `volunteer_id` int(10) DEFAULT NULL COMMENT '志愿者id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='志愿者活动用户关联服务意向表';

-- ----------------------------
-- Records of re_volunteer_apply_intention
-- ----------------------------

-- ----------------------------
-- Table structure for `re_volunteer_apply_time`
-- ----------------------------
DROP TABLE IF EXISTS `re_volunteer_apply_time`;
CREATE TABLE `re_volunteer_apply_time` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '志愿者id',
  `volunteer_id` int(10) DEFAULT NULL COMMENT '志愿者id',
  `times_id` int(10) DEFAULT NULL COMMENT '星期几   1-7',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='志愿者活动用户关联服务时间表';

-- ----------------------------
-- Records of re_volunteer_apply_time
-- ----------------------------

-- ----------------------------
-- Table structure for `re_volunteer_notice`
-- ----------------------------
DROP TABLE IF EXISTS `re_volunteer_notice`;
CREATE TABLE `re_volunteer_notice` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `content` text COMMENT '简介',
  `create_time` datetime DEFAULT NULL COMMENT '开始时间',
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='志愿者须知';

-- ----------------------------
-- Records of re_volunteer_notice
-- ----------------------------

-- ----------------------------
-- Table structure for `re_volunteer_position`
-- ----------------------------
DROP TABLE IF EXISTS `re_volunteer_position`;
CREATE TABLE `re_volunteer_position` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL COMMENT '岗位名称',
  `intro` text COMMENT '简介',
  `is_reader` tinyint(1) DEFAULT '2' COMMENT '打卡是否需要绑定读者证  1 是  2 否',
  `real_info` varchar(1000) DEFAULT NULL COMMENT '报名所需信息 多个 | 链接    1、姓名 2、性别 3、生日  4、身份证号码  5、民族 6、政治面貌  7、健康状态  8、联系电话  9、毕业院校  10、专业  11、工作单位 12、教育程度  13、个人简介  14、特长  15、地址  ',
  `real_info_must` varchar(1000) DEFAULT NULL COMMENT '信息必填参数 多个 | 链接     1、姓名 2、性别 3、生日  4、身份证号码  5、民族 6、政治面貌  7、健康状态  8、联系电话  9、毕业院校  10、专业  11、工作单位 12、教育程度  13、个人简介  14、特长  15、用户地址 ',
  `expire_time` datetime DEFAULT NULL COMMENT '岗位有效期截止时间',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '删除  1 正常 2删除',
  `is_play` tinyint(1) DEFAULT '2' COMMENT '是否发布 1.发布 2.未发布    默认2',
  `create_time` datetime DEFAULT NULL COMMENT '开始时间',
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='志愿者岗位';

-- ----------------------------
-- Records of re_volunteer_position
-- ----------------------------

-- ----------------------------
-- Table structure for `re_volunteer_service_intention`
-- ----------------------------
DROP TABLE IF EXISTS `re_volunteer_service_intention`;
CREATE TABLE `re_volunteer_service_intention` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT '服务意向',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除     1  正常  2 删除',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='志愿者服务意向';

-- ----------------------------
-- Records of re_volunteer_service_intention
-- ----------------------------

-- ----------------------------
-- Table structure for `re_volunteer_service_time`
-- ----------------------------
DROP TABLE IF EXISTS `re_volunteer_service_time`;
CREATE TABLE `re_volunteer_service_time` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `times` varchar(50) DEFAULT NULL COMMENT '服务意向',
  `is_del` tinyint(1) DEFAULT '1' COMMENT '是否删除     1  正常  2 删除',
  `create_time` datetime DEFAULT NULL,
  `change_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='志愿者服务意向';

-- ----------------------------
-- Records of re_volunteer_service_time
-- ----------------------------

-- ----------------------------
-- Table structure for `re_volunteer_service_time_log`
-- ----------------------------
DROP TABLE IF EXISTS `re_volunteer_service_time_log`;
CREATE TABLE `re_volunteer_service_time_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '积分明细id',
  `user_id` int(11) unsigned DEFAULT NULL COMMENT '用户id/团长',
  `account_id` int(11) unsigned DEFAULT NULL COMMENT '用户id/团长',
  `service_time` int(10) DEFAULT NULL COMMENT '消耗或增加的积分     1 ， -1',
  `type` varchar(20) DEFAULT NULL COMMENT '类型',
  `intro` varchar(500) DEFAULT NULL COMMENT '说明',
  `manage_id` int(11) DEFAULT NULL COMMENT '积分操作人',
  `create_time` datetime DEFAULT NULL COMMENT '新增时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='服务时长明细表';

-- ----------------------------
-- Records of re_volunteer_service_time_log
-- ----------------------------
