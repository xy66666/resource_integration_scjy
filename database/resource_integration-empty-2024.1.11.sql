/*
 Navicat Premium Data Transfer

 Source Server         : 贝图项目整合系统
 Source Server Type    : MySQL
 Source Server Version : 50743
 Source Host           : 47.110.241.53:3306
 Source Schema         : resource_integration_scjy

 Target Server Type    : MySQL
 Target Server Version : 50743
 File Encoding         : 65001

 Date: 11/01/2024 11:26:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for re_access_num
-- ----------------------------
DROP TABLE IF EXISTS `re_access_num`;
CREATE TABLE `re_access_num`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `web_num` int(10) NOT NULL DEFAULT 0 COMMENT 'web端浏览量',
  `wx_num` int(10) DEFAULT 0 COMMENT '微信端浏览量',
  `total_num` int(11) DEFAULT 0 COMMENT '总浏览量',
  `yyy` int(4) DEFAULT NULL COMMENT '年',
  `mmm` tinyint(2) DEFAULT NULL COMMENT '月',
  `ddd` tinyint(2) DEFAULT NULL COMMENT '日',
  `create_time` datetime(0) DEFAULT NULL COMMENT '数据创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 137 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '访问统计' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_activity
-- ----------------------------
DROP TABLE IF EXISTS `re_activity`;
CREATE TABLE `re_activity`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '活动名称',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT 'default/default_activity.png' COMMENT '活动封面',
  `type_id` int(11) DEFAULT NULL COMMENT '类型id',
  `tag_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '标签id   多个用逗号拼接',
  `apply_start_time` datetime(0) DEFAULT NULL COMMENT '活动报名开始时间',
  `apply_end_time` datetime(0) DEFAULT NULL COMMENT '活动报名结束时间',
  `start_time` datetime(0) DEFAULT NULL COMMENT '活动开始时间',
  `end_time` datetime(0) DEFAULT NULL COMMENT '活动结束时间',
  `cancel_end_time` int(11) DEFAULT NULL COMMENT '用户可取消活动预约的最后期限(以活动开始时间往前计算）  单位分钟  0表示不限制',
  `is_long` tinyint(1) DEFAULT 2 COMMENT '是否是长期活动   1 是   2 否 （默认）',
  `everyday_start_time` time(0) DEFAULT NULL COMMENT '是长期活动的   每日开始时间 段',
  `everyday_end_time` time(0) DEFAULT NULL COMMENT '是长期活动的   每日结束时间 段',
  `start_age` mediumint(3) DEFAULT 0 COMMENT '开始年龄  默认0   两个都是 0 则表示不需要年龄限制',
  `end_age` mediumint(3) DEFAULT 0 COMMENT '结束年龄  默认0',
  `astrict_sex` tinyint(1) DEFAULT 3 COMMENT '性别限制   1.限男性  2.限女性  3.不限(默认)',
  `is_reader` tinyint(1) DEFAULT 2 COMMENT '报名是否需要绑定读者证  1 是  2 否',
  `is_qr` tinyint(1) DEFAULT 2 COMMENT '是否需要二维码进场   1.是   2.否(默认）',
  `is_check` tinyint(1) DEFAULT 2 COMMENT '报名是否需要审核   1.是   2.否(默认）',
  `is_continue` tinyint(1) DEFAULT 2 COMMENT '取消报名后是否可以继续报名   1.是   2.否(默认）',
  `is_real` tinyint(1) DEFAULT 2 COMMENT '是否需要用户真实信息 1需要 2不需要 默认2',
  `real_info` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '需要验证的真实信息id用|连接起来    1、姓名   2 、身份证号码  3、电话号码   4、读者证号码    5、真实人物头像   6、备注  7 单位名称 8 性别 9 年龄  10、附件 ',
  `province` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '省',
  `city` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '市',
  `district` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '区',
  `street` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '街道',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '活动地址',
  `tel` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '活动联系方式',
  `contacts` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系人',
  `number` int(11) DEFAULT 0 COMMENT '活动名额 0为不限制报名',
  `pattern` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '报名方式',
  `apply_number` int(11) DEFAULT 0 COMMENT '已报名人数',
  `is_apply` tinyint(1) DEFAULT 1 COMMENT '是否需要报名 1.是  2.否  默认1  否：参与人数可不写',
  `objects` tinyint(1) DEFAULT 1 COMMENT '参赛对象  1个人，2团队  默认1',
  `intro` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '活动内容',
  `stadium_id` int(11) DEFAULT NULL COMMENT '场馆id',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员',
  `browse_num` int(11) DEFAULT 0 COMMENT '浏览量  默认0',
  `lon` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '经度',
  `lat` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '纬度',
  `is_play` tinyint(1) DEFAULT 1 COMMENT '是否发布 1.发布 2.未发布    默认1',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1.正常  2.已删除  默认1',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `qr_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '活动二维码引用地址',
  `qr_code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '二维码 code',
  `is_recom` tinyint(1) DEFAULT 2 COMMENT '是否推荐为 热门活动  1 是  2 否 （默认）',
  `push_num` int(1) DEFAULT 0 COMMENT '发布次数',
  `link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '跳转外链',
  `sign_way` tinyint(1) DEFAULT 2 COMMENT '扫码方式  1 、2者都可以  2、用户自主扫码  3、管理员设备扫码  ',
  `sign_qr_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '活动二维码引用地址',
  `sign_qr_code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '二维码 code',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '活动表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_activity_apply
-- ----------------------------
DROP TABLE IF EXISTS `re_activity_apply`;
CREATE TABLE `re_activity_apply`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证号id',
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `status` tinyint(2) DEFAULT 0 COMMENT '报名状态    1.已通过  2.已取消  3.已拒绝  4.审核中',
  `reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '拒绝理由',
  `sign_num` int(11) DEFAULT 0 COMMENT '签到次数  默认0    如果是短期活动  签到一次就表示已签到',
  `username` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '姓名',
  `id_card` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '身份证',
  `tel` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '电话',
  `reader_id` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '读者证',
  `img` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '头像',
  `is_violate` tinyint(1) DEFAULT 1 COMMENT '是否违规  1正常  2 违规',
  `violate_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '违规原因',
  `violate_time` datetime(0) DEFAULT NULL COMMENT '违规时间',
  `score` int(10) DEFAULT 0 COMMENT '积分信息',
  `accessory` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '附件  ',
  `accessory_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '附件名',
  `remark` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注，最多100字',
  `unit` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  `sex` tinyint(1) DEFAULT 3 COMMENT '性别  1男  2女',
  `manage_id` int(11) DEFAULT NULL COMMENT '操作管理员id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '报名时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 416 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '活动报名表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_activity_collect
-- ----------------------------
DROP TABLE IF EXISTS `re_activity_collect`;
CREATE TABLE `re_activity_collect`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '活动收藏表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_activity_signs
-- ----------------------------
DROP TABLE IF EXISTS `re_activity_signs`;
CREATE TABLE `re_activity_signs`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `apply_id` int(11) DEFAULT NULL COMMENT '申请id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `date` date DEFAULT NULL COMMENT '报名时期 年月日',
  `yyy` smallint(4) DEFAULT NULL COMMENT '年',
  `mmm` tinyint(2) DEFAULT NULL COMMENT '月',
  `ddd` tinyint(2) DEFAULT NULL COMMENT '日',
  `create_time` datetime(0) DEFAULT NULL COMMENT '数据插入时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '活动用户签到表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_activity_tag
-- ----------------------------
DROP TABLE IF EXISTS `re_activity_tag`;
CREATE TABLE `re_activity_tag`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '标签名称',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除 1.正常 2.删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '活动标签表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_activity_type
-- ----------------------------
DROP TABLE IF EXISTS `re_activity_type`;
CREATE TABLE `re_activity_type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '活动类型名称',
  `is_del` tinyint(255) DEFAULT 1 COMMENT '是否删除 1.正常 2.删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '数据插入时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '数据上次更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '活动类型表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity`;
CREATE TABLE `re_answer_activity`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '标题',
  `node` tinyint(2) DEFAULT 1 COMMENT '活动类型    1 独立活动  2 单位联盟   3 区域联盟   ',
  `pattern` tinyint(2) DEFAULT NULL COMMENT '答题模式  1  馆内答题形式  2 轮次排名形式 （按轮次排名）  3 爬楼梯形式',
  `prize_form` tinyint(1) DEFAULT 1 COMMENT '获奖情况  1 排名  2 抽奖',
  `is_ushare` tinyint(1) DEFAULT 1 COMMENT '是否阅享用户  1  是  2 否',
  `real_info` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '填写信息  如果是阅享用户，则无效    1、姓名  2、电话   3、身份证号码  4、读者证   多个  | 连接',
  `is_need_unit` tinyint(1) DEFAULT 2 COMMENT '是否需要选择图书馆   1 需要  2 不需要 （单活动模式，默认不需要）',
  `img` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT 'default/default_readshare_activity.png',
  `total_floor` int(11) DEFAULT NULL COMMENT '爬楼梯梯模式，总楼层',
  `number` int(11) DEFAULT 1 COMMENT '每日答题次数    除轮次外 （一轮为一次，不管有多少题），其余一道题就是一次',
  `answer_number` int(11) DEFAULT 1 COMMENT '除轮次排名外（每次多少题）,单纯排名不需要此参数，其余都是多少题获得一次抽奖机会，馆内答题就是馆内答题数量',
  `is_loop` tinyint(1) DEFAULT 0 COMMENT '是否可以循环，只对 场馆答题 有效    1可以   2 不可以',
  `is_show_list` tinyint(1) DEFAULT 1 COMMENT '是否显示列表 1显示 2 不显示  单位联盟 是否显示单位列表  区域联盟 是否显示区域和单位列表',
  `share_number` int(11) DEFAULT NULL COMMENT '分享获取答题次数，每日几次',
  `answer_time` int(11) DEFAULT 0 COMMENT '每题答题时间   单位 秒',
  `answer_rule` tinyint(1) DEFAULT 1 COMMENT '答题规则   1、专属题和公共题 混合  2、优先回答专属题',
  `rule_type` tinyint(1) DEFAULT NULL COMMENT '规则方式   1 内容   2外链',
  `rule` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '规则内容     rule_type  为1 这里是编辑器内容   2 这里是链接',
  `start_time` datetime(0) DEFAULT NULL COMMENT '活动开始时间',
  `end_time` datetime(0) DEFAULT NULL COMMENT '活动结束时间',
  `answer_start_time` datetime(0) DEFAULT NULL COMMENT '开始答题时间',
  `answer_end_time` datetime(0) DEFAULT NULL COMMENT '结束答题时间',
  `lottery_start_time` datetime(0) DEFAULT NULL COMMENT '抽奖开始时间',
  `lottery_end_time` datetime(0) DEFAULT NULL COMMENT '抽奖结束时间',
  `invite_code` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '邀请码',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除   1 正常 2 已删除',
  `is_play` tinyint(1) DEFAULT 2 COMMENT '是否发布   1 已发布  2 未发布',
  `manage_id` int(11) DEFAULT NULL,
  `browse_num` int(10) DEFAULT 0 COMMENT '浏览量',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL,
  `qr_code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `qr_url` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `is_show_small_rank` tinyint(1) DEFAULT 2 COMMENT '排名活动，是否显示分榜排名   1 显示  2 不显示',
  `share_title` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '分享标题',
  `share_img` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '分享图片',
  `is_show_address` tinyint(1) DEFAULT 1 COMMENT '是否需要填写  1 需要  2 不需要',
  `technical_support` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '成都贝图科技有限公司' COMMENT '技术支持,如果不填，则前端不显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '阅享竞答活动' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_area
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_area`;
CREATE TABLE `re_answer_activity_area`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL,
  `name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '区域名称',
  `img` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '区域名称',
  `intro` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '区域简介',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除   1 正常 2 已删除',
  `order` int(11) DEFAULT 0 COMMENT '排序  数字越小越靠前    1在最前面',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '活动区域表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_browse_count
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_browse_count`;
CREATE TABLE `re_answer_activity_browse_count`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL,
  `unit_id` int(11) DEFAULT 0 COMMENT '单位id   0 代表全部',
  `date` date DEFAULT NULL,
  `hour` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '小时',
  `number` int(11) DEFAULT NULL,
  `create_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 64 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '活动浏览量统计表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_gift
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_gift`;
CREATE TABLE `re_answer_activity_gift`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `unit_id` int(11) DEFAULT NULL COMMENT '单位id   无单位表示公共奖品',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '礼品名称',
  `img` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '礼品图片',
  `intro` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '礼品简介',
  `total_number` int(11) DEFAULT NULL COMMENT '礼品总数量',
  `use_number` int(11) DEFAULT 0 COMMENT '礼品已被抽中数量',
  `percent` decimal(6, 2) DEFAULT NULL COMMENT '中奖率(百分比)',
  `price` decimal(6, 2) DEFAULT NULL COMMENT '红包大小',
  `type` tinyint(1) DEFAULT NULL COMMENT '1文化红包 2精美礼品',
  `way` tinyint(1) DEFAULT 0 COMMENT '领取方式   1 自提  2邮递   红包无此选项',
  `start_time` datetime(0) DEFAULT NULL COMMENT '自提开始时间',
  `end_time` datetime(0) DEFAULT NULL COMMENT '自提结束时间',
  `tel` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系电话',
  `contacts` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系人',
  `province` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '省',
  `city` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '市',
  `district` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '区',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '详细地址',
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '自提备注',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否被删除 1正常 2已删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `unit_index`(`unit_id`) USING BTREE,
  INDEX `activity_index`(`act_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '礼物表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_gift_config
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_gift_config`;
CREATE TABLE `re_answer_activity_gift_config`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `percent_way` tinyint(1) DEFAULT 1 COMMENT '概率方式   1 单个奖品独立概率方式    2 所以奖品总概率形式',
  `gift_max_number_day` int(11) DEFAULT NULL COMMENT '礼物每日发送奖品最大数量',
  `red_max_number_day` int(11) DEFAULT NULL COMMENT '礼物每日发送奖品最大数量',
  `user_gift_number_day` int(11) DEFAULT 1 COMMENT '用户当日可获得多少实物礼品',
  `user_red_number_day` int(11) DEFAULT 1 COMMENT '用户当日可获得多少红包礼品',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '礼品领取总配置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_gift_public
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_gift_public`;
CREATE TABLE `re_answer_activity_gift_public`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `unit_id` int(11) DEFAULT NULL COMMENT '单位id   无单位表示公共奖品',
  `type` tinyint(1) DEFAULT NULL COMMENT '礼物类型  1文化红包 2精美礼品',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '礼品名称',
  `way` tinyint(1) DEFAULT NULL COMMENT '操作方式  1 投放  2追加',
  `number` int(11) DEFAULT NULL COMMENT '礼品数量',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否被删除 1正常 2已删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `unit_index`(`unit_id`) USING BTREE,
  INDEX `activity_index`(`act_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '礼物公示表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_gift_record
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_gift_record`;
CREATE TABLE `re_answer_activity_gift_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `unit_id` int(11) DEFAULT NULL COMMENT '单位id',
  `gift_id` int(11) DEFAULT NULL COMMENT '礼物id',
  `before_number` int(11) DEFAULT 0 COMMENT '最加之前数量    ',
  `later_number` int(11) DEFAULT 0 COMMENT '追加之后数量',
  `gift_number` int(11) DEFAULT 0 COMMENT '礼物数量',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态 1 增加 2减少',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '记录时间',
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `activity_index`(`act_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '礼物变动记录表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_gift_time
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_gift_time`;
CREATE TABLE `re_answer_activity_gift_time`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `start_time` datetime(0) DEFAULT NULL COMMENT '奖品开始发放时间   ',
  `end_time` datetime(0) DEFAULT NULL COMMENT '结束时间   和开始时间对应不允许跨天',
  `type` tinyint(4) DEFAULT NULL COMMENT '奖品类型 1文化红包 2精美礼品',
  `number` int(11) DEFAULT NULL COMMENT '当前时间段发放数量',
  `user_gift_number` int(11) DEFAULT 1 COMMENT '用户当前时间段可获得多少礼品',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除 1正常 2删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '礼品领取排班表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_invite
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_invite`;
CREATE TABLE `re_answer_activity_invite`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `user_guid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `invite_code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '活动邀请码' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_limit_address
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_limit_address`;
CREATE TABLE `re_answer_activity_limit_address`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '每个活动独立',
  `province` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '省',
  `city` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '市',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1 正常  2删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '删除、修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '答题活动限制参与地址' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_problem
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_problem`;
CREATE TABLE `re_answer_activity_problem`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `unit_id` int(11) DEFAULT NULL COMMENT '题目所属单位   没有代表是公共题库',
  `title` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '标题',
  `img` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '题目图片',
  `answer_id` int(10) DEFAULT NULL COMMENT '正确答案id',
  `type` tinyint(1) DEFAULT 1 COMMENT '题目类型  1 单选 （默认）2 填空题',
  `state` tinyint(1) DEFAULT 1 COMMENT '1正常 2被禁用',
  `analysis` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '解析',
  `is_show_analysis` tinyint(1) DEFAULT 2 COMMENT '是否显示解析  1、显示 2、不显示',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否被删除 1正常 2已删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `unit_index`(`unit_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2568 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '活动题目表\r\n' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_problem_answer
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_problem_answer`;
CREATE TABLE `re_answer_activity_problem_answer`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pro_id` int(10) DEFAULT NULL COMMENT '标题id',
  `content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '答案内容',
  `status` tinyint(2) DEFAULT 2 COMMENT '答题状态  1代表正确  2代表错误  默认2',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `title_index`(`pro_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7702 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '活动答案表\r\n' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_resource
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_resource`;
CREATE TABLE `re_answer_activity_resource`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '标题',
  `theme_color` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '主题色',
  `progress_color` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '进度条颜色',
  `invite_color` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '邀请码颜色',
  `theme_text_color` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '主题文字色',
  `warning_color` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '警告颜色',
  `error_color` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '错误颜色',
  `resource_path` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '资源路径，中间半截',
  `is_change` tinyint(1) DEFAULT 2 COMMENT '资源是否变化，包括图片资源是否改变  1 未变化  2 有变化',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '活动资源表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_share
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_share`;
CREATE TABLE `re_answer_activity_share`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL,
  `user_guid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '区域名称',
  `date` date DEFAULT NULL COMMENT '日期',
  `number` int(11) DEFAULT 0 COMMENT '分享次数',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '活动分享次数表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_unit
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_unit`;
CREATE TABLE `re_answer_activity_unit`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL COMMENT '区域id  若区域id存在，则每个区域下的单位唯一，若区域不存在，则整个活动下的单位唯一',
  `nature` tinyint(2) DEFAULT NULL COMMENT '单位性质   1 图书馆  2 文化馆  3 博物馆',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '单位名称',
  `img` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT 'default/default_readshare_unit.png',
  `intro` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '单位简介',
  `tel` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系电话',
  `contacts` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系人姓名',
  `words` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '单位赠言，最多50字',
  `letter` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '首字母',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除   1 正常 2 已删除',
  `order` int(11) DEFAULT 0 COMMENT '排序  数字越小越靠前    1在最前面',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL,
  `qr_code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `qr_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '二维码链接',
  `is_default` tinyint(1) DEFAULT 2 COMMENT '是否默认 1 默认 2 不默认',
  `province` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '省',
  `city` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '市',
  `district` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '区',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '活动地址',
  `lon` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '经度',
  `lat` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '纬度',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '活动单位表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_unit_user_number
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_unit_user_number`;
CREATE TABLE `re_answer_activity_unit_user_number`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `act_id` int(10) DEFAULT NULL COMMENT '活动id',
  `user_guid` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `unit_id` int(11) DEFAULT 0 COMMENT '单位id',
  `date` date DEFAULT NULL,
  `number` int(11) DEFAULT 0 COMMENT '次数',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `act_id`(`act_id`) USING BTREE,
  INDEX `user_guid`(`user_guid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户回答单位题目 次数表（3种模式都在里面）\r\n' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_user_answer_record
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_user_answer_record`;
CREATE TABLE `re_answer_activity_user_answer_record`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `guid` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '每次抽区问题时随机id',
  `user_guid` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `act_id` int(10) DEFAULT NULL COMMENT '标题id',
  `unit_id` int(11) DEFAULT 0 COMMENT '单位id',
  `problem_id` int(11) DEFAULT NULL COMMENT '问题id',
  `answer_id` int(11) DEFAULT NULL COMMENT '答案id',
  `answer` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '填空题答案',
  `status` tinyint(2) DEFAULT 0 COMMENT '答题状态  1代表正确  2代表错误   3 超时回答（算错误）默认2',
  `times` int(11) DEFAULT 0 COMMENT '答题所需时长 单位秒',
  `create_time` datetime(0) DEFAULT NULL,
  `expire_time` datetime(0) DEFAULT NULL COMMENT '回答问题超时时间  后台处理问题，允许 2s的延时',
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `act_id`(`act_id`) USING BTREE,
  INDEX `user_guid`(`user_guid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户答题记录表（馆内形式答题）\r\n' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_user_answer_total_number
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_user_answer_total_number`;
CREATE TABLE `re_answer_activity_user_answer_total_number`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_guid` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户id',
  `act_id` int(10) DEFAULT NULL COMMENT '活动id',
  `unit_id` int(10) DEFAULT 0 COMMENT '单位id  如果 是 0则代表总排名  不是则是馆排名',
  `answer_number` int(10) DEFAULT 0 COMMENT '应该答题总数量 默认 0',
  `correct_number` int(10) DEFAULT 0 COMMENT '答题正确数量  默认 0',
  `accuracy` decimal(5, 2) DEFAULT 0.00 COMMENT '答题正确率   百分数',
  `times` int(11) DEFAULT 0 COMMENT '答题所需时长 单位秒',
  `create_time` datetime(0) DEFAULT NULL COMMENT '选题时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '最后一次答题时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `act_id`(`act_id`) USING BTREE,
  INDEX `user_guid`(`user_guid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户答题排名表（馆内形式答题）\r\n' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_user_correct_answer_record
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_user_correct_answer_record`;
CREATE TABLE `re_answer_activity_user_correct_answer_record`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `correct_number_id` int(11) DEFAULT NULL COMMENT '竞答次数id',
  `user_guid` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `guid` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '每次抽区问题时随机id',
  `act_id` int(10) DEFAULT NULL COMMENT '标题id',
  `unit_id` int(11) DEFAULT 0,
  `level` int(11) DEFAULT 1 COMMENT '第几题',
  `problem_id` int(11) DEFAULT NULL COMMENT '问题id',
  `answer_id` int(11) DEFAULT NULL COMMENT '答案id',
  `answer` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '填空题答案',
  `status` tinyint(2) DEFAULT 0 COMMENT '答题状态  1代表正确  2代表错误   3 超时回答（算错误）默认2',
  `times` int(11) DEFAULT 0 COMMENT '答题所需时长 单位秒',
  `create_time` datetime(0) DEFAULT NULL,
  `expire_time` datetime(0) DEFAULT NULL COMMENT '回答问题超时时间  后台处理问题，允许 2s的延时',
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `act_id`(`act_id`) USING BTREE,
  INDEX `user_guid`(`user_guid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 61 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户答题记录表（竞答（轮次记录）形式答题）\r\n' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_user_correct_number
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_user_correct_number`;
CREATE TABLE `re_answer_activity_user_correct_number`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_guid` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户id',
  `act_id` int(10) DEFAULT NULL COMMENT '活动id',
  `unit_id` int(11) DEFAULT 0,
  `date` date DEFAULT NULL COMMENT '年',
  `answer_number` int(10) DEFAULT 0 COMMENT '应该答题总数量 默认 0',
  `have_answer_number` int(10) DEFAULT 0 COMMENT '总共已答题目数量  默认 0',
  `correct_number` int(10) DEFAULT 0 COMMENT '答题正确数量  默认 0',
  `accuracy` decimal(5, 2) DEFAULT 0.00 COMMENT '答题正确率   百分数',
  `status` tinyint(1) DEFAULT 0 COMMENT '是否回复 0 未回复  1 已回复',
  `times` int(11) DEFAULT 0 COMMENT '答题所需时长 单位秒',
  `create_time` datetime(0) DEFAULT NULL COMMENT '选题时间',
  `expire_time` datetime(0) DEFAULT NULL COMMENT '总过期时间，针对用户中途放弃答题的情况   在所有时间上增加  30秒，用户延时消耗',
  `change_time` datetime(0) DEFAULT NULL COMMENT '最后一次答题时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_guid`(`user_guid`) USING BTREE,
  INDEX `act_id`(`act_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户竞答轮次表（一轮一条记录）\r\n' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_user_correct_total_number
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_user_correct_total_number`;
CREATE TABLE `re_answer_activity_user_correct_total_number`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_guid` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户id',
  `act_id` int(10) DEFAULT NULL COMMENT '活动id',
  `unit_id` int(10) DEFAULT 0 COMMENT '单位id  如果 是 0则代表总排名  不是则是馆排名',
  `answer_number` int(10) DEFAULT 0 COMMENT '应该答题总数量 默认 0',
  `correct_number` int(10) DEFAULT 0 COMMENT '答题正确数量  默认 0   取一天中的最高的一次来计算',
  `accuracy` decimal(5, 2) DEFAULT 0.00 COMMENT '答题正确率   百分数',
  `times` int(11) DEFAULT 0 COMMENT '答题所需时长 单位秒',
  `create_time` datetime(0) DEFAULT NULL COMMENT '选题时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '最后一次答题时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `act_id`(`act_id`) USING BTREE,
  INDEX `user_guid`(`user_guid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户竞答总数据（取一天中的最高的一轮来计算）表\r\n' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_user_gift
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_user_gift`;
CREATE TABLE `re_answer_activity_user_gift`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `user_guid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '用户guid',
  `gift_id` int(11) DEFAULT NULL COMMENT '礼物id',
  `order_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '订单号',
  `use_prize_record_id` int(11) DEFAULT NULL COMMENT '用户使用抽奖机会id',
  `price` decimal(6, 2) DEFAULT NULL COMMENT '红包大小',
  `state` tinyint(1) DEFAULT 1 COMMENT '礼物状态 1未发放 2已发放 3未发货 4邮递中 5已到货 6 未领取  7 已领取',
  `date` date DEFAULT NULL COMMENT '获奖日期',
  `hour` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '小时',
  `type` tinyint(1) DEFAULT NULL COMMENT '1文化红包 2精美礼品',
  `create_time` datetime(0) DEFAULT NULL COMMENT '红包创建时间',
  `pay_time` datetime(0) DEFAULT NULL COMMENT '红包到账时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '到货（领取、发货）时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `act_id`(`act_id`) USING BTREE,
  INDEX `user_guid`(`user_guid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户获取礼物表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_user_prize
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_user_prize`;
CREATE TABLE `re_answer_activity_user_prize`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `user_guid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '用户guid',
  `number` int(11) DEFAULT 0 COMMENT '总次数',
  `use_number` int(11) DEFAULT 0 COMMENT '剩余次数',
  `create_time` datetime(0) DEFAULT NULL COMMENT '红包创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '到货时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `act_id`(`act_id`) USING BTREE,
  INDEX `user_guid`(`user_guid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户抽奖机会总表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_user_prize_record
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_user_prize_record`;
CREATE TABLE `re_answer_activity_user_prize_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `pattern` tinyint(1) DEFAULT NULL COMMENT '答题模式（获奖方式）  1  馆内答题形式    3 爬楼梯形式',
  `answer_record_id` int(11) DEFAULT NULL COMMENT '对应答题记录id',
  `user_guid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '用户guid',
  `create_time` datetime(0) DEFAULT NULL COMMENT '红包创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '到货时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `act_id`(`act_id`) USING BTREE,
  INDEX `user_guid`(`user_guid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户获取抽奖机会记录表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_user_stairs_answer_record
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_user_stairs_answer_record`;
CREATE TABLE `re_answer_activity_user_stairs_answer_record`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `guid` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '每次抽区问题时随机id',
  `user_guid` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `act_id` int(10) DEFAULT NULL COMMENT '标题id',
  `unit_id` int(11) DEFAULT 0,
  `floor` int(11) DEFAULT 1 COMMENT '楼层  ',
  `problem_id` int(11) DEFAULT NULL COMMENT '问题id',
  `answer_id` int(11) DEFAULT NULL COMMENT '答案id',
  `answer` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '填空题答案',
  `status` tinyint(2) DEFAULT 0 COMMENT '答题状态  1代表正确  2代表错误   3 超时回答（算错误）默认2',
  `times` int(11) DEFAULT 0 COMMENT '答题所需时长 单位秒',
  `create_time` datetime(0) DEFAULT NULL,
  `expire_time` datetime(0) DEFAULT NULL COMMENT '回答问题超时时间  后台处理问题，允许 2s的延时',
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `act_id`(`act_id`) USING BTREE,
  INDEX `user_guid`(`user_guid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 277 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户答题记录表（楼梯形式答题）\r\n' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_user_stairs_total_number
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_user_stairs_total_number`;
CREATE TABLE `re_answer_activity_user_stairs_total_number`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_guid` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户id',
  `act_id` int(10) DEFAULT NULL COMMENT '活动id',
  `unit_id` int(10) DEFAULT 0 COMMENT '单位id  如果 是 0则代表总排名  不是则是馆排名',
  `answer_number` int(10) DEFAULT 0 COMMENT '应该答题总数量 默认 0',
  `correct_number` int(10) DEFAULT 0 COMMENT '答题正确数量（对一次就是一层）  默认 0',
  `accuracy` decimal(5, 2) DEFAULT 0.00 COMMENT '答题正确率   百分数',
  `times` int(11) DEFAULT 0 COMMENT '答题所需时长 单位秒',
  `create_time` datetime(0) DEFAULT NULL COMMENT '选题时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '最后一次答题时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `act_id`(`act_id`) USING BTREE,
  INDEX `user_guid`(`user_guid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户答题总楼层表（楼梯形式答题）\r\n' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_user_unit
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_user_unit`;
CREATE TABLE `re_answer_activity_user_unit`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_guid` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户id',
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '姓名',
  `tel` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '电话号码',
  `id_card` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '身份证',
  `reader_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '读者证',
  `unit_id` int(11) DEFAULT NULL COMMENT '单位id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '删除、修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 66 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户所属单位表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_user_use_prize_record
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_user_use_prize_record`;
CREATE TABLE `re_answer_activity_user_use_prize_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '订单号',
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `user_guid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '用户guid',
  `status` tinyint(1) DEFAULT NULL COMMENT '是否中奖  1 中奖  2 未中奖',
  `date` date DEFAULT NULL,
  `create_time` datetime(0) DEFAULT NULL COMMENT '红包创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '到货时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `act_id`(`act_id`) USING BTREE,
  INDEX `user_guid`(`user_guid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户使用抽奖机会表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_answer_activity_word_resource
-- ----------------------------
DROP TABLE IF EXISTS `re_answer_activity_word_resource`;
CREATE TABLE `re_answer_activity_word_resource`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '标题',
  `invitation_prompt` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '邀请码提示',
  `relevant_information` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '所属馆填写信息',
  `unit_detail_answer` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '单位详情答题数提示',
  `stair_display` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '楼梯显示数据',
  `answer_success1` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '答题成功2',
  `answer_success2` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '答题成功3',
  `answer_success3` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '答题成功4',
  `answer_error1` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '答题错误2',
  `answer_error2` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '答题错误3',
  `answer_error3` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '答题错误4',
  `answer_error4` tinyint(1) DEFAULT 2 COMMENT '是否显示解析  1、显示 2、不显示',
  `grade1` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '我的成绩1',
  `grade2` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '我的成绩2',
  `grade3` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '我的成绩3',
  `ranking1` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '排名1',
  `ranking2` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '排名2',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '活动资源表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_app_invite_code
-- ----------------------------
DROP TABLE IF EXISTS `re_app_invite_code`;
CREATE TABLE `re_app_invite_code`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注信息',
  `content` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '邀请码',
  `intro` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '备注',
  `start_time` datetime(0) DEFAULT NULL COMMENT '有效期开始时间',
  `end_time` datetime(0) DEFAULT NULL COMMENT '有效期结束时间',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除 1 正常  2删除',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '应用邀请码' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_app_invite_code_behavior
-- ----------------------------
DROP TABLE IF EXISTS `re_app_invite_code_behavior`;
CREATE TABLE `re_app_invite_code_behavior`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type_id` tinyint(3) DEFAULT NULL COMMENT '类型id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `invite_code_id` int(11) DEFAULT NULL COMMENT '邀请码id',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1168 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '应用邀请码用户行为分析' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_app_view_gray
-- ----------------------------
DROP TABLE IF EXISTS `re_app_view_gray`;
CREATE TABLE `re_app_view_gray`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '名称',
  `manage_id` int(11) DEFAULT NULL COMMENT '配置的管理员id',
  `start_time` datetime(0) DEFAULT NULL COMMENT '清除违规时间，在此之前的违规都不算，为空，则都计算在内',
  `end_time` datetime(0) DEFAULT NULL COMMENT '清除违规时间，在此之前的违规都不算，为空，则都计算在内',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1 正常 2已删除',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '应用显示灰色表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_banner
-- ----------------------------
DROP TABLE IF EXISTS `re_banner`;
CREATE TABLE `re_banner`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'banner名称',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'Bannner图片引用地址',
  `link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'banner的跳转链接',
  `type` tinyint(2) DEFAULT 1 COMMENT '显示位置   1 首页     2 商城',
  `is_play` tinyint(2) DEFAULT 1 COMMENT '是否发布   1 发布  2 撤销',
  `sort` int(10) DEFAULT 0 COMMENT '排序  数字越大，排在越前面',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '1 正常  2 删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '数据插入时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '数据上一次更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'banner显示控制表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_bind_log
-- ----------------------------
DROP TABLE IF EXISTS `re_bind_log`;
CREATE TABLE `re_bind_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户微信open_id',
  `account` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '账号',
  `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '密码',
  `state` tinyint(1) DEFAULT 1 COMMENT '状态 1绑定  2 解绑 ',
  `create_time` datetime(0) DEFAULT NULL COMMENT '绑定时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '解绑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '微信公众号绑定读者证日志表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_book_home_budget_log
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_budget_log`;
CREATE TABLE `re_book_home_budget_log`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(2) DEFAULT NULL COMMENT '类型  增加还是减少金额   1增加   2减少',
  `kind` tinyint(1) DEFAULT 1 COMMENT '种类   1   经费   2 邮费',
  `money` decimal(8, 2) DEFAULT NULL COMMENT '增加减少的金额',
  `total_money` decimal(8, 2) DEFAULT 0.00 COMMENT '更改之前总金额',
  `after_total_money` decimal(8, 2) DEFAULT NULL COMMENT '更改之前总金额',
  `change_before_money` decimal(8, 2) DEFAULT NULL COMMENT '更改之前剩余金额',
  `change_after_money` decimal(8, 2) DEFAULT NULL COMMENT '更改之后剩余金额',
  `way` tinyint(1) DEFAULT 1 COMMENT '邮费设置的模式（只对类型14有效）  1 独立模式（只生效一次）  2 循环模式 （每月循环）',
  `start_time` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `end_time` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `times` int(10) DEFAULT 0 COMMENT '限制次数    在此时间段内限制次数',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '添加时间',
  `start_month` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '如果 type为14 必选   其余不需要此参数  循环模式 （开始的月份）',
  `month_number` tinyint(2) DEFAULT NULL COMMENT '如果 type为14 必选   其余不需要此参数  循环模式 几个月循环一次（默认1）范围 1~12',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '采购预算增加的历史表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_book_home_collect
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_collect`;
CREATE TABLE `re_book_home_collect`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `book_id` int(11) DEFAULT NULL COMMENT '书籍id',
  `type` tinyint(1) DEFAULT NULL COMMENT '类型 1 新书   2 馆藏书',
  `create_time` datetime(0) DEFAULT NULL COMMENT '收藏时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '书籍收藏表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_book_home_manage_shop
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_manage_shop`;
CREATE TABLE `re_book_home_manage_shop`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员id',
  `shop_id` int(11) DEFAULT NULL COMMENT '书店id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '收藏时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '图书到家管理员管辖书店表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_book_home_order
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_order`;
CREATE TABLE `re_book_home_order`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单号',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证号id 2020 05 09 新增 ',
  `shop_id` int(11) DEFAULT NULL COMMENT '书店 id',
  `price` decimal(5, 2) DEFAULT NULL COMMENT '邮费  价格',
  `dis_price` decimal(5, 2) DEFAULT NULL COMMENT '折扣后的价格  （默认与实际价格一致）   用户实际支付金额',
  `discount` float(5, 2) DEFAULT 100.00 COMMENT '折扣   默认 100  没有折扣  百分比',
  `is_pay` tinyint(1) DEFAULT 2 COMMENT '1 未支付 2 已支付    3支付异常， 4 订单已失效     5 .已退款    6.已同意（后台单纯同意） 7 已拒绝（后台单纯同意）  8已发货（数据就增加到用户采购列表） 9 无法发货  10已取消（用户自己取消）',
  `create_time` datetime(0) DEFAULT NULL COMMENT '加入时间（订单时间）',
  `expire_time` datetime(0) DEFAULT NULL COMMENT '支付过期时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间  （支付时间，支付异常时间，订单失效后的时间）',
  `payment` tinyint(1) DEFAULT 1 COMMENT '支付方式  1 微信支付   2 系统预算扣除',
  `agree_time` datetime(0) DEFAULT NULL COMMENT '同意时间、拒绝时间',
  `deliver_time` datetime(0) DEFAULT NULL COMMENT '发货时间、无法发货时间',
  `refund_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退款单号',
  `refund_time` datetime(0) DEFAULT NULL COMMENT '退款时间',
  `refund_remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退款备注',
  `refund_manage_id` int(11) DEFAULT NULL COMMENT '退款管理员id',
  `tracking_number` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '快递单号',
  `deliver_manage_id` int(11) DEFAULT NULL COMMENT '确认发货的管理员id',
  `cancel_time` datetime(0) DEFAULT NULL COMMENT '取消时间，用户自己取消订单，只能在未审核前取消',
  `cancel_remark` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '取消备注',
  `settle_state` tinyint(1) DEFAULT 3 COMMENT '邮费结算状态（这里只标记就行）  1 已结算   3 未结算',
  `settle_sponsor_time` datetime(0) DEFAULT NULL COMMENT '结算时间（结算方结算）',
  `settle_affirm_time` datetime(0) DEFAULT NULL COMMENT '确认结算时间',
  `settle_sponsor_manage_id` int(11) DEFAULT NULL COMMENT '书店结算的管理员id',
  `settle_affirm_manage_id` int(11) DEFAULT NULL COMMENT '图书馆结算的管理员id',
  `type` tinyint(1) DEFAULT 1 COMMENT '类型 1 新书（默认）   2 馆藏书',
  `is_sign` tinyint(1) DEFAULT 1 COMMENT '1 未发货 2 已发货 3已派送 4已签收 5拒绝签收',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '新书订单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_book_home_order_address
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_order_address`;
CREATE TABLE `re_book_home_order_address`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL COMMENT '订单id',
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户姓名',
  `tel` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '电话号码',
  `province` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '省',
  `city` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `district` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '区',
  `street` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '街道',
  `address` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '详细地址',
  `send_username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '发货人名称',
  `send_tel` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '发货人电话',
  `send_province` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `send_city` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `send_district` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `send_street` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `send_address` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `manage_id` int(11) DEFAULT NULL COMMENT '修改管理员id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '删除、修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户订单地址表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_book_home_order_book
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_order_book`;
CREATE TABLE `re_book_home_order_book`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL COMMENT '订单id',
  `book_id` int(11) DEFAULT NULL,
  `barcode_id` int(11) DEFAULT NULL COMMENT '条形码id (馆藏书才有)',
  `type` tinyint(1) DEFAULT 1 COMMENT '类型 1 新书（默认）   2 馆藏书',
  `node` tinyint(1) DEFAULT 3 COMMENT '是否借阅成功   1成功  2 失败  3未处理（默认）',
  `create_time` datetime(0) DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '成功或失败的时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '新书订单书籍表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_book_home_order_pay
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_order_pay`;
CREATE TABLE `re_book_home_order_pay`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单号',
  `pay_time` datetime(0) DEFAULT NULL COMMENT '支付时间',
  `payment` tinyint(1) DEFAULT 1 COMMENT '支付方式  1 微信支付',
  `trade_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '微信支付订单号',
  `buyer_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '买家微信支付分配的终端设备号',
  `total_amount` decimal(8, 2) DEFAULT NULL COMMENT '订单总金额,订单总金额，单位为分',
  `receipt_amount` decimal(8, 2) DEFAULT NULL COMMENT '实收金额,商家在交易中实际收到的款项',
  `buyer_pay_amount` decimal(8, 2) DEFAULT NULL COMMENT '付款金额,现金支付金额订单现金支付金额，详见支付金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单支付表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_book_home_order_refund
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_order_refund`;
CREATE TABLE `re_book_home_order_refund`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `refund_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退款单号',
  `refund_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '微信退款单号',
  `refund_time` datetime(0) DEFAULT NULL COMMENT '退款时间',
  `price` decimal(5, 2) DEFAULT NULL COMMENT '退款金额',
  `payment` tinyint(1) DEFAULT NULL COMMENT '退款路径  1 微信',
  `status` tinyint(1) DEFAULT NULL COMMENT '退款结果  1 成功  2失败',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员id',
  `error_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退款失败的code',
  `error_msg` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退款失败的消息提示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单退款日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_book_home_postage_set
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_postage_set`;
CREATE TABLE `re_book_home_postage_set`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) DEFAULT NULL COMMENT '类型  1 新书  2 馆藏书',
  `number` mediumint(5) DEFAULT NULL COMMENT '本数',
  `price` decimal(5, 2) DEFAULT NULL COMMENT '邮费    根据本数来设置      本数必须从小到大依次排',
  `create_time` datetime(0) DEFAULT NULL COMMENT '设置时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '邮费设置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_book_home_postal_return
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_postal_return`;
CREATE TABLE `re_book_home_postal_return`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tracking_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单号 （一次性归还多本，订单号一致）',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证号id',
  `purchase_id` int(11) DEFAULT NULL COMMENT '采购记录 id',
  `courier_id` int(11) DEFAULT NULL COMMENT '快递名称',
  `barcode` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '条形码',
  `create_time` datetime(0) DEFAULT NULL COMMENT '加入时间（归还时间）',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间  （确认归还时间）',
  `status` tinyint(1) DEFAULT 3 COMMENT '归还状态   1 已确认归还  2 已取消归还(管理员取消)  3 归还中  4已撤销（用户自己操作）',
  `return_manage_id` int(11) DEFAULT NULL COMMENT '确认归还的管理员id',
  `book_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '书名',
  `author` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作者',
  `price` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '金额',
  `book_num` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '索书号',
  `isbn` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'ISBN号',
  `press` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '出版社',
  `pre_time` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '出版时间',
  `img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '/default/default_book.png',
  `borrow_time` datetime(0) DEFAULT NULL COMMENT '到期时间 ',
  `expire_time` datetime(0) DEFAULT NULL COMMENT '到期时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '新书邮递归还表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_book_home_postal_rev
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_postal_rev`;
CREATE TABLE `re_book_home_postal_rev`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trace_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '邮政运单号',
  `send_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '推送方标识，JDPT：寄递平台',
  `provice_no` int(11) DEFAULT NULL COMMENT '数据生产的省公司代码，对不能确定的省份取99',
  `msg_kind` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '消息类别，接口编码（JDPT_BOTTLE_TRACE）',
  `serial_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '消息唯一序列号，相对于消息类别的唯一流水号，对于本类消息，是一个不重复的ID值，不同类别的消息，该值会重复',
  `receive_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '接收方标识，BOTTLE：贝图科技',
  `batch_no` int(11) DEFAULT NULL COMMENT '批次号',
  `data_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '数据类型1-JSON   2-XML   3-压缩后的Byte[]',
  `data_digest` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '签名结果',
  `trace_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '轨迹信息',
  `receive_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '推送信息原本',
  `create_time` datetime(0) DEFAULT NULL COMMENT '接收时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '邮政轨迹推送信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_book_home_purchase
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_purchase`;
CREATE TABLE `re_book_home_purchase`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `book_id` int(11) DEFAULT NULL COMMENT '书籍id',
  `barcode_id` int(11) DEFAULT NULL COMMENT '条形码id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证号id',
  `shop_id` int(11) DEFAULT NULL COMMENT '书店id',
  `order_id` int(11) DEFAULT NULL COMMENT '订单id',
  `price` decimal(5, 2) DEFAULT NULL COMMENT '金额   采购时的金额',
  `isbn` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'isbn号',
  `barcode` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '条形码',
  `type` tinyint(1) DEFAULT 1 COMMENT '类型 1 新书  2 馆藏书',
  `create_time` datetime(0) DEFAULT NULL COMMENT '采购时间',
  `expire_time` datetime(0) DEFAULT NULL COMMENT '到期时间   默认是采购时间后面一个月',
  `return_time` datetime(0) DEFAULT NULL COMMENT '归还时间  (预归还时间)',
  `return_state` tinyint(1) DEFAULT 1 COMMENT '归还状态  1 借阅中  2 已归还   3 归还中',
  `settle_state` tinyint(1) DEFAULT NULL COMMENT '结算状态  1 已结算  2 结算中  3 未结算',
  `settle_sponsor_time` datetime(0) DEFAULT NULL COMMENT '结算发起时间',
  `settle_affirm_time` datetime(0) DEFAULT NULL COMMENT '结算确认时间',
  `settle_sponsor_manage_id` int(11) DEFAULT NULL COMMENT '结算发起的管理员id',
  `settle_affirm_manage_id` int(11) DEFAULT NULL COMMENT '结算确认的管理员id',
  `overdue_money` decimal(5, 2) DEFAULT NULL COMMENT '逾期金额     超期已交金额，归还后才有的金额，没归还，列表显示的时候，自己算',
  `is_drop` tinyint(1) DEFAULT 1 COMMENT '丢失状态  1 正常  2 已丢失',
  `drop_money` decimal(5, 2) DEFAULT NULL COMMENT '丢失后补偿的金额',
  `total_money` decimal(5, 2) DEFAULT NULL COMMENT '总赔付金额',
  `is_pay` tinyint(1) DEFAULT 4 COMMENT '逾期金额和丢失赔付金额 缴纳状态 1 已缴纳   2 免除违约金   3 未缴纳   4 无违约金',
  `pay_time` datetime(0) DEFAULT NULL COMMENT '缴纳逾期金额时间',
  `pur_manage_id` int(11) DEFAULT NULL COMMENT '采购管理员id',
  `return_manage_id` int(11) DEFAULT NULL COMMENT '归还书籍管理员id',
  `money_manage_id` int(11) DEFAULT NULL COMMENT '收取滞纳金的管理员id   如果未逾期也未丢失  就没有此数据',
  `node` tinyint(1) DEFAULT NULL COMMENT '是否借阅成功   1成功  2 失败',
  `is_dispose` tinyint(1) DEFAULT 2 COMMENT '是否处理  1 已处理  2 未处理（默认）',
  `dispose_time` datetime(0) DEFAULT NULL COMMENT '处理时间',
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `barcode`(`barcode`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '书店书籍采购表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_book_home_purchase_set
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_purchase_set`;
CREATE TABLE `re_book_home_purchase_set`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(2) DEFAULT NULL COMMENT '类型 1 图书采购经费预算（总预算） 2.逾期滞纳金（多少元每天）3.每人每次可采购图书本数4.每人每次可采购图书金额 5.每人每月可采购图书本数6.每人每月可采购图书金额7.每人每年可采购图书本数8.每人每年可采购图书金额 9.单本金额上限10. 复本数不允许超过 几本11.多少年之前的书不允许采购（0 表示全部 都可以采购）12.读者一次性借阅的天数13.书籍损坏赔偿原价的 多少（百分比） 14 图书采购邮费预算（总预算） 15.积分扣除比例  （欠费缴纳） 16.复本数不允许超过 几本（本系统）17.每人同种书不允许超过 几本（增对新书有效）18.有效复本书计算排除的馆藏地点代码（多个逗号拼接） 19.馆藏书可借阅的馆藏地点代码（多个逗号拼接）',
  `number` float(10, 2) DEFAULT NULL COMMENT '数量（金额）  存放数字',
  `content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '存放文本内容',
  `create_time` datetime(0) DEFAULT NULL COMMENT '设置时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  `way` tinyint(1) DEFAULT NULL COMMENT '邮费设置的模式（只对类型14有效）  1 独立模式（只生效一次）  2 循环模式 （每月循环）',
  `start_time` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '开始时间  只对活动邮费预算有效',
  `end_time` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '结束时间  只对活动邮费预算有效',
  `times` int(10) DEFAULT NULL COMMENT '限制次数    在此时间段内限制次数',
  `start_month` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '如果 type为14 必选   其余不需要此参数  循环模式 （开始的月份）',
  `month_number` tinyint(2) DEFAULT NULL COMMENT '如果 type为14 必选   其余不需要此参数  循环模式 几个月循环一次（默认1）范围 1~12',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '采购设置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_book_home_return_address
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_return_address`;
CREATE TABLE `re_book_home_return_address`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) DEFAULT NULL COMMENT '书店id，只增对  way为 3有效',
  `way` tinyint(1) DEFAULT NULL COMMENT '方式  1 现场归还   2 邮递归还   3 发货地址（只能有一个）',
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '发货人名称',
  `tel` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '发货人联系电话号码',
  `province` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '省',
  `city` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '市',
  `district` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '区',
  `street` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '街道',
  `address` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '详细地址',
  `intro` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '简介',
  `type` tinyint(1) DEFAULT NULL COMMENT '类型 1 新书（默认）   2 馆藏书',
  `dispark_time` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '开发时间（一段话）',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1正常  2删除',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL COMMENT '删除、修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '图书到家邮递地址、归还地址设置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_book_home_return_address_explain
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_return_address_explain`;
CREATE TABLE `re_book_home_return_address_explain`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `way` tinyint(1) DEFAULT NULL COMMENT '方式   1 现场归还   2 邮递归还',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '说明内容',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL COMMENT '删除、修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '图书到家归还地址说明表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_book_home_return_record
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_return_record`;
CREATE TABLE `re_book_home_return_record`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '读者证号',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证号id',
  `book_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '书名',
  `author` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作者',
  `price` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '价格',
  `book_num` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '索书号',
  `isbn` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'ISBN号',
  `press` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '出版社',
  `pre_time` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `barcode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '条形码',
  `borrow_time` datetime(0) DEFAULT NULL COMMENT '借阅时间',
  `expire_time` datetime(0) DEFAULT NULL COMMENT '到期时间',
  `return_time` datetime(0) DEFAULT NULL COMMENT '归还时间 ',
  `return_manage_id` int(11) DEFAULT NULL COMMENT '归还书籍管理员id',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户归还书籍记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_book_home_schoolbag
-- ----------------------------
DROP TABLE IF EXISTS `re_book_home_schoolbag`;
CREATE TABLE `re_book_home_schoolbag`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `book_id` int(11) DEFAULT NULL COMMENT '书籍id',
  `barcode_id` int(11) DEFAULT NULL COMMENT '条形码id（只有馆藏书才有此字段）',
  `type` tinyint(1) DEFAULT 1 COMMENT '类型 1 新书   2 馆藏书',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '加入时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间(购买时间)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '新书书包表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_book_types
-- ----------------------------
DROP TABLE IF EXISTS `re_book_types`;
CREATE TABLE `re_book_types`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '名称',
  `classify` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '编码',
  `pid` int(10) UNSIGNED DEFAULT NULL COMMENT 'pid',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '路径',
  `level` int(10) UNSIGNED DEFAULT NULL COMMENT '等级',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 45837 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '中图分类法' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_branch_access_num
-- ----------------------------
DROP TABLE IF EXISTS `re_branch_access_num`;
CREATE TABLE `re_branch_access_num`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0 COMMENT '分馆id',
  `type` tinyint(1) DEFAULT 1 COMMENT '类型   1.点击量   2. 下载量  3.浏览人次   4.评估年度 ',
  `number` int(11) DEFAULT 0 COMMENT '访问量',
  `yyy` int(4) DEFAULT NULL COMMENT '年',
  `mmm` tinyint(2) DEFAULT NULL COMMENT '月',
  `ddd` tinyint(2) DEFAULT NULL COMMENT '日',
  `create_time` datetime(0) DEFAULT NULL COMMENT '数据创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '数字阅读资源访问统计' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_branch_account
-- ----------------------------
DROP TABLE IF EXISTS `re_branch_account`;
CREATE TABLE `re_branch_account`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT 0 COMMENT '分馆id',
  `username` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '名称',
  `account` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '账号',
  `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '密码',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除 1 正常   2 删除',
  `manage_id` int(11) DEFAULT NULL,
  `create_time` datetime(0) DEFAULT NULL COMMENT '数据创建时间',
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '分馆用户登录账号和密码' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_branch_info
-- ----------------------------
DROP TABLE IF EXISTS `re_branch_info`;
CREATE TABLE `re_branch_info`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '单页面id',
  `branch_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '场馆名称',
  `is_main` tinyint(1) DEFAULT 1 COMMENT '是否总馆  1 总馆 2 分馆',
  `img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '封面图片',
  `intro` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '场馆简介',
  `dispark_time` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '开放时间（一段文字）',
  `transport_line` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '交通线路 ',
  `province` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '省',
  `city` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '市',
  `district` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '区县',
  `street` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '街道',
  `address` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '详细地址',
  `tel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '联系方式',
  `contacts` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '联系人',
  `lon` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '经度',
  `lat` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '纬度',
  `borrow_notice` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '借阅须知',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1 正常  2 删除',
  `browse_num` int(11) DEFAULT 0 COMMENT '浏览量',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '总分馆简介表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_code_borrow_log
-- ----------------------------
DROP TABLE IF EXISTS `re_code_borrow_log`;
CREATE TABLE `re_code_borrow_log`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `place_code_id` int(11) DEFAULT NULL COMMENT '场所码id',
  `user_id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户id',
  `book_name` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '书名',
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '书的类别   22大类',
  `small_type` int(11) DEFAULT NULL COMMENT '小类型',
  `author` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '作者',
  `price` float(5, 2) DEFAULT NULL COMMENT '价格',
  `press` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '出版社',
  `isbn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT 'Isbn号',
  `barcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '条码号	主要是处理图书馆的书籍，图书馆的系统都有统一的条码',
  `img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'default/default_book.png' COMMENT '图片',
  `thumb_img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '封面缩略图',
  `pre_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '出版时间',
  `callno` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '索书号',
  `classno` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `intro` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '简介',
  `handler` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '是否是管理员操作	YES_APP(手机管理员)，YES_WEB（web管理员）,NO，YES_APP，YES_WEB的话，user_id就为管理员的id',
  `expire_time` datetime(0) DEFAULT NULL COMMENT '预计归还时间    时间戳',
  `return_time` datetime(0) DEFAULT NULL COMMENT '归还时间   日期时间格式',
  `create_time` datetime(0) DEFAULT NULL COMMENT '借阅时间',
  `change_time` datetime(0) DEFAULT NULL,
  `rdrrecno` int(10) DEFAULT NULL COMMENT '读者记录号',
  `lib_id` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '所属馆',
  `lib_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '所属馆',
  `cur_local` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '当前所在地',
  `cur_local_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '当前所在地',
  `cur_lib` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '当前所在馆',
  `cur_lib_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '当前所在馆',
  `owner_local` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '所属地',
  `owner_local_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '所属地',
  `recno` int(10) DEFAULT NULL COMMENT '馆藏记录号',
  `bibrecno` int(10) DEFAULT NULL COMMENT '书目记录号',
  `account` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '读者证号',
  `status` tinyint(1) DEFAULT 2 COMMENT '状态  1  已归还  2借阅中',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '码上借 书籍借阅记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_code_guide_exhibition
-- ----------------------------
DROP TABLE IF EXISTS `re_code_guide_exhibition`;
CREATE TABLE `re_code_guide_exhibition`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '展览名',
  `img` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `start_time` datetime(0) DEFAULT NULL COMMENT '展览开始时间',
  `end_time` datetime(0) DEFAULT NULL COMMENT '展览结束时间',
  `intro` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '简介详情',
  `qr_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '活动二维码引用地址',
  `qr_code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '二维码 code',
  `browse_num` int(10) UNSIGNED DEFAULT 0 COMMENT '浏览量',
  `manage_id` int(11) DEFAULT NULL,
  `is_play` tinyint(1) DEFAULT 2 COMMENT '是否发布  1 发布  2 未发布',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1 正常  2 已删除',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 71 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '作品展览表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_code_guide_production
-- ----------------------------
DROP TABLE IF EXISTS `re_code_guide_production`;
CREATE TABLE `re_code_guide_production`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '作品名',
  `exhibition_id` int(11) DEFAULT NULL COMMENT '展览id',
  `type_id` int(11) DEFAULT NULL COMMENT '类型id',
  `intro` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '简介',
  `img` varchar(1200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图片,多张用 | 拼接',
  `thumb_img` varchar(1200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '作品正文内容',
  `voice` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '作品音频地址，可以用内容直接转换成语音',
  `voice_name` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '音频名称',
  `video` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '视频资源',
  `video_name` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '视频名称',
  `browse_num` int(10) UNSIGNED DEFAULT 0 COMMENT '浏览量',
  `qr_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '活动二维码引用地址',
  `qr_code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '二维码 code',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1 正常  2 已删除',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL,
  `manage_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 66 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '作品表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_code_guide_production_type
-- ----------------------------
DROP TABLE IF EXISTS `re_code_guide_production_type`;
CREATE TABLE `re_code_guide_production_type`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '类型名',
  `intro` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `is_del` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否删除  1 正常   2 删除',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '作品类型表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_code_place
-- ----------------------------
DROP TABLE IF EXISTS `re_code_place`;
CREATE TABLE `re_code_place`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '志愿者id',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '姓名',
  `way` tinyint(1) DEFAULT 1 COMMENT '定位方式  1 经纬度   2 地址',
  `lon` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '经度',
  `lat` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '纬度',
  `province` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '身份证号码',
  `city` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '出生日期',
  `district` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '学校',
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '地址|住址',
  `qr_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `qr_url` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `intro` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '简介',
  `distance` int(11) DEFAULT 0 COMMENT '扫码有效距离，单位米  只有admin才能配置，若未配置，则使用配置环境数据',
  `is_del` tinyint(1) UNSIGNED DEFAULT 1 COMMENT '是否删除   1.正常   2.已删除',
  `is_play` tinyint(1) DEFAULT 1 COMMENT '是否发布  1 已发布  2 未发布',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '码上借场所码' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_code_return_log
-- ----------------------------
DROP TABLE IF EXISTS `re_code_return_log`;
CREATE TABLE `re_code_return_log`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `place_code_id` int(1) DEFAULT NULL COMMENT '场所码id',
  `book_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '书名',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '用户id',
  `type` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '书的类别	参照book_types表    22大类',
  `small_type` int(11) DEFAULT NULL COMMENT '小类型',
  `author` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `price` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '价格',
  `press` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '出版社',
  `isbn` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `barcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '增对后台添加图书馆的数据',
  `pre_time` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '出版时间',
  `callno` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '索书号',
  `classno` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `intro` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '简介',
  `img` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'default/default_book.png',
  `thumb_img` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '封面缩略图',
  `borrow_time` datetime(0) DEFAULT NULL COMMENT '添加时间',
  `is_del` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'NO' COMMENT '是否删除，用户自己删除',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL,
  `rdrrecno` int(10) DEFAULT NULL COMMENT '读者记录号',
  `lib_id` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '所属馆',
  `lib_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '所属馆',
  `cur_local` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '当前所在地',
  `cur_local_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '当前所在地',
  `cur_lib` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '当前所在馆',
  `cur_lib_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '当前所在馆',
  `owner_local` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '所属地',
  `owner_local_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '所属地',
  `recno` int(10) DEFAULT NULL COMMENT '馆藏记录号',
  `bibrecno` int(10) DEFAULT NULL COMMENT '书目记录号',
  `account` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '读者证号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '码上借 书籍归还记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_competite_activity
-- ----------------------------
DROP TABLE IF EXISTS `re_competite_activity`;
CREATE TABLE `re_competite_activity`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '征集活动id',
  `node` tinyint(1) DEFAULT 1 COMMENT '举办方式   1 独立活动  2 多馆联合   ',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '活动名称',
  `web_img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'web封面图片',
  `app_img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'default/default_contest_activity.png' COMMENT 'app封面图片',
  `host_handle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '主办单位',
  `bear_handle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '承办单位   弃用',
  `tel` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '咨询方式',
  `contacts` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '联系人',
  `con_start_time` datetime(0) DEFAULT NULL COMMENT '投稿开始时间',
  `con_end_time` datetime(0) DEFAULT NULL COMMENT '投稿结束时间',
  `vote_start_time` datetime(0) DEFAULT NULL COMMENT '投票开始时间',
  `vote_end_time` datetime(0) DEFAULT NULL COMMENT '投票结束时间',
  `is_reader` tinyint(1) DEFAULT 2 COMMENT '报名是否需要绑定读者证   1.是   2.否(默认）',
  `vote_way` tinyint(1) DEFAULT NULL COMMENT '投票方式   1  总票数方式   2 每日投票方式',
  `number` int(10) DEFAULT 0 COMMENT '票数',
  `deliver_way` tinyint(1) DEFAULT 1 COMMENT '投递方式  1 全部  2 前端用户自己上传  3 团队账号上传',
  `appraise_way` tinyint(1) DEFAULT 1 COMMENT '评选方式  1、全部  2、用户投票  3 专家评选 ',
  `originals` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '原创申明 配置  在每个馆也可单独设置  ',
  `promise` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '作者承诺',
  `intro` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '简介',
  `rule_content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '活动规则内容',
  `manage_id` int(10) UNSIGNED DEFAULT NULL COMMENT '管理员id',
  `is_play` tinyint(1) DEFAULT 2 COMMENT '是否发布 1.发布 2.未发布    默认1',
  `is_del` tinyint(1) UNSIGNED DEFAULT 1 COMMENT '是否删除   1.正常   2.已删除',
  `browse_num` int(11) DEFAULT 0 COMMENT '浏览量',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  `real_info` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '需要验证的真实信息id用|连接起来    1、姓名   2 、身份证号码  3、电话号码   4、读者证号码    5、微信号 6、单位名称 7、指导老师  8、指导老师联系方式',
  `check_manage_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '审核管理员id，多个，逗号拼接，逐个审评，一旦有人上传作品，则不能更改审核人',
  `type_id` int(11) DEFAULT NULL COMMENT '类型id',
  `tag_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '标签id   多个用逗号拼接',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '线上大赛活动表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_competite_activity_database
-- ----------------------------
DROP TABLE IF EXISTS `re_competite_activity_database`;
CREATE TABLE `re_competite_activity_database`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '征集活动作品表',
  `name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作品数据库名称',
  `img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作品图片地址 node=1',
  `intro` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作品简介',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  `is_play` tinyint(1) DEFAULT 1 COMMENT '是否发布 1 发布  2 未发布',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1 正常  2 删除',
  `browse_num` int(11) DEFAULT 0 COMMENT '浏览量',
  `sort` int(10) DEFAULT 0 COMMENT '排序  数字越大，排在越前面',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '线上大赛作品数据库表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_competite_activity_declare
-- ----------------------------
DROP TABLE IF EXISTS `re_competite_activity_declare`;
CREATE TABLE `re_competite_activity_declare`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '征集活动id',
  `con_id` int(11) DEFAULT NULL COMMENT '征集活动id',
  `lib_id` int(11) DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '内容名称',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '线上大赛申明表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_competite_activity_ebook
-- ----------------------------
DROP TABLE IF EXISTS `re_competite_activity_ebook`;
CREATE TABLE `re_competite_activity_ebook`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '征集活动作品表',
  `name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作品数据库电子书标题',
  `img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作品图片地址 node=1',
  `intro` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作品简介',
  `author` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作者名称 ',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1 正常  2 删除',
  `sort` int(10) DEFAULT 0 COMMENT '排序  数字越大，排在越前面',
  `browse_num` int(11) DEFAULT 0 COMMENT '浏览量',
  `is_play` tinyint(1) DEFAULT 1 COMMENT '是否发布 1 发布  2 未发布',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '征集活动作品电子书表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_competite_activity_group
-- ----------------------------
DROP TABLE IF EXISTS `re_competite_activity_group`;
CREATE TABLE `re_competite_activity_group`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `con_id` int(11) DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '团队名称',
  `account` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '团队账号',
  `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '密码',
  `img` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT 'default/default_readshacqse_unit.png',
  `intro` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '单位简介',
  `tel` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系电话',
  `contacts` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系人姓名',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除   1 正常 2 已删除',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL,
  `token` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '大赛团队账号表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_competite_activity_group_login_log
-- ----------------------------
DROP TABLE IF EXISTS `re_competite_activity_group_login_log`;
CREATE TABLE `re_competite_activity_group_login_log`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `login_time` datetime(0) DEFAULT NULL COMMENT '登陆时间',
  `login_addr` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '登陆地址',
  `login_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '登陆ip',
  `is_success` tinyint(1) DEFAULT 2 COMMENT '是否成功 1 成功  2 失败',
  `is_lock` tinyint(1) DEFAULT 1 COMMENT '是否锁定  1 正常  2 锁定',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员登陆日志表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_competite_activity_tag
-- ----------------------------
DROP TABLE IF EXISTS `re_competite_activity_tag`;
CREATE TABLE `re_competite_activity_tag`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '标签名称',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除 1.正常 2.删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '征集活动标签表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_competite_activity_type
-- ----------------------------
DROP TABLE IF EXISTS `re_competite_activity_type`;
CREATE TABLE `re_competite_activity_type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '征集活动类型名称',
  `is_del` tinyint(255) DEFAULT 1 COMMENT '是否删除 1.正常 2.删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '数据插入时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '数据上次更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '征集活动类型表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_competite_activity_vote
-- ----------------------------
DROP TABLE IF EXISTS `re_competite_activity_vote`;
CREATE TABLE `re_competite_activity_vote`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户投票',
  `con_id` int(10) UNSIGNED DEFAULT NULL COMMENT '征集活动id',
  `works_id` int(10) UNSIGNED DEFAULT NULL COMMENT '作品id',
  `yyy` smallint(4) UNSIGNED DEFAULT NULL COMMENT '年',
  `mmm` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `ddd` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `date` date DEFAULT NULL COMMENT '时间 2019-08-07',
  `user_id` int(10) UNSIGNED DEFAULT NULL COMMENT '用户id',
  `terminal` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '投票终端    web   android   ios  wx',
  `create_time` datetime(0) DEFAULT NULL COMMENT '投票时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户投票' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_competite_activity_works
-- ----------------------------
DROP TABLE IF EXISTS `re_competite_activity_works`;
CREATE TABLE `re_competite_activity_works`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '征集活动作品表',
  `serial_number` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作品编号  大写字母开头   在加8位数字',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作者姓名',
  `tel` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '联系电话',
  `id_card` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '身份证号码',
  `group_id` int(11) DEFAULT NULL COMMENT '团队id',
  `unit` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `wechat_number` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '微信号',
  `reader_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '读者证号',
  `adviser` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '指导老师',
  `adviser_tel` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '指导老师联系方式',
  `type` tinyint(1) DEFAULT 1 COMMENT '类型  1 个人账号  2 团队账号',
  `user_id` int(10) UNSIGNED DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '用户读者证号id',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '名称',
  `con_id` int(10) UNSIGNED DEFAULT NULL COMMENT '征集活动id',
  `type_id` int(10) DEFAULT NULL COMMENT '征集活动类型id',
  `img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作品图片地址 node=1',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '作品正文内容 node=2',
  `voice` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作品音频地址 node=3',
  `voice_name` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '音频名称',
  `video` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '视频资源 node=4',
  `video_name` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '视频名称',
  `intro` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作品简介',
  `cover` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作品封面',
  `width` int(11) DEFAULT 0 COMMENT '宽度',
  `height` int(11) UNSIGNED DEFAULT 0 COMMENT '高度',
  `terminal` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作品上传终端    web   android   ios  wx',
  `browse_num` int(10) UNSIGNED DEFAULT 0 COMMENT '浏览量',
  `vote_num` int(10) UNSIGNED DEFAULT 0 COMMENT '投票量',
  `status` tinyint(1) UNSIGNED DEFAULT 3 COMMENT '状态    1.已通过   2.未通过   3.未审核(默认)  4.已撤销， 5已删除 ',
  `reason` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '拒绝理由',
  `is_look` tinyint(1) DEFAULT 2 COMMENT '是否被查看 1已查看  2未查看',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  `score` int(10) DEFAULT 0 COMMENT '积分',
  `is_violate` tinyint(1) DEFAULT 1 COMMENT '是否违规  1正常 2违规 默认1',
  `violate_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '违规原因',
  `violate_time` datetime(0) DEFAULT NULL COMMENT '违规时间',
  `next_score_manage_id` int(11) DEFAULT NULL COMMENT '下一个打分管理员id',
  `next_check_manage_id` int(11) DEFAULT NULL COMMENT '下一个审核管理员id',
  `review_score` decimal(5, 2) DEFAULT NULL COMMENT '评审打分  多个评审人就是总分',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `serial_number`(`serial_number`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '征集活动作品表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_competite_activity_works_check
-- ----------------------------
DROP TABLE IF EXISTS `re_competite_activity_works_check`;
CREATE TABLE `re_competite_activity_works_check`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '征集活动作品表',
  `works_id` int(11) DEFAULT NULL COMMENT '作品id',
  `check_manage_id` int(11) DEFAULT NULL COMMENT '审核人id',
  `status` tinyint(1) UNSIGNED DEFAULT 3 COMMENT '状态    1.已通过   2.未通过   3.未审核(默认)',
  `reason` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '拒绝理由',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '征集活动作品表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_competite_activity_works_database
-- ----------------------------
DROP TABLE IF EXISTS `re_competite_activity_works_database`;
CREATE TABLE `re_competite_activity_works_database`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '征集活动作品表',
  `database_id` int(11) DEFAULT NULL COMMENT '作品图片地址 node=1',
  `works_id` int(11) DEFAULT NULL COMMENT '作品数据库名称',
  `ebook_id` int(11) DEFAULT NULL COMMENT '电子书id  与  做id二选一',
  `type` tinyint(1) DEFAULT 1 COMMENT '类型  1 作品id  2 电子书id ',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  `sort` int(10) DEFAULT 0 COMMENT '排序  数字越大，排在越前面',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '征集活动作品数据库关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_competite_activity_works_ebook
-- ----------------------------
DROP TABLE IF EXISTS `re_competite_activity_works_ebook`;
CREATE TABLE `re_competite_activity_works_ebook`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '征集活动作品表',
  `ebook_id` int(11) DEFAULT NULL COMMENT '电子书id',
  `img` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作品电子书图片',
  `works_id` int(11) DEFAULT NULL COMMENT '作品id  如果是从作品里面选择的作品',
  `pdf_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'pdf地址  如果是pdf转换为的图片',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除 1 正常  2 删除',
  `sort` int(10) DEFAULT 0 COMMENT '排序  数字越大，排在越前面',
  `type` tinyint(1) DEFAULT 1 COMMENT '类型  1 作品选择  2 图片上传  3 pdf 上传',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '征集活动作品电子书表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_competite_activity_works_score
-- ----------------------------
DROP TABLE IF EXISTS `re_competite_activity_works_score`;
CREATE TABLE `re_competite_activity_works_score`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '征集活动作品表',
  `works_id` int(11) DEFAULT NULL COMMENT '作品id',
  `score_manage_id` int(11) DEFAULT NULL COMMENT '审核人id',
  `review_score` decimal(5, 2) UNSIGNED DEFAULT NULL,
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `serial_number`(`works_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '征集活动作品表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_competite_activity_works_type
-- ----------------------------
DROP TABLE IF EXISTS `re_competite_activity_works_type`;
CREATE TABLE `re_competite_activity_works_type`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '征集活动相关类型',
  `con_id` int(10) UNSIGNED DEFAULT NULL COMMENT '征集活动id',
  `type_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类型名称',
  `limit_num` int(10) UNSIGNED DEFAULT NULL COMMENT '限制该分类作品上传最大数量  0为不限',
  `letter` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '编号首字母',
  `node` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '1' COMMENT '上传文件类型，连接  1.图片(默认)  2.文字  3.音频  4.视频  多个逗号链接',
  `score_manage_id` int(11) DEFAULT NULL COMMENT '评分管理员id  只有当需要评分的活动才有',
  `is_del` tinyint(1) UNSIGNED DEFAULT 1 COMMENT '是否删除   1.正常   2.已删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '征集活动相关类型' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_contest_activity
-- ----------------------------
DROP TABLE IF EXISTS `re_contest_activity`;
CREATE TABLE `re_contest_activity`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '线上大赛活动id',
  `node` tinyint(1) DEFAULT 1 COMMENT '举办方式   1 独立活动  2 多馆联合   ',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '活动名称',
  `web_img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'web封面图片',
  `app_img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'default/default_contest_activity.png' COMMENT 'app封面图片',
  `host_handle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '主办单位',
  `bear_handle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '承办单位   弃用',
  `tel` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '咨询方式',
  `con_start_time` datetime(0) DEFAULT NULL COMMENT '投稿开始时间',
  `con_end_time` datetime(0) DEFAULT NULL COMMENT '投稿结束时间',
  `vote_start_time` datetime(0) DEFAULT NULL COMMENT '投票开始时间',
  `vote_end_time` datetime(0) DEFAULT NULL COMMENT '投票结束时间',
  `is_reader` tinyint(1) DEFAULT 2 COMMENT '报名是否需要绑定读者证   1.是   2.否(默认）',
  `vote_way` tinyint(1) DEFAULT NULL COMMENT '投票方式   1  总票数方式   2 每日投票方式',
  `number` int(10) DEFAULT 0 COMMENT '票数',
  `originals` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '原创申明 配置  在每个馆也可单独设置  ',
  `promise` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '作者承诺',
  `intro` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '简介',
  `rule_content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '活动规则内容',
  `manage_id` int(10) UNSIGNED DEFAULT NULL COMMENT '管理员id',
  `is_play` tinyint(1) DEFAULT 2 COMMENT '是否发布 1.发布 2.未发布    默认1',
  `is_del` tinyint(1) UNSIGNED DEFAULT 1 COMMENT '是否删除   1.正常   2.已删除',
  `browse_num` int(11) DEFAULT 0 COMMENT '浏览量',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  `real_info` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '需要验证的真实信息id用|连接起来    1、姓名   2 、身份证号码  3、电话号码   4、读者证号码    5、微信号 6、单位名称 7、指导老师  8、指导老师联系方式',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '线上大赛活动表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_contest_activity_declare
-- ----------------------------
DROP TABLE IF EXISTS `re_contest_activity_declare`;
CREATE TABLE `re_contest_activity_declare`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '线上大赛活动id',
  `con_id` int(11) DEFAULT NULL COMMENT '大赛id',
  `lib_id` int(11) DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '内容名称',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '线上大赛申明表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_contest_activity_type
-- ----------------------------
DROP TABLE IF EXISTS `re_contest_activity_type`;
CREATE TABLE `re_contest_activity_type`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '线上作品大赛活动相关类型',
  `con_id` int(10) UNSIGNED DEFAULT NULL COMMENT '线上大赛活动id',
  `type_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类型名称',
  `limit_num` int(10) UNSIGNED DEFAULT NULL COMMENT '限制该分类作品上传最大数量  0为不限',
  `letter` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '编号首字母',
  `node` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '1' COMMENT '上传文件类型，连接  1.图片(默认)  2.文字  3.音频  4.视频  多个逗号链接',
  `is_del` tinyint(1) UNSIGNED DEFAULT 1 COMMENT '是否删除   1.正常   2.已删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '线上大赛活动相关类型' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_contest_activity_unit
-- ----------------------------
DROP TABLE IF EXISTS `re_contest_activity_unit`;
CREATE TABLE `re_contest_activity_unit`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `con_id` int(11) DEFAULT NULL,
  `nature` tinyint(2) DEFAULT NULL COMMENT '单位性质   1 图书馆  2 文化馆  3 博物馆',
  `name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '单位名称',
  `img` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT 'default/default_readshare_unit.png',
  `intro` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '单位简介',
  `tel` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系电话',
  `contacts` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系人姓名',
  `originals` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '原创申明  可为空，空则用活动设置的申明',
  `letter` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '首字母',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除   1 正常 2 已删除',
  `order` int(11) DEFAULT 0 COMMENT '排序  数字越小越靠前    1在最前面',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '大赛单位表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_contest_activity_vote
-- ----------------------------
DROP TABLE IF EXISTS `re_contest_activity_vote`;
CREATE TABLE `re_contest_activity_vote`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户投票',
  `con_id` int(10) UNSIGNED DEFAULT NULL COMMENT '线上大赛活动id',
  `works_id` int(10) UNSIGNED DEFAULT NULL COMMENT '作品id',
  `yyy` smallint(4) UNSIGNED DEFAULT NULL COMMENT '年',
  `mmm` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `ddd` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `date` date DEFAULT NULL COMMENT '时间 2019-08-07',
  `user_id` int(10) UNSIGNED DEFAULT NULL COMMENT '用户id',
  `terminal` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '投票终端    web   android   ios  wx',
  `create_time` datetime(0) DEFAULT NULL COMMENT '投票时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 81 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户投票' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_contest_activity_works
-- ----------------------------
DROP TABLE IF EXISTS `re_contest_activity_works`;
CREATE TABLE `re_contest_activity_works`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '线上大赛作品表',
  `serial_number` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作品编号  大写字母开头   在加8位数字',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作者姓名',
  `tel` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '联系电话',
  `id_card` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '身份证号码',
  `unit_id` int(11) DEFAULT NULL COMMENT '单位',
  `unit` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `wechat_number` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '微信号',
  `reader_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '读者证号',
  `adviser` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '指导老师',
  `adviser_tel` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '指导老师联系方式',
  `user_id` int(10) UNSIGNED DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '用户读者证号id',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '名称',
  `con_id` int(10) UNSIGNED DEFAULT NULL COMMENT '线上大赛活动id',
  `type_id` int(10) DEFAULT NULL COMMENT '线上大赛活动类型id',
  `img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作品图片地址 node=1',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '作品正文内容 node=2',
  `voice` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作品音频地址 node=3',
  `voice_name` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '音频名称',
  `video` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '视频资源 node=4',
  `video_name` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '视频名称',
  `intro` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作品简介',
  `cover` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作品封面',
  `width` int(11) DEFAULT 0 COMMENT '宽度',
  `height` int(11) UNSIGNED DEFAULT 0 COMMENT '高度',
  `terminal` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作品上传终端    web   android   ios  wx',
  `browse_num` int(10) UNSIGNED DEFAULT 0 COMMENT '浏览量',
  `vote_num` int(10) UNSIGNED DEFAULT 0 COMMENT '投票量',
  `status` tinyint(1) UNSIGNED DEFAULT 3 COMMENT '状态    1.已通过   2.未通过   3.未审核(默认)  4.已撤销， 5已删除 ',
  `reason` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '拒绝理由',
  `is_look` tinyint(1) DEFAULT 2 COMMENT '是否被查看 1已查看  2未查看',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  `score` int(10) DEFAULT 0 COMMENT '积分',
  `is_violate` tinyint(1) DEFAULT 1 COMMENT '是否违规  1正常 2违规 默认1',
  `violate_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '违规原因',
  `violate_time` datetime(0) DEFAULT NULL COMMENT '违规时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `serial_number`(`serial_number`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '线上大赛作品表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_courier_name
-- ----------------------------
DROP TABLE IF EXISTS `re_courier_name`;
CREATE TABLE `re_courier_name`  (
  `id` int(1) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `create_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '快递公司名称' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_digital_access_num
-- ----------------------------
DROP TABLE IF EXISTS `re_digital_access_num`;
CREATE TABLE `re_digital_access_num`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `digital_id` int(11) DEFAULT 0 COMMENT '数字资源id ',
  `type` tinyint(1) DEFAULT 1 COMMENT '类型   1.点击量   2. 下载量  3.浏览人次   4.评估年度 ',
  `number` int(11) DEFAULT 1 COMMENT '访问量',
  `yyy` int(4) DEFAULT NULL COMMENT '年',
  `mmm` tinyint(2) DEFAULT NULL COMMENT '月',
  `ddd` tinyint(2) DEFAULT NULL COMMENT '日',
  `create_time` datetime(0) DEFAULT NULL COMMENT '数据创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 68 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '数字阅读资源访问统计（单个访问量）' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_digital_read
-- ----------------------------
DROP TABLE IF EXISTS `re_digital_read`;
CREATE TABLE `re_digital_read`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '名称',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图片',
  `url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '链接',
  `type_id` int(11) DEFAULT NULL COMMENT '类型id',
  `sort` int(11) DEFAULT 0 COMMENT '排序 数字越大越靠前',
  `browse_num` int(11) DEFAULT 0 COMMENT '浏览量',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1 正常  2 已删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '数据插入时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '数据上一次更新时间',
  `is_play` tinyint(1) DEFAULT 1 COMMENT '是否发布 1.发布 2.未发布    默认1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '数字阅读表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_digital_type
-- ----------------------------
DROP TABLE IF EXISTS `re_digital_type`;
CREATE TABLE `re_digital_type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '类型名称',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否被删除 1否 2是',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '数字资源类型表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_district
-- ----------------------------
DROP TABLE IF EXISTS `re_district`;
CREATE TABLE `re_district`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL COMMENT '多少年公布的数据',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '地区名称',
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '地区代码',
  `level` tinyint(1) DEFAULT NULL COMMENT '等级 1省 2市 3区 4街道',
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '城乡分类代码',
  `url` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `is_spider` tinyint(1) DEFAULT 0 COMMENT '是否全部爬取 1是 0否',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `level_index`(`level`) USING BTREE,
  INDEX `parent_index`(`parent_id`) USING BTREE,
  INDEX `year_index`(`year`) USING BTREE,
  INDEX `code_index`(`code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 45487 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '省市区街道地址表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_feedback
-- ----------------------------
DROP TABLE IF EXISTS `re_feedback`;
CREATE TABLE `re_feedback`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `username` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `tel` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '电话号码',
  `feedback` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `create_time` datetime(0) DEFAULT NULL,
  `is_look` tinyint(1) DEFAULT 2 COMMENT '是否查看  1 已查看  2 未查看',
  `manage_feedback` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '管理员回复',
  `manage_id` int(11) DEFAULT NULL COMMENT '回复管理id',
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户反馈表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_goods
-- ----------------------------
DROP TABLE IF EXISTS `re_goods`;
CREATE TABLE `re_goods`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '商品名称',
  `type_id` int(11) DEFAULT NULL COMMENT '类型id',
  `img` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '商品封面图片',
  `score` int(11) DEFAULT NULL COMMENT '所需积分',
  `price` decimal(8, 2) DEFAULT NULL COMMENT '除了积分外，还需要多少金额 ',
  `number` int(11) DEFAULT NULL COMMENT '数量',
  `has_number` int(11) DEFAULT 0 COMMENT '已兑换数量',
  `times` int(11) DEFAULT 0 COMMENT '每个用户可兑换次数 默认0次(不限制兑换)',
  `tel` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系方式',
  `contacts` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系人',
  `send_way` tinyint(1) DEFAULT NULL COMMENT '1 自提 2 邮递  3、 2者都可以',
  `start_time` datetime(0) DEFAULT NULL COMMENT '开始兑换时间',
  `end_time` datetime(0) DEFAULT NULL COMMENT '截止兑换时间',
  `pick_end_time` datetime(0) DEFAULT NULL COMMENT '自提截止时间',
  `cancel_end_time` int(11) UNSIGNED DEFAULT 0 COMMENT '支付成功后，可取消的最后期限  单位 分钟',
  `is_play` tinyint(1) DEFAULT 1 COMMENT '1 上架  2下架  默认2',
  `intro` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '简介',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员id',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1 正常  2 删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_goods_img
-- ----------------------------
DROP TABLE IF EXISTS `re_goods_img`;
CREATE TABLE `re_goods_img`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) DEFAULT NULL COMMENT '商品id',
  `img` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '商品图片',
  `thumb_img` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '商品缩略图',
  `create_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品图片表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_goods_order
-- ----------------------------
DROP TABLE IF EXISTS `re_goods_order`;
CREATE TABLE `re_goods_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单号',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证号id 2020 05 09 新增 ',
  `goods_id` int(11) DEFAULT NULL COMMENT '商品id',
  `score` int(11) DEFAULT 0 COMMENT '兑换商品的积分',
  `price` decimal(8, 2) DEFAULT NULL COMMENT '价格 null  表示 不需要支付金额',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态 1 待领取  2已取走（自提） 3待发货  4已发货 （邮递）  5 已过期   6 已取消',
  `is_pay` tinyint(1) DEFAULT 1 COMMENT '1 未支付 2 已支付    3支付异常， 4 订单已失效 5 .已退款',
  `payment` tinyint(1) DEFAULT 1 COMMENT '支付方式  1 微信支付   2 其他',
  `refund_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退款单号',
  `refund_time` datetime(0) DEFAULT NULL COMMENT '退款时间',
  `refund_remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退款备注',
  `refund_manage_id` int(11) DEFAULT NULL COMMENT '退款管理员id 为空表示  自己主动退款',
  `redeem_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '兑换码（8位）',
  `send_way` tinyint(1) DEFAULT NULL COMMENT '1 自提 2 邮递',
  `tracking_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '运单号',
  `take_time` datetime(0) DEFAULT NULL COMMENT '确认发货、领取时间',
  `take_manage_id` int(11) DEFAULT NULL COMMENT '确认发货、领取 的管理员id',
  `cancel_end_time` datetime(0) DEFAULT NULL COMMENT '支付后可取消的最后阶段',
  `settle_state` tinyint(1) DEFAULT 3 COMMENT '结算状态 只有涉及金额的才有结算（这里只标记就行）  1 已结算  2 结算中 3 未结算',
  `settle_shop_time` datetime(0) DEFAULT NULL COMMENT '结算时间（书店结算）',
  `settle_lib_time` datetime(0) DEFAULT NULL,
  `settle_shop_manage_id` int(11) DEFAULT NULL COMMENT '书店结算的管理员id',
  `settle_lib_manage_id` int(11) DEFAULT NULL COMMENT '图书馆结算的管理员id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '加入时间（订单时间）',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间  （支付时间，支付异常时间，订单失效后的时间）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 76 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品订单表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_goods_order_address
-- ----------------------------
DROP TABLE IF EXISTS `re_goods_order_address`;
CREATE TABLE `re_goods_order_address`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL COMMENT '订单id',
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户姓名',
  `tel` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '电话号码',
  `province` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '省',
  `city` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `district` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '区',
  `street` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '街道',
  `address` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '详细地址',
  `send_username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '发货人名称',
  `send_tel` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '发货人电话',
  `send_province` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '省(发货）',
  `send_city` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '市(发货）',
  `send_district` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '区(发货）',
  `send_street` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '街道(发货）',
  `send_address` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '详细地址(发货）',
  `manage_id` int(11) DEFAULT NULL COMMENT '修改管理员id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '删除、修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户订单地址表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_goods_order_pay
-- ----------------------------
DROP TABLE IF EXISTS `re_goods_order_pay`;
CREATE TABLE `re_goods_order_pay`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单号',
  `pay_time` datetime(0) DEFAULT NULL COMMENT '支付时间',
  `payment` tinyint(1) DEFAULT 1 COMMENT '支付方式  1 微信支付',
  `trade_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '微信支付订单号',
  `buyer_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '买家微信支付分配的终端设备号',
  `total_amount` decimal(8, 2) DEFAULT NULL COMMENT '订单总金额,订单总金额，单位为分',
  `receipt_amount` decimal(8, 2) DEFAULT NULL COMMENT '实收金额,商家在交易中实际收到的款项',
  `buyer_pay_amount` decimal(8, 2) DEFAULT NULL COMMENT '付款金额,现金支付金额订单现金支付金额，详见支付金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品订单支付表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_goods_order_refund
-- ----------------------------
DROP TABLE IF EXISTS `re_goods_order_refund`;
CREATE TABLE `re_goods_order_refund`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `refund_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退款单号',
  `refund_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '微信退款单号',
  `refund_time` datetime(0) DEFAULT NULL COMMENT '退款时间',
  `price` decimal(5, 2) DEFAULT NULL COMMENT '退款金额',
  `payment` tinyint(1) DEFAULT NULL COMMENT '退款路径  1 微信',
  `status` tinyint(1) DEFAULT NULL COMMENT '退款结果  1 成功  2失败',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员id',
  `error_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退款失败的code',
  `error_msg` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退款失败的消息提示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单退款日志表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_goods_pick_address
-- ----------------------------
DROP TABLE IF EXISTS `re_goods_pick_address`;
CREATE TABLE `re_goods_pick_address`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `province` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `city` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `district` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `street` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `tel` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系电话',
  `contacts` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系人',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品 自提地址' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_goods_type
-- ----------------------------
DROP TABLE IF EXISTS `re_goods_type`;
CREATE TABLE `re_goods_type`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '类型',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1 成功  2 删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品类型表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_hot_search
-- ----------------------------
DROP TABLE IF EXISTS `re_hot_search`;
CREATE TABLE `re_hot_search`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `content` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '检索热词',
  `number` int(11) DEFAULT NULL COMMENT '次数',
  `type` tinyint(1) DEFAULT NULL COMMENT '类型 1 图书到家新书   2 图书到家新书馆藏书  3 单纯馆藏书检索',
  `create_time` datetime(0) DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '热门检索表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_lib_book
-- ----------------------------
DROP TABLE IF EXISTS `re_lib_book`;
CREATE TABLE `re_lib_book`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `metaid` int(11) DEFAULT NULL COMMENT '馆藏书籍id',
  `metatable` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '书目库',
  `book_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '书名',
  `author` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作者',
  `press` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '出版社',
  `pre_time` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '出版时间',
  `isbn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'ISBN号',
  `old_isbn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '原本的isbn号',
  `price` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '金额',
  `img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'default/default_lib_book.png' COMMENT '封面图片',
  `intro` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '简介',
  `book_num` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '索书号',
  `type_id` int(11) DEFAULT 0 COMMENT '类型id  （自己手动分类）',
  `type_big_id` int(11) DEFAULT NULL COMMENT '对应的中图分类法id  只需要一级大类',
  `holding_num` int(11) DEFAULT NULL COMMENT '馆藏数量',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  `access_number` int(5) DEFAULT 0 COMMENT '获取图片次数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '(馆藏书籍表)（有实际操作才放入数据库）' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_lib_book_barcode
-- ----------------------------
DROP TABLE IF EXISTS `re_lib_book_barcode`;
CREATE TABLE `re_lib_book_barcode`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `book_id` int(11) DEFAULT NULL COMMENT '对应馆藏书籍id',
  `barcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '条形码',
  `book_num` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '索书号',
  `init_sub_lib` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '馆藏所属馆备注',
  `init_local` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '馆藏所属地备注',
  `cur_sub_lib` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '馆藏当前所在馆备注',
  `cur_local` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '馆藏当前所在地备注',
  `init_sub_lib_note` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `init_local_note` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `cur_sub_lib_note` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `cur_local_note` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `reg_date` date DEFAULT NULL,
  `ecard_flag` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `status` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '馆藏状态',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '书店书籍对应条形码表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_lib_book_recom
-- ----------------------------
DROP TABLE IF EXISTS `re_lib_book_recom`;
CREATE TABLE `re_lib_book_recom`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `metaid` int(11) DEFAULT NULL COMMENT '馆藏书籍id',
  `metatable` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '书目库',
  `book_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '书名',
  `author` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作者',
  `press` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `pre_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '出版时间',
  `isbn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'ISBN号',
  `old_isbn` varchar(35) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '原本的isbn号',
  `barcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '条形码',
  `price` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '金额  ',
  `img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'default/default_lib_book.png',
  `book_num` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '索书号',
  `intro` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '简介',
  `type_id` int(11) DEFAULT NULL COMMENT '类型id',
  `is_recom` tinyint(1) DEFAULT 1 COMMENT '是否推荐  1 推荐  2 未推荐 ',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1 正常  2 删除 ',
  `access_number` int(5) DEFAULT 0 COMMENT '获取图片次数',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '馆藏推荐书籍表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_lib_book_type
-- ----------------------------
DROP TABLE IF EXISTS `re_lib_book_type`;
CREATE TABLE `re_lib_book_type`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类型名称',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1 正常  2 删除 ',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '书馆藏推荐书籍类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_lib_borrow_log
-- ----------------------------
DROP TABLE IF EXISTS `re_lib_borrow_log`;
CREATE TABLE `re_lib_borrow_log`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户id',
  `book_name` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '书名',
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '书的类别   22大类',
  `small_type` int(11) DEFAULT NULL COMMENT '小类型',
  `author` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '作者',
  `price` float(5, 2) DEFAULT NULL COMMENT '价格',
  `press` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '出版社',
  `isbn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT 'Isbn号',
  `barcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '条码号	主要是处理图书馆的书籍，图书馆的系统都有统一的条码',
  `img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'default/default_book.png' COMMENT '图片',
  `thumb_img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '封面缩略图',
  `pre_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '出版时间',
  `callno` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '索书号',
  `classno` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `intro` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '简介',
  `handler` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '是否是管理员操作	YES_APP(手机管理员)，YES_WEB（web管理员）,NO，YES_APP，YES_WEB的话，user_id就为管理员的id',
  `borrow_time` datetime(0) DEFAULT NULL COMMENT '借阅时间',
  `expire_time` datetime(0) DEFAULT NULL COMMENT '预计归还时间    时间戳',
  `return_time` datetime(0) DEFAULT NULL COMMENT '归还时间   日期时间格式',
  `status` tinyint(1) DEFAULT 2 COMMENT '状态  1 已归还  2 借阅中',
  `create_time` datetime(0) DEFAULT NULL COMMENT '借阅时间',
  `rdrrecno` int(10) DEFAULT NULL COMMENT '读者记录号',
  `lib_id` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '所属馆',
  `lib_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '所属馆',
  `account` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '读者证号',
  `cur_local` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `cur_local_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `cur_lib` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `cur_lib_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `owner_local` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `owner_local_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `renew_num` int(11) DEFAULT 0 COMMENT '续借次数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '作为图书馆借阅记录表，实际可不用' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_manage
-- ----------------------------
DROP TABLE IF EXISTS `re_manage`;
CREATE TABLE `re_manage`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '账号',
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户名',
  `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '密码(md5)',
  `tel` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户电话',
  `create_time` datetime(0) DEFAULT NULL COMMENT '数据插入时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '数据更新时间',
  `token` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户令牌，每次登录刷新一次token（单点登录）',
  `expire_time` datetime(0) DEFAULT NULL COMMENT '令牌过期时间',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除   1.正常(默认)  2.已删除',
  `manage_id` int(10) DEFAULT NULL,
  `way` tinyint(1) DEFAULT 1 COMMENT '管辖位置   1 全部  2 图书馆  3 书店',
  `end_login_time` datetime(0) DEFAULT NULL,
  `end_login_ip` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`, `tel`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '后台管理员表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_manage_login_log
-- ----------------------------
DROP TABLE IF EXISTS `re_manage_login_log`;
CREATE TABLE `re_manage_login_log`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `manage_id` int(11) DEFAULT NULL,
  `login_time` datetime(0) DEFAULT NULL COMMENT '登陆时间',
  `login_addr` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '登陆地址',
  `login_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '登陆ip',
  `is_success` tinyint(1) DEFAULT 2 COMMENT '是否成功 1 成功  2 失败',
  `is_lock` tinyint(1) DEFAULT 1 COMMENT '是否锁定  1 正常  2 锁定',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 862 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员登陆日志表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_manage_role
-- ----------------------------
DROP TABLE IF EXISTS `re_manage_role`;
CREATE TABLE `re_manage_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manage_id` int(11) DEFAULT NULL COMMENT '人员id',
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  `create_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理员 角色关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_migrations
-- ----------------------------
DROP TABLE IF EXISTS `re_migrations`;
CREATE TABLE `re_migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_navigation_area
-- ----------------------------
DROP TABLE IF EXISTS `re_navigation_area`;
CREATE TABLE `re_navigation_area`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `build_id` int(11) DEFAULT NULL COMMENT '所属建筑id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '区域名称',
  `alias_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '区域别名',
  `img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '楼层svg图',
  `rotation_angle` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '指北针偏移角度 0~360',
  `is_start` tinyint(1) DEFAULT 2 COMMENT '是否建筑起点 1起点 2非起点,默认2',
  `level` tinyint(1) DEFAULT 1 COMMENT '级别 默认1级 2级为房间内部区域',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `is_play` tinyint(1) DEFAULT 1 COMMENT '是否展示1正常 2停用',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除 1正常 2删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  `sort` int(11) DEFAULT 0 COMMENT '排序 数字越大越靠前',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '导航线路图' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_navigation_build
-- ----------------------------
DROP TABLE IF EXISTS `re_navigation_build`;
CREATE TABLE `re_navigation_build`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '建筑名称',
  `province` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '省',
  `city` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '市',
  `district` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '区县',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '具体地址',
  `is_play` tinyint(1) DEFAULT 1 COMMENT '是否展示1正常 2停用',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `is_del` tinyint(1) DEFAULT 1 COMMENT ' 是否删除 1正常 2删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '导航线路图' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_navigation_point
-- ----------------------------
DROP TABLE IF EXISTS `re_navigation_point`;
CREATE TABLE `re_navigation_point`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '点位名称',
  `area_id` int(11) DEFAULT NULL COMMENT '所属区域id',
  `transtion_area_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '过渡区域，多个用,链接',
  `line` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '路线代码',
  `type` tinyint(1) DEFAULT 2 COMMENT '类型 1起点 2目的地 3过渡点 4起点/过渡点',
  `is_play` tinyint(1) DEFAULT 1 COMMENT '是否禁用 1 正常 2禁用',
  `level` tinyint(1) DEFAULT 1 COMMENT '级别 默认1级',
  `create_time` datetime(0) DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  `sort` int(11) DEFAULT 0 COMMENT '排序 数字越大越靠前',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除 1 正常 2 删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 50 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '室内分布表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_new_book_recommend
-- ----------------------------
DROP TABLE IF EXISTS `re_new_book_recommend`;
CREATE TABLE `re_new_book_recommend`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `book_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '书名',
  `author` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作者',
  `press` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '出版社',
  `pre_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '出版时间',
  `isbn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'ISBN号',
  `price` decimal(8, 2) DEFAULT NULL COMMENT '金额',
  `img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'default/default_new_book.png' COMMENT '封面图片',
  `book_num` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `intro` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '简介',
  `type_id` int(11) DEFAULT 0 COMMENT '类型id   0 代表没有分类',
  `is_recom` tinyint(1) DEFAULT 1 COMMENT '是否为推荐图书  1 是 2 否',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1 正常  2 删除',
  `qr_url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '书籍二维码链接',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `type_id`(`type_id`) USING BTREE,
  INDEX `isbn`(`isbn`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '新书推荐书籍表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_new_book_recommend_collect
-- ----------------------------
DROP TABLE IF EXISTS `re_new_book_recommend_collect`;
CREATE TABLE `re_new_book_recommend_collect`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `book_id` int(11) DEFAULT NULL COMMENT '书籍id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '收藏时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 61 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '书籍收藏表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_new_book_recommend_type
-- ----------------------------
DROP TABLE IF EXISTS `re_new_book_recommend_type`;
CREATE TABLE `re_new_book_recommend_type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '类型名称',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否被删除 1否 2是',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_news
-- ----------------------------
DROP TABLE IF EXISTS `re_news`;
CREATE TABLE `re_news`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '新闻标题',
  `img` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '封面',
  `type_id` int(11) DEFAULT NULL COMMENT '新闻类型id',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '新闻正文',
  `browse_num` int(11) DEFAULT 0 COMMENT '浏览量',
  `is_top` tinyint(1) DEFAULT 0 COMMENT '是否置顶   只能置顶3条  越大越在前  0表示未置顶',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除 1否 2是',
  `add_time` datetime(0) DEFAULT NULL COMMENT '用户手动选择的创建时间',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '阅享精选' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_news_collect
-- ----------------------------
DROP TABLE IF EXISTS `re_news_collect`;
CREATE TABLE `re_news_collect`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_id` int(11) DEFAULT NULL COMMENT '新闻id',
  `user_id` int(11) DEFAULT 1 COMMENT '用户id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '阅享精选类型表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_news_type
-- ----------------------------
DROP TABLE IF EXISTS `re_news_type`;
CREATE TABLE `re_news_type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '类型名称',
  `type` tinyint(1) DEFAULT 1 COMMENT '列表数据展示方式   1 列表   2 图文展示',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否被删除 1否 2是',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '阅享精选类型表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_online_registration
-- ----------------------------
DROP TABLE IF EXISTS `re_online_registration`;
CREATE TABLE `re_online_registration`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `shop_id` int(11) DEFAULT NULL COMMENT '书店id',
  `order_id` int(11) DEFAULT NULL COMMENT '订单id 为null表示不需要支付',
  `reader_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '读者证号',
  `certificate_type` tinyint(1) DEFAULT 1 COMMENT '证件类型  1 为身份证  2 为其他证',
  `certificate_num` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '证件号码',
  `username` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '姓名',
  `id_card` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '身份证号码',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `sex` tinyint(1) DEFAULT NULL COMMENT '性别  1 男  2 女',
  `card_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '读者证类型',
  `workunit` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '工作单位',
  `province` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `city` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `district` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `street` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '街道',
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系地址',
  `card_front` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '身份证正面 ',
  `card_reverse` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '身份证反面',
  `zip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '邮编',
  `tel` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '手机号码',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '电话号码',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '邮箱',
  `culture_id` int(11) DEFAULT NULL,
  `culture` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `position` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `pay_way` tinyint(2) DEFAULT 1 COMMENT '1  现金  2 微信',
  `cash` decimal(6, 2) DEFAULT 0.00 COMMENT '押金 单位元',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `is_pay` tinyint(1) DEFAULT 2 COMMENT '支付状态 1 未支付 2 已支付    3支付异常， 4 订单已失效 5 .已退款',
  `source` tinyint(1) DEFAULT 1 COMMENT '办证来源 1前端 2后台',
  `status` tinyint(1) DEFAULT 2 COMMENT '是否领取 1 已领取  2 待领取  3 无需领取   4、已注销',
  `create_time` datetime(0) DEFAULT NULL,
  `certificate_manage_id` int(11) DEFAULT NULL,
  `settle_state` tinyint(1) DEFAULT 3 COMMENT '结算状态（这里只标记就行）  1 已结算  2 结算中 3 未结算',
  `settle_sponsor_time` datetime(0) DEFAULT NULL COMMENT '结算时间（书店结算）',
  `settle_sponsor_manage_id` int(11) DEFAULT NULL COMMENT '书店结算的管理员id',
  `settle_affirm_time` datetime(0) DEFAULT NULL,
  `settle_affirm_manage_id` int(11) DEFAULT NULL COMMENT '图书馆结算时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '领取时间',
  `node` tinyint(1) DEFAULT 2 COMMENT '是否写入成功  1 成功  2 失败',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '在线办证表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_online_registration_config
-- ----------------------------
DROP TABLE IF EXISTS `re_online_registration_config`;
CREATE TABLE `re_online_registration_config`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `intro` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '简介',
  `real_info` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '报名所需信息 多个 | 链接    1、姓名 2、性别 3、生日  4、身份证号码  5、民族 6、政治面貌  7、健康状态  8、联系电话  9、毕业院校  10、专业  11、工作单位 12、教育程度  13、个人简介  14、特长  15、地址  ',
  `real_info_must` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '信息必填参数 多个 | 链接     1、姓名 2、性别 3、生日  4、身份证号码  5、民族 6、政治面貌  7、健康状态  8、联系电话  9、毕业院校  10、专业  11、工作单位 12、教育程度  13、个人简介  14、特长  15、用户地址 ',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '删除  1 正常 2删除',
  `is_play` tinyint(1) DEFAULT 2 COMMENT '是否发布 1.发布 2.未发布    默认2',
  `create_time` datetime(0) DEFAULT NULL COMMENT '开始时间',
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '志愿者岗位' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_online_registration_order
-- ----------------------------
DROP TABLE IF EXISTS `re_online_registration_order`;
CREATE TABLE `re_online_registration_order`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单号',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证号id 2020 05 09 新增 ',
  `shop_id` int(11) DEFAULT NULL COMMENT '书店 id',
  `price` decimal(5, 2) DEFAULT NULL COMMENT '押金 价格',
  `dis_price` decimal(5, 2) DEFAULT NULL COMMENT '折扣后的价格  （默认与实际价格一致）   用户实际支付金额',
  `discount` float(5, 2) DEFAULT 100.00 COMMENT '折扣   默认 100  没有折扣  百分比',
  `is_pay` tinyint(1) DEFAULT 2 COMMENT '1 未支付 2 已支付    3支付异常， 4 订单已失效     5 .已退款',
  `create_time` datetime(0) DEFAULT NULL COMMENT '加入时间（订单时间）',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间  （支付时间，支付异常时间，订单失效后的时间）',
  `payment` tinyint(1) DEFAULT 1 COMMENT '支付方式  1 微信支付 ',
  `refund_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退款单号',
  `refund_time` datetime(0) DEFAULT NULL COMMENT '退款时间',
  `refund_remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退款备注',
  `refund_manage_id` int(11) DEFAULT NULL COMMENT '退款管理员id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '在线办证订单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_online_registration_order_pay
-- ----------------------------
DROP TABLE IF EXISTS `re_online_registration_order_pay`;
CREATE TABLE `re_online_registration_order_pay`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单号',
  `pay_time` datetime(0) DEFAULT NULL COMMENT '支付时间',
  `payment` tinyint(1) DEFAULT 1 COMMENT '支付方式  1 微信支付',
  `trade_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '微信支付订单号',
  `buyer_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '买家微信支付分配的终端设备号',
  `total_amount` decimal(8, 2) DEFAULT NULL COMMENT '订单总金额,订单总金额，单位为分',
  `receipt_amount` decimal(8, 2) DEFAULT NULL COMMENT '实收金额,商家在交易中实际收到的款项',
  `buyer_pay_amount` decimal(8, 2) DEFAULT NULL COMMENT '付款金额,现金支付金额订单现金支付金额，详见支付金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '在线办证订单支付表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_online_registration_order_refund
-- ----------------------------
DROP TABLE IF EXISTS `re_online_registration_order_refund`;
CREATE TABLE `re_online_registration_order_refund`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `refund_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退款单号',
  `refund_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '微信退款单号',
  `refund_time` datetime(0) DEFAULT NULL COMMENT '退款时间',
  `price` decimal(5, 2) DEFAULT NULL COMMENT '退款金额',
  `payment` tinyint(1) DEFAULT NULL COMMENT '退款路径  1 微信',
  `status` tinyint(1) DEFAULT NULL COMMENT '退款结果  1 成功  2失败',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员id',
  `error_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退款失败的code',
  `error_msg` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退款失败的消息提示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单退款日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_other_access_num
-- ----------------------------
DROP TABLE IF EXISTS `re_other_access_num`;
CREATE TABLE `re_other_access_num`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `total_num` int(11) DEFAULT 0 COMMENT '总浏览量',
  `type` tinyint(4) DEFAULT 1 COMMENT '    1 数字阅读  2 图书到家 3 活动 4 场馆预约 5 空间预约 6阅读互动',
  `yyy` int(4) DEFAULT NULL COMMENT '年',
  `mmm` tinyint(2) DEFAULT NULL COMMENT '月',
  `ddd` tinyint(2) DEFAULT NULL COMMENT '日',
  `create_time` datetime(0) DEFAULT NULL COMMENT '数据创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 340 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '其他界面访问统计' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_owe_order
-- ----------------------------
DROP TABLE IF EXISTS `re_owe_order`;
CREATE TABLE `re_owe_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单号',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证号id  ',
  `score` int(11) DEFAULT 0 COMMENT '抵扣的积分',
  `price` decimal(8, 2) DEFAULT 0.00 COMMENT '价格 null  表示 不需要支付金额  单位元',
  `price_fen` int(11) DEFAULT NULL,
  `is_pay` tinyint(1) DEFAULT 1 COMMENT '1 未支付 2 已支付    3支付异常， 4 订单已失效 5 .已退款',
  `payment` tinyint(1) DEFAULT 1 COMMENT '支付方式  1 微信支付   2 积分抵扣  3其他',
  `refund_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退款单号',
  `refund_time` datetime(0) DEFAULT NULL COMMENT '退款时间',
  `refund_remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '退款备注',
  `create_time` datetime(0) DEFAULT NULL COMMENT '加入时间（订单时间）',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间  （支付时间，支付异常时间，订单失效后的时间）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 71 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '欠费订单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_owe_order_pay
-- ----------------------------
DROP TABLE IF EXISTS `re_owe_order_pay`;
CREATE TABLE `re_owe_order_pay`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单号',
  `pay_time` datetime(0) DEFAULT NULL COMMENT '支付时间',
  `payment` tinyint(1) DEFAULT 1 COMMENT '支付方式  1 微信支付',
  `trade_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '微信支付订单号',
  `buyer_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '买家微信支付分配的终端设备号',
  `total_amount` decimal(8, 2) DEFAULT NULL COMMENT '订单总金额,订单总金额，单位为分',
  `receipt_amount` decimal(8, 2) DEFAULT NULL COMMENT '实收金额,商家在交易中实际收到的款项',
  `buyer_pay_amount` decimal(8, 2) DEFAULT NULL COMMENT '付款金额,现金支付金额订单现金支付金额，详见支付金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '欠费订单支付表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_owe_order_record
-- ----------------------------
DROP TABLE IF EXISTS `re_owe_order_record`;
CREATE TABLE `re_owe_order_record`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '欠费订单id',
  `fin_account_id` int(11) DEFAULT NULL COMMENT '欠款记录号',
  `price` decimal(8, 2) DEFAULT 0.00 COMMENT '欠费金额',
  `price_fen` int(10) DEFAULT 0 COMMENT '单位元',
  `cost_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `cost_type_note` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `update_time` datetime(0) DEFAULT NULL,
  `intro` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `fin_event_id` int(11) DEFAULT NULL COMMENT '财经事务记录号',
  `create_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 117 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '欠费订单记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_permission
-- ----------------------------
DROP TABLE IF EXISTS `re_permission`;
CREATE TABLE `re_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '权限名称',
  `sidebar_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '侧边栏名称',
  `meta_title` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '图标',
  `route_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '如果为页面级权限，页面组件名称',
  `route_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '访问路径，菜单级权限必填',
  `component_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '组件路径,菜单与页面级权限必填',
  `type` tinyint(1) DEFAULT 1 COMMENT '权限类型 1目录 2菜单 3页面 4按钮',
  `path` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '权限树，补齐4位显示',
  `view_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `path_name` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '权限树(名称)',
  `redirect` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `api_path` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '权限对应的接口路由',
  `pid` int(11) DEFAULT NULL COMMENT '父级id',
  `level` int(11) DEFAULT NULL COMMENT '深度',
  `hl_routes` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '高亮路由定义',
  `order` int(10) DEFAULT NULL COMMENT '排序',
  `is_admin` tinyint(1) DEFAULT 2 COMMENT '是否为admin管理员独有权限  1是 2不是 默认2',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除   1.正常(默认)  2.已删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 852 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_picture_live
-- ----------------------------
DROP TABLE IF EXISTS `re_picture_live`;
CREATE TABLE `re_picture_live`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '线上大赛活动id',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '活动名称',
  `main_img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '进入主界面封面图片',
  `img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'default/default_picture_live.png' COMMENT '封面图片',
  `host_handle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '主办单位',
  `tel` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '咨询方式',
  `address` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '地址',
  `con_start_time` datetime(0) DEFAULT NULL COMMENT '投稿开始时间',
  `con_end_time` datetime(0) DEFAULT NULL COMMENT '投稿结束时间',
  `vote_start_time` datetime(0) DEFAULT NULL COMMENT '投票开始时间',
  `vote_end_time` datetime(0) DEFAULT NULL COMMENT '投票结束时间',
  `is_check` tinyint(1) DEFAULT 1 COMMENT '前台上传照片是否需要审核   1.是   2.否(默认）',
  `vote_way` tinyint(1) DEFAULT NULL COMMENT '投票方式   1  总票数方式   2 每日投票方式',
  `number` int(10) DEFAULT 0 COMMENT '票数',
  `uploads_number` int(11) DEFAULT 0 COMMENT '每个人上传限制张数，后台上传不影响   0 不限制',
  `uploads_way` tinyint(1) DEFAULT 1 COMMENT '上传方式  1 前台后台都可以上传   2  仅支持后台上传  3 仅支持前台上传  ',
  `is_appoint` tinyint(1) DEFAULT 2 COMMENT '是否指定用户上传  1 指定  2 不指定（都可以上传）',
  `intro` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '简介',
  `manage_id` int(10) UNSIGNED DEFAULT NULL COMMENT '管理员id',
  `is_play` tinyint(1) DEFAULT 2 COMMENT '是否发布 1.发布 2.未发布    默认1',
  `is_del` tinyint(1) UNSIGNED DEFAULT 1 COMMENT '是否删除   1.正常   2.已删除',
  `browse_num` int(11) DEFAULT 0 COMMENT '浏览量',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  `theme_color` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '主题色',
  `watermark_img` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'default/default_watermark_img.png' COMMENT '水印图片',
  `animation` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '动画',
  `animation_time` int(11) DEFAULT 1 COMMENT '单位秒',
  `qr_code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `qr_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '二维码链接',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '图片直播活动表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_picture_live_appoint_user
-- ----------------------------
DROP TABLE IF EXISTS `re_picture_live_appoint_user`;
CREATE TABLE `re_picture_live_appoint_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '线上大赛作品表',
  `act_id` int(11) DEFAULT NULL COMMENT '直播活动id  0 表示全部活动',
  `user_id` int(10) UNSIGNED DEFAULT NULL COMMENT '用户id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '图片直播指定用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_picture_live_black_user
-- ----------------------------
DROP TABLE IF EXISTS `re_picture_live_black_user`;
CREATE TABLE `re_picture_live_black_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '线上大赛作品表',
  `act_id` int(11) DEFAULT NULL COMMENT '直播活动id  0 表示全部活动 ，先不判断此值，所有活动通用',
  `user_id` int(10) UNSIGNED DEFAULT NULL COMMENT '用户id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '图片直播黑名单用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_picture_live_browse
-- ----------------------------
DROP TABLE IF EXISTS `re_picture_live_browse`;
CREATE TABLE `re_picture_live_browse`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '线上大赛作品表',
  `act_id` int(11) DEFAULT NULL COMMENT '单位',
  `user_id` int(10) UNSIGNED DEFAULT NULL COMMENT '用户id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 968 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '图片直播活动浏览过的表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_picture_live_vote
-- ----------------------------
DROP TABLE IF EXISTS `re_picture_live_vote`;
CREATE TABLE `re_picture_live_vote`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户投票',
  `act_id` int(10) UNSIGNED DEFAULT NULL COMMENT '线上大赛活动id',
  `works_id` int(10) UNSIGNED DEFAULT NULL COMMENT '作品id',
  `yyy` smallint(4) UNSIGNED DEFAULT NULL COMMENT '年',
  `mmm` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `ddd` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `date` date DEFAULT NULL COMMENT '时间 2019-08-07',
  `user_id` int(10) UNSIGNED DEFAULT NULL COMMENT '用户id',
  `terminal` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '投票终端    web   android   ios  wx',
  `create_time` datetime(0) DEFAULT NULL COMMENT '投票时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 275 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户投票' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_picture_live_works
-- ----------------------------
DROP TABLE IF EXISTS `re_picture_live_works`;
CREATE TABLE `re_picture_live_works`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '线上大赛作品表',
  `serial_number` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作品编号  大写字母开头   在加8位数字',
  `act_id` int(11) DEFAULT NULL COMMENT '单位',
  `user_id` int(10) UNSIGNED DEFAULT NULL COMMENT '用户id',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '名称',
  `intro` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作品简介',
  `cover` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作品封面  默认是图片最小版本',
  `img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作品图片地址 node=1',
  `thumb_img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '缩略图',
  `width` int(11) DEFAULT 0 COMMENT '宽度  裁剪后',
  `height` int(11) UNSIGNED DEFAULT 0 COMMENT '高度  裁剪后',
  `size` int(11) DEFAULT NULL COMMENT '原图大小  单位 字节',
  `ratio` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT ' 原图尺寸  长x宽',
  `browse_num` int(10) UNSIGNED DEFAULT 0 COMMENT '浏览量',
  `vote_num` int(10) UNSIGNED DEFAULT 0 COMMENT '投票量',
  `status` tinyint(1) UNSIGNED DEFAULT 3 COMMENT '状态    1.已通过   2.未通过   3.未审核(默认)  4.已撤销， 5已删除（用户自己删除） 6.已违规',
  `reason` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '拒绝理由',
  `way` tinyint(1) DEFAULT 1 COMMENT '上传方式  1 前台上传  2 后台上传',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1正常  2 删除   后台删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `serial_number`(`serial_number`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 230 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '图片直播图片表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_problem_feedback
-- ----------------------------
DROP TABLE IF EXISTS `re_problem_feedback`;
CREATE TABLE `re_problem_feedback`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `problem_id` int(10) DEFAULT NULL COMMENT '问题id',
  `content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '返回其他内容',
  `case_guid` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '书柜guid',
  `user_guid` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户guid',
  `status` tinyint(1) DEFAULT 3 COMMENT '状态   1 已完成  2 处理中 3 未处理',
  `create_time` datetime(0) DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '问题反馈' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_reading_task
-- ----------------------------
DROP TABLE IF EXISTS `re_reading_task`;
CREATE TABLE `re_reading_task`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '征集活动相关类型',
  `title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '标题',
  `img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类型名称',
  `is_appoint` tinyint(1) DEFAULT 2 COMMENT '是否指定用户上传  1 指定  2 不指定（都可以指定）',
  `start_time` datetime(0) DEFAULT NULL COMMENT '限制该分类作品上传最大数量  0为不限',
  `end_time` datetime(0) DEFAULT NULL COMMENT '编号首字母',
  `browse_num` int(11) DEFAULT 0 COMMENT '浏览量',
  `intro` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `is_del` tinyint(1) UNSIGNED DEFAULT 1 COMMENT '是否删除   1.正常   2.已删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  `is_play` tinyint(1) DEFAULT 2 COMMENT '是否发布 1.发布 2.未发布    默认1',
  `is_reader` tinyint(1) DEFAULT 2 COMMENT '阅读是否需要绑定读者证  1 是  2 否',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '阅读任务表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_reading_task_appoint_user
-- ----------------------------
DROP TABLE IF EXISTS `re_reading_task_appoint_user`;
CREATE TABLE `re_reading_task_appoint_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '征集活动相关类型',
  `task_id` int(10) UNSIGNED DEFAULT NULL COMMENT '任务id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '阅读任务关联用户信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_reading_task_database
-- ----------------------------
DROP TABLE IF EXISTS `re_reading_task_database`;
CREATE TABLE `re_reading_task_database`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '征集活动相关类型',
  `task_id` int(10) UNSIGNED DEFAULT NULL COMMENT '任务id',
  `database_id` int(11) DEFAULT NULL COMMENT '数据库id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '阅读任务关联数据库表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_reading_task_execute
-- ----------------------------
DROP TABLE IF EXISTS `re_reading_task_execute`;
CREATE TABLE `re_reading_task_execute`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '征集活动相关类型',
  `task_id` int(11) DEFAULT NULL COMMENT '阅读任务id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证号 ，以完成时绑定的读者证号为准',
  `progress` decimal(5, 2) DEFAULT NULL COMMENT '进度',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '阅读任务用户执行表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_reading_task_execute_database
-- ----------------------------
DROP TABLE IF EXISTS `re_reading_task_execute_database`;
CREATE TABLE `re_reading_task_execute_database`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '征集活动相关类型',
  `task_id` int(11) DEFAULT NULL COMMENT '阅读任务id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `database_id` int(11) DEFAULT NULL COMMENT '进度',
  `progress` decimal(5, 2) DEFAULT NULL COMMENT '进度',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '阅读任务用户执行单个记录表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_reading_task_execute_works
-- ----------------------------
DROP TABLE IF EXISTS `re_reading_task_execute_works`;
CREATE TABLE `re_reading_task_execute_works`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '征集活动相关类型',
  `task_id` int(11) DEFAULT NULL COMMENT '阅读任务id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `database_id` int(11) DEFAULT NULL COMMENT '进度',
  `works_id` int(11) DEFAULT NULL COMMENT '作品id',
  `type` tinyint(1) DEFAULT 1 COMMENT '类型  1 作品  2 电子书',
  `ebook_id` int(11) DEFAULT NULL COMMENT '电子书id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '阅读任务用户执行作品记录-记录表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_recommend_book
-- ----------------------------
DROP TABLE IF EXISTS `re_recommend_book`;
CREATE TABLE `re_recommend_book`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `book_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '书名      书名+ISBN 确定唯一性',
  `author` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作者',
  `isbn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'ISBN号',
  `number` int(11) DEFAULT 1 COMMENT '荐购次数 ',
  `status` tinyint(1) DEFAULT 1 COMMENT '状态 1待审核 2审核通过 3审核不通过   4 已购买 5 购买失败默认1',
  `reason` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '拒绝理由',
  `create_time` datetime(0) DEFAULT NULL COMMENT '第一次创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改（购买）时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户荐购书籍表---推荐购买' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_recommend_user
-- ----------------------------
DROP TABLE IF EXISTS `re_recommend_user`;
CREATE TABLE `re_recommend_user`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `recom_id` int(11) DEFAULT NULL COMMENT '荐购id',
  `book_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '书名      书名+ISBN 确定唯一性',
  `author` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作者',
  `press` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '出版社',
  `pre_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '出版时间',
  `isbn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'ISBN号',
  `price` decimal(5, 2) DEFAULT NULL COMMENT '金额',
  `img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '封面图片',
  `intro` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '简介',
  `create_time` datetime(0) DEFAULT NULL COMMENT '第一次创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改（购买）时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户荐购表---推荐购买' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_register_verify
-- ----------------------------
DROP TABLE IF EXISTS `re_register_verify`;
CREATE TABLE `re_register_verify`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '验证码',
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '电话号码',
  `token` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `create_time` datetime(0) DEFAULT NULL COMMENT '添加时间',
  `expir_time` datetime(0) DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_reservation
-- ----------------------------
DROP TABLE IF EXISTS `re_reservation`;
CREATE TABLE `re_reservation`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `node` tinyint(1) DEFAULT 0 COMMENT '分类  1  人物    2  物品    3 教室   4 展览预约    5 到馆预约 6 座位预约',
  `kind` tinyint(1) DEFAULT 1 COMMENT '物品种类   1 单一物品  2 多物品（物品默认一个可预约多个，其他都只能预约一个）',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图片',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '名称',
  `unit` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '物品所属单位（node为2才有）',
  `type_tag` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '补充类型(多个|连接)',
  `intro` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '简介',
  `province` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `city` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `district` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `street` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '街道',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '详细地址',
  `contacts` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '联系人姓名',
  `tel` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '联系方式',
  `number` int(11) DEFAULT 1 COMMENT '预约量 默认1   多物品预约才有  座位预约，这里为座位量',
  `apply_number` int(11) DEFAULT 0 COMMENT '总预约次数',
  `is_reader` tinyint(1) NOT NULL DEFAULT 2 COMMENT '报名是否需要绑定读者证  1 是  2 否',
  `education` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '人物学历（node为1才有）',
  `clock_time` int(11) DEFAULT 30 COMMENT '预约开始后，多少分钟之内可打卡  单位分钟  打卡限制时间 用户预约时间段为9:00-12:00，当前设置为20分钟内如果用户在9:00之前预约，则用户打卡有效时间为9:00-9:20',
  `serial_prefix` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '座位预约编号前缀',
  `day_make_time` time(0) DEFAULT NULL COMMENT '每日开始预约时间，这个时间之前不能预约',
  `display_day` tinyint(2) DEFAULT 7 COMMENT '前台展示 天数  1 ~ 30',
  `limit_num` int(11) DEFAULT 1 COMMENT '当前借阅（未使用）次数   1 ~ 30',
  `is_sign` tinyint(1) DEFAULT 1 COMMENT '是否需要签到 1需要 2不需要',
  `qr_code` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '二维码code',
  `qr_url` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '预约二维码引用链接',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除   默认 1 正常   2 已删除',
  `is_play` tinyint(1) DEFAULT 1 COMMENT '是否发布  1.发布  2.未发布  默认2',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员id',
  `lon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '经度',
  `lat` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '纬度',
  `is_approval` tinyint(1) DEFAULT NULL COMMENT '预约是否需要审核 1需要 2不需要',
  `is_cancel` tinyint(1) DEFAULT NULL COMMENT '是否可以取消 1是 2否',
  `cancel_end_time` int(11) DEFAULT NULL COMMENT '用户可取消活动预约的最后期限(以活动开始时间往前计算）  单位分钟 0表示不限制',
  `start_age` mediumint(3) DEFAULT 0 COMMENT '开始年龄  默认0   两个都是 0 则表示不需要年龄限制',
  `end_age` mediumint(3) DEFAULT 0 COMMENT '结束年龄  默认0',
  `astrict_sex` tinyint(1) DEFAULT 3 COMMENT '性别限制   1.限男性  2.限女性  3.不限(默认)',
  `is_real` tinyint(1) DEFAULT 2 COMMENT '是否需要用户真实信息 1需要 2不需要 默认2',
  `real_info` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '需要验证的真实信息id用|连接起来    1、姓名   2 、身份证号码  3、电话号码   4、读者证号码    5、真实人物头像   6、备注  7 单位名称 8 性别 9 年龄  10、附件 ',
  `make_max_num` int(11) DEFAULT 1 COMMENT '设备限制一次预约个数',
  `return_time` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '领取后，多少小时之内归还    储物柜预约独有',
  `sign_way` tinyint(1) DEFAULT 2 COMMENT '扫码方式  1 、2者都可以  2、用户自主扫码  3、管理员设备扫码  ',
  PRIMARY KEY (`id`, `is_reader`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文化配送物品预约表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_reservation_apply
-- ----------------------------
DROP TABLE IF EXISTS `re_reservation_apply`;
CREATE TABLE `re_reservation_apply`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证id',
  `reservation_id` int(11) DEFAULT NULL COMMENT '申请的文化配送预约表主键id',
  `schedule_id` int(11) DEFAULT NULL COMMENT '排班id ',
  `seat_id` int(11) DEFAULT NULL COMMENT '座位预约id',
  `make_time` date DEFAULT NULL COMMENT '预约日期',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '预约备注',
  `status` tinyint(2) DEFAULT 0 COMMENT '预约状态   1.已通过  2.已取消，3待审核，4已拒绝 ,5 已过期 (签到过期） , 6 已签到（储物柜已领取）  7 结束打卡（增对座位预约,储物柜已归还）',
  `reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '拒绝理由 ',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员',
  `number` int(11) DEFAULT 0 COMMENT '针对多物品    预约的数量   默认0',
  `is_violate` tinyint(1) DEFAULT 1 COMMENT '是否违规  1正常 2违规 默认1',
  `violate_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '违规原因',
  `violate_time` datetime(0) DEFAULT NULL COMMENT '违规时间',
  `tel` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户电话号码',
  `unit` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户填写单位信息',
  `id_card` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sex` tinyint(1) DEFAULT 0 COMMENT '性别   1 男  2 女',
  `age` int(100) DEFAULT NULL COMMENT '年龄',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  `expire_time` datetime(0) DEFAULT NULL COMMENT ' 签到过期时间 （存储柜领取过期时间）',
  `sign_time` datetime(0) DEFAULT NULL COMMENT '签到时间（存储柜领取时间）',
  `sign_end_time` datetime(0) DEFAULT NULL COMMENT '结束签到时间，座位预约才有（存储柜归还时间）',
  `return_expire_time` datetime(0) DEFAULT NULL COMMENT '储存柜归还过期时间',
  `score` int(11) DEFAULT 0 COMMENT '积分 ',
  `reader_id` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '读者证',
  `img` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '头像',
  `username` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '姓名',
  `schedule_type` tinyint(1) DEFAULT 1 COMMENT '排版类型 1正常排班 2特殊日期排',
  `serial_number` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '预约编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 301 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户预约表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_reservation_schedule
-- ----------------------------
DROP TABLE IF EXISTS `re_reservation_schedule`;
CREATE TABLE `re_reservation_schedule`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reservation_id` int(11) DEFAULT NULL COMMENT '文化配送预约表主键id',
  `week` tinyint(1) DEFAULT 0 COMMENT '星期   1-7   星期日 为  7 ',
  `start_time` time(0) DEFAULT NULL COMMENT '开始时间  时分秒',
  `end_time` time(0) DEFAULT NULL COMMENT '结束时间   时分秒',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1 正常  2 已删除 ',
  `create_time` datetime(0) DEFAULT NULL COMMENT '数据插入时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 174 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文化配送排班表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_reservation_seat
-- ----------------------------
DROP TABLE IF EXISTS `re_reservation_seat`;
CREATE TABLE `re_reservation_seat`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reservation_id` int(11) DEFAULT NULL COMMENT '预约id',
  `number` int(11) DEFAULT 1 COMMENT '序号',
  `qr_code` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '二维码code',
  `qr_url` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '座位二维码引用地址',
  `is_play` tinyint(1) DEFAULT 1 COMMENT '是否发布 1.发布 2.未发布    默认1',
  `is_del` tinyint(1) DEFAULT 1,
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 148 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '预约座位管理' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_reservation_special_schedule
-- ----------------------------
DROP TABLE IF EXISTS `re_reservation_special_schedule`;
CREATE TABLE `re_reservation_special_schedule`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自动id',
  `reservation_id` int(11) DEFAULT NULL COMMENT '预约id',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '名称',
  `date` date DEFAULT NULL COMMENT '日期',
  `start_time` time(0) DEFAULT NULL COMMENT '开始时间',
  `end_time` time(0) DEFAULT NULL COMMENT '结束时间',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1 正常  2 已删除  默认1 ',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文化配送特殊日期排班表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_reservation_type
-- ----------------------------
DROP TABLE IF EXISTS `re_reservation_type`;
CREATE TABLE `re_reservation_type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reservation_id` int(11) DEFAULT NULL COMMENT '文化配送预约表主键id',
  `type_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '标签名称',
  `type_id` int(11) DEFAULT 0 COMMENT '标签来源 1固定标签 2用户自己手动添加',
  `create_time` datetime(0) DEFAULT NULL COMMENT '数据插入时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文化配送类型表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_role
-- ----------------------------
DROP TABLE IF EXISTS `re_role`;
CREATE TABLE `re_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '角色名称',
  `manage_id` int(11) DEFAULT NULL COMMENT '创建该角色人员的id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除   1.正常(默认)  2.已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `re_role_permission`;
CREATE TABLE `re_role_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) DEFAULT 1 COMMENT '1选中权限 2选中权限父级权限',
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  `permission_id` int(11) DEFAULT NULL COMMENT '权限id',
  `create_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 983 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色权限关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_scenic
-- ----------------------------
DROP TABLE IF EXISTS `re_scenic`;
CREATE TABLE `re_scenic`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '景点名称',
  `img` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '景点封面',
  `intro` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '景点简介',
  `province` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '地址，省',
  `city` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '地址，市',
  `district` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '地址，区',
  `street` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '街道',
  `address` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '地址，详细地址',
  `start_time` datetime(0) DEFAULT NULL COMMENT '作品可上传开始时间',
  `end_time` datetime(0) DEFAULT NULL COMMENT '作品可上传结束时间',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否被删除 1正常  2 已被删除',
  `is_reader` tinyint(1) DEFAULT 2 COMMENT '打卡是否需要绑定读者证  1 是  2 否',
  `is_play` tinyint(1) DEFAULT 2 COMMENT ' 是否发布 1是 2否',
  `lon` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '经度',
  `lat` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '纬度',
  `type_id` int(11) DEFAULT NULL COMMENT '类型id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_scenic_type
-- ----------------------------
DROP TABLE IF EXISTS `re_scenic_type`;
CREATE TABLE `re_scenic_type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '类型名称',
  `color` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '类型图标颜色',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL,
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除 1否 2是',
  `icon` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '类型图标',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_scenic_works
-- ----------------------------
DROP TABLE IF EXISTS `re_scenic_works`;
CREATE TABLE `re_scenic_works`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scenic_id` int(11) DEFAULT NULL COMMENT '景点id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证号id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '作品标题',
  `cover` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '作品封面',
  `intro` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '作品简介',
  `status` tinyint(1) DEFAULT 3 COMMENT '状态    1.已通过   2.未通过   3.未审核(默认)  4.已撤销， 5已删除 ',
  `reason` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '拒绝原因',
  `browse_num` int(11) DEFAULT 0 COMMENT '浏览量',
  `vote_num` int(11) DEFAULT 0 COMMENT '点赞量',
  `score` int(11) DEFAULT 0 COMMENT '积分',
  `is_look` tinyint(1) DEFAULT 2 COMMENT '是否被查看 1已查看  2未查看',
  `is_violate` tinyint(1) DEFAULT 1 COMMENT '是否违规  1 正常  2 违规',
  `violate_reason` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '违规原因',
  `violate_time` datetime(0) DEFAULT NULL COMMENT '违规时间',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_scenic_works_img
-- ----------------------------
DROP TABLE IF EXISTS `re_scenic_works_img`;
CREATE TABLE `re_scenic_works_img`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scenic_works_id` int(11) DEFAULT NULL COMMENT '景点打卡作品id',
  `img` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '原图',
  `thumb_img` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `width` int(11) DEFAULT NULL COMMENT '宽',
  `height` int(11) DEFAULT NULL COMMENT '高',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_scenic_works_vote
-- ----------------------------
DROP TABLE IF EXISTS `re_scenic_works_vote`;
CREATE TABLE `re_scenic_works_vote`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scenic_id` int(11) DEFAULT NULL COMMENT '景点id',
  `user_id` int(11) DEFAULT NULL,
  `scenic_works_id` int(11) DEFAULT NULL COMMENT '作品id',
  `create_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_score_info
-- ----------------------------
DROP TABLE IF EXISTS `re_score_info`;
CREATE TABLE `re_score_info`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '积分管理id',
  `manage_id` int(11) UNSIGNED DEFAULT NULL COMMENT '管理员id',
  `type` int(10) DEFAULT NULL COMMENT '类型',
  `type_icon` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '图标显示文字',
  `type_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类型名称',
  `score` int(10) DEFAULT NULL COMMENT '消耗或获得的积分   0 表示关闭',
  `number` int(11) DEFAULT 1 COMMENT '一天最多几次     0 表示不限',
  `intro` varchar(5000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '简介',
  `node` tinyint(1) DEFAULT 3 COMMENT '1 正数  2 负数  3 不限',
  `is_open` tinyint(1) DEFAULT 1 COMMENT '是否开启   1 开启  2 关闭',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除 1正常 2删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '积分管理表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_score_log
-- ----------------------------
DROP TABLE IF EXISTS `re_score_log`;
CREATE TABLE `re_score_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '积分明细id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) UNSIGNED DEFAULT NULL COMMENT '用户id/团长',
  `score` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '消耗或增加的积分     1 ， -1',
  `total_score` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '应该扣除的积分  与 socre一致',
  `system_id` int(11) DEFAULT NULL COMMENT '系统消息id',
  `type_id` int(11) DEFAULT 0 COMMENT '类型id    0为商品兑换类型id    0为商品兑换 1000为管理员调整积分',
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类型',
  `intro` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '说明',
  `manage_id` int(11) DEFAULT NULL COMMENT '积分操作人',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '积分明细表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_score_rule
-- ----------------------------
DROP TABLE IF EXISTS `re_score_rule`;
CREATE TABLE `re_score_rule`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '积分管理id',
  `manage_id` int(11) UNSIGNED DEFAULT NULL COMMENT '管理员id',
  `intro` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '简介',
  `create_time` datetime(0) DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '积分规则表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_service_directory
-- ----------------------------
DROP TABLE IF EXISTS `re_service_directory`;
CREATE TABLE `re_service_directory`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '新闻标题',
  `type` tinyint(1) DEFAULT 1 COMMENT '1.本馆概况 2.开馆时间 3.入馆须知 4.读者须知 5.办证指南 6.馆内导航 7.分馆服务 8.交通指南 9.联系图书馆',
  `img` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '封面',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '新闻正文',
  `browse_num` int(11) DEFAULT 0 COMMENT '浏览量',
  `lon` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '经度',
  `lat` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '纬度',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除 1否 2是',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '服务指南' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_service_notice
-- ----------------------------
DROP TABLE IF EXISTS `re_service_notice`;
CREATE TABLE `re_service_notice`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `node` tinyint(1) DEFAULT 1 COMMENT '是否批量推送  1 批量推送， 2、微信昵称  3 读者证号 ',
  `content` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '内容',
  `type_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '服务类别',
  `time` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `remark` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `link` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '链接',
  `is_send` tinyint(1) DEFAULT 2 COMMENT '是否发送  1 立即发送   2 后续发送',
  `wechat_id` varchar(5000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '微信id  多个逗号拼接',
  `account_id` varchar(5000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '读者证号id  多个逗号拼接',
  `user_id` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '最终发送的用户id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime(0) DEFAULT NULL,
  `manage_id` int(11) DEFAULT NULL COMMENT '推送管理员',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1 正常   2已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_service_notice_send_log
-- ----------------------------
DROP TABLE IF EXISTS `re_service_notice_send_log`;
CREATE TABLE `re_service_notice_send_log`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `notice_id` int(11) DEFAULT 1 COMMENT '服务提醒通知id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `status` tinyint(1) DEFAULT 3 COMMENT '状态  1 已成功 2 失败  3 待发送',
  `open_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户微信open_id',
  `reason` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '失败理由',
  `create_time` datetime(0) DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '服务提醒通知发送日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_shop
-- ----------------------------
DROP TABLE IF EXISTS `re_shop`;
CREATE TABLE `re_shop`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '书店名称',
  `img` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '封面图片',
  `province` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '省',
  `city` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '市',
  `district` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '区',
  `street` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '街道',
  `address` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '详细地址',
  `tel` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '书店电话',
  `contacts` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '书店联系人姓名',
  `intro` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '书店简介',
  `lon` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '经度',
  `lat` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '纬度',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除   1正常  2删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '数据插入时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '数据上一次更新时间',
  `purchase_number` int(11) DEFAULT 0 COMMENT '采购本书',
  `purchase_money` decimal(10, 2) DEFAULT 0.00 COMMENT '采购金额',
  `manage_id` int(10) DEFAULT NULL,
  `way` tinyint(1) DEFAULT 1 COMMENT '1开通荐购方式，线上线下等服务，2 只开通线下、3 只开通线上 ',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '书店表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_shop_book
-- ----------------------------
DROP TABLE IF EXISTS `re_shop_book`;
CREATE TABLE `re_shop_book`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) DEFAULT NULL COMMENT '书店id',
  `shop_identifier` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '图书馆唯一标识  我们自己是  书名+ISBN+价格  在MD5',
  `book_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '书名',
  `author` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作者',
  `press` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '出版社',
  `pre_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '出版时间',
  `isbn` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'ISBN号',
  `price` decimal(8, 2) DEFAULT NULL COMMENT '金额',
  `img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'default/default_new_book.png' COMMENT '封面图片',
  `book_num` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `intro` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '简介',
  `type_id` int(11) DEFAULT 0 COMMENT '类型id   0 代表没有分类',
  `is_recom` tinyint(1) DEFAULT 2 COMMENT '是否为推荐图书  1 是 2 否',
  `book_selector` tinyint(1) DEFAULT 2 COMMENT '是否套书  1 是  2 否   默认 2 ',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1 正常  2 删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  `number` int(11) UNSIGNED DEFAULT 0 COMMENT '门市陈列数(库存数) 总数，每次导入覆盖',
  `borrow_number` int(11) DEFAULT 0 COMMENT '借阅本数量',
  `rack_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '架类型',
  `rack_describe` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '架描述',
  `rack_code` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '架位代码',
  `is_plane` tinyint(1) DEFAULT 1 COMMENT '是否在架   1 是   2 已下架',
  `access_number` int(5) DEFAULT 0 COMMENT '获取图片次数，超过5次，不在获取',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `type_id`(`type_id`) USING BTREE,
  INDEX `ISBN`(`isbn`) USING BTREE,
  INDEX `shop_identifier`(`shop_identifier`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '书店书籍表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_shop_book_type
-- ----------------------------
DROP TABLE IF EXISTS `re_shop_book_type`;
CREATE TABLE `re_shop_book_type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) DEFAULT NULL,
  `type_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `order_num` int(11) DEFAULT 0 COMMENT '对应类型数据排序，数字越大，越靠前',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除 1否 2是 默认1',
  `create_time` datetime(0) DEFAULT NULL COMMENT '数据创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '数据更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '新书类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_sign_score
-- ----------------------------
DROP TABLE IF EXISTS `re_sign_score`;
CREATE TABLE `re_sign_score`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证用户id',
  `count` int(11) DEFAULT 0 COMMENT '签到次数计数',
  `date` date DEFAULT NULL COMMENT '打卡签到日期',
  `create_time` datetime(0) DEFAULT NULL COMMENT '录入时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_study_room_reservation
-- ----------------------------
DROP TABLE IF EXISTS `re_study_room_reservation`;
CREATE TABLE `re_study_room_reservation`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图片',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '名称',
  `intro` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '简介',
  `province` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `city` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `district` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `street` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '街道',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '详细地址',
  `contacts` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '联系人姓名',
  `tel` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '联系方式',
  `number` int(11) DEFAULT 0 COMMENT '预约量 默认0   多物品预约才有',
  `apply_number` int(11) DEFAULT 0 COMMENT '总预约次数',
  `clock_time` int(11) DEFAULT 30 COMMENT '预约开始后，多少分钟之内可打卡  单位分钟',
  `qr_code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `qr_url` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '预约二维码引用链接',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除   默认 1 正常   2 已删除',
  `is_play` tinyint(1) DEFAULT 2 COMMENT '是否发布  1.未发布  2.发布  默认2',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员id',
  `lon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '经度',
  `lat` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '纬度',
  `is_approval` tinyint(1) DEFAULT NULL COMMENT '预约是否需要审核 1需要 2不需要',
  `is_cancel` tinyint(1) DEFAULT NULL COMMENT '是否可以取消 1是 2否',
  `cancel_end_time` int(11) DEFAULT NULL COMMENT '用户可取消活动预约的最后期限(以活动开始时间往前计算）  单位分钟 0表示不限制',
  `type_tag` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '补充类型(多个|连接)',
  `cab_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '整套柜子的编号，柜子分为，主柜和副柜，和柜子对接的编号',
  `stack_number` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '栈号，对外',
  `display_day` int(11) DEFAULT 7 COMMENT '预约展示天数',
  `day_make_time` time(0) DEFAULT NULL COMMENT '每日开始预约时间，这个时间之前不能预约',
  `is_real` tinyint(1) DEFAULT 2 COMMENT '是否需要用户真实信息 1需要 2不需要 默认2',
  `real_info` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '需要验证的真实信息id用|连接起来    1、姓名   2 、身份证号码  3、电话号码   4、读者证号码    5、真实人物头像   6、备注  7 单位名称 8 性别 9 年龄  10、附件 ',
  `is_reader` tinyint(1) NOT NULL DEFAULT 2 COMMENT '报名是否需要绑定读者证  1 是  2 否',
  `limit_num` int(11) DEFAULT 1 COMMENT '可预约次数   1 ~ 30',
  `astrict_sex` tinyint(1) DEFAULT 3 COMMENT '性别限制   1.限男性  2.限女性  3.不限(默认)',
  `start_age` mediumint(3) DEFAULT 0 COMMENT '开始年龄  默认0   两个都是 0 则表示不需要年龄限制',
  `end_age` mediumint(3) DEFAULT 0 COMMENT '结束年龄  默认0',
  `is_sign` tinyint(1) DEFAULT 1 COMMENT '是否需要签到 1需要 固定为1',
  `sign_way` tinyint(1) DEFAULT 2 COMMENT '扫码方式  1 、2者都可以  2、用户自主扫码  3、管理员设备扫码  ',
  PRIMARY KEY (`id`, `is_reader`) USING BTREE,
  UNIQUE INDEX `cab_code`(`cab_code`) USING BTREE,
  UNIQUE INDEX `stack_number`(`stack_number`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '书房预约表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_study_room_reservation_apply
-- ----------------------------
DROP TABLE IF EXISTS `re_study_room_reservation_apply`;
CREATE TABLE `re_study_room_reservation_apply`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT NULL COMMENT '读者证号id',
  `reservation_id` int(11) DEFAULT NULL COMMENT '申请的文化配送预约表主键id',
  `schedule_id` int(11) DEFAULT NULL COMMENT '排班id ',
  `make_time` date DEFAULT NULL COMMENT '预约日期',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '预约备注',
  `status` tinyint(2) DEFAULT 0 COMMENT '预约状态   1.已通过  2.已取消，3待审核，4已拒绝 ,5 已过期 , 6 已签到 7 已签退',
  `reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '拒绝理由 ',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员',
  `number` int(11) DEFAULT 0 COMMENT '针对多物品    预约的数量   默认0',
  `is_violate` tinyint(1) DEFAULT 1 COMMENT '是否违规  1正常 2违规 默认1',
  `violate_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '违规原因',
  `violate_time` datetime(0) DEFAULT NULL COMMENT '违规时间',
  `tel` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户电话号码',
  `unit` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户填写单位信息',
  `id_card` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  `expire_time` datetime(0) DEFAULT NULL COMMENT '签到过期时间 ',
  `sign_time` datetime(0) DEFAULT NULL COMMENT '签到开始时间',
  `sign_end_time` datetime(0) DEFAULT NULL COMMENT '签到结束时间',
  `sex` tinyint(1) DEFAULT 0 COMMENT '性别   1 男  2 女',
  `age` int(100) DEFAULT NULL COMMENT '年龄',
  `score` int(11) DEFAULT 0 COMMENT '积分 ',
  `reader_id` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '读者证',
  `img` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '头像',
  `username` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '姓名',
  `schedule_type` tinyint(1) DEFAULT 1 COMMENT '排版类型 1正常排班 2特殊日期排',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 125 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户书房预约表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_study_room_reservation_collect
-- ----------------------------
DROP TABLE IF EXISTS `re_study_room_reservation_collect`;
CREATE TABLE `re_study_room_reservation_collect`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `reservation_id` int(11) DEFAULT NULL COMMENT '文化配送id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '收藏时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文化配送收藏表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_study_room_reservation_enter_log
-- ----------------------------
DROP TABLE IF EXISTS `re_study_room_reservation_enter_log`;
CREATE TABLE `re_study_room_reservation_enter_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '数据插入时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 75 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '书房用户开门进入记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_study_room_reservation_remote_operation
-- ----------------------------
DROP TABLE IF EXISTS `re_study_room_reservation_remote_operation`;
CREATE TABLE `re_study_room_reservation_remote_operation`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_number` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '订单号',
  `type` tinyint(2) DEFAULT NULL COMMENT 'type 类型  1 远程开门   6 下载白名单； 7 清空本地所有白名单；',
  `reservation_id` int(11) DEFAULT NULL COMMENT '开始时间',
  `cab_code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1 COMMENT '状态   1 已成功 2 处理中 3待处理  4 执行失败',
  `account` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '白名单账号  多个逗号拼接',
  `node` tinyint(1) DEFAULT 1 COMMENT '操作平台，1、前台用户  2 、后台管理员',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '数据插入时间',
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '书房开门白名单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_study_room_reservation_schedule
-- ----------------------------
DROP TABLE IF EXISTS `re_study_room_reservation_schedule`;
CREATE TABLE `re_study_room_reservation_schedule`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reservation_id` int(11) DEFAULT NULL COMMENT '文化配送预约表主键id',
  `week` tinyint(1) DEFAULT 0 COMMENT '星期   1-7   星期日 为  7 ',
  `start_time` time(0) DEFAULT NULL COMMENT '开始时间  时分秒',
  `end_time` time(0) DEFAULT NULL COMMENT '结束时间   时分秒',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1 正常  2 已删除 ',
  `create_time` datetime(0) DEFAULT NULL COMMENT '数据插入时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '书房预约排班表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_study_room_reservation_special_schedule
-- ----------------------------
DROP TABLE IF EXISTS `re_study_room_reservation_special_schedule`;
CREATE TABLE `re_study_room_reservation_special_schedule`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自动id',
  `reservation_id` int(11) DEFAULT NULL COMMENT '预约id',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '名称',
  `date` date DEFAULT NULL COMMENT '日期',
  `start_time` time(0) DEFAULT NULL COMMENT '开始时间',
  `end_time` time(0) DEFAULT NULL COMMENT '结束时间',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1 正常  2 已删除  默认1 ',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 42 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文化配送特殊日期排班表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_study_room_reservation_white
-- ----------------------------
DROP TABLE IF EXISTS `re_study_room_reservation_white`;
CREATE TABLE `re_study_room_reservation_white`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '文化配送预约表主键id',
  `account` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '账号',
  `start_time` datetime(0) DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime(0) DEFAULT NULL,
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1 正常  2删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '数据插入时间',
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '书房开门白名单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_study_room_reservation_white_enter_log
-- ----------------------------
DROP TABLE IF EXISTS `re_study_room_reservation_white_enter_log`;
CREATE TABLE `re_study_room_reservation_white_enter_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL COMMENT '文化配送预约表主键id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '数据插入时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '书房白名单开门进入记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_survey
-- ----------------------------
DROP TABLE IF EXISTS `re_survey`;
CREATE TABLE `re_survey`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '问卷名称',
  `intro` varchar(3000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '问卷简介',
  `is_play` tinyint(255) DEFAULT 1 COMMENT '是否发布的状态   1 已发布   2 未发布',
  `start_time` datetime(0) DEFAULT NULL COMMENT '问卷开始时间',
  `end_time` datetime(0) DEFAULT NULL COMMENT '问卷结束时间',
  `create_time` datetime(0) DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  `is_del` tinyint(2) DEFAULT 1 COMMENT '是否删除 1未删除 2已删除',
  `reply_num` int(11) DEFAULT 0 COMMENT '回答数量 ',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '问卷调查表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_survey_answer
-- ----------------------------
DROP TABLE IF EXISTS `re_survey_answer`;
CREATE TABLE `re_survey_answer`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sur_id` int(11) DEFAULT NULL COMMENT '问卷调查id',
  `pro_id` int(11) DEFAULT NULL COMMENT '问卷调查问题  id ',
  `answer` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '问卷答案   有 答案 ： 定向题  无答案：用户自定义回答题目',
  `reply_num` int(11) DEFAULT 0 COMMENT '回复量 默认0',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 82 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '问卷调查 答案表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_survey_problem
-- ----------------------------
DROP TABLE IF EXISTS `re_survey_problem`;
CREATE TABLE `re_survey_problem`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sur_id` int(11) DEFAULT NULL COMMENT '问卷调查  id  ',
  `problem` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '问卷问题',
  `type` tinyint(1) DEFAULT 1 COMMENT '题目类型  1  多选  2 单选 （默认）3 填空题',
  `create_time` datetime(0) DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '问卷调查 问题表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_survey_reply
-- ----------------------------
DROP TABLE IF EXISTS `re_survey_reply`;
CREATE TABLE `re_survey_reply`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sur_id` int(11) DEFAULT NULL COMMENT '问卷调查信息  id ',
  `pro_id` int(11) DEFAULT NULL COMMENT '问卷调查问题  id  ',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `answer` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '问卷答案 id，多选用 | 拼接 , ',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '用户填空回答回答',
  `create_time` datetime(0) DEFAULT NULL COMMENT '答题时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '问卷调查 答案表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_system_info
-- ----------------------------
DROP TABLE IF EXISTS `re_system_info`;
CREATE TABLE `re_system_info`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '消息标题',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `account_id` int(11) DEFAULT 0 COMMENT '读者证号id',
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类型',
  `con_id` int(11) DEFAULT NULL COMMENT '内容id',
  `intro` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '消息内容',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1正常   2 删除',
  `is_look` tinyint(1) UNSIGNED DEFAULT 2 COMMENT '是否已读   1.是   2.否(默认)',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 193 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统消息表\r\n' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_turn_activity
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity`;
CREATE TABLE `re_turn_activity`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '标题',
  `node` tinyint(2) DEFAULT 1 COMMENT '执行时间类型    1 单次  2 每周   3 每月   ',
  `execute_day` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '执行时间设置   如果是每周，1代表星期一，2代表星期2，多个逗号拼接，如果是每月，1代表 1号，2代表 2号，多个逗号拼接',
  `is_ushare` tinyint(1) DEFAULT 1 COMMENT '是否阅享用户  1  是  2 否',
  `real_info` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '填写信息  如果是阅享用户，则无效    1、姓名  2、电话   3、身份证号码  4、读者证   多个  | 连接',
  `img` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT 'default/default_readshare_activity.png',
  `number` int(11) DEFAULT 1 COMMENT '次数，根据 vote_way执行方式， 总形式，则表示总执行次数，每日模式，则是每日次数    ',
  `round_number` int(11) DEFAULT 1 COMMENT '每轮几次    设置前端抽奖后（或抽几次后），重置界面',
  `share_number` int(11) DEFAULT NULL COMMENT '分享获取答题次数，每日几次  ',
  `rule_type` tinyint(1) DEFAULT NULL COMMENT '规则方式   1 内容   2外链',
  `rule` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '规则内容     rule_type  为1 这里是编辑器内容   2 这里是链接',
  `start_time` datetime(0) DEFAULT NULL COMMENT '活动开始时间',
  `end_time` datetime(0) DEFAULT NULL COMMENT '活动结束时间    不设置则表示结束时间',
  `turn_start_time` datetime(0) DEFAULT NULL COMMENT '开始抽奖的时间 ',
  `turn_end_time` datetime(0) DEFAULT NULL COMMENT '结束抽奖的时间',
  `invite_code` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '邀请码',
  `vote_way` tinyint(1) DEFAULT 1 COMMENT '活动执行方式    1 总数量形式   2 每日执行形式',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除   1 正常 2 已删除',
  `is_play` tinyint(1) DEFAULT 2 COMMENT '是否发布   1 已发布  2 未发布',
  `manage_id` int(11) DEFAULT NULL,
  `browse_num` int(11) DEFAULT 0 COMMENT '浏览量',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL,
  `share_title` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '分享标题',
  `share_img` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '分享图片',
  `is_show_address` tinyint(1) DEFAULT 1 COMMENT '是否需要填写  1 需要  2 不需要',
  `technical_support` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '成都贝图科技有限公司' COMMENT '技术支持,如果不填，则前端不显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '翻一翻活动' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_turn_activity_browse_count
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_browse_count`;
CREATE TABLE `re_turn_activity_browse_count`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL,
  `unit_id` int(11) DEFAULT 0 COMMENT '单位id   0 代表全部',
  `date` date DEFAULT NULL,
  `hour` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '小时',
  `number` int(11) DEFAULT 0,
  `create_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '翻一翻活动浏览量统计表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_turn_activity_gift
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_gift`;
CREATE TABLE `re_turn_activity_gift`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '礼品名称',
  `img` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '礼品图片',
  `intro` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '礼品简介',
  `total_number` int(11) DEFAULT NULL COMMENT '礼品总数量',
  `price` decimal(6, 2) DEFAULT NULL COMMENT '红包金额',
  `percent` decimal(6, 2) DEFAULT NULL COMMENT '中奖率(百分比)',
  `type` tinyint(1) DEFAULT NULL COMMENT '1文化红包 2精美礼品',
  `way` tinyint(1) DEFAULT 0 COMMENT '领取方式   1 自提  2邮递   红包无此选项',
  `start_time` datetime(0) DEFAULT NULL COMMENT '自提开始时间',
  `end_time` datetime(0) DEFAULT NULL COMMENT '自提结束时间',
  `tel` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系电话',
  `contacts` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系人',
  `province` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '省',
  `city` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '市',
  `district` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '区',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '详细地址',
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '自提备注',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否被删除 1正常 2已删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `activity_index`(`act_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '礼物表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_turn_activity_gift_config
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_gift_config`;
CREATE TABLE `re_turn_activity_gift_config`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `percent_way` tinyint(1) DEFAULT 1 COMMENT '概率方式   1 单个奖品独立概率方式    2 所以奖品总概率形式',
  `gift_max_number_day` int(11) DEFAULT NULL COMMENT '礼物每日发送奖品最大数量',
  `red_max_number_day` int(11) DEFAULT NULL COMMENT '礼物每日发送奖品最大数量',
  `user_gift_number_day` int(11) DEFAULT 1 COMMENT '用户当日可获得多少实物礼品',
  `user_red_number_day` int(11) DEFAULT 1 COMMENT '用户当日可获得多少红包礼品',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '礼品领取总配置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_turn_activity_gift_public
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_gift_public`;
CREATE TABLE `re_turn_activity_gift_public`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `type` tinyint(1) DEFAULT NULL COMMENT '礼物类型  1文化红包 2精美礼品',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '礼品名称',
  `way` tinyint(1) DEFAULT NULL COMMENT '操作方式  1 投放  2追加',
  `number` int(11) DEFAULT NULL COMMENT '礼品数量',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否被删除 1正常 2已删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `activity_index`(`act_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '礼物公示表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_turn_activity_gift_record
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_gift_record`;
CREATE TABLE `re_turn_activity_gift_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `unit_id` int(11) DEFAULT NULL COMMENT '单位id',
  `gift_id` int(11) DEFAULT NULL COMMENT '礼物id',
  `before_number` int(11) DEFAULT 0 COMMENT '最加之前数量    ',
  `later_number` int(11) DEFAULT 0 COMMENT '追加之后数量',
  `gift_number` int(11) DEFAULT 0 COMMENT '礼物数量',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态 1 增加 2减少',
  `manage_id` int(11) DEFAULT NULL COMMENT '管理员id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '记录时间',
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `act_id`(`act_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '礼物变动记录表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_turn_activity_gift_time
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_gift_time`;
CREATE TABLE `re_turn_activity_gift_time`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `start_time` datetime(0) DEFAULT NULL COMMENT '奖品开始发放时间   ',
  `end_time` datetime(0) DEFAULT NULL COMMENT '结束时间   和开始时间对应不允许跨天',
  `type` tinyint(4) DEFAULT NULL COMMENT '奖品类型 1文化红包 2精美礼品',
  `number` int(11) DEFAULT NULL COMMENT '发放数量',
  `user_gift_number` int(11) DEFAULT 1 COMMENT '用户当日可获得多少礼品',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除 1正常 2删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '礼品领取排班表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_turn_activity_invite
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_invite`;
CREATE TABLE `re_turn_activity_invite`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `user_guid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `invite_code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `create_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '活动邀请码' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_turn_activity_limit_address
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_limit_address`;
CREATE TABLE `re_turn_activity_limit_address`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '每个活动独立',
  `province` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '省',
  `city` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '市',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1 正常  2删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '删除、修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '答题活动限制参与地址' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_turn_activity_resource
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_resource`;
CREATE TABLE `re_turn_activity_resource`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '标题',
  `theme_color` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '主题色',
  `progress_color` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '进度条颜色',
  `invite_color` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '邀请码颜色',
  `theme_text_color` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '主题文字色',
  `warning_color` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '警告颜色',
  `error_color` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '错误颜色',
  `resource_path` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '资源路径，中间半截',
  `is_change` tinyint(1) DEFAULT 2 COMMENT '资源是否变化，包括图片资源是否改变  1 未变化  2 有变化',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '活动资源表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_turn_activity_share
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_share`;
CREATE TABLE `re_turn_activity_share`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL,
  `user_guid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '区域名称',
  `date` date DEFAULT NULL COMMENT '日期',
  `number` int(11) DEFAULT 0 COMMENT '分享次数',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '活动分享次数表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_turn_activity_user_gift
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_user_gift`;
CREATE TABLE `re_turn_activity_user_gift`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `user_guid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '用户guid',
  `gift_id` int(11) DEFAULT NULL COMMENT '礼物id',
  `order_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '订单号',
  `use_prize_record_id` int(11) DEFAULT NULL COMMENT '用户使用抽奖机会id',
  `price` decimal(6, 2) DEFAULT NULL COMMENT '红包大小',
  `state` tinyint(1) DEFAULT 1 COMMENT '礼物状态 1未发放 2已发放 3未发货 4邮递中 5已到货 6 未领取  7 已领取',
  `date` date DEFAULT NULL COMMENT '获奖日期',
  `hour` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '小时',
  `type` tinyint(1) DEFAULT NULL COMMENT '1文化红包 2精美礼品',
  `create_time` datetime(0) DEFAULT NULL COMMENT '红包创建时间',
  `pay_time` datetime(0) DEFAULT NULL COMMENT '红包到账时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '到货时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `activity_index`(`act_id`) USING BTREE,
  INDEX `user_index`(`user_guid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户获取礼物表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_turn_activity_user_unit
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_user_unit`;
CREATE TABLE `re_turn_activity_user_unit`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_guid` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户id',
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '姓名',
  `tel` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '电话号码',
  `id_card` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '身份证',
  `reader_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '读者证',
  `unit_id` int(11) DEFAULT NULL COMMENT '电话号码',
  `create_time` datetime(0) DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '删除、修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户所属单位表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_turn_activity_user_use_prize_record
-- ----------------------------
DROP TABLE IF EXISTS `re_turn_activity_user_use_prize_record`;
CREATE TABLE `re_turn_activity_user_use_prize_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_id` int(11) DEFAULT NULL COMMENT '活动id',
  `user_guid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '用户guid',
  `date` date DEFAULT NULL COMMENT '日期',
  `status` tinyint(1) DEFAULT NULL COMMENT '是否中奖  1 中奖  2 未中奖',
  `order_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '订单号',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `act_id`(`act_id`) USING BTREE,
  INDEX `user_guid`(`user_guid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 113 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户抽奖记录表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_user_account_borrow
-- ----------------------------
DROP TABLE IF EXISTS `re_user_account_borrow`;
CREATE TABLE `re_user_account_borrow`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL COMMENT '读者证号id',
  `year` int(10) DEFAULT NULL COMMENT '年',
  `number` int(10) DEFAULT NULL COMMENT '数量',
  `create_time` datetime(0) DEFAULT NULL COMMENT '开始时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '最近一次修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '图书馆系统 读者证号对应的借阅数量' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_user_account_borrow_log
-- ----------------------------
DROP TABLE IF EXISTS `re_user_account_borrow_log`;
CREATE TABLE `re_user_account_borrow_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `api_id` int(11) UNSIGNED DEFAULT NULL COMMENT '外部接口返回的id',
  `eventType` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '状态 读者借出 Ea   读者续借Eb   读者过期Ec  文献损坏Ed   读者丢失文献Ef',
  `loginWay` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `readerId` int(10) DEFAULT NULL COMMENT '读者id',
  `assetId` int(10) DEFAULT NULL,
  `servrequestId` int(10) DEFAULT NULL,
  `metatable` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `metaid` int(10) DEFAULT NULL,
  `curSublib` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `curLocal` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `initSublib` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `initLocal` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `cardno` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `readerLib` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `realName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `barcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `cirType` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `volumeno` int(10) DEFAULT NULL,
  `title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `author` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `isbn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `publish` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `callno` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `price` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `notes` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `adminName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `updateDateTime` datetime(0) DEFAULT NULL,
  `eventTypeNote` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `loginWayNote` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `curSublibNote` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `curLocalNote` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `initSublibNote` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `initLocalNote` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `readerLibNote` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `cirTypeNote` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `create_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '图书馆系统 读者证号对应的借阅数据日志表，在表的标识' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_user_account_lib
-- ----------------------------
DROP TABLE IF EXISTS `re_user_account_lib`;
CREATE TABLE `re_user_account_lib`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `account` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '账户',
  `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '密码',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户名',
  `cash` decimal(10, 0) DEFAULT NULL COMMENT '押金',
  `id_card` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '身份证号码',
  `sex` tinyint(1) DEFAULT 1 COMMENT '1男 2女',
  `tel` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '电话',
  `type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '普通卡' COMMENT '借阅卡类型 ',
  `time` datetime(0) DEFAULT NULL COMMENT '借阅证注册时间',
  `is_surpass` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT 'NO',
  `pavilion` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '开户馆',
  `pavilion_note` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '开户馆注释',
  `end_time` datetime(0) DEFAULT NULL COMMENT '借阅证有效期',
  `diff_money` decimal(10, 2) DEFAULT 0.00 COMMENT '欠费（图书馆欠费金额）',
  `recom_diff_money` decimal(10, 2) DEFAULT 0.00 COMMENT '书店荐购欠费金额',
  `status_card` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '有效' COMMENT '证件状态',
  `recomed_num` mediumint(5) DEFAULT 0 COMMENT '用户一年采购本数',
  `recomed_money` decimal(10, 2) DEFAULT 0.00 COMMENT '用户一年采购金额',
  `s_time` datetime(0) DEFAULT NULL COMMENT ' 一年的开始时间',
  `e_time` datetime(0) DEFAULT NULL COMMENT '一年的结束时间',
  `last_time` datetime(0) DEFAULT NULL COMMENT '上一次采购时间',
  `last_mmm` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '上一次采购月份   Ym',
  `last_num` tinyint(3) DEFAULT 0 COMMENT '上一次采购的本数，如果还是本月就是本月采购的本数，如果不是本月，就是上一月采购的本数',
  `last_money` decimal(10, 2) DEFAULT 0.00 COMMENT '上一次采购的金额，如果还是本月就是本月采购的金额，如果不是本月，就是上一月采购的金额',
  `purch_num_lib` mediumint(5) DEFAULT 0 COMMENT '读者在图书馆采购本数',
  `purch_money_lib` decimal(10, 2) DEFAULT 0.00 COMMENT '读者在图书馆采购金额',
  `purch_num_shop` int(5) DEFAULT 0 COMMENT '书店采购总本书',
  `purch_money_shop` decimal(10, 2) DEFAULT 0.00 COMMENT '书店采购总金额',
  `qr_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '读者证二维码，与用户表对应',
  `score` int(10) DEFAULT 0 COMMENT '积分',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL COMMENT '数据更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `account`(`account`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户账号表（图书馆读者证号表）' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_user_address
-- ----------------------------
DROP TABLE IF EXISTS `re_user_address`;
CREATE TABLE `re_user_address`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户姓名',
  `tel` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '电话号码',
  `province` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '省',
  `city` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '市',
  `district` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '区',
  `street` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '街道',
  `address` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '详细地址',
  `area_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT ' 地区代码',
  `is_default` tinyint(1) DEFAULT 2 COMMENT '是否默认   1 默认  2 非默认',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1 正常  2删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '删除、修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户收货地址表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_user_draw_address
-- ----------------------------
DROP TABLE IF EXISTS `re_user_draw_address`;
CREATE TABLE `re_user_draw_address`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_guid` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户id',
  `type` tinyint(1) DEFAULT NULL COMMENT '类型  1、答题活动   2、翻一翻',
  `act_id` int(11) DEFAULT NULL COMMENT '每个活动独立',
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户姓名',
  `tel` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '电话号码',
  `province` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '省',
  `city` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '市',
  `district` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '区',
  `street` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '街道',
  `address` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '详细地址',
  `area_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT ' 地区代码',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除  1 正常  2删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '添加时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '删除、修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户抽奖收货地址表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_user_info
-- ----------------------------
DROP TABLE IF EXISTS `re_user_info`;
CREATE TABLE `re_user_info`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL COMMENT '图书证表id',
  `wechat_id` int(11) DEFAULT NULL COMMENT '微信表id',
  `token` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户token',
  `qr_url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户二维码',
  `qr_code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '二维码标识code   固定 前  2位 为  10',
  `invite_user_id` int(11) DEFAULT NULL,
  `invite_account_id` int(11) DEFAULT NULL,
  `bind_time` datetime(0) DEFAULT NULL COMMENT '绑定时间',
  `is_volunteer` tinyint(1) DEFAULT 2 COMMENT '是否是志愿者   1 是  2 否',
  `volunteer_service_time` int(11) DEFAULT 0 COMMENT '志愿者服务时长 单位 分钟',
  `score` int(11) DEFAULT 0 COMMENT '用户积分，与图书馆账号表中的score 2选一',
  `create_time` datetime(0) DEFAULT NULL,
  `invite_number` int(11) DEFAULT 0 COMMENT '邀请用户数量',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `token`(`token`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 168 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_user_protocol
-- ----------------------------
DROP TABLE IF EXISTS `re_user_protocol`;
CREATE TABLE `re_user_protocol`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) DEFAULT 1 COMMENT '1 用户协议    2  大赛原创声明  3 景点打卡  4 在线办证  5、扫码借借阅规则',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '内容',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户协议' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_user_violate
-- ----------------------------
DROP TABLE IF EXISTS `re_user_violate`;
CREATE TABLE `re_user_violate`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `activity_violate_num` int(11) DEFAULT 0 COMMENT '用户活动违规次数',
  `reservation_violate_num` int(11) DEFAULT 0 COMMENT '用户文化预约违规次数',
  `contest_violate_num` int(11) DEFAULT 0 COMMENT '线上大赛违规次数',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL,
  `activity_violate_clear_time` datetime(0) DEFAULT NULL COMMENT '清除违规时间，在此之前的违规都不算，为空，则都计算在内',
  `reservation_violate_clear_time` datetime(0) DEFAULT NULL COMMENT '清除违规时间，在此之前的违规都不算，为空，则都计算在内',
  `contest_violate_clear_time` datetime(0) DEFAULT NULL COMMENT '清除违规时间，在此之前的违规都不算，为空，则都计算在内',
  `scenic_violate_num` int(11) DEFAULT 0 COMMENT '用户景点打卡违规次数  ',
  `scenic_violate_clear_time` datetime(0) DEFAULT NULL COMMENT '清除违规时间，在此之前的违规都不算，为空，则都计算在内',
  `study_room_reservation_violate_clear_time` datetime(0) DEFAULT NULL COMMENT '清除违规时间，在此之前的违规都不算，为空，则都计算在内',
  `study_room_reservation_violate_num` int(11) DEFAULT 0 COMMENT '自习室门禁预约违规次数',
  `competite_violate_num` int(11) UNSIGNED ZEROFILL NOT NULL COMMENT '赛违规次数',
  `competite_violate_clear_time` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 168 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户违规次数表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_user_wechat_info
-- ----------------------------
DROP TABLE IF EXISTS `re_user_wechat_info`;
CREATE TABLE `re_user_wechat_info`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `union_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `session_key` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户微信id',
  `nickname` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '微信昵称',
  `head_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '微信头像',
  `tel` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '微信获取的用户电话',
  `create_time` datetime(0) DEFAULT NULL COMMENT '数据插入时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '上一次绑定借阅证时间',
  `invite_user_id` int(11) DEFAULT NULL,
  `invite_account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `open_id`(`open_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 168 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '微信用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_video_live
-- ----------------------------
DROP TABLE IF EXISTS `re_video_live`;
CREATE TABLE `re_video_live`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '线上大赛活动id',
  `cid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '网易直播cid',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '活动名称',
  `channel_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '网易云信直播频道名称',
  `img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'default/default_picture_live.png' COMMENT '封面图片',
  `tel` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '咨询方式',
  `host_handle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '主办单位',
  `address` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '地址',
  `start_time` datetime(0) DEFAULT NULL COMMENT '直播开始时间（对外展示时间，实际不以这个时间为准）',
  `end_time` datetime(0) DEFAULT NULL COMMENT '投稿结束时间',
  `browse_num` int(11) DEFAULT 0 COMMENT '浏览量',
  `size` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '文件大小',
  `total_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '总时长(毫秒)  默认0',
  `status` tinyint(1) DEFAULT 1 COMMENT '直播状态   1 未开始直播 2 直播中 3 已结束',
  `wy_status` tinyint(1) DEFAULT 0 COMMENT '网易直播状态   0：空闲； 1：直播； 2：禁用； 3：直播录制',
  `intro` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '简介',
  `http_pull_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'http拉流地址',
  `hls_pull_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'hls拉流地址',
  `push_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '推流地址',
  `rtmp_pull_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'rtmp拉流地址',
  `manage_id` int(10) UNSIGNED DEFAULT NULL COMMENT '管理员id',
  `is_play` tinyint(1) DEFAULT 1 COMMENT '是否发布 1.发布 2.未发布    默认1',
  `is_del` tinyint(1) UNSIGNED DEFAULT 1 COMMENT '是否删除   1.正常   2.已删除',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  `video_num` int(11) DEFAULT 0 COMMENT '视频个数(主要是统计直播分段视频个数) 默认0',
  `qr_code` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `qr_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '二维码链接',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '视频直播活动表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_video_live_resource
-- ----------------------------
DROP TABLE IF EXISTS `re_video_live_resource`;
CREATE TABLE `re_video_live_resource`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '线上大赛活动id',
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '视频名称',
  `act_id` int(11) DEFAULT NULL COMMENT '视频id',
  `link` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分段链接原链接',
  `video_address` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '本地视频地址',
  `format` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '格式',
  `size` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分段视频大小',
  `duration_time` int(11) DEFAULT NULL COMMENT '分段视频时长  毫秒',
  `start_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '视频开始时间   (毫秒)',
  `end_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '视频结束时间   (毫秒)',
  `is_del` tinyint(1) UNSIGNED DEFAULT 1 COMMENT '是否删除   1.正常   2.已删除',
  `browse_num` int(11) DEFAULT 0 COMMENT '浏览量',
  `callbcak_video_info` varchar(5000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '获取原本的回调信息',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '视频直播数据资源表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_video_live_vote
-- ----------------------------
DROP TABLE IF EXISTS `re_video_live_vote`;
CREATE TABLE `re_video_live_vote`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '线上大赛活动id',
  `act_id` int(11) DEFAULT NULL COMMENT '网易直播cid',
  `user_id` int(11) DEFAULT NULL COMMENT '活动名称',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '视频直播活动表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_violate_config
-- ----------------------------
DROP TABLE IF EXISTS `re_violate_config`;
CREATE TABLE `re_violate_config`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `activity` int(11) DEFAULT 0 COMMENT '用户活动可违规次数',
  `reservation` int(11) DEFAULT 0 COMMENT '用户预约可违规次数',
  `contest` int(11) DEFAULT 0 COMMENT '用户线上大赛可违规次数',
  `activity_clear_day` int(11) DEFAULT 0 COMMENT '用户活动自动清空违规天数    0 为用不清空',
  `reservation_clear_day` int(11) DEFAULT 0 COMMENT '用户预约自动清空违规天数    0 为用不清空',
  `contest_clear_day` int(11) DEFAULT 0 COMMENT '用户线上大赛自动清空违规天数   0 为用不清空',
  `create_time` datetime(0) DEFAULT NULL COMMENT '数据上次更新时间',
  `change_time` datetime(0) DEFAULT NULL COMMENT '数据上次更新时间',
  `scenic` int(11) DEFAULT 0 COMMENT '用户景点打卡可违规次数',
  `scenic_clear_day` int(11) DEFAULT 0 COMMENT '用户景点打卡自动清空违规天数  0 为用不清空',
  `study_room_reservation` int(11) DEFAULT 0 COMMENT '用户预约可违规次数',
  `study_room_reservation_clear_day` int(11) DEFAULT 0 COMMENT '用户预约自动清空违规天数    0 为用不清空',
  `competite` int(11) DEFAULT 0 COMMENT '用户线上大赛可违规次数',
  `competite_clear_day` int(11) DEFAULT 0 COMMENT '用户线上大赛自动清空违规天数   0 为用不清空',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '违规设置' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_volunteer_apply
-- ----------------------------
DROP TABLE IF EXISTS `re_volunteer_apply`;
CREATE TABLE `re_volunteer_apply`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL COMMENT '岗位名称',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '姓名',
  `sex` tinyint(1) DEFAULT NULL COMMENT '性别   1 男  2 女 ',
  `birth` date DEFAULT NULL COMMENT '信息必填参数   1、姓名 2、性别 3、生日  4、身份证号码  5、名族 6、政治面貌  7、健康状态  8、联系电话  9、毕业院校  10、专业  11、工作单位 12、教育程度  13、个人简介  14、特长  ',
  `id_card` varchar(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '身份证号码',
  `nation` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '民族',
  `politics` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '政治面貌',
  `health` tinyint(1) UNSIGNED ZEROFILL DEFAULT 1 COMMENT '健康状态  1 良好  2 较好 3 较差',
  `tel` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '电话号码',
  `school` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '毕业院校',
  `major` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '专业',
  `unit` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '工作单位',
  `degree` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '教育程度',
  `address` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '地址',
  `intro` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '个人简介',
  `specialty` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '特长',
  `status` tinyint(1) DEFAULT 4 COMMENT '报名状态    1. 已通过  2.已取消  3.已拒绝  4.审核中',
  `reason` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '拒绝原因',
  `manage_id` int(11) DEFAULT NULL,
  `create_time` datetime(0) DEFAULT NULL COMMENT '开始时间',
  `expire_time` datetime(0) DEFAULT NULL COMMENT '过期时间',
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '志愿者申请' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_volunteer_apply_intention
-- ----------------------------
DROP TABLE IF EXISTS `re_volunteer_apply_intention`;
CREATE TABLE `re_volunteer_apply_intention`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '志愿者id',
  `intention_id` int(10) UNSIGNED DEFAULT NULL COMMENT '意向id',
  `volunteer_id` int(10) DEFAULT NULL COMMENT '志愿者id',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '志愿者活动用户关联服务意向表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_volunteer_apply_time
-- ----------------------------
DROP TABLE IF EXISTS `re_volunteer_apply_time`;
CREATE TABLE `re_volunteer_apply_time`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '志愿者id',
  `volunteer_id` int(10) DEFAULT NULL COMMENT '志愿者id',
  `times_id` int(10) DEFAULT NULL COMMENT '星期几   1-7',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '志愿者活动用户关联服务时间表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_volunteer_notice
-- ----------------------------
DROP TABLE IF EXISTS `re_volunteer_notice`;
CREATE TABLE `re_volunteer_notice`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '简介',
  `create_time` datetime(0) DEFAULT NULL COMMENT '开始时间',
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '志愿者须知' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_volunteer_position
-- ----------------------------
DROP TABLE IF EXISTS `re_volunteer_position`;
CREATE TABLE `re_volunteer_position`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '岗位名称',
  `intro` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '简介',
  `is_reader` tinyint(1) DEFAULT 2 COMMENT '打卡是否需要绑定读者证  1 是  2 否',
  `real_info` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '报名所需信息 多个 | 链接    1、姓名 2、性别 3、生日  4、身份证号码  5、民族 6、政治面貌  7、健康状态  8、联系电话  9、毕业院校  10、专业  11、工作单位 12、教育程度  13、个人简介  14、特长  15、地址  ',
  `real_info_must` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '信息必填参数 多个 | 链接     1、姓名 2、性别 3、生日  4、身份证号码  5、民族 6、政治面貌  7、健康状态  8、联系电话  9、毕业院校  10、专业  11、工作单位 12、教育程度  13、个人简介  14、特长  15、用户地址 ',
  `expire_time` datetime(0) DEFAULT NULL COMMENT '岗位有效期截止时间',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '删除  1 正常 2删除',
  `is_play` tinyint(1) DEFAULT 2 COMMENT '是否发布 1.发布 2.未发布    默认2',
  `create_time` datetime(0) DEFAULT NULL COMMENT '开始时间',
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '志愿者岗位' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for re_volunteer_service_intention
-- ----------------------------
DROP TABLE IF EXISTS `re_volunteer_service_intention`;
CREATE TABLE `re_volunteer_service_intention`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '服务意向',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除     1  正常  2 删除',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '志愿者服务意向' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_volunteer_service_time
-- ----------------------------
DROP TABLE IF EXISTS `re_volunteer_service_time`;
CREATE TABLE `re_volunteer_service_time`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `times` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '服务意向',
  `is_del` tinyint(1) DEFAULT 1 COMMENT '是否删除     1  正常  2 删除',
  `create_time` datetime(0) DEFAULT NULL,
  `change_time` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '志愿者服务意向' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for re_volunteer_service_time_log
-- ----------------------------
DROP TABLE IF EXISTS `re_volunteer_service_time_log`;
CREATE TABLE `re_volunteer_service_time_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '积分明细id',
  `user_id` int(11) UNSIGNED DEFAULT NULL COMMENT '用户id/团长',
  `account_id` int(11) UNSIGNED DEFAULT NULL COMMENT '用户id/团长',
  `service_time` int(10) DEFAULT NULL COMMENT '消耗或增加的积分     1 ， -1',
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类型',
  `intro` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '说明',
  `manage_id` int(11) DEFAULT NULL COMMENT '积分操作人',
  `create_time` datetime(0) DEFAULT NULL COMMENT '新增时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 50 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '服务时长明细表' ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
